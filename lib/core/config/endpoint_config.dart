class EndpointConfig {
  static String basePathUrl = 'https://dev.api.subtraid.com/';

  static String userLogin = 'users/login';
  static String userRegister = 'users/register';

  static String userUpdatePass = 'users/updatepass';

  static String contactsUrl = 'users';

  static String myContactsUrl = 'users/full-connections';

  static String userChats = 'users/chats';

  static String userUrl = 'users';

  static String userProjects = 'users/projects';

  static String projects = 'projects';

  static String userProjectsList = 'users/projects-list';

  static String projectCard = 'projects/card';

  static String projectList = 'projects/list';

  static String projectLabel = 'projects/label';

  static String projectSubproject = 'projects/subproject';

  static String weather = 'weather';

  static String userEmployerPpg = 'users/employer/ppg';

  static String companiesProjectGroupPermission =
      'companies/project_group_permission';

  static String userRequests = 'users/requests';

  static String userAcceptEmployeement = 'users/accept-employeement';

  static String userFavourite = 'users/favourite';

  static String userEmployerEmployees = 'users/employer/employees';

  static String userEmployerCrews = 'users/employer/crews';

  static String userEvents = 'users/events';

  static String events = 'events';

  static String timesheetsCompany = 'timesheets/company';

  static String timesheetsProject = 'timesheets/project';

  static String timesheetsCard = 'timesheets/card';

  static String timesheets = 'timesheets';

  static String companies = 'companies';

  static String companyAddEmployee = 'companies/add-employee';

  static String companiesAddcrew = 'companies/addcrew';

  static String companiesUpdatecrew = 'companies/updatecrew';

  static String companiesDeletecrew = 'companies/deletecrew';

  static String chatsUpdateSeen = 'chats/updateSeen';

  static String chatsAddMessage = 'chats/add_Message';

  static String memberRemove = 'member/remove';

  static String notificationsCount = 'users/notifications-count';

  static String userNotifications = 'users/notifications';

  static String notifications = 'notifications';

  static String templates = 'templates';

  static String acceptCollaborator = 'users/accept-collaborator';

  static String createCollaborator = 'projects/create-collaborator';
}
