import 'package:flutter/material.dart';

class ColorConfig {
  static Color primary = Color(0xff124584);

  static Color lightPrimary = Color(0xff124584).withOpacity(0.1);

  static Color orange = Color(0xffFBB03B);
  static Color activeGrey = Color(0x7E124584);
  static Color searchBarFillColor = Color(0xffC1DDFF);
  static Color lightOrange = Colors.orange.shade100;

  static Color fillColor = Color(0xff00A99D);
  static Color fillColorShade = Color(0x2F00A99D);

  static Color black2 = Color(0xff474747);
  static Color black4 = Color(0x48474747);
  static Color black5 = Color(0xAC000000);
  static Color black6 = Color(0x88000000);

  static Color grey1 = Color(0x96000000);
  static Color grey2 = Color(0x79000000);
  static Color grey3 = Color(0x65AEAEAE);
  static Color grey4 = Color(0xff757576);
  static Color grey5 = Color(0x8B000000);
  static Color grey6 = Color(0xADD2D2D2);
  static Color grey7 = Color(0x72000000);
  static Color grey9 = Color(0xffDFE1E6);
  static Color grey10 = Color(0xffF6F7F8);
  static Color grey11 = Color(0x9C000000);
  static Color grey14 = Color(0x88000000);
  static Color grey15 = Colors.white.withOpacity(0.66);
  static Color grey16 = Color(0x84000000);
  static Color grey17 = Color(0xAEAEAE8D);
  static Color grey18 = Color(0xff000079);
  static Color grey19 = Color(0xffE7EDF3);

  static Color red1 = Color(0xffEB5B5B);
  static Color red2 = Color(0xffDE1F1F);
  static Color red3 = Color(0xffEA3434);
  static Color red4 = Color(0xffF03131);

  static Color blue1 = Color(0xffF3F7FF);
  static Color blue2 = Colors.white.withOpacity(0.33);
  static Color blue3 = Colors.blue.shade100;
  static Color blue4 = Color(0xB4D9E4FF);
  static Color blue5 = Color(0x39397FD5).withOpacity(0.1);
  static Color blue6 = Color(0xAF124584);
  static Color blue7 = Color(0x9BF8FBFF);
  static Color blue9 = Color(0x9A124584);
  static Color blue10 = Color(0x33124584);
  static Color blue11 = Color(0x3B124584);
  static Color blue12 = Color(0xff104584);
  static Color blue13 = Color(0xff2A3A48);
  static Color blue16 = Color(0xffC1DDFF);
  static Color blue17 = Color(0xffE4ECFF).withOpacity(0.7);
  static Color blue18 = Color(0xffE4ECFF);

  static Color black1 = Colors.black.withOpacity(0.5);

  static Color shadow1 = Color(0x21124584);
  static Color shadow2 = Color(0x29000000);

  static Color green2 = Color(0xff00A99D);
  static Color green4 = Color(0xff58E64B);
  static Color green5 = Color(0xff0B9F94);
  static Color green6 = Color(0xff0CADA1);

  static Color completed = Color(0xff33bab0);
  static Color active = Color(0xff0052cc);
  static Color irrelevant = Color(0xffde351b);
  static Color resolved = Color(0xff00875a);
}
