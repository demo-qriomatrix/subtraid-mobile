import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';

bool getCompanyUser(AuthenticationBloc authenticationBloc) {
  if (authenticationBloc.state is AuthentcatedState) {
    return ((authenticationBloc.state as AuthentcatedState).user?.accountType ??
            '') ==
        'company';
  }

  return false;
}

String getUserId(AuthenticationBloc authenticationBloc) {
  if (authenticationBloc.state is AuthentcatedState) {
    return (authenticationBloc.state as AuthentcatedState).user?.id;
  }

  return null;
}
