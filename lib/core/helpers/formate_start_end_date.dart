import 'package:intl/intl.dart';

String formatStartEndDate(String date) {
  if (date != null)
    try {
      return DateFormat('dd MMM yyyy')
          .format(DateFormat('MM/dd/yyyy hh:mm a').parse(date));
    } catch (e) {
      // print(e);
      // print(date);
      return DateFormat('dd MMM yyyy').format(DateTime.now());
    }
  else
    return DateFormat('dd MMM yyyy').format(DateTime.now());
}

DateTime formatStartEndDateToDate(String date) {
  if (date != null)
    try {
      return DateFormat('MM/dd/yyyy hh:mm a').parse(date);
    } catch (e) {
      // print(e);
      // print(date);
      return DateTime.now();
    }
  else
    return DateTime.now();
}
