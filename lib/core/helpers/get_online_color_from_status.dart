import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/enums/online_status_enum.dart';

Color getOnlineStatusColor(OnlineStatus onlineStatus) {
  switch (onlineStatus) {
    case OnlineStatus.online:
      return ColorConfig.green4;

      break;
    case OnlineStatus.offline:
      return ColorConfig.blue16;
      break;
    case OnlineStatus.away:
      break;
  }

  return null;
}
