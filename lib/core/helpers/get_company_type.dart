String getCompanyType(String type) {
  if (type == 'General Contractor') {
    return 'gc';
  }
  if (type == 'Contractor') {
    return 'contractor';
  }

  return null;
}
