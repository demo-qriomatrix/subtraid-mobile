String getDurationFromSeconds(int _start) {
  int start = _start.abs();

  int hours = Duration(seconds: start).inHours;
  int minutes = Duration(seconds: start).inMinutes.remainder(60);
  int seconds = Duration(seconds: start).inSeconds.remainder(60);

  String time = "${_start.isNegative ? '-' : ''}" +
      "${hours < 10 ? '0$hours' : '$hours'}" +
      "${minutes < 10 ? ':0$minutes' : ':$minutes'}" +
      "${seconds < 10 ? ':0$seconds' : ':$seconds'}";

  return time;
}
