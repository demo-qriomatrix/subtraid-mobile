import 'package:Subtraid/features/calender/repository/calender_repository.dart';
import 'package:Subtraid/features/more/company/features/companies/repository/companies_repository.dart';
import 'package:Subtraid/features/more/company/features/crew/repository/crew_reposiory.dart';
import 'package:Subtraid/features/more/company/features/employee/repository/employee_reposiory.dart';
import 'package:Subtraid/features/notifications/repository/notification_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/repository/label_repository.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/repository/projects_timesheets_repository.dart';
import 'package:Subtraid/features/projects/features/templates/repository/templates_repository.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/authentication/repository/authentication_repository.dart';
import 'package:Subtraid/features/chat/repository/chat_repository.dart';
import 'package:Subtraid/features/contacts/repository/contacts_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/repository/card_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/repository/collaborators_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/repository/list_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/repository/weather_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/sub_project/repository/sub_project_repository.dart';
import 'package:Subtraid/features/projects/features/project/repository/project_repository.dart';
import 'package:Subtraid/features/projects/features/project_list/repository/project_list_repository.dart';
import 'package:Subtraid/features/projects/features/project_permissions/repository/project_permission_repository.dart';

final sl = GetIt.instance;

Future<void> initDependencyInjector() async {
  sl.registerSingleton<Dio>(
    Dio(),
  );
  sl.registerLazySingleton<HttpClient>(
    () => HttpClient(dio: sl()),
  );

  sl.registerLazySingleton<AuthenticationRepository>(
    () => AuthenticationRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ContactsRepository>(
    () => ContactsRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ProjectsRepository>(
    () => ProjectsRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<CardRepository>(
    () => CardRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ListRepository>(
    () => ListRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<SubProjectRepository>(
    () => SubProjectRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<WeatherRepository>(
    () => WeatherRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ProjectPermissionRepository>(
    () => ProjectPermissionRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ChatRepository>(
    () => ChatRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ProjectRepository>(
    () => ProjectRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<UserRequestsRepository>(
    () => UserRequestsRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<EmployeeRepository>(
    () => EmployeeRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<CrewRepository>(
    () => CrewRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<CalenderRepository>(
    () => CalenderRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<ProjectsTimesheetsRepository>(
    () => ProjectsTimesheetsRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<LabelRepository>(
    () => LabelRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<NotificationRepository>(
    () => NotificationRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<TemplatesRepository>(
    () => TemplatesRepository(httpClient: sl()),
  );
  sl.registerLazySingleton<CompaniesRepository>(
    () => CompaniesRepository(httpClient: sl()),
  );
}
