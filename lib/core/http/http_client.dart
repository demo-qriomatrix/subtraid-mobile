import 'package:dio/dio.dart';

import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';

class HttpClient {
  final Dio dio;

  HttpClient({@required this.dio}) : assert(dio != null) {
    addBasePathInterceptor();
    addUrlInterceptor();
  }

  InterceptorsWrapper tokenInteceptor;
  InterceptorsWrapper status401Interceptor;

  void addBasePathInterceptor() {
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      if (!options.path.startsWith("http")) {
        options.baseUrl = EndpointConfig.basePathUrl;
      }
    }));

    // dio.interceptors.add(LogInterceptor(requestHeader: true));

    // dio.interceptors.add(LogInterceptor(
    //     requestBody: true, requestHeader: true, responseBody: true));
  }

  addTokenInterceptor(token) {
    print('addubg token');

    tokenInteceptor = InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers['Authorization'] = "Bearer " + token;
    });

    // Add interceptor
    dio.interceptors.add(tokenInteceptor);
  }

  removeTokenInterceptor() {
    dio.interceptors.remove(tokenInteceptor);
  }

  void addUrlInterceptor() {
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      // print(options.uri);
    }, onResponse: (Response response) {
      print('onApiCall ${response.statusCode} ${response.request.uri}');
    }, onError: (DioError error) {
      print('------------------------');
      print(error.error);
      print(error.request.uri);
      if (error.type == DioErrorType.RESPONSE) {
        print(error.response.statusCode);
        print(error.response.data);
      }

      print('------------------------');
    }));
  }

  AuthenticationBloc authenticationBloc;
  void setAuthenticationBloc(AuthenticationBloc _authenticationBloc) {
    authenticationBloc = _authenticationBloc;
  }

  GlobalBloc globalBloc;
  void setGlobalBloc(GlobalBloc _globalBloc) {
    globalBloc = _globalBloc;
  }

  void add401Interceptor() {
    print("Adding 401 interceptor");
    status401Interceptor = InterceptorsWrapper(onError: (DioError res) {
      if (res.response != null && res.response.statusCode == 401) {
        globalBloc.add(ShowErrorSnackBar(
            error: 'You have been logged out, Please login again'));
        authenticationBloc.add(LogoutEvent());
        globalBloc.add(LougoutEvent());
      }
      return res;
    });
    dio.interceptors.add(status401Interceptor);
  }

  void remove401Interceptor() {
    dio.interceptors.remove(status401Interceptor);
  }
}
