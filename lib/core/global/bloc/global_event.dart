part of 'global_bloc.dart';

class GlobalEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ShowErrorSnackBar extends GlobalEvent {
  final String error;

  ShowErrorSnackBar({@required this.error});
}

class ShowSuccessSnackBar extends GlobalEvent {
  final String message;
  final SucceessEvents event;

  ShowSuccessSnackBar({@required this.message, @required this.event});
}

class ShowBetSlipSuccessDialog extends GlobalEvent {
  ShowBetSlipSuccessDialog();
}

class ShowLoadingWidget extends GlobalEvent {}

class HideLoadingWidget extends GlobalEvent {}

class LougoutEvent extends GlobalEvent {}
