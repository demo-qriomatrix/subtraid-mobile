part of 'global_bloc.dart';

class GlobalState extends Equatable {
  @override
  List<Object> get props => [];
}

class GlobalStateInitial extends GlobalState {}

class ErrorSnackBarState extends GlobalState {
  final String error;
  ErrorSnackBarState({@required this.error});

  @override
  // ignore: hash_and_equals
  bool operator ==(Object other) {
    return false;
  }
}

class SuccessSnackBarState extends GlobalState {
  final String message;
  final SucceessEvents event;

  SuccessSnackBarState({@required this.message, @required this.event});

  @override
  // ignore: hash_and_equals
  bool operator ==(Object other) {
    return false;
  }
}

class ShowBetSlipSuccessDialogState extends GlobalState {
  ShowBetSlipSuccessDialogState();

  @override
  // ignore: hash_and_equals
  bool operator ==(Object other) {
    return false;
  }
}

class ShowLoadingState extends GlobalState {}

class HideLoadingState extends GlobalState {}

class LogoutState extends GlobalState {}
