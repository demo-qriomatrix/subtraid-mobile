import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/http/http_client.dart';

part 'global_event.dart';
part 'global_state.dart';

class GlobalBloc extends Bloc<GlobalEvent, GlobalState> {
  final HttpClient httpClient;
  GlobalBloc({@required this.httpClient}) : super(GlobalStateInitial()) {
    httpClient.setGlobalBloc(this);
  }

  @override
  Stream<GlobalState> mapEventToState(GlobalEvent event) async* {
    if (event is ShowErrorSnackBar) {
      yield ErrorSnackBarState(error: event.error);
    }

    if (event is ShowSuccessSnackBar) {
      yield SuccessSnackBarState(message: event.message, event: event.event);
    }

    if (event is ShowBetSlipSuccessDialog) {
      yield ShowBetSlipSuccessDialogState();
    }

    if (event is ShowLoadingWidget) {
      yield ShowLoadingState();
    }
    if (event is HideLoadingWidget) {
      yield HideLoadingState();
    }
    if (event is LougoutEvent) {
      yield LogoutState();
    }
  }
}
