enum SucceessEvents {
  AddList,
  AddCard,
  AddProject,
  UpdateCard,
  UserRole,
  ProjectDelete,
  CheckItem,
  Label,
  AddCollaborator
}
