import 'package:Subtraid/features/authentication/models/access_token_model.dart';
import 'package:Subtraid/features/authentication/models/access_token_response_model.dart';
import 'package:Subtraid/features/authentication/models/credential_model.dart';
import 'package:Subtraid/features/authentication/models/experience_model.dart';
import 'package:Subtraid/features/authentication/models/license_model.dart';
import 'package:Subtraid/features/authentication/models/register_post_model.dart';
import 'package:Subtraid/features/authentication/models/skill_model.dart';
import 'package:Subtraid/features/authentication/models/token_payload_model.dart';
import 'package:Subtraid/features/authentication/models/update_password_post_model.dart';
import 'package:Subtraid/features/authentication/models/user_company_model.dart';
import 'package:Subtraid/features/authentication/models/user_location_model.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/authentication/models/user_response_model.dart';
import 'package:Subtraid/features/chat/models/chat_model.dart';
import 'package:Subtraid/features/chat/models/chats_model.dart';
import 'package:Subtraid/features/chat/models/message_model.dart';
import 'package:Subtraid/features/chat/models/seen_model.dart';
import 'package:Subtraid/features/contacts/models/add_employee_model.dart';
import 'package:Subtraid/features/contacts/models/connections_model.dart';
import 'package:Subtraid/features/contacts/models/contact_detail_model.dart';
import 'package:Subtraid/features/contacts/models/contact_location_model.dart';
import 'package:Subtraid/features/contacts/models/contacts_response_model.dart';
import 'package:Subtraid/features/contacts/models/contract_details_model.dart';
import 'package:Subtraid/features/contacts/models/favourite_post_model.dart';
import 'package:Subtraid/features/contacts/models/user_connections_model.dart';
import 'package:Subtraid/features/calender/models/calender_color_model.dart';
import 'package:Subtraid/features/calender/models/calender_event_model.dart';
import 'package:Subtraid/features/calender/models/calender_events_model.dart';
import 'package:Subtraid/features/calender/models/calender_meta_model.dart';
import 'package:Subtraid/features/more/company/features/companies/models/companies_reponse_model.dart';
import 'package:Subtraid/features/more/company/features/companies/models/company_public_model.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_member_model.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:Subtraid/features/more/company/features/pending/models/request_employee_model.dart';
import 'package:Subtraid/features/more/company/models/company_info_model.dart';
import 'package:Subtraid/features/more/company/models/company_model.dart';
import 'package:Subtraid/features/more/company/models/company_permission_model.dart';
import 'package:Subtraid/features/more/company/models/company_permissions_model.dart';
import 'package:Subtraid/features/more/company/models/company_response_model.dart';
import 'package:Subtraid/features/notifications/models/notification_count_model.dart';
import 'package:Subtraid/features/notifications/models/notification_ids_model.dart';
import 'package:Subtraid/features/notifications/models/notification_model.dart';
import 'package:Subtraid/features/notifications/models/notifications_response_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_success_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_success_response_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/location_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/settings_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_label_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_to_calender_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_comment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_comment_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_attachment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_comment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_employee_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_update_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/shift_card_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_label_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_status_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/add_collaborator_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_member_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_message_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_owner_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/requests_response_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/add_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/remove_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/current_observation_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/forecast_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/location_detail_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_astronomy_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_atmosphere_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_condition_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_response_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_wind_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/sub_project/models/add_sub_project_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/models/remove_member_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/model/create_project_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/model/custom_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_collaborators_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_label_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_list_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_member_id_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_response_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_sub_project_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_location_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_member_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/projects_list_response_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/company_info_response_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/card_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/list_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/member_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/project_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/sub_project_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permissions_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/ppg_post_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_company_info_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_company_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_group_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_role_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_update_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheets_filters_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheets_response.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/compony_timesheets_request_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/create_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_ops_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_update_model.dart';
import 'package:Subtraid/features/projects/features/templates/models/template_model.dart';
import 'package:Subtraid/features/projects/features/templates/models/template_request_model.dart';
import 'package:Subtraid/features/projects/features/templates/models/templates_response_model.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  AccessTokenModel,
  CredentialModel,
  AccessTokenResponseModel,
  ContactsResponseModel,
  ContactDetailsModel,
  ContactLocationModel,
  UserModel,
  TokenPayloadModel,
  RegisterPostModel,
  ProjectsListResponseModel,
  ProjectListItemModel,
  ProjectListItemMemberModel,
  ProjectListItemLocationModel,
  CreateProjectModel,
  CreateProjectSuccessResponseModel,
  CreateProjectSuccuessModel,
  SettingsModel,
  ProjectModel,
  ProjectResponseModel,
  ProjectCardModel,
  LocationModel,
  ProjectMemberIdModel,
  ProjectListModel,
  CardAttachmentModel,
  CardChecklistModel,
  CardTimesheetModel,
  CardCommentModel,
  CardChecklistItemModel,
  ProjectLabelModel,
  AddCardPostModel,
  ApiResponseModel,
  AddListPostModel,
  MemberModel,
  ProjectSubProjectModel,
  AddSubProjectPostModel,
  RemoveListPostModel,
  UpdateCardStaticModel,
  ShiftCardModel,
  UpdateCardStatusModel,
  CardAddCommentModel,
  CardAddCommentPostModel,
  UserResponseModel,
  CurrentObservationModel,
  ForecastModel,
  LocationDetailModel,
  WeatherAstronomyModel,
  WeatherAtmosphereModel,
  WeatherConditionModel,
  WeatherModel,
  WeatherResponseModel,
  WeatherWindModel,
  CompanyInfoResponseModel,
  CardPermissionModel,
  ListPermissionModel,
  MemberPermissionModel,
  ProjectPermissionModel,
  SubProjectPermissionModel,
  ProjectCompanyInfoModel,
  ProjectCompanyModel,
  PermissionsModel,
  ProjectPermissionGroupModel,
  UpdatePasswordPostModel,
  ChatModel,
  ChatsModel,
  MessageModel,
  SeenModel,
  PpgPostModel,
  RequestsResponseModel,
  RequestsModel,
  FavouritePostModel,
  RequestMessageModel,
  RequestMemberModel,
  RequestOwnerModel,
  CompanyInfoModel,
  CompanyModel,
  CompanyResponseModel,
  EmployeeModel,
  CompanyPermissionsModel,
  CompanyPermissionModel,
  CrewModel,
  CrewMemberModel,
  CalenderMetaModel,
  CalenderColorModel,
  CalenderEventModel,
  CalenderEventsModel,
  CompanyTimesheetsResponse,
  CompanyTimesheetModel,
  CompanyTimesheetsRequestModel,
  TimesheetOpsModel,
  TimesheetUpdateModel,
  CompanyTimesheetsFiltersModel,
  ConnectionsModel,
  UserConnectionsModel,
  AddEmployeeModel,
  ContractDetailsModel,
  RequestEmployeeModel,
  CardEmployeeModel,
  ProjectTimesheetModel,
  CreateTimesheetModel,
  CardAddChecklistModel,
  CardAddChecklistItemModel,
  CardUpdateChecklistItemModel,
  AddCardLabelModel,
  UpdateCardLabelModel,
  AddCardToCalenderModel,
  ProjectPermissionUpdateModel,
  ProjectPermissionRoleModel,
  RemoveMemberModel,
  SkillModel,
  UserCompanyModel,
  LicenseModel,
  ExperienceModel,
  UserLocationModel,
  NotificationCountModel,
  NotificationModel,
  NotificationsResponseModel,
  NotificationIds,
  TemplatesResponseModel,
  TemplateModel,
  TemplateRequestModel,
  ProjectCollaboratorsModel,
  CompanyPublicModel,
  CompaniesResponseModel,
  AddCollaboratorModel,
  CustomTimesheetModel,
  CreateProjectTimesheetModel
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
