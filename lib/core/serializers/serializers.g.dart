// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(AccessTokenModel.serializer)
      ..add(AccessTokenResponseModel.serializer)
      ..add(AddCardLabelModel.serializer)
      ..add(AddCardPostModel.serializer)
      ..add(AddCardToCalenderModel.serializer)
      ..add(AddCollaboratorModel.serializer)
      ..add(AddEmployeeModel.serializer)
      ..add(AddListPostModel.serializer)
      ..add(AddSubProjectPostModel.serializer)
      ..add(ApiResponseModel.serializer)
      ..add(CalenderColorModel.serializer)
      ..add(CalenderEventModel.serializer)
      ..add(CalenderEventsModel.serializer)
      ..add(CalenderMetaModel.serializer)
      ..add(CardAddChecklistItemModel.serializer)
      ..add(CardAddChecklistModel.serializer)
      ..add(CardAddCommentModel.serializer)
      ..add(CardAddCommentPostModel.serializer)
      ..add(CardAttachmentModel.serializer)
      ..add(CardChecklistItemModel.serializer)
      ..add(CardChecklistModel.serializer)
      ..add(CardCommentModel.serializer)
      ..add(CardEmployeeModel.serializer)
      ..add(CardPermissionModel.serializer)
      ..add(CardTimesheetModel.serializer)
      ..add(CardUpdateChecklistItemModel.serializer)
      ..add(ChatModel.serializer)
      ..add(ChatsModel.serializer)
      ..add(CompaniesResponseModel.serializer)
      ..add(CompanyInfoModel.serializer)
      ..add(CompanyInfoResponseModel.serializer)
      ..add(CompanyModel.serializer)
      ..add(CompanyPermissionModel.serializer)
      ..add(CompanyPermissionsModel.serializer)
      ..add(CompanyPublicModel.serializer)
      ..add(CompanyResponseModel.serializer)
      ..add(CompanyTimesheetModel.serializer)
      ..add(CompanyTimesheetsFiltersModel.serializer)
      ..add(CompanyTimesheetsRequestModel.serializer)
      ..add(CompanyTimesheetsResponse.serializer)
      ..add(ConnectionsModel.serializer)
      ..add(ContactDetailsModel.serializer)
      ..add(ContactLocationModel.serializer)
      ..add(ContactsResponseModel.serializer)
      ..add(ContractDetailsModel.serializer)
      ..add(CreateProjectModel.serializer)
      ..add(CreateProjectSuccessResponseModel.serializer)
      ..add(CreateProjectSuccuessModel.serializer)
      ..add(CreateProjectTimesheetModel.serializer)
      ..add(CreateTimesheetModel.serializer)
      ..add(CredentialModel.serializer)
      ..add(CrewMemberModel.serializer)
      ..add(CrewModel.serializer)
      ..add(CurrentObservationModel.serializer)
      ..add(CustomTimesheetModel.serializer)
      ..add(EmployeeModel.serializer)
      ..add(ExperienceModel.serializer)
      ..add(FavouritePostModel.serializer)
      ..add(ForecastModel.serializer)
      ..add(LicenseModel.serializer)
      ..add(ListPermissionModel.serializer)
      ..add(LocationDetailModel.serializer)
      ..add(LocationModel.serializer)
      ..add(MemberModel.serializer)
      ..add(MemberPermissionModel.serializer)
      ..add(MessageModel.serializer)
      ..add(NotificationCountModel.serializer)
      ..add(NotificationIds.serializer)
      ..add(NotificationModel.serializer)
      ..add(NotificationsResponseModel.serializer)
      ..add(PermissionsModel.serializer)
      ..add(PpgPostModel.serializer)
      ..add(ProjectCardModel.serializer)
      ..add(ProjectCollaboratorsModel.serializer)
      ..add(ProjectCompanyInfoModel.serializer)
      ..add(ProjectCompanyModel.serializer)
      ..add(ProjectLabelModel.serializer)
      ..add(ProjectListItemLocationModel.serializer)
      ..add(ProjectListItemMemberModel.serializer)
      ..add(ProjectListItemModel.serializer)
      ..add(ProjectListModel.serializer)
      ..add(ProjectMemberIdModel.serializer)
      ..add(ProjectModel.serializer)
      ..add(ProjectPermissionGroupModel.serializer)
      ..add(ProjectPermissionModel.serializer)
      ..add(ProjectPermissionRoleModel.serializer)
      ..add(ProjectPermissionUpdateModel.serializer)
      ..add(ProjectResponseModel.serializer)
      ..add(ProjectSubProjectModel.serializer)
      ..add(ProjectTimesheetModel.serializer)
      ..add(ProjectsListResponseModel.serializer)
      ..add(RegisterPostModel.serializer)
      ..add(RemoveListPostModel.serializer)
      ..add(RemoveMemberModel.serializer)
      ..add(RequestEmployeeModel.serializer)
      ..add(RequestMemberModel.serializer)
      ..add(RequestMessageModel.serializer)
      ..add(RequestOwnerModel.serializer)
      ..add(RequestsModel.serializer)
      ..add(RequestsResponseModel.serializer)
      ..add(SeenModel.serializer)
      ..add(SettingsModel.serializer)
      ..add(ShiftCardModel.serializer)
      ..add(SkillModel.serializer)
      ..add(SubProjectPermissionModel.serializer)
      ..add(TemplateModel.serializer)
      ..add(TemplateRequestModel.serializer)
      ..add(TemplatesResponseModel.serializer)
      ..add(TimesheetOpsModel.serializer)
      ..add(TimesheetUpdateModel.serializer)
      ..add(TokenPayloadModel.serializer)
      ..add(UpdateCardLabelModel.serializer)
      ..add(UpdateCardStaticModel.serializer)
      ..add(UpdateCardStatusModel.serializer)
      ..add(UpdatePasswordPostModel.serializer)
      ..add(UserCompanyModel.serializer)
      ..add(UserConnectionsModel.serializer)
      ..add(UserLocationModel.serializer)
      ..add(UserModel.serializer)
      ..add(UserResponseModel.serializer)
      ..add(WeatherAstronomyModel.serializer)
      ..add(WeatherAtmosphereModel.serializer)
      ..add(WeatherConditionModel.serializer)
      ..add(WeatherModel.serializer)
      ..add(WeatherResponseModel.serializer)
      ..add(WeatherWindModel.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CalenderEventModel)]),
          () => new ListBuilder<CalenderEventModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(CardChecklistItemModel)]),
          () => new ListBuilder<CardChecklistItemModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CardTimesheetModel)]),
          () => new ListBuilder<CardTimesheetModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(UserModel)]),
          () => new ListBuilder<UserModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(CardAttachmentModel)]),
          () => new ListBuilder<CardAttachmentModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CardChecklistModel)]),
          () => new ListBuilder<CardChecklistModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CardCommentModel)]),
          () => new ListBuilder<CardCommentModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ChatModel)]),
          () => new ListBuilder<ChatModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CompanyPublicModel)]),
          () => new ListBuilder<CompanyPublicModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(CompanyTimesheetModel)]),
          () => new ListBuilder<CompanyTimesheetModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ContactDetailsModel)]),
          () => new ListBuilder<ContactDetailsModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ContactDetailsModel)]),
          () => new ListBuilder<ContactDetailsModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EmployeeModel)]),
          () => new ListBuilder<EmployeeModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EmployeeModel)]),
          () => new ListBuilder<EmployeeModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CrewModel)]),
          () => new ListBuilder<CrewModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ForecastModel)]),
          () => new ListBuilder<ForecastModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(MemberModel)]),
          () => new ListBuilder<MemberModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(MessageModel)]),
          () => new ListBuilder<MessageModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SeenModel)]),
          () => new ListBuilder<SeenModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(NotificationModel)]),
          () => new ListBuilder<NotificationModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectListItemMemberModel)]),
          () => new ListBuilder<ProjectListItemMemberModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectListItemModel)]),
          () => new ListBuilder<ProjectListItemModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectListItemModel)]),
          () => new ListBuilder<ProjectListItemModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(EmployeeModel)]),
          () => new ListBuilder<EmployeeModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CrewModel)]),
          () => new ListBuilder<CrewModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectPermissionGroupModel)]),
          () => new ListBuilder<ProjectPermissionGroupModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectSubProjectModel)]),
          () => new ListBuilder<ProjectSubProjectModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectCollaboratorsModel)]),
          () => new ListBuilder<ProjectCollaboratorsModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectTimesheetModel)]),
          () => new ListBuilder<ProjectTimesheetModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectMemberIdModel)]),
          () => new ListBuilder<ProjectMemberIdModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ProjectLabelModel)]),
          () => new ListBuilder<ProjectLabelModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ProjectListModel)]),
          () => new ListBuilder<ProjectListModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ProjectCardModel)]),
          () => new ListBuilder<ProjectCardModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(ProjectPermissionRoleModel)]),
          () => new ListBuilder<ProjectPermissionRoleModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(RequestsModel)]),
          () => new ListBuilder<RequestsModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SkillModel)]),
          () => new ListBuilder<SkillModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(LicenseModel)]),
          () => new ListBuilder<LicenseModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ExperienceModel)]),
          () => new ListBuilder<ExperienceModel>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TemplateModel)]),
          () => new ListBuilder<TemplateModel>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(TemplateRequestModel)]),
          () => new ListBuilder<TemplateRequestModel>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
