import 'package:bloc/bloc.dart';

List<String> skipLog = [
  'RacingListMeetingPusherUpdate',
  'RaceCardPusherUpdate',
];

class SimpleBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);

    if (!(skipLog.contains(event.toString()))) {
      // print(Trace.from(StackTrace.current).terse.frames[0]);
      print('onEvent $event');
    }
  }

  @override
  onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    if (!(skipLog.contains(transition.event.toString())))
      print('onTransition $transition');
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    super.onError(cubit, error, stackTrace);
    print('onError $error');
  }
}
