extension StringExtension on String {
  String get capitalize =>
      "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
  String get inCaps => '${this[0].toUpperCase()}${this.substring(1)}';
  String get allInCaps => this.toUpperCase();

  String get capitalizeFirstofEach => this
      .split(" ")
      .where((element) => element.isNotEmpty)
      .map((str) => str.inCaps)
      .join(" ");
}
