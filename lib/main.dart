import 'package:Subtraid/core/config/color_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:Subtraid/core/bloc_observer/simple_bloc_observer.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';

import 'core/di/injection_container.dart';
import 'core/global/bloc/global_bloc.dart';
import 'core/helpers/get_material_color_from_color.dart';
import 'features/splash/view/splash_screen.dart';

void main() async {
  // Initializing Dependency Injector
  await initDependencyInjector();

  HttpClient(dio: sl());

  // Initializing Bloc Observer
  Bloc.observer = SimpleBlocObserver();

  initializeDateFormatting('en_US', null);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<GlobalBloc>(
            create: (context) => GlobalBloc(httpClient: sl()),
            lazy: false,
          ),
          BlocProvider<AuthenticationBloc>(
            create: (context) => AuthenticationBloc(
                authenticationRepository: sl(),
                globalBloc: context.read<GlobalBloc>()),
            lazy: false,
          ),
        ],
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: MaterialApp(
            builder: (context, child) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                child: child,
              );
            },
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primarySwatch: getMaterialColorFromColor(ColorConfig.primary),
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: SplashScreen(),
          ),
        ));
  }
}
