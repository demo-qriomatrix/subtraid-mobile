// GENERATED CODE - DO NOT MODIFY BY HAND

part of calender_events_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CalenderEventsModel> _$calenderEventsModelSerializer =
    new _$CalenderEventsModelSerializer();

class _$CalenderEventsModelSerializer
    implements StructuredSerializer<CalenderEventsModel> {
  @override
  final Iterable<Type> types = const [
    CalenderEventsModel,
    _$CalenderEventsModel
  ];
  @override
  final String wireName = 'CalenderEventsModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CalenderEventsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.events != null) {
      result
        ..add('events')
        ..add(serializers.serialize(object.events,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CalenderEventModel)])));
    }
    return result;
  }

  @override
  CalenderEventsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CalenderEventsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'events':
          result.events.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CalenderEventModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$CalenderEventsModel extends CalenderEventsModel {
  @override
  final BuiltList<CalenderEventModel> events;

  factory _$CalenderEventsModel(
          [void Function(CalenderEventsModelBuilder) updates]) =>
      (new CalenderEventsModelBuilder()..update(updates)).build();

  _$CalenderEventsModel._({this.events}) : super._();

  @override
  CalenderEventsModel rebuild(
          void Function(CalenderEventsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CalenderEventsModelBuilder toBuilder() =>
      new CalenderEventsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CalenderEventsModel && events == other.events;
  }

  @override
  int get hashCode {
    return $jf($jc(0, events.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CalenderEventsModel')
          ..add('events', events))
        .toString();
  }
}

class CalenderEventsModelBuilder
    implements Builder<CalenderEventsModel, CalenderEventsModelBuilder> {
  _$CalenderEventsModel _$v;

  ListBuilder<CalenderEventModel> _events;
  ListBuilder<CalenderEventModel> get events =>
      _$this._events ??= new ListBuilder<CalenderEventModel>();
  set events(ListBuilder<CalenderEventModel> events) => _$this._events = events;

  CalenderEventsModelBuilder();

  CalenderEventsModelBuilder get _$this {
    if (_$v != null) {
      _events = _$v.events?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CalenderEventsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CalenderEventsModel;
  }

  @override
  void update(void Function(CalenderEventsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CalenderEventsModel build() {
    _$CalenderEventsModel _$result;
    try {
      _$result = _$v ?? new _$CalenderEventsModel._(events: _events?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'events';
        _events?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CalenderEventsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
