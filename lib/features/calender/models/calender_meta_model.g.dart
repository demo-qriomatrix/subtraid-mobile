// GENERATED CODE - DO NOT MODIFY BY HAND

part of calender_meta_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CalenderMetaModel> _$calenderMetaModelSerializer =
    new _$CalenderMetaModelSerializer();

class _$CalenderMetaModelSerializer
    implements StructuredSerializer<CalenderMetaModel> {
  @override
  final Iterable<Type> types = const [CalenderMetaModel, _$CalenderMetaModel];
  @override
  final String wireName = 'CalenderMetaModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CalenderMetaModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(String)));
    }
    if (object.notes != null) {
      result
        ..add('notes')
        ..add(serializers.serialize(object.notes,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CalenderMetaModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CalenderMetaModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'location':
          result.location = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'notes':
          result.notes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CalenderMetaModel extends CalenderMetaModel {
  @override
  final String location;
  @override
  final String notes;

  factory _$CalenderMetaModel(
          [void Function(CalenderMetaModelBuilder) updates]) =>
      (new CalenderMetaModelBuilder()..update(updates)).build();

  _$CalenderMetaModel._({this.location, this.notes}) : super._();

  @override
  CalenderMetaModel rebuild(void Function(CalenderMetaModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CalenderMetaModelBuilder toBuilder() =>
      new CalenderMetaModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CalenderMetaModel &&
        location == other.location &&
        notes == other.notes;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, location.hashCode), notes.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CalenderMetaModel')
          ..add('location', location)
          ..add('notes', notes))
        .toString();
  }
}

class CalenderMetaModelBuilder
    implements Builder<CalenderMetaModel, CalenderMetaModelBuilder> {
  _$CalenderMetaModel _$v;

  String _location;
  String get location => _$this._location;
  set location(String location) => _$this._location = location;

  String _notes;
  String get notes => _$this._notes;
  set notes(String notes) => _$this._notes = notes;

  CalenderMetaModelBuilder();

  CalenderMetaModelBuilder get _$this {
    if (_$v != null) {
      _location = _$v.location;
      _notes = _$v.notes;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CalenderMetaModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CalenderMetaModel;
  }

  @override
  void update(void Function(CalenderMetaModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CalenderMetaModel build() {
    final _$result =
        _$v ?? new _$CalenderMetaModel._(location: location, notes: notes);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
