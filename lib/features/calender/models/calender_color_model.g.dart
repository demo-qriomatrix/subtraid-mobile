// GENERATED CODE - DO NOT MODIFY BY HAND

part of calender_color_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CalenderColorModel> _$calenderColorModelSerializer =
    new _$CalenderColorModelSerializer();

class _$CalenderColorModelSerializer
    implements StructuredSerializer<CalenderColorModel> {
  @override
  final Iterable<Type> types = const [CalenderColorModel, _$CalenderColorModel];
  @override
  final String wireName = 'CalenderColorModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CalenderColorModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.primary != null) {
      result
        ..add('primary')
        ..add(serializers.serialize(object.primary,
            specifiedType: const FullType(String)));
    }
    if (object.secondary != null) {
      result
        ..add('secondary')
        ..add(serializers.serialize(object.secondary,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CalenderColorModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CalenderColorModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'primary':
          result.primary = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'secondary':
          result.secondary = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CalenderColorModel extends CalenderColorModel {
  @override
  final String primary;
  @override
  final String secondary;

  factory _$CalenderColorModel(
          [void Function(CalenderColorModelBuilder) updates]) =>
      (new CalenderColorModelBuilder()..update(updates)).build();

  _$CalenderColorModel._({this.primary, this.secondary}) : super._();

  @override
  CalenderColorModel rebuild(
          void Function(CalenderColorModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CalenderColorModelBuilder toBuilder() =>
      new CalenderColorModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CalenderColorModel &&
        primary == other.primary &&
        secondary == other.secondary;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, primary.hashCode), secondary.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CalenderColorModel')
          ..add('primary', primary)
          ..add('secondary', secondary))
        .toString();
  }
}

class CalenderColorModelBuilder
    implements Builder<CalenderColorModel, CalenderColorModelBuilder> {
  _$CalenderColorModel _$v;

  String _primary;
  String get primary => _$this._primary;
  set primary(String primary) => _$this._primary = primary;

  String _secondary;
  String get secondary => _$this._secondary;
  set secondary(String secondary) => _$this._secondary = secondary;

  CalenderColorModelBuilder();

  CalenderColorModelBuilder get _$this {
    if (_$v != null) {
      _primary = _$v.primary;
      _secondary = _$v.secondary;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CalenderColorModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CalenderColorModel;
  }

  @override
  void update(void Function(CalenderColorModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CalenderColorModel build() {
    final _$result = _$v ??
        new _$CalenderColorModel._(primary: primary, secondary: secondary);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
