// GENERATED CODE - DO NOT MODIFY BY HAND

part of calender_event_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CalenderEventModel> _$calenderEventModelSerializer =
    new _$CalenderEventModelSerializer();

class _$CalenderEventModelSerializer
    implements StructuredSerializer<CalenderEventModel> {
  @override
  final Iterable<Type> types = const [CalenderEventModel, _$CalenderEventModel];
  @override
  final String wireName = 'CalenderEventModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CalenderEventModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.color != null) {
      result
        ..add('color')
        ..add(serializers.serialize(object.color,
            specifiedType: const FullType(CalenderColorModel)));
    }
    if (object.meta != null) {
      result
        ..add('meta')
        ..add(serializers.serialize(object.meta,
            specifiedType: const FullType(CalenderMetaModel)));
    }
    if (object.members != null) {
      result
        ..add('members')
        ..add(serializers.serialize(object.members,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.creator != null) {
      result
        ..add('creator')
        ..add(serializers.serialize(object.creator,
            specifiedType: const FullType(String)));
    }
    if (object.companyId != null) {
      result
        ..add('companyId')
        ..add(serializers.serialize(object.companyId,
            specifiedType: const FullType(String)));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.startTime != null) {
      result
        ..add('startTime')
        ..add(serializers.serialize(object.startTime,
            specifiedType: const FullType(String)));
    }
    if (object.endTime != null) {
      result
        ..add('endTime')
        ..add(serializers.serialize(object.endTime,
            specifiedType: const FullType(String)));
    }
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.allDay != null) {
      result
        ..add('allDay')
        ..add(serializers.serialize(object.allDay,
            specifiedType: const FullType(bool)));
    }
    if (object.title != null) {
      result
        ..add('title')
        ..add(serializers.serialize(object.title,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CalenderEventModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CalenderEventModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'color':
          result.color.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CalenderColorModel))
              as CalenderColorModel);
          break;
        case 'meta':
          result.meta.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CalenderMetaModel))
              as CalenderMetaModel);
          break;
        case 'members':
          result.members.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'creator':
          result.creator = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'companyId':
          result.companyId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'startTime':
          result.startTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'endTime':
          result.endTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'allDay':
          result.allDay = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CalenderEventModel extends CalenderEventModel {
  @override
  final CalenderColorModel color;
  @override
  final CalenderMetaModel meta;
  @override
  final BuiltList<String> members;
  @override
  final String id;
  @override
  final String creator;
  @override
  final String companyId;
  @override
  final String start;
  @override
  final String startTime;
  @override
  final String endTime;
  @override
  final String end;
  @override
  final String dateCreated;
  @override
  final bool allDay;
  @override
  final String title;

  factory _$CalenderEventModel(
          [void Function(CalenderEventModelBuilder) updates]) =>
      (new CalenderEventModelBuilder()..update(updates)).build();

  _$CalenderEventModel._(
      {this.color,
      this.meta,
      this.members,
      this.id,
      this.creator,
      this.companyId,
      this.start,
      this.startTime,
      this.endTime,
      this.end,
      this.dateCreated,
      this.allDay,
      this.title})
      : super._();

  @override
  CalenderEventModel rebuild(
          void Function(CalenderEventModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CalenderEventModelBuilder toBuilder() =>
      new CalenderEventModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CalenderEventModel &&
        color == other.color &&
        meta == other.meta &&
        members == other.members &&
        id == other.id &&
        creator == other.creator &&
        companyId == other.companyId &&
        start == other.start &&
        startTime == other.startTime &&
        endTime == other.endTime &&
        end == other.end &&
        dateCreated == other.dateCreated &&
        allDay == other.allDay &&
        title == other.title;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc($jc(0, color.hashCode),
                                                    meta.hashCode),
                                                members.hashCode),
                                            id.hashCode),
                                        creator.hashCode),
                                    companyId.hashCode),
                                start.hashCode),
                            startTime.hashCode),
                        endTime.hashCode),
                    end.hashCode),
                dateCreated.hashCode),
            allDay.hashCode),
        title.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CalenderEventModel')
          ..add('color', color)
          ..add('meta', meta)
          ..add('members', members)
          ..add('id', id)
          ..add('creator', creator)
          ..add('companyId', companyId)
          ..add('start', start)
          ..add('startTime', startTime)
          ..add('endTime', endTime)
          ..add('end', end)
          ..add('dateCreated', dateCreated)
          ..add('allDay', allDay)
          ..add('title', title))
        .toString();
  }
}

class CalenderEventModelBuilder
    implements Builder<CalenderEventModel, CalenderEventModelBuilder> {
  _$CalenderEventModel _$v;

  CalenderColorModelBuilder _color;
  CalenderColorModelBuilder get color =>
      _$this._color ??= new CalenderColorModelBuilder();
  set color(CalenderColorModelBuilder color) => _$this._color = color;

  CalenderMetaModelBuilder _meta;
  CalenderMetaModelBuilder get meta =>
      _$this._meta ??= new CalenderMetaModelBuilder();
  set meta(CalenderMetaModelBuilder meta) => _$this._meta = meta;

  ListBuilder<String> _members;
  ListBuilder<String> get members =>
      _$this._members ??= new ListBuilder<String>();
  set members(ListBuilder<String> members) => _$this._members = members;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _creator;
  String get creator => _$this._creator;
  set creator(String creator) => _$this._creator = creator;

  String _companyId;
  String get companyId => _$this._companyId;
  set companyId(String companyId) => _$this._companyId = companyId;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _startTime;
  String get startTime => _$this._startTime;
  set startTime(String startTime) => _$this._startTime = startTime;

  String _endTime;
  String get endTime => _$this._endTime;
  set endTime(String endTime) => _$this._endTime = endTime;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  bool _allDay;
  bool get allDay => _$this._allDay;
  set allDay(bool allDay) => _$this._allDay = allDay;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  CalenderEventModelBuilder();

  CalenderEventModelBuilder get _$this {
    if (_$v != null) {
      _color = _$v.color?.toBuilder();
      _meta = _$v.meta?.toBuilder();
      _members = _$v.members?.toBuilder();
      _id = _$v.id;
      _creator = _$v.creator;
      _companyId = _$v.companyId;
      _start = _$v.start;
      _startTime = _$v.startTime;
      _endTime = _$v.endTime;
      _end = _$v.end;
      _dateCreated = _$v.dateCreated;
      _allDay = _$v.allDay;
      _title = _$v.title;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CalenderEventModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CalenderEventModel;
  }

  @override
  void update(void Function(CalenderEventModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CalenderEventModel build() {
    _$CalenderEventModel _$result;
    try {
      _$result = _$v ??
          new _$CalenderEventModel._(
              color: _color?.build(),
              meta: _meta?.build(),
              members: _members?.build(),
              id: id,
              creator: creator,
              companyId: companyId,
              start: start,
              startTime: startTime,
              endTime: endTime,
              end: end,
              dateCreated: dateCreated,
              allDay: allDay,
              title: title);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'color';
        _color?.build();
        _$failedField = 'meta';
        _meta?.build();
        _$failedField = 'members';
        _members?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CalenderEventModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
