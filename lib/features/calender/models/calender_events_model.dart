library calender_events_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'calender_event_model.dart';

part 'calender_events_model.g.dart';

abstract class CalenderEventsModel
    implements Built<CalenderEventsModel, CalenderEventsModelBuilder> {
  @nullable
  BuiltList<CalenderEventModel> get events;

  CalenderEventsModel._();

  factory CalenderEventsModel([updates(CalenderEventsModelBuilder b)]) =
      _$CalenderEventsModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CalenderEventsModel.serializer, this));
  }

  static CalenderEventsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CalenderEventsModel.serializer, json.decode(jsonString));
  }

  static Serializer<CalenderEventsModel> get serializer =>
      _$calenderEventsModelSerializer;
}
