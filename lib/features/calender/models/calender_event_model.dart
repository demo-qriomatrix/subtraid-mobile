library calender_event_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'calender_color_model.dart';
import 'calender_meta_model.dart';

part 'calender_event_model.g.dart';

abstract class CalenderEventModel
    implements Built<CalenderEventModel, CalenderEventModelBuilder> {
  @nullable
  CalenderColorModel get color;

  @nullable
  CalenderMetaModel get meta;

  @nullable
  BuiltList<String> get members;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get creator;

  @nullable
  String get companyId;

  @nullable
  String get start;
  @nullable
  String get startTime;

  @nullable
  String get endTime;

  @nullable
  String get end;

  @nullable
  String get dateCreated;

  @nullable
  bool get allDay;

  @nullable
  String get title;

  CalenderEventModel._();

  factory CalenderEventModel([updates(CalenderEventModelBuilder b)]) =
      _$CalenderEventModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CalenderEventModel.serializer, this));
  }

  static CalenderEventModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CalenderEventModel.serializer, json.decode(jsonString));
  }

  static Serializer<CalenderEventModel> get serializer =>
      _$calenderEventModelSerializer;
}
