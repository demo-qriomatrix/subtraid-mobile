library calender_meta_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'calender_meta_model.g.dart';

abstract class CalenderMetaModel
    implements Built<CalenderMetaModel, CalenderMetaModelBuilder> {
  @nullable
  String get location;

  @nullable
  String get notes;

  CalenderMetaModel._();

  factory CalenderMetaModel([updates(CalenderMetaModelBuilder b)]) =
      _$CalenderMetaModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CalenderMetaModel.serializer, this));
  }

  static CalenderMetaModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CalenderMetaModel.serializer, json.decode(jsonString));
  }

  static Serializer<CalenderMetaModel> get serializer =>
      _$calenderMetaModelSerializer;
}
