library calender_color_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'calender_color_model.g.dart';

abstract class CalenderColorModel
    implements Built<CalenderColorModel, CalenderColorModelBuilder> {
  @nullable
  String get primary;

  @nullable
  String get secondary;

  CalenderColorModel._();

  factory CalenderColorModel([updates(CalenderColorModelBuilder b)]) =
      _$CalenderColorModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CalenderColorModel.serializer, this));
  }

  static CalenderColorModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CalenderColorModel.serializer, json.decode(jsonString));
  }

  static Serializer<CalenderColorModel> get serializer =>
      _$calenderColorModelSerializer;
}
