import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/calender/models/calender_event_model.dart';
import 'package:Subtraid/features/calender/models/calender_events_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_to_calender_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class CalenderRepository {
  final HttpClient httpClient;

  CalenderRepository({@required this.httpClient});

  Future<CalenderEventsModel> getCalenderEvents() async {
    Response response = await httpClient.dio.get(EndpointConfig.userEvents);

    return CalenderEventsModel.fromJson(json.encode(response.data));
  }

  Future<CalenderEventsModel> addCalenderEvent(
      CalenderEventModel calenderEventModel) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.events, data: calenderEventModel.toJson());

    return CalenderEventsModel.fromJson(json.encode(response.data));
  }

  Future<CalenderEventsModel> addCardToCalender(
      AddCardToCalenderModel calenderEventModel) async {
    Response response = await httpClient.dio.post(
        EndpointConfig.events + '/add-project-task',
        data: calenderEventModel.toJson());

    return CalenderEventsModel.fromJson(json.encode(response.data));
  }

  Future<CalenderEventsModel> updateCalenderEvent(
      CalenderEventModel calenderEventModel) async {
    print(calenderEventModel.toJson());
    Response response = await httpClient.dio
        .patch(EndpointConfig.events, data: calenderEventModel.toJson());

    return CalenderEventsModel.fromJson(json.encode(response.data));
  }

  Future<Response> deleteEvent(CalenderEventModel calenderEventModel) async {
    Response response = await httpClient.dio.delete(
      EndpointConfig.events + '/' + calenderEventModel.id,
    );

    return response;
  }
}
