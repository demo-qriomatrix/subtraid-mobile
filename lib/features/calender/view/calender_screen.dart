import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/calender/bloc/calender/calender_bloc.dart';
import 'package:Subtraid/features/calender/models/calender_event_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import 'add_event_screen.dart';

class CalenderScreen extends StatefulWidget {
  @override
  _CalenderScreenState createState() => _CalenderScreenState();
}

class _CalenderScreenState extends State<CalenderScreen>
    with TickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  // List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;
  // DateTime _clickedDay;

  TextEditingController titleController = TextEditingController();
  TextEditingController statusController = TextEditingController();
  TextEditingController primaryColor = TextEditingController();
  TextEditingController secondaryColor = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController startTime = TextEditingController();
  TextEditingController endTime = TextEditingController();
  TextEditingController location = TextEditingController();
  TextEditingController note = TextEditingController();

  bool monVal = false;
  // int _radioValue = 0;
  // double _result = 0.0;
  bool hasAEvent = false;

  DateTime _clickedDay =
      DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now()));

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    // _selectedEvents = _events[_selectedDay] ?? [];
    _calendarController = CalendarController();

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');

    setState(() {
      _clickedDay = DateTime.parse(DateFormat('yyyy-MM-dd').format(day));
      // _selectedEvents = events;
    });

    print(day.toUtc().toString().split(" ")[0]);
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }

  void _onCalendarCreated(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onCalendarCreated');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldState,
        backgroundColor: ColorConfig.primary,
        appBar: StAppBar(
          title: 'Calendar',
        ),
        body: BlocProvider<CalenderBloc>(
          create: (context) => CalenderBloc(
              calenderRepository: sl(), globalBloc: context.read<GlobalBloc>()),
          child: BlocBuilder<CalenderBloc, CalenderState>(
            builder: (context, state) => Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 24),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: ConstrainedBox(
                    constraints:
                        BoxConstraints(minHeight: getMinHeight(context)),
                    child: BlocBuilder<CalenderBloc, CalenderState>(
                      builder: (context, state) {
                        if (state is CalenderLoadSuccess) {
                          return ListView(
                            padding: EdgeInsets.symmetric(horizontal: 30),
                            children: <Widget>[
                              SizedBox(
                                height: 10,
                              ),
                              // _buildTableCalendar(),
                              _buildTableCalendarWithBuilders(state.events),
                              const SizedBox(height: 10.0),

                              _buildEventList(state.events, context),
                              SizedBox(
                                height: 40,
                              ),
                            ],
                          );
                        }

                        if (state is CalenderLoadInProgress) {
                          return SizedBox(
                              height: getMinHeight(context),
                              child:
                                  Center(child: CircularProgressIndicator()));
                        }

                        return Container();
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RawMaterialButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: CircleBorder(),
                        constraints: BoxConstraints(),
                        fillColor: ColorConfig.orange,
                        onPressed: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(
                            builder: (context) => AddEventScreen(
                              event: null,
                            ),
                          ))
                              .then((value) {
                            context
                                .read<CalenderBloc>()
                                .add(CalenderEventsRequested());
                          });

                          // _editView();
                        },
                        padding: EdgeInsets.all(8),
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 24,
                        ),
                      ),
                      RawMaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        constraints: BoxConstraints(),
                        fillColor: Colors.white,
                        onPressed: () {
                          _filterView();
                        },
                        padding: EdgeInsets.all(6),
                        child: ImageIcon(
                          AssetImage(LocalImages.filter),
                          color: ColorConfig.primary,
                          size: 14,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  // _editView() {
  //   List<String> selectedMember = [];
  //   print(_clickedDay);
  //   showDialog(
  //       context: context,
  //       builder: (context) {
  //         return StatefulBuilder(
  //           builder: (context, setState) {
  //             return Dialog(
  //               shape: RoundedRectangleBorder(
  //                   borderRadius: BorderRadius.circular(20)),
  //               backgroundColor: Colors.white,
  //               child: Container(
  //                 padding: EdgeInsets.all(20),
  //                 child: ListView(
  //                   children: <Widget>[
  //                     Row(
  //                       children: <Widget>[
  //                         CircleAvatar(
  //                           backgroundColor: ColorConfig.orange,
  //                           child: Icon(
  //                             Icons.add,
  //                             color: Colors.white,
  //                           ),
  //                         ),
  //                         SizedBox(
  //                           width: 20,
  //                         ),
  //                         Text(
  //                           'Add Event',
  //                           style: TextStyle(
  //                               color: ColorConfig.primary,
  //                               fontSize: 14,
  //                               fontWeight: FontWeight.w500),
  //                         ),
  //                         Spacer(),
  //                         IconButton(
  //                           icon: Icon(Icons.close),
  //                           color: ColorConfig.primary,
  //                           onPressed: () {
  //                             Navigator.of(context).pop();
  //                           },
  //                         )
  //                       ],
  //                     ),
  //                     SizedBox(
  //                       height: 20,
  //                     ),
  //                     TextField(
  //                       controller: titleController,
  //                       decoration: InputDecoration(
  //                         fillColor: ColorConfig.blue1,
  //                         filled: true,
  //                         hintText: 'Title',
  //                         hintStyle: TextStyle(
  //                             color: ColorConfig.black1,
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.w400),
  //                         contentPadding: EdgeInsets.symmetric(
  //                             horizontal: 10, vertical: 10),
  //                         border: OutlineInputBorder(
  //                             borderRadius: BorderRadius.circular(10),
  //                             borderSide: BorderSide.none),
  //                       ),
  //                       maxLines: null,
  //                       style: TextStyle(
  //                           fontWeight: FontWeight.w500,
  //                           color: Colors.black.withOpacity(0.74),
  //                           fontSize: 14),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     TextField(
  //                       controller: statusController,
  //                       decoration: InputDecoration(
  //                         fillColor: ColorConfig.blue1,
  //                         filled: true,
  //                         hintText: 'Status',
  //                         hintStyle: TextStyle(
  //                             color: ColorConfig.black1,
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.w400),
  //                         contentPadding: EdgeInsets.symmetric(
  //                             horizontal: 10, vertical: 10),
  //                         border: OutlineInputBorder(
  //                             borderRadius: BorderRadius.circular(10),
  //                             borderSide: BorderSide.none),
  //                       ),
  //                       maxLines: null,
  //                       style: TextStyle(
  //                           fontWeight: FontWeight.w500,
  //                           color: Colors.black.withOpacity(0.74),
  //                           fontSize: 14),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                       children: <Widget>[
  //                         Flexible(
  //                           child: Row(
  //                             children: [
  //                               Container(
  //                                 color: Colors.red,
  //                                 height: 20,
  //                               )
  //                             ],
  //                           ),
  //                         ),
  //                         SizedBox(
  //                           width: 10,
  //                         ),
  //                         Flexible(
  //                           child: TextField(
  //                             controller: secondaryColor,
  //                             decoration: InputDecoration(
  //                               fillColor: ColorConfig.blue1,
  //                               filled: true,
  //                               hintText: 'Secondary Color',
  //                               hintStyle: TextStyle(
  //                                   color: ColorConfig.black1,
  //                                   fontSize: 14,
  //                                   fontWeight: FontWeight.w400),
  //                               contentPadding: EdgeInsets.symmetric(
  //                                   horizontal: 10, vertical: 10),
  //                               border: OutlineInputBorder(
  //                                   borderRadius: BorderRadius.circular(10),
  //                                   borderSide: BorderSide.none),
  //                             ),
  //                             maxLines: null,
  //                             style: TextStyle(
  //                                 fontWeight: FontWeight.w500,
  //                                 color: Colors.black.withOpacity(0.74),
  //                                 fontSize: 14),
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                     SizedBox(
  //                       height: 20,
  //                     ),
  //                     Text(
  //                       'Members',
  //                       style: TextStyle(
  //                           color: Colors.black,
  //                           fontSize: 13,
  //                           fontWeight: FontWeight.w500),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     selectedMember.length == 0
  //                         ? Container(
  //                             height: 50,
  //                             decoration: BoxDecoration(
  //                                 color: ColorConfig.grey10,
  //                                 borderRadius: BorderRadius.circular(5)),
  //                             padding: EdgeInsets.symmetric(
  //                                 horizontal: 10, vertical: 5),
  //                             child: Row(
  //                               mainAxisAlignment:
  //                                   MainAxisAlignment.spaceBetween,
  //                               children: [
  //                                 Text('Select members'),
  //                                 Icon(
  //                                   Icons.add,
  //                                   color: ColorConfig.primary,
  //                                 )
  //                               ],
  //                             ),
  //                           )
  //                         : Container(
  //                             height: 50,
  //                             decoration: BoxDecoration(
  //                                 color: ColorConfig.grey10,
  //                                 borderRadius: BorderRadius.circular(5)),
  //                             padding: EdgeInsets.symmetric(
  //                                 horizontal: 10, vertical: 5),
  //                             child: Row(
  //                               mainAxisAlignment:
  //                                   MainAxisAlignment.spaceBetween,
  //                               children: [
  //                                 Expanded(
  //                                   child: ListView(
  //                                     scrollDirection: Axis.horizontal,
  //                                     children: List.generate(
  //                                         selectedMember.length,
  //                                         (index) => Padding(
  //                                               padding:
  //                                                   const EdgeInsets.symmetric(
  //                                                       horizontal: 5),
  //                                               child: InkWell(
  //                                                 onTap: () {
  //                                                   print(index);
  //                                                   setState(() {
  //                                                     selectedMember
  //                                                         .removeAt(index);
  //                                                   });
  //                                                 },
  //                                                 child: CircleAvatar(
  //                                                   backgroundImage: NetworkImage(
  //                                                       "https://diievents.dii.eu/brexit-pro-diirect/wp-content/uploads/sites/52/2015/04/speaker-3-v2.jpg"),
  //                                                   radius: 15,
  //                                                 ),
  //                                               ),
  //                                             )),
  //                                   ),
  //                                 ),
  //                                 Icon(
  //                                   Icons.add,
  //                                   color: ColorConfig.primary,
  //                                 )
  //                               ],
  //                             ),
  //                           ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Padding(
  //                       padding: const EdgeInsets.symmetric(horizontal: 5),
  //                       child: Container(
  //                         decoration: BoxDecoration(
  //                             borderRadius: BorderRadius.circular(5),
  //                             boxShadow: [
  //                               BoxShadow(
  //                                   blurRadius: 3,
  //                                   color: Colors.grey.shade300,
  //                                   offset: Offset(2, 2),
  //                                   spreadRadius: 3)
  //                             ],
  //                             color: Colors.white),
  //                         child: ClipRRect(
  //                           borderRadius: BorderRadius.circular(5),
  //                           child: Container(
  //                             height: 150,
  //                             child: ListView(
  //                               children: List.generate(
  //                                 10,
  //                                 (index) => Column(
  //                                   crossAxisAlignment:
  //                                       CrossAxisAlignment.start,
  //                                   children: <Widget>[
  //                                     Material(
  //                                       child: InkWell(
  //                                         onTap: () {
  //                                           setState(() {
  //                                             selectedMember.add('John Doe');
  //                                           });
  //                                         },
  //                                         child: Container(
  //                                           height: 50,
  //                                           padding: EdgeInsets.symmetric(
  //                                               horizontal: 10, vertical: 10),
  //                                           child: Row(
  //                                             children: [
  //                                               CircleAvatar(
  //                                                 backgroundImage: NetworkImage(
  //                                                     'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
  //                                                 radius: 15,
  //                                               ),
  //                                               SizedBox(
  //                                                 width: 10,
  //                                               ),
  //                                               Column(
  //                                                 children: [
  //                                                   Text(
  //                                                     'John Doe',
  //                                                     style: TextStyle(
  //                                                         color: Colors.black),
  //                                                   ),
  //                                                   Text(
  //                                                     'Supervisor',
  //                                                     style: TextStyle(
  //                                                         color: Colors.grey,
  //                                                         fontSize: 12),
  //                                                   ),
  //                                                 ],
  //                                               )
  //                                             ],
  //                                           ),
  //                                         ),
  //                                       ),
  //                                     )
  //                                   ],
  //                                 ),
  //                               ),
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                       children: <Widget>[
  //                         Flexible(
  //                           child: TextField(
  //                             controller: startDate,
  //                             decoration: InputDecoration(
  //                               fillColor: ColorConfig.blue1,
  //                               filled: true,
  //                               hintText: 'Start Date',
  //                               hintStyle: TextStyle(
  //                                   color: ColorConfig.black1,
  //                                   fontSize: 14,
  //                                   fontWeight: FontWeight.w400),
  //                               contentPadding: EdgeInsets.symmetric(
  //                                   horizontal: 10, vertical: 10),
  //                               border: OutlineInputBorder(
  //                                   borderRadius: BorderRadius.circular(10),
  //                                   borderSide: BorderSide.none),
  //                             ),
  //                             maxLines: null,
  //                             style: TextStyle(
  //                                 fontWeight: FontWeight.w500,
  //                                 color: Colors.black.withOpacity(0.74),
  //                                 fontSize: 14),
  //                           ),
  //                         ),
  //                         SizedBox(
  //                           width: 10,
  //                         ),
  //                         Flexible(
  //                           child: TextField(
  //                             controller: endDate,
  //                             decoration: InputDecoration(
  //                               fillColor: ColorConfig.blue1,
  //                               filled: true,
  //                               hintText: 'End Date',
  //                               hintStyle: TextStyle(
  //                                   color: ColorConfig.black1,
  //                                   fontSize: 14,
  //                                   fontWeight: FontWeight.w400),
  //                               contentPadding: EdgeInsets.symmetric(
  //                                   horizontal: 10, vertical: 10),
  //                               border: OutlineInputBorder(
  //                                   borderRadius: BorderRadius.circular(10),
  //                                   borderSide: BorderSide.none),
  //                             ),
  //                             maxLines: null,
  //                             style: TextStyle(
  //                                 fontWeight: FontWeight.w500,
  //                                 color: Colors.black.withOpacity(0.74),
  //                                 fontSize: 14),
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                       children: <Widget>[
  //                         Flexible(
  //                           child: TextField(
  //                             controller: startTime,
  //                             decoration: InputDecoration(
  //                               fillColor: ColorConfig.blue1,
  //                               filled: true,
  //                               hintText: 'Start Time',
  //                               hintStyle: TextStyle(
  //                                   color: ColorConfig.black1,
  //                                   fontSize: 14,
  //                                   fontWeight: FontWeight.w400),
  //                               contentPadding: EdgeInsets.symmetric(
  //                                   horizontal: 10, vertical: 10),
  //                               border: OutlineInputBorder(
  //                                   borderRadius: BorderRadius.circular(10),
  //                                   borderSide: BorderSide.none),
  //                             ),
  //                             maxLines: null,
  //                             style: TextStyle(
  //                                 fontWeight: FontWeight.w500,
  //                                 color: Colors.black.withOpacity(0.74),
  //                                 fontSize: 14),
  //                           ),
  //                         ),
  //                         SizedBox(
  //                           width: 10,
  //                         ),
  //                         Flexible(
  //                           child: TextField(
  //                             controller: endTime,
  //                             decoration: InputDecoration(
  //                               fillColor: ColorConfig.blue1,
  //                               filled: true,
  //                               hintText: 'End Time',
  //                               hintStyle: TextStyle(
  //                                   color: ColorConfig.black1,
  //                                   fontSize: 14,
  //                                   fontWeight: FontWeight.w400),
  //                               contentPadding: EdgeInsets.symmetric(
  //                                   horizontal: 10, vertical: 10),
  //                               border: OutlineInputBorder(
  //                                   borderRadius: BorderRadius.circular(10),
  //                                   borderSide: BorderSide.none),
  //                             ),
  //                             maxLines: null,
  //                             style: TextStyle(
  //                                 fontWeight: FontWeight.w500,
  //                                 color: Colors.black.withOpacity(0.74),
  //                                 fontSize: 14),
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     TextField(
  //                       controller: location,
  //                       decoration: InputDecoration(
  //                         fillColor: ColorConfig.blue1,
  //                         filled: true,
  //                         hintText: 'Location',
  //                         hintStyle: TextStyle(
  //                             color: ColorConfig.black1,
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.w400),
  //                         contentPadding: EdgeInsets.symmetric(
  //                             horizontal: 10, vertical: 10),
  //                         border: OutlineInputBorder(
  //                             borderRadius: BorderRadius.circular(10),
  //                             borderSide: BorderSide.none),
  //                       ),
  //                       maxLines: null,
  //                       style: TextStyle(
  //                           fontWeight: FontWeight.w500,
  //                           color: Colors.black.withOpacity(0.74),
  //                           fontSize: 14),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     TextField(
  //                       controller: note,
  //                       decoration: InputDecoration(
  //                         fillColor: ColorConfig.blue1,
  //                         filled: true,
  //                         hintText: 'Note',
  //                         hintStyle: TextStyle(
  //                             color: ColorConfig.black1,
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.w400),
  //                         contentPadding: EdgeInsets.symmetric(
  //                             horizontal: 10, vertical: 10),
  //                         border: OutlineInputBorder(
  //                             borderRadius: BorderRadius.circular(10),
  //                             borderSide: BorderSide.none),
  //                       ),
  //                       maxLines: null,
  //                       style: TextStyle(
  //                           fontWeight: FontWeight.w500,
  //                           color: Colors.black.withOpacity(0.74),
  //                           fontSize: 14),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.end,
  //                       children: <Widget>[
  //                         FlatButton(
  //                           shape: RoundedRectangleBorder(
  //                               borderRadius: BorderRadius.circular(5)),
  //                           color: ColorConfig.grey3,
  //                           child: Text(
  //                             'No',
  //                             style: TextStyle(fontSize: 12)
  //                                 .copyWith(color: ColorConfig.grey2),
  //                           ),
  //                           onPressed: () {
  //                             Navigator.of(context).pop();
  //                           },
  //                         ),
  //                         SizedBox(
  //                           width: 10,
  //                         ),
  //                         FlatButton(
  //                           shape: RoundedRectangleBorder(
  //                               borderRadius: BorderRadius.circular(5)),
  //                           color: ColorConfig.primary,
  //                           child: Text(
  //                             'Add',
  //                             style: TextStyle(fontSize: 12)
  //                                 .copyWith(color: Colors.white),
  //                           ),
  //                           onPressed: () {
  //                             // List<dynamic> prevOb = [];
  //                             // /**This is because the return type of the library is Datetime.Therefor need a comparison to find the key */
  //                             // _events.forEach((key, value) {
  //                             //   if (key.toString().split(" ")[0] ==
  //                             //       _clickedDay
  //                             //           .toUtc()
  //                             //           .toString()
  //                             //           .split(" ")[0]) {
  //                             //     setState(() {
  //                             //       hasAEvent = true;
  //                             //     });
  //                             //     prevOb = _events[key];
  //                             //     prevOb.add(titleController.text);
  //                             //     _events[key] = prevOb;
  //                             //     print(_events[key]);
  //                             //   }
  //                             // });

  //                             // if (!hasAEvent) {
  //                             //   _events[_clickedDay] = [titleController.text];
  //                             // }

  //                             // Navigator.of(context).pop();
  //                           },
  //                         )
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //               ),
  //             );
  //           },
  //         );
  //       });
  // }

  _filterView() {
    Set<String> selectedEmployeeList = Set();

    int selectedRadio = 2;

    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            // StatefulBuilder
            builder: (context, setState) {
              return Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                backgroundColor: Colors.white,
                child: Container(
                  height: 600,
                  padding: EdgeInsets.all(20),
                  child: ListView(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: ColorConfig.orange,
                            child: Icon(
                              Icons.filter_hdr,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Text(
                            'Select Filters',
                            style: TextStyle(
                                color: ColorConfig.primary,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          IconButton(
                            icon: Icon(Icons.close),
                            color: ColorConfig.primary,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      ),
                      Column(
                        // mainAxisSize: MainAxisSize.min,
                        // children: List<Widget>.generate(4, (int index) {
                        //   return Radio<int>(
                        //     value: index,
                        //     groupValue: selectedRadio,
                        //     onChanged: (int value) {
                        //       print(value);
                        //       setState(() => selectedRadio = value);
                        //     },
                        //   );
                        // }),
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 0,
                                groupValue: selectedRadio,
                                onChanged: (int value) {
                                  setState(() => selectedRadio = value);
                                },
                              ),
                              Text(
                                "All",
                                style: TextStyle(color: Colors.blueAccent),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 1,
                                groupValue: selectedRadio,
                                onChanged: (int value) {
                                  setState(() => selectedRadio = value);
                                },
                              ),
                              Text(
                                "By Scrum board & sub project",
                                style: TextStyle(color: Colors.blueAccent),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 2,
                                groupValue: selectedRadio,
                                onChanged: (int value) {
                                  setState(() => selectedRadio = value);
                                },
                              ),
                              Text(
                                "By Employee",
                                style: TextStyle(color: Colors.blueAccent),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Radio(
                                value: 3,
                                groupValue: selectedRadio,
                                onChanged: (int value) {
                                  setState(() => selectedRadio = value);
                                },
                              ),
                              Text(
                                "By Crew",
                                style: TextStyle(color: Colors.blueAccent),
                              )
                            ],
                          ),
                        ],
                      ),
                      Text(
                        'Select Employee',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      selectedEmployeeList.length == 0
                          ? Container(
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Select Employee'),
                                  Icon(
                                    Icons.add,
                                    color: ColorConfig.primary,
                                  )
                                ],
                              ),
                            )
                          : Container(
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5)),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: ListView(
                                      scrollDirection: Axis.horizontal,
                                      children: List.generate(
                                          selectedEmployeeList.length,
                                          (index) => InputChip(
                                              label: Text(
                                                selectedEmployeeList
                                                    .elementAt(index),
                                                style: TextStyle(
                                                    color: ColorConfig.primary),
                                              ),
                                              deleteIcon: Icon(Icons.close,
                                                  size: 14,
                                                  color: ColorConfig.primary),
                                              onDeleted: () {
                                                setState(() {
                                                  selectedEmployeeList.remove(
                                                      selectedEmployeeList
                                                          .elementAt(index));
                                                });
                                              },
                                              onPressed: () {})),
                                    ),
                                  ),
                                  Icon(
                                    Icons.add,
                                    color: ColorConfig.primary,
                                  )
                                ],
                              ),
                            ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 3,
                                  color: Colors.grey.shade300,
                                  offset: Offset(2, 2),
                                  spreadRadius: 3)
                            ],
                            color: Colors.white),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Container(
                            height: 150,
                            child: ListView(
                              children: List.generate(
                                10,
                                (index) => Material(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        selectedEmployeeList
                                            .add("John Doe" + index.toString());
                                      });
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 5),
                                      child: Row(
                                        children: <Widget>[
                                          Checkbox(
                                            materialTapTargetSize:
                                                MaterialTapTargetSize
                                                    .shrinkWrap,
                                            onChanged: (val) {
                                              if (val) {
                                                setState(() {
                                                  selectedEmployeeList.add(
                                                      "John Doe" +
                                                          index.toString());
                                                });
                                              } else {
                                                setState(() {
                                                  selectedEmployeeList.remove(
                                                      "John Doe" +
                                                          index.toString());
                                                });
                                              }
                                            },
                                            value: selectedEmployeeList
                                                .contains("John Doe" +
                                                    index.toString()),
                                          ),
                                          Text(
                                            "John Doe" + index.toString(),
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: ColorConfig.primary),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                            color: ColorConfig.grey3,
                            child: Text(
                              'No',
                              style: TextStyle(fontSize: 12)
                                  .copyWith(color: ColorConfig.grey2),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                            color: ColorConfig.primary,
                            child: Text(
                              'Add',
                              style: TextStyle(fontSize: 12)
                                  .copyWith(color: Colors.white),
                            ),
                            onPressed: () {
                              // List<dynamic> prevOb = [];
                              /**This is because the return type of the library is Datetime.Therefor need a comparison to find the key */
                              // _events.forEach((key, value) {
                              //   // print(key.toString().split(" ")[0]);
                              //   if (key.toString().split(" ")[0] ==
                              //       _clickedDay
                              //           .toUtc()
                              //           .toString()
                              //           .split(" ")[0]) {
                              //     // print(key);
                              //     prevOb = _events[key];
                              //     prevOb.add(titleController.text);
                              //     _events[key] = prevOb;
                              //     print(_events[key]);
                              //   }
                              // });

                              // Navigator.of(context).pop();
                            },
                          )
                        ],
                      )
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders(
      Map<DateTime, List<CalenderEventModel>> events) {
    return TableCalendar(
      locale: 'en_US',
      calendarController: _calendarController,
      events: events,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.horizontalSwipe,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.blue[800]),
        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
              opacity:
                  Tween(begin: 0.0, end: 1.0).animate(_animationController),
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: ColorConfig.primary,
                ),
                child: Center(
                  child: Text(
                    '${date.day}',
                    style: TextStyle()
                        .copyWith(fontSize: 14.0, color: Colors.white),
                  ),
                ),
              ));
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.orange[300],
            ),
            child: Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 14.0),
              ),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays.isNotEmpty) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: (date, events, events2) {
        _onDaySelected(date, events);
        _animationController.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: _calendarController.isSelected(date)
            ? Colors.green[500]
            : _calendarController.isToday(date)
                ? Colors.green[300]
                : Colors.green[400],
      ),
      width: 14.0,
      height: 14.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildEventList(
      Map<DateTime, List<CalenderEventModel>> events, BuildContext _context) {
    return events[_clickedDay] == null
        ? Container()
        : Column(
            children: events[_clickedDay]
                .map((event) => Container(
                      // decoration: BoxDecoration(
                      //   border: Border.all(width: 0.8, color: ColorConfig. blue1),
                      //   borderRadius: BorderRadius.circular(12.0),
                      //   color: ColorConfig. grey10,
                      // ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            spreadRadius: 2,
                            blurRadius: 6,
                            offset: Offset(0, 3),
                          ),
                        ],
                      ),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 4.0),
                      // child: ListTile(
                      //   title: Text(event.toString()),
                      //   onTap: () => {print('$event tapped!'), _editView()},

                      // ),Ed

                      child: InkWell(
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(8),
                              child: CircleAvatar(
                                radius: 15,
                                backgroundColor: ColorConfig.orange,
                                child: ImageIcon(
                                  AssetImage(LocalImages.calendarCheck),
                                  color: Colors.white,
                                  size: 14,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              event?.title ?? '',
                              maxLines: 5,
                              overflow: TextOverflow.clip,
                            ),
                            Spacer(),
                            PopupMenuButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              onSelected: (val) {
                                if (val == 'edit') {
                                  Navigator.of(context)
                                      .push(MaterialPageRoute(
                                    builder: (context) => AddEventScreen(
                                      event: event,
                                    ),
                                  ))
                                      .then((value) {
                                    _context
                                        .read<CalenderBloc>()
                                        .add(CalenderEventsRequested());
                                  });

                                  // _editView();
                                }
                                if (val == 'delete') {
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) => Dialog(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      backgroundColor: Colors.white,
                                      child: Container(
                                        width: 0,
                                        padding: EdgeInsets.all(20),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                CircleAvatar(
                                                  backgroundColor:
                                                      ColorConfig.orange,
                                                  child: ImageIcon(
                                                    AssetImage(LocalImages
                                                        .exclamation),
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Flexible(
                                                  child: Text(
                                                    'Are you sure you want to delete this event. All data will be lost.',
                                                    style: TextStyle(
                                                        color:
                                                            ColorConfig.primary,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                FlatButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  color: ColorConfig.grey3,
                                                  child: Text(
                                                    'No',
                                                    style:
                                                        TextStyle(fontSize: 12)
                                                            .copyWith(
                                                                color:
                                                                    ColorConfig
                                                                        .grey2),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                FlatButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  color: ColorConfig.primary,
                                                  child: Text(
                                                    'Yes',
                                                    style:
                                                        TextStyle(fontSize: 12)
                                                            .copyWith(
                                                                color: Colors
                                                                    .white),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                    _context
                                                        .read<CalenderBloc>()
                                                        .add(CalenderDeleteEvent(
                                                            calenderEventModel:
                                                                event));
                                                  },
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              },
                              itemBuilder: (context) {
                                return [
                                  PopupMenuItem(
                                    height: 36,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('Edit',
                                            textAlign: TextAlign.center),
                                      ],
                                    ),
                                    value: 'edit',
                                  ),
                                  PopupMenuItem(
                                    height: 36,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('Delete',
                                            textAlign: TextAlign.center),
                                      ],
                                    ),
                                    value: 'delete',
                                  ),
                                ];
                              },
                            )
                          ],
                        ),
                      ),
                    ))
                .toList(),
          );
  }
}
