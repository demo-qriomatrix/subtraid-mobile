import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/calender/bloc/add_event/add_event_bloc.dart';
import 'package:Subtraid/features/calender/models/calender_color_model.dart';
import 'package:Subtraid/features/calender/models/calender_event_model.dart';
import 'package:Subtraid/features/calender/models/calender_meta_model.dart';
import 'package:Subtraid/features/more/company/features/employee/bloc/employee_bloc.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:built_collection/built_collection.dart';

class AddEventScreen extends StatefulWidget {
  final CalenderEventModel event;

  AddEventScreen({@required this.event});

  @override
  _AddEventScreenState createState() => _AddEventScreenState();
}

class _AddEventScreenState extends State<AddEventScreen> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  bool autoValidate = false;

  TextEditingController titleController = TextEditingController();
  TextEditingController statusController = TextEditingController();
  TextEditingController primaryColor = TextEditingController();
  TextEditingController secondaryColor = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController startTime = TextEditingController();
  TextEditingController endTime = TextEditingController();
  TextEditingController location = TextEditingController();
  TextEditingController note = TextEditingController();
  TextEditingController member = TextEditingController();

  Set<EmployeeModel> selectedMembers = Set();

  @override
  void initState() {
    if (widget.event != null) {
      titleController.text = widget.event.title;
      location.text = widget.event.meta.location;
      note.text = widget.event.meta.notes;
      String userId;

      if (context.read<AuthenticationBloc>().state is AuthentcatedState) {
        userId = (context.read<AuthenticationBloc>().state as AuthentcatedState)
            .user
            .id;
      }

      selectedMembers = widget.event.members
          .where((mem) => mem != userId)
          .map((e) => EmployeeModel((emp) =>
              emp..user = MemberModel((mem) => mem.id = e).toBuilder()))
          .toSet();

      startDate.text = DateFormat('MM/dd/yyyy')
          .format(DateTime.parse(widget.event.start).toLocal());
      endDate.text = DateFormat('MM/dd/yyyy')
          .format(DateTime.parse(widget.event.end).toLocal());
      startTime.text = DateFormat('hh:mm a')
          .format(DateTime.parse(widget.event.start).toLocal());
      endTime.text = DateFormat('hh:mm a')
          .format(DateTime.parse(widget.event.end).toLocal());

      pickerPrimaryColor = Color(
          int.parse(widget.event.color.primary.substring(1, 7), radix: 16) +
              0xFF000000);
      currentPrimaryColor = Color(
          int.parse(widget.event.color.primary.substring(1, 7), radix: 16) +
              0xFF000000);

      pickerSecondaryColor = Color(
          int.parse(widget.event.color.secondary.substring(1, 7), radix: 16) +
              0xFF000000);
      currentSecondaryColor = Color(
          int.parse(widget.event.color.secondary.substring(1, 7), radix: 16) +
              0xFF000000);
    } else {
      startDate.text = DateFormat('MM/dd/yyyy').format(DateTime.now());
      endDate.text = DateFormat('MM/dd/yyyy').format(DateTime.now());
      startTime.text = DateFormat('hh:mm a').format(DateTime.now());
      endTime.text =
          DateFormat('hh:mm a').format(DateTime.now().add(Duration(hours: 1)));

      pickerPrimaryColor = Color(0xff1e90ff);
      currentPrimaryColor = Color(0xff1e90ff);

      pickerSecondaryColor = Color(0xffd1e8ff);
      currentSecondaryColor = Color(0xffd1e8ff);
    }

    super.initState();
  }

  // create some values
  Color pickerPrimaryColor;
  Color currentPrimaryColor;

// ValueChanged<Color> callback
  void changePrimaryColor(Color color) {
    setState(() => pickerPrimaryColor = color);
  }

  // create some values
  Color pickerSecondaryColor;
  Color currentSecondaryColor;

// ValueChanged<Color> callback
  void changeSecondaryColor(Color color) {
    setState(() => pickerSecondaryColor = color);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConfig.primary,
      appBar: StAppBar(
        title: widget.event == null ? 'Add Event' : 'Update Event',
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: BlocProvider<AddEventBloc>(
          lazy: false,
          create: (context) => AddEventBloc(calenderRepository: sl()),
          child: Column(
            children: [
              Expanded(
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(50))),
                    child: ConstrainedBox(
                      constraints:
                          BoxConstraints(minHeight: getMinHeight(context) + 30),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 32),
                        child: BlocConsumer<AddEventBloc, AddEventState>(
                          listener: (context, state) {
                            if (state is AddEventInProgress) {
                              context
                                  .read<GlobalBloc>()
                                  .add(ShowLoadingWidget());
                            }
                            if (state is AddEventSuccess) {
                              context
                                  .read<GlobalBloc>()
                                  .add(HideLoadingWidget());
                              context.read<GlobalBloc>().add(
                                  ShowSuccessSnackBar(
                                      message: 'Event added successfully',
                                      event: null));

                              Future.delayed(
                                  Duration(
                                    seconds: 3,
                                  ), () {
                                Navigator.of(context).pop();
                              });
                            }
                            if (state is AddEventFailure) {
                              context
                                  .read<GlobalBloc>()
                                  .add(HideLoadingWidget());
                              context.read<GlobalBloc>().add(ShowErrorSnackBar(
                                  error: 'Something went wrong'));
                            }
                          },
                          builder: (context, state) => Form(
                            key: formKey,
                            autovalidateMode: autoValidate
                                ? AutovalidateMode.always
                                : AutovalidateMode.disabled,
                            child: ListView(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              children: <Widget>[
                                // Row(
                                //   children: <Widget>[
                                //     CircleAvatar(
                                //       backgroundColor: ColorConfig.orange,
                                //       child: Icon(
                                //         Icons.add,
                                //         color: Colors.white,
                                //       ),
                                //     ),
                                //     SizedBox(
                                //       width: 20,
                                //     ),
                                //     Text(
                                //       'Add Event',
                                //       style: TextStyle(
                                //           color: ColorConfig.primary,
                                //           fontSize: 14,
                                //           fontWeight: FontWeight.w500),
                                //     ),
                                //     Spacer(),
                                //     IconButton(
                                //       icon: Icon(Icons.close),
                                //       color: ColorConfig.primary,
                                //       onPressed: () {
                                //         Navigator.of(context).pop();
                                //       },
                                //     )
                                //   ],
                                // ),

                                SizedBox(
                                  height: 20,
                                ),
                                TextFormField(
                                  validator: (val) {
                                    if (val.isEmpty) return 'Title is required';

                                    return null;
                                  },
                                  controller: titleController,
                                  decoration: InputDecoration(
                                    fillColor: ColorConfig.blue1,
                                    filled: true,
                                    labelText: 'Title',
                                    labelStyle: TextStyle(
                                        color: ColorConfig.black1,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide.none),
                                  ),
                                  maxLines: null,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black.withOpacity(0.74),
                                      fontSize: 14),
                                ),
                                // SizedBox(
                                //   height: 10,
                                // ),
                                // TextField(
                                //   controller: statusController,
                                //   decoration: InputDecoration(
                                //     fillColor: ColorConfig.blue1,
                                //     filled: true,
                                //     labelText: 'Status',
                                //     labelStyle: TextStyle(
                                //         color: ColorConfig.black1,
                                //         fontSize: 14,
                                //         fontWeight: FontWeight.w400),
                                //     contentPadding: EdgeInsets.symmetric(
                                //         horizontal: 10, vertical: 10),
                                //     border: OutlineInputBorder(
                                //         borderRadius: BorderRadius.circular(10),
                                //         borderSide: BorderSide.none),
                                //   ),
                                //   maxLines: null,
                                //   style: TextStyle(
                                //       fontWeight: FontWeight.w500,
                                //       color: Colors.black.withOpacity(0.74),
                                //       fontSize: 14),
                                // ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Flexible(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Primary Color',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 13,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              showDialog(
                                                  useRootNavigator: false,
                                                  context: context,
                                                  child: AlertDialog(
                                                    title: const Text(
                                                        'Pick a color!'),
                                                    content:
                                                        SingleChildScrollView(
                                                      child: ColorPicker(
                                                        pickerColor:
                                                            pickerPrimaryColor,
                                                        onColorChanged:
                                                            changePrimaryColor,
                                                        showLabel: true,
                                                        pickerAreaHeightPercent:
                                                            0.8,
                                                      ),
                                                    ),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        child: const Text(
                                                            'Got it'),
                                                        onPressed: () {
                                                          setState(() =>
                                                              currentPrimaryColor =
                                                                  pickerPrimaryColor);
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                    ],
                                                  ));
                                            },
                                            child: Container(
                                              margin:
                                                  EdgeInsets.only(right: 40),
                                              decoration: BoxDecoration(
                                                  color: currentPrimaryColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              height: 32,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Flexible(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Secondary Color',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 13,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              showDialog(
                                                  useRootNavigator: false,
                                                  context: context,
                                                  child: AlertDialog(
                                                    title: const Text(
                                                        'Pick a color!'),
                                                    content:
                                                        SingleChildScrollView(
                                                      child: ColorPicker(
                                                        pickerColor:
                                                            pickerSecondaryColor,
                                                        onColorChanged:
                                                            changeSecondaryColor,
                                                        showLabel: true,
                                                        pickerAreaHeightPercent:
                                                            0.8,
                                                      ),
                                                    ),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        child: const Text(
                                                            'Got it'),
                                                        onPressed: () {
                                                          setState(() =>
                                                              currentSecondaryColor =
                                                                  pickerSecondaryColor);
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                      ),
                                                    ],
                                                  ));
                                            },
                                            child: Container(
                                              margin:
                                                  EdgeInsets.only(right: 40),
                                              decoration: BoxDecoration(
                                                  color: currentSecondaryColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20)),
                                              height: 32,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  'Members',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500),
                                ),

                                selectedMembers.isEmpty
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Row(
                                          children: selectedMembers
                                              .map((e) => Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 10),
                                                    child: InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          selectedMembers
                                                              .remove(e);
                                                        });
                                                      },
                                                      child: SizedBox(
                                                        height: 40,
                                                        width: 40,
                                                        child: Stack(
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .all(4.0),
                                                              child:
                                                                  CircleAvatar(
                                                                backgroundImage:
                                                                    AssetImage(
                                                                        LocalImages
                                                                            .defaultAvatar),
                                                                radius: 16,
                                                              ),
                                                            ),
                                                            Align(
                                                              alignment:
                                                                  Alignment
                                                                      .topRight,
                                                              child: Icon(
                                                                Icons
                                                                    .cancel_outlined,
                                                                color:
                                                                    Colors.red,
                                                                size: 18,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ))
                                              .toList(),
                                        ),
                                      ),
                                SizedBox(
                                  height: 10,
                                ),

                                BlocProvider<EmployeeBloc>(
                                  create: (context) =>
                                      EmployeeBloc(employeeRepository: sl()),
                                  child:
                                      BlocBuilder<EmployeeBloc, EmployeeState>(
                                    builder: (context, state) {
                                      return TypeAheadFormField(
                                          enabled: true,
                                          textFieldConfiguration:
                                              TextFieldConfiguration(
                                            controller: member,
                                            decoration: InputDecoration(
                                              fillColor: ColorConfig.blue1,
                                              filled: true,
                                              labelText: 'Select members',
                                              labelStyle: TextStyle(
                                                  color: ColorConfig.black1,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400),
                                              suffixIcon: Icon(
                                                Icons.add,
                                                color: ColorConfig.primary,
                                              ),
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 10,
                                                      vertical: 10),
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  borderSide: BorderSide.none),
                                            ),
                                          ),
                                          suggestionsCallback: (pattern) {
                                            if (state is EmployeeLoadSuccess) {
                                              return state
                                                  .companyInfo.company.employees
                                                  .where((element) => element
                                                      .user.name
                                                      .toLowerCase()
                                                      .contains(pattern
                                                          .toLowerCase()))
                                                  .toList();
                                            }
                                            return null;
                                          },
                                          itemBuilder: (context,
                                              EmployeeModel suggestion) {
                                            return Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 10,
                                                      vertical: 10),
                                              child: Row(
                                                children: [
                                                  Theme(
                                                    data: ThemeData(
                                                        unselectedWidgetColor:
                                                            ColorConfig.orange),
                                                    child: IgnorePointer(
                                                      child: Checkbox(
                                                          value: selectedMembers
                                                              .where((mem) =>
                                                                  mem.user.id ==
                                                                  suggestion
                                                                      .user.id)
                                                              .isNotEmpty,
                                                          onChanged: (val) {
                                                            if (val) {
                                                              setState(() {
                                                                selectedMembers.add(
                                                                    suggestion);
                                                              });
                                                            } else {
                                                              setState(() {
                                                                selectedMembers.remove(selectedMembers
                                                                    .where((mem) =>
                                                                        mem.user
                                                                            .id ==
                                                                        suggestion
                                                                            .user
                                                                            .id)
                                                                    .first);
                                                              });
                                                            }
                                                          }),
                                                    ),
                                                  ),
                                                  CircleAvatar(
                                                    backgroundImage: AssetImage(
                                                        LocalImages
                                                            .defaultAvatar),
                                                    radius: 16,
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(suggestion.user.name)
                                                ],
                                              ),
                                            );
                                          },
                                          transitionBuilder: (context,
                                              suggestionsBox, controller) {
                                            return suggestionsBox;
                                          },
                                          onSuggestionSelected: (suggestion) {
                                            if (selectedMembers
                                                .where((mem) =>
                                                    mem.user.id ==
                                                    suggestion.user.id)
                                                .isEmpty) {
                                              setState(() {
                                                selectedMembers.add(suggestion);
                                              });
                                            } else {
                                              setState(() {
                                                selectedMembers.remove(
                                                    selectedMembers
                                                        .where((mem) =>
                                                            mem.user.id ==
                                                            suggestion.user.id)
                                                        .first);
                                              });
                                            }
                                          },
                                          validator: (value) {
                                            return null;
                                          },
                                          onSaved: (value) {});
                                    },
                                  ),
                                ),

                                // selectedMember.length == 0
                                //     ? Container(
                                //         height: 50,
                                //         decoration: BoxDecoration(
                                //             color: ColorConfig.grey10,
                                //             borderRadius: BorderRadius.circular(5)),
                                //         padding: EdgeInsets.symmetric(
                                //             horizontal: 10, vertical: 5),
                                //         child: Row(
                                //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //           children: [
                                //             Text('Select members'),
                                //             Icon(
                                //               Icons.add,
                                //               color: ColorConfig.primary,
                                //             )
                                //           ],
                                //         ),
                                //       )
                                //     : Container(
                                //         height: 50,
                                //         decoration: BoxDecoration(
                                //             color: ColorConfig.grey10,
                                //             borderRadius: BorderRadius.circular(5)),
                                //         padding: EdgeInsets.symmetric(
                                //             horizontal: 10, vertical: 5),
                                //         child: Row(
                                //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //           children: [
                                //             // Expanded(
                                //             //   child: ListView(
                                //             //     scrollDirection: Axis.horizontal,
                                //             //     children: List.generate(
                                //             //         selectedMember.length,
                                //             //         (index) => Padding(
                                //             //               padding:
                                //             //                   const EdgeInsets.symmetric(
                                //             //                       horizontal: 5),
                                //             //               child: InkWell(
                                //             //                 onTap: () {
                                //             //                   print(index);
                                //             //                   setState(() {
                                //             //                     selectedMember
                                //             //                         .removeAt(index);
                                //             //                   });
                                //             //                 },
                                //             //                 child: CircleAvatar(
                                //             //                   backgroundImage: NetworkImage(
                                //             //                       "https://diievents.dii.eu/brexit-pro-diirect/wp-content/uploads/sites/52/2015/04/speaker-3-v2.jpg"),
                                //             //                   radius: 15,
                                //             //                 ),
                                //             //               ),
                                //             //             )),
                                //             //   ),
                                //             // ),
                                //             Icon(
                                //               Icons.add,
                                //               color: ColorConfig.primary,
                                //             )
                                //           ],
                                //         ),
                                //       ),

                                // SizedBox(
                                //   height: 10,
                                // ),
                                // Padding(
                                //   padding: const EdgeInsets.symmetric(horizontal: 5),
                                //   child: Container(
                                //     decoration: BoxDecoration(
                                //         borderRadius: BorderRadius.circular(5),
                                //         boxShadow: [
                                //           BoxShadow(
                                //               blurRadius: 3,
                                //               color: Colors.grey.shade300,
                                //               offset: Offset(2, 2),
                                //               spreadRadius: 3)
                                //         ],
                                //         color: Colors.white),
                                //     child: ClipRRect(
                                //       borderRadius: BorderRadius.circular(5),
                                //       child: Container(
                                //         height: 150,
                                //         child: ListView(
                                //           children: List.generate(
                                //             10,
                                //             (index) => Column(
                                //               crossAxisAlignment:
                                //                   CrossAxisAlignment.start,
                                //               children: <Widget>[
                                //                 Material(
                                //                   child: InkWell(
                                //                     onTap: () {
                                //                       // setState(() {
                                //                       //   selectedMember.add('John Doe');
                                //                       // });
                                //                     },
                                //                     child: Container(
                                //                       height: 50,
                                //                       padding: EdgeInsets.symmetric(
                                //                           horizontal: 10, vertical: 10),
                                //                       child: Row(
                                //                         children: [
                                //                           CircleAvatar(
                                //                             backgroundImage: NetworkImage(
                                //                                 'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
                                //                             radius: 15,
                                //                           ),
                                //                           SizedBox(
                                //                             width: 10,
                                //                           ),
                                //                           Column(
                                //                             children: [
                                //                               Text(
                                //                                 'John Doe',
                                //                                 style: TextStyle(
                                //                                     color:
                                //                                         Colors.black),
                                //                               ),
                                //                               Text(
                                //                                 'Supervisor',
                                //                                 style: TextStyle(
                                //                                     color: Colors.grey,
                                //                                     fontSize: 12),
                                //                               ),
                                //                             ],
                                //                           )
                                //                         ],
                                //                       ),
                                //                     ),
                                //                   ),
                                //                 )
                                //               ],
                                //             ),
                                //           ),
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                                // ),

                                SizedBox(
                                  height: 12,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Flexible(
                                      child: TextFormField(
                                        onTap: () {
                                          showDatePicker(
                                            context: context,
                                            firstDate: DateTime.now()
                                                .subtract(Duration(days: 3650)),
                                            initialDate: DateTime.now(),
                                            lastDate: DateTime.now()
                                                .add(Duration(days: 3650)),
                                          ).then((value) {
                                            if (value != null) {
                                              startDate.text =
                                                  DateFormat('MM/dd/yyyy')
                                                      .format(value);
                                            }
                                          });
                                        },
                                        readOnly: true,
                                        controller: startDate,
                                        decoration: InputDecoration(
                                          fillColor: ColorConfig.blue1,
                                          filled: true,
                                          labelText: 'Start Date',
                                          labelStyle: TextStyle(
                                              color: ColorConfig.black1,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400),
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                        maxLines: null,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color:
                                                Colors.black.withOpacity(0.74),
                                            fontSize: 14),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Flexible(
                                      child: TextFormField(
                                        onTap: () {
                                          showDatePicker(
                                            context: context,
                                            firstDate: DateTime.now()
                                                .subtract(Duration(days: 3650)),
                                            initialDate: DateTime.now(),
                                            lastDate: DateTime.now()
                                                .add(Duration(days: 3650)),
                                          ).then((value) {
                                            if (value != null) {
                                              endDate.text =
                                                  DateFormat('MM/dd/yyyy')
                                                      .format(value);
                                            }
                                          });
                                        },
                                        readOnly: true,
                                        controller: endDate,
                                        decoration: InputDecoration(
                                          fillColor: ColorConfig.blue1,
                                          filled: true,
                                          labelText: 'End Date',
                                          labelStyle: TextStyle(
                                              color: ColorConfig.black1,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400),
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                        maxLines: null,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color:
                                                Colors.black.withOpacity(0.74),
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Flexible(
                                      child: TextFormField(
                                        onTap: () {
                                          showTimePicker(
                                                  context: context,
                                                  initialTime: TimeOfDay.now())
                                              .then((value) {
                                            if (value != null) {
                                              startTime.text =
                                                  value.format(context);
                                            }
                                          });
                                        },
                                        readOnly: true,
                                        controller: startTime,
                                        decoration: InputDecoration(
                                          fillColor: ColorConfig.blue1,
                                          filled: true,
                                          labelText: 'Start Time',
                                          labelStyle: TextStyle(
                                              color: ColorConfig.black1,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400),
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                        maxLines: null,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color:
                                                Colors.black.withOpacity(0.74),
                                            fontSize: 14),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Flexible(
                                      child: TextFormField(
                                        onTap: () {
                                          showTimePicker(
                                                  context: context,
                                                  initialTime: TimeOfDay.now())
                                              .then((value) {
                                            if (value != null) {
                                              endTime.text =
                                                  value.format(context);
                                            }
                                          });
                                        },
                                        readOnly: true,
                                        controller: endTime,
                                        decoration: InputDecoration(
                                          fillColor: ColorConfig.blue1,
                                          filled: true,
                                          labelText: 'End Time',
                                          labelStyle: TextStyle(
                                              color: ColorConfig.black1,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400),
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide.none),
                                        ),
                                        maxLines: null,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            color:
                                                Colors.black.withOpacity(0.74),
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  controller: location,
                                  decoration: InputDecoration(
                                    fillColor: ColorConfig.blue1,
                                    filled: true,
                                    labelText: 'Location',
                                    labelStyle: TextStyle(
                                        color: ColorConfig.black1,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide.none),
                                  ),
                                  maxLines: null,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black.withOpacity(0.74),
                                      fontSize: 14),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                TextField(
                                  controller: note,
                                  decoration: InputDecoration(
                                    fillColor: ColorConfig.blue1,
                                    filled: true,
                                    labelText: 'Note',
                                    labelStyle: TextStyle(
                                        color: ColorConfig.black1,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide.none),
                                  ),
                                  maxLines: null,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black.withOpacity(0.74),
                                      fontSize: 14),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      color: ColorConfig.grey3,
                                      child: Text(
                                        'No',
                                        style: TextStyle(fontSize: 12)
                                            .copyWith(color: ColorConfig.grey2),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      color: ColorConfig.primary,
                                      child: Text(
                                        widget.event == null ? 'Add' : 'Update',
                                        style: TextStyle(fontSize: 12)
                                            .copyWith(color: Colors.white),
                                      ),
                                      onPressed: () {
                                        if (formKey.currentState.validate()) {
                                          CalenderEventModel calenderEventModel =
                                              CalenderEventModel((data) => data
                                                ..id = widget.event == null
                                                    ? null
                                                    : widget.event.id
                                                ..color = CalenderColorModel(
                                                        (color) => color
                                                          ..primary =
                                                              '#${currentPrimaryColor.value.toRadixString(16).substring(2)}'
                                                          ..secondary =
                                                              '#${currentSecondaryColor.value.toRadixString(16).substring(2)}')
                                                    .toBuilder()
                                                ..title = titleController.text
                                                ..allDay = false
                                                ..end =
                                                    DateFormat("MM/dd/yyyy hh:mm a")
                                                        .parse(endDate.text +
                                                            ' ' +
                                                            endTime.text)
                                                        .toUtc()
                                                        .toIso8601String()
                                                ..endTime = endTime.text
                                                ..start =
                                                    DateFormat("MM/dd/yyyy hh:mm a")
                                                        .parse(startDate.text +
                                                            ' ' +
                                                            startTime.text)
                                                        .toUtc()
                                                        .toIso8601String()
                                                ..startTime = startTime.text
                                                ..meta =
                                                    CalenderMetaModel((meta) => meta
                                                      ..location = location.text
                                                      ..notes = note.text).toBuilder()
                                                ..members = ListBuilder(selectedMembers.map((e) => e.user.id).toList()));

                                          context.read<AddEventBloc>().add(
                                              AddCalenderEvent(
                                                  calenderEventModel:
                                                      calenderEventModel));
                                        } else {
                                          setState(() {
                                            autoValidate = true;
                                          });
                                        }
                                      },
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
