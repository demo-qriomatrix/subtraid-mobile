part of 'calender_bloc.dart';

abstract class CalenderEvent extends Equatable {
  const CalenderEvent();

  @override
  List<Object> get props => [];
}

class CalenderEventsRequested extends CalenderEvent {}

class CalenderDeleteEvent extends CalenderEvent {
  final CalenderEventModel calenderEventModel;

  CalenderDeleteEvent({@required this.calenderEventModel});
}
