part of 'calender_bloc.dart';

abstract class CalenderState extends Equatable {
  const CalenderState();

  @override
  List<Object> get props => [];
}

class CalenderInitial extends CalenderState {}

class CalenderLoadInProgress extends CalenderState {}

class CalenderLoadSuccess extends CalenderState {
  final Map<DateTime, List<CalenderEventModel>> events;

  CalenderLoadSuccess({@required this.events});
}

class CalenderLoadFailure extends CalenderState {}
