import 'dart:async';
import 'dart:convert';

import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/calender/models/calender_event_model.dart';
import 'package:Subtraid/features/calender/models/calender_events_model.dart';
import 'package:Subtraid/features/calender/repository/calender_repository.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'calender_event.dart';
part 'calender_state.dart';

class CalenderBloc extends Bloc<CalenderEvent, CalenderState> {
  CalenderBloc({@required this.calenderRepository, @required this.globalBloc})
      : super(CalenderInitial()) {
    add(CalenderEventsRequested());
  }

  final CalenderRepository calenderRepository;
  final GlobalBloc globalBloc;

  @override
  Stream<CalenderState> mapEventToState(
    CalenderEvent event,
  ) async* {
    if (event is CalenderEventsRequested) {
      yield* _mapCalenderEventsRequestedToState();
    }

    if (event is CalenderDeleteEvent) {
      yield* _mapCalenderDeleteEventToState(event.calenderEventModel);
    }
  }

  Stream<CalenderState> _mapCalenderDeleteEventToState(
      CalenderEventModel calenderEventModel) async* {
    try {
      globalBloc.add(ShowLoadingWidget());
      await calenderRepository.deleteEvent(calenderEventModel);

      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowSuccessSnackBar(
          message: 'Event Removed successfully', event: null));

      add(CalenderEventsRequested());
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<CalenderState> _mapCalenderEventsRequestedToState() async* {
    try {
      yield CalenderLoadInProgress();

      CalenderEventsModel calenderEventsModel =
          await calenderRepository.getCalenderEvents();

      Map<DateTime, List<CalenderEventModel>> _events = Map();

      if (calenderEventsModel.events.isNotEmpty) {
        calenderEventsModel.events.forEach((event) {
          DateTime start = DateTime.parse(
              DateFormat('yyyy-MM-dd').format(DateTime.parse(event.start)));

          if (_events[start] != null) {
            List<CalenderEventModel> events = _events[start];
            events.add(event);
            _events[start] = events;
          } else {
            _events.putIfAbsent(start, () => [event]);
          }
        });
      }

      yield CalenderLoadSuccess(events: _events);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);

      yield CalenderLoadFailure();
    }
  }
}
