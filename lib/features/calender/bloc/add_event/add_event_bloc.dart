import 'dart:async';

import 'package:Subtraid/features/calender/models/calender_event_model.dart';
import 'package:Subtraid/features/calender/repository/calender_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'add_event_event.dart';
part 'add_event_state.dart';

class AddEventBloc extends Bloc<AddEventEvent, AddEventState> {
  AddEventBloc({@required this.calenderRepository}) : super(AddEventInitial());

  final CalenderRepository calenderRepository;

  @override
  Stream<AddEventState> mapEventToState(
    AddEventEvent event,
  ) async* {
    if (event is AddCalenderEvent) {
      yield* _mapAddCalenderEventToState(event.calenderEventModel);
    }
  }

  Stream<AddEventState> _mapAddCalenderEventToState(
      CalenderEventModel calenderEventModel) async* {
    try {
      yield AddEventInProgress();

      if (calenderEventModel.id == null) {
        await calenderRepository.addCalenderEvent(calenderEventModel);
      } else {
        await calenderRepository.updateCalenderEvent(calenderEventModel);
      }

      yield AddEventSuccess();
    } catch (e) {
      print(e);

      yield AddEventFailure();
    }
  }
}
