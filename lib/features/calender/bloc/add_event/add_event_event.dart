part of 'add_event_bloc.dart';

abstract class AddEventEvent extends Equatable {
  const AddEventEvent();

  @override
  List<Object> get props => [];
}

class AddCalenderEvent extends AddEventEvent {
  final CalenderEventModel calenderEventModel;

  AddCalenderEvent({@required this.calenderEventModel});
}
