import 'package:flutter/material.dart';

SnackBar buildErrorSnackBar(String message) {
  return SnackBar(
      backgroundColor: Colors.red,
      content: Text(
        message,
        style: TextStyle(color: Colors.white),
      ));
}

SnackBar buildSuccessSnackBar(String message) {
  return SnackBar(
      backgroundColor: Colors.green,
      content: Text(
        message,
        style: TextStyle(color: Colors.white),
      ));
}
