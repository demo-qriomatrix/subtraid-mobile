import 'package:flutter/material.dart';

Dialog buildLoader() {
  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    child: Center(child: CircularProgressIndicator()),
  );
}
