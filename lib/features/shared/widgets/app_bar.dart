import 'package:Subtraid/features/notifications/bloc/notifications_bloc.dart';
import 'package:Subtraid/features/notifications/view/notifications_screen.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final Widget leading;

  StAppBar({@required this.title, this.leading});

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: leading != null ? leading : null,
      backgroundColor: ColorConfig.primary,
      actions: [
        BlocBuilder<NotificationsBloc, NotificationsState>(
          builder: (context, state) => IconButton(
              icon: Badge(
                  showBadge: state.count > 0,
                  badgeContent: state.count > 0
                      ? Text(
                          state.count.toString(),
                          style: TextStyle(fontSize: 10, color: Colors.white),
                        )
                      : Container(),
                  child: Icon(Icons.notifications)),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => NotificationsScreen(),
                ));
              }),
        ),
        SizedBox(
          width: 20,
        )
      ],
      centerTitle: true,
      title: Text(title),
      elevation: 0,
    );
  }
}

class StAppBarGuest extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final Widget leading;

  StAppBarGuest({@required this.title, this.leading});

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: leading != null ? leading : null,
      backgroundColor: ColorConfig.primary,
      actions: [],
      centerTitle: true,
      title: Text(title),
      elevation: 0,
    );
  }
}
