import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/main/bloc/main_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomBottomNavBar extends StatefulWidget {
  final Function onPressed;

  const CustomBottomNavBar({Key key, this.onPressed}) : super(key: key);

  @override
  _CustomBottomNavBarState createState() => _CustomBottomNavBarState();
}

class _CustomBottomNavBarState extends State<CustomBottomNavBar> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BottomNavBarItem(
            selected: context.watch<MainBloc>().state.selectedIndex == 0,
            icon: LocalImages.folder,
            title: 'Projects',
            onTap: () {
              // if (selectedIndex != 0) {
              //   setState(() {
              //     selectedIndex = 0;
              //   });
              // }
              widget.onPressed(0);
            },
          ),

          BottomNavBarItem(
            selected: context.watch<MainBloc>().state.selectedIndex == 1,
            icon: LocalImages.calenderAlt,
            title: 'Calendar',
            onTap: () {
              // if (selectedIndex != 1) {
              //   setState(() {
              //     selectedIndex = 1;
              //   });
              // }
              widget.onPressed(1);
            },
          ),
          BottomNavBarItem(
            selected: context.watch<MainBloc>().state.selectedIndex == 2,
            icon: LocalImages.contact,
            title: 'Contacts',
            onTap: () {
              // if (selectedIndex != 2) {
              //   setState(() {
              //     selectedIndex = 2;
              //   });
              // }
              widget.onPressed(2);
            },
          ),
          BottomNavBarItem(
            selected: context.watch<MainBloc>().state.selectedIndex == 3,
            icon: LocalImages.mesenger,
            title: 'Messages',
            onTap: () {
              // if (selectedIndex != 3) {
              //   setState(() {
              //     selectedIndex = 3;
              //   });
              // }
              widget.onPressed(3);
            },
          ),
          BottomNavBarItem(
            selected: context.watch<MainBloc>().state.selectedIndex == 4,
            icon: LocalImages.more,
            title: 'More',
            onTap: () {
              // if (selectedIndex != 4) {
              //   setState(() {
              //     selectedIndex = 4;
              //   });
              // }
              widget.onPressed(4);
            },
          ),
          // SizedBox(
          //   width: MediaQuery.of(context).size.width * 0.1,
          // )
        ],
      ),
    );
  }
}

class BottomNavBarItem extends StatelessWidget {
  final String icon;
  final String title;
  final bool selected;
  final Function onTap;

  const BottomNavBarItem(
      {Key key, this.icon, this.title, this.selected, this.onTap})
      : super(key: key);

  static Color selectedColor = ColorConfig.orange;
  static Color unSelectedColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: BoxConstraints(minWidth: 60),
      shape: CircleBorder(),
      materialTapTargetSize: MaterialTapTargetSize.padded,
      padding: EdgeInsets.symmetric(vertical: 14),
      onPressed: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ImageIcon(
            AssetImage(icon),
            color: selected ? selectedColor : unSelectedColor,
            size: 16,
          ),
          SizedBox(
            height: 2,
          ),
          Text(
            title,
            style: TextStyle(
              fontSize: 10,
              color: selected ? selectedColor : unSelectedColor,
            ),
          )
        ],
      ),
    );
  }
}
