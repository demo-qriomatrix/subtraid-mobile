import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class CancelButton extends StatelessWidget {
  final VoidCallback onPress;

  CancelButton({@required this.onPress});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: (MediaQuery.of(context).size.width - 150) * 0.5,
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: ColorConfig.grey3,
        child: Text(
          'Cancel',
          style: TextStyle(fontSize: 12, color: ColorConfig.grey2),
        ),
        onPressed: () {
          onPress();
        },
      ),
    );
  }
}

class SaveButton extends StatelessWidget {
  final String title;
  final VoidCallback callback;

  SaveButton(this.title, this.callback);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: (MediaQuery.of(context).size.width - 150) * 0.5,
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: ColorConfig.primary,
        child: Text(
          title,
          style: TextStyle(fontSize: 12, color: Colors.white),
        ),
        onPressed: () {
          callback();
        },
      ),
    );
  }
}
