import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class DrawerItem extends StatelessWidget {
  final bool selected;
  final VoidCallback callback;
  final String title;
  final String icon;

  DrawerItem({this.selected, this.callback, this.title, this.icon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Material(
        color: selected ? ColorConfig.orange : Colors.white,
        borderRadius: BorderRadius.circular(5),
        child: InkWell(
          borderRadius: BorderRadius.circular(5),
          onTap: () {
            callback();
          },
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color:
                                selected ? Colors.white : ColorConfig.primary,
                            borderRadius: BorderRadius.circular(5)),
                        padding: EdgeInsets.all(5),
                        child: ImageIcon(
                          AssetImage(icon),
                          color: selected ? ColorConfig.primary : Colors.white,
                          size: 18,
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Text(
                      title,
                      style: TextStyle(
                          color: ColorConfig.primary,
                          fontWeight: FontWeight.w500),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
