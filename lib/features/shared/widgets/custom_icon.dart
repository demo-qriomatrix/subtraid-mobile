import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class CustomIcon extends StatelessWidget {
  final String icon;
  final String title;
  final bool selected;
  final Function ontap;

  const CustomIcon(
      {Key key, this.icon, this.title, this.selected = false, this.ontap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: selected ? ColorConfig.primary : ColorConfig.blue1,
      borderRadius: BorderRadius.circular(20),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        onTap: ontap,
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
          padding: EdgeInsets.symmetric(vertical: 15),
          width: 100,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: ImageIcon(
                  AssetImage(icon),
                  color: selected ? Colors.white : ColorConfig.primary,
                  size: 20,
                ),
              ),
              SizedBox(height: 10),
              Text(
                title,
                style: TextStyle(
                    color: selected ? Colors.white : ColorConfig.primary,
                    fontSize: 12),
              )
            ],
          ),
        ),
      ),
    );
  }
}
