import 'dart:async';

import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:flutter/material.dart';

class CountDownTimer extends StatefulWidget {
  final String startDateTime;
  final bool fullFormat;

  CountDownTimer({@required this.startDateTime, this.fullFormat});

  @override
  _CountDownTimerState createState() => _CountDownTimerState();
}

class _CountDownTimerState extends State<CountDownTimer> {
  Timer _timer;
  int _start;
  String time = '';
  bool ready = false;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  void didUpdateWidget(CountDownTimer oldWidget) {
    if (_timer != null) _timer.cancel();
    startTimer();
    super.didUpdateWidget(oldWidget);
  }

  void startTimer() {
    _start = DateTime.now()
        .difference(DateTime.parse(widget.startDateTime))
        .inSeconds;

    time = formatTime(_start);

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          _start = _start + 1;
          time = formatTime(_start);
        },
      ),
    );
  }

  void stopTimer() {
    _timer.cancel();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  String formatTime(int _start) {
    int start = _start.abs();

    if (widget.startDateTime != null) {
      int hours = Duration(seconds: start).inHours;
      int minutes = Duration(seconds: start).inMinutes.remainder(60);
      int seconds = Duration(seconds: start).inSeconds.remainder(60);

      String time;
      if (widget.fullFormat == true) {
        time = "${_start.isNegative ? '-' : ''}" +
            "${hours < 10 ? '0$hours' : '$hours'}" +
            "${minutes < 10 ? ':0$minutes' : ':$minutes'}" +
            "${seconds < 10 ? ':0$seconds' : ':$seconds'}";
      } else {
        time = "${_start.isNegative ? '-' : ''}${hours > 0 ? '$hours:' : ''}" +
            "$minutes" +
            "${seconds < 10 ? ':0$seconds' : ':$seconds'}";
      }

      return time;
    }

    return '';
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ImageIcon(
          AssetImage(LocalImages.stopwatch),
          color: ColorConfig.red1,
          size: 14,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          time,
          style:
              TextStyle(color: ColorConfig.red1, fontWeight: FontWeight.w700),
        ),
      ],
    );
  }
}
