import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

double bottomNavbarHeight = 58;

double getMinHeight(BuildContext context) {
  return MediaQuery.of(context).size.height -
      MediaQuery.of(context).padding.bottom * 0.3 -
      bottomNavbarHeight -
      kToolbarHeight -
      MediaQuery.of(context).padding.top;
}

InputDecoration searchInputDecoration = InputDecoration(
    prefixIcon: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Icon(
        Icons.search,
        color: ColorConfig.primary,
      ),
    ),
    prefixIconConstraints: BoxConstraints(),
    suffixIconConstraints: BoxConstraints(),
    isDense: true,
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: const BorderRadius.all(
        const Radius.circular(50.0),
      ),
    ),
    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    filled: true,
    hintStyle: TextStyle(color: ColorConfig.primary, fontSize: 14),
    hintText: "Search",
    fillColor: ColorConfig.searchBarFillColor);

TextStyle textStyle1 =
    TextStyle(fontWeight: FontWeight.w500, color: Colors.black, fontSize: 12);
TextStyle textStyle2 = TextStyle(
    fontWeight: FontWeight.w500,
    color: Colors.black.withOpacity(0.74),
    fontSize: 14);

InputDecoration profileInputDecoration = InputDecoration(
    fillColor: ColorConfig.blue2,
    filled: true,
    hintStyle: TextStyle(
        color: ColorConfig.blue1, fontSize: 12, fontWeight: FontWeight.w500),
    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10), borderSide: BorderSide.none));

double height2 = 10;
