// GENERATED CODE - DO NOT MODIFY BY HAND

part of api_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ApiResponseModel> _$apiResponseModelSerializer =
    new _$ApiResponseModelSerializer();

class _$ApiResponseModelSerializer
    implements StructuredSerializer<ApiResponseModel> {
  @override
  final Iterable<Type> types = const [ApiResponseModel, _$ApiResponseModel];
  @override
  final String wireName = 'ApiResponseModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ApiResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ApiResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ApiResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ApiResponseModel extends ApiResponseModel {
  @override
  final String message;
  @override
  final int status;

  factory _$ApiResponseModel(
          [void Function(ApiResponseModelBuilder) updates]) =>
      (new ApiResponseModelBuilder()..update(updates)).build();

  _$ApiResponseModel._({this.message, this.status}) : super._();

  @override
  ApiResponseModel rebuild(void Function(ApiResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ApiResponseModelBuilder toBuilder() =>
      new ApiResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ApiResponseModel &&
        message == other.message &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, message.hashCode), status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ApiResponseModel')
          ..add('message', message)
          ..add('status', status))
        .toString();
  }
}

class ApiResponseModelBuilder
    implements Builder<ApiResponseModel, ApiResponseModelBuilder> {
  _$ApiResponseModel _$v;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  ApiResponseModelBuilder();

  ApiResponseModelBuilder get _$this {
    if (_$v != null) {
      _message = _$v.message;
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ApiResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ApiResponseModel;
  }

  @override
  void update(void Function(ApiResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ApiResponseModel build() {
    final _$result =
        _$v ?? new _$ApiResponseModel._(message: message, status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
