library api_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'api_reponse_model.g.dart';

abstract class ApiResponseModel
    implements Built<ApiResponseModel, ApiResponseModelBuilder> {
  @nullable
  String get message;

  @nullable
  int get status;

  ApiResponseModel._();

  factory ApiResponseModel([void Function(ApiResponseModelBuilder) updates]) =
      _$ApiResponseModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ApiResponseModel.serializer, this));
  }

  static ApiResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ApiResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<ApiResponseModel> get serializer =>
      _$apiResponseModelSerializer;
}
