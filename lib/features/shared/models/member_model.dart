library member_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'member_model.g.dart';

abstract class MemberModel implements Built<MemberModel, MemberModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  String get img;

  MemberModel._();

  factory MemberModel([updates(MemberModelBuilder b)]) = _$MemberModel;

  String toJson() {
    return json.encode(serializers.serializeWith(MemberModel.serializer, this));
  }

  static MemberModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        MemberModel.serializer, json.decode(jsonString));
  }

  static Serializer<MemberModel> get serializer => _$memberModelSerializer;
}
