part of 'company_bloc.dart';

abstract class CompanyEvent extends Equatable {
  const CompanyEvent();

  @override
  List<Object> get props => [];
}

class CompanyIndexChanged extends CompanyEvent {
  final int index;

  CompanyIndexChanged({@required this.index});
}
