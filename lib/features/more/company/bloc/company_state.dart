part of 'company_bloc.dart';

abstract class CompanyState extends Equatable {
  final int selectedIndex;

  CompanyState({@required this.selectedIndex});

  @override
  List<Object> get props => [selectedIndex];
}

class CompanyInitial extends CompanyState {
  CompanyInitial({@required int selectedIndex})
      : super(selectedIndex: selectedIndex);

  @override
  List<Object> get props => [selectedIndex];
}
