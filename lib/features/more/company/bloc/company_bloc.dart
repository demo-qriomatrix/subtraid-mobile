import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'company_event.dart';
part 'company_state.dart';

class CompanyBloc extends Bloc<CompanyEvent, CompanyState> {
  final int selectedIndex;

  CompanyBloc({@required this.selectedIndex})
      : super(CompanyInitial(selectedIndex: selectedIndex ?? 0));

  @override
  Stream<CompanyState> mapEventToState(
    CompanyEvent event,
  ) async* {
    if (event is CompanyIndexChanged) {
      yield* _mapCompanyIndexChangedToState(event);
    }
  }

  Stream<CompanyState> _mapCompanyIndexChangedToState(
      CompanyIndexChanged event) async* {
    // if (state is CompanyLoadSuccess)
    //   yield CompanyLoadSuccess(
    //       selectedIndex: event.index,
    //       companyInfoModel: (state as CompanyLoadSuccess).companyInfoModel);
    if (state is CompanyInitial)
      yield CompanyInitial(selectedIndex: event.index);
    // if (state is CompanyLoadFailure)
    //   yield CompanyLoadFailure(selectedIndex: event.index);
  }
}
