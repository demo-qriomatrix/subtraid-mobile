import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/contacts/view/contacts_screen.dart';
import 'package:Subtraid/features/more/company/bloc/company_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/view/crew_screen.dart';
import 'package:Subtraid/features/more/company/features/employee/view/employee_screen.dart';
import 'package:Subtraid/features/more/company/features/pending/view/pending_screen.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/custom_icon.dart';
import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompanyScreen extends StatelessWidget {
  final int selectedIndex;
  final TextEditingController companySearch = TextEditingController();

  CompanyScreen({this.selectedIndex});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CompanyBloc>(
      create: (context) => CompanyBloc(selectedIndex: selectedIndex),
      lazy: false,
      child: BlocBuilder<CompanyBloc, CompanyState>(
        builder: (context, state) => Container(
          color: ColorConfig.primary,
          child: Column(
            children: [
              StAppBar(
                title: 'Company',
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: companySearch,
                  keyboardType: TextInputType.text,
                  decoration: searchInputDecoration.copyWith(
                      hintText: "Search by name",
                      suffixIcon: IconButton(
                          constraints: BoxConstraints(),
                          icon: Icon(
                            Icons.close,
                            size: 20,
                          ),
                          onPressed: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            companySearch.clear();
                          })),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                  child: Stack(children: [
                Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(50))),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: getMinHeight(context),
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: <Widget>[
                              SizedBox(
                                width: 20,
                              ),
                              CustomIcon(
                                icon: LocalImages.employee,
                                title: 'Employee',
                                selected: state.selectedIndex == 0,
                                ontap: () {
                                  context
                                      .read<CompanyBloc>()
                                      .add(CompanyIndexChanged(index: 0));
                                },
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              CustomIcon(
                                icon: LocalImages.userClock,
                                title: 'Pending',
                                selected: state.selectedIndex == 1,
                                ontap: () {
                                  context
                                      .read<CompanyBloc>()
                                      .add(CompanyIndexChanged(index: 1));
                                },
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              CustomIcon(
                                icon: LocalImages.crew,
                                title: 'Crew',
                                selected: state.selectedIndex == 2,
                                ontap: () {
                                  context
                                      .read<CompanyBloc>()
                                      .add(CompanyIndexChanged(index: 2));
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                              child: [
                            EmployeeScreen(
                              companySearch: companySearch,
                            ),
                            PendingScreen(companySearch: companySearch),
                            CrewScreen(companySearch: companySearch)
                          ][state.selectedIndex])
                        ],
                      ),
                    )),
                state.selectedIndex == 0
                    ? Positioned(
                        right: 24,
                        child: RawMaterialButton(
                          padding: EdgeInsets.all(6),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          shape: CircleBorder(),
                          constraints: BoxConstraints(),
                          onPressed: () {
                            if (state.selectedIndex == 0) {
                              Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ContactsScreen(
                                  index: 2,
                                ),
                              ));
                            }
                          },
                          fillColor: ColorConfig.orange,
                          child: Icon(Icons.add, size: 28, color: Colors.white),
                        ),
                      )
                    : Container(),
              ]))
            ],
          ),
        ),
      ),
    );
  }
}
