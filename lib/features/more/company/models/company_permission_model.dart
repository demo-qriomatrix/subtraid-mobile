library company_permission_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'company_permission_model.g.dart';

abstract class CompanyPermissionModel
    implements Built<CompanyPermissionModel, CompanyPermissionModelBuilder> {
  @nullable
  bool get share;

  @nullable
  bool get create;

  @nullable
  bool get read;

  @nullable
  @BuiltValueField(wireName: 'update')
  bool get updatePermission;

  @nullable
  bool get delete;

  CompanyPermissionModel._();

  factory CompanyPermissionModel([updates(CompanyPermissionModelBuilder b)]) =
      _$CompanyPermissionModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompanyPermissionModel.serializer, this));
  }

  static CompanyPermissionModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyPermissionModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyPermissionModel> get serializer =>
      _$companyPermissionModelSerializer;
}
