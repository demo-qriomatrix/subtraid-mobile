// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_permission_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyPermissionModel> _$companyPermissionModelSerializer =
    new _$CompanyPermissionModelSerializer();

class _$CompanyPermissionModelSerializer
    implements StructuredSerializer<CompanyPermissionModel> {
  @override
  final Iterable<Type> types = const [
    CompanyPermissionModel,
    _$CompanyPermissionModel
  ];
  @override
  final String wireName = 'CompanyPermissionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyPermissionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.share != null) {
      result
        ..add('share')
        ..add(serializers.serialize(object.share,
            specifiedType: const FullType(bool)));
    }
    if (object.create != null) {
      result
        ..add('create')
        ..add(serializers.serialize(object.create,
            specifiedType: const FullType(bool)));
    }
    if (object.read != null) {
      result
        ..add('read')
        ..add(serializers.serialize(object.read,
            specifiedType: const FullType(bool)));
    }
    if (object.updatePermission != null) {
      result
        ..add('update')
        ..add(serializers.serialize(object.updatePermission,
            specifiedType: const FullType(bool)));
    }
    if (object.delete != null) {
      result
        ..add('delete')
        ..add(serializers.serialize(object.delete,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  CompanyPermissionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyPermissionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'share':
          result.share = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'create':
          result.create = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'read':
          result.read = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'update':
          result.updatePermission = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'delete':
          result.delete = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyPermissionModel extends CompanyPermissionModel {
  @override
  final bool share;
  @override
  final bool create;
  @override
  final bool read;
  @override
  final bool updatePermission;
  @override
  final bool delete;

  factory _$CompanyPermissionModel(
          [void Function(CompanyPermissionModelBuilder) updates]) =>
      (new CompanyPermissionModelBuilder()..update(updates)).build();

  _$CompanyPermissionModel._(
      {this.share, this.create, this.read, this.updatePermission, this.delete})
      : super._();

  @override
  CompanyPermissionModel rebuild(
          void Function(CompanyPermissionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyPermissionModelBuilder toBuilder() =>
      new CompanyPermissionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyPermissionModel &&
        share == other.share &&
        create == other.create &&
        read == other.read &&
        updatePermission == other.updatePermission &&
        delete == other.delete;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, share.hashCode), create.hashCode), read.hashCode),
            updatePermission.hashCode),
        delete.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyPermissionModel')
          ..add('share', share)
          ..add('create', create)
          ..add('read', read)
          ..add('updatePermission', updatePermission)
          ..add('delete', delete))
        .toString();
  }
}

class CompanyPermissionModelBuilder
    implements Builder<CompanyPermissionModel, CompanyPermissionModelBuilder> {
  _$CompanyPermissionModel _$v;

  bool _share;
  bool get share => _$this._share;
  set share(bool share) => _$this._share = share;

  bool _create;
  bool get create => _$this._create;
  set create(bool create) => _$this._create = create;

  bool _read;
  bool get read => _$this._read;
  set read(bool read) => _$this._read = read;

  bool _updatePermission;
  bool get updatePermission => _$this._updatePermission;
  set updatePermission(bool updatePermission) =>
      _$this._updatePermission = updatePermission;

  bool _delete;
  bool get delete => _$this._delete;
  set delete(bool delete) => _$this._delete = delete;

  CompanyPermissionModelBuilder();

  CompanyPermissionModelBuilder get _$this {
    if (_$v != null) {
      _share = _$v.share;
      _create = _$v.create;
      _read = _$v.read;
      _updatePermission = _$v.updatePermission;
      _delete = _$v.delete;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyPermissionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyPermissionModel;
  }

  @override
  void update(void Function(CompanyPermissionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyPermissionModel build() {
    final _$result = _$v ??
        new _$CompanyPermissionModel._(
            share: share,
            create: create,
            read: read,
            updatePermission: updatePermission,
            delete: delete);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
