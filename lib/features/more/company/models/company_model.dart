library company_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

part 'company_model.g.dart';

abstract class CompanyModel
    implements Built<CompanyModel, CompanyModelBuilder> {
  @nullable
  BuiltList<EmployeeModel> get employees;

  @nullable
  BuiltList<CrewModel> get crews;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  CompanyModel._();

  factory CompanyModel([updates(CompanyModelBuilder b)]) = _$CompanyModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CompanyModel.serializer, this));
  }

  static CompanyModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyModel> get serializer => _$companyModelSerializer;
}
