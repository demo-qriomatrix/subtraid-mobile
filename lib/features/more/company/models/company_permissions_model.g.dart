// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_permissions_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyPermissionsModel> _$companyPermissionsModelSerializer =
    new _$CompanyPermissionsModelSerializer();

class _$CompanyPermissionsModelSerializer
    implements StructuredSerializer<CompanyPermissionsModel> {
  @override
  final Iterable<Type> types = const [
    CompanyPermissionsModel,
    _$CompanyPermissionsModel
  ];
  @override
  final String wireName = 'CompanyPermissionsModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyPermissionsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.chat != null) {
      result
        ..add('chat')
        ..add(serializers.serialize(object.chat,
            specifiedType: const FullType(CompanyPermissionModel)));
    }
    if (object.products != null) {
      result
        ..add('products')
        ..add(serializers.serialize(object.products,
            specifiedType: const FullType(CompanyPermissionModel)));
    }
    if (object.assemblies != null) {
      result
        ..add('assemblies')
        ..add(serializers.serialize(object.assemblies,
            specifiedType: const FullType(CompanyPermissionModel)));
    }
    if (object.admin != null) {
      result
        ..add('admin')
        ..add(serializers.serialize(object.admin,
            specifiedType: const FullType(bool)));
    }
    if (object.estimate != null) {
      result
        ..add('estimate')
        ..add(serializers.serialize(object.estimate,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  CompanyPermissionsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyPermissionsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'chat':
          result.chat.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyPermissionModel))
              as CompanyPermissionModel);
          break;
        case 'products':
          result.products.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyPermissionModel))
              as CompanyPermissionModel);
          break;
        case 'assemblies':
          result.assemblies.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyPermissionModel))
              as CompanyPermissionModel);
          break;
        case 'admin':
          result.admin = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'estimate':
          result.estimate = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyPermissionsModel extends CompanyPermissionsModel {
  @override
  final CompanyPermissionModel chat;
  @override
  final CompanyPermissionModel products;
  @override
  final CompanyPermissionModel assemblies;
  @override
  final bool admin;
  @override
  final bool estimate;

  factory _$CompanyPermissionsModel(
          [void Function(CompanyPermissionsModelBuilder) updates]) =>
      (new CompanyPermissionsModelBuilder()..update(updates)).build();

  _$CompanyPermissionsModel._(
      {this.chat, this.products, this.assemblies, this.admin, this.estimate})
      : super._();

  @override
  CompanyPermissionsModel rebuild(
          void Function(CompanyPermissionsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyPermissionsModelBuilder toBuilder() =>
      new CompanyPermissionsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyPermissionsModel &&
        chat == other.chat &&
        products == other.products &&
        assemblies == other.assemblies &&
        admin == other.admin &&
        estimate == other.estimate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, chat.hashCode), products.hashCode),
                assemblies.hashCode),
            admin.hashCode),
        estimate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyPermissionsModel')
          ..add('chat', chat)
          ..add('products', products)
          ..add('assemblies', assemblies)
          ..add('admin', admin)
          ..add('estimate', estimate))
        .toString();
  }
}

class CompanyPermissionsModelBuilder
    implements
        Builder<CompanyPermissionsModel, CompanyPermissionsModelBuilder> {
  _$CompanyPermissionsModel _$v;

  CompanyPermissionModelBuilder _chat;
  CompanyPermissionModelBuilder get chat =>
      _$this._chat ??= new CompanyPermissionModelBuilder();
  set chat(CompanyPermissionModelBuilder chat) => _$this._chat = chat;

  CompanyPermissionModelBuilder _products;
  CompanyPermissionModelBuilder get products =>
      _$this._products ??= new CompanyPermissionModelBuilder();
  set products(CompanyPermissionModelBuilder products) =>
      _$this._products = products;

  CompanyPermissionModelBuilder _assemblies;
  CompanyPermissionModelBuilder get assemblies =>
      _$this._assemblies ??= new CompanyPermissionModelBuilder();
  set assemblies(CompanyPermissionModelBuilder assemblies) =>
      _$this._assemblies = assemblies;

  bool _admin;
  bool get admin => _$this._admin;
  set admin(bool admin) => _$this._admin = admin;

  bool _estimate;
  bool get estimate => _$this._estimate;
  set estimate(bool estimate) => _$this._estimate = estimate;

  CompanyPermissionsModelBuilder();

  CompanyPermissionsModelBuilder get _$this {
    if (_$v != null) {
      _chat = _$v.chat?.toBuilder();
      _products = _$v.products?.toBuilder();
      _assemblies = _$v.assemblies?.toBuilder();
      _admin = _$v.admin;
      _estimate = _$v.estimate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyPermissionsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyPermissionsModel;
  }

  @override
  void update(void Function(CompanyPermissionsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyPermissionsModel build() {
    _$CompanyPermissionsModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyPermissionsModel._(
              chat: _chat?.build(),
              products: _products?.build(),
              assemblies: _assemblies?.build(),
              admin: admin,
              estimate: estimate);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'chat';
        _chat?.build();
        _$failedField = 'products';
        _products?.build();
        _$failedField = 'assemblies';
        _assemblies?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyPermissionsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
