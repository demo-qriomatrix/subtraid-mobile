// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyModel> _$companyModelSerializer =
    new _$CompanyModelSerializer();

class _$CompanyModelSerializer implements StructuredSerializer<CompanyModel> {
  @override
  final Iterable<Type> types = const [CompanyModel, _$CompanyModel];
  @override
  final String wireName = 'CompanyModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CompanyModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.employees != null) {
      result
        ..add('employees')
        ..add(serializers.serialize(object.employees,
            specifiedType: const FullType(
                BuiltList, const [const FullType(EmployeeModel)])));
    }
    if (object.crews != null) {
      result
        ..add('crews')
        ..add(serializers.serialize(object.crews,
            specifiedType:
                const FullType(BuiltList, const [const FullType(CrewModel)])));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CompanyModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'employees':
          result.employees.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EmployeeModel)]))
              as BuiltList<Object>);
          break;
        case 'crews':
          result.crews.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CrewModel)]))
              as BuiltList<Object>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyModel extends CompanyModel {
  @override
  final BuiltList<EmployeeModel> employees;
  @override
  final BuiltList<CrewModel> crews;
  @override
  final String id;

  factory _$CompanyModel([void Function(CompanyModelBuilder) updates]) =>
      (new CompanyModelBuilder()..update(updates)).build();

  _$CompanyModel._({this.employees, this.crews, this.id}) : super._();

  @override
  CompanyModel rebuild(void Function(CompanyModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyModelBuilder toBuilder() => new CompanyModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyModel &&
        employees == other.employees &&
        crews == other.crews &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, employees.hashCode), crews.hashCode), id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyModel')
          ..add('employees', employees)
          ..add('crews', crews)
          ..add('id', id))
        .toString();
  }
}

class CompanyModelBuilder
    implements Builder<CompanyModel, CompanyModelBuilder> {
  _$CompanyModel _$v;

  ListBuilder<EmployeeModel> _employees;
  ListBuilder<EmployeeModel> get employees =>
      _$this._employees ??= new ListBuilder<EmployeeModel>();
  set employees(ListBuilder<EmployeeModel> employees) =>
      _$this._employees = employees;

  ListBuilder<CrewModel> _crews;
  ListBuilder<CrewModel> get crews =>
      _$this._crews ??= new ListBuilder<CrewModel>();
  set crews(ListBuilder<CrewModel> crews) => _$this._crews = crews;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  CompanyModelBuilder();

  CompanyModelBuilder get _$this {
    if (_$v != null) {
      _employees = _$v.employees?.toBuilder();
      _crews = _$v.crews?.toBuilder();
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyModel;
  }

  @override
  void update(void Function(CompanyModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyModel build() {
    _$CompanyModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyModel._(
              employees: _employees?.build(), crews: _crews?.build(), id: id);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'employees';
        _employees?.build();
        _$failedField = 'crews';
        _crews?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
