library company_info_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'company_model.dart';

part 'company_info_model.g.dart';

abstract class CompanyInfoModel
    implements Built<CompanyInfoModel, CompanyInfoModelBuilder> {
  @nullable
  String get name;

  @nullable
  CompanyModel get company;

  CompanyInfoModel._();

  factory CompanyInfoModel([updates(CompanyInfoModelBuilder b)]) =
      _$CompanyInfoModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CompanyInfoModel.serializer, this));
  }

  static CompanyInfoModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyInfoModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyInfoModel> get serializer =>
      _$companyInfoModelSerializer;
}
