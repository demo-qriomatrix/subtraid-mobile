library company_response_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'company_info_model.dart';

part 'company_response_model.g.dart';

abstract class CompanyResponseModel
    implements Built<CompanyResponseModel, CompanyResponseModelBuilder> {
  @nullable
  CompanyInfoModel get companyInfo;

  CompanyResponseModel._();

  factory CompanyResponseModel([updates(CompanyResponseModelBuilder b)]) =
      _$CompanyResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompanyResponseModel.serializer, this));
  }

  static CompanyResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyResponseModel> get serializer =>
      _$companyResponseModelSerializer;
}
