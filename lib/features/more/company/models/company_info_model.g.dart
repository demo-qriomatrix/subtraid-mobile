// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_info_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyInfoModel> _$companyInfoModelSerializer =
    new _$CompanyInfoModelSerializer();

class _$CompanyInfoModelSerializer
    implements StructuredSerializer<CompanyInfoModel> {
  @override
  final Iterable<Type> types = const [CompanyInfoModel, _$CompanyInfoModel];
  @override
  final String wireName = 'CompanyInfoModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CompanyInfoModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.company != null) {
      result
        ..add('company')
        ..add(serializers.serialize(object.company,
            specifiedType: const FullType(CompanyModel)));
    }
    return result;
  }

  @override
  CompanyInfoModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyInfoModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'company':
          result.company.replace(serializers.deserialize(value,
              specifiedType: const FullType(CompanyModel)) as CompanyModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyInfoModel extends CompanyInfoModel {
  @override
  final String name;
  @override
  final CompanyModel company;

  factory _$CompanyInfoModel(
          [void Function(CompanyInfoModelBuilder) updates]) =>
      (new CompanyInfoModelBuilder()..update(updates)).build();

  _$CompanyInfoModel._({this.name, this.company}) : super._();

  @override
  CompanyInfoModel rebuild(void Function(CompanyInfoModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyInfoModelBuilder toBuilder() =>
      new CompanyInfoModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyInfoModel &&
        name == other.name &&
        company == other.company;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), company.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyInfoModel')
          ..add('name', name)
          ..add('company', company))
        .toString();
  }
}

class CompanyInfoModelBuilder
    implements Builder<CompanyInfoModel, CompanyInfoModelBuilder> {
  _$CompanyInfoModel _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  CompanyModelBuilder _company;
  CompanyModelBuilder get company =>
      _$this._company ??= new CompanyModelBuilder();
  set company(CompanyModelBuilder company) => _$this._company = company;

  CompanyInfoModelBuilder();

  CompanyInfoModelBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _company = _$v.company?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyInfoModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyInfoModel;
  }

  @override
  void update(void Function(CompanyInfoModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyInfoModel build() {
    _$CompanyInfoModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyInfoModel._(name: name, company: _company?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'company';
        _company?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyInfoModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
