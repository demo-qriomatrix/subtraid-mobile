library company_permissions_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'company_permission_model.dart';

part 'company_permissions_model.g.dart';

abstract class CompanyPermissionsModel
    implements Built<CompanyPermissionsModel, CompanyPermissionsModelBuilder> {
  @nullable
  CompanyPermissionModel get chat;

  @nullable
  CompanyPermissionModel get products;

  @nullable
  CompanyPermissionModel get assemblies;

  @nullable
  bool get admin;

  @nullable
  bool get estimate;

  CompanyPermissionsModel._();

  factory CompanyPermissionsModel([updates(CompanyPermissionsModelBuilder b)]) =
      _$CompanyPermissionsModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompanyPermissionsModel.serializer, this));
  }

  static CompanyPermissionsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyPermissionsModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyPermissionsModel> get serializer =>
      _$companyPermissionsModelSerializer;
}
