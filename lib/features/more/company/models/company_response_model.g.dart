// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyResponseModel> _$companyResponseModelSerializer =
    new _$CompanyResponseModelSerializer();

class _$CompanyResponseModelSerializer
    implements StructuredSerializer<CompanyResponseModel> {
  @override
  final Iterable<Type> types = const [
    CompanyResponseModel,
    _$CompanyResponseModel
  ];
  @override
  final String wireName = 'CompanyResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.companyInfo != null) {
      result
        ..add('companyInfo')
        ..add(serializers.serialize(object.companyInfo,
            specifiedType: const FullType(CompanyInfoModel)));
    }
    return result;
  }

  @override
  CompanyResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'companyInfo':
          result.companyInfo.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyInfoModel))
              as CompanyInfoModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyResponseModel extends CompanyResponseModel {
  @override
  final CompanyInfoModel companyInfo;

  factory _$CompanyResponseModel(
          [void Function(CompanyResponseModelBuilder) updates]) =>
      (new CompanyResponseModelBuilder()..update(updates)).build();

  _$CompanyResponseModel._({this.companyInfo}) : super._();

  @override
  CompanyResponseModel rebuild(
          void Function(CompanyResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyResponseModelBuilder toBuilder() =>
      new CompanyResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyResponseModel && companyInfo == other.companyInfo;
  }

  @override
  int get hashCode {
    return $jf($jc(0, companyInfo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyResponseModel')
          ..add('companyInfo', companyInfo))
        .toString();
  }
}

class CompanyResponseModelBuilder
    implements Builder<CompanyResponseModel, CompanyResponseModelBuilder> {
  _$CompanyResponseModel _$v;

  CompanyInfoModelBuilder _companyInfo;
  CompanyInfoModelBuilder get companyInfo =>
      _$this._companyInfo ??= new CompanyInfoModelBuilder();
  set companyInfo(CompanyInfoModelBuilder companyInfo) =>
      _$this._companyInfo = companyInfo;

  CompanyResponseModelBuilder();

  CompanyResponseModelBuilder get _$this {
    if (_$v != null) {
      _companyInfo = _$v.companyInfo?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyResponseModel;
  }

  @override
  void update(void Function(CompanyResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyResponseModel build() {
    _$CompanyResponseModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyResponseModel._(companyInfo: _companyInfo?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'companyInfo';
        _companyInfo?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
