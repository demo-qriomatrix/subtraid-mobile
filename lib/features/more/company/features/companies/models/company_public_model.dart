library company_public_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'company_public_model.g.dart';

abstract class CompanyPublicModel
    implements Built<CompanyPublicModel, CompanyPublicModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  // @nullable
  // String get email;

  // @nullable
  // String get phone;

  // @nullable
  // List<Location> get location;

  // @nullable
  // CompanyModel get company;

  // @nullable
  // String get dateCreated;

  CompanyPublicModel._();

  factory CompanyPublicModel([updates(CompanyPublicModelBuilder b)]) =
      _$CompanyPublicModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CompanyPublicModel.serializer, this));
  }

  static CompanyPublicModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyPublicModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyPublicModel> get serializer =>
      _$companyPublicModelSerializer;
}
