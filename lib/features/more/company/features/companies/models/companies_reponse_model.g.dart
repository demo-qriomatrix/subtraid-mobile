// GENERATED CODE - DO NOT MODIFY BY HAND

part of companies_reponse_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompaniesResponseModel> _$companiesResponseModelSerializer =
    new _$CompaniesResponseModelSerializer();

class _$CompaniesResponseModelSerializer
    implements StructuredSerializer<CompaniesResponseModel> {
  @override
  final Iterable<Type> types = const [
    CompaniesResponseModel,
    _$CompaniesResponseModel
  ];
  @override
  final String wireName = 'CompaniesResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompaniesResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.result != null) {
      result
        ..add('result')
        ..add(serializers.serialize(object.result,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CompanyPublicModel)])));
    }
    return result;
  }

  @override
  CompaniesResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompaniesResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'result':
          result.result.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CompanyPublicModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$CompaniesResponseModel extends CompaniesResponseModel {
  @override
  final BuiltList<CompanyPublicModel> result;

  factory _$CompaniesResponseModel(
          [void Function(CompaniesResponseModelBuilder) updates]) =>
      (new CompaniesResponseModelBuilder()..update(updates)).build();

  _$CompaniesResponseModel._({this.result}) : super._();

  @override
  CompaniesResponseModel rebuild(
          void Function(CompaniesResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompaniesResponseModelBuilder toBuilder() =>
      new CompaniesResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompaniesResponseModel && result == other.result;
  }

  @override
  int get hashCode {
    return $jf($jc(0, result.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompaniesResponseModel')
          ..add('result', result))
        .toString();
  }
}

class CompaniesResponseModelBuilder
    implements Builder<CompaniesResponseModel, CompaniesResponseModelBuilder> {
  _$CompaniesResponseModel _$v;

  ListBuilder<CompanyPublicModel> _result;
  ListBuilder<CompanyPublicModel> get result =>
      _$this._result ??= new ListBuilder<CompanyPublicModel>();
  set result(ListBuilder<CompanyPublicModel> result) => _$this._result = result;

  CompaniesResponseModelBuilder();

  CompaniesResponseModelBuilder get _$this {
    if (_$v != null) {
      _result = _$v.result?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompaniesResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompaniesResponseModel;
  }

  @override
  void update(void Function(CompaniesResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompaniesResponseModel build() {
    _$CompaniesResponseModel _$result;
    try {
      _$result =
          _$v ?? new _$CompaniesResponseModel._(result: _result?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'result';
        _result?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompaniesResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
