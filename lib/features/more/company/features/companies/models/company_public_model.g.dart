// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_public_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyPublicModel> _$companyPublicModelSerializer =
    new _$CompanyPublicModelSerializer();

class _$CompanyPublicModelSerializer
    implements StructuredSerializer<CompanyPublicModel> {
  @override
  final Iterable<Type> types = const [CompanyPublicModel, _$CompanyPublicModel];
  @override
  final String wireName = 'CompanyPublicModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CompanyPublicModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CompanyPublicModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyPublicModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyPublicModel extends CompanyPublicModel {
  @override
  final String id;
  @override
  final String name;

  factory _$CompanyPublicModel(
          [void Function(CompanyPublicModelBuilder) updates]) =>
      (new CompanyPublicModelBuilder()..update(updates)).build();

  _$CompanyPublicModel._({this.id, this.name}) : super._();

  @override
  CompanyPublicModel rebuild(
          void Function(CompanyPublicModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyPublicModelBuilder toBuilder() =>
      new CompanyPublicModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyPublicModel && id == other.id && name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyPublicModel')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class CompanyPublicModelBuilder
    implements Builder<CompanyPublicModel, CompanyPublicModelBuilder> {
  _$CompanyPublicModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  CompanyPublicModelBuilder();

  CompanyPublicModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyPublicModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyPublicModel;
  }

  @override
  void update(void Function(CompanyPublicModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyPublicModel build() {
    final _$result = _$v ?? new _$CompanyPublicModel._(id: id, name: name);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
