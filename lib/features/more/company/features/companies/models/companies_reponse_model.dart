library companies_reponse_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'company_public_model.dart';

part 'companies_reponse_model.g.dart';

abstract class CompaniesResponseModel
    implements Built<CompaniesResponseModel, CompaniesResponseModelBuilder> {
  @nullable
  BuiltList<CompanyPublicModel> get result;

  CompaniesResponseModel._();

  factory CompaniesResponseModel([updates(CompaniesResponseModelBuilder b)]) =
      _$CompaniesResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompaniesResponseModel.serializer, this));
  }

  static CompaniesResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompaniesResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompaniesResponseModel> get serializer =>
      _$companiesResponseModelSerializer;
}
