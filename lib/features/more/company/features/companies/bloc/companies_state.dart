part of 'companies_bloc.dart';

abstract class CompaniesState extends Equatable {
  const CompaniesState();

  @override
  List<Object> get props => [];
}

class CompaniesInitial extends CompaniesState {}

class CompaniesLoadSuccess extends CompaniesState {
  final List<CompanyPublicModel> companies;

  CompaniesLoadSuccess({@required this.companies});
}
