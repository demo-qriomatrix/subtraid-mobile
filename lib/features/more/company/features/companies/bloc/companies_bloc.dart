import 'dart:async';

import 'package:Subtraid/features/more/company/features/companies/models/companies_reponse_model.dart';
import 'package:Subtraid/features/more/company/features/companies/models/company_public_model.dart';
import 'package:Subtraid/features/more/company/features/companies/repository/companies_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'companies_event.dart';
part 'companies_state.dart';

class CompaniesBloc extends Bloc<CompaniesEvent, CompaniesState> {
  CompaniesBloc({@required this.companiesRepository})
      : super(CompaniesInitial()) {
    add(CompaniesRequested());
  }

  final CompaniesRepository companiesRepository;

  @override
  Stream<CompaniesState> mapEventToState(
    CompaniesEvent event,
  ) async* {
    if (event is CompaniesRequested) {
      yield* _mapCompaniesRequestedToState();
    }
  }

  Stream<CompaniesState> _mapCompaniesRequestedToState() async* {
    try {
      CompaniesResponseModel companiesResponseModel =
          await companiesRepository.getCompanies();

      yield CompaniesLoadSuccess(
          companies: companiesResponseModel.result.toList());
    } catch (e) {}
  }
}
