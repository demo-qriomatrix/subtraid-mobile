import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/more/company/features/companies/models/companies_reponse_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class CompaniesRepository {
  final HttpClient httpClient;

  CompaniesRepository({@required this.httpClient});

  Future<CompaniesResponseModel> getCompanies() async {
    Response response = await httpClient.dio.get(EndpointConfig.companies);

    return CompaniesResponseModel.fromJson(json.encode(response.data));
  }
}
