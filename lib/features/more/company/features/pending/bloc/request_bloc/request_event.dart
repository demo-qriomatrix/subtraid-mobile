part of 'request_bloc.dart';

abstract class RequestEvent extends Equatable {
  const RequestEvent();

  @override
  List<Object> get props => [];
}

class RequestUpdate extends RequestEvent {
  final bool accept;
  final String requestId;

  RequestUpdate({@required this.accept, @required this.requestId});
}
