import 'dart:async';

import 'package:Subtraid/features/projects/features/project/feataures/collaborators/repository/collaborators_repository.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'request_event.dart';
part 'request_state.dart';

class RequestBloc extends Bloc<RequestEvent, RequestState> {
  RequestBloc({@required this.userRequestsRepository})
      : super(RequestInitial());

  final UserRequestsRepository userRequestsRepository;

  @override
  Stream<RequestState> mapEventToState(
    RequestEvent event,
  ) async* {
    if (event is RequestUpdate) {
      yield* _mapRequestUpdateToState(event);
    }
  }

  Stream<RequestState> _mapRequestUpdateToState(RequestUpdate event) async* {
    yield RequestUpdateInProgress();

    try {
      ApiResponseModel responseModel = await userRequestsRepository
          .updateEmployeement(event.requestId, event.accept);

      yield RequestUpdateSuccess(message: responseModel.message);
    } catch (e) {
      yield RequestUpdateFailure();
    }
  }
}
