part of 'request_bloc.dart';

abstract class RequestState extends Equatable {
  const RequestState();

  @override
  List<Object> get props => [];
}

class RequestInitial extends RequestState {}

class RequestUpdateInProgress extends RequestState {}

class RequestUpdateSuccess extends RequestState {
  final String message;
  RequestUpdateSuccess({@required this.message});
}

class RequestUpdateFailure extends RequestState {}
