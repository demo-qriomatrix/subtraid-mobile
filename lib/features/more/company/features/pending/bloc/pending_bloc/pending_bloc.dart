import 'dart:async';

import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/requests_response_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/repository/collaborators_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'pending_event.dart';
part 'pending_state.dart';

class PendingBloc extends Bloc<PendingEvent, PendingState> {
  PendingBloc({@required this.userRequestsRepository})
      : super(PendingInitial()) {
    add(PendingRequested());
  }

  final UserRequestsRepository userRequestsRepository;

  @override
  Stream<PendingState> mapEventToState(
    PendingEvent event,
  ) async* {
    if (event is PendingRequested) {
      yield* _mapPendingRequestedToState();
    }
  }

  Stream<PendingState> _mapPendingRequestedToState() async* {
    yield PendingLoadInProgress();

    try {
      RequestsResponseModel requestsResponseModel =
          await userRequestsRepository.getEmployement();

      yield PendingLoadSuccess(
          requests: requestsResponseModel.requests.toList());
    } catch (e) {
      print(e);

      yield PendingLoadFailure();
    }
  }
}
