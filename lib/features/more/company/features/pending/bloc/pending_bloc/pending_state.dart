part of 'pending_bloc.dart';

abstract class PendingState extends Equatable {
  const PendingState();

  @override
  List<Object> get props => [];
}

class PendingInitial extends PendingState {}

class PendingLoadInProgress extends PendingState {}

class PendingLoadSuccess extends PendingState {
  final List<RequestsModel> requests;
  PendingLoadSuccess({@required this.requests});
}

class PendingLoadFailure extends PendingState {}
