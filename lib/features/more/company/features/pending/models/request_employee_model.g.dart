// GENERATED CODE - DO NOT MODIFY BY HAND

part of request_employee_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestEmployeeModel> _$requestEmployeeModelSerializer =
    new _$RequestEmployeeModelSerializer();

class _$RequestEmployeeModelSerializer
    implements StructuredSerializer<RequestEmployeeModel> {
  @override
  final Iterable<Type> types = const [
    RequestEmployeeModel,
    _$RequestEmployeeModel
  ];
  @override
  final String wireName = 'RequestEmployeeModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, RequestEmployeeModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(String)));
    }
    if (object.role != null) {
      result
        ..add('role')
        ..add(serializers.serialize(object.role,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.rate != null) {
      result
        ..add('rate')
        ..add(serializers.serialize(object.rate,
            specifiedType: const FullType(int)));
    }
    if (object.contractDetails != null) {
      result
        ..add('contractDetails')
        ..add(serializers.serialize(object.contractDetails,
            specifiedType: const FullType(ContractDetailsModel)));
    }
    if (object.permissions != null) {
      result
        ..add('permissions')
        ..add(serializers.serialize(object.permissions,
            specifiedType: const FullType(CompanyPermissionsModel)));
    }
    if (object.company != null) {
      result
        ..add('company')
        ..add(serializers.serialize(object.company,
            specifiedType: const FullType(String)));
    }
    if (object.companyName != null) {
      result
        ..add('companyName')
        ..add(serializers.serialize(object.companyName,
            specifiedType: const FullType(String)));
    }
    if (object.username != null) {
      result
        ..add('username')
        ..add(serializers.serialize(object.username,
            specifiedType: const FullType(String)));
    }
    if (object.userImg != null) {
      result
        ..add('userImg')
        ..add(serializers.serialize(object.userImg,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RequestEmployeeModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestEmployeeModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rate':
          result.rate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'contractDetails':
          result.contractDetails.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ContractDetailsModel))
              as ContractDetailsModel);
          break;
        case 'permissions':
          result.permissions.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyPermissionsModel))
              as CompanyPermissionsModel);
          break;
        case 'company':
          result.company = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'companyName':
          result.companyName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'username':
          result.username = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userImg':
          result.userImg = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RequestEmployeeModel extends RequestEmployeeModel {
  @override
  final String user;
  @override
  final String role;
  @override
  final String type;
  @override
  final int rate;
  @override
  final ContractDetailsModel contractDetails;
  @override
  final CompanyPermissionsModel permissions;
  @override
  final String company;
  @override
  final String companyName;
  @override
  final String username;
  @override
  final String userImg;

  factory _$RequestEmployeeModel(
          [void Function(RequestEmployeeModelBuilder) updates]) =>
      (new RequestEmployeeModelBuilder()..update(updates)).build();

  _$RequestEmployeeModel._(
      {this.user,
      this.role,
      this.type,
      this.rate,
      this.contractDetails,
      this.permissions,
      this.company,
      this.companyName,
      this.username,
      this.userImg})
      : super._();

  @override
  RequestEmployeeModel rebuild(
          void Function(RequestEmployeeModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestEmployeeModelBuilder toBuilder() =>
      new RequestEmployeeModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestEmployeeModel &&
        user == other.user &&
        role == other.role &&
        type == other.type &&
        rate == other.rate &&
        contractDetails == other.contractDetails &&
        permissions == other.permissions &&
        company == other.company &&
        companyName == other.companyName &&
        username == other.username &&
        userImg == other.userImg;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc($jc(0, user.hashCode), role.hashCode),
                                    type.hashCode),
                                rate.hashCode),
                            contractDetails.hashCode),
                        permissions.hashCode),
                    company.hashCode),
                companyName.hashCode),
            username.hashCode),
        userImg.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestEmployeeModel')
          ..add('user', user)
          ..add('role', role)
          ..add('type', type)
          ..add('rate', rate)
          ..add('contractDetails', contractDetails)
          ..add('permissions', permissions)
          ..add('company', company)
          ..add('companyName', companyName)
          ..add('username', username)
          ..add('userImg', userImg))
        .toString();
  }
}

class RequestEmployeeModelBuilder
    implements Builder<RequestEmployeeModel, RequestEmployeeModelBuilder> {
  _$RequestEmployeeModel _$v;

  String _user;
  String get user => _$this._user;
  set user(String user) => _$this._user = user;

  String _role;
  String get role => _$this._role;
  set role(String role) => _$this._role = role;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  int _rate;
  int get rate => _$this._rate;
  set rate(int rate) => _$this._rate = rate;

  ContractDetailsModelBuilder _contractDetails;
  ContractDetailsModelBuilder get contractDetails =>
      _$this._contractDetails ??= new ContractDetailsModelBuilder();
  set contractDetails(ContractDetailsModelBuilder contractDetails) =>
      _$this._contractDetails = contractDetails;

  CompanyPermissionsModelBuilder _permissions;
  CompanyPermissionsModelBuilder get permissions =>
      _$this._permissions ??= new CompanyPermissionsModelBuilder();
  set permissions(CompanyPermissionsModelBuilder permissions) =>
      _$this._permissions = permissions;

  String _company;
  String get company => _$this._company;
  set company(String company) => _$this._company = company;

  String _companyName;
  String get companyName => _$this._companyName;
  set companyName(String companyName) => _$this._companyName = companyName;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  String _userImg;
  String get userImg => _$this._userImg;
  set userImg(String userImg) => _$this._userImg = userImg;

  RequestEmployeeModelBuilder();

  RequestEmployeeModelBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user;
      _role = _$v.role;
      _type = _$v.type;
      _rate = _$v.rate;
      _contractDetails = _$v.contractDetails?.toBuilder();
      _permissions = _$v.permissions?.toBuilder();
      _company = _$v.company;
      _companyName = _$v.companyName;
      _username = _$v.username;
      _userImg = _$v.userImg;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestEmployeeModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestEmployeeModel;
  }

  @override
  void update(void Function(RequestEmployeeModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestEmployeeModel build() {
    _$RequestEmployeeModel _$result;
    try {
      _$result = _$v ??
          new _$RequestEmployeeModel._(
              user: user,
              role: role,
              type: type,
              rate: rate,
              contractDetails: _contractDetails?.build(),
              permissions: _permissions?.build(),
              company: company,
              companyName: companyName,
              username: username,
              userImg: userImg);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'contractDetails';
        _contractDetails?.build();
        _$failedField = 'permissions';
        _permissions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'RequestEmployeeModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
