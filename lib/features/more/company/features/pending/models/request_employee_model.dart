library request_employee_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/contacts/models/contract_details_model.dart';
import 'package:Subtraid/features/more/company/models/company_permissions_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_employee_model.g.dart';

abstract class RequestEmployeeModel
    implements Built<RequestEmployeeModel, RequestEmployeeModelBuilder> {
  @nullable
  String get user;

  @nullable
  String get role;

  @nullable
  String get type;

  @nullable
  int get rate;

  @nullable
  ContractDetailsModel get contractDetails;

  @nullable
  CompanyPermissionsModel get permissions;

  @nullable
  String get company;

  @nullable
  String get companyName;

  @nullable
  String get username;

  @nullable
  String get userImg;

  RequestEmployeeModel._();

  factory RequestEmployeeModel([updates(RequestEmployeeModelBuilder b)]) =
      _$RequestEmployeeModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(RequestEmployeeModel.serializer, this));
  }

  static RequestEmployeeModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestEmployeeModel.serializer, json.decode(jsonString));
  }

  static Serializer<RequestEmployeeModel> get serializer =>
      _$requestEmployeeModelSerializer;
}
