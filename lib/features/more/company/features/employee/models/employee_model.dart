library employee_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/contacts/models/contract_details_model.dart';
import 'package:Subtraid/features/more/company/models/company_permissions_model.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'employee_model.g.dart';

abstract class EmployeeModel
    implements Built<EmployeeModel, EmployeeModelBuilder> {
  @nullable
  CompanyPermissionsModel get permissions;

  @nullable
  String get role;

  @nullable
  String get type;

  @nullable
  int get rate;

  @nullable
  String get crew;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  ContractDetailsModel get contractDetails;

  @nullable
  MemberModel get user;

  EmployeeModel._();

  factory EmployeeModel([updates(EmployeeModelBuilder b)]) = _$EmployeeModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(EmployeeModel.serializer, this));
  }

  static EmployeeModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        EmployeeModel.serializer, json.decode(jsonString));
  }

  static Serializer<EmployeeModel> get serializer => _$employeeModelSerializer;
}
