// GENERATED CODE - DO NOT MODIFY BY HAND

part of employee_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<EmployeeModel> _$employeeModelSerializer =
    new _$EmployeeModelSerializer();

class _$EmployeeModelSerializer implements StructuredSerializer<EmployeeModel> {
  @override
  final Iterable<Type> types = const [EmployeeModel, _$EmployeeModel];
  @override
  final String wireName = 'EmployeeModel';

  @override
  Iterable<Object> serialize(Serializers serializers, EmployeeModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.permissions != null) {
      result
        ..add('permissions')
        ..add(serializers.serialize(object.permissions,
            specifiedType: const FullType(CompanyPermissionsModel)));
    }
    if (object.role != null) {
      result
        ..add('role')
        ..add(serializers.serialize(object.role,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.rate != null) {
      result
        ..add('rate')
        ..add(serializers.serialize(object.rate,
            specifiedType: const FullType(int)));
    }
    if (object.crew != null) {
      result
        ..add('crew')
        ..add(serializers.serialize(object.crew,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.contractDetails != null) {
      result
        ..add('contractDetails')
        ..add(serializers.serialize(object.contractDetails,
            specifiedType: const FullType(ContractDetailsModel)));
    }
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(MemberModel)));
    }
    return result;
  }

  @override
  EmployeeModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EmployeeModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'permissions':
          result.permissions.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyPermissionsModel))
              as CompanyPermissionsModel);
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rate':
          result.rate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'crew':
          result.crew = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'contractDetails':
          result.contractDetails.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ContractDetailsModel))
              as ContractDetailsModel);
          break;
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(MemberModel)) as MemberModel);
          break;
      }
    }

    return result.build();
  }
}

class _$EmployeeModel extends EmployeeModel {
  @override
  final CompanyPermissionsModel permissions;
  @override
  final String role;
  @override
  final String type;
  @override
  final int rate;
  @override
  final String crew;
  @override
  final String id;
  @override
  final ContractDetailsModel contractDetails;
  @override
  final MemberModel user;

  factory _$EmployeeModel([void Function(EmployeeModelBuilder) updates]) =>
      (new EmployeeModelBuilder()..update(updates)).build();

  _$EmployeeModel._(
      {this.permissions,
      this.role,
      this.type,
      this.rate,
      this.crew,
      this.id,
      this.contractDetails,
      this.user})
      : super._();

  @override
  EmployeeModel rebuild(void Function(EmployeeModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EmployeeModelBuilder toBuilder() => new EmployeeModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EmployeeModel &&
        permissions == other.permissions &&
        role == other.role &&
        type == other.type &&
        rate == other.rate &&
        crew == other.crew &&
        id == other.id &&
        contractDetails == other.contractDetails &&
        user == other.user;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, permissions.hashCode), role.hashCode),
                            type.hashCode),
                        rate.hashCode),
                    crew.hashCode),
                id.hashCode),
            contractDetails.hashCode),
        user.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('EmployeeModel')
          ..add('permissions', permissions)
          ..add('role', role)
          ..add('type', type)
          ..add('rate', rate)
          ..add('crew', crew)
          ..add('id', id)
          ..add('contractDetails', contractDetails)
          ..add('user', user))
        .toString();
  }
}

class EmployeeModelBuilder
    implements Builder<EmployeeModel, EmployeeModelBuilder> {
  _$EmployeeModel _$v;

  CompanyPermissionsModelBuilder _permissions;
  CompanyPermissionsModelBuilder get permissions =>
      _$this._permissions ??= new CompanyPermissionsModelBuilder();
  set permissions(CompanyPermissionsModelBuilder permissions) =>
      _$this._permissions = permissions;

  String _role;
  String get role => _$this._role;
  set role(String role) => _$this._role = role;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  int _rate;
  int get rate => _$this._rate;
  set rate(int rate) => _$this._rate = rate;

  String _crew;
  String get crew => _$this._crew;
  set crew(String crew) => _$this._crew = crew;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ContractDetailsModelBuilder _contractDetails;
  ContractDetailsModelBuilder get contractDetails =>
      _$this._contractDetails ??= new ContractDetailsModelBuilder();
  set contractDetails(ContractDetailsModelBuilder contractDetails) =>
      _$this._contractDetails = contractDetails;

  MemberModelBuilder _user;
  MemberModelBuilder get user => _$this._user ??= new MemberModelBuilder();
  set user(MemberModelBuilder user) => _$this._user = user;

  EmployeeModelBuilder();

  EmployeeModelBuilder get _$this {
    if (_$v != null) {
      _permissions = _$v.permissions?.toBuilder();
      _role = _$v.role;
      _type = _$v.type;
      _rate = _$v.rate;
      _crew = _$v.crew;
      _id = _$v.id;
      _contractDetails = _$v.contractDetails?.toBuilder();
      _user = _$v.user?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EmployeeModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$EmployeeModel;
  }

  @override
  void update(void Function(EmployeeModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$EmployeeModel build() {
    _$EmployeeModel _$result;
    try {
      _$result = _$v ??
          new _$EmployeeModel._(
              permissions: _permissions?.build(),
              role: role,
              type: type,
              rate: rate,
              crew: crew,
              id: id,
              contractDetails: _contractDetails?.build(),
              user: _user?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'permissions';
        _permissions?.build();

        _$failedField = 'contractDetails';
        _contractDetails?.build();
        _$failedField = 'user';
        _user?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'EmployeeModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
