import 'dart:convert';

import 'package:Subtraid/features/more/company/models/company_response_model.dart';
import 'package:dio/dio.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class EmployeeRepository {
  final HttpClient httpClient;

  EmployeeRepository({@required this.httpClient});

  Future<CompanyResponseModel> getEmployees() async {
    Response response =
        await httpClient.dio.get(EndpointConfig.userEmployerEmployees);

    return CompanyResponseModel.fromJson(json.encode(response.data));
  }
}
