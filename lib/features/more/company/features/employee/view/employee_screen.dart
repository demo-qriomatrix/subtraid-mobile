import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/more/company/features/employee/bloc/employee_bloc.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:Subtraid/features/shared/widgets/custom_expansion_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/extensions/string_extension.dart';
import 'package:intl/intl.dart';

class EmployeeScreen extends StatefulWidget {
  final TextEditingController companySearch;

  EmployeeScreen({@required this.companySearch});

  @override
  _EmployeeScreenState createState() => _EmployeeScreenState();
}

class _EmployeeScreenState extends State<EmployeeScreen> {
  String search = '';

  @override
  void initState() {
    super.initState();
    search = widget.companySearch.text;
    widget.companySearch.addListener(onChange);
  }

  onChange() {
    setState(() {
      search = widget.companySearch.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EmployeeBloc>(
      create: (context) => EmployeeBloc(employeeRepository: sl()),
      lazy: false,
      child: BlocBuilder<EmployeeBloc, EmployeeState>(
        builder: (context, state) {
          if (state is EmployeeLoadInProgress) {
            return Center(child: CircularProgressIndicator());
          }

          if (state is EmployeeLoadSuccess) {
            if (state.companyInfo?.company?.employees != null &&
                state.companyInfo.company.employees.isNotEmpty) {
              return ListView(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  children: state.companyInfo.company.employees
                      .where((element) => search != ''
                          ? element.user.name
                              .toLowerCase()
                              .contains(search.toLowerCase())
                          : true)
                      .map((e) => Container(
                            margin: EdgeInsets.only(
                                bottom: 20, left: 20, right: 20),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  spreadRadius: 1,
                                  blurRadius: 4,
                                  offset: Offset(0, 3),
                                ),
                              ],
                            ),
                            child: Material(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                              child: CustomExpansionTile(
                                title: Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundImage:
                                          AssetImage(LocalImages.defaultAvatar),
                                      radius: 14,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      e.user.name,
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.black),
                                    ),
                                    // Spacer(),
                                    // IconButton(
                                    //     icon: ImageIcon(
                                    //       AssetImage(LocalImages.trashAlt),
                                    //       color: Colors.red,
                                    //       size: 14,
                                    //     ),
                                    //     onPressed: () {})
                                  ],
                                ),
                                childrenPadding: EdgeInsets.symmetric(
                                  horizontal: 20,
                                ),
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Job Role: ${e.role}',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Job Type: ${e.type.capitalize}',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Text(
                                        'Wage: ${NumberFormat.simpleCurrency().format(e.type == 'contract' ? e.contractDetails.amount : e.rate)}',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  e.type == 'contract'
                                      ? Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            e.contractDetails.startDate != null
                                                ? Text(
                                                    'Start Date : ' +
                                                        DateFormat('MM/dd/yyyy')
                                                            .format(DateTime.parse(e
                                                                .contractDetails
                                                                .startDate)),
                                                    style: TextStyle(
                                                        color:
                                                            ColorConfig.primary,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  )
                                                : Container(),
                                            e.contractDetails.endDate != null
                                                ? Text(
                                                    'End Date : ' +
                                                        DateFormat('MM/dd/yyyy')
                                                            .format(DateTime.parse(e
                                                                .contractDetails
                                                                .endDate)),
                                                    style: TextStyle(
                                                        color:
                                                            ColorConfig.primary,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  )
                                                : Container(),
                                          ],
                                        )
                                      : Container(),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(
                                        height: 28,
                                        child: FlatButton(
                                            padding: EdgeInsets.zero,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            color: ColorConfig.primary,
                                            onPressed: () {
                                              showDialog(
                                                  barrierDismissible: false,
                                                  context: context,
                                                  builder: (context) => Dialog(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20)),
                                                      backgroundColor:
                                                          Colors.white,
                                                      child: EmployeeInfoDialog(
                                                          e)));
                                            },
                                            child: Text(
                                              'View',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ))
                      .toList());
            }
            return Container();
          }

          return Container();
        },
      ),
    );
  }
}

class EmployeeInfoDialog extends StatelessWidget {
  final EmployeeModel employeeModel;

  EmployeeInfoDialog(this.employeeModel);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage(LocalImages.defaultAvatar),
                    radius: 24,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        employeeModel.user.name,
                        style: TextStyle(
                            fontSize: 14,
                            color: ColorConfig.primary,
                            fontWeight: FontWeight.w500),
                      ),
                      Text(
                        employeeModel.role,
                        style:
                            TextStyle(fontSize: 14, color: ColorConfig.black1),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Employement Details',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Jobe Type: ${employeeModel.type.capitalize}',
                    style: TextStyle(
                        color: ColorConfig.primary,
                        fontWeight: FontWeight.w400),
                  ),
                  Text(
                    'Wage: ${NumberFormat.simpleCurrency().format(employeeModel.type == 'contract' ? employeeModel.contractDetails.amount : employeeModel.rate)}',
                    style: TextStyle(
                        color: ColorConfig.primary,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
              employeeModel.type == 'contract'
                  ? Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            ImageIcon(
                              AssetImage(LocalImages.calander),
                              color: ColorConfig.primary,
                              size: 16,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            employeeModel.contractDetails.startDate != null
                                ? Text(
                                    'Start Date : ' +
                                        DateFormat('MM/dd/yyyy').format(
                                            DateTime.parse(employeeModel
                                                .contractDetails.startDate)),
                                    style: TextStyle(
                                        color: ColorConfig.primary,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Container(),
                          ],
                        ),
                      ],
                    )
                  : Container(),
              employeeModel.type == 'contract'
                  ? Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            ImageIcon(
                              AssetImage(LocalImages.calander),
                              color: ColorConfig.primary,
                              size: 16,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            employeeModel.contractDetails.endDate != null
                                ? Text(
                                    'End Date : ' +
                                        DateFormat('MM/dd/yyyy').format(
                                            DateTime.parse(employeeModel
                                                .contractDetails.endDate)),
                                    style: TextStyle(
                                        color: ColorConfig.primary,
                                        fontWeight: FontWeight.w400),
                                  )
                                : Container(),
                          ],
                        ),
                      ],
                    )
                  : Container(),
              // SizedBox(
              //   height: 10,
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: [
              //     Text(
              //       'Amount: \$${employeeModel.rate}',
              //       style: TextStyle(
              //           color: ColorConfig.primary,
              //           fontWeight: FontWeight.w400),
              //     ),
              //   ],
              // ),

              employeeModel.type == 'contract'
                  ? Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Description',
                              style: TextStyle(
                                  color: ColorConfig.primary,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                employeeModel.contractDetails.description ?? '',
                                style: TextStyle(
                                    color: ColorConfig.black6,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  : Container(),
              SizedBox(
                height: 10,
              ),
              Text(
                'Message: ',
                style: TextStyle(
                    color: ColorConfig.primary, fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Theme(
                      data:
                          ThemeData(unselectedWidgetColor: ColorConfig.orange),
                      child: Checkbox(
                          value: employeeModel?.permissions?.admin ?? false,
                          onChanged: (val) {})),
                  Text(
                    'Allow Admin Permission',
                    style: TextStyle(color: ColorConfig.orange),
                  )
                ],
              ),
              //   Row(
              //     mainAxisAlignment: MainAxisAlignment.end,
              //     children: [
              //       FlatButton(
              //           color: ColorConfig.red1,
              //           shape: RoundedRectangleBorder(
              //               borderRadius: BorderRadius.circular(5)),
              //           onPressed: () {
              //             Navigator.of(context).pop();
              //           },
              //           child: Text(
              //             'Delete',
              //             style: TextStyle(color: Colors.white),
              //           )),
              //       SizedBox(
              //         width: 10,
              //       ),
              //       FlatButton(
              //           color: ColorConfig.grey17,
              //           shape: RoundedRectangleBorder(
              //               borderRadius: BorderRadius.circular(5)),
              //           onPressed: () {
              //             Navigator.of(context).pop();
              //           },
              //           child: Text(
              //             'Cancel',
              //             style: TextStyle(color: ColorConfig.grey1),
              //           )),
              //       SizedBox(
              //         width: 10,
              //       ),
              //       FlatButton(
              //           color: ColorConfig.primary,
              //           shape: RoundedRectangleBorder(
              //               borderRadius: BorderRadius.circular(5)),
              //           onPressed: () {
              //             Navigator.of(context).pop();
              //           },
              //           child: Text(
              //             'Edit',
              //             style: TextStyle(color: Colors.white),
              //           )),
              //     ],
              //   )
            ],
          ),
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
} // class
