import 'dart:async';

import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:Subtraid/features/more/company/models/company_info_model.dart';
import 'package:Subtraid/features/more/company/models/company_response_model.dart';
import 'package:Subtraid/features/more/company/features/employee/repository/employee_reposiory.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'employee_event.dart';
part 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  EmployeeBloc({@required this.employeeRepository}) : super(EmployeeInitial()) {
    add(EmployeeRequested());
  }

  final EmployeeRepository employeeRepository;

  @override
  Stream<EmployeeState> mapEventToState(
    EmployeeEvent event,
  ) async* {
    if (event is EmployeeRequested) {
      yield* _mapEmployeeRequestedToState();
    }
  }

  Stream<EmployeeState> _mapEmployeeRequestedToState() async* {
    yield EmployeeLoadInProgress();

    try {
      CompanyResponseModel companyResponseModel =
          await employeeRepository.getEmployees();

      Map<String, EmployeeModel> employeeMap = Map();
      Map<String, CrewModel> crewMap = Map();

      companyResponseModel?.companyInfo?.company?.employees?.forEach((emp) {
        employeeMap.putIfAbsent(emp.user.id, () => emp);
      });
      companyResponseModel?.companyInfo?.company?.crews ??
          [].forEach((crew) {
            crewMap.putIfAbsent(crew.id, () => crew);
          });

      yield EmployeeLoadSuccess(
          employeeMap: employeeMap,
          crewMap: crewMap,
          companyInfo: companyResponseModel.companyInfo);
    } catch (e) {
      print(e);

      yield EmployeeLoadFailure();
    }
  }
}
