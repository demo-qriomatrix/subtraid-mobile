part of 'employee_bloc.dart';

abstract class EmployeeState extends Equatable {
  const EmployeeState();

  @override
  List<Object> get props => [];
}

class EmployeeInitial extends EmployeeState {}

class EmployeeLoadInProgress extends EmployeeState {}

class EmployeeLoadSuccess extends EmployeeState {
  final CompanyInfoModel companyInfo;
  final Map<String, EmployeeModel> employeeMap;
  final Map<String, CrewModel> crewMap;

  EmployeeLoadSuccess({
    @required this.companyInfo,
    @required this.employeeMap,
    @required this.crewMap,
  });
}

class EmployeeLoadFailure extends EmployeeState {}
