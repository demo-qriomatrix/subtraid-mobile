import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/enums/check_list_action_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/bloc/crew/crew_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/bloc/crew_actions/crew_actions_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/crew/widgets/crew_dialog.dart';
import 'package:Subtraid/features/shared/widgets/custom_expansion_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CrewScreen extends StatefulWidget {
  final TextEditingController companySearch;

  CrewScreen({@required this.companySearch});

  @override
  _CrewScreenState createState() => _CrewScreenState();
}

class _CrewScreenState extends State<CrewScreen> {
  String search = '';

  @override
  void initState() {
    super.initState();
    search = widget.companySearch.text;
    widget.companySearch.addListener(onChange);
  }

  onChange() {
    setState(() {
      search = widget.companySearch.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CrewBloc>(
      create: (context) => CrewBloc(crewRepository: sl()),
      lazy: false,
      child: BlocProvider<CrewActionsBloc>(
        lazy: false,
        create: (context) => CrewActionsBloc(
            crewRepository: sl(), crewBloc: context.read<CrewBloc>()),
        child: BlocListener<CrewActionsBloc, CrewActionsState>(
          listener: (context, state) async {
            if (state is CrewActionInProgress) {
              context.read<GlobalBloc>().add(ShowLoadingWidget());
            }

            if (state is CrewActionSuccess) {
              context.read<CrewBloc>().add(CrewRequested());

              context.read<GlobalBloc>().add(HideLoadingWidget());

              context.read<GlobalBloc>().add(
                  ShowSuccessSnackBar(message: state.message, event: null));
            }

            if (state is CrewActionFailure) {
              context.read<GlobalBloc>().add(HideLoadingWidget());

              context
                  .read<GlobalBloc>()
                  .add(ShowErrorSnackBar(error: state.error));
            }
          },
          child: BlocBuilder<CrewBloc, CrewState>(
            builder: (context, state) {
              if (state is CrewLoadInProgress) {
                return Center(child: CircularProgressIndicator());
              }

              if (state is CrewLoadSuccess) {
                if (state.companyInfo?.company?.crews != null &&
                    state.companyInfo.company.crews.isNotEmpty) {
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                                color: ColorConfig.primary,
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                onPressed: () {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (dialogContext) =>
                                          BlocProvider<CrewBloc>.value(
                                            value: context.read<CrewBloc>(),
                                            child: Dialog(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                backgroundColor: Colors.white,
                                                child: CrewDialog(
                                                  action: ActionEnum.add,
                                                )),
                                          ));
                                },
                                child: Row(
                                  children: [
                                    ImageIcon(
                                      AssetImage(LocalImages.crew),
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text('Create a crew',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                        ))
                                  ],
                                )),
                            SizedBox(
                              width: 10,
                            ),
                            FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: state.companyInfo.company.crews
                                          .where((e) => e.selected == true)
                                          .length >
                                      0
                                  ? ColorConfig.red1
                                  : ColorConfig.red1.withOpacity(0.7),
                              onPressed: () {
                                if (state.companyInfo.company.crews
                                        .where((e) => e.selected == true)
                                        .length >
                                    0)
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (dialogContext) =>
                                        BlocProvider<CrewActionsBloc>.value(
                                      value: context.read<CrewActionsBloc>(),
                                      child: Dialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20)),
                                        backgroundColor: Colors.white,
                                        child: Container(
                                          width: 0,
                                          padding: EdgeInsets.all(20),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  CircleAvatar(
                                                    backgroundColor:
                                                        ColorConfig.orange,
                                                    child: ImageIcon(
                                                      AssetImage(LocalImages
                                                          .exclamation),
                                                      color: Colors.white,
                                                      size: 20,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 20,
                                                  ),
                                                  Flexible(
                                                    child: Text(
                                                      'Are you sure you want to delete selected crews. All data will be lost.',
                                                      style: TextStyle(
                                                          color: ColorConfig
                                                              .primary,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: <Widget>[
                                                  FlatButton(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                    color: ColorConfig.grey3,
                                                    child: Text(
                                                      'No',
                                                      style: TextStyle(
                                                              fontSize: 12)
                                                          .copyWith(
                                                              color: ColorConfig
                                                                  .grey2),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.of(
                                                              dialogContext)
                                                          .pop();
                                                    },
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  FlatButton(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                    color: ColorConfig.primary,
                                                    child: Text(
                                                      'Yes',
                                                      style: TextStyle(
                                                              fontSize: 12)
                                                          .copyWith(
                                                              color:
                                                                  Colors.white),
                                                    ),
                                                    onPressed: () async {
                                                      Navigator.of(
                                                              dialogContext)
                                                          .pop();

                                                      // await Future.delayed(
                                                      //     Duration(seconds: 1));

                                                      context
                                                          .read<
                                                              CrewActionsBloc>()
                                                          .add(
                                                              DeleteSelectedCrew());
                                                      // _context
                                                      //     .read<CalenderBloc>()
                                                      //     .add(CalenderDeleteEvent(
                                                      //         calenderEventModel:
                                                      //             event));
                                                    },
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                              },
                              padding: EdgeInsets.symmetric(horizontal: 30),
                              child: Row(
                                children: <Widget>[
                                  ImageIcon(
                                    AssetImage(LocalImages.trashAlt),
                                    color: Colors.white,
                                    size: 14,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Delete',
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: ListView(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            children: state.companyInfo.company.crews
                                .where((element) => search != ''
                                    ? element.name
                                        .toLowerCase()
                                        .contains(search.toLowerCase())
                                    : true)
                                .map((e) => Container(
                                      margin: EdgeInsets.only(
                                          bottom: 20, left: 20, right: 20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.3),
                                            spreadRadius: 1,
                                            blurRadius: 4,
                                            offset: Offset(0, 3),
                                          ),
                                        ],
                                      ),
                                      child: Material(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.white,
                                        child: CustomExpansionTile(
                                          title: Row(
                                            children: [
                                              Theme(
                                                data:
                                                    Theme.of(context).copyWith(
                                                  unselectedWidgetColor:
                                                      ColorConfig.orange,
                                                ),
                                                child: Checkbox(
                                                  onChanged: (val) {
                                                    context
                                                        .read<CrewBloc>()
                                                        .add(CrewSetSelected(
                                                            id: e.id,
                                                            selected: val));
                                                  },
                                                  value:
                                                      (e?.selected ?? false) ==
                                                          true,
                                                ),
                                              ),
                                              Text(
                                                e.name,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.black),
                                              ),
                                              Spacer(),
                                              IconButton(
                                                  icon: ImageIcon(
                                                    AssetImage(
                                                        LocalImages.trashAlt),
                                                    color: Colors.red,
                                                    size: 14,
                                                  ),
                                                  onPressed: () {
                                                    CrewModel crewModel =
                                                        CrewModel((data) => data
                                                          ..crewId = e.id);
                                                    context
                                                        .read<CrewActionsBloc>()
                                                        .add(DeleteCrew(
                                                            crewModel:
                                                                crewModel));
                                                  })
                                            ],
                                          ),
                                          childrenPadding: EdgeInsets.symmetric(
                                            horizontal: 20,
                                          ),
                                          children: [
                                            Row(
                                              children: e.members
                                                  .map(
                                                    (memeber) => Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 10),
                                                      child: CircleAvatar(
                                                        backgroundImage:
                                                            AssetImage(LocalImages
                                                                .defaultAvatar),
                                                        radius: 14,
                                                      ),
                                                    ),
                                                  )
                                                  .toList(),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Text(
                                                    e.description,
                                                    style: TextStyle(
                                                        color: ColorConfig
                                                            .primary),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                SizedBox(
                                                  height: 28,
                                                  child: FlatButton(
                                                      padding: EdgeInsets.zero,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                      color:
                                                          ColorConfig.primary,
                                                      onPressed: () {
                                                        showDialog(
                                                            barrierDismissible:
                                                                false,
                                                            context: context,
                                                            builder:
                                                                (dialogContext) =>
                                                                    BlocProvider<
                                                                        CrewBloc>.value(
                                                                      value: context
                                                                          .read<
                                                                              CrewBloc>(),
                                                                      child: Dialog(
                                                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                                                          backgroundColor: Colors.white,
                                                                          child: CrewDialog(
                                                                            action:
                                                                                ActionEnum.view,
                                                                            crew:
                                                                                e,
                                                                          )),
                                                                    ));
                                                      },
                                                      child: Text(
                                                        'View',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      )),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ))
                                .toList()),
                      ),
                    ],
                  );
                }
                return Container();
              }

              return Container();
            },
          ),
        ),
      ),
    );
  }
}

// class CrewInfoDialog extends StatefulWidget {
//   final CrewModel crewModel;

//   CrewInfoDialog(this.crewModel);

//   @override
//   _CrewInfoDialogState createState() => _CrewInfoDialogState();
// }

// class _CrewInfoDialogState extends State<CrewInfoDialog> {

//   bool editMode = false;

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       mainAxisSize: MainAxisSize.min,
//       children: [
//         Row(
//           mainAxisAlignment: MainAxisAlignment.end,
//           children: [
//             IconButton(
//                 icon: Icon(
//                   Icons.close,
//                   color: Colors.black,
//                 ),
//                 onPressed: () {
//                   Navigator.of(context).pop();
//                 })
//           ],
//         ),
//         Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 20),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Row(
//                 children: [
//                   CircleAvatar(
//                     backgroundImage: AssetImage(LocalImages.defaultAvatar),
//                     radius: 24,
//                   ),
//                   SizedBox(
//                     width: 10,
//                   ),
//                   Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(
//                         widget.crewModel.name,
//                         style: TextStyle(
//                             fontSize: 14,
//                             color: ColorConfig.primary,
//                             fontWeight: FontWeight.w500),
//                       ),
//                       Text(
//                         'Company email',
//                         style:
//                             TextStyle(fontSize: 14, color: ColorConfig.black1),
//                       ),
//                     ],
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               Text(
//                 'Company User Details',
//                 style: TextStyle(fontWeight: FontWeight.w600),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Row(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Text(
//                     'Office Address : ',
//                     style: TextStyle(
//                         color: ColorConfig.primary,
//                         fontWeight: FontWeight.w400),
//                   ),
//                   Flexible(
//                     child: Text(
//                       'Office Address : 55 Port St E, Mississauga, ON L5G 4P3, Canada',
//                       style: TextStyle(
//                           color: ColorConfig.primary,
//                           fontWeight: FontWeight.w400),
//                     ),
//                   )
//                 ],
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Row(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Text(
//                     'Phone : +94 777 758 2587',
//                     style: TextStyle(
//                         color: ColorConfig.primary,
//                         fontWeight: FontWeight.w400),
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 15,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Text(
//                     'Description',
//                     style: TextStyle(
//                         color: ColorConfig.primary,
//                         fontWeight: FontWeight.w400),
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 5,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Flexible(
//                     child: Text(
//                       widget.crewModel.description ?? '',
//                       style: TextStyle(
//                           color: ColorConfig.black6,
//                           fontWeight: FontWeight.w400),
//                     ),
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 15,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Text(
//                     'Services',
//                     style: TextStyle(
//                         color: ColorConfig.primary,
//                         fontWeight: FontWeight.w400),
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 5,
//               ),
//               Wrap(
//                 children: [
//                   ServiceWidget('Management services'),
//                   ServiceWidget('Marketing Service'),
//                   ServiceWidget('Consulting services'),
//                   ServiceWidget('Financial services'),
//                 ],
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//             ],
//           ),
//         ),
//         SizedBox(
//           height: 20,
//         )
//       ],
//     );
//   }
// }

class ServiceWidget extends StatelessWidget {
  final String title;

  ServiceWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(right: 10, bottom: 10),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            color: ColorConfig.grey19, borderRadius: BorderRadius.circular(5)),
        child: Text(
          title,
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 12),
        ));
  }
} // class
