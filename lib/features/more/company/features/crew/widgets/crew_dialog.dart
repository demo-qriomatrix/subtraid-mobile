import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/enums/check_list_action_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/bloc/crew/crew_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/bloc/crew_actions/crew_actions_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/employee/bloc/employee_bloc.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class CrewDialog extends StatefulWidget {
  final ActionEnum action;
  final CrewModel crew;

  CrewDialog({@required this.action, this.crew});

  @override
  _CrewDialogState createState() => _CrewDialogState();
}

class _CrewDialogState extends State<CrewDialog> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Set<EmployeeModel> selectedMembers = Set();
  TextEditingController member = TextEditingController();

  TextEditingController name = TextEditingController();
  TextEditingController description = TextEditingController();

  bool autoValidate = false;
  bool submitted = false;

  ActionEnum action;

  @override
  void initState() {
    action = widget.action;
    if (widget.crew != null) {
      name.text = widget.crew.name;
      description.text = widget.crew.description;
      selectedMembers = widget.crew.members.toSet();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CrewActionsBloc>(
      create: (context) => CrewActionsBloc(
          crewRepository: sl(), crewBloc: context.read<CrewBloc>()),
      child: BlocConsumer<CrewActionsBloc, CrewActionsState>(
        listener: (context, state) async {
          if (state is CrewActionInProgress) {
            context.read<GlobalBloc>().add(ShowLoadingWidget());
          }

          if (state is CrewActionSuccess) {
            context.read<CrewBloc>().add(CrewRequested());

            context.read<GlobalBloc>().add(HideLoadingWidget());

            await Future.delayed(Duration(milliseconds: 1));

            Navigator.of(context).pop(true);

            context
                .read<GlobalBloc>()
                .add(ShowSuccessSnackBar(message: state.message, event: null));
          }

          if (state is CrewActionFailure) {
            context.read<GlobalBloc>().add(HideLoadingWidget());

            Navigator.of(context).pop(false);

            context
                .read<GlobalBloc>()
                .add(ShowErrorSnackBar(error: state.error));
          }
        },
        builder: (context, state) {
          if (action == ActionEnum.view && widget.crew != null) {
            return Container(
              padding: EdgeInsets.all(20),
              child: ListView(shrinkWrap: true, children: <Widget>[
                Row(
                  children: [
                    Text(
                      widget.crew.name ?? '',
                      style: TextStyle(
                          color: ColorConfig.primary,
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                    Spacer(),
                    IconButton(
                      icon: Icon(Icons.close),
                      color: ColorConfig.primary,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ),
                Text(
                  'Description',
                  style: TextStyle(
                    color: ColorConfig.primary,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      widget.crew.description ?? '',
                      style: TextStyle(color: ColorConfig.grey4),
                    )),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  'Member List',
                  style: TextStyle(
                    color: ColorConfig.primary,
                  ),
                ),
                widget.crew.members.isEmpty
                    ? Container()
                    : Column(
                        children: widget.crew.members
                            .map((e) => Container(
                                  margin: EdgeInsets.only(top: 10),
                                  decoration:
                                      BoxDecoration(color: ColorConfig.blue1),
                                  padding: EdgeInsets.symmetric(vertical: 5),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: CircleAvatar(
                                          backgroundImage: AssetImage(
                                              LocalImages.defaultAvatar),
                                          radius: 16,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Text(
                                          e.user.name,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Expanded(
                                        child: Text(
                                          e.role,
                                          style: TextStyle(
                                              color: ColorConfig.grey1,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ],
                                  ),
                                ))
                            .toList(),
                      ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      color: ColorConfig.red1,
                      child: Text(
                        'Delete',
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      ),
                      onPressed: () {
                        CrewModel crewModel =
                            CrewModel((data) => data..crewId = widget.crew.id);
                        context
                            .read<CrewActionsBloc>()
                            .add(DeleteCrew(crewModel: crewModel));
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    FlatButton(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      color: ColorConfig.grey3,
                      child: Text(
                        'Cancel',
                        style:
                            TextStyle(fontSize: 12, color: ColorConfig.grey2),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      color: ColorConfig.primary,
                      padding: EdgeInsets.zero,
                      child: Text(
                        'Edit',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                      onPressed: () {
                        setState(() {
                          action = ActionEnum.update;
                        });
                      },
                    )
                  ],
                )
              ]),
            );
          }

          if (action == ActionEnum.add || action == ActionEnum.update) {
            return Container(
              padding: EdgeInsets.all(20),
              child: Form(
                key: _formKey,
                autovalidateMode: autoValidate
                    ? AutovalidateMode.always
                    : AutovalidateMode.disabled,
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: ColorConfig.orange,
                          child: Icon(
                            action == ActionEnum.update
                                ? Icons.edit
                                : Icons.add,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          action == ActionEnum.update
                              ? 'Edit Crew'
                              : 'Create a Crew',
                          style: TextStyle(
                              color: ColorConfig.primary,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        Spacer(),
                        IconButton(
                          icon: Icon(Icons.close),
                          color: ColorConfig.primary,
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Required';
                        }

                        return null;
                      },
                      controller: name,
                      decoration: InputDecoration(
                          alignLabelWithHint: true,
                          labelText: 'Crew Name',
                          border: UnderlineInputBorder()
                          //  contentPadding: EdgeInsets.zero
                          ),
                    ),
                    TextFormField(
                      controller: description,
                      decoration: InputDecoration(
                          alignLabelWithHint: true,
                          labelText: 'Description',
                          border: UnderlineInputBorder()
                          //  contentPadding: EdgeInsets.zero
                          ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Members',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    BlocProvider<EmployeeBloc>(
                        create: (context) =>
                            EmployeeBloc(employeeRepository: sl()),
                        child: BlocBuilder<EmployeeBloc, EmployeeState>(
                            builder: (context, state) {
                          return TypeAheadFormField(
                              enabled: true,
                              textFieldConfiguration: TextFieldConfiguration(
                                controller: member,
                                decoration: InputDecoration(
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  labelText: 'Select members',
                                  labelStyle: TextStyle(
                                      color: ColorConfig.black1,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                  suffixIcon: Icon(
                                    Icons.add,
                                    color: ColorConfig.primary,
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide.none),
                                ),
                              ),
                              suggestionsCallback: (pattern) {
                                if (state is EmployeeLoadSuccess) {
                                  return state.companyInfo.company.employees
                                      .where((element) => element.user.name
                                          .toLowerCase()
                                          .contains(pattern.toLowerCase()))
                                      .toList();
                                }
                                return null;
                              },
                              itemBuilder: (context, EmployeeModel suggestion) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: Row(
                                    children: [
                                      Theme(
                                        data: ThemeData(
                                            unselectedWidgetColor:
                                                ColorConfig.orange),
                                        child: IgnorePointer(
                                          child: Checkbox(
                                              value: selectedMembers
                                                  .where((mem) =>
                                                      mem.id == suggestion.id)
                                                  .isNotEmpty,
                                              onChanged: (val) {
                                                if (val) {
                                                  setState(() {
                                                    selectedMembers
                                                        .add(suggestion);
                                                  });
                                                  // context.read<ProjectBloc>().add(
                                                  //     ProjectCardAddEmployee(
                                                  //         employeeId: suggestion.id));
                                                } else {
                                                  setState(() {
                                                    selectedMembers.remove(
                                                        selectedMembers
                                                            .where((mem) =>
                                                                mem.id ==
                                                                suggestion.id)
                                                            .first);
                                                  });
                                                  // context.read<ProjectBloc>().add(
                                                  //     ProjectCardRemoveEmployee(
                                                  //         employeeId: suggestion.id));
                                                }
                                              }),
                                        ),
                                      ),
                                      CircleAvatar(
                                        backgroundImage: AssetImage(
                                            LocalImages.defaultAvatar),
                                        radius: 16,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(suggestion.user.name)
                                    ],
                                  ),
                                );
                              },
                              transitionBuilder:
                                  (context, suggestionsBox, controller) {
                                return suggestionsBox;
                              },
                              onSuggestionSelected: (suggestion) {
                                if (selectedMembers
                                    .where((mem) => mem.id == suggestion.id)
                                    .isEmpty) {
                                  setState(() {
                                    selectedMembers.add(suggestion);
                                  });

                                  // context.read<ProjectBloc>().add(
                                  //     ProjectCardAddEmployee(employeeId: suggestion.id));
                                } else {
                                  setState(() {
                                    selectedMembers.remove(selectedMembers
                                        .where((mem) => mem.id == suggestion.id)
                                        .first);
                                  });
                                  // context.read<ProjectBloc>().add(
                                  //     ProjectCardRemoveEmployee(
                                  //         employeeId: suggestion.id));
                                }
                              },
                              validator: (value) {
                                return null;
                              },
                              onSaved: (value) {});
                        })),
                    SizedBox(
                      height: 5,
                    ),
                    selectedMembers.isEmpty
                        ? Container()
                        : Column(
                            children: selectedMembers
                                .map((e) => Container(
                                      margin: EdgeInsets.only(top: 10),
                                      decoration: BoxDecoration(
                                          color: ColorConfig.blue1),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 5),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: CircleAvatar(
                                              backgroundImage: AssetImage(
                                                  LocalImages.defaultAvatar),
                                              radius: 16,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Expanded(
                                            child: Text(
                                              e.user.name,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              e.role,
                                              style: TextStyle(
                                                  color: ColorConfig.grey1,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          IconButton(
                                              icon: Icon(
                                                Icons.close,
                                                color: ColorConfig.primary,
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                  selectedMembers.remove(e);
                                                });
                                              })
                                        ],
                                      ),
                                    ))
                                .toList(),
                          ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        action == ActionEnum.update
                            ? FlatButton(
                                padding: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                color: ColorConfig.red1,
                                child: Text(
                                  'Delete',
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.white),
                                ),
                                onPressed: () {
                                  CrewModel crewModel = CrewModel(
                                      (data) => data..crewId = widget.crew.id);
                                  context
                                      .read<CrewActionsBloc>()
                                      .add(DeleteCrew(crewModel: crewModel));
                                },
                              )
                            : Container(),
                        SizedBox(
                          width: 10,
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          color: ColorConfig.grey3,
                          child: Text(
                            'Cancel',
                            style: TextStyle(
                                fontSize: 12, color: ColorConfig.grey2),
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          color: ColorConfig.primary,
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  action == ActionEnum.update ? 10 : 40),
                          child: Text(
                            action == ActionEnum.update ? 'Save' : 'Create',
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                          onPressed: () {
                            setState(() {
                              autoValidate = true;
                            });
                            if (_formKey.currentState.validate()) {
                              if (!submitted) {
                                submitted = true;

                                if (action == ActionEnum.update) {
                                  CrewModel crewModel = CrewModel((data) => data
                                    ..crewId = widget.crew.id
                                    ..name = name.text
                                    ..description = description.text
                                    ..members = ListBuilder(selectedMembers));

                                  context
                                      .read<CrewActionsBloc>()
                                      .add(EditCrew(crewModel: crewModel));
                                } else {
                                  CrewModel crewModel = CrewModel((data) => data
                                    ..name = name.text
                                    ..description = description.text
                                    ..members = ListBuilder(selectedMembers));

                                  context
                                      .read<CrewActionsBloc>()
                                      .add(AddCrew(crewModel: crewModel));
                                }
                              }
                            }
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
