import 'dart:async';

import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/crew/repository/crew_reposiory.dart';
import 'package:Subtraid/features/more/company/models/company_info_model.dart';
import 'package:Subtraid/features/more/company/models/company_response_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:built_collection/built_collection.dart';

part 'crew_event.dart';
part 'crew_state.dart';

class CrewBloc extends Bloc<CrewEvent, CrewState> {
  CrewBloc({@required this.crewRepository}) : super(CrewInitial()) {
    add(CrewRequested());
  }

  final CrewRepository crewRepository;

  @override
  Stream<CrewState> mapEventToState(
    CrewEvent event,
  ) async* {
    if (event is CrewRequested) {
      yield* _mapCrewRequestedToState();
    }

    if (event is CrewSetSelected) {
      yield* _mapCrewSetSelectedToState(event);
    }
  }

  Stream<CrewState> _mapCrewSetSelectedToState(CrewSetSelected event) async* {
    if (state is CrewLoadSuccess) {
      List<CrewModel> previousCrews =
          (state as CrewLoadSuccess).companyInfo.company.crews.toList();

      int index = previousCrews.indexWhere((element) => element.id == event.id);

      CrewModel crew = previousCrews[index];

      crew = crew.rebuild((t) => t..selected = event.selected);

      previousCrews[index] = crew;

      List<CrewModel> updatedCrews = List.from(previousCrews.toList());

      CompanyInfoModel companyInfoModel = (state as CrewLoadSuccess)
          .companyInfo
          .rebuild((e) => e.company = (state as CrewLoadSuccess)
              .companyInfo
              .company
              .rebuild((e) => e.crews = ListBuilder(updatedCrews))
              .toBuilder());

      yield CrewLoadSuccess(companyInfo: companyInfoModel);
    }
  }

  Stream<CrewState> _mapCrewRequestedToState() async* {
    yield CrewLoadInProgress();

    try {
      CompanyResponseModel companyResponseModel =
          await crewRepository.getCrews();

      yield CrewLoadSuccess(companyInfo: companyResponseModel.companyInfo);
    } catch (e) {
      print(e);

      yield CrewLoadFailure();
    }
  }
}
