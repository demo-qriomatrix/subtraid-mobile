part of 'crew_bloc.dart';

abstract class CrewEvent extends Equatable {
  const CrewEvent();

  @override
  List<Object> get props => [];
}

class CrewRequested extends CrewEvent {}

class CrewSetSelected extends CrewEvent {
  final bool selected;
  final String id;

  CrewSetSelected({@required this.id, @required this.selected});
}
