part of 'crew_bloc.dart';

abstract class CrewState extends Equatable {
  const CrewState();

  @override
  List<Object> get props => [];
}

class CrewInitial extends CrewState {}

class CrewLoadInProgress extends CrewState {}

class CrewLoadSuccess extends CrewState {
  final CompanyInfoModel companyInfo;
  CrewLoadSuccess({@required this.companyInfo});
  @override
  List<Object> get props => [companyInfo];
}

class CrewLoadFailure extends CrewState {}
