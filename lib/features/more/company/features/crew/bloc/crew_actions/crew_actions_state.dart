part of 'crew_actions_bloc.dart';

abstract class CrewActionsState extends Equatable {
  const CrewActionsState();

  @override
  List<Object> get props => [];
}

class CrewActionsInitial extends CrewActionsState {}

class CrewActionInProgress extends CrewActionsState {}

class CrewActionSuccess extends CrewActionsState {
  final String message;

  CrewActionSuccess({@required this.message});
}

class CrewActionFailure extends CrewActionsState {
  final String error;

  CrewActionFailure({@required this.error});
}
