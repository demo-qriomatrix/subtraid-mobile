part of 'crew_actions_bloc.dart';

abstract class CrewActionsEvent extends Equatable {
  const CrewActionsEvent();

  @override
  List<Object> get props => [];
}

class AddCrew extends CrewActionsEvent {
  final CrewModel crewModel;

  AddCrew({@required this.crewModel});
}

class EditCrew extends CrewActionsEvent {
  final CrewModel crewModel;

  EditCrew({@required this.crewModel});
}

class DeleteCrew extends CrewActionsEvent {
  final CrewModel crewModel;

  DeleteCrew({@required this.crewModel});
}

class DeleteSelectedCrew extends CrewActionsEvent {}
