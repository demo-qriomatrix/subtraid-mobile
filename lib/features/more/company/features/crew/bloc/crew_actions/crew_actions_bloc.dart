import 'dart:async';
import 'dart:convert';

import 'package:Subtraid/features/more/company/features/crew/bloc/crew/crew_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/crew/repository/crew_reposiory.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'crew_actions_event.dart';
part 'crew_actions_state.dart';

class CrewActionsBloc extends Bloc<CrewActionsEvent, CrewActionsState> {
  CrewActionsBloc({@required this.crewRepository, @required this.crewBloc})
      : super(CrewActionsInitial());
  final CrewRepository crewRepository;

  final CrewBloc crewBloc;

  @override
  Stream<CrewActionsState> mapEventToState(
    CrewActionsEvent event,
  ) async* {
    if (event is AddCrew) {
      yield* _mapAddCrewToState(event.crewModel);
    }
    if (event is EditCrew) {
      yield* _mapEditCrewToState(event.crewModel);
    }
    if (event is DeleteCrew) {
      yield* _mapDeleteCrewToState(event.crewModel);
    }
    if (event is DeleteSelectedCrew) {
      yield* _mapDeleteSelectedCrewToState();
    }
  }

  Stream<CrewActionsState> _mapDeleteSelectedCrewToState() async* {
    yield CrewActionInProgress();

    try {
      if (crewBloc.state is CrewLoadSuccess) {
        CrewLoadSuccess crewBlocState = crewBloc.state;

        List<CrewModel> crews = crewBlocState.companyInfo.company.crews
            .where((e) => e.selected == true)
            .toList();

        for (CrewModel crew in crews) {
          crew = crew.rebuild((e) => e.crewId = e.id);
          await crewRepository.deleteCrew(crew);
        }
      }

      yield CrewActionSuccess(message: 'Crews have been deleted');
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        yield CrewActionFailure(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message);
      } else {
        yield CrewActionFailure(error: 'Something went error');
      }
    } catch (e) {
      print(e);

      yield CrewActionFailure(error: 'Something went error');
    }
  }

  Stream<CrewActionsState> _mapDeleteCrewToState(CrewModel crewModel) async* {
    yield CrewActionInProgress();

    try {
      await crewRepository.deleteCrew(crewModel);

      yield CrewActionSuccess(message: 'A crew has been deleted');
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        yield CrewActionFailure(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message);
      } else {
        yield CrewActionFailure(error: 'Something went error');
      }
    } catch (e) {
      print(e);

      yield CrewActionFailure(error: 'Something went error');
    }
  }

  Stream<CrewActionsState> _mapEditCrewToState(CrewModel crewModel) async* {
    yield CrewActionInProgress();

    try {
      await crewRepository.editCrew(crewModel);

      yield CrewActionSuccess(message: 'A crew has been updated');
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        yield CrewActionFailure(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message);
      } else {
        yield CrewActionFailure(error: 'Something went error');
      }
    } catch (e) {
      print(e);

      yield CrewActionFailure(error: 'Something went error');
    }
  }

  Stream<CrewActionsState> _mapAddCrewToState(CrewModel crewModel) async* {
    yield CrewActionInProgress();

    try {
      await crewRepository.addCrew(crewModel);

      yield CrewActionSuccess(message: 'A new crew has been added');
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        yield CrewActionFailure(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message);
      } else {
        yield CrewActionFailure(error: 'Something went error');
      }
    } catch (e) {
      print(e);

      yield CrewActionFailure(error: 'Something went error');
    }
  }
}
