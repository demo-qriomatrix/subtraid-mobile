library crew_member_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/company/models/company_permissions_model.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'crew_member_model.g.dart';

abstract class CrewMemberModel
    implements Built<CrewMemberModel, CrewMemberModelBuilder> {
  @nullable
  CompanyPermissionsModel get permissions;

  @nullable
  String get role;

  @nullable
  String get type;

  @nullable
  int get rate;

  @nullable
  String get crew;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  MemberModel get user;

  CrewMemberModel._();

  factory CrewMemberModel([updates(CrewMemberModelBuilder b)]) =
      _$CrewMemberModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CrewMemberModel.serializer, this));
  }

  static CrewMemberModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CrewMemberModel.serializer, json.decode(jsonString));
  }

  static Serializer<CrewMemberModel> get serializer =>
      _$crewMemberModelSerializer;
}
