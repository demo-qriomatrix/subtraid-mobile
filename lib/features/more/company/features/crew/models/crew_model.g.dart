// GENERATED CODE - DO NOT MODIFY BY HAND

part of crew_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CrewModel> _$crewModelSerializer = new _$CrewModelSerializer();

class _$CrewModelSerializer implements StructuredSerializer<CrewModel> {
  @override
  final Iterable<Type> types = const [CrewModel, _$CrewModel];
  @override
  final String wireName = 'CrewModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CrewModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.crewId != null) {
      result
        ..add('crewId')
        ..add(serializers.serialize(object.crewId,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.members != null) {
      result
        ..add('members')
        ..add(serializers.serialize(object.members,
            specifiedType: const FullType(
                BuiltList, const [const FullType(EmployeeModel)])));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.selected != null) {
      result
        ..add('selected')
        ..add(serializers.serialize(object.selected,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  CrewModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CrewModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'crewId':
          result.crewId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'members':
          result.members.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EmployeeModel)]))
              as BuiltList<Object>);
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'selected':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$CrewModel extends CrewModel {
  @override
  final String id;
  @override
  final String crewId;
  @override
  final String name;
  @override
  final BuiltList<EmployeeModel> members;
  @override
  final String type;
  @override
  final String description;
  @override
  final bool selected;

  factory _$CrewModel([void Function(CrewModelBuilder) updates]) =>
      (new CrewModelBuilder()..update(updates)).build();

  _$CrewModel._(
      {this.id,
      this.crewId,
      this.name,
      this.members,
      this.type,
      this.description,
      this.selected})
      : super._();

  @override
  CrewModel rebuild(void Function(CrewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CrewModelBuilder toBuilder() => new CrewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CrewModel &&
        id == other.id &&
        crewId == other.crewId &&
        name == other.name &&
        members == other.members &&
        type == other.type &&
        description == other.description &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), crewId.hashCode),
                        name.hashCode),
                    members.hashCode),
                type.hashCode),
            description.hashCode),
        selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CrewModel')
          ..add('id', id)
          ..add('crewId', crewId)
          ..add('name', name)
          ..add('members', members)
          ..add('type', type)
          ..add('description', description)
          ..add('selected', selected))
        .toString();
  }
}

class CrewModelBuilder implements Builder<CrewModel, CrewModelBuilder> {
  _$CrewModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _crewId;
  String get crewId => _$this._crewId;
  set crewId(String crewId) => _$this._crewId = crewId;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  ListBuilder<EmployeeModel> _members;
  ListBuilder<EmployeeModel> get members =>
      _$this._members ??= new ListBuilder<EmployeeModel>();
  set members(ListBuilder<EmployeeModel> members) => _$this._members = members;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  bool _selected;
  bool get selected => _$this._selected;
  set selected(bool selected) => _$this._selected = selected;

  CrewModelBuilder();

  CrewModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _crewId = _$v.crewId;
      _name = _$v.name;
      _members = _$v.members?.toBuilder();
      _type = _$v.type;
      _description = _$v.description;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CrewModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CrewModel;
  }

  @override
  void update(void Function(CrewModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CrewModel build() {
    _$CrewModel _$result;
    try {
      _$result = _$v ??
          new _$CrewModel._(
              id: id,
              crewId: crewId,
              name: name,
              members: _members?.build(),
              type: type,
              description: description,
              selected: selected);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'members';
        _members?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CrewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
