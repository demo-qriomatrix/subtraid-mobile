// GENERATED CODE - DO NOT MODIFY BY HAND

part of crew_member_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CrewMemberModel> _$crewMemberModelSerializer =
    new _$CrewMemberModelSerializer();

class _$CrewMemberModelSerializer
    implements StructuredSerializer<CrewMemberModel> {
  @override
  final Iterable<Type> types = const [CrewMemberModel, _$CrewMemberModel];
  @override
  final String wireName = 'CrewMemberModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CrewMemberModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.permissions != null) {
      result
        ..add('permissions')
        ..add(serializers.serialize(object.permissions,
            specifiedType: const FullType(CompanyPermissionsModel)));
    }
    if (object.role != null) {
      result
        ..add('role')
        ..add(serializers.serialize(object.role,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.rate != null) {
      result
        ..add('rate')
        ..add(serializers.serialize(object.rate,
            specifiedType: const FullType(int)));
    }
    if (object.crew != null) {
      result
        ..add('crew')
        ..add(serializers.serialize(object.crew,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(MemberModel)));
    }
    return result;
  }

  @override
  CrewMemberModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CrewMemberModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'permissions':
          result.permissions.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CompanyPermissionsModel))
              as CompanyPermissionsModel);
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rate':
          result.rate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'crew':
          result.crew = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(MemberModel)) as MemberModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CrewMemberModel extends CrewMemberModel {
  @override
  final CompanyPermissionsModel permissions;
  @override
  final String role;
  @override
  final String type;
  @override
  final int rate;
  @override
  final String crew;
  @override
  final String id;
  @override
  final MemberModel user;

  factory _$CrewMemberModel([void Function(CrewMemberModelBuilder) updates]) =>
      (new CrewMemberModelBuilder()..update(updates)).build();

  _$CrewMemberModel._(
      {this.permissions,
      this.role,
      this.type,
      this.rate,
      this.crew,
      this.id,
      this.user})
      : super._();

  @override
  CrewMemberModel rebuild(void Function(CrewMemberModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CrewMemberModelBuilder toBuilder() =>
      new CrewMemberModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CrewMemberModel &&
        permissions == other.permissions &&
        role == other.role &&
        type == other.type &&
        rate == other.rate &&
        crew == other.crew &&
        id == other.id &&
        user == other.user;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, permissions.hashCode), role.hashCode),
                        type.hashCode),
                    rate.hashCode),
                crew.hashCode),
            id.hashCode),
        user.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CrewMemberModel')
          ..add('permissions', permissions)
          ..add('role', role)
          ..add('type', type)
          ..add('rate', rate)
          ..add('crew', crew)
          ..add('id', id)
          ..add('user', user))
        .toString();
  }
}

class CrewMemberModelBuilder
    implements Builder<CrewMemberModel, CrewMemberModelBuilder> {
  _$CrewMemberModel _$v;

  CompanyPermissionsModelBuilder _permissions;
  CompanyPermissionsModelBuilder get permissions =>
      _$this._permissions ??= new CompanyPermissionsModelBuilder();
  set permissions(CompanyPermissionsModelBuilder permissions) =>
      _$this._permissions = permissions;

  String _role;
  String get role => _$this._role;
  set role(String role) => _$this._role = role;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  int _rate;
  int get rate => _$this._rate;
  set rate(int rate) => _$this._rate = rate;

  String _crew;
  String get crew => _$this._crew;
  set crew(String crew) => _$this._crew = crew;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  MemberModelBuilder _user;
  MemberModelBuilder get user => _$this._user ??= new MemberModelBuilder();
  set user(MemberModelBuilder user) => _$this._user = user;

  CrewMemberModelBuilder();

  CrewMemberModelBuilder get _$this {
    if (_$v != null) {
      _permissions = _$v.permissions?.toBuilder();
      _role = _$v.role;
      _type = _$v.type;
      _rate = _$v.rate;
      _crew = _$v.crew;
      _id = _$v.id;
      _user = _$v.user?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CrewMemberModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CrewMemberModel;
  }

  @override
  void update(void Function(CrewMemberModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CrewMemberModel build() {
    _$CrewMemberModel _$result;
    try {
      _$result = _$v ??
          new _$CrewMemberModel._(
              permissions: _permissions?.build(),
              role: role,
              type: type,
              rate: rate,
              crew: crew,
              id: id,
              user: _user?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'permissions';
        _permissions?.build();

        _$failedField = 'user';
        _user?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CrewMemberModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
