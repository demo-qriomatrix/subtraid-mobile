library crew_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'crew_model.g.dart';

abstract class CrewModel implements Built<CrewModel, CrewModelBuilder> {
  // @nullable
  // List<Null> get currentProjects;

  // @nullable
  // List<Null> get projectHistory;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get crewId;

  @nullable
  String get name;

  @nullable
  BuiltList<EmployeeModel> get members;

  @nullable
  String get type;

  @nullable
  String get description;

  @nullable
  bool get selected;

  CrewModel._();

  factory CrewModel([updates(CrewModelBuilder b)]) = _$CrewModel;

  String toJson() {
    return json.encode(serializers.serializeWith(CrewModel.serializer, this));
  }

  static CrewModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CrewModel.serializer, json.decode(jsonString));
  }

  static Serializer<CrewModel> get serializer => _$crewModelSerializer;
}
