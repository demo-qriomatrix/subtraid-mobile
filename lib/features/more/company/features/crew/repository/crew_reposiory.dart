import 'dart:convert';

import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/models/company_response_model.dart';
import 'package:dio/dio.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:meta/meta.dart';

class CrewRepository {
  final HttpClient httpClient;

  CrewRepository({@required this.httpClient});

  Future<CompanyResponseModel> getCrews() async {
    Response response =
        await httpClient.dio.get(EndpointConfig.userEmployerCrews);

    return CompanyResponseModel.fromJson(json.encode(response.data));
  }

  Future<Response> addCrew(CrewModel crewModel) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.companiesAddcrew, data: crewModel.toJson());

    return response;
  }

  Future<Response> editCrew(CrewModel crewModel) async {
    Response response = await httpClient.dio
        .patch(EndpointConfig.companiesUpdatecrew, data: crewModel.toJson());

    return response;
  }

  Future<Response> deleteCrew(CrewModel crewModel) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.companiesDeletecrew, data: crewModel.toJson());

    return response;
  }
}
