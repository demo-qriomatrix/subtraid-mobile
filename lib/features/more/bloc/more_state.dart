part of 'more_bloc.dart';

class MoreState extends Equatable {
  final int selectedIndex;

  MoreState({
    @required this.selectedIndex,
  });

  @override
  List<Object> get props => [selectedIndex];
}
