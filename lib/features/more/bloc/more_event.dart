part of 'more_bloc.dart';

abstract class MoreEvent extends Equatable {
  const MoreEvent();

  @override
  List<Object> get props => [];
}

class MoreIndexChanged extends MoreEvent {
  final int index;

  MoreIndexChanged({@required this.index});
}
