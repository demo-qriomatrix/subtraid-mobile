import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/features/more/bloc/more_bloc.dart';
import 'package:Subtraid/features/more/view/screens.dart';

class MoreScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoreBloc, MoreState>(
      builder: (context, state) {
        if (state.selectedIndex == null) return Container();

        return morePageList[state.selectedIndex];

        //  Stack(
        //   fit: StackFit.expand,
        //   children: [
        //     Offstage(
        //         offstage: state.selectedIndex != 0, child: morePageList[0]),
        //     Offstage(
        //       offstage: state.selectedIndex != 1,
        //       child: morePageList[1],
        //     ),
        //     Offstage(
        //       offstage: state.selectedIndex != 2,
        //       child: morePageList[2],
        //     ),
        //     Offstage(
        //       offstage: state.selectedIndex != 3,
        //       child: morePageList[3],
        //     ),
        //     Offstage(
        //       offstage: state.selectedIndex != 4,
        //       child: morePageList[4],
        //     ),
        //   ],
        // );
      },
    );
  }
}
