import 'package:Subtraid/features/more/company/view/company_screen.dart';
import 'package:Subtraid/features/more/help/view/help_screen.dart';
import 'package:Subtraid/features/more/timesheets/view/timesheets_screen.dart';
import 'package:Subtraid/features/my_profile/view/my_profile_screen.dart';
import 'package:flutter/material.dart';

List<GlobalKey<NavigatorState>> moreNavKeys = [];

List<Widget> morePageList = [
  // ProjectsScreen(),
  CompanyScreen(),
  TimesheetsScreen(),
  // CalenderScreen(),
  HelpScreen(),
  MyProfileScreen(),
].map((e) {
  GlobalKey<NavigatorState> key = GlobalKey<NavigatorState>();
  print(e);
  moreNavKeys.add(key);

  return Navigator(
      key: key,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(builder: (context) {
          return e;
        });
      });
}).toList();
