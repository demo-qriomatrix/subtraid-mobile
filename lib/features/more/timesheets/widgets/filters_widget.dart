import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/more/company/features/crew/bloc/crew/crew_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/employee/bloc/employee_bloc.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:Subtraid/features/projects/features/project_list/bloc/project_list_bloc.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_model.dart';
import 'package:Subtraid/features/more/timesheets/bloc/company_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheets_filters_model.dart';
import 'package:Subtraid/features/more/timesheets/features/widgets/timesheet_dialog.dart';
import 'package:Subtraid/features/shared/widgets/custom_expansion_tile2.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';

class TimesheetFiltersWidget extends StatefulWidget {
  @override
  _TimesheetFiltersWidgetState createState() => _TimesheetFiltersWidgetState();
}

class _TimesheetFiltersWidgetState extends State<TimesheetFiltersWidget> {
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();

  TextEditingController member = TextEditingController();

  Set<EmployeeModel> selectedMembers = Set();

  Set<ProjectListItemModel> selectedProjects = Set();

  Set<CrewModel> selectedCrews = Set();

  Set<String> selectedStatuses = Set();

  @override
  void initState() {
    super.initState();

    CompanyTimesheetsFiltersModel filters =
        context.read<CompanyTimesheetsBloc>().state.filters;

    startDate.text = DateFormat('MM/dd/yyyy').format(DateTime.parse(
        context.read<CompanyTimesheetsBloc>().state.filters.start));

    endDate.text = DateFormat('MM/dd/yyyy').format(DateTime.parse(
        context.read<CompanyTimesheetsBloc>().state.filters.end));

    selectedMembers = filters.employees.toSet();

    selectedProjects = filters.projects.toSet();

    if (filters.status != null)
      selectedStatuses.add(getStatusStringFromStatus(filters.status));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProjectListBloc>(
          create: (context) => ProjectListBloc(projectsRepository: sl()),
          lazy: false,
        ),
        BlocProvider<EmployeeBloc>(
          create: (context) => EmployeeBloc(employeeRepository: sl()),
          lazy: false,
        ),
        BlocProvider<CrewBloc>(
          create: (context) => CrewBloc(crewRepository: sl()),
          lazy: false,
        )
      ],
      child: InkWell(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: BlocBuilder<CompanyTimesheetsBloc, CompanyTimesheetsState>(
          builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 14,
                        backgroundColor: ColorConfig.orange,
                        child: ImageIcon(
                          AssetImage(LocalImages.filter),
                          color: Colors.white,
                          size: 14,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Select Filters',
                        style: TextStyle(
                            color: ColorConfig.primary,
                            fontWeight: FontWeight.w600),
                      ),
                      Spacer(),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: RawMaterialButton(
                              shape: CircleBorder(),
                              constraints: BoxConstraints(),
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              fillColor: Colors.white,
                              onPressed: () {
                                context
                                    .read<CompanyTimesheetsBloc>()
                                    .add(CompanyTimesheetsToggleFilter());
                              },
                              padding: EdgeInsets.all(5),
                              child: Icon(
                                Icons.close,
                                color: Colors.black,
                                size: 20,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: RawMaterialButton(
                              shape: CircleBorder(),
                              constraints: BoxConstraints(),
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              fillColor: ColorConfig.green2,
                              onPressed: () {
                                CompanyTimesheetsFiltersModel filter =
                                    CompanyTimesheetsFiltersModel((data) => data
                                      ..start = DateFormat("MM/dd/yyyy")
                                          .parse(startDate.text)
                                          .toIso8601String()
                                      ..end = DateFormat("MM/dd/yyyy")
                                          .parse(endDate.text)
                                          .toIso8601String()
                                      ..status = selectedStatuses.isNotEmpty
                                          ? getStatusIntFromStatus(
                                              selectedStatuses.first)
                                          : null
                                      ..employees = ListBuilder(selectedMembers)
                                      ..projects =
                                          ListBuilder(selectedProjects));

                                context.read<CompanyTimesheetsBloc>().add(
                                    CompanyTimesheetsUpdateFilter(
                                        filter: filter));
                              },
                              padding: EdgeInsets.all(5),
                              child: Icon(
                                Icons.done,
                                color: Colors.white,
                                size: 20,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Column(
                    children: [
                      CustomExpansionTile2(
                        // backgroundColor: Colors.black,
                        title: Text('Projects',
                            style: TextStyle(color: ColorConfig.primary)),
                        tilePadding: EdgeInsets.zero,
                        childrenPadding: EdgeInsets.zero,
                        initiallyExpanded: selectedProjects.isNotEmpty,
                        children: [
                          selectedProjects.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Wrap(
                                          spacing: 10,
                                          crossAxisAlignment:
                                              WrapCrossAlignment.start,
                                          alignment: WrapAlignment.start,
                                          children: selectedProjects
                                              .map((e) => Chip(
                                                    label: Text(e.name),
                                                    onDeleted: () {
                                                      setState(() {
                                                        selectedProjects
                                                            .remove(e);
                                                      });
                                                    },
                                                  ))
                                              .toList(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          BlocBuilder<ProjectListBloc, ProjectListState>(
                            builder: (context, state) {
                              return TypeAheadFormField(
                                  enabled: true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                    controller: member,
                                    decoration: InputDecoration(
                                      fillColor: ColorConfig.blue1,
                                      filled: true,
                                      labelText: 'Search by Projects',
                                      labelStyle: TextStyle(
                                          color: ColorConfig.black1,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide.none),
                                    ),
                                  ),
                                  suggestionsCallback: (pattern) {
                                    if (state is ProjectsLoadSuccess) {
                                      return state.projects
                                          .where((element) => element.name
                                              .toLowerCase()
                                              .contains(pattern.toLowerCase()))
                                          .toList();
                                    }
                                    return null;
                                  },
                                  itemBuilder: (context,
                                      ProjectListItemModel suggestion) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Row(
                                        children: [
                                          Theme(
                                            data: ThemeData(
                                                unselectedWidgetColor:
                                                    ColorConfig.orange),
                                            child: IgnorePointer(
                                              child: Checkbox(
                                                  value: selectedProjects
                                                      .where((project) =>
                                                          project.id ==
                                                          suggestion.id)
                                                      .isNotEmpty,
                                                  onChanged: (val) {
                                                    if (val) {
                                                      setState(() {
                                                        selectedProjects
                                                            .add(suggestion);
                                                      });
                                                    } else {
                                                      setState(() {
                                                        selectedProjects.remove(
                                                            selectedProjects
                                                                .where((project) =>
                                                                    project
                                                                        .id ==
                                                                    suggestion
                                                                        .id)
                                                                .first);
                                                      });
                                                    }
                                                  }),
                                            ),
                                          ),
                                          Text(suggestion.name)
                                        ],
                                      ),
                                    );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    if (selectedProjects
                                        .where((project) =>
                                            project.id == suggestion.id)
                                        .isEmpty) {
                                      setState(() {
                                        selectedProjects.add(suggestion);
                                      });
                                    } else {
                                      setState(() {
                                        selectedProjects.remove(selectedProjects
                                            .where((project) =>
                                                project.id == suggestion.id)
                                            .first);
                                      });
                                    }
                                  },
                                  validator: (value) {
                                    return null;
                                  },
                                  onSaved: (value) {});
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      CustomExpansionTile2(
                        // backgroundColor: Colors.black,
                        title: Text('Employee',
                            style: TextStyle(color: ColorConfig.primary)),
                        tilePadding: EdgeInsets.zero,
                        childrenPadding: EdgeInsets.zero,
                        initiallyExpanded: selectedMembers.isNotEmpty,
                        children: [
                          selectedMembers.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Wrap(
                                          spacing: 10,
                                          crossAxisAlignment:
                                              WrapCrossAlignment.start,
                                          alignment: WrapAlignment.start,
                                          children: selectedMembers
                                              .map((e) => Chip(
                                                    label: Text(e.user.name),
                                                    onDeleted: () {
                                                      setState(() {
                                                        selectedMembers
                                                            .remove(e);
                                                      });
                                                    },
                                                  ))
                                              .toList(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          BlocBuilder<EmployeeBloc, EmployeeState>(
                            builder: (context, state) {
                              return TypeAheadFormField(
                                  enabled: true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                    controller: member,
                                    decoration: InputDecoration(
                                      fillColor: ColorConfig.blue1,
                                      filled: true,
                                      labelText: 'Search by Employees',
                                      labelStyle: TextStyle(
                                          color: ColorConfig.black1,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide.none),
                                    ),
                                  ),
                                  suggestionsCallback: (pattern) {
                                    if (state is EmployeeLoadSuccess) {
                                      return state.companyInfo.company.employees
                                          .where((element) => element.user.name
                                              .toLowerCase()
                                              .contains(pattern.toLowerCase()))
                                          .toList();
                                    }
                                    return null;
                                  },
                                  itemBuilder:
                                      (context, EmployeeModel suggestion) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Row(
                                        children: [
                                          Theme(
                                            data: ThemeData(
                                                unselectedWidgetColor:
                                                    ColorConfig.orange),
                                            child: IgnorePointer(
                                              child: Checkbox(
                                                  value: selectedMembers
                                                      .where((mem) =>
                                                          mem.user.id ==
                                                          suggestion.user.id)
                                                      .isNotEmpty,
                                                  onChanged: (val) {
                                                    if (val) {
                                                      setState(() {
                                                        selectedMembers
                                                            .add(suggestion);
                                                      });
                                                    } else {
                                                      setState(() {
                                                        selectedMembers.remove(
                                                            selectedMembers
                                                                .where((mem) =>
                                                                    mem.user
                                                                        .id ==
                                                                    suggestion
                                                                        .user
                                                                        .id)
                                                                .first);
                                                      });
                                                    }
                                                  }),
                                            ),
                                          ),
                                          CircleAvatar(
                                            backgroundImage: AssetImage(
                                                LocalImages.defaultAvatar),
                                            radius: 16,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(suggestion.user.name)
                                        ],
                                      ),
                                    );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    if (selectedMembers
                                        .where((mem) =>
                                            mem.user.id == suggestion.user.id)
                                        .isEmpty) {
                                      setState(() {
                                        selectedMembers.add(suggestion);
                                      });
                                    } else {
                                      setState(() {
                                        selectedMembers.remove(selectedMembers
                                            .where((mem) =>
                                                mem.user.id ==
                                                suggestion.user.id)
                                            .first);
                                      });
                                    }
                                  },
                                  validator: (value) {
                                    return null;
                                  },
                                  onSaved: (value) {});
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      CustomExpansionTile2(
                        // backgroundColor: Colors.black,
                        title: Text('Crews',
                            style: TextStyle(color: ColorConfig.primary)),
                        tilePadding: EdgeInsets.zero,
                        childrenPadding: EdgeInsets.zero,
                        initiallyExpanded: selectedCrews.isNotEmpty,
                        children: [
                          selectedCrews.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Wrap(
                                          spacing: 10,
                                          crossAxisAlignment:
                                              WrapCrossAlignment.start,
                                          alignment: WrapAlignment.start,
                                          children: selectedCrews
                                              .map((e) => Chip(
                                                    label: Text(e.name),
                                                    onDeleted: () {
                                                      setState(() {
                                                        selectedCrews.remove(e);
                                                      });
                                                    },
                                                  ))
                                              .toList(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          BlocBuilder<CrewBloc, CrewState>(
                            builder: (context, state) {
                              return TypeAheadFormField(
                                  enabled: true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                    controller: member,
                                    decoration: InputDecoration(
                                      fillColor: ColorConfig.blue1,
                                      filled: true,
                                      labelText: 'Search by Crews',
                                      labelStyle: TextStyle(
                                          color: ColorConfig.black1,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide.none),
                                    ),
                                  ),
                                  suggestionsCallback: (pattern) {
                                    if (state is CrewLoadSuccess) {
                                      return state.companyInfo.company.crews
                                          .where((element) => element.name
                                              .toLowerCase()
                                              .contains(pattern.toLowerCase()))
                                          .toList();
                                    }
                                    return null;
                                  },
                                  itemBuilder: (context, CrewModel suggestion) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Row(
                                        children: [
                                          Theme(
                                            data: ThemeData(
                                                unselectedWidgetColor:
                                                    ColorConfig.orange),
                                            child: IgnorePointer(
                                              child: Checkbox(
                                                  value: selectedCrews
                                                      .where((crew) =>
                                                          crew.id ==
                                                          suggestion.id)
                                                      .isNotEmpty,
                                                  onChanged: (val) {
                                                    if (val) {
                                                      setState(() {
                                                        selectedCrews
                                                            .add(suggestion);
                                                      });
                                                    } else {
                                                      setState(() {
                                                        selectedCrews.remove(
                                                            selectedCrews
                                                                .where((crew) =>
                                                                    crew.id ==
                                                                    suggestion
                                                                        .id)
                                                                .first);
                                                      });
                                                    }
                                                  }),
                                            ),
                                          ),
                                          CircleAvatar(
                                            backgroundImage: AssetImage(
                                                LocalImages.defaultAvatar),
                                            radius: 16,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(suggestion.name)
                                        ],
                                      ),
                                    );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected: (suggestion) {
                                    if (selectedCrews
                                        .where(
                                            (crew) => crew.id == suggestion.id)
                                        .isEmpty) {
                                      setState(() {
                                        selectedCrews.add(suggestion);
                                      });
                                    } else {
                                      setState(() {
                                        selectedCrews.remove(selectedCrews
                                            .where((crew) =>
                                                crew.id == suggestion.id)
                                            .first);
                                      });
                                    }
                                  },
                                  validator: (value) {
                                    return null;
                                  },
                                  onSaved: (value) {});
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      CustomExpansionTile2(
                        // backgroundColor: Colors.black,
                        title: Text('Status',
                            style: TextStyle(color: ColorConfig.primary)),
                        tilePadding: EdgeInsets.zero,
                        childrenPadding: EdgeInsets.zero,
                        initiallyExpanded: selectedStatuses.isNotEmpty,
                        children: [
                          selectedStatuses.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Wrap(
                                          spacing: 10,
                                          crossAxisAlignment:
                                              WrapCrossAlignment.start,
                                          alignment: WrapAlignment.start,
                                          children: selectedStatuses
                                              .map((e) => Chip(
                                                    label: Text(e),
                                                    onDeleted: () {
                                                      setState(() {
                                                        selectedStatuses
                                                            .remove(e);
                                                      });
                                                    },
                                                  ))
                                              .toList(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          TypeAheadFormField(
                              enabled: true,
                              textFieldConfiguration: TextFieldConfiguration(
                                controller: member,
                                decoration: InputDecoration(
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  labelText: 'Search by Status',
                                  labelStyle: TextStyle(
                                      color: ColorConfig.black1,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide.none),
                                ),
                              ),
                              suggestionsCallback: (pattern) {
                                return statues
                                    .where((element) => element
                                        .toLowerCase()
                                        .contains(pattern.toLowerCase()))
                                    .toList();
                              },
                              itemBuilder: (context, String suggestion) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: Row(
                                    children: [
                                      Theme(
                                        data: ThemeData(
                                            unselectedWidgetColor:
                                                ColorConfig.orange),
                                        child: IgnorePointer(
                                          child: Checkbox(
                                              value: selectedStatuses
                                                  .where((status) =>
                                                      status == suggestion)
                                                  .isNotEmpty,
                                              onChanged: (val) {
                                                if (val) {
                                                  setState(() {
                                                    selectedStatuses
                                                        .add(suggestion);
                                                  });
                                                } else {
                                                  setState(() {
                                                    selectedStatuses.remove(
                                                        selectedStatuses
                                                            .where((status) =>
                                                                status ==
                                                                suggestion)
                                                            .first);
                                                  });
                                                }
                                              }),
                                        ),
                                      ),
                                      Text(suggestion)
                                    ],
                                  ),
                                );
                              },
                              transitionBuilder:
                                  (context, suggestionsBox, controller) {
                                return suggestionsBox;
                              },
                              onSuggestionSelected: (String suggestion) {
                                if (selectedStatuses
                                    .where((status) => status == suggestion)
                                    .isEmpty) {
                                  setState(() {
                                    selectedStatuses = Set();
                                    selectedStatuses.add(suggestion);
                                  });
                                } else {
                                  setState(() {
                                    selectedStatuses.remove(selectedStatuses
                                        .where((status) => status == suggestion)
                                        .first);
                                  });
                                }
                              },
                              validator: (value) {
                                return null;
                              },
                              onSaved: (value) {})
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Start Date'),
                      TextFormField(
                        onTap: () {
                          showDatePicker(
                            context: context,
                            firstDate:
                                DateTime.now().subtract(Duration(days: 3650)),
                            initialDate: DateTime.parse(state.filters.start),
                            lastDate: DateTime.now().add(Duration(days: 3650)),
                          ).then((value) {
                            if (value != null) {
                              startDate.text =
                                  DateFormat('MM/dd/yyyy').format(value);
                            }
                          });
                        },
                        readOnly: true,
                        controller: startDate,
                        decoration: InputDecoration(
                          suffix: ImageIcon(
                            AssetImage(LocalImages.calenderAlt),
                            color: ColorConfig.primary,
                            size: 16,
                          ),
                          border: UnderlineInputBorder(),
                        ),
                        maxLines: null,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.black.withOpacity(0.74),
                            fontSize: 14),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('End Date'),
                      TextFormField(
                        onTap: () {
                          showDatePicker(
                            context: context,
                            firstDate:
                                DateTime.now().subtract(Duration(days: 3650)),
                            initialDate: DateTime.parse(state.filters.end),
                            lastDate: DateTime.now().add(Duration(days: 3650)),
                          ).then((value) {
                            if (value != null) {
                              endDate.text =
                                  DateFormat('MM/dd/yyyy').format(value);
                            }
                          });
                        },
                        readOnly: true,
                        controller: endDate,
                        decoration: InputDecoration(
                          suffix: ImageIcon(
                            AssetImage(LocalImages.calenderAlt),
                            color: ColorConfig.primary,
                            size: 16,
                          ),
                          border: UnderlineInputBorder(),
                        ),
                        maxLines: null,
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.black.withOpacity(0.74),
                            fontSize: 14),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

List<String> statues = ['VALID', 'INVALID', 'ACTIVE'];
