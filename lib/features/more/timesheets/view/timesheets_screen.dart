import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/more/timesheets/bloc/company_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/payroll/view/payroll_timesheets_screen.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/bloc/projects_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/bloc/timesheet_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/view/projects_timesheets_screen.dart';
import 'package:Subtraid/features/more/timesheets/widgets/filters_widget.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TimesheetsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CompanyTimesheetsBloc>(
          create: (context) => CompanyTimesheetsBloc(),
          lazy: false,
        ),
        BlocProvider<TimesheetBloc>(
          lazy: false,
          create: (context) =>
              TimesheetBloc(projectsTimesheetsRepository: sl()),
        ),
        BlocProvider<ProjectsTimesheetsBloc>(
          create: (context) => ProjectsTimesheetsBloc(
              projectsTimesheetsRepository: sl(),
              companyTimesheetsBloc: context.read<CompanyTimesheetsBloc>()),
          lazy: false,
        )
      ],
      child: BlocListener<TimesheetBloc, TimesheetState>(
        listener: (context, state) {
          if (state is TimesheetUpdateInProgress) {
            context.read<GlobalBloc>().add(ShowLoadingWidget());
          }
          if (state is TimesheetDeleteSuccess) {
            context
                .read<ProjectsTimesheetsBloc>()
                .add(ProjectsTimesheetsRequested());

            context.read<GlobalBloc>().add(HideLoadingWidget());

            context.read<GlobalBloc>().add(ShowSuccessSnackBar(
                message: 'Timesheet deleted successfully', event: null));
          }
          if (state is TimesheetDeleteFailure) {
            context.read<GlobalBloc>().add(HideLoadingWidget());

            context
                .read<GlobalBloc>()
                .add(ShowErrorSnackBar(error: 'Something went wrong'));
          }
        },
        child: BlocBuilder<CompanyTimesheetsBloc, CompanyTimesheetsState>(
          builder: (context, state) => Container(
            color: ColorConfig.primary,
            child: Column(
              children: [
                StAppBar(
                  title: 'Company Timesheets',
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: TextField(
                    keyboardType: TextInputType.text,
                    decoration: searchInputDecoration,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                    child: Stack(children: [
                  Container(
                      margin: EdgeInsets.only(top: 14),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50),
                              topRight: Radius.circular(50))),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minHeight: getMinHeight(context),
                        ),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            // Row(
                            //   children: <Widget>[
                            //     SizedBox(
                            //       width: 20,
                            //     ),
                            //     CustomIcon(
                            //       icon: LocalImages.employee,
                            //       title: 'Projects',
                            //       selected: state.selectedIndex == 0,
                            //       ontap: () {
                            //         context.read<CompanyTimesheetsBloc>().add(
                            //             CompanyTimesheetsIndexChanged(index: 0));
                            //       },
                            //     ),
                            //     SizedBox(
                            //       width: 20,
                            //     ),
                            //     CustomIcon(
                            //       icon: LocalImages.crew,
                            //       title: 'Payroll',
                            //       selected: state.selectedIndex == 1,
                            //       ontap: () {
                            //         context.read<CompanyTimesheetsBloc>().add(
                            //             CompanyTimesheetsIndexChanged(index: 1));
                            //       },
                            //     ),
                            //   ],
                            // ),
                            // SizedBox(
                            //   height: 10,
                            // ),

                            state.filterMode
                                ? Expanded(child: TimesheetFiltersWidget())
                                : Expanded(
                                    child: [
                                    ProjectsTimesheetsScreen(),
                                    PayrollTimesheetsScreen()
                                  ][state.selectedIndex])
                          ],
                        ),
                      )),
                  Positioned(
                    right: 36,
                    child: state.filterMode
                        ? Container()

                        // Row(

                        //     children: [
                        //       Padding(
                        //         padding:
                        //             const EdgeInsets.symmetric(horizontal: 10),
                        //         child: RawMaterialButton(
                        //           shape: CircleBorder(),
                        //           constraints: BoxConstraints(),
                        //           materialTapTargetSize:
                        //               MaterialTapTargetSize.shrinkWrap,
                        //           fillColor: Colors.white,
                        //           onPressed: () {
                        //             context
                        //                 .read<CompanyTimesheetsBloc>()
                        //                 .add(CompanyTimesheetsToggleFilter());
                        //           },
                        //           padding: EdgeInsets.all(5),
                        //           child: Icon(
                        //             Icons.close,
                        //             color: Colors.black,
                        //             size: 20,
                        //           ),
                        //         ),
                        //       ),
                        //       Padding(
                        //         padding:
                        //             const EdgeInsets.symmetric(horizontal: 10),
                        //         child: RawMaterialButton(
                        //           shape: CircleBorder(),
                        //           constraints: BoxConstraints(),
                        //           materialTapTargetSize:
                        //               MaterialTapTargetSize.shrinkWrap,
                        //           fillColor: ColorConfig.green2,
                        //           onPressed: () {
                        //             context
                        //                 .read<CompanyTimesheetsBloc>()
                        //                 .add(CompanyTimesheetsToggleFilter());
                        //           },
                        //           padding: EdgeInsets.all(5),
                        //           child: Icon(
                        //             Icons.done,
                        //             color: Colors.white,
                        //             size: 20,
                        //           ),
                        //         ),
                        //       ),
                        //     ],
                        //   )
                        : Row(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: RawMaterialButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  constraints: BoxConstraints(),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  fillColor: state.filterMode
                                      ? ColorConfig.orange
                                      : Colors.white,
                                  onPressed: () {
                                    context
                                        .read<CompanyTimesheetsBloc>()
                                        .add(CompanyTimesheetsToggleFilter());
                                  },
                                  padding: EdgeInsets.all(5),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.filter),
                                    color: state.filterMode
                                        ? Colors.white
                                        : ColorConfig.primary,
                                    size: 18,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: RawMaterialButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  constraints: BoxConstraints(),
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  fillColor: Colors.white,
                                  onPressed: () {
                                    TimesheetBloc timesheetBloc =
                                        context.read<TimesheetBloc>();

                                    ProjectsTimesheetsBloc
                                        projectsTimesheetsBloc =
                                        context.read<ProjectsTimesheetsBloc>();

                                    ProjectsTimesheetsState state =
                                        projectsTimesheetsBloc.state;

                                    List<String> ids;

                                    if (state
                                        is ProjectsTimesheetsLoadSuccess) {
                                      ids = state.timesheets
                                          .where((element) =>
                                              (element?.selected ?? false) ==
                                              true)
                                          .map((e) => e.id)
                                          .toList();
                                    }

                                    if (ids.isNotEmpty)
                                      showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (context) => Dialog(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          backgroundColor: Colors.white,
                                          child: Container(
                                            width: 0,
                                            padding: EdgeInsets.all(20),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      backgroundColor:
                                                          ColorConfig.orange,
                                                      child: ImageIcon(
                                                        AssetImage(LocalImages
                                                            .exclamation),
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 20,
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        'Are you sure you want to delete this timesheets. All data will be lost.',
                                                        style: TextStyle(
                                                            color: ColorConfig
                                                                .primary,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w500),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    FlatButton(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                      color: ColorConfig.grey3,
                                                      child: Text(
                                                        'No',
                                                        style: TextStyle(
                                                                fontSize: 12)
                                                            .copyWith(
                                                                color:
                                                                    ColorConfig
                                                                        .grey2),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    FlatButton(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5)),
                                                      color:
                                                          ColorConfig.primary,
                                                      child: Text(
                                                        'Yes',
                                                        style: TextStyle(
                                                                fontSize: 12)
                                                            .copyWith(
                                                                color: Colors
                                                                    .white),
                                                      ),
                                                      onPressed: () {
                                                        timesheetBloc.add(
                                                            DeleteTimesheetsEvent(
                                                                ids: ids));

                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                  },
                                  padding: EdgeInsets.all(5),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.trashAlt),
                                    color: ColorConfig.red1,
                                    size: 18,
                                  ),
                                ),
                              ),
                            ],
                          ),

                    // RawMaterialButton(
                    //   padding: EdgeInsets.all(6),
                    //   materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    //   shape: CircleBorder(),
                    //   constraints: BoxConstraints(),
                    //   onPressed: () {},
                    //   fillColor: ColorConfig.orange,
                    //   child: Icon(Icons.add, size: 28, color: Colors.white),
                    // ),
                  ),
                ]))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
