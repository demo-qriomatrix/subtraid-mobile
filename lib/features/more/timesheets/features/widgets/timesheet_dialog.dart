import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/bloc/projects_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/bloc/timesheet_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_ops_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_update_model.dart';
import 'package:Subtraid/features/more/timesheets/features/widgets/project_timesheet_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class TimeSheetDialog extends StatefulWidget {
  final CompanyTimesheetModel timesheetModel;
  final ProjectsTimesheetsBloc projectsTimesheetsBloc;

  TimeSheetDialog({
    @required this.timesheetModel,
    @required this.projectsTimesheetsBloc,
  });

  @override
  _TimeSheetDialogState createState() => _TimeSheetDialogState();
}

class _TimeSheetDialogState extends State<TimeSheetDialog> {
  bool modify = false;

  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController startTime = TextEditingController();
  TextEditingController endTime = TextEditingController();

  String status;

  @override
  void initState() {
    super.initState();

    startDate.text = DateFormat('MM/dd/yyyy')
        .format(DateTime.parse(widget.timesheetModel.start));
    endDate.text = DateFormat('MM/dd/yyyy')
        .format(DateTime.parse(widget.timesheetModel.end));
    startTime.text = DateFormat('hh:mm a')
        .format(DateTime.parse(widget.timesheetModel.start));
    endTime.text =
        DateFormat('hh:mm a').format(DateTime.parse(widget.timesheetModel.end));

    status = getStatusStringFromStatus(widget.timesheetModel.status)
            ?.toUpperCase() ??
        null;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<TimesheetBloc, TimesheetState>(
      listener: (context, state) {
        if (state is TimesheetUpdateInProgress) {
          context.read<GlobalBloc>().add(ShowLoadingWidget());
        }

        if (state is TimesheetUpdateSuccess) {
          widget.projectsTimesheetsBloc.add(ProjectsTimesheetsRequested());

          context.read<GlobalBloc>().add(HideLoadingWidget());

          Navigator.of(context).pop(true);

          context.read<GlobalBloc>().add(ShowSuccessSnackBar(
              message: 'Timesheet updated successfully', event: null));
        }
        if (state is TimesheetDeleteSuccess) {
          widget.projectsTimesheetsBloc.add(ProjectsTimesheetsRequested());

          context.read<GlobalBloc>().add(HideLoadingWidget());

          Navigator.of(context).pop(true);

          context.read<GlobalBloc>().add(ShowSuccessSnackBar(
              message: 'Timesheet deleted successfully', event: null));
        }
        if (state is TimesheetUpdateFailure ||
            state is TimesheetDeleteFailure) {
          context.read<GlobalBloc>().add(HideLoadingWidget());

          Navigator.pop(context);
          context
              .read<GlobalBloc>()
              .add(ShowErrorSnackBar(error: 'Something went wrong'));
        }
      },
      child: Container(
        height: MediaQuery.of(context).size.height * 0.7,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                ImageIcon(
                  AssetImage(LocalImages.checkSquare),
                  color: getColorFromStatus(widget.timesheetModel.status),
                  size: 16,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  getStatusStringFromStatus(widget.timesheetModel.status)
                      .toUpperCase(),
                  style: TextStyle(
                      color: getColorFromStatus(widget.timesheetModel.status),
                      fontSize: 13,
                      fontWeight: FontWeight.w600),
                ),
                Spacer(),
                IconButton(
                    padding: EdgeInsets.zero,
                    constraints: BoxConstraints(),
                    icon: Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    })
              ],
            ),
            SizedBox(
              height: 20,
            ),
            widget.timesheetModel.card != null
                ? Column(
                    children: [
                      Text(
                        widget.timesheetModel.card.name,
                        style: TextStyle(
                            color: ColorConfig.primary,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(color: ColorConfig.blue18),
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        child: Row(
                          children: [
                            Text(
                              'Card Name : ' + widget.timesheetModel.card.name,
                              style: TextStyle(
                                  fontSize: 14, color: ColorConfig.primary),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  )
                : Container(),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.all(5),
                  child: CircleAvatar(
                    backgroundImage: AssetImage(LocalImages.defaultAvatar),
                    radius: 15,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    widget.timesheetModel?.user?.name ?? '',
                    style: TextStyle(color: Colors.black, fontSize: 14),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            modify
                ? Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Select by status'),
                            DropdownButtonFormField(
                              dropdownColor: ColorConfig.primary,
                              icon: Padding(
                                  padding: const EdgeInsets.only(left: 40),
                                  child: Icon(
                                    Icons.keyboard_arrow_down,
                                  )),
                              value: status,
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: ColorConfig.black2)),
                              ),
                              iconEnabledColor: Colors.black,
                              iconDisabledColor: Colors.black,
                              items: ['VALID', 'INVALID', 'ACTIVE']
                                  .map((e) => DropdownMenuItem(
                                        value: e,
                                        child: Text(
                                          e,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.white),
                                        ),
                                      ))
                                  .toList(),
                              selectedItemBuilder: (context) =>
                                  ['VALID', 'INVALID', 'ACTIVE']
                                      .map((e) => DropdownMenuItem(
                                            value: e,
                                            child: Text(
                                              e,
                                              style: TextStyle(
                                                  color: ColorConfig.black1,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ))
                                      .toList(),
                              onChanged: (val) {
                                status = val;
                              },
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Start Date'),
                                  TextFormField(
                                    onTap: () {
                                      showDatePicker(
                                        context: context,
                                        firstDate: DateTime.now()
                                            .subtract(Duration(days: 3650)),
                                        initialDate: DateTime.parse(
                                            widget.timesheetModel.start),
                                        lastDate: DateTime.now()
                                            .add(Duration(days: 3650)),
                                      ).then((value) {
                                        if (value != null) {
                                          startDate.text =
                                              DateFormat('MM/dd/yyyy')
                                                  .format(value);
                                        }
                                      });
                                    },
                                    readOnly: true,
                                    controller: startDate,
                                    decoration: InputDecoration(
                                      suffix: ImageIcon(
                                        AssetImage(LocalImages.calenderAlt),
                                        color: ColorConfig.primary,
                                        size: 16,
                                      ),
                                      border: UnderlineInputBorder(),
                                    ),
                                    maxLines: null,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black.withOpacity(0.74),
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Start Time'),
                                  TextFormField(
                                    onTap: () {
                                      showTimePicker(
                                              context: context,
                                              initialTime:
                                                  TimeOfDay.fromDateTime(
                                                      DateTime.parse(widget
                                                          .timesheetModel
                                                          .start)))
                                          .then((value) {
                                        if (value != null) {
                                          startTime.text =
                                              value.format(context);
                                        }
                                      });
                                    },
                                    readOnly: true,
                                    controller: startTime,
                                    decoration: InputDecoration(
                                      suffix: ImageIcon(
                                        AssetImage(LocalImages.clock),
                                        color: ColorConfig.primary,
                                        size: 16,
                                      ),
                                      border: UnderlineInputBorder(),
                                    ),
                                    maxLines: null,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black.withOpacity(0.74),
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('End Date'),
                                  TextFormField(
                                    onTap: () {
                                      showDatePicker(
                                        context: context,
                                        firstDate: DateTime.now()
                                            .subtract(Duration(days: 3650)),
                                        initialDate: DateTime.parse(
                                            widget.timesheetModel.end),
                                        lastDate: DateTime.now()
                                            .add(Duration(days: 3650)),
                                      ).then((value) {
                                        if (value != null) {
                                          endDate.text =
                                              DateFormat('MM/dd/yyyy')
                                                  .format(value);
                                        }
                                      });
                                    },
                                    readOnly: true,
                                    controller: endDate,
                                    decoration: InputDecoration(
                                      suffix: ImageIcon(
                                        AssetImage(LocalImages.calenderAlt),
                                        color: ColorConfig.primary,
                                        size: 16,
                                      ),
                                      border: UnderlineInputBorder(),
                                    ),
                                    maxLines: null,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black.withOpacity(0.74),
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('End Time'),
                                  TextFormField(
                                    onTap: () {
                                      showTimePicker(
                                              context: context,
                                              initialTime:
                                                  TimeOfDay.fromDateTime(
                                                      DateTime.parse(widget
                                                          .timesheetModel.end)))
                                          .then((value) {
                                        if (value != null) {
                                          endTime.text = value.format(context);
                                        }
                                      });
                                    },
                                    readOnly: true,
                                    controller: endTime,
                                    decoration: InputDecoration(
                                      suffix: ImageIcon(
                                        AssetImage(LocalImages.clock),
                                        color: ColorConfig.primary,
                                        size: 16,
                                      ),
                                      border: UnderlineInputBorder(),
                                    ),
                                    maxLines: null,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black.withOpacity(0.74),
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                                color: ColorConfig.red1,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                onPressed: () {
                                  context
                                      .read<TimesheetBloc>()
                                      .add(DeleteTimesheetEvent(
                                        id: widget.timesheetModel.id,
                                      ));
                                },
                                child: Text(
                                  'Delete',
                                  style: TextStyle(color: Colors.white),
                                )),
                            SizedBox(
                              width: 10,
                            ),
                            FlatButton(
                                color: ColorConfig.grey17,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                onPressed: () {
                                  setState(() {
                                    modify = false;
                                  });
                                },
                                child: Text(
                                  'Cancel',
                                  style: TextStyle(color: ColorConfig.black1),
                                )),
                            SizedBox(
                              width: 10,
                            ),
                            FlatButton(
                                color: ColorConfig.primary,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                onPressed: () {
                                  TimesheetUpdateModel timesheetUpdateModel =
                                      TimesheetUpdateModel((data) => data
                                        ..status =
                                            getStatusIntFromStatus(status)
                                        ..start =
                                            DateFormat("MM/dd/yyyy hh:mm a")
                                                .parse(startDate.text +
                                                    ' ' +
                                                    startTime.text)
                                                .toIso8601String()
                                        ..end = DateFormat("MM/dd/yyyy hh:mm a")
                                            .parse(endDate.text +
                                                ' ' +
                                                endTime.text)
                                            .toIso8601String());

                                  TimesheetOpsModel timesheetOpsModel =
                                      TimesheetOpsModel((data) => data
                                        ..ops =
                                            timesheetUpdateModel.toBuilder());

                                  context.read<TimesheetBloc>().add(
                                      UpdateTimesheetEvent(
                                          id: widget.timesheetModel.id,
                                          data: timesheetOpsModel));
                                },
                                child: Text(
                                  'Update',
                                  style: TextStyle(color: Colors.white),
                                )),
                          ],
                        )
                      ],
                    ),
                  )
                : Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        widget.timesheetModel.start != null
                            ? Column(
                                children: [
                                  Text(
                                    'Start : ' +
                                        DateFormat('MMM dd, yyyy, hh:mm:ss aa')
                                            .format(DateTime.parse(
                                                widget.timesheetModel.start)),
                                    style: TextStyle(
                                        color: ColorConfig.primary,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              )
                            : Container(),
                        SizedBox(
                          height: 20,
                        ),
                        widget.timesheetModel.end != null
                            ? Text(
                                'End : ' +
                                    DateFormat('MMM dd, yyyy, hh:mm:ss aa')
                                        .format(DateTime.parse(
                                            widget.timesheetModel.end)),
                                style: TextStyle(
                                    color: ColorConfig.primary,
                                    fontWeight: FontWeight.w400),
                              )
                            : Container(),
                        SizedBox(
                          height: 20,
                        ),
                        widget.timesheetModel.start != null &&
                                widget.timesheetModel.end != null
                            ? Row(
                                children: [
                                  Text('Total Hours : ',
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontWeight: FontWeight.w600)),
                                  Text(getDuration(widget.timesheetModel),
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontWeight: FontWeight.w400)),
                                ],
                              )
                            : Container(),
                        Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                                color: ColorConfig.primary,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                onPressed: () {
                                  setState(() {
                                    modify = true;
                                  });
                                },
                                child: Text(
                                  'Modify',
                                  style: TextStyle(color: Colors.white),
                                )),
                          ],
                        )
                      ],
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}

String getDuration(CompanyTimesheetModel timesheetModel) {
  Duration diff = DateTime.parse(timesheetModel.end)
      .difference(DateTime.parse(timesheetModel.start));

  return format(diff);
}

format(Duration d) => d.toString().split('.').first.padLeft(8, "0");

String getStatusStringFromStatus(int status) {
  switch (status) {
    case 1:
      return 'ACTIVE';
      break;
    case 2:
      return 'VALID';
      break;
    case 3:
      return 'INVALID';
      break;
    default:
      return null;
  }
}

int getStatusIntFromStatus(String status) {
  switch (status) {
    case 'ACTIVE':
      return 1;
      break;
    case 'VALID':
      return 2;
      break;
    case 'INVALID':
      return 3;
      break;
    default:
      return 0;
  }
}
