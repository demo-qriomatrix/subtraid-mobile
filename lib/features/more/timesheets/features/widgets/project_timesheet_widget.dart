import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/bloc/projects_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/bloc/timesheet_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/widgets/timesheet_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProjectTimesheetWidget extends StatelessWidget {
  ProjectTimesheetWidget({@required this.timesheetModel});

  final CompanyTimesheetModel timesheetModel;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TimesheetBloc, TimesheetState>(
      listener: (context, state) {},
      builder: (context, state) => Container(
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  blurRadius: 12,
                  offset: Offset(0, 0),
                  color: ColorConfig.shadow1)
            ]),
        child: Material(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          child: InkWell(
            borderRadius: BorderRadius.circular(10),
            onTap: () {
              ProjectsTimesheetsBloc projectsTimesheetsBloc =
                  context.read<ProjectsTimesheetsBloc>();

              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (context) {
                    return Dialog(
                        insetPadding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        backgroundColor: Colors.white,
                        child: BlocProvider(
                          create: (context) =>
                              TimesheetBloc(projectsTimesheetsRepository: sl()),
                          child: TimeSheetDialog(
                            projectsTimesheetsBloc: projectsTimesheetsBloc,
                            timesheetModel: timesheetModel,
                          ),
                        ));
                  }).then((value) {});
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Row(
                children: <Widget>[
                  Theme(
                    data: Theme.of(context).copyWith(
                      unselectedWidgetColor: ColorConfig.orange,
                    ),
                    child: Checkbox(
                      onChanged: (val) {
                        context.read<ProjectsTimesheetsBloc>().add(
                            ProjectsTimesheetsSetSelected(
                                id: timesheetModel.id, selected: val));
                      },
                      value: (timesheetModel?.selected ?? false) == true,
                    ),
                  ),
                  timesheetModel.card != null
                      ? Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                timesheetModel?.card?.name ?? '',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 14),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundImage:
                                        AssetImage(LocalImages.defaultAvatar),
                                    radius: 10,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    timesheetModel?.user?.name ?? '',
                                    style: TextStyle(
                                        color: ColorConfig.black2,
                                        fontSize: 12),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      : Expanded(
                          child: Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: CircleAvatar(
                                  backgroundImage:
                                      AssetImage(LocalImages.defaultAvatar),
                                  radius: 15,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Text(
                                  timesheetModel?.user?.name ?? '',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                        ),
                  ImageIcon(
                    AssetImage(LocalImages.checkSquare),
                    color: getColorFromStatus(timesheetModel.status),
                    size: 16,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RawMaterialButton(
                    constraints: BoxConstraints(),
                    padding: EdgeInsets.all(12),
                    shape: CircleBorder(),
                    onPressed: () {
                      TimesheetBloc timesheetBloc =
                          context.read<TimesheetBloc>();

                      showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (context) => Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          backgroundColor: Colors.white,
                          child: Container(
                            width: 0,
                            padding: EdgeInsets.all(20),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    CircleAvatar(
                                      backgroundColor: ColorConfig.orange,
                                      child: ImageIcon(
                                        AssetImage(LocalImages.exclamation),
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Flexible(
                                      child: Text(
                                        'Are you sure you want to delete this timesheet. All data will be lost.',
                                        style: TextStyle(
                                            color: ColorConfig.primary,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      color: ColorConfig.grey3,
                                      child: Text(
                                        'No',
                                        style: TextStyle(fontSize: 12)
                                            .copyWith(color: ColorConfig.grey2),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      color: ColorConfig.primary,
                                      child: Text(
                                        'Yes',
                                        style: TextStyle(fontSize: 12)
                                            .copyWith(color: Colors.white),
                                      ),
                                      onPressed: () {
                                        timesheetBloc.add(DeleteTimesheetEvent(
                                            id: timesheetModel.id));
                                        Navigator.of(context).pop();
                                      },
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    child: ImageIcon(
                      AssetImage(LocalImages.trashAlt),
                      color: ColorConfig.red1,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Color getColorFromStatus(int status) {
  switch (status) {
    case 1:
      return ColorConfig.primary;
      break;
    case 2:
      return Colors.green;
      break;
    case 3:
      return ColorConfig.red1;
      break;
    default:
      return Colors.black;
  }
}
