import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/bloc/projects_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/widgets/project_timesheet_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProjectsTimesheetsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProjectsTimesheetsBloc, ProjectsTimesheetsState>(
        builder: (context, state) {
      if (state is ProjectsTimesheetsLoadInProgress) {
        return Center(child: CircularProgressIndicator());
      }

      if (state is ProjectsTimesheetsLoadSuccess) {
        return RefreshIndicator(
          onRefresh: () {
            context
                .read<ProjectsTimesheetsBloc>()
                .add(ProjectsTimesheetsRequested());
            return Future.delayed(Duration.zero);
          },
          child: state.timesheets.isEmpty
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'No timesheets found for the selected filters',
                          style: TextStyle(
                              color: ColorConfig.grey2,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    )
                  ],
                )
              : ListView(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  children: state.timesheets
                      .map((e) => ProjectTimesheetWidget(
                            timesheetModel: e,
                          ))
                      .toList()),
        );
      }

      return Container();
    });
  }
}
