import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheets_response.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/compony_timesheets_request_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/create_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_ops_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/model/create_project_timesheet_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class ProjectsTimesheetsRepository {
  final HttpClient httpClient;

  ProjectsTimesheetsRepository({@required this.httpClient});

  Future<CompanyTimesheetsResponse> getProjectTimesheets(
      CompanyTimesheetsRequestModel companyTimesheetsRequestModel) async {
    Response response = await httpClient.dio.post(
        EndpointConfig.timesheetsCompany,
        data: companyTimesheetsRequestModel.toJson());

    return CompanyTimesheetsResponse.fromJson(json.encode(response.data));
  }

  Future<Response> updateTimesheet(String id, TimesheetOpsModel data) async {
    Response response = await httpClient.dio
        .patch(EndpointConfig.timesheets + '/$id', data: data.toJson());

    // Response response = Response();

    return response;
  }

  Future<Response> deleteTimesheet(
    String id,
  ) async {
    Response response = await httpClient.dio.delete(
      EndpointConfig.timesheets + '/$id',
    );

    // Response response = Response();

    return response;
  }

  Future<Response> createTimesheetCard(
      CreateTimesheetModel createTimesheetModel) async {
    print(createTimesheetModel.toJson());
    Response response = await httpClient.dio.post(EndpointConfig.timesheetsCard,
        data: createTimesheetModel.toJson());

    return response;
  }

  Future<Response> createTimesheetProject(
      CreateProjectTimesheetModel createProjectTimesheetModel) async {
    print(createProjectTimesheetModel.toJson());
    Response response = await httpClient.dio.post(
        EndpointConfig.timesheetsProject,
        data: createProjectTimesheetModel.toJson());

    return response;
  }
}
