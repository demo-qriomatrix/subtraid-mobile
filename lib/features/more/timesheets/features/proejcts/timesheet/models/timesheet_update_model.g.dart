// GENERATED CODE - DO NOT MODIFY BY HAND

part of timesheet_update_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TimesheetUpdateModel> _$timesheetUpdateModelSerializer =
    new _$TimesheetUpdateModelSerializer();

class _$TimesheetUpdateModelSerializer
    implements StructuredSerializer<TimesheetUpdateModel> {
  @override
  final Iterable<Type> types = const [
    TimesheetUpdateModel,
    _$TimesheetUpdateModel
  ];
  @override
  final String wireName = 'TimesheetUpdateModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, TimesheetUpdateModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  TimesheetUpdateModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TimesheetUpdateModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$TimesheetUpdateModel extends TimesheetUpdateModel {
  @override
  final String end;
  @override
  final String start;
  @override
  final int status;

  factory _$TimesheetUpdateModel(
          [void Function(TimesheetUpdateModelBuilder) updates]) =>
      (new TimesheetUpdateModelBuilder()..update(updates)).build();

  _$TimesheetUpdateModel._({this.end, this.start, this.status}) : super._();

  @override
  TimesheetUpdateModel rebuild(
          void Function(TimesheetUpdateModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TimesheetUpdateModelBuilder toBuilder() =>
      new TimesheetUpdateModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TimesheetUpdateModel &&
        end == other.end &&
        start == other.start &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, end.hashCode), start.hashCode), status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TimesheetUpdateModel')
          ..add('end', end)
          ..add('start', start)
          ..add('status', status))
        .toString();
  }
}

class TimesheetUpdateModelBuilder
    implements Builder<TimesheetUpdateModel, TimesheetUpdateModelBuilder> {
  _$TimesheetUpdateModel _$v;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  TimesheetUpdateModelBuilder();

  TimesheetUpdateModelBuilder get _$this {
    if (_$v != null) {
      _end = _$v.end;
      _start = _$v.start;
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TimesheetUpdateModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TimesheetUpdateModel;
  }

  @override
  void update(void Function(TimesheetUpdateModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TimesheetUpdateModel build() {
    final _$result = _$v ??
        new _$TimesheetUpdateModel._(end: end, start: start, status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
