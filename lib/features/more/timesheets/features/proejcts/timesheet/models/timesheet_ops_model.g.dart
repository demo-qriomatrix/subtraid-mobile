// GENERATED CODE - DO NOT MODIFY BY HAND

part of timesheet_ops_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TimesheetOpsModel> _$timesheetOpsModelSerializer =
    new _$TimesheetOpsModelSerializer();

class _$TimesheetOpsModelSerializer
    implements StructuredSerializer<TimesheetOpsModel> {
  @override
  final Iterable<Type> types = const [TimesheetOpsModel, _$TimesheetOpsModel];
  @override
  final String wireName = 'TimesheetOpsModel';

  @override
  Iterable<Object> serialize(Serializers serializers, TimesheetOpsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.ops != null) {
      result
        ..add('ops')
        ..add(serializers.serialize(object.ops,
            specifiedType: const FullType(TimesheetUpdateModel)));
    }
    return result;
  }

  @override
  TimesheetOpsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TimesheetOpsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ops':
          result.ops.replace(serializers.deserialize(value,
                  specifiedType: const FullType(TimesheetUpdateModel))
              as TimesheetUpdateModel);
          break;
      }
    }

    return result.build();
  }
}

class _$TimesheetOpsModel extends TimesheetOpsModel {
  @override
  final TimesheetUpdateModel ops;

  factory _$TimesheetOpsModel(
          [void Function(TimesheetOpsModelBuilder) updates]) =>
      (new TimesheetOpsModelBuilder()..update(updates)).build();

  _$TimesheetOpsModel._({this.ops}) : super._();

  @override
  TimesheetOpsModel rebuild(void Function(TimesheetOpsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TimesheetOpsModelBuilder toBuilder() =>
      new TimesheetOpsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TimesheetOpsModel && ops == other.ops;
  }

  @override
  int get hashCode {
    return $jf($jc(0, ops.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TimesheetOpsModel')..add('ops', ops))
        .toString();
  }
}

class TimesheetOpsModelBuilder
    implements Builder<TimesheetOpsModel, TimesheetOpsModelBuilder> {
  _$TimesheetOpsModel _$v;

  TimesheetUpdateModelBuilder _ops;
  TimesheetUpdateModelBuilder get ops =>
      _$this._ops ??= new TimesheetUpdateModelBuilder();
  set ops(TimesheetUpdateModelBuilder ops) => _$this._ops = ops;

  TimesheetOpsModelBuilder();

  TimesheetOpsModelBuilder get _$this {
    if (_$v != null) {
      _ops = _$v.ops?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TimesheetOpsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TimesheetOpsModel;
  }

  @override
  void update(void Function(TimesheetOpsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TimesheetOpsModel build() {
    _$TimesheetOpsModel _$result;
    try {
      _$result = _$v ?? new _$TimesheetOpsModel._(ops: _ops?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'ops';
        _ops?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TimesheetOpsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
