library timesheet_update_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'timesheet_update_model.g.dart';

abstract class TimesheetUpdateModel
    implements Built<TimesheetUpdateModel, TimesheetUpdateModelBuilder> {
  @nullable
  String get end;

  @nullable
  String get start;

  @nullable
  int get status;

  TimesheetUpdateModel._();

  factory TimesheetUpdateModel([updates(TimesheetUpdateModelBuilder b)]) =
      _$TimesheetUpdateModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(TimesheetUpdateModel.serializer, this));
  }

  static TimesheetUpdateModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TimesheetUpdateModel.serializer, json.decode(jsonString));
  }

  static Serializer<TimesheetUpdateModel> get serializer =>
      _$timesheetUpdateModelSerializer;
}
