library timesheet_ops_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_update_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'timesheet_ops_model.g.dart';

abstract class TimesheetOpsModel
    implements Built<TimesheetOpsModel, TimesheetOpsModelBuilder> {
  @nullable
  TimesheetUpdateModel get ops;

  TimesheetOpsModel._();

  factory TimesheetOpsModel([updates(TimesheetOpsModelBuilder b)]) =
      _$TimesheetOpsModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(TimesheetOpsModel.serializer, this));
  }

  static TimesheetOpsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TimesheetOpsModel.serializer, json.decode(jsonString));
  }

  static Serializer<TimesheetOpsModel> get serializer =>
      _$timesheetOpsModelSerializer;
}
