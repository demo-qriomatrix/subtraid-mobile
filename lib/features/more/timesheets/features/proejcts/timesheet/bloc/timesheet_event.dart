part of 'timesheet_bloc.dart';

abstract class TimesheetEvent extends Equatable {
  const TimesheetEvent();

  @override
  List<Object> get props => [];
}

class UpdateTimesheetEvent extends TimesheetEvent {
  final String id;
  final TimesheetOpsModel data;

  UpdateTimesheetEvent({@required this.id, @required this.data});
}

class DeleteTimesheetEvent extends TimesheetEvent {
  final String id;

  DeleteTimesheetEvent({
    @required this.id,
  });
}

class DeleteTimesheetsEvent extends TimesheetEvent {
  final List<String> ids;

  DeleteTimesheetsEvent({
    @required this.ids,
  });
}
