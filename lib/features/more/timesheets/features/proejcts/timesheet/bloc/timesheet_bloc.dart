import 'dart:async';

import 'package:Subtraid/features/more/timesheets/features/proejcts/repository/projects_timesheets_repository.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_ops_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'timesheet_event.dart';
part 'timesheet_state.dart';

class TimesheetBloc extends Bloc<TimesheetEvent, TimesheetState> {
  TimesheetBloc({@required this.projectsTimesheetsRepository})
      : super(TimesheetInitial());

  final ProjectsTimesheetsRepository projectsTimesheetsRepository;

  @override
  Stream<TimesheetState> mapEventToState(
    TimesheetEvent event,
  ) async* {
    if (event is UpdateTimesheetEvent) {
      yield* _mapUpdateTimesheetEventToState(event);
    }
    if (event is DeleteTimesheetEvent) {
      yield* _mapDeleteTimesheetEventToState(event);
    }
    if (event is DeleteTimesheetsEvent) {
      yield* _mapDeleteTimesheetsEventToState(event);
    }
  }

  Stream<TimesheetState> _mapUpdateTimesheetEventToState(
      UpdateTimesheetEvent event) async* {
    yield TimesheetUpdateInProgress();

    try {
      // Response response =
      await projectsTimesheetsRepository.updateTimesheet(event.id, event.data);

      yield TimesheetUpdateSuccess();
    } catch (e) {
      print(e);

      yield TimesheetUpdateFailure();
    }
  }

  Stream<TimesheetState> _mapDeleteTimesheetEventToState(
      DeleteTimesheetEvent event) async* {
    yield TimesheetUpdateInProgress();

    try {
      // Response response =
      await projectsTimesheetsRepository.deleteTimesheet(
        event.id,
      );

      yield TimesheetDeleteSuccess();
    } catch (e) {
      print(e);

      yield TimesheetDeleteFailure();
    }
  }

  Stream<TimesheetState> _mapDeleteTimesheetsEventToState(
      DeleteTimesheetsEvent event) async* {
    yield TimesheetUpdateInProgress();

    try {
      for (String id in event.ids) {
        // Response response =
        await projectsTimesheetsRepository.deleteTimesheet(id);
      }

      yield TimesheetDeleteSuccess();
    } catch (e) {
      print(e);

      yield TimesheetDeleteFailure();
    }
  }
}
