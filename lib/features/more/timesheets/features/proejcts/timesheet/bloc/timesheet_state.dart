part of 'timesheet_bloc.dart';

abstract class TimesheetState extends Equatable {
  const TimesheetState();

  @override
  List<Object> get props => [];
}

class TimesheetInitial extends TimesheetState {}

class TimesheetUpdateInProgress extends TimesheetState {}

class TimesheetUpdateSuccess extends TimesheetState {}

class TimesheetUpdateFailure extends TimesheetState {}

class TimesheetDeleteSuccess extends TimesheetState {}

class TimesheetDeleteFailure extends TimesheetState {}
