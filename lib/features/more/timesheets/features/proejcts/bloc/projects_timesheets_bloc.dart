import 'dart:async';

import 'package:Subtraid/features/more/timesheets/bloc/company_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheets_response.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/compony_timesheets_request_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/repository/projects_timesheets_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:built_collection/built_collection.dart';

part 'projects_timesheets_event.dart';
part 'projects_timesheets_state.dart';

class ProjectsTimesheetsBloc
    extends Bloc<ProjectsTimesheetsEvent, ProjectsTimesheetsState> {
  ProjectsTimesheetsBloc(
      {@required this.projectsTimesheetsRepository,
      @required this.companyTimesheetsBloc})
      : super(ProjectsTimesheetsInitial()) {
    add(ProjectsTimesheetsRequested());
    this.companyTimesheetsBloc.setProjectsTimesheetsBloc(this);
  }

  final ProjectsTimesheetsRepository projectsTimesheetsRepository;
  final CompanyTimesheetsBloc companyTimesheetsBloc;

  @override
  Stream<ProjectsTimesheetsState> mapEventToState(
    ProjectsTimesheetsEvent event,
  ) async* {
    if (event is ProjectsTimesheetsRequested) {
      yield* _mapProjectsTimesheetsRequestedToState();
    }

    if (event is ProjectsTimesheetsSetSelected) {
      yield* _mapProjectsTimesheetsSetSelected(event);
    }
  }

  Stream<ProjectsTimesheetsState> _mapProjectsTimesheetsSetSelected(
      ProjectsTimesheetsSetSelected event) async* {
    if (state is ProjectsTimesheetsLoadSuccess) {
      List<CompanyTimesheetModel> previousTimesheets =
          (state as ProjectsTimesheetsLoadSuccess).timesheets;

      int index =
          previousTimesheets.indexWhere((element) => element.id == event.id);

      CompanyTimesheetModel timesheet = previousTimesheets[index];

      timesheet = timesheet.rebuild((t) => t..selected = event.selected);

      previousTimesheets[index] = timesheet;

      List<CompanyTimesheetModel> updatedTimesheets =
          List.from(previousTimesheets.toList());

      yield ProjectsTimesheetsLoadSuccess(timesheets: updatedTimesheets);
    }
  }

  Stream<ProjectsTimesheetsState>
      _mapProjectsTimesheetsRequestedToState() async* {
    yield ProjectsTimesheetsLoadInProgress();

    try {
      CompanyTimesheetsRequestModel companyTimesheetsRequestModel =
          CompanyTimesheetsRequestModel((data) => data
            ..start = companyTimesheetsBloc.state.filters.start
            ..end = companyTimesheetsBloc.state.filters.end
            ..status = companyTimesheetsBloc.state.filters.status
            ..userIds = ListBuilder(companyTimesheetsBloc
                .state.filters.employees
                .map((e) => e.id)
                .toList())
            ..projectIds = ListBuilder(companyTimesheetsBloc
                .state.filters.projects
                .map((e) => e.id)
                .toList()));

      CompanyTimesheetsResponse companyTimesheetsResponse =
          await projectsTimesheetsRepository
              .getProjectTimesheets(companyTimesheetsRequestModel);

      yield ProjectsTimesheetsLoadSuccess(
          timesheets: companyTimesheetsResponse.result.toList());
    } catch (e) {
      print(e);

      yield ProjectsTimesheetsLoadFailure();
    }
  }
}
