part of 'projects_timesheets_bloc.dart';

abstract class ProjectsTimesheetsEvent extends Equatable {
  const ProjectsTimesheetsEvent();

  @override
  List<Object> get props => [];
}

class ProjectsTimesheetsRequested extends ProjectsTimesheetsEvent {}

class ProjectsTimesheetsSetSelected extends ProjectsTimesheetsEvent {
  final bool selected;
  final String id;

  ProjectsTimesheetsSetSelected({@required this.id, @required this.selected});
}
