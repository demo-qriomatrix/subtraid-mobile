part of 'projects_timesheets_bloc.dart';

abstract class ProjectsTimesheetsState extends Equatable {
  const ProjectsTimesheetsState();

  @override
  List<Object> get props => [];
}

class ProjectsTimesheetsInitial extends ProjectsTimesheetsState {}

class ProjectsTimesheetsLoadInProgress extends ProjectsTimesheetsState {}

class ProjectsTimesheetsLoadSuccess extends ProjectsTimesheetsState {
  final List<CompanyTimesheetModel> timesheets;

  ProjectsTimesheetsLoadSuccess({@required this.timesheets});

  @override
  List<Object> get props => [timesheets, timesheets.hashCode];
}

class ProjectsTimesheetsLoadFailure extends ProjectsTimesheetsState {}
