// GENERATED CODE - DO NOT MODIFY BY HAND

part of compony_timesheets_request_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyTimesheetsRequestModel>
    _$companyTimesheetsRequestModelSerializer =
    new _$CompanyTimesheetsRequestModelSerializer();

class _$CompanyTimesheetsRequestModelSerializer
    implements StructuredSerializer<CompanyTimesheetsRequestModel> {
  @override
  final Iterable<Type> types = const [
    CompanyTimesheetsRequestModel,
    _$CompanyTimesheetsRequestModel
  ];
  @override
  final String wireName = 'CompanyTimesheetsRequestModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyTimesheetsRequestModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.projectIds != null) {
      result
        ..add('projectIds')
        ..add(serializers.serialize(object.projectIds,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    if (object.userIds != null) {
      result
        ..add('userIds')
        ..add(serializers.serialize(object.userIds,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    return result;
  }

  @override
  CompanyTimesheetsRequestModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyTimesheetsRequestModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectIds':
          result.projectIds.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'userIds':
          result.userIds.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyTimesheetsRequestModel extends CompanyTimesheetsRequestModel {
  @override
  final String end;
  @override
  final BuiltList<String> projectIds;
  @override
  final String start;
  @override
  final int status;
  @override
  final BuiltList<String> userIds;

  factory _$CompanyTimesheetsRequestModel(
          [void Function(CompanyTimesheetsRequestModelBuilder) updates]) =>
      (new CompanyTimesheetsRequestModelBuilder()..update(updates)).build();

  _$CompanyTimesheetsRequestModel._(
      {this.end, this.projectIds, this.start, this.status, this.userIds})
      : super._();

  @override
  CompanyTimesheetsRequestModel rebuild(
          void Function(CompanyTimesheetsRequestModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyTimesheetsRequestModelBuilder toBuilder() =>
      new CompanyTimesheetsRequestModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyTimesheetsRequestModel &&
        end == other.end &&
        projectIds == other.projectIds &&
        start == other.start &&
        status == other.status &&
        userIds == other.userIds;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, end.hashCode), projectIds.hashCode), start.hashCode),
            status.hashCode),
        userIds.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyTimesheetsRequestModel')
          ..add('end', end)
          ..add('projectIds', projectIds)
          ..add('start', start)
          ..add('status', status)
          ..add('userIds', userIds))
        .toString();
  }
}

class CompanyTimesheetsRequestModelBuilder
    implements
        Builder<CompanyTimesheetsRequestModel,
            CompanyTimesheetsRequestModelBuilder> {
  _$CompanyTimesheetsRequestModel _$v;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  ListBuilder<String> _projectIds;
  ListBuilder<String> get projectIds =>
      _$this._projectIds ??= new ListBuilder<String>();
  set projectIds(ListBuilder<String> projectIds) =>
      _$this._projectIds = projectIds;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  ListBuilder<String> _userIds;
  ListBuilder<String> get userIds =>
      _$this._userIds ??= new ListBuilder<String>();
  set userIds(ListBuilder<String> userIds) => _$this._userIds = userIds;

  CompanyTimesheetsRequestModelBuilder();

  CompanyTimesheetsRequestModelBuilder get _$this {
    if (_$v != null) {
      _end = _$v.end;
      _projectIds = _$v.projectIds?.toBuilder();
      _start = _$v.start;
      _status = _$v.status;
      _userIds = _$v.userIds?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyTimesheetsRequestModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyTimesheetsRequestModel;
  }

  @override
  void update(void Function(CompanyTimesheetsRequestModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyTimesheetsRequestModel build() {
    _$CompanyTimesheetsRequestModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyTimesheetsRequestModel._(
              end: end,
              projectIds: _projectIds?.build(),
              start: start,
              status: status,
              userIds: _userIds?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'projectIds';
        _projectIds?.build();

        _$failedField = 'userIds';
        _userIds?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyTimesheetsRequestModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
