library company_timesheets_filters_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/more/company/features/employee/models/employee_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'company_timesheets_filters_model.g.dart';

abstract class CompanyTimesheetsFiltersModel
    implements
        Built<CompanyTimesheetsFiltersModel,
            CompanyTimesheetsFiltersModelBuilder> {
  @nullable
  String get end;

  @nullable
  BuiltList<ProjectListItemModel> get projects;

  @nullable
  String get start;

  @nullable
  int get status;

  @nullable
  BuiltList<EmployeeModel> get employees;

  @nullable
  BuiltList<CrewModel> get crews;

  CompanyTimesheetsFiltersModel._();

  factory CompanyTimesheetsFiltersModel(
          [updates(CompanyTimesheetsFiltersModelBuilder b)]) =
      _$CompanyTimesheetsFiltersModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        CompanyTimesheetsFiltersModel.serializer, this));
  }

  static CompanyTimesheetsFiltersModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyTimesheetsFiltersModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyTimesheetsFiltersModel> get serializer =>
      _$companyTimesheetsFiltersModelSerializer;
}
