library create_timesheet_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_timesheet_model.g.dart';

abstract class CreateTimesheetModel
    implements Built<CreateTimesheetModel, CreateTimesheetModelBuilder> {
  @nullable
  String get cardName;

  @nullable
  String get start;

  @nullable
  @BuiltValueField(wireName: '_cardId')
  String get cardId;

  @nullable
  @BuiltValueField(wireName: '_projectId')
  String get projectId;

  CreateTimesheetModel._();

  factory CreateTimesheetModel([updates(CreateTimesheetModelBuilder b)]) =
      _$CreateTimesheetModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CreateTimesheetModel.serializer, this));
  }

  static CreateTimesheetModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CreateTimesheetModel.serializer, json.decode(jsonString));
  }

  static Serializer<CreateTimesheetModel> get serializer =>
      _$createTimesheetModelSerializer;
}
