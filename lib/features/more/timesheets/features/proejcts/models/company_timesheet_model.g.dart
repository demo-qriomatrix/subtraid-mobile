// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_timesheet_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyTimesheetModel> _$companyTimesheetModelSerializer =
    new _$CompanyTimesheetModelSerializer();

class _$CompanyTimesheetModelSerializer
    implements StructuredSerializer<CompanyTimesheetModel> {
  @override
  final Iterable<Type> types = const [
    CompanyTimesheetModel,
    _$CompanyTimesheetModel
  ];
  @override
  final String wireName = 'CompanyTimesheetModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyTimesheetModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(UserModel)));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.card != null) {
      result
        ..add('card')
        ..add(serializers.serialize(object.card,
            specifiedType: const FullType(ProjectCardModel)));
    }
    if (object.project != null) {
      result
        ..add('_projectId')
        ..add(serializers.serialize(object.project,
            specifiedType: const FullType(ProjectModel)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    if (object.selected != null) {
      result
        ..add('selected')
        ..add(serializers.serialize(object.selected,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  CompanyTimesheetModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyTimesheetModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserModel)) as UserModel);
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'card':
          result.card.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectCardModel))
              as ProjectCardModel);
          break;
        case '_projectId':
          result.project.replace(serializers.deserialize(value,
              specifiedType: const FullType(ProjectModel)) as ProjectModel);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'selected':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyTimesheetModel extends CompanyTimesheetModel {
  @override
  final UserModel user;
  @override
  final String start;
  @override
  final String end;
  @override
  final ProjectCardModel card;
  @override
  final ProjectModel project;
  @override
  final String id;
  @override
  final int status;
  @override
  final bool selected;

  factory _$CompanyTimesheetModel(
          [void Function(CompanyTimesheetModelBuilder) updates]) =>
      (new CompanyTimesheetModelBuilder()..update(updates)).build();

  _$CompanyTimesheetModel._(
      {this.user,
      this.start,
      this.end,
      this.card,
      this.project,
      this.id,
      this.status,
      this.selected})
      : super._();

  @override
  CompanyTimesheetModel rebuild(
          void Function(CompanyTimesheetModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyTimesheetModelBuilder toBuilder() =>
      new CompanyTimesheetModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyTimesheetModel &&
        user == other.user &&
        start == other.start &&
        end == other.end &&
        card == other.card &&
        project == other.project &&
        id == other.id &&
        status == other.status &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, user.hashCode), start.hashCode),
                            end.hashCode),
                        card.hashCode),
                    project.hashCode),
                id.hashCode),
            status.hashCode),
        selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyTimesheetModel')
          ..add('user', user)
          ..add('start', start)
          ..add('end', end)
          ..add('card', card)
          ..add('project', project)
          ..add('id', id)
          ..add('status', status)
          ..add('selected', selected))
        .toString();
  }
}

class CompanyTimesheetModelBuilder
    implements Builder<CompanyTimesheetModel, CompanyTimesheetModelBuilder> {
  _$CompanyTimesheetModel _$v;

  UserModelBuilder _user;
  UserModelBuilder get user => _$this._user ??= new UserModelBuilder();
  set user(UserModelBuilder user) => _$this._user = user;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  ProjectCardModelBuilder _card;
  ProjectCardModelBuilder get card =>
      _$this._card ??= new ProjectCardModelBuilder();
  set card(ProjectCardModelBuilder card) => _$this._card = card;

  ProjectModelBuilder _project;
  ProjectModelBuilder get project =>
      _$this._project ??= new ProjectModelBuilder();
  set project(ProjectModelBuilder project) => _$this._project = project;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  bool _selected;
  bool get selected => _$this._selected;
  set selected(bool selected) => _$this._selected = selected;

  CompanyTimesheetModelBuilder();

  CompanyTimesheetModelBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user?.toBuilder();
      _start = _$v.start;
      _end = _$v.end;
      _card = _$v.card?.toBuilder();
      _project = _$v.project?.toBuilder();
      _id = _$v.id;
      _status = _$v.status;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyTimesheetModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyTimesheetModel;
  }

  @override
  void update(void Function(CompanyTimesheetModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyTimesheetModel build() {
    _$CompanyTimesheetModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyTimesheetModel._(
              user: _user?.build(),
              start: start,
              end: end,
              card: _card?.build(),
              project: _project?.build(),
              id: id,
              status: status,
              selected: selected);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        _user?.build();

        _$failedField = 'card';
        _card?.build();
        _$failedField = 'project';
        _project?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyTimesheetModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
