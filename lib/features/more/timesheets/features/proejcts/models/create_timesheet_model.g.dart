// GENERATED CODE - DO NOT MODIFY BY HAND

part of create_timesheet_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CreateTimesheetModel> _$createTimesheetModelSerializer =
    new _$CreateTimesheetModelSerializer();

class _$CreateTimesheetModelSerializer
    implements StructuredSerializer<CreateTimesheetModel> {
  @override
  final Iterable<Type> types = const [
    CreateTimesheetModel,
    _$CreateTimesheetModel
  ];
  @override
  final String wireName = 'CreateTimesheetModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CreateTimesheetModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.cardName != null) {
      result
        ..add('cardName')
        ..add(serializers.serialize(object.cardName,
            specifiedType: const FullType(String)));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.cardId != null) {
      result
        ..add('_cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('_projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CreateTimesheetModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreateTimesheetModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cardName':
          result.cardName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CreateTimesheetModel extends CreateTimesheetModel {
  @override
  final String cardName;
  @override
  final String start;
  @override
  final String cardId;
  @override
  final String projectId;

  factory _$CreateTimesheetModel(
          [void Function(CreateTimesheetModelBuilder) updates]) =>
      (new CreateTimesheetModelBuilder()..update(updates)).build();

  _$CreateTimesheetModel._(
      {this.cardName, this.start, this.cardId, this.projectId})
      : super._();

  @override
  CreateTimesheetModel rebuild(
          void Function(CreateTimesheetModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateTimesheetModelBuilder toBuilder() =>
      new CreateTimesheetModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateTimesheetModel &&
        cardName == other.cardName &&
        start == other.start &&
        cardId == other.cardId &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, cardName.hashCode), start.hashCode), cardId.hashCode),
        projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateTimesheetModel')
          ..add('cardName', cardName)
          ..add('start', start)
          ..add('cardId', cardId)
          ..add('projectId', projectId))
        .toString();
  }
}

class CreateTimesheetModelBuilder
    implements Builder<CreateTimesheetModel, CreateTimesheetModelBuilder> {
  _$CreateTimesheetModel _$v;

  String _cardName;
  String get cardName => _$this._cardName;
  set cardName(String cardName) => _$this._cardName = cardName;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  CreateTimesheetModelBuilder();

  CreateTimesheetModelBuilder get _$this {
    if (_$v != null) {
      _cardName = _$v.cardName;
      _start = _$v.start;
      _cardId = _$v.cardId;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateTimesheetModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CreateTimesheetModel;
  }

  @override
  void update(void Function(CreateTimesheetModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateTimesheetModel build() {
    final _$result = _$v ??
        new _$CreateTimesheetModel._(
            cardName: cardName,
            start: start,
            cardId: cardId,
            projectId: projectId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
