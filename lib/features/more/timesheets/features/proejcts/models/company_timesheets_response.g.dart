// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_timesheets_response;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyTimesheetsResponse> _$companyTimesheetsResponseSerializer =
    new _$CompanyTimesheetsResponseSerializer();

class _$CompanyTimesheetsResponseSerializer
    implements StructuredSerializer<CompanyTimesheetsResponse> {
  @override
  final Iterable<Type> types = const [
    CompanyTimesheetsResponse,
    _$CompanyTimesheetsResponse
  ];
  @override
  final String wireName = 'CompanyTimesheetsResponse';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyTimesheetsResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.result != null) {
      result
        ..add('result')
        ..add(serializers.serialize(object.result,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CompanyTimesheetModel)])));
    }
    return result;
  }

  @override
  CompanyTimesheetsResponse deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyTimesheetsResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'result':
          result.result.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CompanyTimesheetModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyTimesheetsResponse extends CompanyTimesheetsResponse {
  @override
  final BuiltList<CompanyTimesheetModel> result;

  factory _$CompanyTimesheetsResponse(
          [void Function(CompanyTimesheetsResponseBuilder) updates]) =>
      (new CompanyTimesheetsResponseBuilder()..update(updates)).build();

  _$CompanyTimesheetsResponse._({this.result}) : super._();

  @override
  CompanyTimesheetsResponse rebuild(
          void Function(CompanyTimesheetsResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyTimesheetsResponseBuilder toBuilder() =>
      new CompanyTimesheetsResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyTimesheetsResponse && result == other.result;
  }

  @override
  int get hashCode {
    return $jf($jc(0, result.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyTimesheetsResponse')
          ..add('result', result))
        .toString();
  }
}

class CompanyTimesheetsResponseBuilder
    implements
        Builder<CompanyTimesheetsResponse, CompanyTimesheetsResponseBuilder> {
  _$CompanyTimesheetsResponse _$v;

  ListBuilder<CompanyTimesheetModel> _result;
  ListBuilder<CompanyTimesheetModel> get result =>
      _$this._result ??= new ListBuilder<CompanyTimesheetModel>();
  set result(ListBuilder<CompanyTimesheetModel> result) =>
      _$this._result = result;

  CompanyTimesheetsResponseBuilder();

  CompanyTimesheetsResponseBuilder get _$this {
    if (_$v != null) {
      _result = _$v.result?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyTimesheetsResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyTimesheetsResponse;
  }

  @override
  void update(void Function(CompanyTimesheetsResponseBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyTimesheetsResponse build() {
    _$CompanyTimesheetsResponse _$result;
    try {
      _$result =
          _$v ?? new _$CompanyTimesheetsResponse._(result: _result?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'result';
        _result?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyTimesheetsResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
