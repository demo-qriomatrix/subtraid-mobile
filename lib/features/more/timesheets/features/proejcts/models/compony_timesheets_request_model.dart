library compony_timesheets_request_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'compony_timesheets_request_model.g.dart';

abstract class CompanyTimesheetsRequestModel
    implements
        Built<CompanyTimesheetsRequestModel,
            CompanyTimesheetsRequestModelBuilder> {
  @nullable
  String get end;

  @nullable
  BuiltList<String> get projectIds;

  @nullable
  String get start;

  @nullable
  int get status;

  @nullable
  BuiltList<String> get userIds;

  CompanyTimesheetsRequestModel._();

  factory CompanyTimesheetsRequestModel(
          [updates(CompanyTimesheetsRequestModelBuilder b)]) =
      _$CompanyTimesheetsRequestModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        CompanyTimesheetsRequestModel.serializer, this));
  }

  static CompanyTimesheetsRequestModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyTimesheetsRequestModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyTimesheetsRequestModel> get serializer =>
      _$companyTimesheetsRequestModelSerializer;
}
