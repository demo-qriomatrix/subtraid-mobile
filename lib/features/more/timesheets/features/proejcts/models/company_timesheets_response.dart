library company_timesheets_response;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheet_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'company_timesheets_response.g.dart';

abstract class CompanyTimesheetsResponse
    implements
        Built<CompanyTimesheetsResponse, CompanyTimesheetsResponseBuilder> {
  @nullable
  BuiltList<CompanyTimesheetModel> get result;

  CompanyTimesheetsResponse._();

  factory CompanyTimesheetsResponse(
          [updates(CompanyTimesheetsResponseBuilder b)]) =
      _$CompanyTimesheetsResponse;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompanyTimesheetsResponse.serializer, this));
  }

  static CompanyTimesheetsResponse fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyTimesheetsResponse.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyTimesheetsResponse> get serializer =>
      _$companyTimesheetsResponseSerializer;
}
