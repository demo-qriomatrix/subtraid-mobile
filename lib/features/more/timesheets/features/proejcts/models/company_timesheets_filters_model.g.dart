// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_timesheets_filters_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyTimesheetsFiltersModel>
    _$companyTimesheetsFiltersModelSerializer =
    new _$CompanyTimesheetsFiltersModelSerializer();

class _$CompanyTimesheetsFiltersModelSerializer
    implements StructuredSerializer<CompanyTimesheetsFiltersModel> {
  @override
  final Iterable<Type> types = const [
    CompanyTimesheetsFiltersModel,
    _$CompanyTimesheetsFiltersModel
  ];
  @override
  final String wireName = 'CompanyTimesheetsFiltersModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyTimesheetsFiltersModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.projects != null) {
      result
        ..add('projects')
        ..add(serializers.serialize(object.projects,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectListItemModel)])));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    if (object.employees != null) {
      result
        ..add('employees')
        ..add(serializers.serialize(object.employees,
            specifiedType: const FullType(
                BuiltList, const [const FullType(EmployeeModel)])));
    }
    if (object.crews != null) {
      result
        ..add('crews')
        ..add(serializers.serialize(object.crews,
            specifiedType:
                const FullType(BuiltList, const [const FullType(CrewModel)])));
    }
    return result;
  }

  @override
  CompanyTimesheetsFiltersModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyTimesheetsFiltersModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projects':
          result.projects.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectListItemModel)]))
              as BuiltList<Object>);
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'employees':
          result.employees.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(EmployeeModel)]))
              as BuiltList<Object>);
          break;
        case 'crews':
          result.crews.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CrewModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyTimesheetsFiltersModel extends CompanyTimesheetsFiltersModel {
  @override
  final String end;
  @override
  final BuiltList<ProjectListItemModel> projects;
  @override
  final String start;
  @override
  final int status;
  @override
  final BuiltList<EmployeeModel> employees;
  @override
  final BuiltList<CrewModel> crews;

  factory _$CompanyTimesheetsFiltersModel(
          [void Function(CompanyTimesheetsFiltersModelBuilder) updates]) =>
      (new CompanyTimesheetsFiltersModelBuilder()..update(updates)).build();

  _$CompanyTimesheetsFiltersModel._(
      {this.end,
      this.projects,
      this.start,
      this.status,
      this.employees,
      this.crews})
      : super._();

  @override
  CompanyTimesheetsFiltersModel rebuild(
          void Function(CompanyTimesheetsFiltersModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyTimesheetsFiltersModelBuilder toBuilder() =>
      new CompanyTimesheetsFiltersModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyTimesheetsFiltersModel &&
        end == other.end &&
        projects == other.projects &&
        start == other.start &&
        status == other.status &&
        employees == other.employees &&
        crews == other.crews;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, end.hashCode), projects.hashCode),
                    start.hashCode),
                status.hashCode),
            employees.hashCode),
        crews.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyTimesheetsFiltersModel')
          ..add('end', end)
          ..add('projects', projects)
          ..add('start', start)
          ..add('status', status)
          ..add('employees', employees)
          ..add('crews', crews))
        .toString();
  }
}

class CompanyTimesheetsFiltersModelBuilder
    implements
        Builder<CompanyTimesheetsFiltersModel,
            CompanyTimesheetsFiltersModelBuilder> {
  _$CompanyTimesheetsFiltersModel _$v;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  ListBuilder<ProjectListItemModel> _projects;
  ListBuilder<ProjectListItemModel> get projects =>
      _$this._projects ??= new ListBuilder<ProjectListItemModel>();
  set projects(ListBuilder<ProjectListItemModel> projects) =>
      _$this._projects = projects;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  ListBuilder<EmployeeModel> _employees;
  ListBuilder<EmployeeModel> get employees =>
      _$this._employees ??= new ListBuilder<EmployeeModel>();
  set employees(ListBuilder<EmployeeModel> employees) =>
      _$this._employees = employees;

  ListBuilder<CrewModel> _crews;
  ListBuilder<CrewModel> get crews =>
      _$this._crews ??= new ListBuilder<CrewModel>();
  set crews(ListBuilder<CrewModel> crews) => _$this._crews = crews;

  CompanyTimesheetsFiltersModelBuilder();

  CompanyTimesheetsFiltersModelBuilder get _$this {
    if (_$v != null) {
      _end = _$v.end;
      _projects = _$v.projects?.toBuilder();
      _start = _$v.start;
      _status = _$v.status;
      _employees = _$v.employees?.toBuilder();
      _crews = _$v.crews?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyTimesheetsFiltersModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyTimesheetsFiltersModel;
  }

  @override
  void update(void Function(CompanyTimesheetsFiltersModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyTimesheetsFiltersModel build() {
    _$CompanyTimesheetsFiltersModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyTimesheetsFiltersModel._(
              end: end,
              projects: _projects?.build(),
              start: start,
              status: status,
              employees: _employees?.build(),
              crews: _crews?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'projects';
        _projects?.build();

        _$failedField = 'employees';
        _employees?.build();
        _$failedField = 'crews';
        _crews?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyTimesheetsFiltersModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
