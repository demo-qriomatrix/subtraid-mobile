library company_timesheet_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'company_timesheet_model.g.dart';

abstract class CompanyTimesheetModel
    implements Built<CompanyTimesheetModel, CompanyTimesheetModelBuilder> {
  @nullable
  UserModel get user;

  @nullable
  String get start;

  @nullable
  String get end;

  @nullable
  ProjectCardModel get card;

  @nullable
  @BuiltValueField(wireName: '_projectId')
  ProjectModel get project;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  int get status;

  @nullable
  bool get selected;

  CompanyTimesheetModel._();

  factory CompanyTimesheetModel([updates(CompanyTimesheetModelBuilder b)]) =
      _$CompanyTimesheetModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompanyTimesheetModel.serializer, this));
  }

  static CompanyTimesheetModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyTimesheetModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyTimesheetModel> get serializer =>
      _$companyTimesheetModelSerializer;
}
