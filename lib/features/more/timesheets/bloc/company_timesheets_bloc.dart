import 'dart:async';

import 'package:Subtraid/features/more/timesheets/features/proejcts/bloc/projects_timesheets_bloc.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/company_timesheets_filters_model.dart';
import 'package:bloc/bloc.dart';
import 'package:built_collection/built_collection.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'company_timesheets_event.dart';
part 'company_timesheets_state.dart';

class CompanyTimesheetsBloc
    extends Bloc<CompanyTimesheetsEvent, CompanyTimesheetsState> {
  CompanyTimesheetsBloc({companyTimesheetsRepository})
      : super(CompanyTimesheetsState(
            selectedIndex: 0,
            filterMode: false,
            filters: CompanyTimesheetsFiltersModel((filters) => filters
              ..employees = ListBuilder([])
              ..projects = ListBuilder([])
              ..start =
                  DateFormat('MM/dd/yyyy').parse('01/01/2020').toIso8601String()
              ..end = DateTime.now().toIso8601String())));

  ProjectsTimesheetsBloc projectsTimesheetsBloc;

  setProjectsTimesheetsBloc(ProjectsTimesheetsBloc _projectsTimesheetsBloc) {
    this.projectsTimesheetsBloc = _projectsTimesheetsBloc;
  }

  @override
  Stream<CompanyTimesheetsState> mapEventToState(
    CompanyTimesheetsEvent event,
  ) async* {
    if (event is CompanyTimesheetsIndexChanged) {
      yield* _mapCompanyTimesheetsIndexChangedIndexChangedToState(event);
    }

    if (event is CompanyTimesheetsToggleFilter) {
      yield* _mapCompanyTimesheetsToggleFilterToState();
    }

    if (event is CompanyTimesheetsUpdateFilter) {
      yield* _mapCompanyTimesheetsUpdateFilterToState(event);
    }
  }

  Stream<CompanyTimesheetsState>
      _mapCompanyTimesheetsToggleFilterToState() async* {
    yield CompanyTimesheetsState(
        selectedIndex: state.selectedIndex,
        filterMode: !state.filterMode,
        filters: state.filters);
  }

  Stream<CompanyTimesheetsState> _mapCompanyTimesheetsUpdateFilterToState(
      CompanyTimesheetsUpdateFilter event) async* {
    yield CompanyTimesheetsState(
        selectedIndex: state.selectedIndex,
        filterMode: !state.filterMode,
        filters: event.filter);

    if (projectsTimesheetsBloc != null)
      projectsTimesheetsBloc.add(ProjectsTimesheetsRequested());
  }

  Stream<CompanyTimesheetsState>
      _mapCompanyTimesheetsIndexChangedIndexChangedToState(
          CompanyTimesheetsIndexChanged event) async* {
    // if (state is CompanyLoadSuccess)
    //   yield CompanyLoadSuccess(
    //       selectedIndex: event.index,
    //       companyInfoModel: (state as CompanyLoadSuccess).companyInfoModel);

    yield CompanyTimesheetsState(
        selectedIndex: event.index,
        filterMode: state.filterMode,
        filters: state.filters);
    // if (state is CompanyLoadFailure)
    //   yield CompanyLoadFailure(selectedIndex: event.index);
  }
}
