part of 'company_timesheets_bloc.dart';

class CompanyTimesheetsState extends Equatable {
  final int selectedIndex;
  final bool filterMode;
  final CompanyTimesheetsFiltersModel filters;

  CompanyTimesheetsState(
      {@required this.selectedIndex,
      @required this.filterMode,
      @required this.filters});

  @override
  List<Object> get props => [selectedIndex, filterMode, filters];
}
