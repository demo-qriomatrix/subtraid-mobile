part of 'company_timesheets_bloc.dart';

abstract class CompanyTimesheetsEvent extends Equatable {
  const CompanyTimesheetsEvent();

  @override
  List<Object> get props => [];
}

class CompanyTimesheetsIndexChanged extends CompanyTimesheetsEvent {
  final int index;

  CompanyTimesheetsIndexChanged({@required this.index});
}

class CompanyTimesheetsToggleFilter extends CompanyTimesheetsEvent {}

class CompanyTimesheetsUpdateFilter extends CompanyTimesheetsEvent {
  final CompanyTimesheetsFiltersModel filter;

  CompanyTimesheetsUpdateFilter({@required this.filter});
}
