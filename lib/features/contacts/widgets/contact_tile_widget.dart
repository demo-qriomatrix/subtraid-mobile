import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/contacts/bloc/contact_bloc/contacts_bloc.dart';
import 'package:Subtraid/features/contacts/models/contact_detail_model.dart';
import 'package:Subtraid/features/contacts/models/favourite_post_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'contact_dialog.dart';

class ContactTile extends StatelessWidget {
  final ContactDetailsModel contact;
  final int index;
  final bool canFavourite;
  final bool canSelect;
  ContactTile({this.contact, this.index, this.canFavourite, this.canSelect});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Material(
        borderRadius: BorderRadius.circular(10),
        color:
            //  index % 2 != 0
            //     ? ColorConfig.primary
            //     :
            ColorConfig.lightPrimary,
        child: InkWell(
          borderRadius: BorderRadius.circular(10),
          onTap: () {
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    backgroundColor: Colors.white,
                    child: ContactInfoDialog(contact)));
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5),
                  child: CircleAvatar(
                    backgroundImage: AssetImage(LocalImages.defaultAvatar),
                    // child: Image.network(''),
                    // CachedNetworkImage(
                    //   imageUrl: ,
                    //   errorWidget: (context, url, error) =>
                    //       Image.asset(LocalImages.defaultAvatar),
                    // ),
                    radius: 25,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        contact.name,
                        style: TextStyle(color: ColorConfig.orange),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        contact.email,
                        style: TextStyle(
                            color:
                                // index % 2 != 0 ? Colors.white :
                                ColorConfig.primary,
                            fontSize: 12),
                      ),
                    ],
                  ),
                ),
                canFavourite
                    ? RawMaterialButton(
                        constraints: BoxConstraints(),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        padding: EdgeInsets.all(10),
                        shape: CircleBorder(),
                        child: Icon(
                            contact?.isFavourite ?? true
                                ? Icons.favorite
                                : Icons.favorite_outline,
                            color: ColorConfig.green5),
                        onPressed: () {
                          FavouritePostModel fav =
                              FavouritePostModel((model) => model
                                ..favouriteUserId = contact.id
                                ..isAdd = !(contact.isFavourite ?? true)
                                ..search = '');

                          context
                              .read<ContactsBloc>()
                              .add(ContactUpdateFavourite(data: fav));
                        })
                    : Container(),

                canSelect
                    ? Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: ColorConfig.orange,
                        ),
                        child: Checkbox(
                          onChanged: (val) {
                            context.read<ContactsBloc>().add(ContactSetSelected(
                                id: contact.id, selected: val));
                          },
                          value: (contact?.selected ?? false) == true,
                        ),
                      )
                    : Container(),

                // Theme(
                //   data: ThemeData(
                //     unselectedWidgetColor: Colors.white,
                //   ),
                //   child: Checkbox(
                //     checkColor: Colors.white,
                //     activeColor: Colors.white24,
                //     value: contact.isVerified,
                //     onChanged: (newValue) {},
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
