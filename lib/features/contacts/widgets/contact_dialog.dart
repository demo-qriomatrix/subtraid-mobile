import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/contacts/bloc/user_bloc/user_bloc.dart';
import 'package:Subtraid/features/contacts/models/contact_detail_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContactInfoDialog extends StatelessWidget {
  final ContactDetailsModel contact;

  ContactInfoDialog(this.contact);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
                icon: Icon(
                  Icons.close,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        ),
        BlocProvider<UserBloc>(
          create: (context) =>
              UserBloc(contactsRepository: sl(), userId: contact.id),
          child: BlocBuilder<UserBloc, UserState>(
            builder: (context, state) {
              if (state is UserLoadFailure) {
                return Flexible(
                    child: ListView(
                        shrinkWrap: true,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        children: [
                      Text(
                        state.error,
                        style: TextStyle(color: ColorConfig.red1),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ]));
              }

              if (state is UserLoadInProgress) {
                return Flexible(
                  child: contact.accountType == 'company'
                      ? ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage:
                                      AssetImage(LocalImages.defaultAvatar),
                                  radius: 24,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      contact?.name ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      contact?.email ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.black1,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Office Address: ',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Flexible(
                                        child: Text(
                                          '-',
                                          style: TextStyle(
                                              color: ColorConfig.primary),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Phone: ',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Text(
                                        '-',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'About Company:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Services',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        )
                      : ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage:
                                      AssetImage(LocalImages.defaultAvatar),
                                  radius: 24,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      contact?.name ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      contact?.email ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.black1,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Job Title: ',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Text(
                                        '-',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Employer:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                  contact.isAvailable != null &&
                                          contact.isAvailable
                                      ? Column(
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              'Available',
                                              style: TextStyle(
                                                  fontStyle: FontStyle.italic,
                                                  color: ColorConfig.green6),
                                            ),
                                          ],
                                        )
                                      : Container(),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'About Me:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Skills:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Licences/Certifications :',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                );
              }

              if (state is UserLoadSuccess) {
                return Flexible(
                  child: state.user.accountType == 'company'
                      ? ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage:
                                      AssetImage(LocalImages.defaultAvatar),
                                  radius: 24,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      contact?.name ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      contact?.email ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.black1,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            // Text(
                            //   'Employee User Details',
                            //   style: TextStyle(fontWeight: FontWeight.w500),
                            // ),
                            // SizedBox(
                            //   height: 10,
                            // ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Office Address: ',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Flexible(
                                        child: Text(
                                          state.user?.getLocation
                                                  ?.formattedAddress ??
                                              '-',
                                          style: TextStyle(
                                              color: ColorConfig.primary),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Phone: ',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Text(
                                        state.user?.phone ?? '-',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'About Company:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    state.user?.description ?? '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Services',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  // state.user.skills.isEmpty
                                  //     ?
                                  Text(
                                    '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  )
                                  // : Column(
                                  //     children: state.user.skills
                                  //         .where((e) => e.name != null)
                                  //         .map((e) => Column(
                                  //               children: [
                                  //                 Row(
                                  //                   children: [
                                  //                     Flexible(
                                  //                       child: Text(
                                  //                         e?.name ?? '',
                                  //                         overflow:
                                  //                             TextOverflow
                                  //                                 .ellipsis,
                                  //                         style: TextStyle(
                                  //                             color: ColorConfig
                                  //                                 .primary),
                                  //                       ),
                                  //                     ),
                                  //                     Text(
                                  //                       ' | ${e?.from ?? ''} - ${e?.to ?? ''}',
                                  //                       style: TextStyle(
                                  //                           color:
                                  //                               ColorConfig
                                  //                                   .black2),
                                  //                     ),
                                  //                   ],
                                  //                 ),
                                  //                 SizedBox(
                                  //                   height: 5,
                                  //                 ),
                                  //               ],
                                  //             ))
                                  //         .toList()),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        )
                      : ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage:
                                      AssetImage(LocalImages.defaultAvatar),
                                  radius: 24,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      contact?.name ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      contact?.email ?? '',
                                      style: TextStyle(
                                          color: ColorConfig.black1,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            // Text(
                            //   'Employee User Details',
                            //   style: TextStyle(fontWeight: FontWeight.w500),
                            // ),
                            // SizedBox(
                            //   height: 10,
                            // ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Job Title: ',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                      Text(
                                        '-',
                                        style: TextStyle(
                                            color: ColorConfig.primary),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Employer:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  state.employer != null
                                      ? Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              state.employer?.name ?? '',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            Text(
                                              state.employer?.email ?? '',
                                              style: TextStyle(
                                                  color: ColorConfig.grey16),
                                            ),
                                          ],
                                        )
                                      : Text(
                                          '-',
                                          style: TextStyle(
                                              color: ColorConfig.grey16),
                                        ),
                                  contact.isAvailable != null &&
                                          contact.isAvailable
                                      ? Column(
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              'Available',
                                              style: TextStyle(
                                                  fontStyle: FontStyle.italic,
                                                  color: ColorConfig.green6),
                                            ),
                                          ],
                                        )
                                      : Container(),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'About Me:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    state.user?.description ?? '-',
                                    style: TextStyle(color: ColorConfig.grey16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Skills:',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  state.user.skills.isEmpty
                                      ? Text(
                                          '-',
                                          style: TextStyle(
                                              color: ColorConfig.grey16),
                                        )
                                      : Column(
                                          children: state.user.skills
                                              .where((e) => e.name != null)
                                              .map((e) => Column(
                                                    children: [
                                                      Row(
                                                        children: [
                                                          Flexible(
                                                            child: Text(
                                                              e?.name ?? '',
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              style: TextStyle(
                                                                  color: ColorConfig
                                                                      .primary),
                                                            ),
                                                          ),
                                                          Text(
                                                            ' | ${e?.from ?? ''} - ${e?.to ?? ''}',
                                                            style: TextStyle(
                                                                color:
                                                                    ColorConfig
                                                                        .black2),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                    ],
                                                  ))
                                              .toList()),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    'Licences/Certifications :',
                                    style:
                                        TextStyle(color: ColorConfig.primary),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  state.user.licences.isEmpty
                                      ? Text(
                                          '-',
                                          style: TextStyle(
                                              color: ColorConfig.grey16),
                                        )
                                      : Column(
                                          children: state.user.licences
                                              .where((e) => e.name != null)
                                              .map((e) => Column(
                                                    children: [
                                                      Text(
                                                        e?.name ?? '',
                                                        style: TextStyle(
                                                            color: ColorConfig
                                                                .primary),
                                                      ),
                                                      Text(
                                                        '${e?.yearExpire ?? ''}',
                                                        style: TextStyle(
                                                            color: ColorConfig
                                                                .black2),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                    ],
                                                  ))
                                              .toList()),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                );
              }

              return Container();
            },
          ),
        )
      ],
    );
  }
} // class
