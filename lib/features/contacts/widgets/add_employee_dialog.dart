import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/contacts/bloc/add_employee_bloc/add_employee_bloc.dart';
import 'package:Subtraid/features/contacts/bloc/contact_bloc/contacts_bloc.dart';
import 'package:Subtraid/features/contacts/models/add_employee_model.dart';
import 'package:Subtraid/features/contacts/models/contact_detail_model.dart';
import 'package:Subtraid/features/contacts/models/contract_details_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'contact_tile_widget.dart';

class AddEmployeeDialog extends StatefulWidget {
  final ContactsBloc contactsBloc;

  AddEmployeeDialog({@required this.contactsBloc});

  @override
  _AddEmployeeDialogState createState() => _AddEmployeeDialogState();
}

class _AddEmployeeDialogState extends State<AddEmployeeDialog> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String jobTypeStatus;

  bool autoValidate = false;
  bool submitted = false;
  bool isAdmin = false;

  TextEditingController jobRole = TextEditingController();
  TextEditingController wageController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddEmployeeBloc>(
      create: (context) => AddEmployeeBloc(contactsRepository: sl()),
      lazy: false,
      child: BlocConsumer<AddEmployeeBloc, AddEmployeeState>(
        listener: (context, state) async {
          if (state is AddEmployeeInProgress) {
            context.read<GlobalBloc>().add(ShowLoadingWidget());
          }

          if (state is AddEmployeeSuccess) {
            widget.contactsBloc.add(ContactsRequested(hidden: false));

            context.read<GlobalBloc>().add(HideLoadingWidget());

            await Future.delayed(Duration(milliseconds: 1));

            Navigator.of(context).pop(true);

            context
                .read<GlobalBloc>()
                .add(ShowSuccessSnackBar(message: state.message, event: null));
          }

          if (state is AddEmployeeFailure) {
            context.read<GlobalBloc>().add(HideLoadingWidget());

            Navigator.of(context).pop(false);

            context
                .read<GlobalBloc>()
                .add(ShowErrorSnackBar(error: state.error));
          }
        },
        builder: (context, state) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: GestureDetector(
              onTap: () {
                // FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 14,
                        backgroundColor: ColorConfig.orange,
                        child: Icon(Icons.add, size: 20, color: Colors.white),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Add Employee',
                        style: TextStyle(
                            color: ColorConfig.primary,
                            fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      IconButton(
                          padding: EdgeInsets.zero,
                          constraints: BoxConstraints(),
                          icon: Icon(Icons.close, color: ColorConfig.primary),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                          })
                    ],
                  ),
                  Expanded(
                      child: Form(
                    key: _formKey,
                    autovalidateMode: autoValidate
                        ? AutovalidateMode.always
                        : AutovalidateMode.disabled,
                    child: ListView(
                      children: [
                        if (widget.contactsBloc.state is ContactsLoadSuccess)
                          ContactTile(
                            contact: (widget.contactsBloc.state
                                    as ContactsLoadSuccess)
                                .selectedAvailableContact,
                            canFavourite: false,
                            canSelect: false,
                            index: null,
                          ),
                        TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Required';
                            }

                            return null;
                          },
                          controller: jobRole,
                          decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Job Role',
                              border: UnderlineInputBorder()
                              //  contentPadding: EdgeInsets.zero
                              ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        DropdownButtonFormField(
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return 'Required';
                            }

                            return null;
                          },
                          dropdownColor: ColorConfig.primary,
                          icon: Padding(
                              padding: const EdgeInsets.only(left: 40),
                              child: Icon(
                                Icons.keyboard_arrow_down,
                              )),
                          value: jobTypeStatus,
                          decoration: InputDecoration(
                            alignLabelWithHint: true,
                            labelText: 'Job Type',
                            border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black45)),
                          ),
                          iconEnabledColor: Colors.black,
                          iconDisabledColor: Colors.black,
                          items: ['Hourly', 'Salary', 'Contract']
                              .map((e) => DropdownMenuItem(
                                    value: e,
                                    child: Text(
                                      e,
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.white),
                                    ),
                                  ))
                              .toList(),
                          selectedItemBuilder: (context) =>
                              ['Hourly', 'Salary', 'Contract']
                                  .map((e) => DropdownMenuItem(
                                        value: e,
                                        child: Text(
                                          e,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ))
                                  .toList(),
                          onChanged: (val) {
                            setState(() {
                              jobTypeStatus = val;
                            });
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        if (jobTypeStatus == 'Hourly' ||
                            jobTypeStatus == 'Salary')
                          TextFormField(
                            controller: wageController,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Required';
                              }

                              return null;
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            decoration: InputDecoration(
                                alignLabelWithHint: true,
                                labelText: 'Wage',
                                border: UnderlineInputBorder()
                                //  contentPadding: EdgeInsets.zero
                                ),
                          ),
                        if (jobTypeStatus == 'Contract')
                          Column(
                            children: [
                              TextFormField(
                                controller: amountController,
                                validator: (String value) {
                                  if (value.isEmpty) {
                                    return 'Required';
                                  }

                                  return null;
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: 'Amount',
                                    border: UnderlineInputBorder()
                                    //  contentPadding: EdgeInsets.zero
                                    ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Flexible(
                                    child: TextFormField(
                                      validator: (String value) {
                                        if (value.isEmpty) {
                                          return 'Required';
                                        }

                                        return null;
                                      },
                                      onTap: () {
                                        showDatePicker(
                                          context: context,
                                          firstDate: DateTime.now()
                                              .subtract(Duration(days: 3650)),
                                          initialDate: DateTime.now(),
                                          lastDate: DateTime.now()
                                              .add(Duration(days: 3650)),
                                        ).then((value) {
                                          if (value != null) {
                                            startDate.text =
                                                DateFormat('MM/dd/yyyy')
                                                    .format(value);
                                          }
                                        });
                                      },
                                      readOnly: true,
                                      controller: startDate,
                                      decoration: InputDecoration(
                                        alignLabelWithHint: true,
                                        labelText: 'Start Date',
                                        suffixIconConstraints: BoxConstraints(
                                            minHeight: 20, minWidth: 20),
                                        suffixIcon: ImageIcon(
                                          AssetImage(LocalImages.calenderAlt),
                                          color: ColorConfig.primary,
                                          size: 16,
                                        ),
                                        border: UnderlineInputBorder(),
                                      ),
                                      maxLines: null,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black.withOpacity(0.74),
                                          fontSize: 14),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Flexible(
                                    child: TextFormField(
                                      validator: (String value) {
                                        if (value.isEmpty) {
                                          return 'Required';
                                        }

                                        return null;
                                      },
                                      onTap: () {
                                        showDatePicker(
                                          context: context,
                                          firstDate: DateTime.now()
                                              .subtract(Duration(days: 3650)),
                                          initialDate: DateTime.now(),
                                          lastDate: DateTime.now()
                                              .add(Duration(days: 3650)),
                                        ).then((value) {
                                          if (value != null) {
                                            endDate.text =
                                                DateFormat('MM/dd/yyyy')
                                                    .format(value);
                                          }
                                        });
                                      },
                                      readOnly: true,
                                      controller: endDate,
                                      decoration: InputDecoration(
                                        alignLabelWithHint: true,
                                        labelText: 'End Date',
                                        suffixIconConstraints: BoxConstraints(
                                            minHeight: 20, minWidth: 20),
                                        suffixIcon: ImageIcon(
                                          AssetImage(LocalImages.calenderAlt),
                                          color: ColorConfig.primary,
                                          size: 16,
                                        ),
                                        border: UnderlineInputBorder(),
                                      ),
                                      maxLines: null,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black.withOpacity(0.74),
                                          fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: descriptionController,
                                decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: 'Description',
                                    border: UnderlineInputBorder()
                                    //  contentPadding: EdgeInsets.zero
                                    ),
                              ),
                            ],
                          ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: messageController,
                          decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Message',
                              border: UnderlineInputBorder()
                              //  contentPadding: EdgeInsets.zero
                              ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Theme(
                              data: ThemeData(
                                  unselectedWidgetColor: ColorConfig.orange),
                              child: Checkbox(
                                checkColor: ColorConfig.orange,
                                activeColor: ColorConfig.lightOrange,
                                value: isAdmin,
                                onChanged: (val) {
                                  setState(() {
                                    isAdmin = val;
                                  });
                                },
                              ),
                            ),
                            Text(
                              'Allow Admin Persmission',
                              style: TextStyle(color: ColorConfig.orange),
                            )
                          ],
                        ),
                      ],
                    ),
                  )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        color: ColorConfig.grey3,
                        child: Text(
                          'No',
                          style: TextStyle(fontSize: 12)
                              .copyWith(color: ColorConfig.grey2),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      FlatButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        color: ColorConfig.primary,
                        child: Text(
                          'Add',
                          style: TextStyle(fontSize: 12)
                              .copyWith(color: Colors.white),
                        ),
                        onPressed: () {
                          setState(() {
                            autoValidate = true;
                          });
                          if (_formKey.currentState.validate()) {
                            if (!submitted) {
                              submitted = true;

                              if (widget.contactsBloc.state
                                  is ContactsLoadSuccess) {
                                ContactDetailsModel contact = (widget
                                        .contactsBloc
                                        .state as ContactsLoadSuccess)
                                    .selectedAvailableContact;
                                ContractDetailsModel contractDetailsModel;
                                if (jobTypeStatus == 'Contract') {
                                  contractDetailsModel = ContractDetailsModel(
                                      (model) => model
                                        ..amount =
                                            int.tryParse(amountController.text)
                                        ..description =
                                            descriptionController.text
                                        ..startDate = DateFormat("MM/dd/yyyy")
                                            .parse(startDate.text)
                                            .toIso8601String()
                                        ..endDate = DateFormat("MM/dd/yyyy")
                                            .parse(endDate.text)
                                            .toIso8601String());
                                }

                                AddEmployeeModel data = AddEmployeeModel(
                                    (model) => model
                                      ..id = contact.id
                                      ..isAdmin = isAdmin
                                      ..role = jobRole.text
                                      ..type = jobTypeStatus.toLowerCase()
                                      ..contractDetails =
                                          (jobTypeStatus == 'Contract')
                                              ? contractDetailsModel.toBuilder()
                                              : null
                                      ..rate = (jobTypeStatus == 'Hourly' ||
                                              jobTypeStatus == 'Salary')
                                          ? int.tryParse(wageController.text)
                                          : null
                                      ..employerMessage =
                                          messageController.text);

                                context
                                    .read<AddEmployeeBloc>()
                                    .add(AddEmployee(data: data));
                              }
                            }
                          }
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
