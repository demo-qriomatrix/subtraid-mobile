part of 'add_employee_bloc.dart';

abstract class AddEmployeeState extends Equatable {
  const AddEmployeeState();

  @override
  List<Object> get props => [];
}

class AddEmployeeInitial extends AddEmployeeState {}

class AddEmployeeInProgress extends AddEmployeeState {}

class AddEmployeeSuccess extends AddEmployeeState {
  final String message;

  AddEmployeeSuccess({@required this.message});
}

class AddEmployeeFailure extends AddEmployeeState {
  final String error;

  AddEmployeeFailure({@required this.error});
}
