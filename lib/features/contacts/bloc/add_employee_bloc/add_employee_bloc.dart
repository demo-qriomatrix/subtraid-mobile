import 'dart:async';
import 'dart:convert';

import 'package:Subtraid/features/contacts/models/add_employee_model.dart';
import 'package:Subtraid/features/contacts/repository/contacts_repository.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'add_employee_event.dart';
part 'add_employee_state.dart';

class AddEmployeeBloc extends Bloc<AddEmployeeEvent, AddEmployeeState> {
  final ContactsRepository contactsRepository;

  AddEmployeeBloc({@required this.contactsRepository})
      : super(AddEmployeeInitial());

  @override
  Stream<AddEmployeeState> mapEventToState(
    AddEmployeeEvent event,
  ) async* {
    if (event is AddEmployee) {
      yield* _mapAddEmployeeToState(event.data);
    }
  }

  Stream<AddEmployeeState> _mapAddEmployeeToState(
      AddEmployeeModel data) async* {
    yield AddEmployeeInProgress();

    try {
      ApiResponseModel apiResponseModel =
          await contactsRepository.addEmployee(data);

      yield AddEmployeeSuccess(message: apiResponseModel.message);

      // await Future.delayed(Duration(seconds: 2));
      // yield AddEmployeeSuccess(message: 'apiResponseModel.message');
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        yield AddEmployeeFailure(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message);
      } else {
        yield AddEmployeeFailure(error: 'Something went error');
      }
    } catch (e) {
      print(e);

      yield AddEmployeeFailure(error: 'Something went error');
    }
  }
}
