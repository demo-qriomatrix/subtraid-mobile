part of 'add_employee_bloc.dart';

abstract class AddEmployeeEvent extends Equatable {
  const AddEmployeeEvent();

  @override
  List<Object> get props => [];
}

class AddEmployee extends AddEmployeeEvent {
  final AddEmployeeModel data;

  AddEmployee({@required this.data});
}
