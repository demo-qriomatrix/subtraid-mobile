import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:Subtraid/features/contacts/models/connections_model.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/contacts/models/contact_detail_model.dart';
import 'package:Subtraid/features/contacts/models/contacts_response_model.dart';
import 'package:Subtraid/features/contacts/models/favourite_post_model.dart';
import 'package:Subtraid/features/contacts/repository/contacts_repository.dart';

part 'contacts_event.dart';
part 'contacts_state.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  ContactsBloc(
      {@required this.contactsRepository,
      @required this.globalBloc,
      @required this.index})
      : super(ContactsInitial(selectedIndex: index)) {
    add(ContactsRequested(hidden: false));
  }

  final ContactsRepository contactsRepository;
  final GlobalBloc globalBloc;
  final int index;

  @override
  Stream<ContactsState> mapEventToState(
    ContactsEvent event,
  ) async* {
    if (event is ContactsRequested) {
      yield* _mapContactsRequestedToState(event.hidden, null);
    }
    if (event is ContactsIndexChanged) {
      // yield ContactsState(selectedIndex: event.index);
      yield* _mapContactsIndexChangedToState(event.index);
    }

    if (event is ContactUpdateFavourite) {
      yield* _mapContactUpdateFavourite(event.data);
    }

    if (event is ContactSetSelected) {
      yield* _mapContactSetSelected(event);
    }
  }

  Stream<ContactsState> _mapContactSetSelected(
      ContactSetSelected event) async* {
    if (state is ContactsLoadSuccess) {
      ContactsLoadSuccess successState = (state as ContactsLoadSuccess);
      if (state.selectedIndex == 2) {
        List<ContactDetailsModel> previousAvailableContacts =
            successState.avaliableContacts;

        //   previousAvailableContacts.asMap().forEach((index, element) {
        //     previousAvailableContacts[index] =
        //         element.rebuild((t) => t..selected = false);
        //   });

        if (successState.selectedAvailableContact != null) {
          int index = previousAvailableContacts.indexWhere((element) =>
              element.id == successState.selectedAvailableContact.id);

          if (index != -1) {
            ContactDetailsModel contact = previousAvailableContacts[index];

            contact = contact.rebuild((t) => t..selected = false);

            previousAvailableContacts[index] = contact;
          }
        }

        int index = previousAvailableContacts
            .indexWhere((element) => element.id == event.id);

        ContactDetailsModel contact = previousAvailableContacts[index];

        contact = contact.rebuild((t) => t..selected = event.selected);

        previousAvailableContacts[index] = contact;

        List<ContactDetailsModel> updatedAvailableContacts =
            List.from(previousAvailableContacts.toList());

        yield ContactsLoadSuccess(
            selectedAvailableContact: event.selected ? contact : null,
            avaliableContacts: updatedAvailableContacts,
            contacts: successState.contacts,
            favouriteContacts: successState.favouriteContacts,
            selectedIndex: successState.selectedIndex,
            usersConnections: successState.usersConnections);
        // }
      }
    }
  }

  Stream<ContactsState> _mapContactUpdateFavourite(
      FavouritePostModel data) async* {
    try {
      globalBloc.add(ShowLoadingWidget());
      await contactsRepository.favouriteUpdate(data);

      add(ContactsRequested(hidden: true));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Favourite update successfully', event: null));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ContactsState> _mapContactsIndexChangedToState(int index) async* {
    if (state is ContactsLoadSuccess) {
      ContactsLoadSuccess currentState = state;

      yield ContactsLoadSuccess(
          selectedAvailableContact: currentState.selectedAvailableContact,
          avaliableContacts: currentState.avaliableContacts,
          contacts: currentState.contacts,
          favouriteContacts: currentState.favouriteContacts,
          usersConnections: currentState.usersConnections,
          selectedIndex: index);
    }

    yield* _mapContactsRequestedToState(false, index);
  }

  Stream<ContactsState> _mapContactsRequestedToState(
      bool hidden, int index) async* {
    // List<ContactDetailsModel> selectedAvaliableContacts;

    // if (state is ContactsLoadSuccess) {
    //   selectedAvaliableContacts = (state as ContactsLoadSuccess)
    //       .avaliableContacts
    //       .where((element) => element.selected != null && element.selected)
    //       .toList();
    // }

    ContactDetailsModel selectedAvaliableContact;

    if (state is ContactsLoadSuccess) {
      selectedAvaliableContact =
          (state as ContactsLoadSuccess).selectedAvailableContact;
    }

    if (!hidden)
      yield ContactsLoadInProgress(selectedIndex: state.selectedIndex);

    try {
      List<dynamic> fututres = await Future.wait([
        contactsRepository.getContact(),
        contactsRepository.getMyContacts()
      ]);

      ContactsResponseModel response = fututres[0];

      List<ContactDetailsModel> contacts = response.result.toList();
      List<ContactDetailsModel> avaliableContacts = contacts
          .where((element) =>
              element.accountType != null &&
              element.accountType == 'user' &&
              element.isAvailable != null &&
              element.isAvailable)
          .toList();

      // if (selectedAvaliableContacts != null &&
      //     selectedAvaliableContacts.isNotEmpty) {
      //   selectedAvaliableContacts.forEach((each) {
      //     int index =
      //         avaliableContacts.indexWhere((element) => element.id == each.id);

      //     if (index != -1)
      //       avaliableContacts[index] =
      //           avaliableContacts[index].rebuild((e) => e.selected = true);
      //   });
      // }

      if (selectedAvaliableContact != null) {
        int index = avaliableContacts
            .indexWhere((element) => element.id == selectedAvaliableContact.id);

        if (index != -1)
          avaliableContacts[index] =
              avaliableContacts[index].rebuild((e) => e.selected = true);
        else
          selectedAvaliableContact = null;
      }

      List<ContactDetailsModel> favouriteContacts = contacts
          .where(
              (element) => element.isFavourite != null && element.isFavourite)
          .toList();

      ConnectionsModel connections = fututres[1];

      List<ContactDetailsModel> usersConnections =
          connections.connections.usersConnections.toList();

      yield ContactsLoadSuccess(
          selectedAvailableContact: selectedAvaliableContact,
          contacts: contacts,
          favouriteContacts: favouriteContacts,
          avaliableContacts: avaliableContacts,
          usersConnections: usersConnections,
          selectedIndex: index != null ? index : state.selectedIndex);
    } on DioError catch (dioError) {
      log(dioError.toString());
      yield ContactsLoadFailure(selectedIndex: state.selectedIndex);
    } catch (e) {
      log(e.toString());
      yield ContactsLoadFailure(selectedIndex: state.selectedIndex);
    }
  }
}
