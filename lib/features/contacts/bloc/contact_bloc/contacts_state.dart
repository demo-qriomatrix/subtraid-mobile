part of 'contacts_bloc.dart';

abstract class ContactsState extends Equatable {
  final int selectedIndex;

  ContactsState({
    @required this.selectedIndex,
  });

  @override
  List<Object> get props => [
        selectedIndex,
      ];
}

class ContactsInitial extends ContactsState {
  ContactsInitial({@required int selectedIndex})
      : super(
          selectedIndex: selectedIndex,
        );
}

class ContactsLoadInProgress extends ContactsState {
  ContactsLoadInProgress({@required int selectedIndex})
      : super(
          selectedIndex: selectedIndex,
        );
}

class ContactsLoadSuccess extends ContactsState {
  final List<ContactDetailsModel> contacts;
  final List<ContactDetailsModel> avaliableContacts;
  final List<ContactDetailsModel> favouriteContacts;
  final List<ContactDetailsModel> usersConnections;
  final ContactDetailsModel selectedAvailableContact;
  ContactsLoadSuccess({
    @required this.contacts,
    @required this.avaliableContacts,
    @required this.favouriteContacts,
    @required this.usersConnections,
    @required this.selectedAvailableContact,
    @required int selectedIndex,
  }) : super(selectedIndex: selectedIndex);

  @override
  List<Object> get props => [
        selectedIndex,
        contacts,
        selectedAvailableContact,
        favouriteContacts,
        usersConnections,
        avaliableContacts.hashCode
      ];
}

class ContactsLoadFailure extends ContactsState {
  ContactsLoadFailure({@required int selectedIndex})
      : super(
          selectedIndex: selectedIndex,
        );
}
