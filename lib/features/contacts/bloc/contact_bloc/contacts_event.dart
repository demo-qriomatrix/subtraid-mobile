part of 'contacts_bloc.dart';

abstract class ContactsEvent extends Equatable {
  const ContactsEvent();

  @override
  List<Object> get props => [];
}

class ContactsRequested extends ContactsEvent {
  final bool hidden;
  ContactsRequested({@required this.hidden});
}

class ContactsIndexChanged extends ContactsEvent {
  final int index;

  ContactsIndexChanged({@required this.index});
}

class ContactUpdateFavourite extends ContactsEvent {
  final FavouritePostModel data;

  ContactUpdateFavourite({@required this.data});
}

class ContactSetSelected extends ContactsEvent {
  final bool selected;
  final String id;

  ContactSetSelected({@required this.id, @required this.selected});
}
