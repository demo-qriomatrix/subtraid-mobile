part of 'user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];
}

class UserInitial extends UserState {}

class UserLoadInProgress extends UserState {}

class UserLoadSuccess extends UserState {
  final UserModel user;
  final UserModel employer;

  UserLoadSuccess({@required this.user, @required this.employer});
}

class UserLoadFailure extends UserState {
  final String error;

  UserLoadFailure({@required this.error});
}
