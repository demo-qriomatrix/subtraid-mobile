import 'dart:async';

import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/authentication/models/user_response_model.dart';
import 'package:Subtraid/features/contacts/repository/contacts_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc({@required this.contactsRepository, @required this.userId})
      : super(UserInitial()) {
    add(UserRequested(userId: userId));
  }

  final ContactsRepository contactsRepository;
  final String userId;

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is UserRequested) {
      yield* _mapUserRequestedToState(event.userId);
    }
  }

  Stream<UserState> _mapUserRequestedToState(String userId) async* {
    yield UserLoadInProgress();
    try {
      UserResponseModel employer;
      UserResponseModel userResponseModel =
          await contactsRepository.getUser(userId);

      if (userResponseModel.user.accountType == 'user' &&
          userResponseModel.user.employer != null) {
        employer =
            await contactsRepository.getUser(userResponseModel.user.employer);
      }

      yield UserLoadSuccess(
          user: userResponseModel.user, employer: employer?.user);
    } catch (e) {
      print(e);
      yield UserLoadFailure(error: e.toString());
    }
  }
}
