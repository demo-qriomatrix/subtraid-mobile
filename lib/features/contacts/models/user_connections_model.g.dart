// GENERATED CODE - DO NOT MODIFY BY HAND

part of user_connections_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserConnectionsModel> _$userConnectionsModelSerializer =
    new _$UserConnectionsModelSerializer();

class _$UserConnectionsModelSerializer
    implements StructuredSerializer<UserConnectionsModel> {
  @override
  final Iterable<Type> types = const [
    UserConnectionsModel,
    _$UserConnectionsModel
  ];
  @override
  final String wireName = 'UserConnectionsModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, UserConnectionsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.usersConnections != null) {
      result
        ..add('usersConnections')
        ..add(serializers.serialize(object.usersConnections,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ContactDetailsModel)])));
    }
    return result;
  }

  @override
  UserConnectionsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserConnectionsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'usersConnections':
          result.usersConnections.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ContactDetailsModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$UserConnectionsModel extends UserConnectionsModel {
  @override
  final BuiltList<ContactDetailsModel> usersConnections;

  factory _$UserConnectionsModel(
          [void Function(UserConnectionsModelBuilder) updates]) =>
      (new UserConnectionsModelBuilder()..update(updates)).build();

  _$UserConnectionsModel._({this.usersConnections}) : super._();

  @override
  UserConnectionsModel rebuild(
          void Function(UserConnectionsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserConnectionsModelBuilder toBuilder() =>
      new UserConnectionsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserConnectionsModel &&
        usersConnections == other.usersConnections;
  }

  @override
  int get hashCode {
    return $jf($jc(0, usersConnections.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserConnectionsModel')
          ..add('usersConnections', usersConnections))
        .toString();
  }
}

class UserConnectionsModelBuilder
    implements Builder<UserConnectionsModel, UserConnectionsModelBuilder> {
  _$UserConnectionsModel _$v;

  ListBuilder<ContactDetailsModel> _usersConnections;
  ListBuilder<ContactDetailsModel> get usersConnections =>
      _$this._usersConnections ??= new ListBuilder<ContactDetailsModel>();
  set usersConnections(ListBuilder<ContactDetailsModel> usersConnections) =>
      _$this._usersConnections = usersConnections;

  UserConnectionsModelBuilder();

  UserConnectionsModelBuilder get _$this {
    if (_$v != null) {
      _usersConnections = _$v.usersConnections?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserConnectionsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserConnectionsModel;
  }

  @override
  void update(void Function(UserConnectionsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserConnectionsModel build() {
    _$UserConnectionsModel _$result;
    try {
      _$result = _$v ??
          new _$UserConnectionsModel._(
              usersConnections: _usersConnections?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'usersConnections';
        _usersConnections?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserConnectionsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
