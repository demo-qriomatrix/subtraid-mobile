library contact_detail_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'contact_detail_model.g.dart';

abstract class ContactDetailsModel
    implements Built<ContactDetailsModel, ContactDetailsModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get email;

  @nullable
  String get name;

  // @nullable
  // BuiltList<ContactLocationModel> get location;

  // @nullable
  // String get dateCreated;

  @nullable
  bool get isVerified;

  @nullable
  bool get isAvailable;

  @nullable
  bool get isFavourite;

  @nullable
  String get img;

  @nullable
  String get accountType;

  @nullable
  bool get selected;

  ContactDetailsModel._();

  factory ContactDetailsModel([updates(ContactDetailsModelBuilder b)]) =
      _$ContactDetailsModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ContactDetailsModel.serializer, this));
  }

  static ContactDetailsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ContactDetailsModel.serializer, json.decode(jsonString));
  }

  static Serializer<ContactDetailsModel> get serializer =>
      _$contactDetailsModelSerializer;
}
