library contacts_response_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'contact_detail_model.dart';

part 'contacts_response_model.g.dart';

abstract class ContactsResponseModel
    implements Built<ContactsResponseModel, ContactsResponseModelBuilder> {
  // @nullable
  // String get message;

  @nullable
  BuiltList<ContactDetailsModel> get result;

  // @nullable
  // int get count;

  // @nullable
  // String get lastdateCreated;

  ContactsResponseModel._();

  factory ContactsResponseModel([updates(ContactsResponseModelBuilder b)]) =
      _$ContactsResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ContactsResponseModel.serializer, this));
  }

  static ContactsResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ContactsResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<ContactsResponseModel> get serializer =>
      _$contactsResponseModelSerializer;
}
