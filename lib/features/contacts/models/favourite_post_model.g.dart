// GENERATED CODE - DO NOT MODIFY BY HAND

part of favourite_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<FavouritePostModel> _$favouritePostModelSerializer =
    new _$FavouritePostModelSerializer();

class _$FavouritePostModelSerializer
    implements StructuredSerializer<FavouritePostModel> {
  @override
  final Iterable<Type> types = const [FavouritePostModel, _$FavouritePostModel];
  @override
  final String wireName = 'FavouritePostModel';

  @override
  Iterable<Object> serialize(Serializers serializers, FavouritePostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.favouriteUserId != null) {
      result
        ..add('favouriteUserId')
        ..add(serializers.serialize(object.favouriteUserId,
            specifiedType: const FullType(String)));
    }
    if (object.isAdd != null) {
      result
        ..add('isAdd')
        ..add(serializers.serialize(object.isAdd,
            specifiedType: const FullType(bool)));
    }
    if (object.search != null) {
      result
        ..add('search')
        ..add(serializers.serialize(object.search,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  FavouritePostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FavouritePostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'favouriteUserId':
          result.favouriteUserId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isAdd':
          result.isAdd = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'search':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$FavouritePostModel extends FavouritePostModel {
  @override
  final String favouriteUserId;
  @override
  final bool isAdd;
  @override
  final String search;

  factory _$FavouritePostModel(
          [void Function(FavouritePostModelBuilder) updates]) =>
      (new FavouritePostModelBuilder()..update(updates)).build();

  _$FavouritePostModel._({this.favouriteUserId, this.isAdd, this.search})
      : super._();

  @override
  FavouritePostModel rebuild(
          void Function(FavouritePostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FavouritePostModelBuilder toBuilder() =>
      new FavouritePostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FavouritePostModel &&
        favouriteUserId == other.favouriteUserId &&
        isAdd == other.isAdd &&
        search == other.search;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, favouriteUserId.hashCode), isAdd.hashCode),
        search.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FavouritePostModel')
          ..add('favouriteUserId', favouriteUserId)
          ..add('isAdd', isAdd)
          ..add('search', search))
        .toString();
  }
}

class FavouritePostModelBuilder
    implements Builder<FavouritePostModel, FavouritePostModelBuilder> {
  _$FavouritePostModel _$v;

  String _favouriteUserId;
  String get favouriteUserId => _$this._favouriteUserId;
  set favouriteUserId(String favouriteUserId) =>
      _$this._favouriteUserId = favouriteUserId;

  bool _isAdd;
  bool get isAdd => _$this._isAdd;
  set isAdd(bool isAdd) => _$this._isAdd = isAdd;

  String _search;
  String get search => _$this._search;
  set search(String search) => _$this._search = search;

  FavouritePostModelBuilder();

  FavouritePostModelBuilder get _$this {
    if (_$v != null) {
      _favouriteUserId = _$v.favouriteUserId;
      _isAdd = _$v.isAdd;
      _search = _$v.search;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FavouritePostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$FavouritePostModel;
  }

  @override
  void update(void Function(FavouritePostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FavouritePostModel build() {
    final _$result = _$v ??
        new _$FavouritePostModel._(
            favouriteUserId: favouriteUserId, isAdd: isAdd, search: search);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
