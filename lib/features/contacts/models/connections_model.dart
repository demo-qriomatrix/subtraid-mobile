library connections_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'user_connections_model.dart';

part 'connections_model.g.dart';

abstract class ConnectionsModel
    implements Built<ConnectionsModel, ConnectionsModelBuilder> {
  @nullable
  UserConnectionsModel get connections;

  ConnectionsModel._();

  factory ConnectionsModel([updates(ConnectionsModelBuilder b)]) =
      _$ConnectionsModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ConnectionsModel.serializer, this));
  }

  static ConnectionsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ConnectionsModel.serializer, json.decode(jsonString));
  }

  static Serializer<ConnectionsModel> get serializer =>
      _$connectionsModelSerializer;
}
