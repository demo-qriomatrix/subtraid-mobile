library contract_details_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'contract_details_model.g.dart';

abstract class ContractDetailsModel
    implements Built<ContractDetailsModel, ContractDetailsModelBuilder> {
  @nullable
  int get amount;

  @nullable
  String get description;

  @nullable
  String get endDate;

  @nullable
  String get startDate;

  ContractDetailsModel._();

  factory ContractDetailsModel([updates(ContractDetailsModelBuilder b)]) =
      _$ContractDetailsModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ContractDetailsModel.serializer, this));
  }

  static ContractDetailsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ContractDetailsModel.serializer, json.decode(jsonString));
  }

  static Serializer<ContractDetailsModel> get serializer =>
      _$contractDetailsModelSerializer;
}
