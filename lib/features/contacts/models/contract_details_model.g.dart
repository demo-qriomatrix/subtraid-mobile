// GENERATED CODE - DO NOT MODIFY BY HAND

part of contract_details_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ContractDetailsModel> _$contractDetailsModelSerializer =
    new _$ContractDetailsModelSerializer();

class _$ContractDetailsModelSerializer
    implements StructuredSerializer<ContractDetailsModel> {
  @override
  final Iterable<Type> types = const [
    ContractDetailsModel,
    _$ContractDetailsModel
  ];
  @override
  final String wireName = 'ContractDetailsModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ContractDetailsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.amount != null) {
      result
        ..add('amount')
        ..add(serializers.serialize(object.amount,
            specifiedType: const FullType(int)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.endDate != null) {
      result
        ..add('endDate')
        ..add(serializers.serialize(object.endDate,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('startDate')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ContractDetailsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ContractDetailsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'amount':
          result.amount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'endDate':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'startDate':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ContractDetailsModel extends ContractDetailsModel {
  @override
  final int amount;
  @override
  final String description;
  @override
  final String endDate;
  @override
  final String startDate;

  factory _$ContractDetailsModel(
          [void Function(ContractDetailsModelBuilder) updates]) =>
      (new ContractDetailsModelBuilder()..update(updates)).build();

  _$ContractDetailsModel._(
      {this.amount, this.description, this.endDate, this.startDate})
      : super._();

  @override
  ContractDetailsModel rebuild(
          void Function(ContractDetailsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContractDetailsModelBuilder toBuilder() =>
      new ContractDetailsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContractDetailsModel &&
        amount == other.amount &&
        description == other.description &&
        endDate == other.endDate &&
        startDate == other.startDate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, amount.hashCode), description.hashCode),
            endDate.hashCode),
        startDate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContractDetailsModel')
          ..add('amount', amount)
          ..add('description', description)
          ..add('endDate', endDate)
          ..add('startDate', startDate))
        .toString();
  }
}

class ContractDetailsModelBuilder
    implements Builder<ContractDetailsModel, ContractDetailsModelBuilder> {
  _$ContractDetailsModel _$v;

  int _amount;
  int get amount => _$this._amount;
  set amount(int amount) => _$this._amount = amount;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _endDate;
  String get endDate => _$this._endDate;
  set endDate(String endDate) => _$this._endDate = endDate;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  ContractDetailsModelBuilder();

  ContractDetailsModelBuilder get _$this {
    if (_$v != null) {
      _amount = _$v.amount;
      _description = _$v.description;
      _endDate = _$v.endDate;
      _startDate = _$v.startDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContractDetailsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContractDetailsModel;
  }

  @override
  void update(void Function(ContractDetailsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContractDetailsModel build() {
    final _$result = _$v ??
        new _$ContractDetailsModel._(
            amount: amount,
            description: description,
            endDate: endDate,
            startDate: startDate);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
