library add_employee_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'contract_details_model.dart';

part 'add_employee_model.g.dart';

abstract class AddEmployeeModel
    implements Built<AddEmployeeModel, AddEmployeeModelBuilder> {
  @nullable
  String get employerMessage;

  @nullable
  bool get isAdmin;

  @nullable
  int get rate;

  @nullable
  String get role;

  @nullable
  String get type;

  @nullable
  ContractDetailsModel get contractDetails;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  AddEmployeeModel._();

  factory AddEmployeeModel([updates(AddEmployeeModelBuilder b)]) =
      _$AddEmployeeModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(AddEmployeeModel.serializer, this));
  }

  static AddEmployeeModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddEmployeeModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddEmployeeModel> get serializer =>
      _$addEmployeeModelSerializer;
}
