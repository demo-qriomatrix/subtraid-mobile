// GENERATED CODE - DO NOT MODIFY BY HAND

part of contact_detail_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ContactDetailsModel> _$contactDetailsModelSerializer =
    new _$ContactDetailsModelSerializer();

class _$ContactDetailsModelSerializer
    implements StructuredSerializer<ContactDetailsModel> {
  @override
  final Iterable<Type> types = const [
    ContactDetailsModel,
    _$ContactDetailsModel
  ];
  @override
  final String wireName = 'ContactDetailsModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ContactDetailsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.isVerified != null) {
      result
        ..add('isVerified')
        ..add(serializers.serialize(object.isVerified,
            specifiedType: const FullType(bool)));
    }
    if (object.isAvailable != null) {
      result
        ..add('isAvailable')
        ..add(serializers.serialize(object.isAvailable,
            specifiedType: const FullType(bool)));
    }
    if (object.isFavourite != null) {
      result
        ..add('isFavourite')
        ..add(serializers.serialize(object.isFavourite,
            specifiedType: const FullType(bool)));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    if (object.accountType != null) {
      result
        ..add('accountType')
        ..add(serializers.serialize(object.accountType,
            specifiedType: const FullType(String)));
    }
    if (object.selected != null) {
      result
        ..add('selected')
        ..add(serializers.serialize(object.selected,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ContactDetailsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ContactDetailsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isVerified':
          result.isVerified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isAvailable':
          result.isAvailable = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isFavourite':
          result.isFavourite = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'accountType':
          result.accountType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'selected':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ContactDetailsModel extends ContactDetailsModel {
  @override
  final String id;
  @override
  final String email;
  @override
  final String name;
  @override
  final bool isVerified;
  @override
  final bool isAvailable;
  @override
  final bool isFavourite;
  @override
  final String img;
  @override
  final String accountType;
  @override
  final bool selected;

  factory _$ContactDetailsModel(
          [void Function(ContactDetailsModelBuilder) updates]) =>
      (new ContactDetailsModelBuilder()..update(updates)).build();

  _$ContactDetailsModel._(
      {this.id,
      this.email,
      this.name,
      this.isVerified,
      this.isAvailable,
      this.isFavourite,
      this.img,
      this.accountType,
      this.selected})
      : super._();

  @override
  ContactDetailsModel rebuild(
          void Function(ContactDetailsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContactDetailsModelBuilder toBuilder() =>
      new ContactDetailsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContactDetailsModel &&
        id == other.id &&
        email == other.email &&
        name == other.name &&
        isVerified == other.isVerified &&
        isAvailable == other.isAvailable &&
        isFavourite == other.isFavourite &&
        img == other.img &&
        accountType == other.accountType &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, id.hashCode), email.hashCode),
                                name.hashCode),
                            isVerified.hashCode),
                        isAvailable.hashCode),
                    isFavourite.hashCode),
                img.hashCode),
            accountType.hashCode),
        selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContactDetailsModel')
          ..add('id', id)
          ..add('email', email)
          ..add('name', name)
          ..add('isVerified', isVerified)
          ..add('isAvailable', isAvailable)
          ..add('isFavourite', isFavourite)
          ..add('img', img)
          ..add('accountType', accountType)
          ..add('selected', selected))
        .toString();
  }
}

class ContactDetailsModelBuilder
    implements Builder<ContactDetailsModel, ContactDetailsModelBuilder> {
  _$ContactDetailsModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  bool _isVerified;
  bool get isVerified => _$this._isVerified;
  set isVerified(bool isVerified) => _$this._isVerified = isVerified;

  bool _isAvailable;
  bool get isAvailable => _$this._isAvailable;
  set isAvailable(bool isAvailable) => _$this._isAvailable = isAvailable;

  bool _isFavourite;
  bool get isFavourite => _$this._isFavourite;
  set isFavourite(bool isFavourite) => _$this._isFavourite = isFavourite;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  String _accountType;
  String get accountType => _$this._accountType;
  set accountType(String accountType) => _$this._accountType = accountType;

  bool _selected;
  bool get selected => _$this._selected;
  set selected(bool selected) => _$this._selected = selected;

  ContactDetailsModelBuilder();

  ContactDetailsModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _email = _$v.email;
      _name = _$v.name;
      _isVerified = _$v.isVerified;
      _isAvailable = _$v.isAvailable;
      _isFavourite = _$v.isFavourite;
      _img = _$v.img;
      _accountType = _$v.accountType;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContactDetailsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContactDetailsModel;
  }

  @override
  void update(void Function(ContactDetailsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContactDetailsModel build() {
    final _$result = _$v ??
        new _$ContactDetailsModel._(
            id: id,
            email: email,
            name: name,
            isVerified: isVerified,
            isAvailable: isAvailable,
            isFavourite: isFavourite,
            img: img,
            accountType: accountType,
            selected: selected);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
