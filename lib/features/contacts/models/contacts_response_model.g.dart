// GENERATED CODE - DO NOT MODIFY BY HAND

part of contacts_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ContactsResponseModel> _$contactsResponseModelSerializer =
    new _$ContactsResponseModelSerializer();

class _$ContactsResponseModelSerializer
    implements StructuredSerializer<ContactsResponseModel> {
  @override
  final Iterable<Type> types = const [
    ContactsResponseModel,
    _$ContactsResponseModel
  ];
  @override
  final String wireName = 'ContactsResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ContactsResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.result != null) {
      result
        ..add('result')
        ..add(serializers.serialize(object.result,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ContactDetailsModel)])));
    }
    return result;
  }

  @override
  ContactsResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ContactsResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'result':
          result.result.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ContactDetailsModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$ContactsResponseModel extends ContactsResponseModel {
  @override
  final BuiltList<ContactDetailsModel> result;

  factory _$ContactsResponseModel(
          [void Function(ContactsResponseModelBuilder) updates]) =>
      (new ContactsResponseModelBuilder()..update(updates)).build();

  _$ContactsResponseModel._({this.result}) : super._();

  @override
  ContactsResponseModel rebuild(
          void Function(ContactsResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContactsResponseModelBuilder toBuilder() =>
      new ContactsResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContactsResponseModel && result == other.result;
  }

  @override
  int get hashCode {
    return $jf($jc(0, result.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContactsResponseModel')
          ..add('result', result))
        .toString();
  }
}

class ContactsResponseModelBuilder
    implements Builder<ContactsResponseModel, ContactsResponseModelBuilder> {
  _$ContactsResponseModel _$v;

  ListBuilder<ContactDetailsModel> _result;
  ListBuilder<ContactDetailsModel> get result =>
      _$this._result ??= new ListBuilder<ContactDetailsModel>();
  set result(ListBuilder<ContactDetailsModel> result) =>
      _$this._result = result;

  ContactsResponseModelBuilder();

  ContactsResponseModelBuilder get _$this {
    if (_$v != null) {
      _result = _$v.result?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContactsResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContactsResponseModel;
  }

  @override
  void update(void Function(ContactsResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContactsResponseModel build() {
    _$ContactsResponseModel _$result;
    try {
      _$result = _$v ?? new _$ContactsResponseModel._(result: _result?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'result';
        _result?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ContactsResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
