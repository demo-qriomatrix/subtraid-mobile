// GENERATED CODE - DO NOT MODIFY BY HAND

part of contact_location_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ContactLocationModel> _$contactLocationModelSerializer =
    new _$ContactLocationModelSerializer();

class _$ContactLocationModelSerializer
    implements StructuredSerializer<ContactLocationModel> {
  @override
  final Iterable<Type> types = const [
    ContactLocationModel,
    _$ContactLocationModel
  ];
  @override
  final String wireName = 'ContactLocationModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ContactLocationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object>[];
  }

  @override
  ContactLocationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new ContactLocationModelBuilder().build();
  }
}

class _$ContactLocationModel extends ContactLocationModel {
  factory _$ContactLocationModel(
          [void Function(ContactLocationModelBuilder) updates]) =>
      (new ContactLocationModelBuilder()..update(updates)).build();

  _$ContactLocationModel._() : super._();

  @override
  ContactLocationModel rebuild(
          void Function(ContactLocationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContactLocationModelBuilder toBuilder() =>
      new ContactLocationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContactLocationModel;
  }

  @override
  int get hashCode {
    return 575072824;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ContactLocationModel').toString();
  }
}

class ContactLocationModelBuilder
    implements Builder<ContactLocationModel, ContactLocationModelBuilder> {
  _$ContactLocationModel _$v;

  ContactLocationModelBuilder();

  @override
  void replace(ContactLocationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ContactLocationModel;
  }

  @override
  void update(void Function(ContactLocationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContactLocationModel build() {
    final _$result = _$v ?? new _$ContactLocationModel._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
