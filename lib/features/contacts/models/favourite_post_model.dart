library favourite_post_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'favourite_post_model.g.dart';

abstract class FavouritePostModel
    implements Built<FavouritePostModel, FavouritePostModelBuilder> {
  @nullable
  String get favouriteUserId;

  @nullable
  bool get isAdd;

  @nullable
  String get search;

  FavouritePostModel._();

  factory FavouritePostModel([updates(FavouritePostModelBuilder b)]) =
      _$FavouritePostModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(FavouritePostModel.serializer, this));
  }

  static FavouritePostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        FavouritePostModel.serializer, json.decode(jsonString));
  }

  static Serializer<FavouritePostModel> get serializer =>
      _$favouritePostModelSerializer;
}
