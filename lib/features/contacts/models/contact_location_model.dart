library contact_location_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'contact_location_model.g.dart';

abstract class ContactLocationModel
    implements Built<ContactLocationModel, ContactLocationModelBuilder> {
  //  @nullable
  // @BuiltValueField(wireName: '_id')
  // String get id;

  // @nullable
  // String get address;

  // @nullable
  // String get unit;

  // @nullable
  // String get city;

  // @nullable
  // String get province;

  // @nullable
  // String get country;

  // @nullable
  // String get zip;

  // @nullable
  // String get formattedAddress;

  // @nullable
  // double get lat;

  // @nullable
  // double get lng;

  ContactLocationModel._();

  factory ContactLocationModel([updates(ContactLocationModelBuilder b)]) =
      _$ContactLocationModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ContactLocationModel.serializer, this));
  }

  static ContactLocationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ContactLocationModel.serializer, json.decode(jsonString));
  }

  static Serializer<ContactLocationModel> get serializer =>
      _$contactLocationModelSerializer;
}
