library user_connections_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'contact_detail_model.dart';

part 'user_connections_model.g.dart';

abstract class UserConnectionsModel
    implements Built<UserConnectionsModel, UserConnectionsModelBuilder> {
  @nullable
  BuiltList<ContactDetailsModel> get usersConnections;

  UserConnectionsModel._();

  factory UserConnectionsModel([updates(UserConnectionsModelBuilder b)]) =
      _$UserConnectionsModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(UserConnectionsModel.serializer, this));
  }

  static UserConnectionsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserConnectionsModel.serializer, json.decode(jsonString));
  }

  static Serializer<UserConnectionsModel> get serializer =>
      _$userConnectionsModelSerializer;
}
