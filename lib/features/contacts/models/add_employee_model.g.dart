// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_employee_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddEmployeeModel> _$addEmployeeModelSerializer =
    new _$AddEmployeeModelSerializer();

class _$AddEmployeeModelSerializer
    implements StructuredSerializer<AddEmployeeModel> {
  @override
  final Iterable<Type> types = const [AddEmployeeModel, _$AddEmployeeModel];
  @override
  final String wireName = 'AddEmployeeModel';

  @override
  Iterable<Object> serialize(Serializers serializers, AddEmployeeModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.employerMessage != null) {
      result
        ..add('employerMessage')
        ..add(serializers.serialize(object.employerMessage,
            specifiedType: const FullType(String)));
    }
    if (object.isAdmin != null) {
      result
        ..add('isAdmin')
        ..add(serializers.serialize(object.isAdmin,
            specifiedType: const FullType(bool)));
    }
    if (object.rate != null) {
      result
        ..add('rate')
        ..add(serializers.serialize(object.rate,
            specifiedType: const FullType(int)));
    }
    if (object.role != null) {
      result
        ..add('role')
        ..add(serializers.serialize(object.role,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.contractDetails != null) {
      result
        ..add('contractDetails')
        ..add(serializers.serialize(object.contractDetails,
            specifiedType: const FullType(ContractDetailsModel)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AddEmployeeModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddEmployeeModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'employerMessage':
          result.employerMessage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isAdmin':
          result.isAdmin = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'rate':
          result.rate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'role':
          result.role = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'contractDetails':
          result.contractDetails.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ContractDetailsModel))
              as ContractDetailsModel);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AddEmployeeModel extends AddEmployeeModel {
  @override
  final String employerMessage;
  @override
  final bool isAdmin;
  @override
  final int rate;
  @override
  final String role;
  @override
  final String type;
  @override
  final ContractDetailsModel contractDetails;
  @override
  final String id;

  factory _$AddEmployeeModel(
          [void Function(AddEmployeeModelBuilder) updates]) =>
      (new AddEmployeeModelBuilder()..update(updates)).build();

  _$AddEmployeeModel._(
      {this.employerMessage,
      this.isAdmin,
      this.rate,
      this.role,
      this.type,
      this.contractDetails,
      this.id})
      : super._();

  @override
  AddEmployeeModel rebuild(void Function(AddEmployeeModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddEmployeeModelBuilder toBuilder() =>
      new AddEmployeeModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddEmployeeModel &&
        employerMessage == other.employerMessage &&
        isAdmin == other.isAdmin &&
        rate == other.rate &&
        role == other.role &&
        type == other.type &&
        contractDetails == other.contractDetails &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, employerMessage.hashCode), isAdmin.hashCode),
                        rate.hashCode),
                    role.hashCode),
                type.hashCode),
            contractDetails.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddEmployeeModel')
          ..add('employerMessage', employerMessage)
          ..add('isAdmin', isAdmin)
          ..add('rate', rate)
          ..add('role', role)
          ..add('type', type)
          ..add('contractDetails', contractDetails)
          ..add('id', id))
        .toString();
  }
}

class AddEmployeeModelBuilder
    implements Builder<AddEmployeeModel, AddEmployeeModelBuilder> {
  _$AddEmployeeModel _$v;

  String _employerMessage;
  String get employerMessage => _$this._employerMessage;
  set employerMessage(String employerMessage) =>
      _$this._employerMessage = employerMessage;

  bool _isAdmin;
  bool get isAdmin => _$this._isAdmin;
  set isAdmin(bool isAdmin) => _$this._isAdmin = isAdmin;

  int _rate;
  int get rate => _$this._rate;
  set rate(int rate) => _$this._rate = rate;

  String _role;
  String get role => _$this._role;
  set role(String role) => _$this._role = role;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  ContractDetailsModelBuilder _contractDetails;
  ContractDetailsModelBuilder get contractDetails =>
      _$this._contractDetails ??= new ContractDetailsModelBuilder();
  set contractDetails(ContractDetailsModelBuilder contractDetails) =>
      _$this._contractDetails = contractDetails;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  AddEmployeeModelBuilder();

  AddEmployeeModelBuilder get _$this {
    if (_$v != null) {
      _employerMessage = _$v.employerMessage;
      _isAdmin = _$v.isAdmin;
      _rate = _$v.rate;
      _role = _$v.role;
      _type = _$v.type;
      _contractDetails = _$v.contractDetails?.toBuilder();
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddEmployeeModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddEmployeeModel;
  }

  @override
  void update(void Function(AddEmployeeModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddEmployeeModel build() {
    _$AddEmployeeModel _$result;
    try {
      _$result = _$v ??
          new _$AddEmployeeModel._(
              employerMessage: employerMessage,
              isAdmin: isAdmin,
              rate: rate,
              role: role,
              type: type,
              contractDetails: _contractDetails?.build(),
              id: id);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'contractDetails';
        _contractDetails?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AddEmployeeModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
