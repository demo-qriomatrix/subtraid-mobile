// GENERATED CODE - DO NOT MODIFY BY HAND

part of connections_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ConnectionsModel> _$connectionsModelSerializer =
    new _$ConnectionsModelSerializer();

class _$ConnectionsModelSerializer
    implements StructuredSerializer<ConnectionsModel> {
  @override
  final Iterable<Type> types = const [ConnectionsModel, _$ConnectionsModel];
  @override
  final String wireName = 'ConnectionsModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ConnectionsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.connections != null) {
      result
        ..add('connections')
        ..add(serializers.serialize(object.connections,
            specifiedType: const FullType(UserConnectionsModel)));
    }
    return result;
  }

  @override
  ConnectionsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ConnectionsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'connections':
          result.connections.replace(serializers.deserialize(value,
                  specifiedType: const FullType(UserConnectionsModel))
              as UserConnectionsModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ConnectionsModel extends ConnectionsModel {
  @override
  final UserConnectionsModel connections;

  factory _$ConnectionsModel(
          [void Function(ConnectionsModelBuilder) updates]) =>
      (new ConnectionsModelBuilder()..update(updates)).build();

  _$ConnectionsModel._({this.connections}) : super._();

  @override
  ConnectionsModel rebuild(void Function(ConnectionsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ConnectionsModelBuilder toBuilder() =>
      new ConnectionsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ConnectionsModel && connections == other.connections;
  }

  @override
  int get hashCode {
    return $jf($jc(0, connections.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ConnectionsModel')
          ..add('connections', connections))
        .toString();
  }
}

class ConnectionsModelBuilder
    implements Builder<ConnectionsModel, ConnectionsModelBuilder> {
  _$ConnectionsModel _$v;

  UserConnectionsModelBuilder _connections;
  UserConnectionsModelBuilder get connections =>
      _$this._connections ??= new UserConnectionsModelBuilder();
  set connections(UserConnectionsModelBuilder connections) =>
      _$this._connections = connections;

  ConnectionsModelBuilder();

  ConnectionsModelBuilder get _$this {
    if (_$v != null) {
      _connections = _$v.connections?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ConnectionsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ConnectionsModel;
  }

  @override
  void update(void Function(ConnectionsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ConnectionsModel build() {
    _$ConnectionsModel _$result;
    try {
      _$result =
          _$v ?? new _$ConnectionsModel._(connections: _connections?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'connections';
        _connections?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ConnectionsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
