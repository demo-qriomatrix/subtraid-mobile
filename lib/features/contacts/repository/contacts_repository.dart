import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/authentication/models/user_response_model.dart';
import 'package:Subtraid/features/contacts/models/add_employee_model.dart';
import 'package:Subtraid/features/contacts/models/connections_model.dart';
import 'package:Subtraid/features/contacts/models/contacts_response_model.dart';
import 'package:Subtraid/features/contacts/models/favourite_post_model.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class ContactsRepository {
  final HttpClient httpClient;

  ContactsRepository({@required this.httpClient});

  Future<ContactsResponseModel> getContact() async {
    Map<String, dynamic> params = Map();

    params.putIfAbsent('lastdateCreated', () => null);
    params.putIfAbsent('search', () => null);
    // params.putIfAbsent('limit', () => 10);

    final Response response = await httpClient.dio
        .get(EndpointConfig.contactsUrl, queryParameters: params);

    return ContactsResponseModel.fromJson(json.encode(response.data));
  }

  Future<ConnectionsModel> getMyContacts() async {
    Map<String, dynamic> params = Map();

    params.putIfAbsent('search', () => null);

    final Response response = await httpClient.dio
        .get(EndpointConfig.myContactsUrl, queryParameters: params);

    return ConnectionsModel.fromJson(json.encode(response.data));
  }

  Future<Response> favouriteUpdate(FavouritePostModel data) async {
    return await httpClient.dio
        .post(EndpointConfig.userFavourite, data: data.toJson());
  }

  Future<ApiResponseModel> addEmployee(AddEmployeeModel data) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.companyAddEmployee, data: data.toJson());

    return ApiResponseModel.fromJson(json.encode(response.data));
  }

  Future<UserResponseModel> getUser(String userId) async {
    Response response =
        await httpClient.dio.get(EndpointConfig.contactsUrl + '/$userId');

    return UserResponseModel.fromJson(json.encode(response.data));
  }
}
