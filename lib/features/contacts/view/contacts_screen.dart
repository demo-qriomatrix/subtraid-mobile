import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/contacts/bloc/contact_bloc/contacts_bloc.dart';
import 'package:Subtraid/features/contacts/widgets/add_employee_dialog.dart';
import 'package:Subtraid/features/contacts/widgets/contact_tile_widget.dart';
import 'package:Subtraid/features/more/company/view/company_screen.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/custom_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContactsScreen extends StatefulWidget {
  final int index;

  ContactsScreen({@required this.index});

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  TextEditingController contactSearch = TextEditingController();
  String search = '';

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ContactsBloc(
          index: widget.index,
          contactsRepository: sl(),
          globalBloc: context.read<GlobalBloc>()),
      child: Column(
        children: [
          StAppBar(
            title: 'Contacts',
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              controller: contactSearch,
              onChanged: (val) {
                setState(() {
                  search = val;
                });
              },
              keyboardType: TextInputType.text,
              decoration: searchInputDecoration.copyWith(
                  hintText: "Search for a contact",
                  suffixIcon: IconButton(
                      constraints: BoxConstraints(),
                      icon: Icon(
                        Icons.close,
                        size: 20,
                      ),
                      onPressed: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        contactSearch.clear();
                        setState(() {
                          search = '';
                        });
                      })),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: getMinHeight(context),
                    ),
                    child: BlocBuilder<ContactsBloc, ContactsState>(
                      builder: (context, state) {
                        if (state is ContactsLoadSuccess ||
                            state is ContactsLoadInProgress) {
                          if (state is ContactsLoadSuccess &&
                              state.contacts.isEmpty) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [Text('No contacts')],
                            );
                          }

                          return Column(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  CustomIcon(
                                    icon: LocalImages.myList,
                                    title: 'My Contacts',
                                    selected: state.selectedIndex == 0,
                                    ontap: () {
                                      context
                                          .read<ContactsBloc>()
                                          .add(ContactsIndexChanged(index: 0));
                                    },
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  CustomIcon(
                                    icon: LocalImages.allUsers,
                                    title: 'All users',
                                    selected: state.selectedIndex == 1,
                                    ontap: () {
                                      context
                                          .read<ContactsBloc>()
                                          .add(ContactsIndexChanged(index: 1));
                                    },
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  CustomIcon(
                                    icon: LocalImages.streetView,
                                    title: 'Available Users',
                                    selected: state.selectedIndex == 2,
                                    ontap: () {
                                      context
                                          .read<ContactsBloc>()
                                          .add(ContactsIndexChanged(index: 2));
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.end,
                              //   children: <Widget>[
                              //     Text(
                              //       'Clear',
                              //       style: TextStyle(
                              //           color: ColorConfig.orange),
                              //     )
                              //   ],
                              // ),
                              if (state.selectedIndex == 2 &&
                                  state is ContactsLoadSuccess)
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 30, right: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      FlatButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        color: state.selectedAvailableContact !=
                                                null
                                            ? ColorConfig.primary
                                            : ColorConfig.primary
                                                .withOpacity(0.5),
                                        child: Text(
                                          'Offer Employement',
                                          style: TextStyle(fontSize: 12)
                                              .copyWith(color: Colors.white),
                                        ),
                                        onPressed: () {
                                          ContactsBloc contactsBloc =
                                              context.read<ContactsBloc>();

                                          showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              useRootNavigator: true,
                                              builder: (context) {
                                                return Dialog(
                                                    insetPadding:
                                                        EdgeInsets.symmetric(
                                                            horizontal: 20,
                                                            vertical: 20),
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20)),
                                                    backgroundColor:
                                                        Colors.white,
                                                    child: AddEmployeeDialog(
                                                      contactsBloc:
                                                          contactsBloc,
                                                    ));
                                              }).then((value) {
                                            if (value == true) {
                                              Navigator.of(context)
                                                  .push(MaterialPageRoute(
                                                builder: (context) =>
                                                    CompanyScreen(
                                                  selectedIndex: 1,
                                                ),
                                              ));
                                            }
                                          });
                                        },
                                      ),
                                      FlatButton(
                                          materialTapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          onPressed: () {
                                            if (state
                                                    .selectedAvailableContact !=
                                                null)
                                              context.read<ContactsBloc>().add(
                                                  ContactSetSelected(
                                                      id: state
                                                          .selectedAvailableContact
                                                          .id,
                                                      selected: false));
                                          },
                                          child: Text('Clear',
                                              style: TextStyle(
                                                  color: ColorConfig.orange,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 12)))
                                    ],
                                  ),
                                ),
                              Expanded(
                                child: ListView(
                                  padding: EdgeInsets.zero,
                                  children: [
                                    if (state is ContactsLoadInProgress)
                                      SizedBox(
                                        height: getMinHeight(context) - 200,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            CircularProgressIndicator()
                                          ],
                                        ),
                                      ),
                                    if (state is ContactsLoadSuccess)
                                      Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 30),
                                          child: Column(children: <Widget>[
                                            if (state.selectedIndex == 0)
                                              ...state.usersConnections
                                                  .where((element) => search !=
                                                          ''
                                                      ? element.name
                                                          .toLowerCase()
                                                          .contains(search
                                                              .toLowerCase())
                                                      : true)
                                                  .toList()
                                                  .asMap()
                                                  .map((i, f) => MapEntry(
                                                      i,
                                                      ContactTile(
                                                        contact: f,
                                                        index: i,
                                                        canFavourite: true,
                                                        canSelect: false,
                                                      )))
                                                  .values
                                                  .toList(),
                                            if (state.selectedIndex == 1)
                                              ...state.contacts
                                                  .where((element) => search !=
                                                          ''
                                                      ? element.name
                                                          .toLowerCase()
                                                          .contains(search
                                                              .toLowerCase())
                                                      : true)
                                                  .toList()
                                                  .asMap()
                                                  .map((i, f) => MapEntry(
                                                      i,
                                                      ContactTile(
                                                        contact: f,
                                                        index: i,
                                                        canFavourite: false,
                                                        canSelect: false,
                                                      )))
                                                  .values
                                                  .toList(),
                                            if (state.selectedIndex == 2)
                                              ...state.avaliableContacts
                                                  .where((element) => search !=
                                                          ''
                                                      ? element.name
                                                          .toLowerCase()
                                                          .contains(search
                                                              .toLowerCase())
                                                      : true)
                                                  .toList()
                                                  .asMap()
                                                  .map((i, f) => MapEntry(
                                                      i,
                                                      ContactTile(
                                                        contact: f,
                                                        index: i,
                                                        canFavourite: true,
                                                        canSelect: true,
                                                      )))
                                                  .values
                                                  .toList()
                                          ]))
                                  ],
                                ),
                              ),
                            ],
                          );
                        }
                        if (state is ContactsLoadFailure) {
                          return Text('Something went wrong');
                        }
                        return Container();
                      },
                    ),
                  )))
        ],
      ),
    );
  }
}
