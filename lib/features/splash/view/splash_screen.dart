import 'package:Subtraid/features/authentication/onboard/onboard_screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/authentication/views/login_screen.dart';
import 'package:Subtraid/features/main/view/main_screen.dart';

class SplashScreen extends StatefulWidget {
  final bool fromRegister;

  SplashScreen({this.fromRegister});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool delayFutureSolved = false;
  bool checkTokenFutureSolved = false;
  AuthenticationState authState;

  @override
  void initState() {
    super.initState();
    splashDelay();
    context.read<AuthenticationBloc>().add(CheckLocalToken());
  }

  void splashDelay() {
    Future.delayed(Duration(seconds: 2), () {
      setState(() {
        delayFutureSolved = true;
      });
      checkFuturesAndPerform();
    });
  }

  void checkFuturesAndPerform() {
    authState = context.read<AuthenticationBloc>().state;

    if (!(authState is AuthenticationInitial) && delayFutureSolved) {
      if (authState is AuthentcatedState) {
        if (widget.fromRegister == true) {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => OnboardingScreen()));
        } else {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => MainScreen()));
        }
      } else {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => LoginScreen()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
          checkFuturesAndPerform();
        },
        child: Scaffold(
          backgroundColor: ColorConfig.primary,
          body: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                child: Image.asset(
                  'assets/images/splash-image.png',
                  fit: BoxFit.fitWidth,
                  width: MediaQuery.of(context).size.width,
                ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/logo.png',
                      width: 150,
                      fit: BoxFit.fitWidth,
                    ),
                    SizedBox(height: 30),
                    Text(
                      'Everything you need to run your business better',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    SizedBox(height: 100),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
