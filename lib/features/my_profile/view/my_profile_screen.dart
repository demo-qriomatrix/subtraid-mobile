import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/my_profile/bloc/my_profile_bloc.dart';
import 'package:Subtraid/features/my_profile/features/general/view/general_screen.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/custom_icon.dart';

class MyProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: BlocProvider<MyProfileBloc>(
        create: (context) => MyProfileBloc(),
        child: Column(
          children: [
            StAppBar(
              title: 'My Profile',
            ),
            Expanded(
                child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
              builder: (context, state) {
                if (state is AuthentcatedState) {
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 25, vertical: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  state.user.name,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600,
                                      color: ColorConfig.orange),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  state.user.email,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: ColorConfig.grey15),
                                ),
                              ],
                            ),
                            CircleAvatar(
                              backgroundImage:
                                  AssetImage(LocalImages.defaultAvatar),
                              radius: 24,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(50),
                                      topRight: Radius.circular(50))),
                              child: ConstrainedBox(
                                  constraints: BoxConstraints(
                                      minHeight: getMinHeight(context)),
                                  child: BlocBuilder<MyProfileBloc,
                                      MyProfileState>(
                                    builder: (context, state) {
                                      return Column(children: <Widget>[
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            CustomIcon(
                                              icon: LocalImages.info,
                                              title: 'General',
                                              selected:
                                                  state.selectedIndex == 0,
                                              ontap: () {
                                                context
                                                    .read<MyProfileBloc>()
                                                    .add(MyProfileIndexChanged(
                                                        index: 0));
                                              },
                                            ),
                                            CustomIcon(
                                              icon: LocalImages.settings,
                                              title: 'Settings',
                                              selected:
                                                  state.selectedIndex == 1,
                                              ontap: () {
                                                context
                                                    .read<MyProfileBloc>()
                                                    .add(MyProfileIndexChanged(
                                                        index: 1));
                                              },
                                            ),
                                            CustomIcon(
                                              icon: LocalImages.message,
                                              title: 'Messages',
                                              selected:
                                                  state.selectedIndex == 2,
                                              ontap: () {
                                                context
                                                    .read<MyProfileBloc>()
                                                    .add(MyProfileIndexChanged(
                                                        index: 2));
                                              },
                                            ),
                                          ],
                                        ),
                                        Expanded(child: GeneralScreen())
                                      ]);
                                    },
                                  ))))
                    ],
                  );
                }
                return Container();
              },
            ))
          ],
        ),
      ),
    );
  }
}
