part of 'my_profile_bloc.dart';

class MyProfileState extends Equatable {
  final int selectedIndex;

  MyProfileState({@required this.selectedIndex});

  @override
  List<Object> get props => [selectedIndex];
}
