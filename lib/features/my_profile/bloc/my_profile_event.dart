part of 'my_profile_bloc.dart';

abstract class MyProfileEvent extends Equatable {
  const MyProfileEvent();

  @override
  List<Object> get props => [];
}

class MyProfileIndexChanged extends MyProfileEvent {
  final int index;

  MyProfileIndexChanged({@required this.index});
}
