import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'my_profile_event.dart';
part 'my_profile_state.dart';

class MyProfileBloc extends Bloc<MyProfileEvent, MyProfileState> {
  MyProfileBloc() : super(MyProfileState(selectedIndex: 0));

  @override
  Stream<MyProfileState> mapEventToState(
    MyProfileEvent event,
  ) async* {
    if (event is MyProfileIndexChanged) {
      yield MyProfileState(selectedIndex: event.index);
    }
  }
}
