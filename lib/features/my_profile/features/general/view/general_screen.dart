import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:Subtraid/core/config/color_config.dart';

import 'change_password_screen.dart';
import 'employer_details_screen.dart';
import 'general_information_screen.dart';

class GeneralScreen extends StatefulWidget {
  @override
  _GeneralScreenState createState() => _GeneralScreenState();
}

class _GeneralScreenState extends State<GeneralScreen> {
  PageController pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: PageView(
          controller: pageController,
          children: [
            GeneralInformation(),
            EmployerDetails(),
            ChangePassword(),
          ],
        )),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: SmoothPageIndicator(
            controller: pageController,
            count: 3,
            effect: ScrollingDotsEffect(
                maxVisibleDots: 7,
                spacing: MediaQuery.of(context).size.width * 0.035,
                radius: 4.0,
                dotWidth: MediaQuery.of(context).size.width * 0.075,
                dotHeight: 5,
                dotColor: ColorConfig.blue11,
                paintStyle: PaintingStyle.fill,
                activeDotColor: ColorConfig.primary),
          ),
        )
      ],
    );
  }
}
