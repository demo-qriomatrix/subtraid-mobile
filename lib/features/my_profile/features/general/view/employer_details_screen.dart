import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/shared/styles.dart';

class EmployerDetails extends StatefulWidget {
  @override
  _EmployerDetailsState createState() => _EmployerDetailsState();
}

class _EmployerDetailsState extends State<EmployerDetails> {
  TextEditingController jobTitleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        decoration: BoxDecoration(
            color: ColorConfig.blue4, borderRadius: BorderRadius.circular(10)),
        child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is AuthentcatedState) {
              return ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        'Employer Details',
                        style: TextStyle(
                            color: ColorConfig.primary,
                            fontSize: 16,
                            fontWeight: FontWeight.w700),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Currently seeking Employment',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          fontSize: 12)),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          value: false,
                          onChanged: (val) {}),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Job Title',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                          fontSize: 12)),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: jobTitleController,
                    decoration: profileInputDecoration,
                    maxLines: null,
                    style: textStyle2,
                  ),
                ],
              );
            }

            return Container();
          },
        ));
  }
}
