import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/shared/styles.dart';

class GeneralInformation extends StatefulWidget {
  @override
  _GeneralInformationState createState() => _GeneralInformationState();
}

class _GeneralInformationState extends State<GeneralInformation> {
  TextEditingController nameController =
      TextEditingController(text: 'Haputhanthrige Vayodya Tamari');
  TextEditingController addressController = TextEditingController(
      text: 'No 23, Pettigalawaththa, Colombo, Sri Lanka');

  TextEditingController aboutController = TextEditingController(
      text:
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but');
  TextEditingController currentStatusController =
      TextEditingController(text: 'False');

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      decoration: BoxDecoration(
          color: ColorConfig.blue4, borderRadius: BorderRadius.circular(10)),
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                'General Information',
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text('Name',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12)),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: nameController,
            decoration: profileInputDecoration,
            maxLines: null,
            style: textStyle2,
          ),
          SizedBox(
            height: 10,
          ),
          Text('Address',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12)),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: addressController,
            decoration: profileInputDecoration,
            maxLines: null,
            style: textStyle2,
          ),
          SizedBox(
            height: 10,
          ),
          Text('About Me',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12)),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: aboutController,
            maxLines: 7,
            style: textStyle2,
            decoration: InputDecoration(
                fillColor: ColorConfig.blue2,
                filled: true,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide.none)),
          ),
          SizedBox(
            height: 10,
          ),
          Text('Currently Seeking employment',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12)),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: currentStatusController,
            decoration: profileInputDecoration,
            maxLines: null,
            style: textStyle2,
          ),
        ],
      ),
    );
  }
}
