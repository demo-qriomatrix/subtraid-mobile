import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/authentication/models/update_password_post_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController currentPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void reset() {
    currentPasswordController.clear();
    newPasswordController.clear();
    confirmPasswordController.clear();

    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        decoration: BoxDecoration(
            color: ColorConfig.blue4, borderRadius: BorderRadius.circular(10)),
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  'Change Password',
                  style: TextStyle(
                      color: ColorConfig.primary,
                      fontSize: 16,
                      fontWeight: FontWeight.w700),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Current Password',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12),
            ),
            SizedBox(
              height: 5,
            ),
            TextFormField(
              validator: (val) {
                if (val.isEmpty) {
                  return 'Cannot be empty';
                }

                return null;
              },
              obscureText: true,
              controller: currentPasswordController,
              decoration: profileInputDecoration,
              style: textStyle2,
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              'New Password',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12),
            ),
            SizedBox(
              height: 5,
            ),
            TextFormField(
              validator: (val) {
                if (val.isEmpty) {
                  return 'Cannot be empty';
                }

                return null;
              },
              obscureText: true,
              controller: newPasswordController,
              decoration: profileInputDecoration,
              style: textStyle2,
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              'Re-type Password',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                  fontSize: 12),
            ),
            SizedBox(
              height: 5,
            ),
            TextFormField(
              obscureText: true,
              validator: (val) {
                if (val.isEmpty) {
                  return 'Cannot be empty';
                }

                return null;
              },
              controller: confirmPasswordController,
              style: textStyle2,
              decoration: profileInputDecoration,
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                SaveButton('Save', () {
                  if (_formKey.currentState.validate()) {
                    UpdatePasswordPostModel data =
                        UpdatePasswordPostModel((model) => model
                          ..newpwd = newPasswordController.text
                          ..pwd = currentPasswordController.text);

                    context
                        .read<AuthenticationBloc>()
                        .add(UpdatePassword(model: data));
                  }
                }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
