import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      child: Center(
        child: Text(
          'Welcome to the Subtraid!',
          style: TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.w800),
        ),
      ),
    );
  }
}
