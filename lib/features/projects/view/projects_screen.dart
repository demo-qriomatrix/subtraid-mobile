import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/collaborators/view/collaboraters_screen.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/bloc/projects_bloc.dart';
import 'package:Subtraid/features/projects/features/project_list/view/project_list_screen.dart';
import 'package:Subtraid/features/projects/features/project_permissions/view/project_permissions_screen.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';

class ProjectsScreen extends StatefulWidget {
  @override
  _ProjectsScreenState createState() => _ProjectsScreenState();
}

class _ProjectsScreenState extends State<ProjectsScreen> {
  TextEditingController projectSearch = TextEditingController();
  TextEditingController roleSearch = TextEditingController();
  TextEditingController collaboratorSearch = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProjectsBloc>(
        create: (context) => ProjectsBloc(),
        child: BlocBuilder<ProjectsBloc, ProjectsState>(
          builder: (context, state) => Container(
              color: ColorConfig.primary,
              child: Column(children: [
                StAppBar(
                  title: 'Projects',
                ),
                Expanded(
                    child: Column(
                  children: [
                    if (state.selectedIndex == 0)
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextField(
                          controller: projectSearch,
                          keyboardType: TextInputType.text,
                          decoration: searchInputDecoration.copyWith(
                              hintText: "Search by projects",
                              suffixIcon: IconButton(
                                  constraints: BoxConstraints(),
                                  icon: Icon(
                                    Icons.close,
                                    size: 20,
                                  ),
                                  onPressed: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    projectSearch.clear();
                                  })),
                        ),
                      ),
                    if (state.selectedIndex == 1)
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextField(
                          controller: roleSearch,
                          keyboardType: TextInputType.text,
                          decoration: searchInputDecoration.copyWith(
                              hintText: "Search by User Role",
                              suffixIcon: IconButton(
                                  constraints: BoxConstraints(),
                                  icon: Icon(
                                    Icons.close,
                                    size: 20,
                                  ),
                                  onPressed: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    roleSearch.clear();
                                  })),
                        ),
                      ),
                    if (state.selectedIndex == 2)
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextField(
                          controller: collaboratorSearch,
                          keyboardType: TextInputType.text,
                          decoration: searchInputDecoration.copyWith(
                              hintText: "Search by collaborator",
                              suffixIcon: IconButton(
                                  constraints: BoxConstraints(),
                                  icon: Icon(
                                    Icons.close,
                                    size: 20,
                                  ),
                                  onPressed: () {
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());
                                    collaboratorSearch.clear();
                                  })),
                        ),
                      ),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                        child: Stack(
                      children: [
                        Container(
                            margin: EdgeInsets.only(top: 12),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(50),
                                    topRight: Radius.circular(50))),
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minHeight: getMinHeight(context),
                              ),
                              child: [
                                ProjectListScreen(
                                  projectSearch: projectSearch,
                                ),
                                ProjectPermissionsScreen(
                                  roleSearch: roleSearch,
                                ),
                                IncomingCollaboratorsScreen(
                                  collaboratorSearch: collaboratorSearch,
                                )
                              ][state.selectedIndex],
                            )),
                        Positioned(
                            right: 50,
                            child: Row(
                              children: <Widget>[
                                RawMaterialButton(
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  constraints: BoxConstraints(),
                                  fillColor: state.selectedIndex == 0
                                      ? ColorConfig.orange
                                      : Colors.white,
                                  onPressed: () {
                                    context
                                        .read<ProjectsBloc>()
                                        .add(ProjectsIndexChanged(index: 0));
                                  },
                                  padding: EdgeInsets.all(5),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.clipboardList),
                                    color: state.selectedIndex == 0
                                        ? Colors.white
                                        : ColorConfig.primary,
                                    size: 18,
                                  ),
                                ),
                                SizedBox(width: 20),
                                RawMaterialButton(
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  constraints: BoxConstraints(),
                                  fillColor: state.selectedIndex == 1
                                      ? ColorConfig.orange
                                      : Colors.white,
                                  onPressed: () {
                                    context
                                        .read<ProjectsBloc>()
                                        .add(ProjectsIndexChanged(index: 1));
                                  },
                                  padding: EdgeInsets.all(5),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.userCog),
                                    color: state.selectedIndex == 1
                                        ? Colors.white
                                        : ColorConfig.primary,
                                    size: 18,
                                  ),
                                ),
                                SizedBox(width: 20),
                                RawMaterialButton(
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  constraints: BoxConstraints(),
                                  fillColor: state.selectedIndex == 2
                                      ? ColorConfig.orange
                                      : Colors.white,
                                  onPressed: () {
                                    context
                                        .read<ProjectsBloc>()
                                        .add(ProjectsIndexChanged(index: 2));
                                  },
                                  padding: EdgeInsets.all(5),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.crew),
                                    color: state.selectedIndex == 2
                                        ? Colors.white
                                        : ColorConfig.primary,
                                    size: 18,
                                  ),
                                ),
                              ],
                            ))
                      ],
                    ))
                  ],
                ))
              ])),
        ));
  }
}
