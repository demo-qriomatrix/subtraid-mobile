import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/enums/collaborator_type_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/bloc/collaborators/collaborators_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/view/collaborators_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class IncomingCollaboratorsScreen extends StatelessWidget {
  final TextEditingController collaboratorSearch;

  IncomingCollaboratorsScreen({@required this.collaboratorSearch});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CollaboratorsBloc>(
      create: (context) => CollaboratorsBloc(
          type: CollaboratorType.Incoming,
          projectBloc: context.read<ProjectBloc>(),
          globalBloc: context.read<GlobalBloc>(),
          authenticationBloc: context.read<AuthenticationBloc>(),
          userRequestsRepository: sl()),
      child: BlocBuilder<CollaboratorsBloc, CollaboratorsState>(
        builder: (context, state) {
          if (state is CollaboratorsLoadSuccess) {
            return Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      Column(
                        children: state.requests
                            .map((e) => CollaboratorCard(
                                  type: state.type,
                                  requestsModel: e,
                                  projectCollaboratorsModel: null,
                                ))
                            .toList(),
                      ),
                      SizedBox(
                        height: 40,
                      )
                    ],
                  ),
                ),
              ],
            );
          }

          return Container();
        },
      ),
    );
  }
}
