import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_success_response_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_response_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/projects_list_response_model.dart';

class ProjectsRepository {
  final HttpClient httpClient;

  ProjectsRepository({@required this.httpClient});

  Future<ProjectsListResponseModel> getProjectsList() async {
    Response response =
        await httpClient.dio.get(EndpointConfig.userProjectsList);

    return ProjectsListResponseModel.fromJson(json.encode(response.data));
  }

  Future<CreateProjectSuccessResponseModel> createProject(
      CreateProjectModel createProjectModel) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.projects, data: createProjectModel.toJson());

    print(createProjectModel.toJson());

    return CreateProjectSuccessResponseModel.fromJson(
        json.encode(response.data));
  }

  Future<ProjectResponseModel> getProject(String projectId) async {
    Response response =
        await httpClient.dio.get(EndpointConfig.projects + '/$projectId');

    return ProjectResponseModel.fromJson(json.encode(response.data));
  }
}
