part of 'project_list_bloc.dart';

abstract class ProjectListState extends Equatable {
  @override
  List<Object> get props => [];
}

class ProjectsInitial extends ProjectListState {}

class ProjectsLoadInProgress extends ProjectListState {}

class ProjectsLoadSuccess extends ProjectListState {
  final List<ProjectListItemModel> projects;
  ProjectsLoadSuccess({@required this.projects});
}

class ProjectsLoadFailure extends ProjectListState {}
