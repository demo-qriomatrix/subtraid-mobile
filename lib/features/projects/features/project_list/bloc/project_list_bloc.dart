import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_model.dart';
import 'package:Subtraid/features/projects/features/project_list/models/projects_list_response_model.dart';
import 'package:Subtraid/features/projects/features/project_list/repository/project_list_repository.dart';

import 'package:meta/meta.dart';

part 'project_list_event.dart';
part 'project_list_state.dart';

class ProjectListBloc extends Bloc<ProjectListEvent, ProjectListState> {
  ProjectListBloc({@required this.projectsRepository})
      : super(ProjectsInitial()) {
    add(ProjectsRequested());
  }

  final ProjectsRepository projectsRepository;

  @override
  Stream<ProjectListState> mapEventToState(
    ProjectListEvent event,
  ) async* {
    if (event is ProjectsRequested) {
      yield* _mapProjectsRequestedToState();
    }
  }

  Stream<ProjectListState> _mapProjectsRequestedToState() async* {
    yield ProjectsLoadInProgress();

    try {
      ProjectsListResponseModel projectListResponse =
          await projectsRepository.getProjectsList();
      yield ProjectsLoadSuccess(
          projects: projectListResponse.projectList.toList());
    } catch (e) {
      print(e);
      yield ProjectsLoadFailure();
    }
  }
}
