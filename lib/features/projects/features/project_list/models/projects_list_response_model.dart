library projects_list_response_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'project_list_item_model.dart';

part 'projects_list_response_model.g.dart';

abstract class ProjectsListResponseModel
    implements
        Built<ProjectsListResponseModel, ProjectsListResponseModelBuilder> {
  @nullable
  BuiltList<ProjectListItemModel> get projectList;

  @nullable
  String get message;

  ProjectsListResponseModel._();

  factory ProjectsListResponseModel(
          [updates(ProjectsListResponseModelBuilder b)]) =
      _$ProjectsListResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectsListResponseModel.serializer, this));
  }

  static ProjectsListResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectsListResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectsListResponseModel> get serializer =>
      _$projectsListResponseModelSerializer;
}
