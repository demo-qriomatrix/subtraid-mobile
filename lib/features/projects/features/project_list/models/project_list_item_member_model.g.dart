// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_list_item_member_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectListItemMemberModel> _$projectListItemMemberModelSerializer =
    new _$ProjectListItemMemberModelSerializer();

class _$ProjectListItemMemberModelSerializer
    implements StructuredSerializer<ProjectListItemMemberModel> {
  @override
  final Iterable<Type> types = const [
    ProjectListItemMemberModel,
    _$ProjectListItemMemberModel
  ];
  @override
  final String wireName = 'ProjectListItemMemberModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectListItemMemberModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProjectListItemMemberModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectListItemMemberModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectListItemMemberModel extends ProjectListItemMemberModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final String img;

  factory _$ProjectListItemMemberModel(
          [void Function(ProjectListItemMemberModelBuilder) updates]) =>
      (new ProjectListItemMemberModelBuilder()..update(updates)).build();

  _$ProjectListItemMemberModel._({this.id, this.name, this.img}) : super._();

  @override
  ProjectListItemMemberModel rebuild(
          void Function(ProjectListItemMemberModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectListItemMemberModelBuilder toBuilder() =>
      new ProjectListItemMemberModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectListItemMemberModel &&
        id == other.id &&
        name == other.name &&
        img == other.img;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), img.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectListItemMemberModel')
          ..add('id', id)
          ..add('name', name)
          ..add('img', img))
        .toString();
  }
}

class ProjectListItemMemberModelBuilder
    implements
        Builder<ProjectListItemMemberModel, ProjectListItemMemberModelBuilder> {
  _$ProjectListItemMemberModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  ProjectListItemMemberModelBuilder();

  ProjectListItemMemberModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _img = _$v.img;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectListItemMemberModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectListItemMemberModel;
  }

  @override
  void update(void Function(ProjectListItemMemberModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectListItemMemberModel build() {
    final _$result =
        _$v ?? new _$ProjectListItemMemberModel._(id: id, name: name, img: img);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
