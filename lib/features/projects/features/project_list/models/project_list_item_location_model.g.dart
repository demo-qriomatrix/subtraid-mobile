// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_list_item_location_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectListItemLocationModel>
    _$projectListItemLocationModelSerializer =
    new _$ProjectListItemLocationModelSerializer();

class _$ProjectListItemLocationModelSerializer
    implements StructuredSerializer<ProjectListItemLocationModel> {
  @override
  final Iterable<Type> types = const [
    ProjectListItemLocationModel,
    _$ProjectListItemLocationModel
  ];
  @override
  final String wireName = 'ProjectListItemLocationModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectListItemLocationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object>[];
  }

  @override
  ProjectListItemLocationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new ProjectListItemLocationModelBuilder().build();
  }
}

class _$ProjectListItemLocationModel extends ProjectListItemLocationModel {
  factory _$ProjectListItemLocationModel(
          [void Function(ProjectListItemLocationModelBuilder) updates]) =>
      (new ProjectListItemLocationModelBuilder()..update(updates)).build();

  _$ProjectListItemLocationModel._() : super._();

  @override
  ProjectListItemLocationModel rebuild(
          void Function(ProjectListItemLocationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectListItemLocationModelBuilder toBuilder() =>
      new ProjectListItemLocationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectListItemLocationModel;
  }

  @override
  int get hashCode {
    return 760610258;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ProjectListItemLocationModel')
        .toString();
  }
}

class ProjectListItemLocationModelBuilder
    implements
        Builder<ProjectListItemLocationModel,
            ProjectListItemLocationModelBuilder> {
  _$ProjectListItemLocationModel _$v;

  ProjectListItemLocationModelBuilder();

  @override
  void replace(ProjectListItemLocationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectListItemLocationModel;
  }

  @override
  void update(void Function(ProjectListItemLocationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectListItemLocationModel build() {
    final _$result = _$v ?? new _$ProjectListItemLocationModel._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
