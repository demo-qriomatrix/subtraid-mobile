library project_list_item_member_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'project_list_item_member_model.g.dart';

abstract class ProjectListItemMemberModel
    implements
        Built<ProjectListItemMemberModel, ProjectListItemMemberModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  String get img;

  ProjectListItemMemberModel._();

  factory ProjectListItemMemberModel(
          [updates(ProjectListItemMemberModelBuilder b)]) =
      _$ProjectListItemMemberModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectListItemMemberModel.serializer, this));
  }

  static ProjectListItemMemberModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectListItemMemberModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectListItemMemberModel> get serializer =>
      _$projectListItemMemberModelSerializer;
}
