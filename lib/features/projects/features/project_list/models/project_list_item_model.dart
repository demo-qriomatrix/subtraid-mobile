library project_list_item_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/add_project/models/location_model.dart';

import 'project_list_item_member_model.dart';

part 'project_list_item_model.g.dart';

abstract class ProjectListItemModel
    implements Built<ProjectListItemModel, ProjectListItemModelBuilder> {
  @nullable
  String get id;

  @nullable
  String get name;

  @nullable
  String get address;

  @nullable
  String get author;

  @nullable
  String get owner;

  @nullable
  int get noOfSubProjects;

  @nullable
  String get status;

  @nullable
  String get startDate;

  @nullable
  String get dateCreated;

  @nullable
  BuiltList<ProjectListItemMemberModel> get members;

  @nullable
  int get noOfCards;

  @nullable
  int get noOfCompletedCards;

  @nullable
  double get progress;

  @nullable
  LocationModel get location;

  ProjectListItemModel._();

  factory ProjectListItemModel([updates(ProjectListItemModelBuilder b)]) =
      _$ProjectListItemModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectListItemModel.serializer, this));
  }

  static ProjectListItemModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectListItemModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectListItemModel> get serializer =>
      _$projectListItemModelSerializer;
}
