library project_list_item_location_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'project_list_item_location_model.g.dart';

abstract class ProjectListItemLocationModel
    implements
        Built<ProjectListItemLocationModel,
            ProjectListItemLocationModelBuilder> {
  // fields go here

  ProjectListItemLocationModel._();

  factory ProjectListItemLocationModel(
          [updates(ProjectListItemLocationModelBuilder b)]) =
      _$ProjectListItemLocationModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        ProjectListItemLocationModel.serializer, this));
  }

  static ProjectListItemLocationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectListItemLocationModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectListItemLocationModel> get serializer =>
      _$projectListItemLocationModelSerializer;
}
