// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_list_item_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectListItemModel> _$projectListItemModelSerializer =
    new _$ProjectListItemModelSerializer();

class _$ProjectListItemModelSerializer
    implements StructuredSerializer<ProjectListItemModel> {
  @override
  final Iterable<Type> types = const [
    ProjectListItemModel,
    _$ProjectListItemModel
  ];
  @override
  final String wireName = 'ProjectListItemModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectListItemModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.address != null) {
      result
        ..add('address')
        ..add(serializers.serialize(object.address,
            specifiedType: const FullType(String)));
    }
    if (object.author != null) {
      result
        ..add('author')
        ..add(serializers.serialize(object.author,
            specifiedType: const FullType(String)));
    }
    if (object.owner != null) {
      result
        ..add('owner')
        ..add(serializers.serialize(object.owner,
            specifiedType: const FullType(String)));
    }
    if (object.noOfSubProjects != null) {
      result
        ..add('noOfSubProjects')
        ..add(serializers.serialize(object.noOfSubProjects,
            specifiedType: const FullType(int)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('startDate')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.members != null) {
      result
        ..add('members')
        ..add(serializers.serialize(object.members,
            specifiedType: const FullType(BuiltList,
                const [const FullType(ProjectListItemMemberModel)])));
    }
    if (object.noOfCards != null) {
      result
        ..add('noOfCards')
        ..add(serializers.serialize(object.noOfCards,
            specifiedType: const FullType(int)));
    }
    if (object.noOfCompletedCards != null) {
      result
        ..add('noOfCompletedCards')
        ..add(serializers.serialize(object.noOfCompletedCards,
            specifiedType: const FullType(int)));
    }
    if (object.progress != null) {
      result
        ..add('progress')
        ..add(serializers.serialize(object.progress,
            specifiedType: const FullType(double)));
    }
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(LocationModel)));
    }
    return result;
  }

  @override
  ProjectListItemModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectListItemModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'owner':
          result.owner = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'noOfSubProjects':
          result.noOfSubProjects = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'startDate':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'members':
          result.members.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(ProjectListItemMemberModel)
              ])) as BuiltList<Object>);
          break;
        case 'noOfCards':
          result.noOfCards = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'noOfCompletedCards':
          result.noOfCompletedCards = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'progress':
          result.progress = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'location':
          result.location.replace(serializers.deserialize(value,
              specifiedType: const FullType(LocationModel)) as LocationModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectListItemModel extends ProjectListItemModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final String address;
  @override
  final String author;
  @override
  final String owner;
  @override
  final int noOfSubProjects;
  @override
  final String status;
  @override
  final String startDate;
  @override
  final String dateCreated;
  @override
  final BuiltList<ProjectListItemMemberModel> members;
  @override
  final int noOfCards;
  @override
  final int noOfCompletedCards;
  @override
  final double progress;
  @override
  final LocationModel location;

  factory _$ProjectListItemModel(
          [void Function(ProjectListItemModelBuilder) updates]) =>
      (new ProjectListItemModelBuilder()..update(updates)).build();

  _$ProjectListItemModel._(
      {this.id,
      this.name,
      this.address,
      this.author,
      this.owner,
      this.noOfSubProjects,
      this.status,
      this.startDate,
      this.dateCreated,
      this.members,
      this.noOfCards,
      this.noOfCompletedCards,
      this.progress,
      this.location})
      : super._();

  @override
  ProjectListItemModel rebuild(
          void Function(ProjectListItemModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectListItemModelBuilder toBuilder() =>
      new ProjectListItemModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectListItemModel &&
        id == other.id &&
        name == other.name &&
        address == other.address &&
        author == other.author &&
        owner == other.owner &&
        noOfSubProjects == other.noOfSubProjects &&
        status == other.status &&
        startDate == other.startDate &&
        dateCreated == other.dateCreated &&
        members == other.members &&
        noOfCards == other.noOfCards &&
        noOfCompletedCards == other.noOfCompletedCards &&
        progress == other.progress &&
        location == other.location;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc($jc(0, id.hashCode),
                                                        name.hashCode),
                                                    address.hashCode),
                                                author.hashCode),
                                            owner.hashCode),
                                        noOfSubProjects.hashCode),
                                    status.hashCode),
                                startDate.hashCode),
                            dateCreated.hashCode),
                        members.hashCode),
                    noOfCards.hashCode),
                noOfCompletedCards.hashCode),
            progress.hashCode),
        location.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectListItemModel')
          ..add('id', id)
          ..add('name', name)
          ..add('address', address)
          ..add('author', author)
          ..add('owner', owner)
          ..add('noOfSubProjects', noOfSubProjects)
          ..add('status', status)
          ..add('startDate', startDate)
          ..add('dateCreated', dateCreated)
          ..add('members', members)
          ..add('noOfCards', noOfCards)
          ..add('noOfCompletedCards', noOfCompletedCards)
          ..add('progress', progress)
          ..add('location', location))
        .toString();
  }
}

class ProjectListItemModelBuilder
    implements Builder<ProjectListItemModel, ProjectListItemModelBuilder> {
  _$ProjectListItemModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _address;
  String get address => _$this._address;
  set address(String address) => _$this._address = address;

  String _author;
  String get author => _$this._author;
  set author(String author) => _$this._author = author;

  String _owner;
  String get owner => _$this._owner;
  set owner(String owner) => _$this._owner = owner;

  int _noOfSubProjects;
  int get noOfSubProjects => _$this._noOfSubProjects;
  set noOfSubProjects(int noOfSubProjects) =>
      _$this._noOfSubProjects = noOfSubProjects;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  ListBuilder<ProjectListItemMemberModel> _members;
  ListBuilder<ProjectListItemMemberModel> get members =>
      _$this._members ??= new ListBuilder<ProjectListItemMemberModel>();
  set members(ListBuilder<ProjectListItemMemberModel> members) =>
      _$this._members = members;

  int _noOfCards;
  int get noOfCards => _$this._noOfCards;
  set noOfCards(int noOfCards) => _$this._noOfCards = noOfCards;

  int _noOfCompletedCards;
  int get noOfCompletedCards => _$this._noOfCompletedCards;
  set noOfCompletedCards(int noOfCompletedCards) =>
      _$this._noOfCompletedCards = noOfCompletedCards;

  double _progress;
  double get progress => _$this._progress;
  set progress(double progress) => _$this._progress = progress;

  LocationModelBuilder _location;
  LocationModelBuilder get location =>
      _$this._location ??= new LocationModelBuilder();
  set location(LocationModelBuilder location) => _$this._location = location;

  ProjectListItemModelBuilder();

  ProjectListItemModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _address = _$v.address;
      _author = _$v.author;
      _owner = _$v.owner;
      _noOfSubProjects = _$v.noOfSubProjects;
      _status = _$v.status;
      _startDate = _$v.startDate;
      _dateCreated = _$v.dateCreated;
      _members = _$v.members?.toBuilder();
      _noOfCards = _$v.noOfCards;
      _noOfCompletedCards = _$v.noOfCompletedCards;
      _progress = _$v.progress;
      _location = _$v.location?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectListItemModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectListItemModel;
  }

  @override
  void update(void Function(ProjectListItemModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectListItemModel build() {
    _$ProjectListItemModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectListItemModel._(
              id: id,
              name: name,
              address: address,
              author: author,
              owner: owner,
              noOfSubProjects: noOfSubProjects,
              status: status,
              startDate: startDate,
              dateCreated: dateCreated,
              members: _members?.build(),
              noOfCards: noOfCards,
              noOfCompletedCards: noOfCompletedCards,
              progress: progress,
              location: _location?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'members';
        _members?.build();

        _$failedField = 'location';
        _location?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectListItemModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
