// GENERATED CODE - DO NOT MODIFY BY HAND

part of projects_list_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectsListResponseModel> _$projectsListResponseModelSerializer =
    new _$ProjectsListResponseModelSerializer();

class _$ProjectsListResponseModelSerializer
    implements StructuredSerializer<ProjectsListResponseModel> {
  @override
  final Iterable<Type> types = const [
    ProjectsListResponseModel,
    _$ProjectsListResponseModel
  ];
  @override
  final String wireName = 'ProjectsListResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectsListResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectList != null) {
      result
        ..add('projectList')
        ..add(serializers.serialize(object.projectList,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectListItemModel)])));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProjectsListResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectsListResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectList':
          result.projectList.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectListItemModel)]))
              as BuiltList<Object>);
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectsListResponseModel extends ProjectsListResponseModel {
  @override
  final BuiltList<ProjectListItemModel> projectList;
  @override
  final String message;

  factory _$ProjectsListResponseModel(
          [void Function(ProjectsListResponseModelBuilder) updates]) =>
      (new ProjectsListResponseModelBuilder()..update(updates)).build();

  _$ProjectsListResponseModel._({this.projectList, this.message}) : super._();

  @override
  ProjectsListResponseModel rebuild(
          void Function(ProjectsListResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectsListResponseModelBuilder toBuilder() =>
      new ProjectsListResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectsListResponseModel &&
        projectList == other.projectList &&
        message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, projectList.hashCode), message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectsListResponseModel')
          ..add('projectList', projectList)
          ..add('message', message))
        .toString();
  }
}

class ProjectsListResponseModelBuilder
    implements
        Builder<ProjectsListResponseModel, ProjectsListResponseModelBuilder> {
  _$ProjectsListResponseModel _$v;

  ListBuilder<ProjectListItemModel> _projectList;
  ListBuilder<ProjectListItemModel> get projectList =>
      _$this._projectList ??= new ListBuilder<ProjectListItemModel>();
  set projectList(ListBuilder<ProjectListItemModel> projectList) =>
      _$this._projectList = projectList;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  ProjectsListResponseModelBuilder();

  ProjectsListResponseModelBuilder get _$this {
    if (_$v != null) {
      _projectList = _$v.projectList?.toBuilder();
      _message = _$v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectsListResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectsListResponseModel;
  }

  @override
  void update(void Function(ProjectsListResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectsListResponseModel build() {
    _$ProjectsListResponseModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectsListResponseModel._(
              projectList: _projectList?.build(), message: message);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'projectList';
        _projectList?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectsListResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
