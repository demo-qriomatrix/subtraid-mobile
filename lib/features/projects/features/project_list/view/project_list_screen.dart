import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/extensions/string_extension.dart';
import 'package:Subtraid/features/projects/features/add_project/view/add_project_screen.dart';
import 'package:Subtraid/features/projects/features/project/view/project_screen.dart';
import 'package:Subtraid/features/projects/features/project_list/bloc/project_list_bloc.dart';
import 'package:Subtraid/features/projects/features/project_list/models/project_list_item_model.dart';

class ProjectListScreen extends StatefulWidget {
  final TextEditingController projectSearch;

  ProjectListScreen({@required this.projectSearch});

  @override
  _ProjectListScreenState createState() => _ProjectListScreenState();
}

class _ProjectListScreenState extends State<ProjectListScreen> {
  String search = '';

  @override
  void initState() {
    super.initState();
    widget.projectSearch.addListener(onChange);
  }

  onChange() {
    setState(() {
      search = widget.projectSearch.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProjectListBloc>(
        create: (context) => ProjectListBloc(
              projectsRepository: sl(),
            ),
        lazy: true,
        child: BlocBuilder<ProjectListBloc, ProjectListState>(
          builder: (context, state) {
            return Column(children: [
              SizedBox(
                height: 20,
              ),
              Expanded(
                  child: RefreshIndicator(
                      onRefresh: () {
                        context
                            .read<ProjectListBloc>()
                            .add(ProjectsRequested());
                        return Future.delayed(Duration.zero);
                      },
                      child: ListView(
                        padding: EdgeInsets.zero,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                            child: Row(
                              children: [
                                RawMaterialButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5)),
                                    fillColor: ColorConfig.primary,
                                    padding: EdgeInsets.all(5),
                                    constraints: BoxConstraints(),
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (context) =>
                                            AddProjectScreen(),
                                      ))
                                          .then((_) {
                                        context
                                            .read<ProjectListBloc>()
                                            .add(ProjectsRequested());
                                      });
                                    }),
                                SizedBox(
                                  width: 10,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                          builder: (context) =>
                                              AddProjectScreen(),
                                        ))
                                        .then((value) {});
                                  },
                                  child: Text(
                                    'Add New Project',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),

                          if (state is ProjectsLoadInProgress)
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.5,
                                child:
                                    Center(child: CircularProgressIndicator())),
                          if (state is ProjectsLoadSuccess)
                            if (state is ProjectsLoadSuccess &&
                                state.projects.isEmpty)
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.5,
                                  child: Center(
                                    child: Text('No Projects'),
                                  )),
                          if (state is ProjectsLoadSuccess &&
                              state.projects.isNotEmpty)
                            Column(
                              children: [
                                Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 30),
                                    child: Column(children: <Widget>[
                                      ...state.projects
                                          .where((element) => search != ''
                                              ? element.name
                                                  .toLowerCase()
                                                  .contains(
                                                      search.toLowerCase())
                                              : true)
                                          .map((e) => ProjectItemWidget(
                                                project: e,
                                              ))
                                          .toList()
                                    ])),
                              ],
                            ),
                          // if (state is ProjectsLoadInProgress)
                          //   SizedBox(
                          //       height:
                          //           MediaQuery.of(context).size.height * 0.5,
                          //       child: Center(
                          //         child: CircularProgressIndicator(),
                          //       )),
                          if (state is ProjectsLoadFailure)
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.5,
                                child: Center(
                                  child: Text('Something went wrong'),
                                )),
                        ],
                      )))
            ]);
          },
        ));
  }
}

class ProjectItemWidget extends StatelessWidget {
  final ProjectListItemModel project;

  ProjectItemWidget({@required this.project});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 4,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        child: InkWell(
          borderRadius: BorderRadius.circular(20),
          onTap: () {
            // Provider.of<AppStateProvider>(context, listen: false).projectId =
            //     project.id;

            // Provider.of<AppStateProvider>(context, listen: false).project =
            //     null;

            Navigator.of(context)
                .push(MaterialPageRoute(
              builder: (context) => ProjectScreen(
                projectId: project.id,
              ),
            ))
                .then((value) {
              context.read<ProjectListBloc>().add(ProjectsRequested());
            });
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      project.status.capitalize,
                      style: TextStyle(
                          color: ColorConfig.activeGrey, fontSize: 12),
                    )
                  ],
                ),
                Text(
                  project.name,
                  style: TextStyle(
                      color: ColorConfig.primary,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
                Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    project.address == '' || project.address == null
                        ? Container()
                        : Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                color: ColorConfig.primary,
                                size: 18,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Flexible(
                                child: Text(
                                  project.address ?? '',
                                  style: TextStyle(
                                    color: ColorConfig.grey1,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              )
                            ],
                          ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                project.startDate == '' || project.startDate == null
                    ? Container()
                    : Row(
                        children: <Widget>[
                          ImageIcon(
                            AssetImage(LocalImages.calenderAlt),
                            color: ColorConfig.primary,
                            size: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            DateFormat('dd MMM yyyy')
                                .format(DateTime.parse(project.startDate)),
                            style: TextStyle(
                                color: ColorConfig.grey1,
                                fontWeight: FontWeight.w500,
                                fontSize: 14),
                          )
                        ],
                      ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            project.noOfSubProjects.toString(),
                            style: TextStyle(
                                color: ColorConfig.orange,
                                fontWeight: FontWeight.w700,
                                fontSize: 22),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Sub Projects',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            project.noOfCards.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 22),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Cards',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            project.noOfCompletedCards.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 22),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Completed',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                project.members.isEmpty
                    ? Text(
                        'No Employees',
                        style: TextStyle(fontWeight: FontWeight.w600),
                      )
                    : Row(
                        children: <Widget>[
                          ...project.members.map(
                            (member) => Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: CircleAvatar(
                                backgroundImage:
                                    AssetImage(LocalImages.defaultAvatar),
                                radius: 20,
                              ),
                            ),
                          )

                          // Padding(
                          //     padding: const EdgeInsets.only(left: 8),
                          //     child: Text(
                          //       '20+',
                          //       style: TextStyle(
                          //           fontWeight: FontWeight.w700, fontSize: 16),
                          //     )),
                        ],
                      ),
                SizedBox(
                  height: 5,
                ),
                ProgressIndicatorWidget(
                  progress: project.progress.toDouble(),
                  total: 100 - project.progress.toDouble(),
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProgressIndicatorWidget extends StatelessWidget {
  final double progress;
  final double total;

  ProgressIndicatorWidget({
    @required this.progress,
    @required this.total,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                '${progress.toInt()}%',
                style: TextStyle(color: ColorConfig.fillColor, fontSize: 10),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorConfig.fillColorShade),
                height: 10,
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    flex: progress.toInt(),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: ColorConfig.fillColor),
                      height: 10,
                    ),
                  ),
                  Flexible(
                    flex: total.toInt(),
                    child: Container(
                      height: 10,
                    ),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
