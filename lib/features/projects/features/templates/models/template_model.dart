library template_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/templates/models/template_request_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'template_model.g.dart';

abstract class TemplateModel
    implements Built<TemplateModel, TemplateModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  BuiltList<TemplateRequestModel> get requests;

  TemplateModel._();

  factory TemplateModel([updates(TemplateModelBuilder b)]) = _$TemplateModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(TemplateModel.serializer, this));
  }

  static TemplateModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TemplateModel.serializer, json.decode(jsonString));
  }

  static Serializer<TemplateModel> get serializer => _$templateModelSerializer;
}
