// GENERATED CODE - DO NOT MODIFY BY HAND

part of template_request_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TemplateRequestModel> _$templateRequestModelSerializer =
    new _$TemplateRequestModelSerializer();

class _$TemplateRequestModelSerializer
    implements StructuredSerializer<TemplateRequestModel> {
  @override
  final Iterable<Type> types = const [
    TemplateRequestModel,
    _$TemplateRequestModel
  ];
  @override
  final String wireName = 'TemplateRequestModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, TemplateRequestModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.url != null) {
      result
        ..add('url')
        ..add(serializers.serialize(object.url,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  TemplateRequestModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TemplateRequestModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TemplateRequestModel extends TemplateRequestModel {
  @override
  final String description;
  @override
  final String type;
  @override
  final String url;

  factory _$TemplateRequestModel(
          [void Function(TemplateRequestModelBuilder) updates]) =>
      (new TemplateRequestModelBuilder()..update(updates)).build();

  _$TemplateRequestModel._({this.description, this.type, this.url}) : super._();

  @override
  TemplateRequestModel rebuild(
          void Function(TemplateRequestModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TemplateRequestModelBuilder toBuilder() =>
      new TemplateRequestModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TemplateRequestModel &&
        description == other.description &&
        type == other.type &&
        url == other.url;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, description.hashCode), type.hashCode), url.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TemplateRequestModel')
          ..add('description', description)
          ..add('type', type)
          ..add('url', url))
        .toString();
  }
}

class TemplateRequestModelBuilder
    implements Builder<TemplateRequestModel, TemplateRequestModelBuilder> {
  _$TemplateRequestModel _$v;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _url;
  String get url => _$this._url;
  set url(String url) => _$this._url = url;

  TemplateRequestModelBuilder();

  TemplateRequestModelBuilder get _$this {
    if (_$v != null) {
      _description = _$v.description;
      _type = _$v.type;
      _url = _$v.url;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TemplateRequestModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TemplateRequestModel;
  }

  @override
  void update(void Function(TemplateRequestModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TemplateRequestModel build() {
    final _$result = _$v ??
        new _$TemplateRequestModel._(
            description: description, type: type, url: url);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
