// GENERATED CODE - DO NOT MODIFY BY HAND

part of templates_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TemplatesResponseModel> _$templatesResponseModelSerializer =
    new _$TemplatesResponseModelSerializer();

class _$TemplatesResponseModelSerializer
    implements StructuredSerializer<TemplatesResponseModel> {
  @override
  final Iterable<Type> types = const [
    TemplatesResponseModel,
    _$TemplatesResponseModel
  ];
  @override
  final String wireName = 'TemplatesResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, TemplatesResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.result != null) {
      result
        ..add('result')
        ..add(serializers.serialize(object.result,
            specifiedType: const FullType(
                BuiltList, const [const FullType(TemplateModel)])));
    }
    return result;
  }

  @override
  TemplatesResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TemplatesResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'result':
          result.result.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(TemplateModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$TemplatesResponseModel extends TemplatesResponseModel {
  @override
  final BuiltList<TemplateModel> result;

  factory _$TemplatesResponseModel(
          [void Function(TemplatesResponseModelBuilder) updates]) =>
      (new TemplatesResponseModelBuilder()..update(updates)).build();

  _$TemplatesResponseModel._({this.result}) : super._();

  @override
  TemplatesResponseModel rebuild(
          void Function(TemplatesResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TemplatesResponseModelBuilder toBuilder() =>
      new TemplatesResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TemplatesResponseModel && result == other.result;
  }

  @override
  int get hashCode {
    return $jf($jc(0, result.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TemplatesResponseModel')
          ..add('result', result))
        .toString();
  }
}

class TemplatesResponseModelBuilder
    implements Builder<TemplatesResponseModel, TemplatesResponseModelBuilder> {
  _$TemplatesResponseModel _$v;

  ListBuilder<TemplateModel> _result;
  ListBuilder<TemplateModel> get result =>
      _$this._result ??= new ListBuilder<TemplateModel>();
  set result(ListBuilder<TemplateModel> result) => _$this._result = result;

  TemplatesResponseModelBuilder();

  TemplatesResponseModelBuilder get _$this {
    if (_$v != null) {
      _result = _$v.result?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TemplatesResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TemplatesResponseModel;
  }

  @override
  void update(void Function(TemplatesResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TemplatesResponseModel build() {
    _$TemplatesResponseModel _$result;
    try {
      _$result =
          _$v ?? new _$TemplatesResponseModel._(result: _result?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'result';
        _result?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TemplatesResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
