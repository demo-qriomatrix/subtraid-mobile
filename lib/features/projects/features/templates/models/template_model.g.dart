// GENERATED CODE - DO NOT MODIFY BY HAND

part of template_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TemplateModel> _$templateModelSerializer =
    new _$TemplateModelSerializer();

class _$TemplateModelSerializer implements StructuredSerializer<TemplateModel> {
  @override
  final Iterable<Type> types = const [TemplateModel, _$TemplateModel];
  @override
  final String wireName = 'TemplateModel';

  @override
  Iterable<Object> serialize(Serializers serializers, TemplateModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.requests != null) {
      result
        ..add('requests')
        ..add(serializers.serialize(object.requests,
            specifiedType: const FullType(
                BuiltList, const [const FullType(TemplateRequestModel)])));
    }
    return result;
  }

  @override
  TemplateModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TemplateModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'requests':
          result.requests.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(TemplateRequestModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$TemplateModel extends TemplateModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final BuiltList<TemplateRequestModel> requests;

  factory _$TemplateModel([void Function(TemplateModelBuilder) updates]) =>
      (new TemplateModelBuilder()..update(updates)).build();

  _$TemplateModel._({this.id, this.name, this.requests}) : super._();

  @override
  TemplateModel rebuild(void Function(TemplateModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TemplateModelBuilder toBuilder() => new TemplateModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TemplateModel &&
        id == other.id &&
        name == other.name &&
        requests == other.requests;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), requests.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TemplateModel')
          ..add('id', id)
          ..add('name', name)
          ..add('requests', requests))
        .toString();
  }
}

class TemplateModelBuilder
    implements Builder<TemplateModel, TemplateModelBuilder> {
  _$TemplateModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  ListBuilder<TemplateRequestModel> _requests;
  ListBuilder<TemplateRequestModel> get requests =>
      _$this._requests ??= new ListBuilder<TemplateRequestModel>();
  set requests(ListBuilder<TemplateRequestModel> requests) =>
      _$this._requests = requests;

  TemplateModelBuilder();

  TemplateModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _requests = _$v.requests?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TemplateModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TemplateModel;
  }

  @override
  void update(void Function(TemplateModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TemplateModel build() {
    _$TemplateModel _$result;
    try {
      _$result = _$v ??
          new _$TemplateModel._(
              id: id, name: name, requests: _requests?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'requests';
        _requests?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TemplateModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
