library template_request_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'template_request_model.g.dart';

abstract class TemplateRequestModel
    implements Built<TemplateRequestModel, TemplateRequestModelBuilder> {
  @nullable
  String get description;

  @nullable
  String get type;

  @nullable
  String get url;

  TemplateRequestModel._();

  factory TemplateRequestModel([updates(TemplateRequestModelBuilder b)]) =
      _$TemplateRequestModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(TemplateRequestModel.serializer, this));
  }

  static TemplateRequestModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TemplateRequestModel.serializer, json.decode(jsonString));
  }

  static Serializer<TemplateRequestModel> get serializer =>
      _$templateRequestModelSerializer;
}
