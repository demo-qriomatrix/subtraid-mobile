library templates_response_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/templates/models/template_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'templates_response_model.g.dart';

abstract class TemplatesResponseModel
    implements Built<TemplatesResponseModel, TemplatesResponseModelBuilder> {
  @nullable
  BuiltList<TemplateModel> get result;

  TemplatesResponseModel._();

  factory TemplatesResponseModel([updates(TemplatesResponseModelBuilder b)]) =
      _$TemplatesResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(TemplatesResponseModel.serializer, this));
  }

  static TemplatesResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TemplatesResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<TemplatesResponseModel> get serializer =>
      _$templatesResponseModelSerializer;
}
