import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/templates/models/templates_response_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class TemplatesRepository {
  final HttpClient httpClient;

  TemplatesRepository({@required this.httpClient});

  Future<TemplatesResponseModel> getTemplates() async {
    Response response = await httpClient.dio.get(EndpointConfig.templates);

    return TemplatesResponseModel.fromJson(json.encode(response.data));
  }
}
