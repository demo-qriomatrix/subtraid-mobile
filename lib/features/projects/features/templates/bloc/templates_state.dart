part of 'templates_bloc.dart';

abstract class TemplatesState extends Equatable {
  const TemplatesState();

  @override
  List<Object> get props => [];
}

class TemplatesInitial extends TemplatesState {}

class TemplatesLoadInProgress extends TemplatesState {}

class TemplatesLoadSuccess extends TemplatesState {
  final List<TemplateModel> templates;

  TemplatesLoadSuccess({@required this.templates});
}

class TemplatesLoadFailure extends TemplatesState {}
