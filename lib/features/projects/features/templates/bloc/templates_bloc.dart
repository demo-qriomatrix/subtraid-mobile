import 'dart:async';

import 'package:Subtraid/features/projects/features/templates/models/template_model.dart';
import 'package:Subtraid/features/projects/features/templates/models/templates_response_model.dart';
import 'package:Subtraid/features/projects/features/templates/repository/templates_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'templates_event.dart';
part 'templates_state.dart';

class TemplatesBloc extends Bloc<TemplatesEvent, TemplatesState> {
  TemplatesBloc({@required this.templatesRepository})
      : super(TemplatesInitial()) {
    add(TemplatedRequested());
  }

  final TemplatesRepository templatesRepository;

  @override
  Stream<TemplatesState> mapEventToState(
    TemplatesEvent event,
  ) async* {
    if (event is TemplatedRequested) {
      yield* _mapTemplatedRequestedToState();
    }
  }

  Stream<TemplatesState> _mapTemplatedRequestedToState() async* {
    yield TemplatesLoadInProgress();

    try {
      TemplatesResponseModel templatesResponseModel =
          await templatesRepository.getTemplates();

      yield TemplatesLoadSuccess(
          templates: templatesResponseModel.result.toList());
    } catch (e) {
      yield TemplatesLoadFailure();
    }
  }
}
