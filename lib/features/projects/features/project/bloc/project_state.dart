part of 'project_bloc.dart';

abstract class ProjectState extends Equatable {
  final int selectedIndex;
  final String projectId;
  ProjectState({@required this.selectedIndex, @required this.projectId});

  @override
  List<Object> get props => [];
}

class ProjectInitial extends ProjectState {
  ProjectInitial({@required int selectedIndex, @required String projectId})
      : super(selectedIndex: selectedIndex, projectId: projectId);
}

class ProjectLoadInProgress extends ProjectState {
  ProjectLoadInProgress(
      {@required int selectedIndex, @required String projectId})
      : super(selectedIndex: selectedIndex, projectId: projectId);
}

class ProjectLoadSuccess extends ProjectState {
  final ProjectModel projectModel;
  final Map<String, ProjectCardModel> cards;
  final Map<String, ProjectLabelModel> labels;
  final Map<String, ProjectSubProjectModel> subProjects;
  final bool showAddList;

  final PageController pageController;

  ProjectLoadSuccess(
      {@required int selectedIndex,
      @required this.projectModel,
      @required this.labels,
      @required this.showAddList,
      @required this.subProjects,
      @required this.cards,
      @required this.pageController,
      @required String projectId})
      : super(selectedIndex: selectedIndex, projectId: projectId);

  @override
  List<Object> get props => [projectModel, showAddList, selectedIndex];
}

class ProjectLoadFailure extends ProjectState {
  ProjectLoadFailure({@required int selectedIndex, @required String projectId})
      : super(selectedIndex: selectedIndex, projectId: projectId);
}
