import 'dart:async';
import 'dart:convert';
import 'package:Subtraid/core/enums/check_list_action_enum.dart';
import 'package:Subtraid/features/calender/repository/calender_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_label_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_to_calender_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_label_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/repository/label_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/models/remove_member_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/model/create_project_timesheet_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_comment_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_employee_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_update_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/shift_card_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_status_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/repository/card_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/add_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/remove_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/repository/list_repository.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list_item/bloc/list_item_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/sub_project/models/add_sub_project_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/sub_project/repository/sub_project_repository.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_label_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_list_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_response_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_sub_project_model.dart';
import 'package:Subtraid/features/projects/features/project/repository/project_repository.dart';
import 'package:Subtraid/features/projects/features/project_list/repository/project_list_repository.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/models/create_timesheet_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/repository/projects_timesheets_repository.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_ops_model.dart';
import 'package:Subtraid/features/more/timesheets/features/proejcts/timesheet/models/timesheet_update_model.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'project_event.dart';
part 'project_state.dart';

class ProjectBloc extends Bloc<ProjectEvent, ProjectState> {
  ProjectBloc({
    @required this.projectsRepository,
    @required this.projectRepository,
    @required this.cardRepository,
    @required this.globalBloc,
    @required this.authenticationBloc,
    @required this.labelRepository,
    @required this.calenderRepository,
    @required this.subProjectRepository,
    @required this.listRepository,
    @required this.projectsTimesheetsRepository,
  }) : super(ProjectInitial(selectedIndex: 0, projectId: null)) {
    pageController = PageController(
      initialPage: 0,
    );
  }

  final AuthenticationBloc authenticationBloc;

  final ProjectsRepository projectsRepository;

  final CardRepository cardRepository;

  final LabelRepository labelRepository;

  final CalenderRepository calenderRepository;

  final GlobalBloc globalBloc;

  final ListRepository listRepository;

  final SubProjectRepository subProjectRepository;

  final ProjectRepository projectRepository;

  final ProjectsTimesheetsRepository projectsTimesheetsRepository;

  ListItemBloc savedListItemBloc;

  PageController pageController;

  CardBloc cardBloc;

  void setCardBloc(CardBloc _cardBloc) {
    cardBloc = _cardBloc;
  }

  void setListItemBloc(ListItemBloc _listItemBloc) {
    savedListItemBloc = _listItemBloc;
  }

  @override
  Stream<ProjectState> mapEventToState(
    ProjectEvent event,
  ) async* {
    if (event is ProjectRequested) {
      yield* _mapProjectRequestedToState(event);
    }

    if (event is ProjectChangeIndex) {
      if (state is ProjectLoadSuccess) {
        ProjectLoadSuccess projectLoadSuccess = state;
        yield ProjectLoadSuccess(
            cards: projectLoadSuccess.cards,
            labels: projectLoadSuccess.labels,
            pageController: projectLoadSuccess.pageController,
            projectId: projectLoadSuccess.projectId,
            projectModel: projectLoadSuccess.projectModel,
            selectedIndex: event.index,
            showAddList: projectLoadSuccess.showAddList,
            subProjects: projectLoadSuccess.subProjects);
      }
      if (state is ProjectLoadFailure) {
        ProjectLoadFailure projectLoadSuccess = state;
        yield ProjectLoadFailure(
            selectedIndex: event.index,
            projectId: projectLoadSuccess.projectId);
      }
    }

    if (event is ProjectAddCard) {
      yield* _mapProjectAddCardToState(event.card, event.listItemBloc);
    }
    if (event is ProjectAddSubProject) {
      yield* _mapProjectAddSubProjectToState(
          event.subProject, event.listItemBloc);
    }

    if (event is ProjectToggleShowAddList) {
      yield* _mapProjectToggleShowAddList(event.showAdd);
    }

    if (event is ProjectAddList) {
      yield* _mapProjectAddListToState(event.list);
    }
    if (event is ProjectRemoveList) {
      yield* _mapProjectRemoveListToState(event.list);
    }

    if (event is ProjectUpdateCard) {
      yield* _mapProjectUpdateCardToState(event);
    }

    if (event is ProjectShiftCard) {
      yield* _mapProjectShiftCardToState(event.card);
    }
    if (event is ProjectUpdateCardStatus) {
      yield* _mapProjectUpdateCardStatusToState(event.card);
    }
    if (event is ProjectCardAddComment) {
      yield* _mapProjectCardAddCommentToState(event.comment);
    }

    if (event is ProjectDeleteProject) {
      yield* _mapProjectDeleteProjectToState(event.projectId);
    }

    if (event is ProjectCardAddEmployee) {
      yield* _mapProjectCardAddEmployeeToState(event.employeeId);
    }

    if (event is ProjectCardRemoveEmployee) {
      yield* _mapProjectCardRemoveEmployeeToState(event.employeeId);
    }

    if (event is ProjectCardStartTask) {
      yield* _mapProjectCardStartTaskToState();
    }
    if (event is ProjectStartTask) {
      yield* _mapProjectStartTaskToState(event.userId);
    }
    if (event is ProjectCardStopTask) {
      yield* _mapProjectCardStopTaskToState();
    }
    if (event is ProjectSopTask) {
      yield* _mapProjectSopTaskToState(event.timesheetId);
    }
    if (event is ProjectCardAddChecklist) {
      yield* _mapProjectCardAddChecklistToState(event.subTask);
    }
    if (event is ProjectCardAddChecklistItem) {
      yield* _mapProjectCardAddChecklistItemToState(event);
    }

    if (event is ProjectCardAddLabel) {
      yield* _mapProjectCardAddLabelToState(event.label);
    }
    if (event is ProjectCardUpdateLabel) {
      yield* _mapProjectCardUpdateLabelToState(event.label);
    }
    if (event is ProjectCardAddToEvent) {
      yield* _mapProjectCardAddToEventToState();
    }

    if (event is ProjectRemoveMember) {
      yield* _mapProjectRemoveMemberToState(event.userId);
    }
    if (event is ProjectRemoveMembers) {
      yield* _mapProjectRemoveMembersToState(event.userIds);
    }
    if (event is ProjectUpdate) {
      yield* _mapProjectUpdateToState(event.project);
    }
  }

  Stream<ProjectState> _mapProjectUpdateToState(
      ProjectResponseModel project) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      await projectRepository.updateProject(project);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Project updated successfully', event: null));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectRemoveMembersToState(
      List<String> userIds) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      for (String userId in userIds) {
        RemoveMemberModel member = RemoveMemberModel((data) => data
          ..userId = userId
          ..projectId = state.projectId);

        await projectRepository.removeMember(member);
      }

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Members removed successfully',
          event: SucceessEvents.AddList));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectRemoveMemberToState(String userId) async* {
    try {
      RemoveMemberModel member = RemoveMemberModel((data) => data
        ..userId = userId
        ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await projectRepository.removeMember(member);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Member removed successfully',
          event: SucceessEvents.AddList));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardAddChecklistItemToState(
      ProjectCardAddChecklistItem event) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      if (event.action == ActionEnum.add) {
        CardAddChecklistItemModel cardAddChecklistItemModel =
            CardAddChecklistItemModel((data) => data
              ..cardId = cardBloc.state.cardModel.id
              ..checkListId = event.checkListId
              ..newCheckItem = event.checkItem.toBuilder()
              ..projectId = state.projectId);

        await cardRepository.cardAddChecklistItem(cardAddChecklistItemModel);

        add(ProjectRequested(
            withoutLoader: true,
            listItemBloc: null,
            projectId: state.projectId));

        globalBloc.add(HideLoadingWidget());

        globalBloc.add(ShowSuccessSnackBar(
            message: 'Checkitem added successfully',
            event: SucceessEvents.CheckItem));
      } else if (event.action == ActionEnum.deleteAll) {
        CardAddChecklistItemModel cardAddChecklistItemModel =
            CardAddChecklistItemModel((data) => data
              ..cardId = cardBloc.state.cardModel.id
              ..checkListId = event.checkListId
              ..projectId = state.projectId);

        await cardRepository.cardDeleteChecklist(cardAddChecklistItemModel);

        add(ProjectRequested(
            withoutLoader: true,
            listItemBloc: null,
            projectId: state.projectId));

        globalBloc.add(HideLoadingWidget());

        globalBloc.add(ShowSuccessSnackBar(
            message: 'Checkitem added successfully',
            event: SucceessEvents.CheckItem));
      } else {
        List<CardChecklistModel> checklists =
            cardBloc.state.cardModel.checklists.toList();

        int checkListIndex = checklists.indexWhere(
          (checkList) => checkList.id == event.checkListId,
        );

        if (checkListIndex != -1) {
          CardChecklistModel updatedChecklist = checklists[checkListIndex];

          List<CardChecklistItemModel> checkItems =
              updatedChecklist.checkItems.toList();

          if (event.action == ActionEnum.update) {
            int index = checkItems
                .indexWhere((element) => element.id == event.checkItem.id);

            if (index != -1) {
              checkItems[index] = event.checkItem;

              updatedChecklist = updatedChecklist
                  .rebuild((e) => e.checkItems = ListBuilder(checkItems));

              updatedChecklist = updatedChecklist.rebuild((e) =>
                  e.checkItemsChecked = updatedChecklist.checkItems
                      .fold(0, (t, e) => t + (e.checked ? 1 : 0)));

              checklists[checkListIndex] = updatedChecklist;
            }
          }

          if (event.action == ActionEnum.selectAll ||
              event.action == ActionEnum.deselectAll) {
            checkItems.asMap().forEach((index, value) {
              if (event.action == ActionEnum.selectAll)
                checkItems[index] = value.rebuild((e) => e.checked = true);
              if (event.action == ActionEnum.deselectAll)
                checkItems[index] = value.rebuild((e) => e.checked = false);
            });

            updatedChecklist = updatedChecklist
                .rebuild((e) => e.checkItems = ListBuilder(checkItems));

            updatedChecklist = updatedChecklist.rebuild((e) =>
                e.checkItemsChecked = updatedChecklist.checkItems
                    .fold(0, (t, e) => t + (e.checked ? 1 : 0)));

            checklists[checkListIndex] = updatedChecklist;
          }

          if (event.action == ActionEnum.delete) {
            int index = checkItems
                .indexWhere((element) => element.id == event.checkItem.id);

            if (index != -1) {
              checkItems.removeAt(index);

              updatedChecklist = updatedChecklist
                  .rebuild((e) => e.checkItems = ListBuilder(checkItems));

              updatedChecklist = updatedChecklist.rebuild((e) =>
                  e.checkItemsChecked = updatedChecklist.checkItems
                      .fold(0, (t, e) => t + (e.checked ? 1 : 0)));

              checklists[checkListIndex] = updatedChecklist;
            }
          }

          CardUpdateChecklistItemModel cardUpdateChecklistItemModel =
              CardUpdateChecklistItemModel((data) => data
                ..cardId = cardBloc.state.cardModel.id
                ..checkItemsCount = checklists.fold(
                    0, (t, e) => t + e.checkItems.fold(0, (t, e) => t + 1))
                ..checkItemsCheckedCount = checklists.fold(
                    0,
                    (t, e) =>
                        t +
                        e.checkItems.fold(0, (t, e) => t + (e.checked ? 1 : 0)))
                ..updatedList = updatedChecklist.toBuilder()
                ..projectId = state.projectId);

          await cardRepository
              .cardUpdateChecklistItem(cardUpdateChecklistItemModel);

          add(ProjectRequested(
              withoutLoader: true,
              listItemBloc: null,
              projectId: state.projectId));

          globalBloc.add(HideLoadingWidget());

          if (event.action == ActionEnum.delete) {
            globalBloc.add(ShowSuccessSnackBar(
                message: 'Checkitem deleted successfully',
                event: SucceessEvents.AddList));
          } else {
            globalBloc.add(ShowSuccessSnackBar(
                message: 'Checkitems updated successfully',
                event: SucceessEvents.AddList));
          }
        } else {
          globalBloc.add(HideLoadingWidget());
          globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
        }
      }
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardAddChecklistToState(
      String subTask) async* {
    try {
      CardChecklistModel cardChecklistModel =
          CardChecklistModel((data) => data..name = subTask);

      CardAddChecklistModel cardAddChecklistModel =
          CardAddChecklistModel((data) => data
            ..cardId = cardBloc.state.cardModel.id
            ..newChecklist = cardChecklistModel.toBuilder()
            ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.cardAddChecklist(cardAddChecklistModel);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Subtask added successfully',
          event: SucceessEvents.AddList));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardAddLabelToState(
      ProjectLabelModel label) async* {
    try {
      AddCardLabelModel addCardLabelModel = AddCardLabelModel((data) => data
        ..newLabel = label.toBuilder()
        ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await labelRepository.addLabelToCard(addCardLabelModel);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Label added successfully', event: SucceessEvents.Label));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardAddToEventToState() async* {
    try {
      AddCardToCalenderModel addCardLabelModel =
          AddCardToCalenderModel((data) => data
            ..card = cardBloc.state.cardModel.toBuilder()
            ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await calenderRepository.addCardToCalender(addCardLabelModel);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Label added successfully', event: SucceessEvents.Label));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        print(dioError.response.data);
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardUpdateLabelToState(
      ProjectLabelModel label) async* {
    try {
      UpdateCardLabelModel addCardLabelModel =
          UpdateCardLabelModel((data) => data
            ..label = label.toBuilder()
            ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await labelRepository.updateLabelToCard(addCardLabelModel);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Label added successfully', event: SucceessEvents.Label));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardStopTaskToState() async* {
    try {
      String datetime = DateTime.now().toUtc().toIso8601String();
      String formattedDateTime =
          datetime.replaceRange(datetime.length - 3, datetime.length, 'Z');

      TimesheetUpdateModel timesheetUpdateModel =
          TimesheetUpdateModel((data) => data..end = formattedDateTime);

      TimesheetOpsModel timesheetOpsModel = TimesheetOpsModel(
          (data) => data..ops = timesheetUpdateModel.toBuilder());

      globalBloc.add(ShowLoadingWidget());

      await projectsTimesheetsRepository.updateTimesheet(
          cardBloc.state.startedTask.id, timesheetOpsModel);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Task stopped successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectSopTaskToState(String timesheetId) async* {
    try {
      String datetime = DateTime.now().toUtc().toIso8601String();
      String formattedDateTime =
          datetime.replaceRange(datetime.length - 3, datetime.length, 'Z');

      TimesheetUpdateModel timesheetUpdateModel =
          TimesheetUpdateModel((data) => data..end = formattedDateTime);

      TimesheetOpsModel timesheetOpsModel = TimesheetOpsModel(
          (data) => data..ops = timesheetUpdateModel.toBuilder());

      globalBloc.add(ShowLoadingWidget());

      await projectsTimesheetsRepository.updateTimesheet(
          timesheetId, timesheetOpsModel);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Task stopped successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardStartTaskToState() async* {
    try {
      String datetime = DateTime.now().toUtc().toIso8601String();
      String formattedDateTime =
          datetime.replaceRange(datetime.length - 3, datetime.length, 'Z');
      CreateTimesheetModel createTimesheetModel =
          CreateTimesheetModel((data) => data
            ..cardId = cardBloc.state.cardModel.id
            ..cardName = cardBloc.state.cardModel.name
            ..projectId = state.projectId
            ..start = formattedDateTime);

      globalBloc.add(ShowLoadingWidget());

      await projectsTimesheetsRepository
          .createTimesheetCard(createTimesheetModel);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Task started successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectStartTaskToState(String userId) async* {
    try {
      String datetime = DateTime.now().toUtc().toIso8601String();
      String formattedDateTime =
          datetime.replaceRange(datetime.length - 3, datetime.length, 'Z');
      CreateProjectTimesheetModel createProjectTimesheetModel =
          CreateProjectTimesheetModel((data) => data
            ..userId = userId
            ..projectId = state.projectId
            ..start = formattedDateTime);

      globalBloc.add(ShowLoadingWidget());

      await projectsTimesheetsRepository
          .createTimesheetProject(createProjectTimesheetModel);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Task started successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardAddEmployeeToState(
      String employeeId) async* {
    try {
      CardEmployeeModel cardEmployeeModel = CardEmployeeModel((data) => data
        ..cardId = cardBloc.state.cardModel.id
        ..memberId = employeeId
        ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.cardAddEmployee(cardEmployeeModel);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Employee added successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardRemoveEmployeeToState(
      String employeeId) async* {
    try {
      CardEmployeeModel cardEmployeeModel = CardEmployeeModel((data) => data
        ..cardId = cardBloc.state.cardModel.id
        ..memberId = employeeId
        ..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.cardRemoveEmployee(cardEmployeeModel);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Employee removed successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectDeleteProjectToState(
      String projectId) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      projectRepository.deleteProject(projectId);

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Project deleted successfully',
          event: SucceessEvents.ProjectDelete));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectCardAddCommentToState(
      CardAddCommentPostModel comment) async* {
    try {
      comment = comment.rebuild((c) => c..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.cardAddComment(comment);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Card updated successfully',
          event: SucceessEvents.UpdateCard));

      yield ProjectInitial(selectedIndex: 0, projectId: null);
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectUpdateCardStatusToState(
      UpdateCardStatusModel card) async* {
    try {
      card = card.rebuild((c) => c..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.updateCardStatus(card);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Card updated successfully',
          event: SucceessEvents.UpdateCard));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectShiftCardToState(ShiftCardModel card) async* {
    try {
      card = card.rebuild((c) => c..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.shiftCard(card);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Card updated successfully',
          event: SucceessEvents.UpdateCard));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectUpdateCardToState(
      ProjectUpdateCard event) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      await cardRepository.updateCardStatic(event.card);

      add(ProjectRequested(
        withoutLoader: true,
        listItemBloc: null,
        projectId: state.projectId,
      ));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Card updated successfully',
          event: SucceessEvents.UpdateCard));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectRemoveListToState(
      RemoveListPostModel list) async* {
    try {
      list = list.rebuild((model) => model..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await listRepository.removeListFromProject(list);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'List removed successfully', event: SucceessEvents.AddList));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectAddListToState(AddListPostModel list) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      await listRepository.addListToProject(list);

      add(ProjectRequested(
          withoutLoader: true, listItemBloc: null, projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'List added successfully', event: SucceessEvents.AddList));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectToggleShowAddList(bool showAdd) async* {
    if (state is ProjectLoadSuccess) {
      ProjectLoadSuccess castState = state;

      yield ProjectLoadSuccess(
          projectId: state.projectId,
          cards: castState.cards,
          labels: castState.labels,
          projectModel: castState.projectModel,
          selectedIndex: castState.selectedIndex,
          subProjects: castState.subProjects,
          showAddList: showAdd,
          pageController: pageController);
    }
  }

  Stream<ProjectState> _mapProjectAddCardToState(
      AddCardPostModel card, ListItemBloc listItemBloc) async* {
    try {
      card = card.rebuild((model) => model..projectId = state.projectId);

      globalBloc.add(ShowLoadingWidget());

      await cardRepository.addCardToList(card);

      add(ProjectRequested(
          withoutLoader: true,
          listItemBloc: listItemBloc,
          projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Card added successfully', event: SucceessEvents.AddCard));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectAddSubProjectToState(
      AddSubProjectPostModel subProject, ListItemBloc listItemBloc) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      subProject = subProject
          .rebuild((model) => model..parentProjectId = state.projectId);

      await subProjectRepository.addSubProjectToList(subProject);

      add(ProjectRequested(
          withoutLoader: true,
          listItemBloc: listItemBloc,
          projectId: state.projectId));

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Sub-project added successfully',
          event: SucceessEvents.AddCard));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectState> _mapProjectRequestedToState(
      ProjectRequested event) async* {
    if (!event.withoutLoader)
      yield ProjectLoadInProgress(
        selectedIndex: state.selectedIndex,
        projectId: state.projectId,
      );
    try {
      ProjectResponseModel projectResponseModel =
          await projectsRepository.getProject(event.projectId);

      Map<String, ProjectCardModel> cards = Map<String, ProjectCardModel>();

      projectResponseModel.board.cards.forEach((card) {
        if (card != null && card.id != null) {
          String userId;
          if (authenticationBloc.state is AuthentcatedState) {
            userId = (authenticationBloc.state as AuthentcatedState).user.id;
          }
          bool assignedToCard = card.idMembers?.singleWhere(
                  (member) => member.id == userId,
                  orElse: () => null) !=
              null;
          CardTimesheetModel lastTimesheet;
          if (assignedToCard &&
              card.timesheets != null &&
              card.timesheets.isNotEmpty) {
            lastTimesheet = card.timesheets?.singleWhere(
                (timesheet) =>
                    timesheet.user.id == userId && timesheet.end == null,
                orElse: () => null);
          }

          card = card.rebuild((data) => data
            ..activeTimesheet =
                lastTimesheet != null ? lastTimesheet.toBuilder() : null);

          cards.putIfAbsent(card.id, () => card);
        }
      });

      Map<String, ProjectLabelModel> labels = Map<String, ProjectLabelModel>();

      projectResponseModel.board.labels.forEach((label) {
        if (label != null && label.id != null)
          labels.putIfAbsent(label.id, () => label);
      });

      Map<String, ProjectSubProjectModel> subProjects =
          Map<String, ProjectSubProjectModel>();

      projectResponseModel.board.subProjects.forEach((subProject) {
        if (subProject != null && subProject.id != null)
          subProjects.putIfAbsent(subProject.id, () => subProject);
      });

      yield ProjectLoadSuccess(
          projectId: event.projectId,
          selectedIndex: state.selectedIndex,
          projectModel: projectResponseModel?.board,
          cards: cards,
          labels: labels,
          subProjects: subProjects,
          showAddList: false,
          pageController: pageController);

      if (event.listItemBloc != null) {
        if (event.listItemBloc.state is ListItemInitialed) {
          ProjectListModel currentListItem =
              (event.listItemBloc.state as ListItemInitialed).projectListModel;

          ProjectListModel updatedListItem = projectResponseModel?.board?.lists
              ?.firstWhere(
                  (listItem) =>
                      listItem != null &&
                      currentListItem != null &&
                      listItem.id == currentListItem.id,
                  orElse: () => null);

          if (updatedListItem != null) {
            event.listItemBloc.add(ListItemUddate(
              projectListModel: updatedListItem,
            ));
          }
        }
      } else if (savedListItemBloc != null) {
        savedListItemBloc.add(ListItemUddate(
          projectListModel: projectResponseModel.board.lists.isEmpty
              ? null
              : projectResponseModel?.board?.lists?.first ?? null,
        ));

        // pageController
        //     .jumpToPage((projectResponseModel?.board?.lists?.length ?? 0));
      }

      if (cardBloc != null && cardBloc.state?.cardModel?.id != null) {
        cardBloc.add(CardSetCard(card: cards[cardBloc.state.cardModel.id]));
      }
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);

      yield ProjectLoadFailure(
          projectId: state.projectId, selectedIndex: state.selectedIndex);
    }
  }
}
