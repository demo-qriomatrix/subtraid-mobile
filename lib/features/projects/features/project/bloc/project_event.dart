part of 'project_bloc.dart';

abstract class ProjectEvent extends Equatable {
  const ProjectEvent();

  @override
  List<Object> get props => [];
}

class ProjectRequested extends ProjectEvent {
  final bool withoutLoader;
  final ListItemBloc listItemBloc;
  final String projectId;

  ProjectRequested(
      {@required this.withoutLoader,
      @required this.listItemBloc,
      @required this.projectId});
}

class ProjectChangeIndex extends ProjectEvent {
  final int index;
  ProjectChangeIndex({@required this.index});
}

class ProjectAddCard extends ProjectEvent {
  final AddCardPostModel card;
  final ListItemBloc listItemBloc;

  ProjectAddCard({@required this.card, @required this.listItemBloc});
}

class ProjectAddList extends ProjectEvent {
  final AddListPostModel list;

  ProjectAddList({
    @required this.list,
  });
}

class ProjectRemoveList extends ProjectEvent {
  final RemoveListPostModel list;

  ProjectRemoveList({
    @required this.list,
  });
}

class ProjectToggleShowAddList extends ProjectEvent {
  final bool showAdd;

  ProjectToggleShowAddList({@required this.showAdd});
}

class ProjectAddSubProject extends ProjectEvent {
  final AddSubProjectPostModel subProject;
  final ListItemBloc listItemBloc;

  ProjectAddSubProject({
    @required this.subProject,
    @required this.listItemBloc,
  });
}

class ProjectUpdateCard extends ProjectEvent {
  final UpdateCardStaticModel card;

  ProjectUpdateCard({
    @required this.card,
  });
}

class ProjectShiftCard extends ProjectEvent {
  final ShiftCardModel card;

  ProjectShiftCard({
    @required this.card,
  });
}

class ProjectUpdateCardStatus extends ProjectEvent {
  final UpdateCardStatusModel card;

  ProjectUpdateCardStatus({
    @required this.card,
  });
}

class ProjectCardAddComment extends ProjectEvent {
  final CardAddCommentPostModel comment;

  ProjectCardAddComment({
    @required this.comment,
  });
}

class ProjectDeleteProject extends ProjectEvent {
  final String projectId;

  ProjectDeleteProject({
    @required this.projectId,
  });
}

class ProjectCardAddEmployee extends ProjectEvent {
  final String employeeId;

  ProjectCardAddEmployee({
    @required this.employeeId,
  });
}

class ProjectCardRemoveEmployee extends ProjectEvent {
  final String employeeId;

  ProjectCardRemoveEmployee({
    @required this.employeeId,
  });
}

class ProjectCardAddChecklist extends ProjectEvent {
  final String subTask;

  ProjectCardAddChecklist({
    @required this.subTask,
  });
}

class ProjectCardAddLabel extends ProjectEvent {
  final ProjectLabelModel label;

  ProjectCardAddLabel({
    @required this.label,
  });
}

class ProjectCardAddToEvent extends ProjectEvent {}

class ProjectCardUpdateLabel extends ProjectEvent {
  final ProjectLabelModel label;

  ProjectCardUpdateLabel({
    @required this.label,
  });
}

class ProjectCardStartTask extends ProjectEvent {}

class ProjectStartTask extends ProjectEvent {
  final String userId;

  ProjectStartTask({
    @required this.userId,
  });
}

class ProjectSopTask extends ProjectEvent {
  final String timesheetId;

  ProjectSopTask({
    @required this.timesheetId,
  });
}

class ProjectCardStopTask extends ProjectEvent {}

class ProjectCardAddChecklistItem extends ProjectEvent {
  final ActionEnum action;
  final String checkListId;
  final CardChecklistItemModel checkItem;

  ProjectCardAddChecklistItem(
      {@required this.checkListId,
      @required this.checkItem,
      @required this.action});
}

class ProjectRemoveMember extends ProjectEvent {
  final String userId;

  ProjectRemoveMember({@required this.userId});
}

class ProjectRemoveMembers extends ProjectEvent {
  final List<String> userIds;

  ProjectRemoveMembers({@required this.userIds});
}

class ProjectUpdate extends ProjectEvent {
  final ProjectResponseModel project;

  ProjectUpdate({@required this.project});
}
