import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';

class CardNameWidget extends StatefulWidget {
  CardNameWidget({
    @required this.cardModel,
  });

  final ProjectCardModel cardModel;

  @override
  _CardNameWidgetState createState() => _CardNameWidgetState();
}

class _CardNameWidgetState extends State<CardNameWidget> {
  TextEditingController textEditingController = TextEditingController();
  bool isSubmitted = false;

  @override
  void initState() {
    super.initState();
    textEditingController.text = widget.cardModel.name;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardBloc, CardState>(
      builder: (context, state) {
        return state.nameEditMode ||
                widget.cardModel.name == null ||
                widget.cardModel.name == ''
            ? Row(
                children: <Widget>[
                  Expanded(
                    child: TextFormField(
                      controller: textEditingController,
                      decoration: InputDecoration(
                        hintText: 'Type card name here',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        border: OutlineInputBorder(),
                      ),
                      style: TextStyle(
                          color: ColorConfig.primary,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  RawMaterialButton(
                    onPressed: () {
                      context.read<CardBloc>().add(CardToggleEditMode(
                          addSubtaskMode: null,
                          addCheckItemMode: null,
                          nameEditMode: false,
                          descriptionEditMode: null));
                    },
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    constraints: BoxConstraints(),
                    child: Icon(Icons.close),
                  ),
                  RawMaterialButton(
                    onPressed: () {
                      UpdateCardStaticModel updateCard = UpdateCardStaticModel(
                          (card) => card
                            ..projectId = (context.read<ProjectBloc>().state
                                    as ProjectLoadSuccess)
                                .projectModel
                                .id
                            ..updatedCard = widget.cardModel
                                .rebuild((update) =>
                                    update..name = textEditingController.text)
                                .toBuilder());

                      context.read<ProjectBloc>().add(ProjectUpdateCard(
                            card: updateCard,
                          ));
                    },
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    constraints: BoxConstraints(),
                    child: Icon(Icons.check),
                  ),
                ],
              )
            : GestureDetector(
                onTap: () {
                  context.read<CardBloc>().add(CardToggleEditMode(
                      addSubtaskMode: null,
                      addCheckItemMode: null,
                      nameEditMode: true,
                      descriptionEditMode: null));
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: Text(
                    widget.cardModel.name == null || widget.cardModel.name == ''
                        ? ' - '
                        : widget.cardModel.name,
                    style: TextStyle(
                        color: ColorConfig.primary,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              );
      },
    );
  }
}
