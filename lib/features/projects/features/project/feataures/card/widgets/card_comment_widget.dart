import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_comment_model.dart';

class CardCommentWidget extends StatelessWidget {
  final CardCommentModel commentModel;

  CardCommentWidget({@required this.commentModel});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            backgroundImage: AssetImage(LocalImages.defaultAvatar),
            radius: 16,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${commentModel.idMember?.name ?? ''}",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      "${commentModel.dateCreated != null ? DateFormat('MMMM dd, yyyy, hh:mm: a').format(DateTime.parse(commentModel.dateCreated)) : ''}",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        commentModel.message ?? '',
                        style: TextStyle(
                            color: ColorConfig.black5,
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
