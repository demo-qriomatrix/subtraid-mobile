import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:provider/provider.dart';

class AddMemberDialog extends StatefulWidget {
  final List<UserModel> idMembers;
  final List<UserModel> projectMembers;

  AddMemberDialog({@required this.idMembers, @required this.projectMembers});

  @override
  _AddMemberDialogState createState() => _AddMemberDialogState();
}

class _AddMemberDialogState extends State<AddMemberDialog> {
  Set<UserModel> selectedMembers = Set();
  TextEditingController member = TextEditingController();

  @override
  void initState() {
    selectedMembers = widget.idMembers.toSet();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              CircleAvatar(
                backgroundColor: ColorConfig.orange,
                child: ImageIcon(
                  AssetImage(LocalImages.filter),
                  color: Colors.white,
                  size: 14,
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Text(
                'Assign Members',
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
              Spacer(),
              IconButton(
                icon: Icon(Icons.close),
                color: ColorConfig.primary,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Assignee',
            style: TextStyle(
                color: Colors.black, fontSize: 13, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10,
          ),
          selectedMembers.isEmpty
              ? Container()
              : Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    children: selectedMembers
                        .map((e) => Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    selectedMembers.remove(e);
                                  });

                                  context.read<ProjectBloc>().add(
                                      ProjectCardRemoveEmployee(
                                          employeeId: e.id));
                                },
                                child: SizedBox(
                                  height: 40,
                                  width: 40,
                                  child: Stack(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: CircleAvatar(
                                          backgroundImage: AssetImage(
                                              LocalImages.defaultAvatar),
                                          radius: 16,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Icon(
                                          Icons.cancel_outlined,
                                          color: Colors.red,
                                          size: 18,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ))
                        .toList(),
                  ),
                ),
          SizedBox(
            height: 10,
          ),
          TypeAheadFormField(
              enabled: true,
              textFieldConfiguration: TextFieldConfiguration(
                controller: member,
                decoration: InputDecoration(
                  fillColor: ColorConfig.blue1,
                  filled: true,
                  labelText: 'Select members',
                  labelStyle: TextStyle(
                      color: ColorConfig.black1,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                  suffixIcon: Icon(
                    Icons.add,
                    color: ColorConfig.primary,
                  ),
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide.none),
                ),
              ),
              suggestionsCallback: (pattern) {
                return widget.projectMembers
                    .where((element) => element.name
                        .toLowerCase()
                        .contains(pattern.toLowerCase()))
                    .toList();
              },
              itemBuilder: (context, UserModel suggestion) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Theme(
                        data: ThemeData(
                            unselectedWidgetColor: ColorConfig.orange),
                        child: IgnorePointer(
                          child: Checkbox(
                              value: selectedMembers
                                  .where((mem) => mem.id == suggestion.id)
                                  .isNotEmpty,
                              onChanged: (val) {
                                if (val) {
                                  setState(() {
                                    selectedMembers.add(suggestion);
                                  });
                                  context.read<ProjectBloc>().add(
                                      ProjectCardAddEmployee(
                                          employeeId: suggestion.id));
                                } else {
                                  setState(() {
                                    selectedMembers.remove(selectedMembers
                                        .where((mem) => mem.id == suggestion.id)
                                        .first);
                                  });
                                  context.read<ProjectBloc>().add(
                                      ProjectCardRemoveEmployee(
                                          employeeId: suggestion.id));
                                }
                              }),
                        ),
                      ),
                      CircleAvatar(
                        backgroundImage: AssetImage(LocalImages.defaultAvatar),
                        radius: 16,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(suggestion.name)
                    ],
                  ),
                );
              },
              transitionBuilder: (context, suggestionsBox, controller) {
                return suggestionsBox;
              },
              onSuggestionSelected: (suggestion) {
                if (selectedMembers
                    .where((mem) => mem.id == suggestion.id)
                    .isEmpty) {
                  setState(() {
                    selectedMembers.add(suggestion);
                  });

                  context
                      .read<ProjectBloc>()
                      .add(ProjectCardAddEmployee(employeeId: suggestion.id));
                } else {
                  setState(() {
                    selectedMembers.remove(selectedMembers
                        .where((mem) => mem.id == suggestion.id)
                        .first);
                  });
                  context.read<ProjectBloc>().add(
                      ProjectCardRemoveEmployee(employeeId: suggestion.id));
                }
              },
              validator: (value) {
                return null;
              },
              onSaved: (value) {}),
          SizedBox(
            height: 10,
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   children: <Widget>[
          //     FlatButton(
          //       shape: RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(5)),
          //       color: ColorConfig.grey3,
          //       child: Text(
          //         'Done',
          //         style: TextStyle(fontSize: 12, color: ColorConfig.grey2),
          //       ),
          //       onPressed: () {
          //         Navigator.of(context).pop();
          //       },
          //     ),
          //     // SizedBox(
          //     //   width: 10,
          //     // ),
          //     // FlatButton(
          //     //   shape: RoundedRectangleBorder(
          //     //       borderRadius: BorderRadius.circular(5)),
          //     //   color: ColorConfig.primary,
          //     //   child: Text(
          //     //     'Add',
          //     //     style: TextStyle(fontSize: 12, color: Colors.white),
          //     //   ),
          //     //   onPressed: () {
          //     //     Navigator.of(context).pop();
          //     //   },
          //     // )
          //   ],
          // )
        ],
      ),
    );
  }
}
