import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/widgets/card_label_widget.dart';
import 'package:Subtraid/features/projects/features/project/models/project_label_model.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class AddLabelDialog extends StatefulWidget {
  @override
  _AddLabelDialogState createState() => _AddLabelDialogState();
}

class _AddLabelDialogState extends State<AddLabelDialog> {
  Set<String> selectedLabels = Set();
  TextEditingController search = TextEditingController();
  TextEditingController add = TextEditingController();
  bool addNewMode = false;
  bool ediMode = false;

  ProjectLabelModel editModel;

  Color pickerPrimaryColor = Color(0xff1e90ff);
  Color currentPrimaryColor;

  @override
  void initState() {
    selectedLabels = context.read<CardBloc>().state.cardModel.idLabels.toSet();

    super.initState();
  }

// ValueChanged<Color> callback
  void changePrimaryColor(Color color) {
    setState(() => pickerPrimaryColor = color);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('foxus');
        FocusScope.of(context).requestFocus(new FocusNode());
        FocusScope.of(context).unfocus();
      },
      child: BlocListener<GlobalBloc, GlobalState>(
        listener: (context, state) {
          if (state is SuccessSnackBarState) {
            if (state.event == SucceessEvents.Label) {
              Navigator.of(context).pop();
            }
          }
        },
        child: BlocBuilder<ProjectBloc, ProjectState>(
          builder: (context, projectState) {
            if (projectState is ProjectLoadSuccess) {
              return BlocBuilder<CardBloc, CardState>(
                builder: (context, cardState) {
                  return Container(
                    padding: EdgeInsets.all(20),
                    child: !(addNewMode || ediMode)
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundColor: ColorConfig.orange,
                                    child: ImageIcon(
                                      AssetImage(LocalImages.filter),
                                      color: Colors.white,
                                      size: 14,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Assign Labels',
                                    style: TextStyle(
                                        color: ColorConfig.primary,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.close),
                                    color: ColorConfig.primary,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'Labels',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Wrap(
                                children: [
                                  ...List.generate(
                                      cardState.cardModel.idLabels.length,
                                      (index) => CardLabelChipWidget(
                                              projectState.labels[cardState
                                                  .cardModel
                                                  .idLabels[index]], () {
                                            setState(() {
                                              selectedLabels.remove(
                                                  selectedLabels
                                                      .where((labelId) =>
                                                          labelId ==
                                                          cardState.cardModel
                                                              .idLabels[index])
                                                      .first);
                                            });

                                            UpdateCardStaticModel updateCard =
                                                UpdateCardStaticModel((card) =>
                                                    card
                                                      ..projectId = (context
                                                                  .read<
                                                                      ProjectBloc>()
                                                                  .state
                                                              as ProjectLoadSuccess)
                                                          .projectModel
                                                          .id
                                                      ..updatedCard = cardState
                                                          .cardModel
                                                          .rebuild((e) => e
                                                                  .idLabels =
                                                              ListBuilder(
                                                                  selectedLabels))
                                                          .toBuilder());
                                            context
                                                .read<ProjectBloc>()
                                                .add(ProjectUpdateCard(
                                                  card: updateCard,
                                                ));
                                          })),
                                ],
                              ),

                              SizedBox(
                                height: 10,
                              ),

                              // Container(
                              //   height: 250,
                              //   child: CircleColorPicker(
                              //     initialColor: Colors.blue,
                              //     onChanged: (color) => print(color),
                              //     size: const Size(240, 240),
                              //     strokeWidth: 4,
                              //     thumbSize: 36,
                              //   ),
                              // ),
                              TypeAheadFormField(
                                  // enabled: true,
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                    controller: search,
                                    decoration: InputDecoration(
                                      fillColor: ColorConfig.blue1,
                                      filled: true,
                                      labelText: 'Select label',
                                      labelStyle: TextStyle(
                                          color: ColorConfig.black1,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                      suffixIcon: IconButton(
                                        icon: Icon(
                                          Icons.add,
                                          color: ColorConfig.primary,
                                        ),
                                        onPressed: () {},
                                      ),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide.none),
                                    ),
                                  ),
                                  suggestionsCallback: (pattern) {
                                    return projectState.labels.values
                                        .where((element) => element.name
                                            .toLowerCase()
                                            .contains(pattern.toLowerCase()))
                                        .toList();
                                  },
                                  itemBuilder: (itemBuilderContext,
                                      ProjectLabelModel suggestion) {
                                    return Material(
                                      child: InkWell(
                                        onTap: () {
                                          if (!selectedLabels
                                              .where((labelId) =>
                                                  labelId == suggestion.id)
                                              .isNotEmpty) {
                                            setState(() {
                                              selectedLabels.add(suggestion.id);
                                            });

                                            UpdateCardStaticModel updateCard =
                                                UpdateCardStaticModel((card) =>
                                                    card
                                                      ..projectId = (context
                                                                  .read<
                                                                      ProjectBloc>()
                                                                  .state
                                                              as ProjectLoadSuccess)
                                                          .projectModel
                                                          .id
                                                      ..updatedCard = cardState
                                                          .cardModel
                                                          .rebuild((e) => e
                                                                  .idLabels =
                                                              ListBuilder(
                                                                  selectedLabels))
                                                          .toBuilder());
                                            context
                                                .read<ProjectBloc>()
                                                .add(ProjectUpdateCard(
                                                  card: updateCard,
                                                ));
                                          } else {
                                            setState(() {
                                              selectedLabels.remove(
                                                  selectedLabels
                                                      .where((labelId) =>
                                                          labelId ==
                                                          suggestion.id)
                                                      .first);
                                            });

                                            UpdateCardStaticModel updateCard =
                                                UpdateCardStaticModel((card) =>
                                                    card
                                                      ..projectId = (context
                                                                  .read<
                                                                      ProjectBloc>()
                                                                  .state
                                                              as ProjectLoadSuccess)
                                                          .projectModel
                                                          .id
                                                      ..updatedCard = cardState
                                                          .cardModel
                                                          .rebuild((e) => e
                                                                  .idLabels =
                                                              ListBuilder(
                                                                  selectedLabels))
                                                          .toBuilder());
                                            context
                                                .read<ProjectBloc>()
                                                .add(ProjectUpdateCard(
                                                  card: updateCard,
                                                ));
                                            // context.read<ProjectBloc>().add(
                                            //     ProjectCardRemoveEmployee(
                                            //         employeeId: suggestion.id));
                                          }
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          child: Row(
                                            children: [
                                              // Theme(
                                              //   data: ThemeData(
                                              //       unselectedWidgetColor:
                                              //           ColorConfig.orange),
                                              //   child: IgnorePointer(
                                              //     child: Checkbox(
                                              //         value: selectedLabels
                                              //             .where((labelId) =>
                                              //                 labelId ==
                                              //                 suggestion.id)
                                              //             .isNotEmpty,
                                              //         onChanged: (val) {
                                              //           if (val) {
                                              //             setState(() {
                                              //               selectedLabels.add(
                                              //                   suggestion.id);
                                              //             });

                                              //             UpdateCardStaticModel updateCard = UpdateCardStaticModel(
                                              //                 (card) => card
                                              //                   ..projectId = (context
                                              //                               .read<
                                              //                                   ProjectBloc>()
                                              //                               .state
                                              //                           as ProjectLoadSuccess)
                                              //                       .projectModel
                                              //                       .id
                                              //                   ..updatedCard = cardState
                                              //                       .cardModel
                                              //                       .rebuild((e) => e
                                              //                               .idLabels =
                                              //                           ListBuilder(
                                              //                               selectedLabels))
                                              //                       .toBuilder());
                                              //             context
                                              //                 .read<
                                              //                     ProjectBloc>()
                                              //                 .add(
                                              //                     ProjectUpdateCard(
                                              //                   card:
                                              //                       updateCard,
                                              //                 ));
                                              //           } else {
                                              //             setState(() {
                                              //               selectedLabels.remove(
                                              //                   selectedLabels
                                              //                       .where((labelId) =>
                                              //                           labelId ==
                                              //                           suggestion
                                              //                               .id)
                                              //                       .first);
                                              //             });

                                              //             UpdateCardStaticModel updateCard = UpdateCardStaticModel(
                                              //                 (card) => card
                                              //                   ..projectId = (context
                                              //                               .read<
                                              //                                   ProjectBloc>()
                                              //                               .state
                                              //                           as ProjectLoadSuccess)
                                              //                       .projectModel
                                              //                       .id
                                              //                   ..updatedCard = cardState
                                              //                       .cardModel
                                              //                       .rebuild((e) => e
                                              //                               .idLabels =
                                              //                           ListBuilder(
                                              //                               selectedLabels))
                                              //                       .toBuilder());
                                              //             context
                                              //                 .read<
                                              //                     ProjectBloc>()
                                              //                 .add(
                                              //                     ProjectUpdateCard(
                                              //                   card:
                                              //                       updateCard,
                                              //                 ));
                                              //             // context.read<ProjectBloc>().add(
                                              //             //     ProjectCardRemoveEmployee(
                                              //             //         employeeId: suggestion.id));
                                              //           }
                                              //         }),
                                              //   ),
                                              // ),

                                              Expanded(
                                                  child: CardLabelDropDown(
                                                label: suggestion,
                                                checked: selectedLabels
                                                    .where((labelId) =>
                                                        labelId ==
                                                        suggestion.id)
                                                    .isNotEmpty,
                                              )),
                                              IconButton(
                                                  icon: ImageIcon(
                                                    AssetImage(
                                                      LocalImages.edit,
                                                    ),
                                                    color: ColorConfig.primary,
                                                    size: 18,
                                                  ),
                                                  onPressed: () {
                                                    setState(() {
                                                      ediMode = true;
                                                      editModel = suggestion;
                                                      pickerPrimaryColor =
                                                          fromHex(
                                                              suggestion.color);
                                                      add.text =
                                                          suggestion.name;
                                                    });
                                                  })
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  onSuggestionSelected:
                                      (ProjectLabelModel suggestion) {
                                    if (selectedLabels
                                        .where((labelId) =>
                                            labelId == suggestion.id)
                                        .isEmpty) {
                                      setState(() {
                                        selectedLabels.add(suggestion.id);
                                      });
                                      UpdateCardStaticModel updateCard =
                                          UpdateCardStaticModel((card) => card
                                            ..projectId = (context
                                                        .read<ProjectBloc>()
                                                        .state
                                                    as ProjectLoadSuccess)
                                                .projectModel
                                                .id
                                            ..updatedCard = cardState.cardModel
                                                .rebuild((e) => e.idLabels =
                                                    ListBuilder(selectedLabels))
                                                .toBuilder());
                                      context
                                          .read<ProjectBloc>()
                                          .add(ProjectUpdateCard(
                                            card: updateCard,
                                          ));
                                      // context.read<ProjectBloc>().add(
                                      //     ProjectCardAddEmployee(employeeId: suggestion.id));
                                    } else {
                                      setState(() {
                                        selectedLabels.remove(selectedLabels
                                            .where((labelId) =>
                                                labelId == suggestion.id)
                                            .first);
                                      });

                                      UpdateCardStaticModel updateCard =
                                          UpdateCardStaticModel((card) => card
                                            ..projectId = (context
                                                        .read<ProjectBloc>()
                                                        .state
                                                    as ProjectLoadSuccess)
                                                .projectModel
                                                .id
                                            ..updatedCard = cardState.cardModel
                                                .rebuild((e) => e.idLabels =
                                                    ListBuilder(selectedLabels))
                                                .toBuilder());
                                      context
                                          .read<ProjectBloc>()
                                          .add(ProjectUpdateCard(
                                            card: updateCard,
                                          ));
                                      // context.read<ProjectBloc>().add(
                                      //     ProjectCardRemoveEmployee(
                                      //         employeeId: suggestion.id));
                                    }
                                  },
                                  validator: (value) {
                                    return null;
                                  },
                                  onSaved: (value) {}),

                              SizedBox(
                                height: 10,
                              ),
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.end,
                              //   children: <Widget>[
                              //     FlatButton(
                              //       shape: RoundedRectangleBorder(
                              //           borderRadius: BorderRadius.circular(5)),
                              //       color: ColorConfig.grey3,
                              //       child: Text(
                              //         'Done',
                              //         style: TextStyle(fontSize: 12, color: ColorConfig.grey2),
                              //       ),
                              //       onPressed: () {
                              //         Navigator.of(context).pop();
                              //       },
                              //     ),
                              //     // SizedBox(
                              //     //   width: 10,
                              //     // ),
                              //     // FlatButton(
                              //     //   shape: RoundedRectangleBorder(
                              //     //       borderRadius: BorderRadius.circular(5)),
                              //     //   color: ColorConfig.primary,
                              //     //   child: Text(
                              //     //     'Add',
                              //     //     style: TextStyle(fontSize: 12, color: Colors.white),
                              //     //   ),
                              //     //   onPressed: () {
                              //     //     Navigator.of(context).pop();
                              //     //   },
                              //     // )
                              //   ],
                              // )
                              Divider(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SaveButton('Create New Label', () {
                                    setState(() {
                                      addNewMode = true;
                                    });
                                  })
                                ],
                              )
                            ],
                          )
                        : ListView(
                            shrinkWrap: true,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            // mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(Icons.arrow_back),
                                      onPressed: () {
                                        setState(() {
                                          addNewMode = false;
                                          ediMode = false;
                                        });
                                      }),
                                  CircleAvatar(
                                    backgroundColor: ColorConfig.orange,
                                    child: ImageIcon(
                                      AssetImage(LocalImages.filter),
                                      color: Colors.white,
                                      size: 14,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    ediMode ? 'Edit Label' : 'Create New Label',
                                    style: TextStyle(
                                        color: ColorConfig.primary,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.close),
                                    color: ColorConfig.primary,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'Labels',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Wrap(
                                children: [
                                  ...List.generate(
                                      cardState.cardModel.idLabels.length,
                                      (index) => CardLabelChipWidget(
                                              projectState.labels[cardState
                                                  .cardModel
                                                  .idLabels[index]], () {
                                            setState(() {
                                              selectedLabels.remove(
                                                  selectedLabels
                                                      .where((labelId) =>
                                                          labelId ==
                                                          cardState.cardModel
                                                              .idLabels[index])
                                                      .first);
                                            });

                                            UpdateCardStaticModel updateCard =
                                                UpdateCardStaticModel((card) =>
                                                    card
                                                      ..projectId = (context
                                                                  .read<
                                                                      ProjectBloc>()
                                                                  .state
                                                              as ProjectLoadSuccess)
                                                          .projectModel
                                                          .id
                                                      ..updatedCard = cardState
                                                          .cardModel
                                                          .rebuild((e) => e
                                                                  .idLabels =
                                                              ListBuilder(
                                                                  selectedLabels))
                                                          .toBuilder());
                                            context
                                                .read<ProjectBloc>()
                                                .add(ProjectUpdateCard(
                                                  card: updateCard,
                                                ));
                                          })),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                controller: add,
                                decoration: InputDecoration(
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  labelText: 'Type label name',
                                  labelStyle: TextStyle(
                                      color: ColorConfig.black1,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide.none),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              ColorPicker(
                                pickerColor: pickerPrimaryColor,
                                onColorChanged: changePrimaryColor,
                                showLabel: true,
                                pickerAreaHeightPercent: 0.8,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  SaveButton(ediMode ? 'Edit Label' : 'Create',
                                      () {
                                    if (ediMode) {
                                      ProjectLabelModel label =
                                          editModel.rebuild((data) => data
                                            ..name = add.text != ''
                                                ? add.text
                                                : 'Untitled'
                                            ..color =
                                                "#${pickerPrimaryColor.value.toRadixString(16).substring(2)}");

                                      context.read<ProjectBloc>().add(
                                          ProjectCardUpdateLabel(label: label));
                                    } else {
                                      ProjectLabelModel label =
                                          ProjectLabelModel((data) => data
                                            ..name = add.text != ''
                                                ? add.text
                                                : 'Untitled'
                                            ..color =
                                                "#${pickerPrimaryColor.value.toRadixString(16).substring(2)}");

                                      context.read<ProjectBloc>().add(
                                          ProjectCardAddLabel(label: label));
                                    }
                                  })
                                ],
                              )
                            ],
                          ),
                  );
                },
              );
            }

            return Container();
          },
        ),
      ),
    );
  }
}
