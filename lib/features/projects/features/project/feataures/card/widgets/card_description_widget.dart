import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CardDescriptionWidget extends StatefulWidget {
  const CardDescriptionWidget({
    Key key,
    @required this.cardModel,
  }) : super(key: key);

  final ProjectCardModel cardModel;

  @override
  _CardDescriptionWidgetState createState() => _CardDescriptionWidgetState();
}

class _CardDescriptionWidgetState extends State<CardDescriptionWidget> {
  bool editMode = false;
  TextEditingController textEditingController = TextEditingController();
  bool isSubmitted = false;

  @override
  void initState() {
    super.initState();
    textEditingController.text = widget.cardModel.description;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardBloc, CardState>(builder: (context, state) {
      return state.descriptionEditMode ||
              widget.cardModel.description == null ||
              widget.cardModel.description == ''
          ? Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextFormField(
                      controller: textEditingController,
                      maxLines: null,
                      decoration: InputDecoration(
                        hintText: 'Type description here',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        border: OutlineInputBorder(),
                      ),
                      style: TextStyle(
                          fontSize: 13,
                          color: ColorConfig.grey14,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  RawMaterialButton(
                    onPressed: () {
                      context.read<CardBloc>().add(CardToggleEditMode(
                          addSubtaskMode: null,
                          nameEditMode: null,
                          addCheckItemMode: null,
                          descriptionEditMode: false));
                    },
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    constraints: BoxConstraints(),
                    child: Icon(Icons.close),
                  ),
                  RawMaterialButton(
                    onPressed: () {
                      UpdateCardStaticModel updateCard = UpdateCardStaticModel(
                          (card) => card
                            ..projectId = (context.read<ProjectBloc>().state
                                    as ProjectLoadSuccess)
                                .projectModel
                                .id
                            ..updatedCard = widget.cardModel
                                .rebuild((update) => update
                                  ..description = textEditingController.text)
                                .toBuilder());

                      context.read<ProjectBloc>().add(ProjectUpdateCard(
                            card: updateCard,
                          ));
                    },
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    constraints: BoxConstraints(),
                    child: Icon(Icons.check),
                  ),
                ],
              ),
            )
          : GestureDetector(
              onTap: () {
                context.read<CardBloc>().add(CardToggleEditMode(
                    addSubtaskMode: null,
                    nameEditMode: null,
                    addCheckItemMode: null,
                    descriptionEditMode: true));
              },
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  widget.cardModel.description == null ||
                          widget.cardModel.description == ''
                      ? ' - '
                      : widget.cardModel.description,
                  style: TextStyle(
                      fontSize: 13,
                      color: ColorConfig.grey14,
                      fontWeight: FontWeight.w300),
                ),
              ),
            );
    });
  }
}
