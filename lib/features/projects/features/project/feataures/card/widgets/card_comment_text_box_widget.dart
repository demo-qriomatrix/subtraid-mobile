import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_comment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_comment_post_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';

class CardCommentTextBoxWidget extends StatefulWidget {
  final ProjectCardModel cardModel;

  CardCommentTextBoxWidget({@required this.cardModel});

  @override
  _CardCommentTextBoxWidgetState createState() =>
      _CardCommentTextBoxWidgetState();
}

class _CardCommentTextBoxWidgetState extends State<CardCommentTextBoxWidget> {
  TextEditingController commentText = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CircleAvatar(
          backgroundImage: AssetImage(LocalImages.defaultAvatar),
          radius: 20,
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: TextField(
            controller: commentText,
            decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: ImageIcon(
                    AssetImage(LocalImages.paperPlane),
                    size: 14,
                  ),
                  color: ColorConfig.primary,
                  onPressed: () {
                    AuthenticationState state =
                        context.read<AuthenticationBloc>().state;

                    if (state is AuthentcatedState) {
                      CardAddCommentModel commentModel = CardAddCommentModel(
                          (commentModel) => commentModel
                            ..message = commentText.text
                            ..idMember = state.user.id
                            ..img = state.user.img
                            ..time = DateFormat('MMMM dd, yyyy, hh:mm: a')
                                .format(DateTime.now()));

                      CardAddCommentPostModel addCommentModel =
                          CardAddCommentPostModel(
                              (addCommentModel) => addCommentModel
                                ..cardId = widget.cardModel.id
                                ..newComment = commentModel.toBuilder());

                      context
                          .read<ProjectBloc>()
                          .add(ProjectCardAddComment(comment: addCommentModel));

                      commentText.clear();

                      FocusScope.of(context).requestFocus(new FocusNode());
                    }
                  },
                ),
                fillColor: ColorConfig.blue5,
                filled: true,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide.none),
                hintText: "Add a comment..."),
            maxLines: null,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.black.withOpacity(0.74),
                fontSize: 14),
          ),
        )
      ],
    );
  }
}
