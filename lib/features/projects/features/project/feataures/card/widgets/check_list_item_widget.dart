import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/enums/check_list_action_enum.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'check_item_widget.dart';

class CheckListItem extends StatefulWidget {
  const CheckListItem({
    Key key,
    @required this.cardChecklistModel,
    @required this.addCheckItemMode,
  }) : super(key: key);

  final CardChecklistModel cardChecklistModel;
  final bool addCheckItemMode;

  @override
  _CheckListItemState createState() => _CheckListItemState();
}

class _CheckListItemState extends State<CheckListItem> {
  TextEditingController checkItemTitle = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  bool show = false;

  @override
  Widget build(BuildContext context) {
    return BlocListener<GlobalBloc, GlobalState>(
      listener: (context, state) {
        if (state is SuccessSnackBarState) {
          if (state.event == SucceessEvents.CheckItem) {
            setState(() {
              show = false;
            });
          }
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Theme(
                data: ThemeData(unselectedWidgetColor: ColorConfig.green2),
                child: Checkbox(
                  activeColor: ColorConfig.green2,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: widget.cardChecklistModel.checkItemsChecked /
                          widget.cardChecklistModel.checkItems.length ==
                      1,
                  onChanged: (val) {
                    if (val) {
                      context.read<ProjectBloc>().add(
                          ProjectCardAddChecklistItem(
                              action: ActionEnum.selectAll,
                              checkItem: null,
                              checkListId: widget.cardChecklistModel.id));
                    } else {
                      context.read<ProjectBloc>().add(
                          ProjectCardAddChecklistItem(
                              action: ActionEnum.deselectAll,
                              checkItem: null,
                              checkListId: widget.cardChecklistModel.id));
                    }
                    print(val);
                  },
                ),
              ),
              CheckListItemNameWidget(widget: widget),
              Spacer(),
              RawMaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                constraints: BoxConstraints(),
                fillColor: show ? ColorConfig.orange : Colors.white,
                onPressed: () {
                  setState(() {
                    show = !show;
                  });

                  if (show) {
                    context.read<CardBloc>().add(CardToggleEditMode(
                        addCheckItemMode: false,
                        addSubtaskMode: null,
                        nameEditMode: null,
                        descriptionEditMode: null));
                  } else {
                    // setState(() {
                    //   show = true;
                    // });

                    context.read<CardBloc>().add(CardToggleEditMode(
                        addCheckItemMode: true,
                        addSubtaskMode: null,
                        nameEditMode: null,
                        descriptionEditMode: null));
                  }
                },
                padding: EdgeInsets.all(2),
                child: Icon(
                  Icons.add,
                  color: show ? Colors.white : ColorConfig.primary,
                  size: 22,
                ),
              ),
              PopupMenuButton(
                icon: Icon(
                  Icons.more_vert,
                  color: ColorConfig.primary,
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                onSelected: (val) {
                  if (val == 'delete') {
                    context.read<ProjectBloc>().add(ProjectCardAddChecklistItem(
                        action: ActionEnum.deleteAll,
                        checkItem: null,
                        checkListId: widget.cardChecklistModel.id));
                  }
                },
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      height: 36,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Remove Checklist',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      value: 'delete',
                    ),
                  ];
                },
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Text(
                        '${widget.cardChecklistModel.checkItemsChecked}/${widget.cardChecklistModel.checkItems.length}',
                        style: TextStyle(
                            fontSize: 13, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Expanded(
                      child: Stack(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ColorConfig.fillColorShade),
                            height: 10,
                          ),
                          Row(
                            children: <Widget>[
                              Flexible(
                                flex:
                                    widget.cardChecklistModel.checkItemsChecked,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: ColorConfig.fillColor),
                                  height: 10,
                                ),
                              ),
                              Flexible(
                                flex: widget
                                        .cardChecklistModel.checkItems.length -
                                    widget.cardChecklistModel.checkItemsChecked,
                                child: Container(
                                  height: 10,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),

                Text(
                  'Checklist',
                  style: TextStyle(
                      fontSize: 14,
                      color: ColorConfig.primary,
                      fontWeight: FontWeight.w500),
                ),
                // SizedBox(
                //   height: 10,
                // ),
                //   Text(
                //   "Add Checklist",
                //   style: TextStyle(
                //       color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400),
                // ),
                // SizedBox(
                //   height: 10,
                // ),
                !show
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: TextFormField(
                          controller: checkItemTitle,
                          decoration: InputDecoration(
                              suffixIcon: RawMaterialButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                constraints: BoxConstraints(),
                                onPressed: () {
                                  CardChecklistItemModel checkItemModel =
                                      CardChecklistItemModel((checkItemModel) =>
                                          checkItemModel
                                            ..name = checkItemTitle.text != ''
                                                ? checkItemTitle.text
                                                : 'Untitled Check Item');

                                  context.read<ProjectBloc>().add(
                                      ProjectCardAddChecklistItem(
                                          action: ActionEnum.add,
                                          checkItem: checkItemModel,
                                          checkListId:
                                              widget.cardChecklistModel.id));

                                  // onAddCheckItem(checkItemTitle.text);
                                },
                                shape: CircleBorder(),
                                fillColor: ColorConfig.primary,
                                padding: const EdgeInsets.all(5.0),
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 20,
                                ),
                              ),
                              suffixIconConstraints:
                                  BoxConstraints(maxHeight: 30, maxWidth: 30),
                              fillColor: ColorConfig.blue1,
                              filled: true,
                              hintText: 'Add Checklist',
                              hintStyle: TextStyle(
                                  color: ColorConfig.black1,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide.none)),
                          maxLines: null,
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.black.withOpacity(0.74),
                              fontSize: 14),
                        ),
                      ),

                SizedBox(
                  height: 15,
                ),
                Column(
                  children: widget.cardChecklistModel.checkItems
                      .map(
                        (checkListItem) => CheckItem(
                          cardChecklistItemModel: checkListItem,
                          deleteCardChecklistItemModel: (val) {
                            context.read<ProjectBloc>().add(
                                ProjectCardAddChecklistItem(
                                    action: ActionEnum.delete,
                                    checkItem: val,
                                    checkListId: widget.cardChecklistModel.id));
                          },
                          updateCardChecklistItemModel: (val) {
                            context.read<ProjectBloc>().add(
                                ProjectCardAddChecklistItem(
                                    action: ActionEnum.update,
                                    checkItem: val,
                                    checkListId: widget.cardChecklistModel.id));
                          },
                        ),
                      )
                      .toList(),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Divider(),
          )
        ],
      ),
    );
  }
}

class CheckListItemNameWidget extends StatelessWidget {
  const CheckListItemNameWidget({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final CheckListItem widget;

  @override
  Widget build(BuildContext context) {
    return Text(
      widget.cardChecklistModel.name == '' ||
              widget.cardChecklistModel.name == null
          ? ' - '
          : widget.cardChecklistModel.name,
      style: TextStyle(
          fontSize: 13, fontWeight: FontWeight.w500, color: ColorConfig.black2),
    );
  }
}
