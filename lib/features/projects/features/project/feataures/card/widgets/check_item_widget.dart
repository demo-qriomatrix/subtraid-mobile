import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_item_model.dart';
import 'package:flutter/material.dart';

class CheckItem extends StatelessWidget {
  final CardChecklistItemModel cardChecklistItemModel;
  final Function(CardChecklistItemModel) updateCardChecklistItemModel;
  final Function(CardChecklistItemModel) deleteCardChecklistItemModel;

  CheckItem({
    @required this.cardChecklistItemModel,
    @required this.updateCardChecklistItemModel,
    @required this.deleteCardChecklistItemModel,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                spreadRadius: 1,
                blurRadius: 4,
                offset: Offset(0, 3),
              ),
            ],
          ),
          child: Row(
            children: <Widget>[
              Theme(
                data: ThemeData(unselectedWidgetColor: ColorConfig.green2),
                child: Checkbox(
                  activeColor: ColorConfig.green2,
                  onChanged: (val) {
                    CardChecklistItemModel updated = cardChecklistItemModel
                        .rebuild((cardChecklistItemModel) =>
                            cardChecklistItemModel..checked = val);

                    updateCardChecklistItemModel(updated);
                  },
                  value: cardChecklistItemModel.checked,
                ),
              ),
              Expanded(
                child: Container(
                  height: 15,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Text(
                        cardChecklistItemModel.name ?? 'Untitled',
                        style: TextStyle(
                            color: ColorConfig.black2,
                            fontSize: 13,
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
              ),
              // Spacer(),
              RawMaterialButton(
                constraints: BoxConstraints(),
                padding: EdgeInsets.all(12),
                shape: CircleBorder(),
                onPressed: () {
                  deleteCardChecklistItemModel(cardChecklistItemModel);
                },
                child: ImageIcon(
                  AssetImage(LocalImages.trashAlt),
                  color: ColorConfig.red1,
                  size: 14,
                ),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
