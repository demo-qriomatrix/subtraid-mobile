import 'package:Subtraid/features/shared/widgets/buttons.dart';
import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class CreateSubtaskWidget extends StatefulWidget {
  final Function(String) onCreate;
  final VoidCallback onCancel;

  CreateSubtaskWidget({
    @required this.onCreate,
    @required this.onCancel,
  });

  @override
  _CreateSubtaskWidgetState createState() => _CreateSubtaskWidgetState();
}

class _CreateSubtaskWidgetState extends State<CreateSubtaskWidget> {
  TextEditingController cardTitleTextEditingController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: cardTitleTextEditingController,
                  decoration: InputDecoration(
                      fillColor: ColorConfig.blue1,
                      filled: true,
                      contentPadding: EdgeInsets.only(left: 15),
                      hintText: 'What need to be done',
                      hintStyle:
                          TextStyle(fontSize: 14, color: ColorConfig.black1),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none)),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    CancelButton(
                      onPress: () {
                        widget.onCancel();
                      },
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    SaveButton('Save', () {
                      widget.onCreate(cardTitleTextEditingController.text);
                    })
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
