library shift_card_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'shift_card_model.g.dart';

abstract class ShiftCardModel
    implements Built<ShiftCardModel, ShiftCardModelBuilder> {
  @nullable
  String get projectId;

  @nullable
  String get cardId;

  @nullable
  String get newListId;

  ShiftCardModel._();

  factory ShiftCardModel([updates(ShiftCardModelBuilder b)]) = _$ShiftCardModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ShiftCardModel.serializer, this));
  }

  static ShiftCardModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ShiftCardModel.serializer, json.decode(jsonString));
  }

  static Serializer<ShiftCardModel> get serializer =>
      _$shiftCardModelSerializer;
}
