// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_card_label_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddCardLabelModel> _$addCardLabelModelSerializer =
    new _$AddCardLabelModelSerializer();

class _$AddCardLabelModelSerializer
    implements StructuredSerializer<AddCardLabelModel> {
  @override
  final Iterable<Type> types = const [AddCardLabelModel, _$AddCardLabelModel];
  @override
  final String wireName = 'AddCardLabelModel';

  @override
  Iterable<Object> serialize(Serializers serializers, AddCardLabelModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.newLabel != null) {
      result
        ..add('newLabel')
        ..add(serializers.serialize(object.newLabel,
            specifiedType: const FullType(ProjectLabelModel)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AddCardLabelModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddCardLabelModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'newLabel':
          result.newLabel.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectLabelModel))
              as ProjectLabelModel);
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AddCardLabelModel extends AddCardLabelModel {
  @override
  final ProjectLabelModel newLabel;
  @override
  final String projectId;

  factory _$AddCardLabelModel(
          [void Function(AddCardLabelModelBuilder) updates]) =>
      (new AddCardLabelModelBuilder()..update(updates)).build();

  _$AddCardLabelModel._({this.newLabel, this.projectId}) : super._();

  @override
  AddCardLabelModel rebuild(void Function(AddCardLabelModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddCardLabelModelBuilder toBuilder() =>
      new AddCardLabelModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddCardLabelModel &&
        newLabel == other.newLabel &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, newLabel.hashCode), projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddCardLabelModel')
          ..add('newLabel', newLabel)
          ..add('projectId', projectId))
        .toString();
  }
}

class AddCardLabelModelBuilder
    implements Builder<AddCardLabelModel, AddCardLabelModelBuilder> {
  _$AddCardLabelModel _$v;

  ProjectLabelModelBuilder _newLabel;
  ProjectLabelModelBuilder get newLabel =>
      _$this._newLabel ??= new ProjectLabelModelBuilder();
  set newLabel(ProjectLabelModelBuilder newLabel) =>
      _$this._newLabel = newLabel;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  AddCardLabelModelBuilder();

  AddCardLabelModelBuilder get _$this {
    if (_$v != null) {
      _newLabel = _$v.newLabel?.toBuilder();
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddCardLabelModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddCardLabelModel;
  }

  @override
  void update(void Function(AddCardLabelModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddCardLabelModel build() {
    _$AddCardLabelModel _$result;
    try {
      _$result = _$v ??
          new _$AddCardLabelModel._(
              newLabel: _newLabel?.build(), projectId: projectId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'newLabel';
        _newLabel?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AddCardLabelModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
