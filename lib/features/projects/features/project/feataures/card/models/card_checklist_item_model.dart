library card_checklist_item_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'card_checklist_item_model.g.dart';

abstract class CardChecklistItemModel
    implements Built<CardChecklistItemModel, CardChecklistItemModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  bool get checked;

  @nullable
  String get dateCreated;

  CardChecklistItemModel._();

  factory CardChecklistItemModel([updates(CardChecklistItemModelBuilder b)]) =
      _$CardChecklistItemModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardChecklistItemModel.serializer, this));
  }

  static CardChecklistItemModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardChecklistItemModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardChecklistItemModel> get serializer =>
      _$cardChecklistItemModelSerializer;
}
