// GENERATED CODE - DO NOT MODIFY BY HAND

part of update_card_label_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UpdateCardLabelModel> _$updateCardLabelModelSerializer =
    new _$UpdateCardLabelModelSerializer();

class _$UpdateCardLabelModelSerializer
    implements StructuredSerializer<UpdateCardLabelModel> {
  @override
  final Iterable<Type> types = const [
    UpdateCardLabelModel,
    _$UpdateCardLabelModel
  ];
  @override
  final String wireName = 'UpdateCardLabelModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, UpdateCardLabelModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.label != null) {
      result
        ..add('label')
        ..add(serializers.serialize(object.label,
            specifiedType: const FullType(ProjectLabelModel)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UpdateCardLabelModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UpdateCardLabelModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'label':
          result.label.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectLabelModel))
              as ProjectLabelModel);
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UpdateCardLabelModel extends UpdateCardLabelModel {
  @override
  final ProjectLabelModel label;
  @override
  final String projectId;

  factory _$UpdateCardLabelModel(
          [void Function(UpdateCardLabelModelBuilder) updates]) =>
      (new UpdateCardLabelModelBuilder()..update(updates)).build();

  _$UpdateCardLabelModel._({this.label, this.projectId}) : super._();

  @override
  UpdateCardLabelModel rebuild(
          void Function(UpdateCardLabelModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateCardLabelModelBuilder toBuilder() =>
      new UpdateCardLabelModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateCardLabelModel &&
        label == other.label &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, label.hashCode), projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateCardLabelModel')
          ..add('label', label)
          ..add('projectId', projectId))
        .toString();
  }
}

class UpdateCardLabelModelBuilder
    implements Builder<UpdateCardLabelModel, UpdateCardLabelModelBuilder> {
  _$UpdateCardLabelModel _$v;

  ProjectLabelModelBuilder _label;
  ProjectLabelModelBuilder get label =>
      _$this._label ??= new ProjectLabelModelBuilder();
  set label(ProjectLabelModelBuilder label) => _$this._label = label;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  UpdateCardLabelModelBuilder();

  UpdateCardLabelModelBuilder get _$this {
    if (_$v != null) {
      _label = _$v.label?.toBuilder();
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateCardLabelModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UpdateCardLabelModel;
  }

  @override
  void update(void Function(UpdateCardLabelModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateCardLabelModel build() {
    _$UpdateCardLabelModel _$result;
    try {
      _$result = _$v ??
          new _$UpdateCardLabelModel._(
              label: _label?.build(), projectId: projectId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'label';
        _label?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UpdateCardLabelModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
