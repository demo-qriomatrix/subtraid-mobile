// GENERATED CODE - DO NOT MODIFY BY HAND

part of shift_card_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ShiftCardModel> _$shiftCardModelSerializer =
    new _$ShiftCardModelSerializer();

class _$ShiftCardModelSerializer
    implements StructuredSerializer<ShiftCardModel> {
  @override
  final Iterable<Type> types = const [ShiftCardModel, _$ShiftCardModel];
  @override
  final String wireName = 'ShiftCardModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ShiftCardModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.newListId != null) {
      result
        ..add('newListId')
        ..add(serializers.serialize(object.newListId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ShiftCardModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ShiftCardModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'newListId':
          result.newListId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ShiftCardModel extends ShiftCardModel {
  @override
  final String projectId;
  @override
  final String cardId;
  @override
  final String newListId;

  factory _$ShiftCardModel([void Function(ShiftCardModelBuilder) updates]) =>
      (new ShiftCardModelBuilder()..update(updates)).build();

  _$ShiftCardModel._({this.projectId, this.cardId, this.newListId}) : super._();

  @override
  ShiftCardModel rebuild(void Function(ShiftCardModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ShiftCardModelBuilder toBuilder() =>
      new ShiftCardModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ShiftCardModel &&
        projectId == other.projectId &&
        cardId == other.cardId &&
        newListId == other.newListId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, projectId.hashCode), cardId.hashCode), newListId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ShiftCardModel')
          ..add('projectId', projectId)
          ..add('cardId', cardId)
          ..add('newListId', newListId))
        .toString();
  }
}

class ShiftCardModelBuilder
    implements Builder<ShiftCardModel, ShiftCardModelBuilder> {
  _$ShiftCardModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  String _newListId;
  String get newListId => _$this._newListId;
  set newListId(String newListId) => _$this._newListId = newListId;

  ShiftCardModelBuilder();

  ShiftCardModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _cardId = _$v.cardId;
      _newListId = _$v.newListId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ShiftCardModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ShiftCardModel;
  }

  @override
  void update(void Function(ShiftCardModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ShiftCardModel build() {
    final _$result = _$v ??
        new _$ShiftCardModel._(
            projectId: projectId, cardId: cardId, newListId: newListId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
