// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_attachment_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardAttachmentModel> _$cardAttachmentModelSerializer =
    new _$CardAttachmentModelSerializer();

class _$CardAttachmentModelSerializer
    implements StructuredSerializer<CardAttachmentModel> {
  @override
  final Iterable<Type> types = const [
    CardAttachmentModel,
    _$CardAttachmentModel
  ];
  @override
  final String wireName = 'CardAttachmentModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardAttachmentModel object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object>[];
  }

  @override
  CardAttachmentModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new CardAttachmentModelBuilder().build();
  }
}

class _$CardAttachmentModel extends CardAttachmentModel {
  factory _$CardAttachmentModel(
          [void Function(CardAttachmentModelBuilder) updates]) =>
      (new CardAttachmentModelBuilder()..update(updates)).build();

  _$CardAttachmentModel._() : super._();

  @override
  CardAttachmentModel rebuild(
          void Function(CardAttachmentModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardAttachmentModelBuilder toBuilder() =>
      new CardAttachmentModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardAttachmentModel;
  }

  @override
  int get hashCode {
    return 363275996;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('CardAttachmentModel').toString();
  }
}

class CardAttachmentModelBuilder
    implements Builder<CardAttachmentModel, CardAttachmentModelBuilder> {
  _$CardAttachmentModel _$v;

  CardAttachmentModelBuilder();

  @override
  void replace(CardAttachmentModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardAttachmentModel;
  }

  @override
  void update(void Function(CardAttachmentModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardAttachmentModel build() {
    final _$result = _$v ?? new _$CardAttachmentModel._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
