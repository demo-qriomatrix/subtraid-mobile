// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_add_checklist_item_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardAddChecklistItemModel> _$cardAddChecklistItemModelSerializer =
    new _$CardAddChecklistItemModelSerializer();

class _$CardAddChecklistItemModelSerializer
    implements StructuredSerializer<CardAddChecklistItemModel> {
  @override
  final Iterable<Type> types = const [
    CardAddChecklistItemModel,
    _$CardAddChecklistItemModel
  ];
  @override
  final String wireName = 'CardAddChecklistItemModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardAddChecklistItemModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.checkListId != null) {
      result
        ..add('checkListId')
        ..add(serializers.serialize(object.checkListId,
            specifiedType: const FullType(String)));
    }
    if (object.newCheckItem != null) {
      result
        ..add('newCheckItem')
        ..add(serializers.serialize(object.newCheckItem,
            specifiedType: const FullType(CardChecklistItemModel)));
    }
    return result;
  }

  @override
  CardAddChecklistItemModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardAddChecklistItemModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'checkListId':
          result.checkListId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'newCheckItem':
          result.newCheckItem.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardChecklistItemModel))
              as CardChecklistItemModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CardAddChecklistItemModel extends CardAddChecklistItemModel {
  @override
  final String projectId;
  @override
  final String cardId;
  @override
  final String checkListId;
  @override
  final CardChecklistItemModel newCheckItem;

  factory _$CardAddChecklistItemModel(
          [void Function(CardAddChecklistItemModelBuilder) updates]) =>
      (new CardAddChecklistItemModelBuilder()..update(updates)).build();

  _$CardAddChecklistItemModel._(
      {this.projectId, this.cardId, this.checkListId, this.newCheckItem})
      : super._();

  @override
  CardAddChecklistItemModel rebuild(
          void Function(CardAddChecklistItemModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardAddChecklistItemModelBuilder toBuilder() =>
      new CardAddChecklistItemModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardAddChecklistItemModel &&
        projectId == other.projectId &&
        cardId == other.cardId &&
        checkListId == other.checkListId &&
        newCheckItem == other.newCheckItem;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, projectId.hashCode), cardId.hashCode),
            checkListId.hashCode),
        newCheckItem.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardAddChecklistItemModel')
          ..add('projectId', projectId)
          ..add('cardId', cardId)
          ..add('checkListId', checkListId)
          ..add('newCheckItem', newCheckItem))
        .toString();
  }
}

class CardAddChecklistItemModelBuilder
    implements
        Builder<CardAddChecklistItemModel, CardAddChecklistItemModelBuilder> {
  _$CardAddChecklistItemModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  String _checkListId;
  String get checkListId => _$this._checkListId;
  set checkListId(String checkListId) => _$this._checkListId = checkListId;

  CardChecklistItemModelBuilder _newCheckItem;
  CardChecklistItemModelBuilder get newCheckItem =>
      _$this._newCheckItem ??= new CardChecklistItemModelBuilder();
  set newCheckItem(CardChecklistItemModelBuilder newCheckItem) =>
      _$this._newCheckItem = newCheckItem;

  CardAddChecklistItemModelBuilder();

  CardAddChecklistItemModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _cardId = _$v.cardId;
      _checkListId = _$v.checkListId;
      _newCheckItem = _$v.newCheckItem?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardAddChecklistItemModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardAddChecklistItemModel;
  }

  @override
  void update(void Function(CardAddChecklistItemModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardAddChecklistItemModel build() {
    _$CardAddChecklistItemModel _$result;
    try {
      _$result = _$v ??
          new _$CardAddChecklistItemModel._(
              projectId: projectId,
              cardId: cardId,
              checkListId: checkListId,
              newCheckItem: _newCheckItem?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'newCheckItem';
        _newCheckItem?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardAddChecklistItemModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
