library card_update_checklist_item_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'card_checklist_model.dart';

part 'card_update_checklist_item_model.g.dart';

abstract class CardUpdateChecklistItemModel
    implements
        Built<CardUpdateChecklistItemModel,
            CardUpdateChecklistItemModelBuilder> {
  @nullable
  String get cardId;
  @nullable
  int get checkItemsCheckedCount;

  @nullable
  int get checkItemsCount;

  @nullable
  String get projectId;

  @nullable
  CardChecklistModel get updatedList;

  CardUpdateChecklistItemModel._();

  factory CardUpdateChecklistItemModel(
          [updates(CardUpdateChecklistItemModelBuilder b)]) =
      _$CardUpdateChecklistItemModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        CardUpdateChecklistItemModel.serializer, this));
  }

  static CardUpdateChecklistItemModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardUpdateChecklistItemModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardUpdateChecklistItemModel> get serializer =>
      _$cardUpdateChecklistItemModelSerializer;
}
