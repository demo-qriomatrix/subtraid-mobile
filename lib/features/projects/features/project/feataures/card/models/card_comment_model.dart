library card_comment_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';

part 'card_comment_model.g.dart';

abstract class CardCommentModel
    implements Built<CardCommentModel, CardCommentModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get dateCreated;

  @nullable
  MemberModel get idMember;

  @nullable
  String get message;

  @nullable
  String get time;

  CardCommentModel._();

  factory CardCommentModel([updates(CardCommentModelBuilder b)]) =
      _$CardCommentModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CardCommentModel.serializer, this));
  }

  static CardCommentModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardCommentModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardCommentModel> get serializer =>
      _$cardCommentModelSerializer;
}
