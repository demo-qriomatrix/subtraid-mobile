// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_checklist_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardChecklistModel> _$cardChecklistModelSerializer =
    new _$CardChecklistModelSerializer();

class _$CardChecklistModelSerializer
    implements StructuredSerializer<CardChecklistModel> {
  @override
  final Iterable<Type> types = const [CardChecklistModel, _$CardChecklistModel];
  @override
  final String wireName = 'CardChecklistModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CardChecklistModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.checkItemsChecked != null) {
      result
        ..add('checkItemsChecked')
        ..add(serializers.serialize(object.checkItemsChecked,
            specifiedType: const FullType(int)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.checkItems != null) {
      result
        ..add('checkItems')
        ..add(serializers.serialize(object.checkItems,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CardChecklistItemModel)])));
    }
    return result;
  }

  @override
  CardChecklistModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardChecklistModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'checkItemsChecked':
          result.checkItemsChecked = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'checkItems':
          result.checkItems.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(CardChecklistItemModel)
              ])) as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$CardChecklistModel extends CardChecklistModel {
  @override
  final int checkItemsChecked;
  @override
  final String name;
  @override
  final String id;
  @override
  final String dateCreated;
  @override
  final BuiltList<CardChecklistItemModel> checkItems;

  factory _$CardChecklistModel(
          [void Function(CardChecklistModelBuilder) updates]) =>
      (new CardChecklistModelBuilder()..update(updates)).build();

  _$CardChecklistModel._(
      {this.checkItemsChecked,
      this.name,
      this.id,
      this.dateCreated,
      this.checkItems})
      : super._();

  @override
  CardChecklistModel rebuild(
          void Function(CardChecklistModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardChecklistModelBuilder toBuilder() =>
      new CardChecklistModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardChecklistModel &&
        checkItemsChecked == other.checkItemsChecked &&
        name == other.name &&
        id == other.id &&
        dateCreated == other.dateCreated &&
        checkItems == other.checkItems;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, checkItemsChecked.hashCode), name.hashCode),
                id.hashCode),
            dateCreated.hashCode),
        checkItems.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardChecklistModel')
          ..add('checkItemsChecked', checkItemsChecked)
          ..add('name', name)
          ..add('id', id)
          ..add('dateCreated', dateCreated)
          ..add('checkItems', checkItems))
        .toString();
  }
}

class CardChecklistModelBuilder
    implements Builder<CardChecklistModel, CardChecklistModelBuilder> {
  _$CardChecklistModel _$v;

  int _checkItemsChecked;
  int get checkItemsChecked => _$this._checkItemsChecked;
  set checkItemsChecked(int checkItemsChecked) =>
      _$this._checkItemsChecked = checkItemsChecked;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  ListBuilder<CardChecklistItemModel> _checkItems;
  ListBuilder<CardChecklistItemModel> get checkItems =>
      _$this._checkItems ??= new ListBuilder<CardChecklistItemModel>();
  set checkItems(ListBuilder<CardChecklistItemModel> checkItems) =>
      _$this._checkItems = checkItems;

  CardChecklistModelBuilder();

  CardChecklistModelBuilder get _$this {
    if (_$v != null) {
      _checkItemsChecked = _$v.checkItemsChecked;
      _name = _$v.name;
      _id = _$v.id;
      _dateCreated = _$v.dateCreated;
      _checkItems = _$v.checkItems?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardChecklistModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardChecklistModel;
  }

  @override
  void update(void Function(CardChecklistModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardChecklistModel build() {
    _$CardChecklistModel _$result;
    try {
      _$result = _$v ??
          new _$CardChecklistModel._(
              checkItemsChecked: checkItemsChecked,
              name: name,
              id: id,
              dateCreated: dateCreated,
              checkItems: _checkItems?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'checkItems';
        _checkItems?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardChecklistModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
