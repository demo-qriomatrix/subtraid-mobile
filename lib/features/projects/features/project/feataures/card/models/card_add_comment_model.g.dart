// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_add_comment_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardAddCommentModel> _$cardAddCommentModelSerializer =
    new _$CardAddCommentModelSerializer();

class _$CardAddCommentModelSerializer
    implements StructuredSerializer<CardAddCommentModel> {
  @override
  final Iterable<Type> types = const [
    CardAddCommentModel,
    _$CardAddCommentModel
  ];
  @override
  final String wireName = 'CardAddCommentModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardAddCommentModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idMember != null) {
      result
        ..add('idMember')
        ..add(serializers.serialize(object.idMember,
            specifiedType: const FullType(String)));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    if (object.time != null) {
      result
        ..add('time')
        ..add(serializers.serialize(object.time,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CardAddCommentModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardAddCommentModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'idMember':
          result.idMember = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'time':
          result.time = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CardAddCommentModel extends CardAddCommentModel {
  @override
  final String idMember;
  @override
  final String img;
  @override
  final String message;
  @override
  final String time;

  factory _$CardAddCommentModel(
          [void Function(CardAddCommentModelBuilder) updates]) =>
      (new CardAddCommentModelBuilder()..update(updates)).build();

  _$CardAddCommentModel._({this.idMember, this.img, this.message, this.time})
      : super._();

  @override
  CardAddCommentModel rebuild(
          void Function(CardAddCommentModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardAddCommentModelBuilder toBuilder() =>
      new CardAddCommentModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardAddCommentModel &&
        idMember == other.idMember &&
        img == other.img &&
        message == other.message &&
        time == other.time;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, idMember.hashCode), img.hashCode), message.hashCode),
        time.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardAddCommentModel')
          ..add('idMember', idMember)
          ..add('img', img)
          ..add('message', message)
          ..add('time', time))
        .toString();
  }
}

class CardAddCommentModelBuilder
    implements Builder<CardAddCommentModel, CardAddCommentModelBuilder> {
  _$CardAddCommentModel _$v;

  String _idMember;
  String get idMember => _$this._idMember;
  set idMember(String idMember) => _$this._idMember = idMember;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  String _time;
  String get time => _$this._time;
  set time(String time) => _$this._time = time;

  CardAddCommentModelBuilder();

  CardAddCommentModelBuilder get _$this {
    if (_$v != null) {
      _idMember = _$v.idMember;
      _img = _$v.img;
      _message = _$v.message;
      _time = _$v.time;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardAddCommentModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardAddCommentModel;
  }

  @override
  void update(void Function(CardAddCommentModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardAddCommentModel build() {
    final _$result = _$v ??
        new _$CardAddCommentModel._(
            idMember: idMember, img: img, message: message, time: time);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
