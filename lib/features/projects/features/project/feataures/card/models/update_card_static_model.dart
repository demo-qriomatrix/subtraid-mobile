library update_card_static_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';

part 'update_card_static_model.g.dart';

abstract class UpdateCardStaticModel
    implements Built<UpdateCardStaticModel, UpdateCardStaticModelBuilder> {
  @nullable
  String get projectId;
  @nullable
  ProjectCardModel get updatedCard;

  UpdateCardStaticModel._();

  factory UpdateCardStaticModel([updates(UpdateCardStaticModelBuilder b)]) =
      _$UpdateCardStaticModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(UpdateCardStaticModel.serializer, this));
  }

  static UpdateCardStaticModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UpdateCardStaticModel.serializer, json.decode(jsonString));
  }

  static Serializer<UpdateCardStaticModel> get serializer =>
      _$updateCardStaticModelSerializer;
}
