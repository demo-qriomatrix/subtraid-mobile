// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_update_checklist_item_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardUpdateChecklistItemModel>
    _$cardUpdateChecklistItemModelSerializer =
    new _$CardUpdateChecklistItemModelSerializer();

class _$CardUpdateChecklistItemModelSerializer
    implements StructuredSerializer<CardUpdateChecklistItemModel> {
  @override
  final Iterable<Type> types = const [
    CardUpdateChecklistItemModel,
    _$CardUpdateChecklistItemModel
  ];
  @override
  final String wireName = 'CardUpdateChecklistItemModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardUpdateChecklistItemModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.checkItemsCheckedCount != null) {
      result
        ..add('checkItemsCheckedCount')
        ..add(serializers.serialize(object.checkItemsCheckedCount,
            specifiedType: const FullType(int)));
    }
    if (object.checkItemsCount != null) {
      result
        ..add('checkItemsCount')
        ..add(serializers.serialize(object.checkItemsCount,
            specifiedType: const FullType(int)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.updatedList != null) {
      result
        ..add('updatedList')
        ..add(serializers.serialize(object.updatedList,
            specifiedType: const FullType(CardChecklistModel)));
    }
    return result;
  }

  @override
  CardUpdateChecklistItemModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardUpdateChecklistItemModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'checkItemsCheckedCount':
          result.checkItemsCheckedCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'checkItemsCount':
          result.checkItemsCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updatedList':
          result.updatedList.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardChecklistModel))
              as CardChecklistModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CardUpdateChecklistItemModel extends CardUpdateChecklistItemModel {
  @override
  final String cardId;
  @override
  final int checkItemsCheckedCount;
  @override
  final int checkItemsCount;
  @override
  final String projectId;
  @override
  final CardChecklistModel updatedList;

  factory _$CardUpdateChecklistItemModel(
          [void Function(CardUpdateChecklistItemModelBuilder) updates]) =>
      (new CardUpdateChecklistItemModelBuilder()..update(updates)).build();

  _$CardUpdateChecklistItemModel._(
      {this.cardId,
      this.checkItemsCheckedCount,
      this.checkItemsCount,
      this.projectId,
      this.updatedList})
      : super._();

  @override
  CardUpdateChecklistItemModel rebuild(
          void Function(CardUpdateChecklistItemModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardUpdateChecklistItemModelBuilder toBuilder() =>
      new CardUpdateChecklistItemModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardUpdateChecklistItemModel &&
        cardId == other.cardId &&
        checkItemsCheckedCount == other.checkItemsCheckedCount &&
        checkItemsCount == other.checkItemsCount &&
        projectId == other.projectId &&
        updatedList == other.updatedList;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, cardId.hashCode), checkItemsCheckedCount.hashCode),
                checkItemsCount.hashCode),
            projectId.hashCode),
        updatedList.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardUpdateChecklistItemModel')
          ..add('cardId', cardId)
          ..add('checkItemsCheckedCount', checkItemsCheckedCount)
          ..add('checkItemsCount', checkItemsCount)
          ..add('projectId', projectId)
          ..add('updatedList', updatedList))
        .toString();
  }
}

class CardUpdateChecklistItemModelBuilder
    implements
        Builder<CardUpdateChecklistItemModel,
            CardUpdateChecklistItemModelBuilder> {
  _$CardUpdateChecklistItemModel _$v;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  int _checkItemsCheckedCount;
  int get checkItemsCheckedCount => _$this._checkItemsCheckedCount;
  set checkItemsCheckedCount(int checkItemsCheckedCount) =>
      _$this._checkItemsCheckedCount = checkItemsCheckedCount;

  int _checkItemsCount;
  int get checkItemsCount => _$this._checkItemsCount;
  set checkItemsCount(int checkItemsCount) =>
      _$this._checkItemsCount = checkItemsCount;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  CardChecklistModelBuilder _updatedList;
  CardChecklistModelBuilder get updatedList =>
      _$this._updatedList ??= new CardChecklistModelBuilder();
  set updatedList(CardChecklistModelBuilder updatedList) =>
      _$this._updatedList = updatedList;

  CardUpdateChecklistItemModelBuilder();

  CardUpdateChecklistItemModelBuilder get _$this {
    if (_$v != null) {
      _cardId = _$v.cardId;
      _checkItemsCheckedCount = _$v.checkItemsCheckedCount;
      _checkItemsCount = _$v.checkItemsCount;
      _projectId = _$v.projectId;
      _updatedList = _$v.updatedList?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardUpdateChecklistItemModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardUpdateChecklistItemModel;
  }

  @override
  void update(void Function(CardUpdateChecklistItemModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardUpdateChecklistItemModel build() {
    _$CardUpdateChecklistItemModel _$result;
    try {
      _$result = _$v ??
          new _$CardUpdateChecklistItemModel._(
              cardId: cardId,
              checkItemsCheckedCount: checkItemsCheckedCount,
              checkItemsCount: checkItemsCount,
              projectId: projectId,
              updatedList: _updatedList?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'updatedList';
        _updatedList?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardUpdateChecklistItemModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
