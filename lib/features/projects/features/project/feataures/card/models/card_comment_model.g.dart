// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_comment_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardCommentModel> _$cardCommentModelSerializer =
    new _$CardCommentModelSerializer();

class _$CardCommentModelSerializer
    implements StructuredSerializer<CardCommentModel> {
  @override
  final Iterable<Type> types = const [CardCommentModel, _$CardCommentModel];
  @override
  final String wireName = 'CardCommentModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CardCommentModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.idMember != null) {
      result
        ..add('idMember')
        ..add(serializers.serialize(object.idMember,
            specifiedType: const FullType(MemberModel)));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    if (object.time != null) {
      result
        ..add('time')
        ..add(serializers.serialize(object.time,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CardCommentModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardCommentModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'idMember':
          result.idMember.replace(serializers.deserialize(value,
              specifiedType: const FullType(MemberModel)) as MemberModel);
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'time':
          result.time = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CardCommentModel extends CardCommentModel {
  @override
  final String id;
  @override
  final String dateCreated;
  @override
  final MemberModel idMember;
  @override
  final String message;
  @override
  final String time;

  factory _$CardCommentModel(
          [void Function(CardCommentModelBuilder) updates]) =>
      (new CardCommentModelBuilder()..update(updates)).build();

  _$CardCommentModel._(
      {this.id, this.dateCreated, this.idMember, this.message, this.time})
      : super._();

  @override
  CardCommentModel rebuild(void Function(CardCommentModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardCommentModelBuilder toBuilder() =>
      new CardCommentModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardCommentModel &&
        id == other.id &&
        dateCreated == other.dateCreated &&
        idMember == other.idMember &&
        message == other.message &&
        time == other.time;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, id.hashCode), dateCreated.hashCode),
                idMember.hashCode),
            message.hashCode),
        time.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardCommentModel')
          ..add('id', id)
          ..add('dateCreated', dateCreated)
          ..add('idMember', idMember)
          ..add('message', message)
          ..add('time', time))
        .toString();
  }
}

class CardCommentModelBuilder
    implements Builder<CardCommentModel, CardCommentModelBuilder> {
  _$CardCommentModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  MemberModelBuilder _idMember;
  MemberModelBuilder get idMember =>
      _$this._idMember ??= new MemberModelBuilder();
  set idMember(MemberModelBuilder idMember) => _$this._idMember = idMember;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  String _time;
  String get time => _$this._time;
  set time(String time) => _$this._time = time;

  CardCommentModelBuilder();

  CardCommentModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _dateCreated = _$v.dateCreated;
      _idMember = _$v.idMember?.toBuilder();
      _message = _$v.message;
      _time = _$v.time;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardCommentModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardCommentModel;
  }

  @override
  void update(void Function(CardCommentModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardCommentModel build() {
    _$CardCommentModel _$result;
    try {
      _$result = _$v ??
          new _$CardCommentModel._(
              id: id,
              dateCreated: dateCreated,
              idMember: _idMember?.build(),
              message: message,
              time: time);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'idMember';
        _idMember?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardCommentModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
