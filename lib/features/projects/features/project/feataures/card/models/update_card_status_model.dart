library update_card_status_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'update_card_status_model.g.dart';

abstract class UpdateCardStatusModel
    implements Built<UpdateCardStatusModel, UpdateCardStatusModelBuilder> {
  @nullable
  String get cardId;

  @nullable
  String get projectId;

  @nullable
  String get status;

  UpdateCardStatusModel._();

  factory UpdateCardStatusModel([updates(UpdateCardStatusModelBuilder b)]) =
      _$UpdateCardStatusModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(UpdateCardStatusModel.serializer, this));
  }

  static UpdateCardStatusModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UpdateCardStatusModel.serializer, json.decode(jsonString));
  }

  static Serializer<UpdateCardStatusModel> get serializer =>
      _$updateCardStatusModelSerializer;
}
