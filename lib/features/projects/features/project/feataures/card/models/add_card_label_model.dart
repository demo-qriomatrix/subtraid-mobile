library add_card_label_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/models/project_label_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'add_card_label_model.g.dart';

abstract class AddCardLabelModel
    implements Built<AddCardLabelModel, AddCardLabelModelBuilder> {
  @nullable
  ProjectLabelModel get newLabel;

  @nullable
  String get projectId;

  AddCardLabelModel._();

  factory AddCardLabelModel([updates(AddCardLabelModelBuilder b)]) =
      _$AddCardLabelModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(AddCardLabelModel.serializer, this));
  }

  static AddCardLabelModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddCardLabelModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddCardLabelModel> get serializer =>
      _$addCardLabelModelSerializer;
}
