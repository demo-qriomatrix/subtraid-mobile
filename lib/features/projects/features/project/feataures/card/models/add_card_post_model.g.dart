// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_card_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddCardPostModel> _$addCardPostModelSerializer =
    new _$AddCardPostModelSerializer();

class _$AddCardPostModelSerializer
    implements StructuredSerializer<AddCardPostModel> {
  @override
  final Iterable<Type> types = const [AddCardPostModel, _$AddCardPostModel];
  @override
  final String wireName = 'AddCardPostModel';

  @override
  Iterable<Object> serialize(Serializers serializers, AddCardPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.listId != null) {
      result
        ..add('listId')
        ..add(serializers.serialize(object.listId,
            specifiedType: const FullType(String)));
    }
    if (object.newCard != null) {
      result
        ..add('newCard')
        ..add(serializers.serialize(object.newCard,
            specifiedType: const FullType(ProjectCardModel)));
    }
    return result;
  }

  @override
  AddCardPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddCardPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'listId':
          result.listId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'newCard':
          result.newCard.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectCardModel))
              as ProjectCardModel);
          break;
      }
    }

    return result.build();
  }
}

class _$AddCardPostModel extends AddCardPostModel {
  @override
  final String projectId;
  @override
  final String listId;
  @override
  final ProjectCardModel newCard;

  factory _$AddCardPostModel(
          [void Function(AddCardPostModelBuilder) updates]) =>
      (new AddCardPostModelBuilder()..update(updates)).build();

  _$AddCardPostModel._({this.projectId, this.listId, this.newCard}) : super._();

  @override
  AddCardPostModel rebuild(void Function(AddCardPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddCardPostModelBuilder toBuilder() =>
      new AddCardPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddCardPostModel &&
        projectId == other.projectId &&
        listId == other.listId &&
        newCard == other.newCard;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, projectId.hashCode), listId.hashCode), newCard.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddCardPostModel')
          ..add('projectId', projectId)
          ..add('listId', listId)
          ..add('newCard', newCard))
        .toString();
  }
}

class AddCardPostModelBuilder
    implements Builder<AddCardPostModel, AddCardPostModelBuilder> {
  _$AddCardPostModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _listId;
  String get listId => _$this._listId;
  set listId(String listId) => _$this._listId = listId;

  ProjectCardModelBuilder _newCard;
  ProjectCardModelBuilder get newCard =>
      _$this._newCard ??= new ProjectCardModelBuilder();
  set newCard(ProjectCardModelBuilder newCard) => _$this._newCard = newCard;

  AddCardPostModelBuilder();

  AddCardPostModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _listId = _$v.listId;
      _newCard = _$v.newCard?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddCardPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddCardPostModel;
  }

  @override
  void update(void Function(AddCardPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddCardPostModel build() {
    _$AddCardPostModel _$result;
    try {
      _$result = _$v ??
          new _$AddCardPostModel._(
              projectId: projectId, listId: listId, newCard: _newCard?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'newCard';
        _newCard?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AddCardPostModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
