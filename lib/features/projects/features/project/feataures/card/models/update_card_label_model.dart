library update_card_label_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/models/project_label_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'update_card_label_model.g.dart';

abstract class UpdateCardLabelModel
    implements Built<UpdateCardLabelModel, UpdateCardLabelModelBuilder> {
  @nullable
  ProjectLabelModel get label;

  @nullable
  String get projectId;

  UpdateCardLabelModel._();

  factory UpdateCardLabelModel([updates(UpdateCardLabelModelBuilder b)]) =
      _$UpdateCardLabelModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(UpdateCardLabelModel.serializer, this));
  }

  static UpdateCardLabelModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UpdateCardLabelModel.serializer, json.decode(jsonString));
  }

  static Serializer<UpdateCardLabelModel> get serializer =>
      _$updateCardLabelModelSerializer;
}
