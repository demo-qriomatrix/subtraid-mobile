library card_add_checklist_item_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'card_checklist_item_model.dart';

part 'card_add_checklist_item_model.g.dart';

abstract class CardAddChecklistItemModel
    implements
        Built<CardAddChecklistItemModel, CardAddChecklistItemModelBuilder> {
  @nullable
  String get projectId;

  @nullable
  String get cardId;

  @nullable
  String get checkListId;

  @nullable
  CardChecklistItemModel get newCheckItem;

  CardAddChecklistItemModel._();

  factory CardAddChecklistItemModel(
          [updates(CardAddChecklistItemModelBuilder b)]) =
      _$CardAddChecklistItemModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardAddChecklistItemModel.serializer, this));
  }

  static CardAddChecklistItemModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardAddChecklistItemModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardAddChecklistItemModel> get serializer =>
      _$cardAddChecklistItemModelSerializer;
}
