library card_add_comment_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'card_add_comment_model.g.dart';

abstract class CardAddCommentModel
    implements Built<CardAddCommentModel, CardAddCommentModelBuilder> {
  @nullable
  String get idMember;

  @nullable
  String get img;

  @nullable
  String get message;

  @nullable
  String get time;

  CardAddCommentModel._();

  factory CardAddCommentModel([updates(CardAddCommentModelBuilder b)]) =
      _$CardAddCommentModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardAddCommentModel.serializer, this));
  }

  static CardAddCommentModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardAddCommentModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardAddCommentModel> get serializer =>
      _$cardAddCommentModelSerializer;
}
