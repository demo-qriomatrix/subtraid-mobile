library card_add_checklist_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'card_checklist_model.dart';

part 'card_add_checklist_model.g.dart';

abstract class CardAddChecklistModel
    implements Built<CardAddChecklistModel, CardAddChecklistModelBuilder> {
  @nullable
  String get cardId;

  @nullable
  CardChecklistModel get newChecklist;

  @nullable
  String get name;

  @nullable
  String get projectId;

  CardAddChecklistModel._();

  factory CardAddChecklistModel([updates(CardAddChecklistModelBuilder b)]) =
      _$CardAddChecklistModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardAddChecklistModel.serializer, this));
  }

  static CardAddChecklistModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardAddChecklistModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardAddChecklistModel> get serializer =>
      _$cardAddChecklistModelSerializer;
}
