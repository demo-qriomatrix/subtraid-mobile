library card_attachment_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'card_attachment_model.g.dart';

abstract class CardAttachmentModel
    implements Built<CardAttachmentModel, CardAttachmentModelBuilder> {
  // fields go here

  CardAttachmentModel._();

  factory CardAttachmentModel([updates(CardAttachmentModelBuilder b)]) =
      _$CardAttachmentModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardAttachmentModel.serializer, this));
  }

  static CardAttachmentModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardAttachmentModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardAttachmentModel> get serializer =>
      _$cardAttachmentModelSerializer;
}
