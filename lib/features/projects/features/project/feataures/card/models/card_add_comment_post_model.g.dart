// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_add_comment_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardAddCommentPostModel> _$cardAddCommentPostModelSerializer =
    new _$CardAddCommentPostModelSerializer();

class _$CardAddCommentPostModelSerializer
    implements StructuredSerializer<CardAddCommentPostModel> {
  @override
  final Iterable<Type> types = const [
    CardAddCommentPostModel,
    _$CardAddCommentPostModel
  ];
  @override
  final String wireName = 'CardAddCommentPostModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardAddCommentPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.newComment != null) {
      result
        ..add('newComment')
        ..add(serializers.serialize(object.newComment,
            specifiedType: const FullType(CardAddCommentModel)));
    }
    return result;
  }

  @override
  CardAddCommentPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardAddCommentPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'newComment':
          result.newComment.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardAddCommentModel))
              as CardAddCommentModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CardAddCommentPostModel extends CardAddCommentPostModel {
  @override
  final String projectId;
  @override
  final String cardId;
  @override
  final CardAddCommentModel newComment;

  factory _$CardAddCommentPostModel(
          [void Function(CardAddCommentPostModelBuilder) updates]) =>
      (new CardAddCommentPostModelBuilder()..update(updates)).build();

  _$CardAddCommentPostModel._({this.projectId, this.cardId, this.newComment})
      : super._();

  @override
  CardAddCommentPostModel rebuild(
          void Function(CardAddCommentPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardAddCommentPostModelBuilder toBuilder() =>
      new CardAddCommentPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardAddCommentPostModel &&
        projectId == other.projectId &&
        cardId == other.cardId &&
        newComment == other.newComment;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, projectId.hashCode), cardId.hashCode), newComment.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardAddCommentPostModel')
          ..add('projectId', projectId)
          ..add('cardId', cardId)
          ..add('newComment', newComment))
        .toString();
  }
}

class CardAddCommentPostModelBuilder
    implements
        Builder<CardAddCommentPostModel, CardAddCommentPostModelBuilder> {
  _$CardAddCommentPostModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  CardAddCommentModelBuilder _newComment;
  CardAddCommentModelBuilder get newComment =>
      _$this._newComment ??= new CardAddCommentModelBuilder();
  set newComment(CardAddCommentModelBuilder newComment) =>
      _$this._newComment = newComment;

  CardAddCommentPostModelBuilder();

  CardAddCommentPostModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _cardId = _$v.cardId;
      _newComment = _$v.newComment?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardAddCommentPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardAddCommentPostModel;
  }

  @override
  void update(void Function(CardAddCommentPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardAddCommentPostModel build() {
    _$CardAddCommentPostModel _$result;
    try {
      _$result = _$v ??
          new _$CardAddCommentPostModel._(
              projectId: projectId,
              cardId: cardId,
              newComment: _newComment?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'newComment';
        _newComment?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardAddCommentPostModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
