// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_add_checklist_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardAddChecklistModel> _$cardAddChecklistModelSerializer =
    new _$CardAddChecklistModelSerializer();

class _$CardAddChecklistModelSerializer
    implements StructuredSerializer<CardAddChecklistModel> {
  @override
  final Iterable<Type> types = const [
    CardAddChecklistModel,
    _$CardAddChecklistModel
  ];
  @override
  final String wireName = 'CardAddChecklistModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardAddChecklistModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.newChecklist != null) {
      result
        ..add('newChecklist')
        ..add(serializers.serialize(object.newChecklist,
            specifiedType: const FullType(CardChecklistModel)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CardAddChecklistModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardAddChecklistModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'newChecklist':
          result.newChecklist.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardChecklistModel))
              as CardChecklistModel);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CardAddChecklistModel extends CardAddChecklistModel {
  @override
  final String cardId;
  @override
  final CardChecklistModel newChecklist;
  @override
  final String name;
  @override
  final String projectId;

  factory _$CardAddChecklistModel(
          [void Function(CardAddChecklistModelBuilder) updates]) =>
      (new CardAddChecklistModelBuilder()..update(updates)).build();

  _$CardAddChecklistModel._(
      {this.cardId, this.newChecklist, this.name, this.projectId})
      : super._();

  @override
  CardAddChecklistModel rebuild(
          void Function(CardAddChecklistModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardAddChecklistModelBuilder toBuilder() =>
      new CardAddChecklistModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardAddChecklistModel &&
        cardId == other.cardId &&
        newChecklist == other.newChecklist &&
        name == other.name &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, cardId.hashCode), newChecklist.hashCode), name.hashCode),
        projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardAddChecklistModel')
          ..add('cardId', cardId)
          ..add('newChecklist', newChecklist)
          ..add('name', name)
          ..add('projectId', projectId))
        .toString();
  }
}

class CardAddChecklistModelBuilder
    implements Builder<CardAddChecklistModel, CardAddChecklistModelBuilder> {
  _$CardAddChecklistModel _$v;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  CardChecklistModelBuilder _newChecklist;
  CardChecklistModelBuilder get newChecklist =>
      _$this._newChecklist ??= new CardChecklistModelBuilder();
  set newChecklist(CardChecklistModelBuilder newChecklist) =>
      _$this._newChecklist = newChecklist;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  CardAddChecklistModelBuilder();

  CardAddChecklistModelBuilder get _$this {
    if (_$v != null) {
      _cardId = _$v.cardId;
      _newChecklist = _$v.newChecklist?.toBuilder();
      _name = _$v.name;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardAddChecklistModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardAddChecklistModel;
  }

  @override
  void update(void Function(CardAddChecklistModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardAddChecklistModel build() {
    _$CardAddChecklistModel _$result;
    try {
      _$result = _$v ??
          new _$CardAddChecklistModel._(
              cardId: cardId,
              newChecklist: _newChecklist?.build(),
              name: name,
              projectId: projectId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'newChecklist';
        _newChecklist?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardAddChecklistModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
