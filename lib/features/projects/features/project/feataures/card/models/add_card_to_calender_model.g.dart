// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_card_to_calender_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddCardToCalenderModel> _$addCardToCalenderModelSerializer =
    new _$AddCardToCalenderModelSerializer();

class _$AddCardToCalenderModelSerializer
    implements StructuredSerializer<AddCardToCalenderModel> {
  @override
  final Iterable<Type> types = const [
    AddCardToCalenderModel,
    _$AddCardToCalenderModel
  ];
  @override
  final String wireName = 'AddCardToCalenderModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, AddCardToCalenderModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.card != null) {
      result
        ..add('card')
        ..add(serializers.serialize(object.card,
            specifiedType: const FullType(ProjectCardModel)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AddCardToCalenderModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddCardToCalenderModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'card':
          result.card.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectCardModel))
              as ProjectCardModel);
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AddCardToCalenderModel extends AddCardToCalenderModel {
  @override
  final ProjectCardModel card;
  @override
  final String projectId;

  factory _$AddCardToCalenderModel(
          [void Function(AddCardToCalenderModelBuilder) updates]) =>
      (new AddCardToCalenderModelBuilder()..update(updates)).build();

  _$AddCardToCalenderModel._({this.card, this.projectId}) : super._();

  @override
  AddCardToCalenderModel rebuild(
          void Function(AddCardToCalenderModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddCardToCalenderModelBuilder toBuilder() =>
      new AddCardToCalenderModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddCardToCalenderModel &&
        card == other.card &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, card.hashCode), projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddCardToCalenderModel')
          ..add('card', card)
          ..add('projectId', projectId))
        .toString();
  }
}

class AddCardToCalenderModelBuilder
    implements Builder<AddCardToCalenderModel, AddCardToCalenderModelBuilder> {
  _$AddCardToCalenderModel _$v;

  ProjectCardModelBuilder _card;
  ProjectCardModelBuilder get card =>
      _$this._card ??= new ProjectCardModelBuilder();
  set card(ProjectCardModelBuilder card) => _$this._card = card;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  AddCardToCalenderModelBuilder();

  AddCardToCalenderModelBuilder get _$this {
    if (_$v != null) {
      _card = _$v.card?.toBuilder();
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddCardToCalenderModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddCardToCalenderModel;
  }

  @override
  void update(void Function(AddCardToCalenderModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddCardToCalenderModel build() {
    _$AddCardToCalenderModel _$result;
    try {
      _$result = _$v ??
          new _$AddCardToCalenderModel._(
              card: _card?.build(), projectId: projectId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'card';
        _card?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AddCardToCalenderModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
