// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_timesheet_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardTimesheetModel> _$cardTimesheetModelSerializer =
    new _$CardTimesheetModelSerializer();

class _$CardTimesheetModelSerializer
    implements StructuredSerializer<CardTimesheetModel> {
  @override
  final Iterable<Type> types = const [CardTimesheetModel, _$CardTimesheetModel];
  @override
  final String wireName = 'CardTimesheetModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CardTimesheetModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(UserModel)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('_projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CardTimesheetModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardTimesheetModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserModel)) as UserModel);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CardTimesheetModel extends CardTimesheetModel {
  @override
  final String start;
  @override
  final String end;
  @override
  final UserModel user;
  @override
  final String id;
  @override
  final String projectId;

  factory _$CardTimesheetModel(
          [void Function(CardTimesheetModelBuilder) updates]) =>
      (new CardTimesheetModelBuilder()..update(updates)).build();

  _$CardTimesheetModel._(
      {this.start, this.end, this.user, this.id, this.projectId})
      : super._();

  @override
  CardTimesheetModel rebuild(
          void Function(CardTimesheetModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardTimesheetModelBuilder toBuilder() =>
      new CardTimesheetModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardTimesheetModel &&
        start == other.start &&
        end == other.end &&
        user == other.user &&
        id == other.id &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, start.hashCode), end.hashCode), user.hashCode),
            id.hashCode),
        projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardTimesheetModel')
          ..add('start', start)
          ..add('end', end)
          ..add('user', user)
          ..add('id', id)
          ..add('projectId', projectId))
        .toString();
  }
}

class CardTimesheetModelBuilder
    implements Builder<CardTimesheetModel, CardTimesheetModelBuilder> {
  _$CardTimesheetModel _$v;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  UserModelBuilder _user;
  UserModelBuilder get user => _$this._user ??= new UserModelBuilder();
  set user(UserModelBuilder user) => _$this._user = user;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  CardTimesheetModelBuilder();

  CardTimesheetModelBuilder get _$this {
    if (_$v != null) {
      _start = _$v.start;
      _end = _$v.end;
      _user = _$v.user?.toBuilder();
      _id = _$v.id;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardTimesheetModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardTimesheetModel;
  }

  @override
  void update(void Function(CardTimesheetModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardTimesheetModel build() {
    _$CardTimesheetModel _$result;
    try {
      _$result = _$v ??
          new _$CardTimesheetModel._(
              start: start,
              end: end,
              user: _user?.build(),
              id: id,
              projectId: projectId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        _user?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CardTimesheetModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
