library card_employee_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'card_employee_model.g.dart';

abstract class CardEmployeeModel
    implements Built<CardEmployeeModel, CardEmployeeModelBuilder> {
  @nullable
  String get cardId;

  @nullable
  String get memberId;

  @nullable
  String get projectId;

  CardEmployeeModel._();

  factory CardEmployeeModel([updates(CardEmployeeModelBuilder b)]) =
      _$CardEmployeeModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CardEmployeeModel.serializer, this));
  }

  static CardEmployeeModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardEmployeeModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardEmployeeModel> get serializer =>
      _$cardEmployeeModelSerializer;
}
