library card_checklist_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'card_checklist_item_model.dart';

part 'card_checklist_model.g.dart';

abstract class CardChecklistModel
    implements Built<CardChecklistModel, CardChecklistModelBuilder> {
  @nullable
  int get checkItemsChecked;

  @nullable
  String get name;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get dateCreated;

  @nullable
  BuiltList<CardChecklistItemModel> get checkItems;

  CardChecklistModel._();

  factory CardChecklistModel([updates(CardChecklistModelBuilder b)]) =
      _$CardChecklistModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CardChecklistModel.serializer, this));
  }

  static CardChecklistModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardChecklistModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardChecklistModel> get serializer =>
      _$cardChecklistModelSerializer;
}
