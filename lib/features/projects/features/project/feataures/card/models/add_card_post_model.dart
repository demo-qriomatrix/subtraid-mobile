library add_card_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';

part 'add_card_post_model.g.dart';

abstract class AddCardPostModel
    implements Built<AddCardPostModel, AddCardPostModelBuilder> {
  @nullable
  String get projectId;

  @nullable
  String get listId;

  @nullable
  ProjectCardModel get newCard;

  AddCardPostModel._();

  factory AddCardPostModel([updates(AddCardPostModelBuilder b)]) =
      _$AddCardPostModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(AddCardPostModel.serializer, this));
  }

  static AddCardPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddCardPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddCardPostModel> get serializer =>
      _$addCardPostModelSerializer;
}
