// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_employee_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardEmployeeModel> _$cardEmployeeModelSerializer =
    new _$CardEmployeeModelSerializer();

class _$CardEmployeeModelSerializer
    implements StructuredSerializer<CardEmployeeModel> {
  @override
  final Iterable<Type> types = const [CardEmployeeModel, _$CardEmployeeModel];
  @override
  final String wireName = 'CardEmployeeModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CardEmployeeModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.memberId != null) {
      result
        ..add('memberId')
        ..add(serializers.serialize(object.memberId,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CardEmployeeModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardEmployeeModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'memberId':
          result.memberId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CardEmployeeModel extends CardEmployeeModel {
  @override
  final String cardId;
  @override
  final String memberId;
  @override
  final String projectId;

  factory _$CardEmployeeModel(
          [void Function(CardEmployeeModelBuilder) updates]) =>
      (new CardEmployeeModelBuilder()..update(updates)).build();

  _$CardEmployeeModel._({this.cardId, this.memberId, this.projectId})
      : super._();

  @override
  CardEmployeeModel rebuild(void Function(CardEmployeeModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardEmployeeModelBuilder toBuilder() =>
      new CardEmployeeModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardEmployeeModel &&
        cardId == other.cardId &&
        memberId == other.memberId &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, cardId.hashCode), memberId.hashCode), projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardEmployeeModel')
          ..add('cardId', cardId)
          ..add('memberId', memberId)
          ..add('projectId', projectId))
        .toString();
  }
}

class CardEmployeeModelBuilder
    implements Builder<CardEmployeeModel, CardEmployeeModelBuilder> {
  _$CardEmployeeModel _$v;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  String _memberId;
  String get memberId => _$this._memberId;
  set memberId(String memberId) => _$this._memberId = memberId;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  CardEmployeeModelBuilder();

  CardEmployeeModelBuilder get _$this {
    if (_$v != null) {
      _cardId = _$v.cardId;
      _memberId = _$v.memberId;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardEmployeeModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardEmployeeModel;
  }

  @override
  void update(void Function(CardEmployeeModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardEmployeeModel build() {
    final _$result = _$v ??
        new _$CardEmployeeModel._(
            cardId: cardId, memberId: memberId, projectId: projectId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
