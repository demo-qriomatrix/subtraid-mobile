// GENERATED CODE - DO NOT MODIFY BY HAND

part of update_card_static_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UpdateCardStaticModel> _$updateCardStaticModelSerializer =
    new _$UpdateCardStaticModelSerializer();

class _$UpdateCardStaticModelSerializer
    implements StructuredSerializer<UpdateCardStaticModel> {
  @override
  final Iterable<Type> types = const [
    UpdateCardStaticModel,
    _$UpdateCardStaticModel
  ];
  @override
  final String wireName = 'UpdateCardStaticModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, UpdateCardStaticModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.updatedCard != null) {
      result
        ..add('updatedCard')
        ..add(serializers.serialize(object.updatedCard,
            specifiedType: const FullType(ProjectCardModel)));
    }
    return result;
  }

  @override
  UpdateCardStaticModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UpdateCardStaticModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updatedCard':
          result.updatedCard.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectCardModel))
              as ProjectCardModel);
          break;
      }
    }

    return result.build();
  }
}

class _$UpdateCardStaticModel extends UpdateCardStaticModel {
  @override
  final String projectId;
  @override
  final ProjectCardModel updatedCard;

  factory _$UpdateCardStaticModel(
          [void Function(UpdateCardStaticModelBuilder) updates]) =>
      (new UpdateCardStaticModelBuilder()..update(updates)).build();

  _$UpdateCardStaticModel._({this.projectId, this.updatedCard}) : super._();

  @override
  UpdateCardStaticModel rebuild(
          void Function(UpdateCardStaticModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateCardStaticModelBuilder toBuilder() =>
      new UpdateCardStaticModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateCardStaticModel &&
        projectId == other.projectId &&
        updatedCard == other.updatedCard;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, projectId.hashCode), updatedCard.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateCardStaticModel')
          ..add('projectId', projectId)
          ..add('updatedCard', updatedCard))
        .toString();
  }
}

class UpdateCardStaticModelBuilder
    implements Builder<UpdateCardStaticModel, UpdateCardStaticModelBuilder> {
  _$UpdateCardStaticModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  ProjectCardModelBuilder _updatedCard;
  ProjectCardModelBuilder get updatedCard =>
      _$this._updatedCard ??= new ProjectCardModelBuilder();
  set updatedCard(ProjectCardModelBuilder updatedCard) =>
      _$this._updatedCard = updatedCard;

  UpdateCardStaticModelBuilder();

  UpdateCardStaticModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _updatedCard = _$v.updatedCard?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateCardStaticModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UpdateCardStaticModel;
  }

  @override
  void update(void Function(UpdateCardStaticModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateCardStaticModel build() {
    _$UpdateCardStaticModel _$result;
    try {
      _$result = _$v ??
          new _$UpdateCardStaticModel._(
              projectId: projectId, updatedCard: _updatedCard?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'updatedCard';
        _updatedCard?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UpdateCardStaticModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
