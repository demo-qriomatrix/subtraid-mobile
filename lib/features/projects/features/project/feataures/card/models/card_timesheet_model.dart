library card_timesheet_model;

import 'dart:convert';

import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'card_timesheet_model.g.dart';

abstract class CardTimesheetModel
    implements Built<CardTimesheetModel, CardTimesheetModelBuilder> {
  @nullable
  String get start;

  @nullable
  String get end;

  @nullable
  UserModel get user;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  @BuiltValueField(wireName: '_projectId')
  String get projectId;

  CardTimesheetModel._();

  factory CardTimesheetModel([updates(CardTimesheetModelBuilder b)]) =
      _$CardTimesheetModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CardTimesheetModel.serializer, this));
  }

  static CardTimesheetModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardTimesheetModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardTimesheetModel> get serializer =>
      _$cardTimesheetModelSerializer;
}
