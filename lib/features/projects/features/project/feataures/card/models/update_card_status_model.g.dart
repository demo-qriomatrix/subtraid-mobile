// GENERATED CODE - DO NOT MODIFY BY HAND

part of update_card_status_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UpdateCardStatusModel> _$updateCardStatusModelSerializer =
    new _$UpdateCardStatusModelSerializer();

class _$UpdateCardStatusModelSerializer
    implements StructuredSerializer<UpdateCardStatusModel> {
  @override
  final Iterable<Type> types = const [
    UpdateCardStatusModel,
    _$UpdateCardStatusModel
  ];
  @override
  final String wireName = 'UpdateCardStatusModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, UpdateCardStatusModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.cardId != null) {
      result
        ..add('cardId')
        ..add(serializers.serialize(object.cardId,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UpdateCardStatusModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UpdateCardStatusModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cardId':
          result.cardId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UpdateCardStatusModel extends UpdateCardStatusModel {
  @override
  final String cardId;
  @override
  final String projectId;
  @override
  final String status;

  factory _$UpdateCardStatusModel(
          [void Function(UpdateCardStatusModelBuilder) updates]) =>
      (new UpdateCardStatusModelBuilder()..update(updates)).build();

  _$UpdateCardStatusModel._({this.cardId, this.projectId, this.status})
      : super._();

  @override
  UpdateCardStatusModel rebuild(
          void Function(UpdateCardStatusModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateCardStatusModelBuilder toBuilder() =>
      new UpdateCardStatusModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateCardStatusModel &&
        cardId == other.cardId &&
        projectId == other.projectId &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, cardId.hashCode), projectId.hashCode), status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateCardStatusModel')
          ..add('cardId', cardId)
          ..add('projectId', projectId)
          ..add('status', status))
        .toString();
  }
}

class UpdateCardStatusModelBuilder
    implements Builder<UpdateCardStatusModel, UpdateCardStatusModelBuilder> {
  _$UpdateCardStatusModel _$v;

  String _cardId;
  String get cardId => _$this._cardId;
  set cardId(String cardId) => _$this._cardId = cardId;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  UpdateCardStatusModelBuilder();

  UpdateCardStatusModelBuilder get _$this {
    if (_$v != null) {
      _cardId = _$v.cardId;
      _projectId = _$v.projectId;
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateCardStatusModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UpdateCardStatusModel;
  }

  @override
  void update(void Function(UpdateCardStatusModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateCardStatusModel build() {
    final _$result = _$v ??
        new _$UpdateCardStatusModel._(
            cardId: cardId, projectId: projectId, status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
