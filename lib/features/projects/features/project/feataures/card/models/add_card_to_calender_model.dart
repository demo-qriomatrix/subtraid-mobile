library add_card_to_calender_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'add_card_to_calender_model.g.dart';

abstract class AddCardToCalenderModel
    implements Built<AddCardToCalenderModel, AddCardToCalenderModelBuilder> {
  @nullable
  ProjectCardModel get card;

  @nullable
  String get projectId;

  AddCardToCalenderModel._();

  factory AddCardToCalenderModel([updates(AddCardToCalenderModelBuilder b)]) =
      _$AddCardToCalenderModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(AddCardToCalenderModel.serializer, this));
  }

  static AddCardToCalenderModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddCardToCalenderModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddCardToCalenderModel> get serializer =>
      _$addCardToCalenderModelSerializer;
}
