library card_add_comment_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'card_add_comment_model.dart';

part 'card_add_comment_post_model.g.dart';

abstract class CardAddCommentPostModel
    implements Built<CardAddCommentPostModel, CardAddCommentPostModelBuilder> {
  @nullable
  String get projectId;

  @nullable
  String get cardId;

  @nullable
  CardAddCommentModel get newComment;

  CardAddCommentPostModel._();

  factory CardAddCommentPostModel([updates(CardAddCommentPostModelBuilder b)]) =
      _$CardAddCommentPostModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardAddCommentPostModel.serializer, this));
  }

  static CardAddCommentPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardAddCommentPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardAddCommentPostModel> get serializer =>
      _$cardAddCommentPostModelSerializer;
}
