// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_checklist_item_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardChecklistItemModel> _$cardChecklistItemModelSerializer =
    new _$CardChecklistItemModelSerializer();

class _$CardChecklistItemModelSerializer
    implements StructuredSerializer<CardChecklistItemModel> {
  @override
  final Iterable<Type> types = const [
    CardChecklistItemModel,
    _$CardChecklistItemModel
  ];
  @override
  final String wireName = 'CardChecklistItemModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardChecklistItemModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.checked != null) {
      result
        ..add('checked')
        ..add(serializers.serialize(object.checked,
            specifiedType: const FullType(bool)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CardChecklistItemModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardChecklistItemModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'checked':
          result.checked = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CardChecklistItemModel extends CardChecklistItemModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final bool checked;
  @override
  final String dateCreated;

  factory _$CardChecklistItemModel(
          [void Function(CardChecklistItemModelBuilder) updates]) =>
      (new CardChecklistItemModelBuilder()..update(updates)).build();

  _$CardChecklistItemModel._(
      {this.id, this.name, this.checked, this.dateCreated})
      : super._();

  @override
  CardChecklistItemModel rebuild(
          void Function(CardChecklistItemModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardChecklistItemModelBuilder toBuilder() =>
      new CardChecklistItemModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardChecklistItemModel &&
        id == other.id &&
        name == other.name &&
        checked == other.checked &&
        dateCreated == other.dateCreated;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), name.hashCode), checked.hashCode),
        dateCreated.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardChecklistItemModel')
          ..add('id', id)
          ..add('name', name)
          ..add('checked', checked)
          ..add('dateCreated', dateCreated))
        .toString();
  }
}

class CardChecklistItemModelBuilder
    implements Builder<CardChecklistItemModel, CardChecklistItemModelBuilder> {
  _$CardChecklistItemModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  bool _checked;
  bool get checked => _$this._checked;
  set checked(bool checked) => _$this._checked = checked;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  CardChecklistItemModelBuilder();

  CardChecklistItemModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _checked = _$v.checked;
      _dateCreated = _$v.dateCreated;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardChecklistItemModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardChecklistItemModel;
  }

  @override
  void update(void Function(CardChecklistItemModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardChecklistItemModel build() {
    final _$result = _$v ??
        new _$CardChecklistItemModel._(
            id: id, name: name, checked: checked, dateCreated: dateCreated);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
