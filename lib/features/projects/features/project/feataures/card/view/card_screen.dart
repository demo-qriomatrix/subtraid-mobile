import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/helpers/formate_start_end_date.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/shift_card_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_status_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/add_employee_dialog.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/add_label_dialog.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/card_comment_text_box_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/card_comment_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/card_description_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/card_name_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/check_list_item_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/widgets/create_subtask_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/widgets/card_label_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/widgets/indicator_widget.dart';
import 'package:Subtraid/features/shared/cound_up_timer_widget.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class CardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardBloc, CardState>(
      builder: (context, state) => Container(
        color: ColorConfig.primary,
        child: Column(
          children: [
            StAppBar(
              title: state.cardModel.name ?? '',
            ),
            Expanded(
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50),
                            topRight: Radius.circular(50))),
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Column(children: [
                          Column(
                            children: <Widget>[
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  RawMaterialButton(
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    constraints: BoxConstraints(),
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(
                                      Icons.notifications,
                                      color: ColorConfig.orange,
                                      size: 20,
                                    ),
                                  ),
                                  RawMaterialButton(
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    constraints: BoxConstraints(),
                                    onPressed: () {},
                                    shape: CircleBorder(),
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(
                                      Icons.more_horiz,
                                      color: ColorConfig.primary,
                                      size: 20,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Expanded(
                              child: ListView(
                            scrollDirection: Axis.vertical,
                            padding: EdgeInsets.zero,
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    state.assignedToCard
                                        ? Row(
                                            children: <Widget>[
                                              FlatButton(
                                                color: ColorConfig.grey9,
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 20,
                                                    vertical: 10),
                                                child: Row(
                                                  children: <Widget>[
                                                    ImageIcon(
                                                      AssetImage(LocalImages
                                                          .stopwatch),
                                                      color:
                                                          ColorConfig.fillColor,
                                                      size: 16,
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                        state.startedTask !=
                                                                    null &&
                                                                state.startedTask
                                                                        .end ==
                                                                    null
                                                            ? "Stop Task"
                                                            : "Start Task",
                                                        style: TextStyle(
                                                          // fontSize: 17,
                                                          color: ColorConfig
                                                              .fillColor,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        )),
                                                  ],
                                                ),
                                                onPressed: () {
                                                  if (state.startedTask !=
                                                          null &&
                                                      state.startedTask.end ==
                                                          null) {
                                                    context
                                                        .read<ProjectBloc>()
                                                        .add(
                                                            ProjectCardStopTask());
                                                  } else {
                                                    context.read<ProjectBloc>().add(
                                                        ProjectCardStartTask());
                                                  }
                                                },
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              state.cardModel.activeTimesheet !=
                                                      null
                                                  ? CountDownTimer(
                                                      startDateTime: state
                                                          .cardModel
                                                          .activeTimesheet
                                                          .start,
                                                    )
                                                  : Container()
                                            ],
                                          )
                                        : Container(),
                                    CardNameWidget(
                                      cardModel: state.cardModel,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        FlatButton(
                                          color: ColorConfig.grey10,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(
                                                Icons.attach_file,
                                                color: ColorConfig.grey11,
                                                size: 17,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text("Attach",
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    color: ColorConfig.grey11,
                                                    fontWeight: FontWeight.w500,
                                                  )),
                                            ],
                                          ),
                                          onPressed: () {},
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        FlatButton(
                                          color: state.addSubtaskMode
                                              ? ColorConfig.primary
                                              : ColorConfig.grey10,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(
                                                Icons.check_box,
                                                color: state.addSubtaskMode
                                                    ? Colors.white
                                                    : ColorConfig.grey11,
                                                size: 17,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text("Create Sub Tasks",
                                                  style: TextStyle(
                                                    fontSize: 13,
                                                    color: state.addSubtaskMode
                                                        ? Colors.white
                                                        : ColorConfig.grey11,
                                                    fontWeight: FontWeight.w500,
                                                  )),
                                            ],
                                          ),
                                          onPressed: () {
                                            context.read<CardBloc>().add(
                                                CardToggleEditMode(
                                                    addCheckItemMode: null,
                                                    addSubtaskMode:
                                                        !state.addSubtaskMode,
                                                    nameEditMode: null,
                                                    descriptionEditMode: null));
                                          },
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      'Sub Tasks',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    state.addSubtaskMode
                                        ? Padding(
                                            padding:
                                                const EdgeInsets.only(top: 10),
                                            child: CreateSubtaskWidget(
                                              onCancel: () {
                                                context.read<CardBloc>().add(
                                                    CardToggleEditMode(
                                                        addCheckItemMode: null,
                                                        addSubtaskMode: false,
                                                        nameEditMode: null,
                                                        descriptionEditMode:
                                                            null));
                                              },
                                              onCreate: (String name) {
                                                if (name == '' ||
                                                    name == null) {
                                                  name = 'Untitled Sub task';
                                                }

                                                context.read<ProjectBloc>().add(
                                                    ProjectCardAddChecklist(
                                                        subTask: name));
                                              },
                                            ),
                                          )
                                        : Container(),
                                    state.cardModel.checklists.isEmpty &&
                                            !state.addSubtaskMode
                                        ? Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 10),
                                            child: Text(
                                              'No subtasks',
                                              style: TextStyle(
                                                  color: ColorConfig.grey1),
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                              ),
                              state.cardModel.checklists.isNotEmpty
                                  ? Column(
                                      children: state.cardModel.checklists
                                          .map(
                                            (checkListItem) => CheckListItem(
                                              addCheckItemMode:
                                                  state.addCheckItemMode,
                                              cardChecklistModel: checkListItem,
                                            ),
                                          )
                                          .toList(),
                                    )
                                  : Container(),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 250,
                                          height: 36,
                                          decoration: BoxDecoration(
                                            color: ColorConfig.grey6,
                                            borderRadius:
                                                BorderRadius.circular(4),
                                          ),
                                          // padding: EdgeInsets.symmetric(
                                          //     horizontal: 20, vertical: 5),
                                          child: DropdownButtonFormField(
                                            dropdownColor: ColorConfig.primary,
                                            isDense: true,
                                            // underline: Container(),
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 10),
                                                border: OutlineInputBorder(
                                                    borderSide:
                                                        BorderSide.none)),
                                            icon: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 40),
                                              child:
                                                  Icon(Icons.arrow_drop_down),
                                            ),

                                            value: state.cardModel.listId,
                                            // hint: Text(
                                            //   'Ready for Inspections',
                                            // style: TextStyle(
                                            //     color: Colors.black,
                                            //     fontSize: 13),
                                            // ),
                                            iconEnabledColor: Colors.black,
                                            iconDisabledColor: Colors.black,
                                            items: (context
                                                        .watch<ProjectBloc>()
                                                        .state
                                                    is ProjectLoadSuccess)
                                                ? (context
                                                            .watch<ProjectBloc>()
                                                            .state
                                                        as ProjectLoadSuccess)
                                                    .projectModel
                                                    .lists
                                                    .map((e) =>
                                                        DropdownMenuItem(
                                                          value: e.id,
                                                          child: Text(
                                                            e.name,
                                                            style: TextStyle(
                                                                fontSize: 12,
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ))
                                                    .toList()
                                                : [],
                                            selectedItemBuilder: (context) =>
                                                (context
                                                            .watch<ProjectBloc>()
                                                            .state
                                                        is ProjectLoadSuccess)
                                                    ? (context
                                                                .watch<
                                                                    ProjectBloc>()
                                                                .state
                                                            as ProjectLoadSuccess)
                                                        .projectModel
                                                        .lists
                                                        .map((e) =>
                                                            DropdownMenuItem(
                                                              value: e.id,
                                                              child: Text(
                                                                e.name,
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                              ),
                                                            ))
                                                        .toList()
                                                    : [],
                                            // items: Provider.of<AppStateProvider>(
                                            //         context,
                                            //         listen: false)
                                            //     .mappedListNames
                                            //     .entries
                                            //     .map((e) => DropdownMenuItem(
                                            //           value: e.key,
                                            //           child: Text(e.value),
                                            //         ))
                                            //     .toList(),
                                            onChanged: (val) {
                                              ShiftCardModel card =
                                                  ShiftCardModel((data) => data
                                                    ..cardId =
                                                        state.cardModel.id
                                                    ..newListId = val);

                                              context.read<ProjectBloc>().add(
                                                  ProjectShiftCard(card: card));
                                              // print(val);
                                              // onShiftCard(val);
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 150,
                                          height: 36,
                                          decoration: BoxDecoration(
                                            color: getColorFromStatus(
                                                state.cardModel.status),
                                            borderRadius:
                                                BorderRadius.circular(4),
                                          ),
                                          // padding: EdgeInsets.symmetric(
                                          //     horizontal: 20, vertical: 5),
                                          child: DropdownButtonFormField(
                                            dropdownColor: ColorConfig.primary,
                                            selectedItemBuilder: (context) => [
                                              'Completed',
                                              'Active',
                                              'Irrelevant',
                                              'Resolved'
                                            ]
                                                .map((e) => DropdownMenuItem(
                                                      value: e.toLowerCase(),
                                                      child: Text(
                                                        e,
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ))
                                                .toList(),
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.symmetric(
                                                        horizontal: 10),
                                                border: OutlineInputBorder(
                                                    borderSide:
                                                        BorderSide.none)),
                                            isDense: true,
                                            value: state.cardModel.status,
                                            // underline: Container(),
                                            icon: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20),
                                              child:
                                                  Icon(Icons.arrow_drop_down),
                                            ),
                                            iconEnabledColor: Colors.white,
                                            iconDisabledColor: Colors.white,
                                            items: [
                                              'Completed',
                                              'Active',
                                              'Irrelevant',
                                              'Resolved'
                                            ]
                                                .map((e) => DropdownMenuItem(
                                                      value: e.toLowerCase(),
                                                      child: Text(
                                                        e,
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ))
                                                .toList(),
                                            onChanged: (val) {
                                              UpdateCardStatusModel card =
                                                  UpdateCardStatusModel(
                                                      (data) => data
                                                        ..cardId =
                                                            state.cardModel.id
                                                        ..status = val);

                                              context.read<ProjectBloc>().add(
                                                  ProjectUpdateCardStatus(
                                                      card: card));
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    SaveButton('Add to Calender', () {
                                      context
                                          .read<ProjectBloc>()
                                          .add(ProjectCardAddToEvent());
                                    }),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            showDatePicker(
                                              context: context,
                                              initialDate:
                                                  formatStartEndDateToDate(
                                                      state.cardModel.start),
                                              firstDate:
                                                  formatStartEndDateToDate(
                                                          state.cardModel.start)
                                                      .subtract(
                                                          Duration(days: 3650)),
                                              lastDate:
                                                  formatStartEndDateToDate(
                                                          state.cardModel.start)
                                                      .add(
                                                          Duration(days: 3650)),
                                            ).then((value) {
                                              if (value != null) {
                                                UpdateCardStaticModel
                                                    updateCard =
                                                    UpdateCardStaticModel(
                                                        (card) => card
                                                          ..projectId = (context
                                                                      .read<
                                                                          ProjectBloc>()
                                                                      .state
                                                                  as ProjectLoadSuccess)
                                                              .projectModel
                                                              .id
                                                          ..updatedCard = state
                                                              .cardModel
                                                              .rebuild((update) => update
                                                                ..start = DateFormat(
                                                                        "MM/dd/yyyy hh:mm a")
                                                                    .format(
                                                                        value))
                                                              .toBuilder());

                                                context
                                                    .read<ProjectBloc>()
                                                    .add(ProjectUpdateCard(
                                                      card: updateCard,
                                                    ));
                                              }
                                            });
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                'Start Date',
                                                style: TextStyle(
                                                    color: ColorConfig.primary,
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              ImageIcon(
                                                AssetImage(
                                                    LocalImages.calenderAlt),
                                                color: ColorConfig.primary,
                                                size: 16,
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                formatStartEndDate(
                                                    state.cardModel.start),
                                                style: TextStyle(
                                                    color: ColorConfig.black2,
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              )
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            showDatePicker(
                                              context: context,
                                              initialDate:
                                                  formatStartEndDateToDate(
                                                      state.cardModel.end),
                                              firstDate:
                                                  formatStartEndDateToDate(
                                                          state.cardModel.end)
                                                      .subtract(
                                                          Duration(days: 3650)),
                                              lastDate:
                                                  formatStartEndDateToDate(
                                                          state.cardModel.end)
                                                      .add(
                                                          Duration(days: 3650)),
                                            ).then((value) {
                                              if (value != null) {
                                                UpdateCardStaticModel
                                                    updateCard =
                                                    UpdateCardStaticModel(
                                                        (card) => card
                                                          ..projectId = (context
                                                                      .read<
                                                                          ProjectBloc>()
                                                                      .state
                                                                  as ProjectLoadSuccess)
                                                              .projectModel
                                                              .id
                                                          ..updatedCard = state
                                                              .cardModel
                                                              .rebuild((update) => update
                                                                ..end = DateFormat(
                                                                        "MM/dd/yyyy hh:mm a")
                                                                    .format(
                                                                        value))
                                                              .toBuilder());

                                                context
                                                    .read<ProjectBloc>()
                                                    .add(ProjectUpdateCard(
                                                      card: updateCard,
                                                    ));
                                              }
                                            });
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                'End Date',
                                                style: TextStyle(
                                                    color: ColorConfig.primary,
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              ImageIcon(
                                                AssetImage(
                                                    LocalImages.calenderAlt),
                                                color: ColorConfig.primary,
                                                size: 16,
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                formatStartEndDate(
                                                    state.cardModel.end),
                                                style: TextStyle(
                                                    color: ColorConfig.black2,
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            showAssigneeDialog(context, state);
                                          },
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                'Assignee',
                                                style: TextStyle(
                                                    color: ColorConfig.primary,
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Row(children: [
                                                ...List.generate(
                                                  state.cardModel.idMembers
                                                      .length,
                                                  (index) => Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8),
                                                    child: CircleAvatar(
                                                      backgroundImage:
                                                          AssetImage(LocalImages
                                                              .defaultAvatar),
                                                      radius: 14,
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8),
                                                  child: RawMaterialButton(
                                                    elevation: 0,
                                                    shape: CircleBorder(),
                                                    constraints:
                                                        BoxConstraints(),
                                                    padding: EdgeInsets.all(4),
                                                    child: Icon(
                                                      Icons.add,
                                                      size: 20,
                                                      color: Colors.white,
                                                    ),
                                                    materialTapTargetSize:
                                                        MaterialTapTargetSize
                                                            .shrinkWrap,
                                                    fillColor:
                                                        ColorConfig.grey6,
                                                    onPressed: () {
                                                      showAssigneeDialog(
                                                          context, state);
                                                    },
                                                  ),
                                                )
                                              ])
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: InkWell(
                                            onTap: () {
                                              showLabelsDialog(context, state);
                                            },
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  'Labels',
                                                  style: TextStyle(
                                                      color:
                                                          ColorConfig.primary,
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: BlocBuilder<
                                                      ProjectBloc,
                                                      ProjectState>(
                                                    builder: (context,
                                                        projectState) {
                                                      if (projectState
                                                          is ProjectLoadSuccess) {
                                                        return Wrap(
                                                          alignment:
                                                              WrapAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              WrapCrossAlignment
                                                                  .start,
                                                          runAlignment:
                                                              WrapAlignment
                                                                  .start,
                                                          direction:
                                                              Axis.horizontal,
                                                          runSpacing: 10,
                                                          children: [
                                                            ...List.generate(
                                                                state
                                                                    .cardModel
                                                                    .idLabels
                                                                    .length,
                                                                (index) => CardLabelWithNameWidget(projectState
                                                                    .labels[state
                                                                        .cardModel
                                                                        .idLabels[
                                                                    index]])),
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 8),
                                                              child:
                                                                  RawMaterialButton(
                                                                elevation: 0,
                                                                shape:
                                                                    CircleBorder(),
                                                                constraints:
                                                                    BoxConstraints(),
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(4),
                                                                child: Icon(
                                                                  Icons.add,
                                                                  size: 20,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                                materialTapTargetSize:
                                                                    MaterialTapTargetSize
                                                                        .shrinkWrap,
                                                                fillColor:
                                                                    ColorConfig
                                                                        .grey6,
                                                                onPressed: () {
                                                                  showLabelsDialog(
                                                                      context,
                                                                      state);
                                                                },
                                                              ),
                                                            )
                                                          ],
                                                        );
                                                      }

                                                      return Container();
                                                    },
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),

                                    // Container(
                                    //   padding: EdgeInsets.symmetric(
                                    //       horizontal: 20, vertical: 20),
                                    //   decoration: BoxDecoration(
                                    //     color: Colors.white,
                                    //     boxShadow: [
                                    //       BoxShadow(
                                    //         color: Colors.grey.withOpacity(0.3),
                                    //         spreadRadius: 2,
                                    //         blurRadius: 6,
                                    //         offset: Offset(0, 3),
                                    //       ),
                                    //     ],
                                    //   ),
                                    //   child: Column(
                                    //     crossAxisAlignment:
                                    //         CrossAxisAlignment.start,
                                    //     children: <Widget>[
                                    //       Text(
                                    //         'Time Tracking',
                                    //         style: TextStyle(
                                    //             fontSize: 13,
                                    //             fontWeight: FontWeight.w500,
                                    //             color: blue14),
                                    //       ),
                                    //       SizedBox(
                                    //         height: 10,
                                    //       ),
                                    //       cardModel.timesheets.length == 0
                                    //           ? Row(
                                    //               children: <Widget>[
                                    //                 Text(
                                    //                   'No timesheets logged',
                                    //                   style: TextStyle(
                                    //                       fontSize: 13,
                                    //                       color: grey1),
                                    //                 )
                                    //               ],
                                    //             )
                                    //           : Column(
                                    //               children: cardModel.timesheets
                                    //                   .map((timesheetItem) =>
                                    //                       TimesheetItemWidget(
                                    //                           timesheetModel:
                                    //                               timesheetItem))
                                    //                   .toList()),
                                    //     ],
                                    //   ),
                                    // ),
                                    SizedBox(
                                      height: 10,
                                    ),

                                    // SizedBox(
                                    //   height: 20,
                                    // ),
                                    // Text(
                                    //   "Attachments",
                                    //   style: TextStyle(
                                    //       color: Colors.black,
                                    //       fontSize: 14,
                                    //       fontWeight: FontWeight.w500),
                                    // ),
                                    // SizedBox(
                                    //   height: 20,
                                    // ),
                                    // Row(
                                    //   children: <Widget>[
                                    //     Column(
                                    //       crossAxisAlignment:
                                    //           CrossAxisAlignment.start,
                                    //       children: <Widget>[
                                    //         Image.asset(
                                    //           'assets/images/splash-image.png',
                                    //           width: MediaQuery.of(context)
                                    //                   .size
                                    //                   .width *
                                    //               0.4,
                                    //         ),
                                    //         SizedBox(
                                    //           height: 10,
                                    //         ),
                                    //         Text(
                                    //           "Screen Shot 2020 Jun 6...",
                                    //           style: TextStyle(
                                    //               color: Colors.black,
                                    //               fontSize: 12,
                                    //               fontWeight: FontWeight.w500),
                                    //         ),
                                    //       ],
                                    //     ),
                                    //   ],
                                    // ),
                                    // SizedBox(
                                    //   height: 20,
                                    // ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Description',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),

                                    CardDescriptionWidget(
                                      cardModel: state.cardModel,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),

                                    Text(
                                      "Comments",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Column(
                                      children: state.cardModel.comments
                                          .map(
                                            (comment) => CardCommentWidget(
                                              commentModel: comment,
                                            ),
                                          )
                                          .toList(),
                                    ),

                                    SizedBox(
                                      height: 10,
                                    ),
                                    CardCommentTextBoxWidget(
                                      cardModel: state.cardModel,
                                    ),
                                    SizedBox(
                                      height: 40,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ))
                        ]))))
          ],
        ),
      ),
    );
  }

  Future showAssigneeDialog(BuildContext context, CardState state) {
    List<UserModel> projectMembers =
        (context.read<ProjectBloc>().state as ProjectLoadSuccess)
            .projectModel
            .idMembers
            .map((projectMember) => UserModel((user) => user
              ..name = projectMember.name
              ..id = projectMember.id
              ..img = projectMember.img))
            .toList();

    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (dialogContext) => BlocProvider<ProjectBloc>.value(
              value: context.read<ProjectBloc>(),
              child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  backgroundColor: Colors.white,
                  child: AddMemberDialog(
                    projectMembers: projectMembers,
                    idMembers: state.cardModel.idMembers.toList(),
                  )),
            ));
  }

  Future showLabelsDialog(BuildContext context, CardState state) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (dialogContext) => BlocProvider<ProjectBloc>.value(
              value: context.read<ProjectBloc>(),
              child: BlocProvider<CardBloc>.value(
                value: context.read<CardBloc>(),
                child: Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    backgroundColor: Colors.white,
                    child: AddLabelDialog()),
              ),
            ));
  }
}
