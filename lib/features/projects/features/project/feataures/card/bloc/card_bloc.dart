import 'dart:async';

import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_timesheet_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';

part 'card_event.dart';
part 'card_state.dart';

class CardBloc extends Bloc<CardEvent, CardState> {
  CardBloc({@required this.projectBloc, @required this.authenticationBloc})
      : super(CardState(
            nameEditMode: false,
            descriptionEditMode: false,
            cardModel: null,
            addCheckItemMode: false,
            addSubtaskMode: false,
            startedTask: null,
            assignedToCard: false)) {
    projectBloc.setCardBloc(this);
  }

  final ProjectBloc projectBloc;
  final AuthenticationBloc authenticationBloc;

  @override
  Stream<CardState> mapEventToState(
    CardEvent event,
  ) async* {
    if (event is CardToggleEditMode) {
      yield* _mapCardToggleEditModeToState(event);
    }

    if (event is CardSetCard) {
      yield* _mapCardStateToState(event);
    }
  }

  Stream<CardState> _mapCardStateToState(CardSetCard event) async* {
    String userId;
    if (authenticationBloc.state is AuthentcatedState) {
      userId = (authenticationBloc.state as AuthentcatedState).user.id;
    }
    bool assignedToCard = event.card?.idMembers?.singleWhere(
            (member) => member.id == userId,
            orElse: () => null) !=
        null;

    CardTimesheetModel lastTimesheet;
    if (assignedToCard &&
        event.card?.timesheets != null &&
        event.card.timesheets.isNotEmpty) {
      lastTimesheet = event.card?.timesheets?.singleWhere(
          (timesheet) => timesheet.user.id == userId && timesheet.end == null,
          orElse: () => null);
    }

    yield CardState(
        addCheckItemMode: false,
        addSubtaskMode: false,
        startedTask: lastTimesheet,
        assignedToCard: assignedToCard,
        cardModel: event.card,
        descriptionEditMode: false,
        nameEditMode: false);
  }

  Stream<CardState> _mapCardToggleEditModeToState(
      CardToggleEditMode event) async* {
    yield CardState(
      addSubtaskMode: event.addSubtaskMode == null
          ? state.addSubtaskMode
          : event.addSubtaskMode,
      cardModel: state.cardModel,
      startedTask: state.startedTask,
      addCheckItemMode: event.addCheckItemMode == null
          ? state.addCheckItemMode
          : event.addCheckItemMode,
      assignedToCard: state.assignedToCard,
      nameEditMode:
          event.nameEditMode == null ? state.nameEditMode : event.nameEditMode,
      descriptionEditMode: event.descriptionEditMode == null
          ? state.descriptionEditMode
          : event.descriptionEditMode,
    );
  }
}
