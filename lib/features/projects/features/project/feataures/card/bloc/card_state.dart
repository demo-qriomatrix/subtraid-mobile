part of 'card_bloc.dart';

class CardState extends Equatable {
  final bool nameEditMode;
  final bool descriptionEditMode;
  final ProjectCardModel cardModel;
  final bool assignedToCard;
  final bool addSubtaskMode;
  final bool addCheckItemMode;
  final CardTimesheetModel startedTask;

  CardState({
    @required this.nameEditMode,
    @required this.descriptionEditMode,
    @required this.assignedToCard,
    @required this.addSubtaskMode,
    @required this.addCheckItemMode,
    @required this.cardModel,
    @required this.startedTask,
  });

  @override
  List<Object> get props => [
        nameEditMode,
        descriptionEditMode,
        cardModel,
        assignedToCard,
        addSubtaskMode,
        addCheckItemMode,
        startedTask
      ];
}
