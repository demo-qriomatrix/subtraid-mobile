part of 'card_bloc.dart';

abstract class CardEvent extends Equatable {
  const CardEvent();

  @override
  List<Object> get props => [];
}

class CardToggleEditMode extends CardEvent {
  final bool nameEditMode;
  final bool descriptionEditMode;
  final bool addSubtaskMode;
  final bool addCheckItemMode;

  CardToggleEditMode(
      {@required this.nameEditMode,
      @required this.descriptionEditMode,
      @required this.addCheckItemMode,
      @required this.addSubtaskMode});
}

class CardToggleDescriptionEditMode extends CardEvent {
  final bool editMode;

  CardToggleDescriptionEditMode({@required this.editMode});
}

class CardSetCard extends CardEvent {
  final ProjectCardModel card;

  CardSetCard({@required this.card});
}
