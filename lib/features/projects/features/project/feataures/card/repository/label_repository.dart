import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_label_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_label_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class LabelRepository {
  final HttpClient httpClient;

  LabelRepository({@required this.httpClient});

  Future<Response> addLabelToCard(AddCardLabelModel label) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectLabel + '/add', data: label.toJson());
  }

  Future<Response> updateLabelToCard(UpdateCardLabelModel label) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectLabel + '/update', data: label.toJson());
  }
}
