import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_employee_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_update_checklist_item_model.dart';
import 'package:dio/dio.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_add_comment_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/shift_card_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_static_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/update_card_status_model.dart';

class CardRepository {
  final HttpClient httpClient;

  CardRepository({@required this.httpClient});

  Future<Response> addCardToList(AddCardPostModel card) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectCard + '/add', data: card.toJson());
  }

  Future<Response> updateCardStatic(
      UpdateCardStaticModel updateCardStaticModel) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/update_card_static',
        data: updateCardStaticModel.toJson());
  }

  Future<Response> shiftCard(ShiftCardModel card) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectCard + '/shiftcard', data: card.toJson());
  }

  Future<Response> updateCardStatus(UpdateCardStatusModel card) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectCard + '/status', data: card.toJson());
  }

  Future<Response> cardAddComment(CardAddCommentPostModel comment) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/add/comment',
        data: comment.toJson());
  }

  Future<Response> cardAddEmployee(CardEmployeeModel employee) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/addemployee',
        data: employee.toJson());
  }

  Future<Response> cardRemoveEmployee(CardEmployeeModel employee) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/removeemployee',
        data: employee.toJson());
  }

  Future<Response> cardAddChecklist(CardAddChecklistModel checklist) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/add/checkList',
        data: checklist.toJson());
  }

  Future<Response> cardAddChecklistItem(
      CardAddChecklistItemModel checklistItem) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/add/checkItem',
        data: checklistItem.toJson());
  }

  Future<Response> cardDeleteChecklist(
      CardAddChecklistItemModel checklistItem) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/remove/checkList',
        data: checklistItem.toJson());
  }

  Future<Response> cardUpdateChecklistItem(
      CardUpdateChecklistItemModel checklist) async {
    return await httpClient.dio.patch(
        EndpointConfig.projectCard + '/updateListChecked',
        data: checklist.toJson());
  }
}
