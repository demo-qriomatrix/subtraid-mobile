// GENERATED CODE - DO NOT MODIFY BY HAND

part of remove_member_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RemoveMemberModel> _$removeMemberModelSerializer =
    new _$RemoveMemberModelSerializer();

class _$RemoveMemberModelSerializer
    implements StructuredSerializer<RemoveMemberModel> {
  @override
  final Iterable<Type> types = const [RemoveMemberModel, _$RemoveMemberModel];
  @override
  final String wireName = 'RemoveMemberModel';

  @override
  Iterable<Object> serialize(Serializers serializers, RemoveMemberModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.userId != null) {
      result
        ..add('userId')
        ..add(serializers.serialize(object.userId,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RemoveMemberModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RemoveMemberModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'userId':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RemoveMemberModel extends RemoveMemberModel {
  @override
  final String userId;
  @override
  final String projectId;

  factory _$RemoveMemberModel(
          [void Function(RemoveMemberModelBuilder) updates]) =>
      (new RemoveMemberModelBuilder()..update(updates)).build();

  _$RemoveMemberModel._({this.userId, this.projectId}) : super._();

  @override
  RemoveMemberModel rebuild(void Function(RemoveMemberModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RemoveMemberModelBuilder toBuilder() =>
      new RemoveMemberModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RemoveMemberModel &&
        userId == other.userId &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, userId.hashCode), projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RemoveMemberModel')
          ..add('userId', userId)
          ..add('projectId', projectId))
        .toString();
  }
}

class RemoveMemberModelBuilder
    implements Builder<RemoveMemberModel, RemoveMemberModelBuilder> {
  _$RemoveMemberModel _$v;

  String _userId;
  String get userId => _$this._userId;
  set userId(String userId) => _$this._userId = userId;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  RemoveMemberModelBuilder();

  RemoveMemberModelBuilder get _$this {
    if (_$v != null) {
      _userId = _$v.userId;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RemoveMemberModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RemoveMemberModel;
  }

  @override
  void update(void Function(RemoveMemberModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RemoveMemberModel build() {
    final _$result =
        _$v ?? new _$RemoveMemberModel._(userId: userId, projectId: projectId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
