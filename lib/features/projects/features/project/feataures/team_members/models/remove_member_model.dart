library remove_member_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'remove_member_model.g.dart';

abstract class RemoveMemberModel
    implements Built<RemoveMemberModel, RemoveMemberModelBuilder> {
  @nullable
  String get userId;

  @nullable
  String get projectId;

  RemoveMemberModel._();

  factory RemoveMemberModel([updates(RemoveMemberModelBuilder b)]) =
      _$RemoveMemberModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(RemoveMemberModel.serializer, this));
  }

  static RemoveMemberModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RemoveMemberModel.serializer, json.decode(jsonString));
  }

  static Serializer<RemoveMemberModel> get serializer =>
      _$removeMemberModelSerializer;
}
