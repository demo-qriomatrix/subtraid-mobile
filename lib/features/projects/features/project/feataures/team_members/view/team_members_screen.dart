import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/widgets/crews_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/widgets/members_widget.dart';
import 'package:Subtraid/features/shared/widgets/custom_icon.dart';
import 'package:flutter/material.dart';

class TeamMembersScreen extends StatefulWidget {
  @override
  _TeamMembersScreenState createState() => _TeamMembersScreenState();
}

class _TeamMembersScreenState extends State<TeamMembersScreen> {
  int selectedIndex = 0;
  TextEditingController userRoleController = TextEditingController();
  TextEditingController roleDescriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      child: Column(
        children: <Widget>[
          // StAppBar(
          //   // leading: Row(
          //   //   children: [
          //   //     // BackButton(),
          //   //     IconButton(icon: Icon(Icons.menu), onPressed: () {})
          //   //   ],
          //   // ),
          //   title: 'Team members',
          // ),
          Expanded(
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: <Widget>[
                            SizedBox(
                              width: 20,
                            ),
                            CustomIcon(
                              icon: 'assets/images/my-list.png',
                              title: 'Members',
                              selected: selectedIndex == 0,
                              ontap: () {
                                setState(() {
                                  selectedIndex = 0;
                                });
                              },
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            CustomIcon(
                              icon: 'assets/images/all-users.png',
                              title: 'Crews',
                              selected: selectedIndex == 1,
                              ontap: () {
                                setState(() {
                                  selectedIndex = 1;
                                });
                              },
                            ),
                          ],
                        ),
                        Expanded(
                          child: Stack(
                            children: <Widget>[
                              Offstage(
                                  offstage: selectedIndex != 0,
                                  child: MembersWidget()),
                              Offstage(
                                  offstage: selectedIndex != 1,
                                  child: CrewsWidget()),
                            ],
                          ),
                        ),
                      ],
                    ))),
          )
        ],
      ),
    );
  }
}
