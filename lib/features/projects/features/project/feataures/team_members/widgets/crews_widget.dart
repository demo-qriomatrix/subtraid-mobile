import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/more/company/features/crew/bloc/crew/crew_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/bloc/project_crews/project_crews_bloc.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CrewsWidget extends StatefulWidget {
  @override
  _CrewsWidgetState createState() => _CrewsWidgetState();
}

class _CrewsWidgetState extends State<CrewsWidget> {
  TextEditingController crewNameController = TextEditingController();
  String name;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CrewBloc>(
      create: (context) => CrewBloc(crewRepository: sl()),
      child: BlocProvider<ProjectCrewsBloc>(
        create: (context) => ProjectCrewsBloc(
            projectBloc: context.read<ProjectBloc>(),
            crewBloc: context.read<CrewBloc>()),
        child: BlocBuilder<ProjectBloc, ProjectState>(
            builder: (context, projectState) {
          if (projectState is ProjectLoadSuccess) {
            return BlocBuilder<ProjectCrewsBloc, ProjectCrewsState>(
              builder: (context, crewState) {
                if (crewState is ProjectCrewsLoadSuccess) {
                  return ListView(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                        decoration: BoxDecoration(
                            color: ColorConfig.blue4,
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Member name',
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black,
                                    fontSize: 12)),
                            SizedBox(
                              height: height2,
                            ),
                            TextField(
                              controller: crewNameController,
                              decoration: profileInputDecoration.copyWith(
                                  prefixIcon: Icon(
                                    Icons.search,
                                    color: ColorConfig.primary,
                                  ),
                                  labelText: 'Search by name',
                                  labelStyle: TextStyle(
                                      fontSize: 14, color: ColorConfig.black1)),
                              maxLines: null,
                              style: textStyle2,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                RawMaterialButton(
                                  constraints: BoxConstraints(),
                                  onPressed: () {
                                    crewNameController.clear();
                                    setState(() {
                                      name = null;
                                    });
                                  },
                                  shape: CircleBorder(),
                                  fillColor: ColorConfig.grey6,
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.close,
                                    size: 20,
                                  ),
                                ),
                                RawMaterialButton(
                                  constraints: BoxConstraints(),
                                  onPressed: () {
                                    setState(() {
                                      name = crewNameController.text;
                                      ;
                                    });
                                  },
                                  shape: CircleBorder(),
                                  fillColor: ColorConfig.primary,
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            color: crewState.crews
                                        .where((e) => e.selected == true)
                                        .length >
                                    0
                                ? ColorConfig.red1
                                : ColorConfig.red1.withOpacity(0.7),
                            onPressed: () {
                              if (crewState.crews
                                      .where((e) => e.selected == true)
                                      .length >
                                  0)
                                showDialog(
                                  barrierDismissible: false,
                                  context: context,
                                  builder: (dialogContext) =>
                                      BlocProvider<ProjectBloc>.value(
                                    value: context.read<ProjectBloc>(),
                                    child: Dialog(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      backgroundColor: Colors.white,
                                      child: Container(
                                        width: 0,
                                        padding: EdgeInsets.all(20),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                CircleAvatar(
                                                  backgroundColor:
                                                      ColorConfig.orange,
                                                  child: ImageIcon(
                                                    AssetImage(LocalImages
                                                        .exclamation),
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Flexible(
                                                  child: Text(
                                                    'Are you sure you want to delete selected members. All data will be lost.',
                                                    style: TextStyle(
                                                        color:
                                                            ColorConfig.primary,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                FlatButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  color: ColorConfig.grey3,
                                                  child: Text(
                                                    'No',
                                                    style:
                                                        TextStyle(fontSize: 12)
                                                            .copyWith(
                                                                color:
                                                                    ColorConfig
                                                                        .grey2),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(dialogContext)
                                                        .pop();
                                                  },
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                FlatButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  color: ColorConfig.primary,
                                                  child: Text(
                                                    'Yes',
                                                    style:
                                                        TextStyle(fontSize: 12)
                                                            .copyWith(
                                                                color: Colors
                                                                    .white),
                                                  ),
                                                  onPressed: () async {
                                                    Navigator.of(dialogContext)
                                                        .pop();

                                                    // await Future.delayed(
                                                    //     Duration(seconds: 1));

                                                    context
                                                        .read<ProjectBloc>()
                                                        .add(ProjectRemoveMembers(
                                                            userIds: crewState
                                                                .crews
                                                                .where((e) =>
                                                                    e.selected ==
                                                                    true)
                                                                .map(
                                                                    (e) => e.id)
                                                                .toList()));
                                                    // _context
                                                    //     .read<CalenderBloc>()
                                                    //     .add(CalenderDeleteEvent(
                                                    //         calenderEventModel:
                                                    //             event));
                                                  },
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                            },
                            padding: EdgeInsets.symmetric(horizontal: 30),
                            child: Row(
                              children: <Widget>[
                                ImageIcon(
                                  AssetImage(LocalImages.trashAlt),
                                  color: Colors.white,
                                  size: 14,
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  'Delete',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                          children: crewState.crews
                              .where((element) => name != '' && name != null
                                  ? element.name
                                      .toLowerCase()
                                      .contains(name.toLowerCase())
                                  : true)
                              .map((e) => CrewSelectWidget(
                                    crew: crewState.crewIdToCrewMap[e.id],
                                    stateCrew: e,
                                  ))
                              .toList()),

                      SizedBox(
                        height: 40,
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.end,
                      //   children: <Widget>[
                      //     FlatButton(
                      //       shape: RoundedRectangleBorder(
                      //           borderRadius: BorderRadius.circular(20)),
                      //       color: ColorConfig.red1,
                      //       onPressed: () {},
                      //       padding: EdgeInsets.symmetric(horizontal: 30),
                      //       child: Row(
                      //         children: <Widget>[
                      //           ImageIcon(
                      //             AssetImage(LocalImages.trashAlt),
                      //             color: Colors.white,
                      //             size: 14,
                      //           ),
                      //           SizedBox(
                      //             width: 5,
                      //           ),
                      //           Text(
                      //             'Delete',
                      //             style: TextStyle(
                      //                 fontSize: 14,
                      //                 color: Colors.white,
                      //                 fontWeight: FontWeight.w500),
                      //           )
                      //         ],
                      //       ),
                      //     )
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      // // Column(
                      // //     children: projectState.projectModel.idMembers
                      // //         .map((member) =>
                      // //             employeeState.employeeMap[member.id] != null
                      // //                 ? CrewSelectWidget(
                      // //                     employeeState.crewMap[member.id])
                      // //                 : Container())
                      // //         .toList()),
                      // SizedBox(
                      //   height: 40,
                      // ),
                    ],
                  );
                }
                return Container();
              },
            );
          }

          return Container();
        }),
      ),
    );
  }
}

class CrewSelectWidget extends StatelessWidget {
  final CrewModel crew;
  final CrewModel stateCrew;

  CrewSelectWidget({@required this.crew, @required this.stateCrew});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 12,
                offset: Offset(0, 0),
                color: ColorConfig.shadow1)
          ]),
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Row(
        children: <Widget>[
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: ColorConfig.orange,
            ),
            child: Checkbox(
              onChanged: (val) {
                context
                    .read<ProjectCrewsBloc>()
                    .add(ProjectCrewSetSelected(id: crew.id, selected: val));
              },
              value: (stateCrew?.selected ?? false) == true,
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                crew.name,
                style: TextStyle(
                    fontSize: 13,
                    color: Colors.black,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                  children: crew.members
                      .map(
                        (e) => Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: CircleAvatar(
                            backgroundImage:
                                AssetImage(LocalImages.defaultAvatar),
                            radius: 10,
                          ),
                        ),
                      )
                      .toList())
            ],
          ),
          // Padding(
          //   padding: EdgeInsets.all(5),
          //   child: CircleAvatar(
          //     backgroundImage: NetworkImage(this.contact.image),
          //     radius: 15,
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
          // Column(
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: <Widget>[
          //     Text(
          //       contact.name,
          //       style: TextStyle(
          //           color: Colors.black,
          //           fontSize: 13,
          //           fontWeight: FontWeight.w400),
          //     ),
          //     SizedBox(
          //       height: 2,
          //     ),
          //     Text(
          //       contact.position,
          //       style: TextStyle(
          //           color: grey7, fontSize: 12, fontWeight: FontWeight.w400),
          //     ),
          //   ],
          // ),
          Spacer(),

          // RawMaterialButton(
          //   constraints: BoxConstraints(),
          //   padding: EdgeInsets.all(12),
          //   shape: CircleBorder(),
          //   onPressed: () {},
          //   child: ImageIcon(
          //     AssetImage(LocalImages.edit),
          //     color: ColorConfig.primary,
          //     size: 16,
          //   ),
          // ),
          RawMaterialButton(
            constraints: BoxConstraints(),
            padding: EdgeInsets.all(12),
            shape: CircleBorder(),
            onPressed: () {},
            child: ImageIcon(
              AssetImage(LocalImages.trashAlt),
              color: ColorConfig.red1,
              size: 16,
            ),
          ),
        ],
      ),
    );
  }
}
