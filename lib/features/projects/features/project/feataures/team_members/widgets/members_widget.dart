import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/bloc/project_members/project_members_bloc.dart';
import 'package:Subtraid/features/projects/features/project/models/project_member_id_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/bloc/project_permission_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_role_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MembersWidget extends StatefulWidget {
  @override
  _MembersWidgetState createState() => _MembersWidgetState();
}

class _MembersWidgetState extends State<MembersWidget> {
  TextEditingController nameController = TextEditingController();

  final GlobalKey<FormFieldState> _dropDownKey = GlobalKey<FormFieldState>();

  String name;
  String role;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProjectMembersBloc>(
      create: (context) => ProjectMembersBloc(
          // employeeBloc: context.read<EmployeeBloc>(),
          projectBloc: context.read<ProjectBloc>()),
      child: BlocBuilder<ProjectMembersBloc, ProjectMembersState>(
        builder: (context, state) {
          if (state is ProjectMembersLoadSuccess) {
            return ListView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  decoration: BoxDecoration(
                      color: ColorConfig.blue4,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Member name',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.black,
                              fontSize: 12)),
                      SizedBox(
                        height: height2,
                      ),
                      TextFormField(
                        controller: nameController,
                        onChanged: (val) {},
                        decoration: profileInputDecoration.copyWith(
                            prefixIcon: Icon(
                              Icons.search,
                              color: ColorConfig.primary,
                            ),
                            labelText: 'Search by name',
                            labelStyle: TextStyle(
                                fontSize: 14, color: ColorConfig.black1)),
                        maxLines: null,
                        style: textStyle2,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text('User Role',
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.black,
                              fontSize: 12)),
                      SizedBox(
                        height: height2,
                      ),
                      BlocBuilder<ProjectPermissionBloc,
                          ProjectPermissionState>(
                        builder: (context, state) {
                          return DropdownButtonFormField(
                            key: _dropDownKey,
                            iconSize: 20,
                            dropdownColor: ColorConfig.primary,
                            icon: Icon(Icons.keyboard_arrow_down),
                            decoration: profileInputDecoration.copyWith(
                                hintText: 'Select user role',
                                hintStyle: TextStyle(
                                    fontSize: 14, color: ColorConfig.black1)),
                            items: (state is ProjectPermissionLoadSuccess)
                                ? state.companyInfoModel.company
                                    .projectPermissionGroups
                                    .map((e) => (DropdownMenuItem(
                                          value: e.id,
                                          child: Text(
                                            e.name,
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        )))
                                    .toList()
                                : [],
                            style: TextStyle(fontSize: 14),
                            selectedItemBuilder: (context) => (state
                                    is ProjectPermissionLoadSuccess)
                                ? state.companyInfoModel.company
                                    .projectPermissionGroups
                                    .map((e) => (Text(e.name,
                                        style: TextStyle(color: Colors.black))))
                                    .toList()
                                : [],
                            onChanged: (val) {},
                          );
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          RawMaterialButton(
                            constraints: BoxConstraints(),
                            onPressed: () {
                              _dropDownKey.currentState.reset();
                              nameController.clear();
                              setState(() {
                                name = null;
                                role = null;
                              });
                            },
                            shape: CircleBorder(),
                            fillColor: ColorConfig.grey6,
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.close,
                              size: 20,
                            ),
                          ),
                          RawMaterialButton(
                            constraints: BoxConstraints(),
                            onPressed: () {
                              setState(() {
                                name = nameController.text;
                                role = _dropDownKey.currentState.value;
                              });
                            },
                            shape: CircleBorder(),
                            fillColor: ColorConfig.primary,
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.check,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: state.members
                                  .where((e) => e.selected == true)
                                  .length >
                              0
                          ? ColorConfig.red1
                          : ColorConfig.red1.withOpacity(0.7),
                      onPressed: () {
                        if (state.members
                                .where((e) => e.selected == true)
                                .length >
                            0)
                          showDialog(
                            barrierDismissible: false,
                            context: context,
                            builder: (dialogContext) =>
                                BlocProvider<ProjectBloc>.value(
                              value: context.read<ProjectBloc>(),
                              child: Dialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                backgroundColor: Colors.white,
                                child: Container(
                                  width: 0,
                                  padding: EdgeInsets.all(20),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor: ColorConfig.orange,
                                            child: ImageIcon(
                                              AssetImage(
                                                  LocalImages.exclamation),
                                              color: Colors.white,
                                              size: 20,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Flexible(
                                            child: Text(
                                              'Are you sure you want to delete selected members. All data will be lost.',
                                              style: TextStyle(
                                                  color: ColorConfig.primary,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          FlatButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            color: ColorConfig.grey3,
                                            child: Text(
                                              'No',
                                              style: TextStyle(fontSize: 12)
                                                  .copyWith(
                                                      color: ColorConfig.grey2),
                                            ),
                                            onPressed: () {
                                              Navigator.of(dialogContext).pop();
                                            },
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          FlatButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            color: ColorConfig.primary,
                                            child: Text(
                                              'Yes',
                                              style: TextStyle(fontSize: 12)
                                                  .copyWith(
                                                      color: Colors.white),
                                            ),
                                            onPressed: () async {
                                              Navigator.of(dialogContext).pop();

                                              // await Future.delayed(
                                              //     Duration(seconds: 1));

                                              context.read<ProjectBloc>().add(
                                                  ProjectRemoveMembers(
                                                      userIds: state.members
                                                          .where((e) =>
                                                              e.selected ==
                                                              true)
                                                          .map((e) => e.id)
                                                          .toList()));
                                              // _context
                                              //     .read<CalenderBloc>()
                                              //     .add(CalenderDeleteEvent(
                                              //         calenderEventModel:
                                              //             event));
                                            },
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                      },
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        children: <Widget>[
                          ImageIcon(
                            AssetImage(LocalImages.trashAlt),
                            color: Colors.white,
                            size: 14,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Delete',
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                    children: state.members
                        .where((element) => name != '' && name != null
                            ? element.name
                                .toLowerCase()
                                .contains(name.toLowerCase())
                            : true)
                        .where((element) => role != '' && role != null
                            ? state.memberIdToPermissionMap[element.id]
                                    .permission.id ==
                                role
                            : true)
                        .map((e) => UserSelectWidget(
                              member: e,
                              role: state.memberIdToPermissionMap[e.id],
                            ))
                        .toList()),
                SizedBox(
                  height: 40,
                ),
              ],
            );
          }

          return Container();
        },
      ),
    );
  }
}

class UserSelectWidget extends StatelessWidget {
  final ProjectMemberIdModel member;
  final ProjectPermissionRoleModel role;
  UserSelectWidget({@required this.member, @required this.role});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 12,
                offset: Offset(0, 0),
                color: ColorConfig.shadow1)
          ]),
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Row(
        children: <Widget>[
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: ColorConfig.orange,
            ),
            child: Checkbox(
              onChanged: (val) {
                context
                    .read<ProjectMembersBloc>()
                    .add(MemberSetSelected(id: member.id, selected: val));
              },
              value: (member?.selected ?? false) == true,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(5),
            child: CircleAvatar(
              backgroundImage: AssetImage(LocalImages.defaultAvatar),
              radius: 15,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                member.name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                role?.permission?.name ?? '',
                style: TextStyle(
                    color: ColorConfig.grey7,
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
              ),
            ],
          ),
          Spacer(),
          // RawMaterialButton(
          //   constraints: BoxConstraints(),
          //   padding: EdgeInsets.all(12),
          //   shape: CircleBorder(),
          //   onPressed: () {},
          //   child: ImageIcon(
          //     AssetImage(LocalImages.edit),
          //     color: ColorConfig.primary,
          //     size: 16,
          //   ),
          // ),
          RawMaterialButton(
            constraints: BoxConstraints(),
            padding: EdgeInsets.all(12),
            shape: CircleBorder(),
            onPressed: () {
              context
                  .read<ProjectBloc>()
                  .add(ProjectRemoveMember(userId: member.id));
            },
            child: ImageIcon(
              AssetImage(LocalImages.trashAlt),
              color: ColorConfig.red1,
              size: 16,
            ),
          ),
        ],
      ),
    );
  }
}
