part of 'project_crews_bloc.dart';

abstract class ProjectCrewsState extends Equatable {
  const ProjectCrewsState();

  @override
  List<Object> get props => [];
}

class ProjectCrewsBlocInitial extends ProjectCrewsState {}

class ProjectCrewsLoadSuccess extends ProjectCrewsState {
  final List<CrewModel> crews;
  final Map<String, CrewModel> crewIdToCrewMap;

  ProjectCrewsLoadSuccess(
      {@required this.crews, @required this.crewIdToCrewMap});

  @override
  List<Object> get props => [crews, crewIdToCrewMap, crews.hashCode];
}
