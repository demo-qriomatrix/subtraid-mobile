part of 'project_members_bloc.dart';

abstract class ProjectMembersEvent extends Equatable {
  const ProjectMembersEvent();

  @override
  List<Object> get props => [];
}

class ProjectMembersRequested extends ProjectMembersEvent {}

class MemberSetSelected extends ProjectMembersEvent {
  final bool selected;
  final String id;

  MemberSetSelected({@required this.id, @required this.selected});
}
