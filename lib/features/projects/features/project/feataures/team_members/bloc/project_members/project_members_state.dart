part of 'project_members_bloc.dart';

abstract class ProjectMembersState extends Equatable {
  const ProjectMembersState();

  @override
  List<Object> get props => [];
}

class ProjectMembersBlocInitial extends ProjectMembersState {}

class ProjectMembersLoadSuccess extends ProjectMembersState {
  final List<ProjectMemberIdModel> members;
  final Map<String, ProjectPermissionRoleModel> memberIdToPermissionMap;

  ProjectMembersLoadSuccess(
      {@required this.members, @required this.memberIdToPermissionMap});

  @override
  List<Object> get props =>
      [members, memberIdToPermissionMap, members.hashCode];
}
