import 'dart:async';

import 'package:Subtraid/features/more/company/features/crew/bloc/crew/crew_bloc.dart';
import 'package:Subtraid/features/more/company/features/crew/models/crew_model.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'project_crews_event.dart';
part 'project_crews_state.dart';

class ProjectCrewsBloc extends Bloc<ProjectCrewsEvent, ProjectCrewsState> {
  ProjectCrewsBloc({
    @required this.projectBloc,
    @required this.crewBloc,
  }) : super(ProjectCrewsBlocInitial()) {
    add(ProjectCrewsRequested());

    loadCrew();

    projectBloc.listen((state) {
      if (state is ProjectLoadSuccess) {
        add(ProjectCrewsRequested());
      }
    });
    crewBloc.listen((state) {
      if (state is CrewLoadSuccess) {
        add(ProjectCrewsRequested());
      }
    });
  }

  void loadCrew() {
    crewBloc.add(CrewRequested());
  }

  final ProjectBloc projectBloc;
  final CrewBloc crewBloc;

  @override
  Stream<ProjectCrewsState> mapEventToState(
    ProjectCrewsEvent event,
  ) async* {
    if (event is ProjectCrewsRequested) {
      yield* _mapProjectCrewsRequestedToState();
    }
    if (event is ProjectCrewSetSelected) {
      yield* _mapProjectCrewSetSelectedToState(event);
    }
  }

  Stream<ProjectCrewsState> _mapProjectCrewSetSelectedToState(
      ProjectCrewSetSelected event) async* {
    if (state is ProjectCrewsLoadSuccess) {
      List<CrewModel> previousProjectCrews =
          (state as ProjectCrewsLoadSuccess).crews;

      int index =
          previousProjectCrews.indexWhere((element) => element.id == event.id);

      CrewModel crew = previousProjectCrews[index];

      crew = crew.rebuild((t) => t..selected = event.selected);

      previousProjectCrews[index] = crew;

      List<CrewModel> updatedProjectCrews =
          List.from(previousProjectCrews.toList());

      yield ProjectCrewsLoadSuccess(
          crews: updatedProjectCrews,
          crewIdToCrewMap: (state as ProjectCrewsLoadSuccess).crewIdToCrewMap);
    }
  }

  Stream<ProjectCrewsState> _mapProjectCrewsRequestedToState() async* {
    ProjectState projectState = projectBloc.state;
    CrewState crewState = crewBloc.state;

    List<CrewModel> crews = [];
    List<CrewModel> updatedCrews = [];

    Map<String, CrewModel> crewIdToCrewMap = Map();

    if (projectState is ProjectLoadSuccess) {
      crews = projectState.projectModel.idCrews
          .map((e) => CrewModel((crew) => crew.id = e))
          .toList();
    }

    if (crewState is CrewLoadSuccess) {
      crewState.companyInfo.company.crews.forEach((crew) {
        crewIdToCrewMap.putIfAbsent(crew.id, () => crew);
      });
    }

    crews.forEach((element) {
      if (crewIdToCrewMap[element.id] != null) {
        element =
            element.rebuild((e) => e..name = crewIdToCrewMap[element.id].name);

        updatedCrews.add(element);
      }
    });

    yield ProjectCrewsLoadSuccess(
        crews: updatedCrews, crewIdToCrewMap: crewIdToCrewMap);
  }
}
