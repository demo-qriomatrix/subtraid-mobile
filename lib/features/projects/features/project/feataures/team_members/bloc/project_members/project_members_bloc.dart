import 'dart:async';

import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/models/project_member_id_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_role_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'project_members_event.dart';
part 'project_members_state.dart';

class ProjectMembersBloc
    extends Bloc<ProjectMembersEvent, ProjectMembersState> {
  ProjectMembersBloc({
    @required this.projectBloc,
  }) : super(ProjectMembersBlocInitial()) {
    add(ProjectMembersRequested());

    projectBloc.listen((state) {
      if (state is ProjectLoadSuccess) {
        add(ProjectMembersRequested());
      }
    });
  }

  final ProjectBloc projectBloc;

  @override
  Stream<ProjectMembersState> mapEventToState(
    ProjectMembersEvent event,
  ) async* {
    if (event is ProjectMembersRequested) {
      yield* _mapProjectMembersRequestedToState();
    }
    if (event is MemberSetSelected) {
      yield* _mapMemberSetSelectedToState(event);
    }
  }

  Stream<ProjectMembersState> _mapMemberSetSelectedToState(
      MemberSetSelected event) async* {
    if (state is ProjectMembersLoadSuccess) {
      List<ProjectMemberIdModel> previousProjectMembers =
          (state as ProjectMembersLoadSuccess).members;

      int index = previousProjectMembers
          .indexWhere((element) => element.id == event.id);

      ProjectMemberIdModel member = previousProjectMembers[index];

      member = member.rebuild((t) => t..selected = event.selected);

      previousProjectMembers[index] = member;

      List<ProjectMemberIdModel> updatedProjectMembers =
          List.from(previousProjectMembers.toList());

      yield ProjectMembersLoadSuccess(
          members: updatedProjectMembers,
          memberIdToPermissionMap:
              (state as ProjectMembersLoadSuccess).memberIdToPermissionMap);
    }
  }

  Stream<ProjectMembersState> _mapProjectMembersRequestedToState() async* {
    ProjectState projectState = projectBloc.state;

    List<ProjectMemberIdModel> members = [];
    Map<String, ProjectPermissionRoleModel> memberIdToPermissionMap = Map();

    if (projectState is ProjectLoadSuccess) {
      members = projectState.projectModel.idMembers.toList();

      if (projectState.projectModel.permissions != null) {
        projectState.projectModel.permissions.forEach((permission) {
          memberIdToPermissionMap.putIfAbsent(
              permission.idMember, () => permission);
        });
      }
    }

    yield ProjectMembersLoadSuccess(
        members: members, memberIdToPermissionMap: memberIdToPermissionMap);
  }
}
