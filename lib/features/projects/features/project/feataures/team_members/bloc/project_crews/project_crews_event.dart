part of 'project_crews_bloc.dart';

abstract class ProjectCrewsEvent extends Equatable {
  const ProjectCrewsEvent();

  @override
  List<Object> get props => [];
}

class ProjectCrewsRequested extends ProjectCrewsEvent {}

class ProjectCrewSetSelected extends ProjectCrewsEvent {
  final bool selected;
  final String id;

  ProjectCrewSetSelected({@required this.id, @required this.selected});
}
