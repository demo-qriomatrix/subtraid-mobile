import 'package:Subtraid/core/config/local_images.dart';
import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/enums/saved_templates_views.enum.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';

class SavedTemplatesScreen extends StatefulWidget {
  @override
  _SavedTemplatesScreenState createState() => _SavedTemplatesScreenState();
}

class _SavedTemplatesScreenState extends State<SavedTemplatesScreen> {
  SavedTemplatesViewEnum savedTemplatesViewEnum = SavedTemplatesViewEnum.list;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConfig.primary,
      appBar: StAppBar(
        title: 'Saved Templates',
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: TextField(
              keyboardType: TextInputType.text,
              decoration: searchInputDecoration.copyWith(
                  hintText: 'Search by template name'),
            ),
          ),
          Stack(
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 24),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: ConstrainedBox(
                      constraints: BoxConstraints(
                          minHeight: MediaQuery.of(context).size.height -
                              142 -
                              kToolbarHeight -
                              MediaQuery.of(context).padding.top),
                      child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Stack(
                            children: <Widget>[
                              Offstage(
                                offstage: savedTemplatesViewEnum !=
                                    SavedTemplatesViewEnum.add,
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 40,
                                    ),
                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 20, horizontal: 10),
                                        decoration: BoxDecoration(
                                            color: ColorConfig.blue4,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minHeight: getMinHeight(context)),
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text('Create Board Templates',
                                                    style: TextStyle(
                                                        color:
                                                            ColorConfig.primary,
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w500)),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text('Board Name',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Colors.black,
                                                        fontSize: 12)),
                                                SizedBox(
                                                  height: height2,
                                                ),
                                                TextField(
                                                  decoration:
                                                      profileInputDecoration
                                                          .copyWith(
                                                    hintText: 'Type Board name',
                                                  ),
                                                  maxLines: null,
                                                  style: textStyle2,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text('Location',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Colors.black,
                                                        fontSize: 12)),
                                                SizedBox(
                                                  height: height2,
                                                ),
                                                TextField(
                                                  decoration:
                                                      profileInputDecoration
                                                          .copyWith(
                                                    hintText: 'Type Location',
                                                  ),
                                                  maxLines: null,
                                                  style: textStyle2,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Checkbox(
                                                      value: false,
                                                      onChanged: (val) {},
                                                    ),
                                                    Text(
                                                      'Save as New Project',
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          color: ColorConfig
                                                              .black2,
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 40,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    CancelButton(
                                                      onPress: () {},
                                                    ),
                                                    SizedBox(
                                                      width: 20,
                                                    ),
                                                    SaveButton('Create', () {})
                                                  ],
                                                )
                                              ]),
                                        ))
                                  ],
                                ),
                              ),
                              Offstage(
                                offstage: savedTemplatesViewEnum !=
                                    SavedTemplatesViewEnum.list,
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 40,
                                    ),
                                    SavedTemplateRowItem('Default Template'),
                                    SavedTemplateRowItem(
                                        'Project Manager Template'),
                                    SavedTemplateRowItem(
                                        'Default Construction Template'),
                                    SavedTemplateRowItem('Default Template'),
                                    SavedTemplateRowItem(
                                        'Project Manager Template'),
                                    SavedTemplateRowItem(
                                        'Default Construction Template'),
                                  ],
                                ),
                              ),
                            ],
                          )))),
              Positioned(
                  right: 50,
                  child: Row(children: <Widget>[
                    RawMaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      constraints: BoxConstraints(),
                      fillColor:
                          savedTemplatesViewEnum == SavedTemplatesViewEnum.list
                              ? ColorConfig.orange
                              : Colors.white,
                      onPressed: () {
                        setState(() {
                          savedTemplatesViewEnum = SavedTemplatesViewEnum.list;
                        });
                      },
                      padding: EdgeInsets.all(5),
                      child: ImageIcon(
                        AssetImage(LocalImages.clipboardList),
                        color: savedTemplatesViewEnum ==
                                SavedTemplatesViewEnum.list
                            ? Colors.white
                            : ColorConfig.primary,
                        size: 18,
                      ),
                    ),
                    RawMaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      constraints: BoxConstraints(),
                      fillColor: Colors.white,
                      onPressed: () {},
                      padding: EdgeInsets.all(5),
                      child: ImageIcon(
                        AssetImage(LocalImages.userCog),
                        color: savedTemplatesViewEnum !=
                                SavedTemplatesViewEnum.list
                            ? Colors.white
                            : ColorConfig.primary,
                        size: 18,
                      ),
                    ),
                  ])),
              Positioned(
                  left: 50,
                  child: Row(children: <Widget>[
                    RawMaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      constraints: BoxConstraints(),
                      fillColor:
                          savedTemplatesViewEnum == SavedTemplatesViewEnum.add
                              ? ColorConfig.orange
                              : Colors.white,
                      onPressed: () {
                        setState(() {
                          savedTemplatesViewEnum = SavedTemplatesViewEnum.add;
                        });
                      },
                      padding: EdgeInsets.all(5),
                      child: ImageIcon(
                        AssetImage(LocalImages.plus),
                        color:
                            savedTemplatesViewEnum == SavedTemplatesViewEnum.add
                                ? Colors.white
                                : ColorConfig.primary,
                        size: 18,
                      ),
                    ),
                  ])),
            ],
          )
        ],
      ),
    );
  }
}

class SavedTemplateRowItem extends StatelessWidget {
  final String title;

  SavedTemplateRowItem(this.title);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        children: <Widget>[
          ImageIcon(
            AssetImage(LocalImages.stickyNote),
            color: ColorConfig.primary,
            size: 16,
          ),
          SizedBox(
            width: 20,
          ),
          Text(
            title,
            style: TextStyle(
                color: ColorConfig.blue13,
                fontSize: 14,
                fontWeight: FontWeight.w400),
          ),
          Spacer(),
          RawMaterialButton(
            fillColor: ColorConfig.primary,
            constraints: BoxConstraints(),
            shape: CircleBorder(),
            padding: EdgeInsets.all(3),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onPressed: () {},
            child: Icon(
              Icons.add,
              color: Colors.white,
              size: 16,
            ),
          )
        ],
      ),
    );
  }
}
