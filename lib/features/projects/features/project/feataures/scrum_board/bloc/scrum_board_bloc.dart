import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'scrum_board_event.dart';
part 'scrum_board_state.dart';

class ScrumBoardBloc extends Bloc<ScrumBoardEvent, ScrumBoardState> {
  ScrumBoardBloc() : super(ScrumBoardState(selectedIndex: 0));

  @override
  Stream<ScrumBoardState> mapEventToState(
    ScrumBoardEvent event,
  ) async* {
    if (event is ScrumBoradIndexChanged) {
      yield ScrumBoardState(selectedIndex: event.index);
    }
  }
}
