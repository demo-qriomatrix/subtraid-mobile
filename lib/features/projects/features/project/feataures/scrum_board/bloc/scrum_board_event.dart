part of 'scrum_board_bloc.dart';

abstract class ScrumBoardEvent extends Equatable {
  const ScrumBoardEvent();

  @override
  List<Object> get props => [];
}

class ScrumBoradIndexChanged extends ScrumBoardEvent {
  final int index;
  ScrumBoradIndexChanged({@required this.index});
}
