part of 'scrum_board_bloc.dart';

class ScrumBoardState extends Equatable {
  final int selectedIndex;

  ScrumBoardState({@required this.selectedIndex});

  @override
  List<Object> get props => [selectedIndex];
}
