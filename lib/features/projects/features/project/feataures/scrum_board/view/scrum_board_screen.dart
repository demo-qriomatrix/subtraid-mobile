import 'package:Subtraid/core/config/local_images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/duplicate_board/view/duplicate_board_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/saved_templates/view/saved_templates_screen.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/bloc/scrum_board_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/view/scrum_board_screen.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';

class ScrumBoardWidget extends StatefulWidget {
  @override
  _ScrumBoardWidgetState createState() => _ScrumBoardWidgetState();
}

class _ScrumBoardWidgetState extends State<ScrumBoardWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      child: BlocProvider<ScrumBoardBloc>(
        create: (context) => ScrumBoardBloc(),
        child: BlocBuilder<ProjectBloc, ProjectState>(
          builder: (context, state) {
            if (state is ProjectLoadFailure) {
              return Column(
                children: [
                  Expanded(
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 12),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(50),
                                  topRight: Radius.circular(50))),
                          child: Container(
                            child: Center(child: Text('Something went wrong')),
                          )))
                ],
              );
            }

            if (state is ProjectLoadInProgress) {
              return Column(
                children: [
                  // StAppBar(
                  //   // leading: Row(
                  //   //   children: [
                  //   //     BackButton(),
                  //   //     // IconButton(icon: Icon(Icons.menu), onPressed: () {})
                  //   //   ],
                  //   // ),
                  //   title: '',
                  // ),
                  Expanded(
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 12),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(50),
                                  topRight: Radius.circular(50))),
                          child: Container(
                            child: Center(child: CircularProgressIndicator()),
                          )))
                ],
              );
            }

            if (state is ProjectLoadSuccess) {
              return Column(
                children: [
                  // StAppBar(
                  //   // leading: Row(
                  //   //   children: [
                  //   //     BackButton(),
                  //   //     // IconButton(icon: Icon(Icons.menu), onPressed: () {})
                  //   //   ],
                  //   // ),
                  //   title: (state is ProjectLoadSuccess)
                  //       ? state.projectModel.name
                  //       : '',
                  // ),

                  Expanded(
                      child: BlocBuilder<ScrumBoardBloc, ScrumBoardState>(
                    builder: (context, scrumState) => Stack(children: <Widget>[
                      Positioned.fill(
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 12),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(50),
                                    topRight: Radius.circular(50))),
                            child: [
                              ScrumBoardCardsWidget(),
                              DuplicateProjectScreen(),
                            ][scrumState.selectedIndex]),
                      ),
                      Positioned(
                          top: 0,
                          right: 50,
                          child: Row(children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: RawMaterialButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                constraints: BoxConstraints(),
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                fillColor: scrumState.selectedIndex == 3
                                    ? ColorConfig.orange
                                    : Colors.white,
                                onPressed: () {},
                                padding: EdgeInsets.all(5),
                                child: ImageIcon(
                                  AssetImage(LocalImages.filter),
                                  color: scrumState.selectedIndex == 3
                                      ? Colors.white
                                      : ColorConfig.primary,
                                  size: 18,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: RawMaterialButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                constraints: BoxConstraints(),
                                fillColor: Colors.white,
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        SavedTemplatesScreen(),
                                  ));
                                },
                                padding: EdgeInsets.all(5),
                                child: ImageIcon(
                                  AssetImage(LocalImages.save),
                                  color: scrumState.selectedIndex == 3
                                      ? Colors.white
                                      : ColorConfig.primary,
                                  size: 18,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: RawMaterialButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                constraints: BoxConstraints(),
                                fillColor: scrumState.selectedIndex == 1
                                    ? ColorConfig.orange
                                    : Colors.white,
                                onPressed: () {
                                  if (scrumState.selectedIndex != 1) {
                                    context
                                        .read<ScrumBoardBloc>()
                                        .add(ScrumBoradIndexChanged(index: 1));
                                  } else {
                                    context
                                        .read<ScrumBoardBloc>()
                                        .add(ScrumBoradIndexChanged(index: 0));
                                  }
                                },
                                padding: EdgeInsets.all(5),
                                child: ImageIcon(
                                  AssetImage(LocalImages.copy),
                                  color: scrumState.selectedIndex == 1
                                      ? Colors.white
                                      : ColorConfig.primary,
                                  size: 18,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: RawMaterialButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                constraints: BoxConstraints(),
                                fillColor: Colors.white,
                                onPressed: () {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (_context) => Dialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                            backgroundColor: Colors.white,
                                            child: Container(
                                              width: 0,
                                              padding: EdgeInsets.all(20),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  Row(
                                                    children: <Widget>[
                                                      CircleAvatar(
                                                        backgroundColor:
                                                            ColorConfig.orange,
                                                        child: ImageIcon(
                                                          AssetImage(LocalImages
                                                              .exclamation),
                                                          color: Colors.white,
                                                          size: 20,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 20,
                                                      ),
                                                      Flexible(
                                                        child: Text(
                                                          'Are you sure you want to delete this project. All data will be lost.',
                                                          style: TextStyle(
                                                              color: ColorConfig
                                                                  .primary,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: <Widget>[
                                                      FlatButton(
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5)),
                                                        color:
                                                            ColorConfig.grey3,
                                                        child: Text(
                                                          'No',
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color: ColorConfig
                                                                  .grey2),
                                                        ),
                                                        onPressed: () {
                                                          Navigator.of(_context)
                                                              .pop();
                                                        },
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      FlatButton(
                                                        shape:
                                                            RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5)),
                                                        color:
                                                            ColorConfig.primary,
                                                        child: Text(
                                                          'Yes',
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                        onPressed: () {
                                                          context
                                                              .read<
                                                                  ProjectBloc>()
                                                              .add(ProjectDeleteProject(
                                                                  projectId: state
                                                                      .projectId));

                                                          Navigator.of(_context)
                                                              .pop();
                                                        },
                                                      )
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                          ));
                                },
                                padding: EdgeInsets.all(5),
                                child: ImageIcon(
                                  AssetImage(LocalImages.trashAlt),
                                  color: ColorConfig.red1,
                                  size: 18,
                                ),
                              ),
                            ),
                          ]))
                    ]),
                  ))

                  // Expanded(
                  //     child: ListView(
                  //   children: [
                  //     Container(
                  //         decoration: BoxDecoration(
                  //             color: Colors.white,
                  //             borderRadius: BorderRadius.only(
                  //                 topLeft: Radius.circular(50),
                  //                 topRight: Radius.circular(50))),
                  //         child: ConstrainedBox(
                  //             constraints: BoxConstraints(
                  //               minHeight: getMinHeight(context),
                  //             ),
                  //             child: Padding(
                  //                 padding: const EdgeInsets.symmetric(
                  //                     horizontal: 30),
                  //                 child: Column(children: <Widget>[

                  //                 ]))))
                  //   ],
                  // ))
                ],
              );
            }

            return Column(
              children: [
                StAppBar(
                  // leading: Row(
                  //   children: [
                  //     BackButton(),
                  //     // IconButton(icon: Icon(Icons.menu), onPressed: () {})
                  //   ],
                  // ),
                  title: (state is ProjectLoadSuccess)
                      ? state.projectModel.name
                      : '',
                ),
                Expanded(
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 12),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(50),
                                topRight: Radius.circular(50))),
                        child: Container()))
              ],
            );
          },
        ),
      ),
    );
  }
}
