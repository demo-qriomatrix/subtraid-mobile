import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class CreateCardWidget extends StatefulWidget {
  final Function(String) onCreate;
  final String type;
  final Color color;

  CreateCardWidget(
      {@required this.onCreate, @required this.type, @required this.color});

  @override
  _CreateCardWidgetState createState() => _CreateCardWidgetState();
}

class _CreateCardWidgetState extends State<CreateCardWidget> {
  TextEditingController cardTitleTextEditingController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15),
        decoration: BoxDecoration(
          color: widget.color,
          borderRadius: BorderRadius.circular(13),
        ),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Create ${widget.type}',
                    style: TextStyle(
                        color: ColorConfig.primary,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: cardTitleTextEditingController,
                    decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding: EdgeInsets.only(left: 15),
                        hintText: '${widget.type} Title',
                        hintStyle:
                            TextStyle(fontSize: 12, color: ColorConfig.black1),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide.none)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        color: ColorConfig.primary,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        onPressed: () {
                          FocusScope.of(context).unfocus();

                          this
                              .widget
                              .onCreate(cardTitleTextEditingController.text);
                        },
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        child: Text(
                          'Create',
                          style: TextStyle(fontSize: 14, color: Colors.white),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
