import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/models/project_label_model.dart';
import 'package:flutter/material.dart';

class CardLabelWidget extends StatelessWidget {
  final String color;

  CardLabelWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: fromHex(color), borderRadius: BorderRadius.circular(20)),
      height: 5,
      margin: EdgeInsets.only(right: 5),
      width: MediaQuery.of(context).size.width * 0.1,
    );
  }
}

class CardLabelWithNameWidget extends StatelessWidget {
  final ProjectLabelModel label;

  CardLabelWithNameWidget(this.label);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: fromHex(label.color), borderRadius: BorderRadius.circular(20)),
      // height: 5,
      margin: EdgeInsets.only(right: 5),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      constraints: BoxConstraints(minWidth: 60),
      // width: MediaQuery.of(context).size.width * 0.1,
      child: Text(
        label.name,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: fromHex(label.color).computeLuminance() > 0.5
                ? Colors.black
                : Colors.white),
      ),
    );
  }
}

class CardLabelDropDown extends StatelessWidget {
  final ProjectLabelModel label;
  final bool checked;

  CardLabelDropDown({@required this.label, @required this.checked});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          backgroundColor: fromHex(label.color),
        ),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Text(
            label.name,
            style: TextStyle(fontWeight: FontWeight.w500),
          ),
        ),

        checked
            ? Icon(
                Icons.done,
                color: ColorConfig.primary,
              )
            : Container()
        // Container(
        //   decoration: BoxDecoration(
        //       color: fromHex(label.color),
        //       borderRadius: BorderRadius.circular(20)),
        //   // height: 5,
        //   margin: EdgeInsets.only(right: 5),
        //   padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        //   constraints: BoxConstraints(minWidth: 60),
        //   // width: MediaQuery.of(context).size.width * 0.1,
        //   child: Text(
        //     label.name,
        //     textAlign: TextAlign.center,
        //     style: TextStyle(
        //         color: fromHex(label.color).computeLuminance() > 0.5
        //             ? Colors.black
        //             : Colors.white),
        //   ),
        // ),
      ],
    );
  }
}

class CardLabelChipWidget extends StatelessWidget {
  final ProjectLabelModel label;
  final VoidCallback onDelete;

  CardLabelChipWidget(this.label, this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Chip(
          backgroundColor: fromHex(label.color),
          deleteIconColor: fromHex(label.color).computeLuminance() > 0.5
              ? Colors.black
              : Colors.white,
          onDeleted: () {
            onDelete();
          },
          label: Text(
            label.name,
            style: TextStyle(
                color: fromHex(label.color).computeLuminance() > 0.5
                    ? Colors.black
                    : Colors.white),
          )),
    );
  }
}

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}
