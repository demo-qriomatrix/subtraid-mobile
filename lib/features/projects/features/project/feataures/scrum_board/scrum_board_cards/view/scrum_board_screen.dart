import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/add_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/widgets/card_column_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/widgets/create_card_widget.dart';
import 'package:Subtraid/features/shared/styles.dart';

class ScrumBoardCardsWidget extends StatefulWidget {
  @override
  _ScrumBoardCardsWidgetState createState() => _ScrumBoardCardsWidgetState();
}

class _ScrumBoardCardsWidgetState extends State<ScrumBoardCardsWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: BlocBuilder<ProjectBloc, ProjectState>(
        builder: (context, state) {
          if (state is ProjectLoadInProgress) {
            return SizedBox(
                height: getMinHeight(context),
                child: Center(child: CircularProgressIndicator()));
          }
          if (state is ProjectLoadFailure) {
            return SizedBox(
                height: getMinHeight(context),
                child: Center(
                    child: Text(
                  'Something went wrong',
                  style: TextStyle(color: Colors.red, fontSize: 20),
                )));
          }

          if (state is ProjectLoadSuccess) {
            return Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    ...state.projectModel.idMembers
                        .sublist(
                            0,
                            state.projectModel.idMembers.length >= 5
                                ? 5
                                : state.projectModel.idMembers.length)
                        .map(
                          (member) => Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: CircleAvatar(
                              backgroundImage:
                                  AssetImage(LocalImages.defaultAvatar),
                              radius: 20,
                            ),
                          ),
                        ),
                    Spacer(),
                    (context.watch<ProjectBloc>().state
                                is ProjectLoadSuccess) &&
                            (context.watch<ProjectBloc>().state
                                    as ProjectLoadSuccess)
                                .projectModel
                                .loginUserProjectPermission
                                .list
                                .create
                        ? RawMaterialButton(
                            padding: EdgeInsets.all(5),
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            shape: CircleBorder(),
                            constraints: BoxConstraints(),
                            onPressed: () {
                              context.read<ProjectBloc>().add(
                                  ProjectToggleShowAddList(
                                      showAdd: !state.showAddList));
                            },
                            fillColor: state.showAddList
                                ? ColorConfig.orange
                                : Colors.white,
                            child: Icon(Icons.add,
                                color: state.showAddList
                                    ? Colors.white
                                    : ColorConfig.orange),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(height: 10),
                state.showAddList
                    ? CreateCardWidget(
                        color: ColorConfig.blue4,
                        type: 'List',
                        onCreate: (String name) {
                          AddListPostModel addListModel =
                              AddListPostModel((addListModel) => addListModel
                                ..projectId = state.projectModel.id
                                ..name = name ?? 'Untitled List');

                          context
                              .read<ProjectBloc>()
                              .add(ProjectAddList(list: addListModel));
                        },
                      )
                    : Container(),
                Expanded(
                  child: Container(
                      child: Container(
                          padding: EdgeInsets.only(
                            top: 15,
                          ),
                          decoration: BoxDecoration(
                              color: ColorConfig.blue4,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              state.projectModel.lists.length > 0
                                  ? SmoothPageIndicator(
                                      onDotClicked: (index) {
                                        state.pageController.jumpToPage(index);
                                      },
                                      controller: state.pageController,
                                      count: state.projectModel.lists.length,
                                      effect: ScrollingDotsEffect(
                                          maxVisibleDots: 7,
                                          spacing: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.035,
                                          radius: 4.0,
                                          dotWidth: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.075,
                                          dotHeight: 5,
                                          dotColor: ColorConfig.blue11,
                                          paintStyle: PaintingStyle.fill,
                                          activeDotColor: ColorConfig.primary),
                                    )
                                  : Container(),
                              SizedBox(
                                height: 10,
                              ),
                              Expanded(
                                child: PageView(
                                    controller: state.pageController,
                                    children: state
                                            .projectModel.lists.isNotEmpty
                                        ? state.projectModel.lists
                                            .map((listItem) => CardColumnWidget(
                                                  projectListModel: listItem,
                                                ))
                                            .toList()
                                        : [
                                            CardColumnWidget(
                                              projectListModel: null,
                                            )
                                          ]),
                              ),
                            ],
                          ))),
                )
              ],
            );
          }

          return Container();
        },
      ),
    );
  }
}
