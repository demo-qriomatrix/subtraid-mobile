import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

Color getColorFromStatus(String status) {
  switch (status) {
    case 'active':
    case 'Active':
      return ColorConfig.active;
      break;
    case 'completed':
    case 'Completed':
      return ColorConfig.completed;
      break;
    case 'irrelevant':
    case 'Irrelevant':
      return ColorConfig.irrelevant;
      break;
    case 'resolved':
    case 'Resolved':
      return ColorConfig.resolved;
      break;
    default:
      return Colors.white;
  }
}
