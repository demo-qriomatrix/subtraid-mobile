import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';

class ChecklistItem extends StatelessWidget {
  final CardChecklistModel cardChecklistModel;

  ChecklistItem({@required this.cardChecklistModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            blurRadius: 3,
            color: ColorConfig.shadow2,
            spreadRadius: 1,
            offset: Offset(0, 3))
      ]),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
      child: Row(
        children: <Widget>[
          Text(
            cardChecklistModel.name ?? 'Untitled',
            style: TextStyle(
                color: ColorConfig.primary,
                fontSize: 13,
                fontWeight: FontWeight.w500),
          ),
          Spacer(),
          Icon(
            ((cardChecklistModel?.checkItemsChecked ?? 0) ==
                    (cardChecklistModel?.checkItems?.length ?? 0))
                ? Icons.check_box
                : Icons.check_box_outline_blank,
            size: 18,
            color: ColorConfig.green2,
          ),
          SizedBox(
            width: 10,
          ),
          Text(
              '${cardChecklistModel.checkItemsChecked}/${cardChecklistModel.checkItems.length}',
              style: TextStyle(
                  color: ColorConfig.primary,
                  fontSize: 15,
                  fontWeight: FontWeight.w500))
        ],
      ),
    );
  }
}
