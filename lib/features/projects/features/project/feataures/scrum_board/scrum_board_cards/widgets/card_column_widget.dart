import 'package:Subtraid/core/config/local_images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/add_card_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/remove_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list_item/bloc/list_item_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/sub_project/models/add_sub_project_post_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_list_model.dart';
import 'package:intl/intl.dart';

import 'card_widget.dart';
import 'create_card_widget.dart';
import 'sub_project_widget.dart';

class CardColumnWidget extends StatelessWidget {
  final ProjectListModel projectListModel;
  // final String projectId;

  CardColumnWidget({@required this.projectListModel});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ListItemBloc>(
      create: (context) => ListItemBloc(
          projectListModel: projectListModel,
          projectBloc: context.read<ProjectBloc>()),
      child: BlocBuilder<ListItemBloc, ListItemState>(
        builder: (context, state) {
          if (state is ListItemInitialed) {
            return Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      state.projectListModel == null
                          ? Expanded(
                              child: Center(
                                child: Text(
                                  'Press + icon to add a list',
                                  style: TextStyle(
                                      color: ColorConfig.blue6,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            )
                          : Expanded(
                              child: Column(
                              children: [
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        state.projectListModel.name,
                                        style: TextStyle(
                                            color: ColorConfig.blue6,
                                            fontSize: 13,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                    Spacer(),
                                    (context.watch<ProjectBloc>().state
                                                is ProjectLoadSuccess) &&
                                            (context.watch<ProjectBloc>().state
                                                    as ProjectLoadSuccess)
                                                .projectModel
                                                .loginUserProjectPermission
                                                .cards
                                                .create
                                        ? Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 8.0),
                                            child: RawMaterialButton(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              constraints: BoxConstraints(),
                                              fillColor: state.showAddCard
                                                  ? ColorConfig.orange
                                                  : Colors.white,
                                              onPressed: () {
                                                context.read<ListItemBloc>().add(
                                                    ListItemToggleShowAddCard(
                                                        showAdd: !state
                                                            .showAddCard));
                                              },
                                              padding: EdgeInsets.all(2),
                                              child: Icon(
                                                Icons.add,
                                                color: state.showAddCard
                                                    ? Colors.white
                                                    : ColorConfig.primary,
                                                size: 22,
                                              ),
                                            ),
                                          )
                                        : Container(),
                                    (context.watch<ProjectBloc>().state
                                                is ProjectLoadSuccess) &&
                                            (context.watch<ProjectBloc>().state
                                                    as ProjectLoadSuccess)
                                                .projectModel
                                                .loginUserProjectPermission
                                                .subproject
                                                .create
                                        ? Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 8.0),
                                            child: RawMaterialButton(
                                              materialTapTargetSize:
                                                  MaterialTapTargetSize
                                                      .shrinkWrap,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              constraints: BoxConstraints(),
                                              fillColor: state.showAddSubProject
                                                  ? ColorConfig.orange
                                                  : Colors.white,
                                              onPressed: () {
                                                context.read<ListItemBloc>().add(
                                                    ListItemToggleShowAddSubProject(
                                                        showAdd: !state
                                                            .showAddSubProject));
                                              },
                                              padding: EdgeInsets.all(4),
                                              child: ImageIcon(
                                                AssetImage(
                                                    LocalImages.folderOpen),
                                                color: state.showAddSubProject
                                                    ? Colors.white
                                                    : ColorConfig.primary,
                                                size: 18,
                                              ),
                                            ),
                                          )
                                        : Container(),
                                    (context.watch<ProjectBloc>().state
                                                is ProjectLoadSuccess) &&
                                            (context.watch<ProjectBloc>().state
                                                    as ProjectLoadSuccess)
                                                .projectModel
                                                .loginUserProjectPermission
                                                .list
                                                .delete
                                        ? PopupMenuButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            icon: ImageIcon(
                                              AssetImage(LocalImages.ellipsisV),
                                              color: ColorConfig.primary,
                                              size: 18,
                                            ),
                                            padding: EdgeInsets.zero,
                                            onSelected: (index) {
                                              if (index == 0) {
                                                RemoveListPostModel list =
                                                    RemoveListPostModel(
                                                        (model) => model
                                                          ..listId =
                                                              projectListModel
                                                                  .id);

                                                context.read<ProjectBloc>().add(
                                                    ProjectRemoveList(
                                                        list: list));
                                              }
                                            },
                                            itemBuilder: (context) {
                                              return [
                                                PopupMenuItem<int>(
                                                  value: 0,
                                                  height: 36,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text('Remove List',
                                                          textAlign:
                                                              TextAlign.center),
                                                    ],
                                                  ),
                                                ),
                                              ];
                                            },
                                          )
                                        : Container()
                                    // Padding(
                                    //   padding: const EdgeInsets.symmetric(
                                    //       horizontal: 8.0),
                                    //   child: RawMaterialButton(
                                    //     materialTapTargetSize:
                                    //         MaterialTapTargetSize.shrinkWrap,
                                    //     shape: RoundedRectangleBorder(
                                    //         borderRadius:
                                    //             BorderRadius.circular(5)),
                                    //     constraints: BoxConstraints(),
                                    //     // fillColor: orange,
                                    //     onPressed: () {
                                    //       // showMenu(context: context, items: [
                                    //       //   PopupMenuItem<int>(
                                    //       //     value: 0,
                                    //       //     child:
                                    //       //         Text('Working a lot harder'),
                                    //       //   ),
                                    //       //   PopupMenuItem<int>(
                                    //       //     value: 1,
                                    //       //     child: Text('Working a lot less'),
                                    //       //   ),
                                    //       //   PopupMenuItem<int>(
                                    //       //     value: 1,
                                    //       //     child:
                                    //       //         Text('Working a lot smarter'),
                                    //       //   ),
                                    //       // ]);

                                    //     },
                                    //     padding: EdgeInsets.all(4),
                                    //     child: Icon(
                                    //       Icons.more_vert,
                                    //       color: ColorConfig.grey4,
                                    //       size: 18,
                                    //     ),
                                    //   ),
                                    // ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Expanded(
                                  child: ListView(
                                    padding: EdgeInsets.zero,
                                    children: <Widget>[
                                      state.showAddCard
                                          ? CreateCardWidget(
                                              color: ColorConfig.blue7,
                                              type: 'Card',
                                              onCreate: (String name) {
                                                if (name == '' ||
                                                    name == null) {
                                                  name = 'Untitled Card';
                                                }

                                                ProjectCardModel cardModel = ProjectCardModel(
                                                    (cardModel) => cardModel
                                                      ..name = name
                                                      ..start = DateFormat(
                                                              "MM/dd/yyyy hh:mm a")
                                                          .format(
                                                              DateTime.now())
                                                      ..end = DateFormat(
                                                              "MM/dd/yyyy hh:mm a")
                                                          .format(DateTime.parse(
                                                              '2012-02-27 13:27:00')));

                                                AddCardPostModel addCardModel =
                                                    AddCardPostModel((addCardModel) =>
                                                        addCardModel
                                                          ..listId = state
                                                              .projectListModel
                                                              ?.id
                                                          ..newCard = cardModel
                                                              .toBuilder());

                                                context.read<ProjectBloc>().add(
                                                    ProjectAddCard(
                                                        card: addCardModel,
                                                        listItemBloc:
                                                            context.read<
                                                                ListItemBloc>()));
                                              },
                                            )
                                          : Container(),
                                      state.showAddSubProject
                                          ? CreateCardWidget(
                                              color: ColorConfig.blue7,
                                              type: 'Sub-project',
                                              onCreate: (String name) {
                                                if (name == '' ||
                                                    name == null) {
                                                  name = 'Untitled Sub-project';
                                                }

                                                AddSubProjectPostModel
                                                    addSubProject =
                                                    AddSubProjectPostModel(
                                                        (addSubProject) =>
                                                            addSubProject
                                                              ..listId = state
                                                                  .projectListModel
                                                                  ?.id
                                                              ..name = name);

                                                context.read<ProjectBloc>().add(
                                                    ProjectAddSubProject(
                                                        subProject:
                                                            addSubProject,
                                                        listItemBloc:
                                                            context.read<
                                                                ListItemBloc>()));
                                              },
                                            )
                                          : Container(),
                                      BlocBuilder<ProjectBloc, ProjectState>(
                                        builder: (context, projectState) {
                                          if (projectState
                                              is ProjectLoadSuccess) {
                                            return Column(
                                              children: state
                                                  .projectListModel.idCards
                                                  .where((cardId) =>
                                                      projectState
                                                          .cards[cardId] !=
                                                      null)
                                                  .map((cardId) => CardWidget(
                                                        cardModel: projectState
                                                            .cards[cardId],
                                                      ))
                                                  .toList(),
                                            );
                                          }

                                          return Container();
                                        },
                                      ),
                                      BlocBuilder<ProjectBloc, ProjectState>(
                                        builder: (context, projectState) {
                                          if (projectState
                                              is ProjectLoadSuccess) {
                                            return Column(
                                              children: state
                                                  .projectListModel.idProjects
                                                  .map((projectId) =>
                                                      projectState.subProjects[
                                                                  projectId] !=
                                                              null
                                                          ? SubProjectWidget(
                                                              subProject:
                                                                  projectState
                                                                          .subProjects[
                                                                      projectId],
                                                            )
                                                          : Container())
                                                  .toList(),
                                            );
                                          }

                                          return Container();
                                        },
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ))
                    ]));
          }

          return Container();
        },
      ),
    );
  }
}
