import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/models/project_sub_project_model.dart';
import 'package:Subtraid/features/projects/features/project/view/project_screen.dart';

class SubProjectWidget extends StatelessWidget {
  final ProjectSubProjectModel subProject;

  SubProjectWidget({@required this.subProject});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 15),
        child: Stack(children: <Widget>[
          Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Row(children: <Widget>[
                    Text(
                      subProject?.name ?? '',
                      style: TextStyle(
                          color: ColorConfig.primary,
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    )
                  ]),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.calendar_today,
                        size: 20,
                        color: ColorConfig.primary,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        DateFormat('dd MMM yyyy').format(DateTime.now()
                            // DateTime.parse(cardModel.due)
                            ),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(children: <Widget>[
                    Text('Owned by',
                        style: TextStyle(color: ColorConfig.blue12)),
                    Padding(
                      padding: const EdgeInsets.only(left: 8, right: 8),
                      child: CircleAvatar(
                        backgroundImage: AssetImage(LocalImages.defaultAvatar),
                        radius: 16,
                      ),
                    ),
                    Text(subProject.owner.name),
                  ]),
                ],
              )),
          Positioned.fill(
              child: Material(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(10),
            child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ProjectScreen(
                    projectId: subProject.id,
                  ),
                ));
              },
            ),
          ))
        ]));
  }
}
