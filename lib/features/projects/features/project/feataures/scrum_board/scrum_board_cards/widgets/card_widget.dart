import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/helpers/formate_start_end_date.dart';
import 'package:Subtraid/features/main/view/screens.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_attachment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_comment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/view/card_screen.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:Subtraid/features/shared/cound_up_timer_widget.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'card_checklist_item_widget.dart';
import 'card_label_widget.dart';
import 'indicator_widget.dart';

class CardWidget extends StatelessWidget {
  final ProjectCardModel cardModel;

  CardWidget({
    @required this.cardModel,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.end,
                      //   children: <Widget>[
                      //     Icon(Icons.timer, size: 20, color: ColorConfig.red3),
                      //     Text(
                      //       '12:04',
                      //       style: TextStyle(
                      //           color: ColorConfig.red3,
                      //           fontSize: 16,
                      //           fontWeight: FontWeight.w500),
                      //     )
                      //   ],
                      // ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              cardModel?.name ?? '',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          cardModel?.activeTimesheet != null
                              ? CountDownTimer(
                                  startDateTime:
                                      cardModel.activeTimesheet.start,
                                )
                              : Container()
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      cardModel?.idLabels != null &&
                              cardModel.idLabels.isNotEmpty
                          ? BlocBuilder<ProjectBloc, ProjectState>(
                              builder: (context, state) {
                                if (state is ProjectLoadSuccess) {
                                  return Column(
                                    children: <Widget>[
                                      Row(
                                          children: cardModel.idLabels
                                              .where((label) =>
                                                  state.labels[label] != null)
                                              .map(
                                                (label) => CardLabelWidget(
                                                    state.labels[label].color),
                                              )
                                              .toList()),
                                      SizedBox(
                                        height: 15,
                                      ),
                                    ],
                                  );
                                }

                                return Container();
                              },
                            )
                          : Container(),

                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.calendar_today,
                            size: 20,
                            color: ColorConfig.primary,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            formatStartEndDate(cardModel?.end),
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w400),
                          )
                        ],
                      ),

                      Column(
                        children: <Widget>[
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.end,
                          //   children: <Widget>[
                          //     Text(
                          //       '42 Remain days',
                          //       style: TextStyle(
                          //           fontWeight: FontWeight.w500,
                          //           color: ColorConfig.blue9,
                          //           fontSize: 12),
                          //     ),
                          //   ],
                          // ),
                          // SizedBox(
                          //   height: 5,
                          // ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                ((cardModel?.checkItemsChecked ?? 0) == 0 &&
                                                ((cardModel?.checkItems ?? 0) ==
                                                    0)
                                            ? 0
                                            : (((((cardModel?.checkItemsChecked ??
                                                                0)
                                                            .toInt()) /
                                                        ((cardModel?.checkItems ??
                                                                0)
                                                            .toInt())) *
                                                    100) ??
                                                0))
                                        .toStringAsFixed(2) +
                                    '%',
                                style: TextStyle(
                                    fontSize: 12, color: ColorConfig.fillColor),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Stack(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: ColorConfig.fillColorShade),
                                height: 7,
                              ),
                              Row(
                                children: <Widget>[
                                  Flexible(
                                    flex: cardModel?.checkItemsChecked ?? 0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: ColorConfig.fillColor),
                                      height: 7,
                                    ),
                                  ),
                                  Flexible(
                                    flex: (cardModel?.checkItems ?? 0) -
                                        (cardModel?.checkItemsChecked ?? 0),
                                    child: Container(
                                      height: 7,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ChecklistItem(
                        cardChecklistModel: CardChecklistModel(
                            (cardChecklistModel) => cardChecklistModel
                              ..name = 'Task Summery'
                              ..checkItems = ListBuilder(List.generate(
                                  cardModel?.checkItems,
                                  (index) => CardChecklistItemModel()))
                              ..checkItemsChecked =
                                  cardModel?.checkItemsChecked),
                      ),

                      // cardModel.checklists != null &&
                      //         cardModel.checklists.isNotEmpty
                      //     ? Column(
                      //         children: cardModel.checklists
                      //             .map(
                      //               (checkListItem) => ChecklistItem(
                      //                 cardChecklistModel: checkListItem,
                      //               ),
                      //             )
                      //             .toList(),
                      //       )
                      //     : Container(),
                      SizedBox(
                        height: 15,
                      ),

                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Row(
                            children: List.generate(
                              cardModel.idMembers.length,
                              (index) => Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: CircleAvatar(
                                  backgroundImage:
                                      AssetImage(LocalImages.defaultAvatar),
                                  radius: 12,
                                ),
                              ),
                            ),
                          )),

                          // Icon(
                          //   Icons.notifications,
                          //   size: 18,
                          //   color: ColorConfig.orange,
                          // ),
                          // SizedBox(
                          //   width: 10,
                          // ),
                          ImageIcon(
                            AssetImage(LocalImages.paperclip),
                            color: ColorConfig.primary,
                            size: 14,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            (cardModel?.attachments ??
                                    List<CardAttachmentModel>())
                                .length
                                .toString(),
                            style: TextStyle(
                                color: ColorConfig.primary,
                                fontWeight: FontWeight.w500),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          ImageIcon(
                            AssetImage(LocalImages.commentAlt),
                            color: ColorConfig.primary,
                            size: 14,
                          ),

                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            (cardModel?.comments ?? List<CardCommentModel>())
                                .length
                                .toString(),
                            style: TextStyle(
                                color: ColorConfig.primary,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned.fill(
              child: Row(
            children: [
              Container(
                width: 5,
                decoration: BoxDecoration(
                  color: getColorFromStatus(cardModel.status),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                ),
              ),
            ],
          )),
          Positioned.fill(
              child: Material(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(10),
            child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                // Provider.of<AppStateProvider>(context, listen: false)
                //     .cardModel = cardModel;

                context.read<CardBloc>().add(CardSetCard(card: cardModel));

                navKeys[0].currentState.push(
                    MaterialPageRoute(builder: (context) => CardScreen()));
              },
            ),
          ))
        ],
      ),
    );
  }
}
