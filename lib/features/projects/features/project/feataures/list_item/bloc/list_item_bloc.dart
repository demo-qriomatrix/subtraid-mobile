import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/models/project_list_model.dart';

part 'list_item_event.dart';
part 'list_item_state.dart';

class ListItemBloc extends Bloc<ListItemEvent, ListItemState> {
  ListItemBloc({@required this.projectListModel, @required this.projectBloc})
      : super(ListItemInitial()) {
    add(ListItemIntialize(
      projectListModel: this.projectListModel,
    ));

    projectBloc.setListItemBloc(this);
  }

  final ProjectListModel projectListModel;

  final ProjectBloc projectBloc;

  @override
  Stream<ListItemState> mapEventToState(
    ListItemEvent event,
  ) async* {
    if (event is ListItemIntialize) {
      yield ListItemInitialed(
          // projectId: event.projectId,
          projectListModel: event.projectListModel,
          showAddCard: false,
          showAddSubProject: false);
    }

    if (event is ListItemToggleShowAddCard) {
      if (state is ListItemInitialed) {
        ListItemInitialed castState = state;

        yield ListItemInitialed(
            // projectId: castState.projectId,
            projectListModel: castState.projectListModel,
            showAddCard: event.showAdd,
            showAddSubProject: castState.showAddSubProject);
      }
    }

    if (event is ListItemToggleShowAddSubProject) {
      if (state is ListItemInitialed) {
        ListItemInitialed castState = state;

        yield ListItemInitialed(
            // projectId: castState.projectId,
            projectListModel: castState.projectListModel,
            showAddCard: castState.showAddCard,
            showAddSubProject: event.showAdd);
      }
    }

    if (event is ListItemUddate) {
      if (state is ListItemInitialed) {
        yield ListItemInitialed(
            // projectId: castState.projectId,
            projectListModel: event.projectListModel,
            showAddCard: false,
            showAddSubProject: false);
      }
    }
  }
}
