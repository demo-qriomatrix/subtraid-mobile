part of 'list_item_bloc.dart';

abstract class ListItemState extends Equatable {
  @override
  List<Object> get props => [];
}

class ListItemInitial extends ListItemState {}

class ListItemInitialed extends ListItemState {
  final ProjectListModel projectListModel;
  // final String projectId;
  final bool showAddCard;
  final bool showAddSubProject;

  ListItemInitialed({
    @required this.projectListModel,
    @required this.showAddCard,
    @required this.showAddSubProject,
    // @required this.projectId
  });

  @override
  List<Object> get props => [showAddCard, showAddSubProject, projectListModel];
}
