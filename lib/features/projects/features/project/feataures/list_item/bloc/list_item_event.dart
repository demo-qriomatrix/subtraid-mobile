part of 'list_item_bloc.dart';

abstract class ListItemEvent extends Equatable {
  const ListItemEvent();

  @override
  List<Object> get props => [];
}

class ListItemIntialize extends ListItemEvent {
  final ProjectListModel projectListModel;

  ListItemIntialize({
    @required this.projectListModel,
  });
}

class ListItemToggleShowAddCard extends ListItemEvent {
  final bool showAdd;

  ListItemToggleShowAddCard({@required this.showAdd});
}

class ListItemToggleShowAddSubProject extends ListItemEvent {
  final bool showAdd;

  ListItemToggleShowAddSubProject({@required this.showAdd});
}

class ListItemUddate extends ListItemEvent {
  final ProjectListModel projectListModel;

  ListItemUddate({
    @required this.projectListModel,
  });
}
