// GENERATED CODE - DO NOT MODIFY BY HAND

part of custom_timesheet_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CustomTimesheetModel> _$customTimesheetModelSerializer =
    new _$CustomTimesheetModelSerializer();

class _$CustomTimesheetModelSerializer
    implements StructuredSerializer<CustomTimesheetModel> {
  @override
  final Iterable<Type> types = const [
    CustomTimesheetModel,
    _$CustomTimesheetModel
  ];
  @override
  final String wireName = 'CustomTimesheetModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CustomTimesheetModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.lastTimesheetModel != null) {
      result
        ..add('lastTimesheetModel')
        ..add(serializers.serialize(object.lastTimesheetModel,
            specifiedType: const FullType(ProjectTimesheetModel)));
    }
    if (object.currentDuration != null) {
      result
        ..add('currentDuration')
        ..add(serializers.serialize(object.currentDuration,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  CustomTimesheetModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CustomTimesheetModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'lastTimesheetModel':
          result.lastTimesheetModel.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectTimesheetModel))
              as ProjectTimesheetModel);
          break;
        case 'currentDuration':
          result.currentDuration = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$CustomTimesheetModel extends CustomTimesheetModel {
  @override
  final ProjectTimesheetModel lastTimesheetModel;
  @override
  final int currentDuration;

  factory _$CustomTimesheetModel(
          [void Function(CustomTimesheetModelBuilder) updates]) =>
      (new CustomTimesheetModelBuilder()..update(updates)).build();

  _$CustomTimesheetModel._({this.lastTimesheetModel, this.currentDuration})
      : super._();

  @override
  CustomTimesheetModel rebuild(
          void Function(CustomTimesheetModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CustomTimesheetModelBuilder toBuilder() =>
      new CustomTimesheetModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CustomTimesheetModel &&
        lastTimesheetModel == other.lastTimesheetModel &&
        currentDuration == other.currentDuration;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc(0, lastTimesheetModel.hashCode), currentDuration.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CustomTimesheetModel')
          ..add('lastTimesheetModel', lastTimesheetModel)
          ..add('currentDuration', currentDuration))
        .toString();
  }
}

class CustomTimesheetModelBuilder
    implements Builder<CustomTimesheetModel, CustomTimesheetModelBuilder> {
  _$CustomTimesheetModel _$v;

  ProjectTimesheetModelBuilder _lastTimesheetModel;
  ProjectTimesheetModelBuilder get lastTimesheetModel =>
      _$this._lastTimesheetModel ??= new ProjectTimesheetModelBuilder();
  set lastTimesheetModel(ProjectTimesheetModelBuilder lastTimesheetModel) =>
      _$this._lastTimesheetModel = lastTimesheetModel;

  int _currentDuration;
  int get currentDuration => _$this._currentDuration;
  set currentDuration(int currentDuration) =>
      _$this._currentDuration = currentDuration;

  CustomTimesheetModelBuilder();

  CustomTimesheetModelBuilder get _$this {
    if (_$v != null) {
      _lastTimesheetModel = _$v.lastTimesheetModel?.toBuilder();
      _currentDuration = _$v.currentDuration;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CustomTimesheetModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CustomTimesheetModel;
  }

  @override
  void update(void Function(CustomTimesheetModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CustomTimesheetModel build() {
    _$CustomTimesheetModel _$result;
    try {
      _$result = _$v ??
          new _$CustomTimesheetModel._(
              lastTimesheetModel: _lastTimesheetModel?.build(),
              currentDuration: currentDuration);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'lastTimesheetModel';
        _lastTimesheetModel?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CustomTimesheetModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
