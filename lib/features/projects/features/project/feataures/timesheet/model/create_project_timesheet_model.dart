library create_project_timesheet_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_project_timesheet_model.g.dart';

abstract class CreateProjectTimesheetModel
    implements
        Built<CreateProjectTimesheetModel, CreateProjectTimesheetModelBuilder> {
  @nullable
  String get start;

  @nullable
  @BuiltValueField(wireName: '_userId')
  String get userId;

  @nullable
  @BuiltValueField(wireName: '_projectId')
  String get projectId;

  CreateProjectTimesheetModel._();

  factory CreateProjectTimesheetModel(
          [updates(CreateProjectTimesheetModelBuilder b)]) =
      _$CreateProjectTimesheetModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        CreateProjectTimesheetModel.serializer, this));
  }

  static CreateProjectTimesheetModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CreateProjectTimesheetModel.serializer, json.decode(jsonString));
  }

  static Serializer<CreateProjectTimesheetModel> get serializer =>
      _$createProjectTimesheetModelSerializer;
}
