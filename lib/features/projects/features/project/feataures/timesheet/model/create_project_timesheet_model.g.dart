// GENERATED CODE - DO NOT MODIFY BY HAND

part of create_project_timesheet_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CreateProjectTimesheetModel>
    _$createProjectTimesheetModelSerializer =
    new _$CreateProjectTimesheetModelSerializer();

class _$CreateProjectTimesheetModelSerializer
    implements StructuredSerializer<CreateProjectTimesheetModel> {
  @override
  final Iterable<Type> types = const [
    CreateProjectTimesheetModel,
    _$CreateProjectTimesheetModel
  ];
  @override
  final String wireName = 'CreateProjectTimesheetModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CreateProjectTimesheetModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.userId != null) {
      result
        ..add('_userId')
        ..add(serializers.serialize(object.userId,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('_projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CreateProjectTimesheetModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreateProjectTimesheetModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_userId':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CreateProjectTimesheetModel extends CreateProjectTimesheetModel {
  @override
  final String start;
  @override
  final String userId;
  @override
  final String projectId;

  factory _$CreateProjectTimesheetModel(
          [void Function(CreateProjectTimesheetModelBuilder) updates]) =>
      (new CreateProjectTimesheetModelBuilder()..update(updates)).build();

  _$CreateProjectTimesheetModel._({this.start, this.userId, this.projectId})
      : super._();

  @override
  CreateProjectTimesheetModel rebuild(
          void Function(CreateProjectTimesheetModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateProjectTimesheetModelBuilder toBuilder() =>
      new CreateProjectTimesheetModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateProjectTimesheetModel &&
        start == other.start &&
        userId == other.userId &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, start.hashCode), userId.hashCode), projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateProjectTimesheetModel')
          ..add('start', start)
          ..add('userId', userId)
          ..add('projectId', projectId))
        .toString();
  }
}

class CreateProjectTimesheetModelBuilder
    implements
        Builder<CreateProjectTimesheetModel,
            CreateProjectTimesheetModelBuilder> {
  _$CreateProjectTimesheetModel _$v;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _userId;
  String get userId => _$this._userId;
  set userId(String userId) => _$this._userId = userId;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  CreateProjectTimesheetModelBuilder();

  CreateProjectTimesheetModelBuilder get _$this {
    if (_$v != null) {
      _start = _$v.start;
      _userId = _$v.userId;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateProjectTimesheetModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CreateProjectTimesheetModel;
  }

  @override
  void update(void Function(CreateProjectTimesheetModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateProjectTimesheetModel build() {
    final _$result = _$v ??
        new _$CreateProjectTimesheetModel._(
            start: start, userId: userId, projectId: projectId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
