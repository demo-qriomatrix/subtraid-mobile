library custom_timesheet_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/models/project_timesheet_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'custom_timesheet_model.g.dart';

abstract class CustomTimesheetModel
    implements Built<CustomTimesheetModel, CustomTimesheetModelBuilder> {
  @nullable
  ProjectTimesheetModel get lastTimesheetModel;

  @nullable
  int get currentDuration;

  // fields go here

  CustomTimesheetModel._();

  factory CustomTimesheetModel([updates(CustomTimesheetModelBuilder b)]) =
      _$CustomTimesheetModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CustomTimesheetModel.serializer, this));
  }

  static CustomTimesheetModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CustomTimesheetModel.serializer, json.decode(jsonString));
  }

  static Serializer<CustomTimesheetModel> get serializer =>
      _$customTimesheetModelSerializer;
}
