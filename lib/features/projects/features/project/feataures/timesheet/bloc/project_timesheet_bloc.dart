import 'dart:async';

import 'package:Subtraid/core/helpers/get_company_user.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/model/custom_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_member_id_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_timesheet_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'project_timesheet_event.dart';
part 'project_timesheet_state.dart';

class ProjectTimesheetBloc
    extends Bloc<ProjectTimesheetEvent, ProjectTimesheetState> {
  ProjectTimesheetBloc(
      {@required this.projectBloc, @required this.authenticationBloc})
      : super(ProjectTimesheetInitial()) {
    add(ProjectTimesheetRequested());

    projectBloc.listen((state) {
      if (state is ProjectLoadSuccess) {
        add(ProjectTimesheetRequested());
      }
    });
  }

  final ProjectBloc projectBloc;
  final AuthenticationBloc authenticationBloc;

  @override
  Stream<ProjectTimesheetState> mapEventToState(
    ProjectTimesheetEvent event,
  ) async* {
    if (event is ProjectTimesheetRequested) {
      yield* _mapProjectTimesheetRequestedToState();
    }
  }

  Stream<ProjectTimesheetState> _mapProjectTimesheetRequestedToState() async* {
    ProjectState projectState = projectBloc.state;

    List<ProjectMemberIdModel> members = [];

    Map<String, CustomTimesheetModel> memberIdToTimesheetMap = Map();

    if (projectState is ProjectLoadSuccess) {
      if (getCompanyUser(authenticationBloc)) {
        members = projectState.projectModel.idMembers.toList();
      } else {
        members = projectState.projectModel.idMembers
            .where((memeber) => memeber.id == getUserId(authenticationBloc))
            .toList();
      }

      DateTime now = DateTime.now().toUtc();
      DateTime lastMidnight = DateTime(now.year, now.month, now.day).toLocal();

      print(lastMidnight);

      List<ProjectTimesheetModel> timesheets = projectState
          .projectModel.timesheets
          .where((timesheet) =>
              DateTime.parse(timesheet.start).isAfter(lastMidnight))
          .toList();

      members.forEach((member) {
        List<ProjectTimesheetModel> userTimesheets = timesheets
            .where((timesheet) => timesheet.user == member.id)
            .toList();

        List<ProjectTimesheetModel> completedTimesheets =
            userTimesheets.where((timesheet) => timesheet.status == 1).toList();

        // ProjectTimesheetModel onGoingTimesheet = userTimesheets.lastWhere(
        //     (timesheet) => timesheet.status == 0,
        //     orElse: () => null);

        int durations = 0;

        completedTimesheets.forEach((element) {
          durations += DateTime.parse(element.end)
              .difference(DateTime.parse(element.start))
              .inSeconds;
        });

        CustomTimesheetModel customTimesheetModel = CustomTimesheetModel(
            (data) => data
              ..currentDuration = durations
              ..lastTimesheetModel =
                  userTimesheets != null && userTimesheets.isNotEmpty
                      ? userTimesheets.last.toBuilder()
                      : null);

        memberIdToTimesheetMap.putIfAbsent(
            member.id, () => customTimesheetModel);
      });

      yield ProjectTimesheetLoadSuccess(
          members: members, memberIdToTimesheetMap: memberIdToTimesheetMap);
    }
  }
}
