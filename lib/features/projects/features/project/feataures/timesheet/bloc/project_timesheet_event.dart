part of 'project_timesheet_bloc.dart';

abstract class ProjectTimesheetEvent extends Equatable {
  const ProjectTimesheetEvent();

  @override
  List<Object> get props => [];
}

class ProjectTimesheetRequested extends ProjectTimesheetEvent {}
