part of 'project_timesheet_bloc.dart';

abstract class ProjectTimesheetState extends Equatable {
  const ProjectTimesheetState();

  @override
  List<Object> get props => [];
}

class ProjectTimesheetInitial extends ProjectTimesheetState {}

class ProjectTimesheetLoadSuccess extends ProjectTimesheetState {
  final List<ProjectMemberIdModel> members;
  final Map<String, CustomTimesheetModel> memberIdToTimesheetMap;

  ProjectTimesheetLoadSuccess(
      {@required this.members, @required this.memberIdToTimesheetMap});

  @override
  List<Object> get props =>
      [members, memberIdToTimesheetMap, memberIdToTimesheetMap.hashCode];
}
