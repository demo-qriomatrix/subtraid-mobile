import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/helpers/get_durations_from_seconds.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/bloc/project_timesheet_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/model/custom_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_member_id_model.dart';
import 'package:Subtraid/features/shared/cound_up_timer_widget.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class TimesheetScreen extends StatefulWidget {
  @override
  _TimesheetScreenState createState() => _TimesheetScreenState();
}

class _TimesheetScreenState extends State<TimesheetScreen> {
  // final GlobalKey<FormFieldState> _dropDownKey = GlobalKey<FormFieldState>();
  TextEditingController nameController = TextEditingController();

  String name;

  String role;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      child: Column(
        children: [
          Expanded(
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: BlocProvider<ProjectTimesheetBloc>(
                        create: (context) => ProjectTimesheetBloc(
                            authenticationBloc:
                                context.read<AuthenticationBloc>(),
                            projectBloc: context.read<ProjectBloc>()),
                        child: BlocBuilder<ProjectTimesheetBloc,
                            ProjectTimesheetState>(
                          builder: (context, state) {
                            if (state is ProjectTimesheetLoadSuccess) {
                              return ListView(
                                  padding: EdgeInsets.zero,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      padding: EdgeInsets.symmetric(
                                          vertical: 20, horizontal: 20),
                                      decoration: BoxDecoration(
                                          color: ColorConfig.blue4,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('Member name',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.black,
                                                  fontSize: 12)),
                                          SizedBox(
                                            height: height2,
                                          ),
                                          TextFormField(
                                            controller: nameController,
                                            onChanged: (val) {},
                                            decoration:
                                                profileInputDecoration.copyWith(
                                                    prefixIcon: Icon(
                                                      Icons.search,
                                                      color:
                                                          ColorConfig.primary,
                                                    ),
                                                    labelText: 'Search by name',
                                                    labelStyle: TextStyle(
                                                        fontSize: 14,
                                                        color: ColorConfig
                                                            .black1)),
                                            maxLines: null,
                                            style: textStyle2,
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          // Text('User Role',
                                          //     style: TextStyle(
                                          //         fontWeight: FontWeight.w500,
                                          //         color: Colors.black,
                                          //         fontSize: 12)),
                                          // SizedBox(
                                          //   height: height2,
                                          // ),
                                          // BlocBuilder<ProjectPermissionBloc,
                                          //     ProjectPermissionState>(
                                          //   builder: (context, state) {
                                          //     return DropdownButtonFormField(
                                          //       key: _dropDownKey,
                                          //       iconSize: 20,
                                          //       dropdownColor:
                                          //           ColorConfig.primary,
                                          //       icon: Icon(
                                          //           Icons.keyboard_arrow_down),
                                          //       decoration: profileInputDecoration
                                          //           .copyWith(
                                          //               hintText:
                                          //                   'Select user role',
                                          //               hintStyle: TextStyle(
                                          //                   fontSize: 14,
                                          //                   color: ColorConfig
                                          //                       .black1)),
                                          //       items: (state
                                          //               is ProjectPermissionLoadSuccess)
                                          //           ? state
                                          //               .companyInfoModel
                                          //               .company
                                          //               .projectPermissionGroups
                                          //               .map((e) =>
                                          //                   (DropdownMenuItem(
                                          //                     value: e.id,
                                          //                     child: Text(
                                          //                       e.name,
                                          //                       style: TextStyle(
                                          //                           color: Colors
                                          //                               .white),
                                          //                     ),
                                          //                   )))
                                          //               .toList()
                                          //           : [],
                                          //       style: TextStyle(fontSize: 14),
                                          //       selectedItemBuilder: (context) => (state
                                          //               is ProjectPermissionLoadSuccess)
                                          //           ? state
                                          //               .companyInfoModel
                                          //               .company
                                          //               .projectPermissionGroups
                                          //               .map((e) => (Text(
                                          //                   e.name,
                                          //                   style: TextStyle(
                                          //                       color: Colors
                                          //                           .black))))
                                          //               .toList()
                                          //           : [],
                                          //       onChanged: (val) {},
                                          //     );
                                          //   },
                                          // ),
                                          // SizedBox(
                                          //   height: 20,
                                          // ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              RawMaterialButton(
                                                constraints: BoxConstraints(),
                                                onPressed: () {
                                                  // _dropDownKey.currentState
                                                  //     .reset();
                                                  nameController.clear();
                                                  setState(() {
                                                    name = null;
                                                    role = null;
                                                  });

                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                },
                                                shape: CircleBorder(),
                                                fillColor: ColorConfig.grey6,
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Icon(
                                                  Icons.close,
                                                  size: 20,
                                                ),
                                              ),
                                              RawMaterialButton(
                                                constraints: BoxConstraints(),
                                                onPressed: () {
                                                  setState(() {
                                                    name = nameController.text;
                                                    // role = _dropDownKey
                                                    //     .currentState.value;
                                                  });
                                                  FocusScope.of(context)
                                                      .requestFocus(
                                                          FocusNode());
                                                },
                                                shape: CircleBorder(),
                                                fillColor: ColorConfig.primary,
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Icon(
                                                  Icons.check,
                                                  color: Colors.white,
                                                  size: 20,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Column(
                                        children: state.members
                                            .where((element) =>
                                                name != '' && name != null
                                                    ? element.name
                                                        .toLowerCase()
                                                        .contains(
                                                            name.toLowerCase())
                                                    : true)
                                            // .where((element) => role != '' &&
                                            //         role != null
                                            //     ? state
                                            //             .memberIdToPermissionMap[
                                            //                 element.id]
                                            //             .permission
                                            //             .id ==
                                            //         role
                                            //     : true)
                                            .map((e) => EmployeeTaskWidget(
                                                  member: e,
                                                  timesheetModel: state
                                                          .memberIdToTimesheetMap[
                                                      e.id],
                                                  // role: state
                                                  //         .memberIdToPermissionMap[
                                                  //     e.id],
                                                ))
                                            .toList()),
                                  ]);
                            }

                            return Container();
                          },
                        ),
                      ))))
        ],
      ),
    );
  }
}

class EmployeeTaskWidget extends StatelessWidget {
  final ProjectMemberIdModel member;
  // final ProjectPermissionRoleModel role;
  final CustomTimesheetModel timesheetModel;

  EmployeeTaskWidget({
    @required this.member,
    @required this.timesheetModel,

    // @required this.role
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                blurRadius: 12,
                offset: Offset(0, 0),
                color: ColorConfig.shadow1)
          ]),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                member.name,
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontWeight: FontWeight.w600,
                    fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),

          timesheetModel?.lastTimesheetModel?.start != null
              ? Text(
                  'Start : ' +
                      DateFormat('MMM dd, yyyy, hh:mm:ss aa').format(
                          DateTime.parse(
                                  timesheetModel?.lastTimesheetModel?.start)
                              .toLocal()),
                  style: TextStyle(
                      color: ColorConfig.primary, fontWeight: FontWeight.w400),
                )
              : Container(),
          SizedBox(
            height: 10,
          ),

          timesheetModel?.lastTimesheetModel?.end != null
              ? Text(
                  'End : ' +
                      DateFormat('MMM dd, yyyy, hh:mm:ss aa').format(
                          DateTime.parse(
                                  timesheetModel?.lastTimesheetModel?.end)
                              .toLocal()),
                  style: TextStyle(
                      color: ColorConfig.primary, fontWeight: FontWeight.w400),
                )
              : Container(),
          SizedBox(
            height: 10,
          ),
          // Row(
          //   children: [
          //     Text(
          //       role.permission.name,
          //       style: TextStyle(
          //           color: ColorConfig.grey1,
          //           fontWeight: FontWeight.w500,
          //           fontSize: 14),
          //     ),
          //   ],
          // ),
          // SizedBox(
          //   height: 10,
          // ),
          Row(
            children: [
              Text(
                'Logged Time ',
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontWeight: FontWeight.w500,
                    fontSize: 14),
              ),
              Text(
                timesheetModel != null
                    ? getDurationFromSeconds(timesheetModel.currentDuration)
                    : '00:00:00',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 14),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              FlatButton(
                color: timesheetModel?.lastTimesheetModel != null &&
                        timesheetModel?.lastTimesheetModel?.end == null
                    ? ColorConfig.red1
                    : ColorConfig.fillColor,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ImageIcon(
                      AssetImage(LocalImages.stopwatch),
                      color: Colors.white,
                      size: 16,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                        timesheetModel?.lastTimesheetModel != null &&
                                timesheetModel?.lastTimesheetModel?.end == null
                            ? "Stop Task"
                            : "Start Task",
                        style: TextStyle(
                          // fontSize: 17,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        )),
                  ],
                ),
                onPressed: () {
                  if (timesheetModel?.lastTimesheetModel != null &&
                      timesheetModel?.lastTimesheetModel?.end == null) {
                    context.read<ProjectBloc>().add(ProjectSopTask(
                        timesheetId: timesheetModel.lastTimesheetModel.id));
                  } else {
                    context
                        .read<ProjectBloc>()
                        .add(ProjectStartTask(userId: member.id));
                  }
                },
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Text(
                'Active Time ',
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontWeight: FontWeight.w500,
                    fontSize: 14),
              ),
              SizedBox(
                width: 20,
              ),
              timesheetModel?.lastTimesheetModel != null &&
                      timesheetModel?.lastTimesheetModel?.end == null
                  ? CountDownTimer(
                      fullFormat: true,
                      startDateTime: timesheetModel?.lastTimesheetModel?.start)
                  : Text(
                      '00:00:00',
                      style: TextStyle(
                          color: ColorConfig.red1,
                          fontWeight: FontWeight.w500,
                          fontSize: 14),
                    ),
            ],
          ),
        ],
      ),
    );
  }
}
