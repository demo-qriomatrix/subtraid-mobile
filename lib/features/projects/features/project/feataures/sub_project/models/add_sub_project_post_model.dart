library add_sub_project_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'add_sub_project_post_model.g.dart';

abstract class AddSubProjectPostModel
    implements Built<AddSubProjectPostModel, AddSubProjectPostModelBuilder> {
  @nullable
  String get parentProjectId;

  @nullable
  String get listId;

  @nullable
  String get name;

  AddSubProjectPostModel._();

  factory AddSubProjectPostModel([updates(AddSubProjectPostModelBuilder b)]) =
      _$AddSubProjectPostModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(AddSubProjectPostModel.serializer, this));
  }

  static AddSubProjectPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddSubProjectPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddSubProjectPostModel> get serializer =>
      _$addSubProjectPostModelSerializer;
}
