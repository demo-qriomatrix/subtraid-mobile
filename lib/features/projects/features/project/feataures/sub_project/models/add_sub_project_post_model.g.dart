// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_sub_project_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddSubProjectPostModel> _$addSubProjectPostModelSerializer =
    new _$AddSubProjectPostModelSerializer();

class _$AddSubProjectPostModelSerializer
    implements StructuredSerializer<AddSubProjectPostModel> {
  @override
  final Iterable<Type> types = const [
    AddSubProjectPostModel,
    _$AddSubProjectPostModel
  ];
  @override
  final String wireName = 'AddSubProjectPostModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, AddSubProjectPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.parentProjectId != null) {
      result
        ..add('parentProjectId')
        ..add(serializers.serialize(object.parentProjectId,
            specifiedType: const FullType(String)));
    }
    if (object.listId != null) {
      result
        ..add('listId')
        ..add(serializers.serialize(object.listId,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AddSubProjectPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddSubProjectPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'parentProjectId':
          result.parentProjectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'listId':
          result.listId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AddSubProjectPostModel extends AddSubProjectPostModel {
  @override
  final String parentProjectId;
  @override
  final String listId;
  @override
  final String name;

  factory _$AddSubProjectPostModel(
          [void Function(AddSubProjectPostModelBuilder) updates]) =>
      (new AddSubProjectPostModelBuilder()..update(updates)).build();

  _$AddSubProjectPostModel._({this.parentProjectId, this.listId, this.name})
      : super._();

  @override
  AddSubProjectPostModel rebuild(
          void Function(AddSubProjectPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddSubProjectPostModelBuilder toBuilder() =>
      new AddSubProjectPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddSubProjectPostModel &&
        parentProjectId == other.parentProjectId &&
        listId == other.listId &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, parentProjectId.hashCode), listId.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddSubProjectPostModel')
          ..add('parentProjectId', parentProjectId)
          ..add('listId', listId)
          ..add('name', name))
        .toString();
  }
}

class AddSubProjectPostModelBuilder
    implements Builder<AddSubProjectPostModel, AddSubProjectPostModelBuilder> {
  _$AddSubProjectPostModel _$v;

  String _parentProjectId;
  String get parentProjectId => _$this._parentProjectId;
  set parentProjectId(String parentProjectId) =>
      _$this._parentProjectId = parentProjectId;

  String _listId;
  String get listId => _$this._listId;
  set listId(String listId) => _$this._listId = listId;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  AddSubProjectPostModelBuilder();

  AddSubProjectPostModelBuilder get _$this {
    if (_$v != null) {
      _parentProjectId = _$v.parentProjectId;
      _listId = _$v.listId;
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddSubProjectPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddSubProjectPostModel;
  }

  @override
  void update(void Function(AddSubProjectPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddSubProjectPostModel build() {
    final _$result = _$v ??
        new _$AddSubProjectPostModel._(
            parentProjectId: parentProjectId, listId: listId, name: name);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
