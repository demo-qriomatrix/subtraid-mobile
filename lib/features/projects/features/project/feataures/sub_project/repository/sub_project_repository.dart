import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/project/feataures/sub_project/models/add_sub_project_post_model.dart';

class SubProjectRepository {
  final HttpClient httpClient;

  SubProjectRepository({@required this.httpClient});

  Future<Response> addSubProjectToList(
      AddSubProjectPostModel subProject) async {
    return await httpClient.dio.patch(EndpointConfig.projectSubproject + '/add',
        data: subProject.toJson());
  }
}
