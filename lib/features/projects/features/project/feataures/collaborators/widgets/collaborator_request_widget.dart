import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/more/company/features/companies/bloc/companies_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/bloc/add_collaborators_bloc/add_colloborators_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/bloc/collaborators/collaborators_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/add_collaborator_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CollaboratorRequestWidget extends StatefulWidget {
  @override
  _CollaboratorRequestWidgetState createState() =>
      _CollaboratorRequestWidgetState();
}

class _CollaboratorRequestWidgetState extends State<CollaboratorRequestWidget> {
  TextEditingController subProjectNameController = TextEditingController();
  final GlobalKey<FormFieldState> _dropDownKey = GlobalKey<FormFieldState>();

  String company;

  bool autoValidate = false;
  bool submitted = false;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void reset() {
    subProjectNameController.clear();
    _dropDownKey.currentState.reset();

    setState(() {
      company = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GlobalBloc, GlobalState>(
      listener: (context, state) {
        if (state is SuccessSnackBarState) {
          if (state.event == SucceessEvents.AddCollaborator) {
            reset();
            FocusScope.of(context).requestFocus(FocusNode());

            setState(() {
              submitted = false;
              autoValidate = false;
            });
          }
        }

        if (state is ErrorSnackBarState) {
          FocusScope.of(context).requestFocus(FocusNode());
          setState(() {
            submitted = false;
            autoValidate = false;
          });
        }
      },
      child: BlocProvider<AddColloboratorsBloc>(
        create: (context) => AddColloboratorsBloc(
            collaboratorsBloc: context.read<CollaboratorsBloc>(),
            globalBloc: context.read<GlobalBloc>(),
            projectBloc: context.read<ProjectBloc>(),
            userRequestsRepository: sl()),
        child: BlocBuilder<AddColloboratorsBloc, AddColloboratorsState>(
          builder: (context, state) {
            return Form(
              key: _formKey,
              autovalidateMode: autoValidate
                  ? AutovalidateMode.always
                  : AutovalidateMode.disabled,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                    decoration: BoxDecoration(
                        color: ColorConfig.blue4,
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Subproject name',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.black,
                                fontSize: 12)),
                        SizedBox(height: 10),
                        TextFormField(
                          controller: subProjectNameController,
                          validator: (val) {
                            if (val.isEmpty) {
                              return 'Required';
                            }

                            return null;
                          },
                          decoration: profileInputDecoration.copyWith(
                              hintText: 'Type Subproject name',
                              hintStyle: TextStyle(
                                  color: ColorConfig.black1,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500)),
                          maxLines: null,
                          style: textStyle2,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Company name',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Colors.black,
                                fontSize: 12)),
                        SizedBox(
                          height: height2,
                        ),
                        BlocProvider<CompaniesBloc>(
                          create: (context) =>
                              CompaniesBloc(companiesRepository: sl()),
                          child: BlocBuilder<CompaniesBloc, CompaniesState>(
                            builder: (context, state) {
                              return DropdownButtonFormField(
                                key: _dropDownKey,
                                validator: (val) {
                                  if (val == null || val.isEmpty) {
                                    return 'Required';
                                  }

                                  return null;
                                },
                                dropdownColor: ColorConfig.primary,
                                decoration: profileInputDecoration.copyWith(
                                    hintText: 'Select Company',
                                    hintStyle: TextStyle(
                                        color: ColorConfig.black1,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500)),
                                value: company,
                                items: (state is CompaniesLoadSuccess)
                                    ? state.companies
                                        .map((e) => DropdownMenuItem(
                                              value: e.id,
                                              child: Text(
                                                e.name,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.white),
                                              ),
                                            ))
                                        .toList()
                                    : [],
                                selectedItemBuilder: (context) =>
                                    (state is CompaniesLoadSuccess)
                                        ? state.companies
                                            .map((e) => DropdownMenuItem(
                                                  value: e.id,
                                                  child: Text(
                                                    e.name,
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.black),
                                                  ),
                                                ))
                                            .toList()
                                        : [],
                                onChanged: (val) {
                                  company = val;
                                },
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            RawMaterialButton(
                              constraints: BoxConstraints(),
                              onPressed: () {
                                reset();
                              },
                              shape: CircleBorder(),
                              fillColor: ColorConfig.grey6,
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.close,
                                size: 20,
                              ),
                            ),
                            RawMaterialButton(
                              constraints: BoxConstraints(),
                              onPressed: () {
                                setState(() {
                                  autoValidate = true;
                                });
                                if (_formKey.currentState.validate()) {
                                  if (!submitted) {
                                    submitted = true;

                                    AddCollaboratorModel data =
                                        AddCollaboratorModel((model) => model
                                          ..owner = company
                                          ..projectName =
                                              subProjectNameController.text);

                                    context.read<AddColloboratorsBloc>().add(
                                        AddCollaboratorEvent(
                                            collaborator: data));
                                  }
                                }
                              },
                              shape: CircleBorder(),
                              fillColor: ColorConfig.primary,
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.check,
                                color: Colors.white,
                                size: 20,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
