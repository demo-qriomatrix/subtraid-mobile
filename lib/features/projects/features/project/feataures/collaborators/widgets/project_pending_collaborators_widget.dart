import 'package:Subtraid/features/projects/features/project/feataures/collaborators/bloc/collaborators/collaborators_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/view/collaborators_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'collaborator_request_widget.dart';

class ProjectPendingCollaboratorsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CollaboratorsBloc, CollaboratorsState>(
        builder: (context, state) {
      if (state is CollaboratorsLoadSuccess) {
        return ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            CollaboratorRequestWidget(),
            Column(
              children: state.requests
                  .map((e) => CollaboratorCard(
                        type: state.type,
                        requestsModel: e,
                        projectCollaboratorsModel: null,
                      ))
                  .toList(),
            ),
            SizedBox(
              height: 40,
            )
          ],
        );
      }

      return Container();
    });
  }
}
