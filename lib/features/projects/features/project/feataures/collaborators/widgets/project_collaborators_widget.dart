import 'package:Subtraid/core/enums/collaborator_type_enum.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/view/collaborators_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'collaborator_request_widget.dart';

class ProjectCollaboratorsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProjectBloc, ProjectState>(
      builder: (context, state) {
        if (state is ProjectLoadSuccess) {
          return ListView(padding: EdgeInsets.zero, children: <Widget>[
            SizedBox(
              height: 30,
            ),
            CollaboratorRequestWidget(),
            Column(
              children: state.projectModel.collaborators
                  .map((e) => CollaboratorCard(
                      projectCollaboratorsModel: e,
                      requestsModel: null,
                      type: CollaboratorType.Project))
                  .toList(),
            )
          ]);
        }

        return Container();
      },
    );
  }
}
