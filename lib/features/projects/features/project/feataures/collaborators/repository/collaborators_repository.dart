import 'dart:convert';

import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/add_collaborator_model.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/requests_response_model.dart';

class UserRequestsRepository {
  final HttpClient httpClient;

  UserRequestsRepository({@required this.httpClient});

  Future<RequestsResponseModel> getCollaborators() async {
    Map<String, dynamic> queryParams = Map<String, dynamic>()
      ..putIfAbsent('type', () => 'collaborator');

    Response response = await httpClient.dio
        .get(EndpointConfig.userRequests, queryParameters: queryParams);

    return RequestsResponseModel.fromJson(json.encode(response.data));
  }

  Future<RequestsResponseModel> getEmployement() async {
    Map<String, dynamic> queryParams = Map<String, dynamic>()
      ..putIfAbsent('type', () => 'employment');

    Response response = await httpClient.dio
        .get(EndpointConfig.userRequests, queryParameters: queryParams);

    return RequestsResponseModel.fromJson(json.encode(response.data));
  }

  Future<ApiResponseModel> updateEmployeement(
      String requestId, bool accept) async {
    Map<String, dynamic> queryParams = Map<String, dynamic>()
      ..putIfAbsent('requestId', () => requestId)
      ..putIfAbsent('accept', () => accept);

    Response response = await httpClient.dio.post(
        EndpointConfig.userAcceptEmployeement,
        queryParameters: queryParams);

    return ApiResponseModel.fromJson(json.encode(response.data));
  }

  Future<Response> acceptCollaborators(String requestId, bool action) async {
    Response response = await httpClient.dio.post(
        EndpointConfig.acceptCollaborator +
            '?requestId=$requestId&accept=$action');

    return response;
  }

  Future<Response> addCollaborator(
      AddCollaboratorModel addCollaboratorModel) async {
    Response response = await httpClient.dio.post(
        EndpointConfig.createCollaborator,
        data: addCollaboratorModel.toJson());

    return response;
  }
}
