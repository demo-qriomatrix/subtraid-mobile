import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/enums/collaborator_type_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/bloc/collaborators/collaborators_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/widgets/project_collaborators_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/widgets/project_pending_collaborators_widget.dart';
import 'package:Subtraid/features/projects/features/project/models/project_collaborators_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/custom_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CollaboratorsScreen extends StatefulWidget {
  @override
  _CollaboratorsScreenState createState() => _CollaboratorsScreenState();
}

class _CollaboratorsScreenState extends State<CollaboratorsScreen> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CollaboratorsBloc>(
      create: (context) => CollaboratorsBloc(
          type: CollaboratorType.Pending,
          globalBloc: context.read<GlobalBloc>(),
          projectBloc: context.read<ProjectBloc>(),
          authenticationBloc: context.read<AuthenticationBloc>(),
          userRequestsRepository: sl()),
      child: Column(children: <Widget>[
        // StAppBar(title: 'Collaborators'),
        Expanded(
          child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: getMinHeight(context) + 30,
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 20,
                          ),
                          CustomIcon(
                            icon: LocalImages.userCheck,
                            title: 'Collaborators',
                            selected: selectedIndex == 0,
                            ontap: () {
                              setState(() {
                                selectedIndex = 0;
                              });
                            },
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          CustomIcon(
                            icon: LocalImages.userClock,
                            title: 'Pending',
                            selected: selectedIndex == 1,
                            ontap: () {
                              setState(() {
                                selectedIndex = 1;
                              });
                            },
                          ),
                        ],
                      ),
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            Offstage(
                                offstage: selectedIndex != 0,
                                child: ProjectCollaboratorsWidget()),
                            Offstage(
                                offstage: selectedIndex != 1,
                                child: ProjectPendingCollaboratorsWidget()),
                          ],
                        ),
                      ),
                    ],
                  ))),
        )
      ]),
    );
  }
}

class CollaboratorCard extends StatelessWidget {
  final RequestsModel requestsModel;
  final CollaboratorType type;
  final ProjectCollaboratorsModel projectCollaboratorsModel;

  CollaboratorCard(
      {@required this.requestsModel,
      @required this.type,
      @required this.projectCollaboratorsModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
              blurRadius: 12,
              color: ColorConfig.shadow2,
              spreadRadius: 1,
              offset: Offset(0, 3))
        ],
      ),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   children: <Widget>[
          //     RawMaterialButton(
          //       constraints: BoxConstraints(),
          //       onPressed: () {},
          //       shape: CircleBorder(),
          //       padding: const EdgeInsets.all(8.0),
          //       child: Icon(
          //         Icons.delete,
          //         color: ColorConfig.red3,
          //       ),
          //     ),
          //   ],
          // ),
          Text(
            type == CollaboratorType.Incoming
                ? requestsModel?.parseMessage?.parentProjectName ?? ''
                : type == CollaboratorType.Pending
                    ? requestsModel?.parseMessage?.name ?? ''
                    : type == CollaboratorType.Project &&
                            (context.watch<ProjectBloc>().state
                                is ProjectLoadSuccess)
                        ? (context.watch<ProjectBloc>().state
                                as ProjectLoadSuccess)
                            .projectModel
                            .name
                        : '',
            style: TextStyle(
                color: ColorConfig.primary,
                fontSize: 20,
                fontWeight: FontWeight.w700),
          ),
          type == CollaboratorType.Incoming
              ? Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      requestsModel?.parseMessage?.name ?? '',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              : Container(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: <Widget>[
              Text(
                type == CollaboratorType.Incoming ? 'Owned By' : 'Authored By',
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontSize: 15,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                width: 10,
              ),
              CircleAvatar(
                backgroundImage: AssetImage(LocalImages.defaultAvatar),
                radius: 14,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                type == CollaboratorType.Incoming
                    ? requestsModel?.parseMessage?.authorName ?? ''
                    : type == CollaboratorType.Pending
                        ? requestsModel?.parseMessage?.ownerName ?? ''
                        : type == CollaboratorType.Project
                            ? projectCollaboratorsModel?.name
                            : '',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),

          type == CollaboratorType.Incoming
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      color: ColorConfig.primary,
                      padding: EdgeInsets.zero,
                      child: Text(
                        'Accept',
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                      onPressed: () {
                        context.read<CollaboratorsBloc>().add(
                            CollaboratorAccept(
                                requestId: requestsModel.id, action: true));
                      },
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    FlatButton(
                      padding: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      color: ColorConfig.red1,
                      child: Text(
                        'Delete',
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      ),
                      onPressed: () {
                        context.read<CollaboratorsBloc>().add(
                            CollaboratorAccept(
                                requestId: requestsModel.id, action: false));
                      },
                    ),
                  ],
                )
              : Container()
          // Row(
          //   children: <Widget>[
          //     Text(
          //       'List Label',
          //       style: TextStyle(
          //           color: ColorConfig.primary,
          //           fontSize: 15,
          //           fontWeight: FontWeight.w400),
          //     ),
          //     SizedBox(
          //       width: 25,
          //     ),
          //     Text(
          //       'Ready For Inspection',
          //       style: TextStyle(
          //           color: Colors.black,
          //           fontSize: 14,
          //           fontWeight: FontWeight.w400),
          //     ),
          //   ],
          // ),
          // SizedBox(
          //   height: 15,
          // ),
          // FlatButton(
          //   padding: EdgeInsets.symmetric(horizontal: 4),
          //   shape:
          //       RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          //   color: ColorConfig.primary,
          //   child: Text(
          //     "Invite",
          //     style: TextStyle(fontSize: 13, color: Colors.white),
          //   ),
          //   onPressed: () {},
          // )
        ],
      ),
    );
  }
}
