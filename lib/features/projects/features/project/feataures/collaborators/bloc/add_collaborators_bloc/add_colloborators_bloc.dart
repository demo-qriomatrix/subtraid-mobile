import 'dart:async';

import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/bloc/collaborators/collaborators_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/add_collaborator_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/repository/collaborators_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'add_colloborators_event.dart';
part 'add_colloborators_state.dart';

class AddColloboratorsBloc
    extends Bloc<AddColloboratorsEvent, AddColloboratorsState> {
  AddColloboratorsBloc(
      {@required this.globalBloc,
      @required this.projectBloc,
      @required this.collaboratorsBloc,
      @required this.userRequestsRepository})
      : super(AddColloboratorsInitial());

  final UserRequestsRepository userRequestsRepository;
  final GlobalBloc globalBloc;
  final ProjectBloc projectBloc;
  final CollaboratorsBloc collaboratorsBloc;

  @override
  Stream<AddColloboratorsState> mapEventToState(
    AddColloboratorsEvent event,
  ) async* {
    if (event is AddCollaboratorEvent) {
      yield* _mapAddCollaboratorEventToState(event);
    }
  }

  Stream<AddColloboratorsState> _mapAddCollaboratorEventToState(
      AddCollaboratorEvent event) async* {
    globalBloc.add(ShowLoadingWidget());

    try {
      AddCollaboratorModel collaborator = event.collaborator;

      String projectId;

      if (projectBloc.state is ProjectLoadSuccess)
        projectId = (projectBloc.state as ProjectLoadSuccess).projectId;

      collaborator = collaborator.rebuild((e) => e.parentProjectId = projectId);

      await userRequestsRepository.addCollaborator(collaborator);

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Request has been sent successfully',
          event: SucceessEvents.AddCollaborator));

      if (collaboratorsBloc != null)
        collaboratorsBloc.add(CollaboratorsRequested());
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }
}
