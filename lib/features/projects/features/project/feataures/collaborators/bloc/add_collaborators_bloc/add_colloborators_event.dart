part of 'add_colloborators_bloc.dart';

abstract class AddColloboratorsEvent extends Equatable {
  const AddColloboratorsEvent();

  @override
  List<Object> get props => [];
}

class AddCollaboratorEvent extends AddColloboratorsEvent {
  final AddCollaboratorModel collaborator;

  AddCollaboratorEvent({@required this.collaborator});
}
