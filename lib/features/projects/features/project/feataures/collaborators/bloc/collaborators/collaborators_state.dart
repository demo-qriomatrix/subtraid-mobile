part of 'collaborators_bloc.dart';

abstract class CollaboratorsState extends Equatable {
  const CollaboratorsState();

  @override
  List<Object> get props => [];
}

class CollaboratorsInitial extends CollaboratorsState {}

class CollaboratorsLoadInProgress extends CollaboratorsState {}

class CollaboratorsLoadSuccess extends CollaboratorsState {
  final CollaboratorType type;
  final List<RequestsModel> requests;

  CollaboratorsLoadSuccess({@required this.requests, @required this.type});
}

class CollaboratorsLoadFailure extends CollaboratorsState {}
