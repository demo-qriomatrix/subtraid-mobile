part of 'collaborators_bloc.dart';

abstract class CollaboratorsEvent extends Equatable {
  const CollaboratorsEvent();

  @override
  List<Object> get props => [];
}

class CollaboratorsRequested extends CollaboratorsEvent {}

class CollaboratorAccept extends CollaboratorsEvent {
  final String requestId;
  final bool action;

  CollaboratorAccept({@required this.requestId, @required this.action});
}

class CollaboratorDeny extends CollaboratorsEvent {
  final String requestId;

  CollaboratorDeny({@required this.requestId});
}
