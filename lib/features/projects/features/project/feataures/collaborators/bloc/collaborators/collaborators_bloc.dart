import 'dart:async';

import 'package:Subtraid/core/enums/collaborator_type_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/requests_response_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/repository/collaborators_repository.dart';
import 'package:meta/meta.dart';

part 'collaborators_event.dart';
part 'collaborators_state.dart';

class CollaboratorsBloc extends Bloc<CollaboratorsEvent, CollaboratorsState> {
  CollaboratorsBloc({
    @required this.userRequestsRepository,
    @required this.authenticationBloc,
    @required this.globalBloc,
    @required this.type,
    @required this.projectBloc,
  }) : super(CollaboratorsInitial()) {
    add(CollaboratorsRequested());
  }

  final CollaboratorType type;

  final UserRequestsRepository userRequestsRepository;
  final AuthenticationBloc authenticationBloc;
  final GlobalBloc globalBloc;
  final ProjectBloc projectBloc;

  @override
  Stream<CollaboratorsState> mapEventToState(
    CollaboratorsEvent event,
  ) async* {
    if (event is CollaboratorsRequested) {
      yield* _mapCollaboratorsRequestedToState();
    }

    if (event is CollaboratorAccept) {
      yield* _mapCollaboratorAcceptToState(event);
    }
  }

  Stream<CollaboratorsState> _mapCollaboratorAcceptToState(
      CollaboratorAccept request) async* {
    globalBloc.add(ShowLoadingWidget());
    try {
      await userRequestsRepository.acceptCollaborators(
          request.requestId, request.action);

      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowSuccessSnackBar(
          message:
              'Request ${request.action ? 'accepted' : 'denied'} successfully',
          event: null));

      add(CollaboratorsRequested());
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<CollaboratorsState> _mapCollaboratorsRequestedToState() async* {
    yield CollaboratorsLoadInProgress();

    try {
      RequestsResponseModel requestsResponseModel =
          await userRequestsRepository.getCollaborators();

      List<RequestsModel> requests = [];
      String userId;
      if (authenticationBloc.state is AuthentcatedState) {
        userId = (authenticationBloc.state as AuthentcatedState).user.id;
      }

      if (type == CollaboratorType.Incoming) {
        requests = requestsResponseModel.requests
            .where((element) => element?.parseMessage?.owner == userId)
            .toList();
      }

      if (type == CollaboratorType.Pending) {
        String projectId;

        if (projectBloc.state is ProjectLoadSuccess)
          projectId = (projectBloc.state as ProjectLoadSuccess).projectId;
        requests = requestsResponseModel.requests
            .where((element) => element?.parseMessage?.parent == projectId)
            .toList();
      }

      yield CollaboratorsLoadSuccess(requests: requests, type: type);
    } catch (e) {
      print(e);

      yield CollaboratorsLoadFailure();
    }
  }
}
