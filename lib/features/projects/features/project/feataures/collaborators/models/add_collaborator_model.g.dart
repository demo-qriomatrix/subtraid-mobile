// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_collaborator_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddCollaboratorModel> _$addCollaboratorModelSerializer =
    new _$AddCollaboratorModelSerializer();

class _$AddCollaboratorModelSerializer
    implements StructuredSerializer<AddCollaboratorModel> {
  @override
  final Iterable<Type> types = const [
    AddCollaboratorModel,
    _$AddCollaboratorModel
  ];
  @override
  final String wireName = 'AddCollaboratorModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, AddCollaboratorModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.owner != null) {
      result
        ..add('owner')
        ..add(serializers.serialize(object.owner,
            specifiedType: const FullType(String)));
    }
    if (object.parentProjectId != null) {
      result
        ..add('parentProjectId')
        ..add(serializers.serialize(object.parentProjectId,
            specifiedType: const FullType(String)));
    }
    if (object.projectName != null) {
      result
        ..add('projectName')
        ..add(serializers.serialize(object.projectName,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AddCollaboratorModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddCollaboratorModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'owner':
          result.owner = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'parentProjectId':
          result.parentProjectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectName':
          result.projectName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AddCollaboratorModel extends AddCollaboratorModel {
  @override
  final String owner;
  @override
  final String parentProjectId;
  @override
  final String projectName;

  factory _$AddCollaboratorModel(
          [void Function(AddCollaboratorModelBuilder) updates]) =>
      (new AddCollaboratorModelBuilder()..update(updates)).build();

  _$AddCollaboratorModel._({this.owner, this.parentProjectId, this.projectName})
      : super._();

  @override
  AddCollaboratorModel rebuild(
          void Function(AddCollaboratorModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddCollaboratorModelBuilder toBuilder() =>
      new AddCollaboratorModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddCollaboratorModel &&
        owner == other.owner &&
        parentProjectId == other.parentProjectId &&
        projectName == other.projectName;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, owner.hashCode), parentProjectId.hashCode),
        projectName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddCollaboratorModel')
          ..add('owner', owner)
          ..add('parentProjectId', parentProjectId)
          ..add('projectName', projectName))
        .toString();
  }
}

class AddCollaboratorModelBuilder
    implements Builder<AddCollaboratorModel, AddCollaboratorModelBuilder> {
  _$AddCollaboratorModel _$v;

  String _owner;
  String get owner => _$this._owner;
  set owner(String owner) => _$this._owner = owner;

  String _parentProjectId;
  String get parentProjectId => _$this._parentProjectId;
  set parentProjectId(String parentProjectId) =>
      _$this._parentProjectId = parentProjectId;

  String _projectName;
  String get projectName => _$this._projectName;
  set projectName(String projectName) => _$this._projectName = projectName;

  AddCollaboratorModelBuilder();

  AddCollaboratorModelBuilder get _$this {
    if (_$v != null) {
      _owner = _$v.owner;
      _parentProjectId = _$v.parentProjectId;
      _projectName = _$v.projectName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddCollaboratorModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddCollaboratorModel;
  }

  @override
  void update(void Function(AddCollaboratorModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddCollaboratorModel build() {
    final _$result = _$v ??
        new _$AddCollaboratorModel._(
            owner: owner,
            parentProjectId: parentProjectId,
            projectName: projectName);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
