// GENERATED CODE - DO NOT MODIFY BY HAND

part of request_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestsModel> _$requestsModelSerializer =
    new _$RequestsModelSerializer();

class _$RequestsModelSerializer implements StructuredSerializer<RequestsModel> {
  @override
  final Iterable<Type> types = const [RequestsModel, _$RequestsModel];
  @override
  final String wireName = 'RequestsModel';

  @override
  Iterable<Object> serialize(Serializers serializers, RequestsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.reqType != null) {
      result
        ..add('reqType')
        ..add(serializers.serialize(object.reqType,
            specifiedType: const FullType(String)));
    }
    if (object.senderId != null) {
      result
        ..add('senderId')
        ..add(serializers.serialize(object.senderId,
            specifiedType: const FullType(String)));
    }
    if (object.receiverId != null) {
      result
        ..add('receiverId')
        ..add(serializers.serialize(object.receiverId,
            specifiedType: const FullType(String)));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(JsonObject)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RequestsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'reqType':
          result.reqType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'senderId':
          result.senderId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'receiverId':
          result.receiverId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RequestsModel extends RequestsModel {
  @override
  final String id;
  @override
  final String reqType;
  @override
  final String senderId;
  @override
  final String receiverId;
  @override
  final JsonObject message;
  @override
  final String dateCreated;

  factory _$RequestsModel([void Function(RequestsModelBuilder) updates]) =>
      (new RequestsModelBuilder()..update(updates)).build();

  _$RequestsModel._(
      {this.id,
      this.reqType,
      this.senderId,
      this.receiverId,
      this.message,
      this.dateCreated})
      : super._();

  @override
  RequestsModel rebuild(void Function(RequestsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestsModelBuilder toBuilder() => new RequestsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestsModel &&
        id == other.id &&
        reqType == other.reqType &&
        senderId == other.senderId &&
        receiverId == other.receiverId &&
        message == other.message &&
        dateCreated == other.dateCreated;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), reqType.hashCode),
                    senderId.hashCode),
                receiverId.hashCode),
            message.hashCode),
        dateCreated.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestsModel')
          ..add('id', id)
          ..add('reqType', reqType)
          ..add('senderId', senderId)
          ..add('receiverId', receiverId)
          ..add('message', message)
          ..add('dateCreated', dateCreated))
        .toString();
  }
}

class RequestsModelBuilder
    implements Builder<RequestsModel, RequestsModelBuilder> {
  _$RequestsModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _reqType;
  String get reqType => _$this._reqType;
  set reqType(String reqType) => _$this._reqType = reqType;

  String _senderId;
  String get senderId => _$this._senderId;
  set senderId(String senderId) => _$this._senderId = senderId;

  String _receiverId;
  String get receiverId => _$this._receiverId;
  set receiverId(String receiverId) => _$this._receiverId = receiverId;

  JsonObject _message;
  JsonObject get message => _$this._message;
  set message(JsonObject message) => _$this._message = message;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  RequestsModelBuilder();

  RequestsModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _reqType = _$v.reqType;
      _senderId = _$v.senderId;
      _receiverId = _$v.receiverId;
      _message = _$v.message;
      _dateCreated = _$v.dateCreated;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestsModel;
  }

  @override
  void update(void Function(RequestsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestsModel build() {
    final _$result = _$v ??
        new _$RequestsModel._(
            id: id,
            reqType: reqType,
            senderId: senderId,
            receiverId: receiverId,
            message: message,
            dateCreated: dateCreated);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
