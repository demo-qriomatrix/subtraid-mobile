library request_member_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_owner_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_member_model.g.dart';

abstract class RequestMemberModel
    implements Built<RequestMemberModel, RequestMemberModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  RequestOwnerModel get owner;

  RequestMemberModel._();

  factory RequestMemberModel([updates(RequestMemberModelBuilder b)]) =
      _$RequestMemberModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(RequestMemberModel.serializer, this));
  }

  static RequestMemberModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestMemberModel.serializer, json.decode(jsonString));
  }

  static Serializer<RequestMemberModel> get serializer =>
      _$requestMemberModelSerializer;
}
