library request_owner_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_owner_model.g.dart';

abstract class RequestOwnerModel
    implements Built<RequestOwnerModel, RequestOwnerModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  String get img;

  RequestOwnerModel._();

  factory RequestOwnerModel([updates(RequestOwnerModelBuilder b)]) =
      _$RequestOwnerModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(RequestOwnerModel.serializer, this));
  }

  static RequestOwnerModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestOwnerModel.serializer, json.decode(jsonString));
  }

  static Serializer<RequestOwnerModel> get serializer =>
      _$requestOwnerModelSerializer;
}
