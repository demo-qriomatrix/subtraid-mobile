// GENERATED CODE - DO NOT MODIFY BY HAND

part of request_owner_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestOwnerModel> _$requestOwnerModelSerializer =
    new _$RequestOwnerModelSerializer();

class _$RequestOwnerModelSerializer
    implements StructuredSerializer<RequestOwnerModel> {
  @override
  final Iterable<Type> types = const [RequestOwnerModel, _$RequestOwnerModel];
  @override
  final String wireName = 'RequestOwnerModel';

  @override
  Iterable<Object> serialize(Serializers serializers, RequestOwnerModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RequestOwnerModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestOwnerModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RequestOwnerModel extends RequestOwnerModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final String img;

  factory _$RequestOwnerModel(
          [void Function(RequestOwnerModelBuilder) updates]) =>
      (new RequestOwnerModelBuilder()..update(updates)).build();

  _$RequestOwnerModel._({this.id, this.name, this.img}) : super._();

  @override
  RequestOwnerModel rebuild(void Function(RequestOwnerModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestOwnerModelBuilder toBuilder() =>
      new RequestOwnerModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestOwnerModel &&
        id == other.id &&
        name == other.name &&
        img == other.img;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), img.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestOwnerModel')
          ..add('id', id)
          ..add('name', name)
          ..add('img', img))
        .toString();
  }
}

class RequestOwnerModelBuilder
    implements Builder<RequestOwnerModel, RequestOwnerModelBuilder> {
  _$RequestOwnerModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  RequestOwnerModelBuilder();

  RequestOwnerModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _img = _$v.img;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestOwnerModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestOwnerModel;
  }

  @override
  void update(void Function(RequestOwnerModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestOwnerModel build() {
    final _$result =
        _$v ?? new _$RequestOwnerModel._(id: id, name: name, img: img);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
