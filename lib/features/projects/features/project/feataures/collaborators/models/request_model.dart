library request_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/more/company/features/pending/models/request_employee_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_message_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

part 'request_model.g.dart';

abstract class RequestsModel
    implements Built<RequestsModel, RequestsModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get reqType;

  @nullable
  String get senderId;

  @nullable
  String get receiverId;

  @nullable
  JsonObject get message;

  @nullable
  RequestEmployeeModel get parseEmployeeModel {
    if (message != null && message.isString) {
      try {
        return RequestEmployeeModel.fromJson(message.asString);
      } catch (e) {
        print(e);
      }
    }

    return null;
  }

  @nullable
  RequestMessageModel get parseMessage {
    if (message != null && message.isString) {
      return RequestMessageModel.fromJson(message.asString);
    }

    return null;
  }

  @nullable
  String get dateCreated;

  RequestsModel._();

  factory RequestsModel([updates(RequestsModelBuilder b)]) = _$RequestsModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(RequestsModel.serializer, this));
  }

  static RequestsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestsModel.serializer, json.decode(jsonString));
  }

  static Serializer<RequestsModel> get serializer => _$requestsModelSerializer;
}
