library requests_response_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/models/request_model.dart';

part 'requests_response_model.g.dart';

abstract class RequestsResponseModel
    implements Built<RequestsResponseModel, RequestsResponseModelBuilder> {
  BuiltList<RequestsModel> get requests;

  RequestsResponseModel._();

  factory RequestsResponseModel([updates(RequestsResponseModelBuilder b)]) =
      _$RequestsResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(RequestsResponseModel.serializer, this));
  }

  static RequestsResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestsResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<RequestsResponseModel> get serializer =>
      _$requestsResponseModelSerializer;
}
