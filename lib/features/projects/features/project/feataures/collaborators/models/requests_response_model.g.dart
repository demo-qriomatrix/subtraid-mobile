// GENERATED CODE - DO NOT MODIFY BY HAND

part of requests_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestsResponseModel> _$requestsResponseModelSerializer =
    new _$RequestsResponseModelSerializer();

class _$RequestsResponseModelSerializer
    implements StructuredSerializer<RequestsResponseModel> {
  @override
  final Iterable<Type> types = const [
    RequestsResponseModel,
    _$RequestsResponseModel
  ];
  @override
  final String wireName = 'RequestsResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, RequestsResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'requests',
      serializers.serialize(object.requests,
          specifiedType:
              const FullType(BuiltList, const [const FullType(RequestsModel)])),
    ];

    return result;
  }

  @override
  RequestsResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestsResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'requests':
          result.requests.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(RequestsModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$RequestsResponseModel extends RequestsResponseModel {
  @override
  final BuiltList<RequestsModel> requests;

  factory _$RequestsResponseModel(
          [void Function(RequestsResponseModelBuilder) updates]) =>
      (new RequestsResponseModelBuilder()..update(updates)).build();

  _$RequestsResponseModel._({this.requests}) : super._() {
    if (requests == null) {
      throw new BuiltValueNullFieldError('RequestsResponseModel', 'requests');
    }
  }

  @override
  RequestsResponseModel rebuild(
          void Function(RequestsResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestsResponseModelBuilder toBuilder() =>
      new RequestsResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestsResponseModel && requests == other.requests;
  }

  @override
  int get hashCode {
    return $jf($jc(0, requests.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestsResponseModel')
          ..add('requests', requests))
        .toString();
  }
}

class RequestsResponseModelBuilder
    implements Builder<RequestsResponseModel, RequestsResponseModelBuilder> {
  _$RequestsResponseModel _$v;

  ListBuilder<RequestsModel> _requests;
  ListBuilder<RequestsModel> get requests =>
      _$this._requests ??= new ListBuilder<RequestsModel>();
  set requests(ListBuilder<RequestsModel> requests) =>
      _$this._requests = requests;

  RequestsResponseModelBuilder();

  RequestsResponseModelBuilder get _$this {
    if (_$v != null) {
      _requests = _$v.requests?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestsResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestsResponseModel;
  }

  @override
  void update(void Function(RequestsResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestsResponseModel build() {
    _$RequestsResponseModel _$result;
    try {
      _$result =
          _$v ?? new _$RequestsResponseModel._(requests: requests.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'requests';
        requests.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'RequestsResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
