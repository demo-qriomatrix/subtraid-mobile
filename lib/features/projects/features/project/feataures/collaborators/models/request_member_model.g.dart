// GENERATED CODE - DO NOT MODIFY BY HAND

part of request_member_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestMemberModel> _$requestMemberModelSerializer =
    new _$RequestMemberModelSerializer();

class _$RequestMemberModelSerializer
    implements StructuredSerializer<RequestMemberModel> {
  @override
  final Iterable<Type> types = const [RequestMemberModel, _$RequestMemberModel];
  @override
  final String wireName = 'RequestMemberModel';

  @override
  Iterable<Object> serialize(Serializers serializers, RequestMemberModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.owner != null) {
      result
        ..add('owner')
        ..add(serializers.serialize(object.owner,
            specifiedType: const FullType(RequestOwnerModel)));
    }
    return result;
  }

  @override
  RequestMemberModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestMemberModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'owner':
          result.owner.replace(serializers.deserialize(value,
                  specifiedType: const FullType(RequestOwnerModel))
              as RequestOwnerModel);
          break;
      }
    }

    return result.build();
  }
}

class _$RequestMemberModel extends RequestMemberModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final RequestOwnerModel owner;

  factory _$RequestMemberModel(
          [void Function(RequestMemberModelBuilder) updates]) =>
      (new RequestMemberModelBuilder()..update(updates)).build();

  _$RequestMemberModel._({this.id, this.name, this.owner}) : super._();

  @override
  RequestMemberModel rebuild(
          void Function(RequestMemberModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestMemberModelBuilder toBuilder() =>
      new RequestMemberModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestMemberModel &&
        id == other.id &&
        name == other.name &&
        owner == other.owner;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), name.hashCode), owner.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestMemberModel')
          ..add('id', id)
          ..add('name', name)
          ..add('owner', owner))
        .toString();
  }
}

class RequestMemberModelBuilder
    implements Builder<RequestMemberModel, RequestMemberModelBuilder> {
  _$RequestMemberModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  RequestOwnerModelBuilder _owner;
  RequestOwnerModelBuilder get owner =>
      _$this._owner ??= new RequestOwnerModelBuilder();
  set owner(RequestOwnerModelBuilder owner) => _$this._owner = owner;

  RequestMemberModelBuilder();

  RequestMemberModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _owner = _$v.owner?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestMemberModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestMemberModel;
  }

  @override
  void update(void Function(RequestMemberModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestMemberModel build() {
    _$RequestMemberModel _$result;
    try {
      _$result = _$v ??
          new _$RequestMemberModel._(
              id: id, name: name, owner: _owner?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'owner';
        _owner?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'RequestMemberModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
