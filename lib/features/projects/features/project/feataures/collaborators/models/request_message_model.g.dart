// GENERATED CODE - DO NOT MODIFY BY HAND

part of request_message_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestMessageModel> _$requestMessageModelSerializer =
    new _$RequestMessageModelSerializer();

class _$RequestMessageModelSerializer
    implements StructuredSerializer<RequestMessageModel> {
  @override
  final Iterable<Type> types = const [
    RequestMessageModel,
    _$RequestMessageModel
  ];
  @override
  final String wireName = 'RequestMessageModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, RequestMessageModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.author != null) {
      result
        ..add('author')
        ..add(serializers.serialize(object.author,
            specifiedType: const FullType(String)));
    }
    if (object.authorName != null) {
      result
        ..add('authorName')
        ..add(serializers.serialize(object.authorName,
            specifiedType: const FullType(String)));
    }
    if (object.authorImg != null) {
      result
        ..add('authorImg')
        ..add(serializers.serialize(object.authorImg,
            specifiedType: const FullType(String)));
    }
    if (object.owner != null) {
      result
        ..add('owner')
        ..add(serializers.serialize(object.owner,
            specifiedType: const FullType(String)));
    }
    if (object.ownerName != null) {
      result
        ..add('ownerName')
        ..add(serializers.serialize(object.ownerName,
            specifiedType: const FullType(String)));
    }
    if (object.ownerImg != null) {
      result
        ..add('ownerImg')
        ..add(serializers.serialize(object.ownerImg,
            specifiedType: const FullType(String)));
    }
    if (object.parent != null) {
      result
        ..add('parent')
        ..add(serializers.serialize(object.parent,
            specifiedType: const FullType(String)));
    }
    if (object.parentProjectName != null) {
      result
        ..add('parentProjectName')
        ..add(serializers.serialize(object.parentProjectName,
            specifiedType: const FullType(String)));
    }
    if (object.companyName != null) {
      result
        ..add('companyName')
        ..add(serializers.serialize(object.companyName,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RequestMessageModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestMessageModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'authorName':
          result.authorName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'authorImg':
          result.authorImg = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'owner':
          result.owner = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ownerName':
          result.ownerName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ownerImg':
          result.ownerImg = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'parent':
          result.parent = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'parentProjectName':
          result.parentProjectName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'companyName':
          result.companyName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RequestMessageModel extends RequestMessageModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final String author;
  @override
  final String authorName;
  @override
  final String authorImg;
  @override
  final String owner;
  @override
  final String ownerName;
  @override
  final String ownerImg;
  @override
  final String parent;
  @override
  final String parentProjectName;
  @override
  final String companyName;

  factory _$RequestMessageModel(
          [void Function(RequestMessageModelBuilder) updates]) =>
      (new RequestMessageModelBuilder()..update(updates)).build();

  _$RequestMessageModel._(
      {this.id,
      this.name,
      this.author,
      this.authorName,
      this.authorImg,
      this.owner,
      this.ownerName,
      this.ownerImg,
      this.parent,
      this.parentProjectName,
      this.companyName})
      : super._();

  @override
  RequestMessageModel rebuild(
          void Function(RequestMessageModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestMessageModelBuilder toBuilder() =>
      new RequestMessageModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestMessageModel &&
        id == other.id &&
        name == other.name &&
        author == other.author &&
        authorName == other.authorName &&
        authorImg == other.authorImg &&
        owner == other.owner &&
        ownerName == other.ownerName &&
        ownerImg == other.ownerImg &&
        parent == other.parent &&
        parentProjectName == other.parentProjectName &&
        companyName == other.companyName;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                                        author.hashCode),
                                    authorName.hashCode),
                                authorImg.hashCode),
                            owner.hashCode),
                        ownerName.hashCode),
                    ownerImg.hashCode),
                parent.hashCode),
            parentProjectName.hashCode),
        companyName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestMessageModel')
          ..add('id', id)
          ..add('name', name)
          ..add('author', author)
          ..add('authorName', authorName)
          ..add('authorImg', authorImg)
          ..add('owner', owner)
          ..add('ownerName', ownerName)
          ..add('ownerImg', ownerImg)
          ..add('parent', parent)
          ..add('parentProjectName', parentProjectName)
          ..add('companyName', companyName))
        .toString();
  }
}

class RequestMessageModelBuilder
    implements Builder<RequestMessageModel, RequestMessageModelBuilder> {
  _$RequestMessageModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _author;
  String get author => _$this._author;
  set author(String author) => _$this._author = author;

  String _authorName;
  String get authorName => _$this._authorName;
  set authorName(String authorName) => _$this._authorName = authorName;

  String _authorImg;
  String get authorImg => _$this._authorImg;
  set authorImg(String authorImg) => _$this._authorImg = authorImg;

  String _owner;
  String get owner => _$this._owner;
  set owner(String owner) => _$this._owner = owner;

  String _ownerName;
  String get ownerName => _$this._ownerName;
  set ownerName(String ownerName) => _$this._ownerName = ownerName;

  String _ownerImg;
  String get ownerImg => _$this._ownerImg;
  set ownerImg(String ownerImg) => _$this._ownerImg = ownerImg;

  String _parent;
  String get parent => _$this._parent;
  set parent(String parent) => _$this._parent = parent;

  String _parentProjectName;
  String get parentProjectName => _$this._parentProjectName;
  set parentProjectName(String parentProjectName) =>
      _$this._parentProjectName = parentProjectName;

  String _companyName;
  String get companyName => _$this._companyName;
  set companyName(String companyName) => _$this._companyName = companyName;

  RequestMessageModelBuilder();

  RequestMessageModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _author = _$v.author;
      _authorName = _$v.authorName;
      _authorImg = _$v.authorImg;
      _owner = _$v.owner;
      _ownerName = _$v.ownerName;
      _ownerImg = _$v.ownerImg;
      _parent = _$v.parent;
      _parentProjectName = _$v.parentProjectName;
      _companyName = _$v.companyName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestMessageModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestMessageModel;
  }

  @override
  void update(void Function(RequestMessageModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestMessageModel build() {
    final _$result = _$v ??
        new _$RequestMessageModel._(
            id: id,
            name: name,
            author: author,
            authorName: authorName,
            authorImg: authorImg,
            owner: owner,
            ownerName: ownerName,
            ownerImg: ownerImg,
            parent: parent,
            parentProjectName: parentProjectName,
            companyName: companyName);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
