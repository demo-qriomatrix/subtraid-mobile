library request_message_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_message_model.g.dart';

abstract class RequestMessageModel
    implements Built<RequestMessageModel, RequestMessageModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  // @nullable
  // JsonObject get name;

  // @nullable
  // RequestMemberModel get getName {
  //   if (name != null && name.isString) {
  //     print(name);
  //     return RequestMemberModel((model) => model.name = name.asString);
  //   }

  //   if (name != null && name.isMap) {
  //     return RequestMemberModel.fromJson(json.encode(name.asMap));
  //   }
  //   return null;
  // }

  @nullable
  String get author;

  @nullable
  String get authorName;

  @nullable
  String get authorImg;
  @nullable
  String get owner;

  @nullable
  String get ownerName;

  @nullable
  String get ownerImg;

  // @nullable
  // List<Null> get members;

  @nullable
  String get parent;

  @nullable
  String get parentProjectName;

  @nullable
  String get companyName;

  RequestMessageModel._();

  factory RequestMessageModel([updates(RequestMessageModelBuilder b)]) =
      _$RequestMessageModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(RequestMessageModel.serializer, this));
  }

  static RequestMessageModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestMessageModel.serializer, json.decode(jsonString));
  }

  static Serializer<RequestMessageModel> get serializer =>
      _$requestMessageModelSerializer;
}
