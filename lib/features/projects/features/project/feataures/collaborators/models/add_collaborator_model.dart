library add_collaborator_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'add_collaborator_model.g.dart';

abstract class AddCollaboratorModel
    implements Built<AddCollaboratorModel, AddCollaboratorModelBuilder> {
  @nullable
  String get owner;

  @nullable
  String get parentProjectId;

  @nullable
  String get projectName;

  AddCollaboratorModel._();

  factory AddCollaboratorModel([updates(AddCollaboratorModelBuilder b)]) =
      _$AddCollaboratorModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(AddCollaboratorModel.serializer, this));
  }

  static AddCollaboratorModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddCollaboratorModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddCollaboratorModel> get serializer =>
      _$addCollaboratorModelSerializer;
}
