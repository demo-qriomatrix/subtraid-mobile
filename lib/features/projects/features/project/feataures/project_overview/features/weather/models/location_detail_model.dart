library location_detail_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'location_detail_model.g.dart';

abstract class LocationDetailModel
    implements Built<LocationDetailModel, LocationDetailModelBuilder> {
  @nullable
  String get city;

  @nullable
  String get region;

  @nullable
  int get woeid;

  @nullable
  String get country;

  @nullable
  double get lat;

  @nullable
  double get long;

  @nullable
  String get timezoneId;

  LocationDetailModel._();

  factory LocationDetailModel([updates(LocationDetailModelBuilder b)]) =
      _$LocationDetailModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(LocationDetailModel.serializer, this));
  }

  static LocationDetailModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        LocationDetailModel.serializer, json.decode(jsonString));
  }

  static Serializer<LocationDetailModel> get serializer =>
      _$locationDetailModelSerializer;
}
