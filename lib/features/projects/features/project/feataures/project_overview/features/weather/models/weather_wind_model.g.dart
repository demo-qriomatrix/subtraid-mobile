// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_wind_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherWindModel> _$weatherWindModelSerializer =
    new _$WeatherWindModelSerializer();

class _$WeatherWindModelSerializer
    implements StructuredSerializer<WeatherWindModel> {
  @override
  final Iterable<Type> types = const [WeatherWindModel, _$WeatherWindModel];
  @override
  final String wireName = 'WeatherWindModel';

  @override
  Iterable<Object> serialize(Serializers serializers, WeatherWindModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.chill != null) {
      result
        ..add('chill')
        ..add(serializers.serialize(object.chill,
            specifiedType: const FullType(int)));
    }
    if (object.direction != null) {
      result
        ..add('direction')
        ..add(serializers.serialize(object.direction,
            specifiedType: const FullType(int)));
    }
    if (object.speed != null) {
      result
        ..add('speed')
        ..add(serializers.serialize(object.speed,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  WeatherWindModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherWindModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'chill':
          result.chill = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'direction':
          result.direction = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'speed':
          result.speed = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherWindModel extends WeatherWindModel {
  @override
  final int chill;
  @override
  final int direction;
  @override
  final int speed;

  factory _$WeatherWindModel(
          [void Function(WeatherWindModelBuilder) updates]) =>
      (new WeatherWindModelBuilder()..update(updates)).build();

  _$WeatherWindModel._({this.chill, this.direction, this.speed}) : super._();

  @override
  WeatherWindModel rebuild(void Function(WeatherWindModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherWindModelBuilder toBuilder() =>
      new WeatherWindModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherWindModel &&
        chill == other.chill &&
        direction == other.direction &&
        speed == other.speed;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, chill.hashCode), direction.hashCode), speed.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherWindModel')
          ..add('chill', chill)
          ..add('direction', direction)
          ..add('speed', speed))
        .toString();
  }
}

class WeatherWindModelBuilder
    implements Builder<WeatherWindModel, WeatherWindModelBuilder> {
  _$WeatherWindModel _$v;

  int _chill;
  int get chill => _$this._chill;
  set chill(int chill) => _$this._chill = chill;

  int _direction;
  int get direction => _$this._direction;
  set direction(int direction) => _$this._direction = direction;

  int _speed;
  int get speed => _$this._speed;
  set speed(int speed) => _$this._speed = speed;

  WeatherWindModelBuilder();

  WeatherWindModelBuilder get _$this {
    if (_$v != null) {
      _chill = _$v.chill;
      _direction = _$v.direction;
      _speed = _$v.speed;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherWindModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherWindModel;
  }

  @override
  void update(void Function(WeatherWindModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherWindModel build() {
    final _$result = _$v ??
        new _$WeatherWindModel._(
            chill: chill, direction: direction, speed: speed);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
