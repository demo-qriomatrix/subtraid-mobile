library weather_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_model.dart';

part 'weather_response_model.g.dart';

abstract class WeatherResponseModel
    implements Built<WeatherResponseModel, WeatherResponseModelBuilder> {
  @nullable
  WeatherModel get weather;

  WeatherResponseModel._();

  factory WeatherResponseModel([updates(WeatherResponseModelBuilder b)]) =
      _$WeatherResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(WeatherResponseModel.serializer, this));
  }

  static WeatherResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherResponseModel> get serializer =>
      _$weatherResponseModelSerializer;
}
