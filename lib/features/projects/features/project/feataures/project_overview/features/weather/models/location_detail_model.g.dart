// GENERATED CODE - DO NOT MODIFY BY HAND

part of location_detail_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LocationDetailModel> _$locationDetailModelSerializer =
    new _$LocationDetailModelSerializer();

class _$LocationDetailModelSerializer
    implements StructuredSerializer<LocationDetailModel> {
  @override
  final Iterable<Type> types = const [
    LocationDetailModel,
    _$LocationDetailModel
  ];
  @override
  final String wireName = 'LocationDetailModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, LocationDetailModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.city != null) {
      result
        ..add('city')
        ..add(serializers.serialize(object.city,
            specifiedType: const FullType(String)));
    }
    if (object.region != null) {
      result
        ..add('region')
        ..add(serializers.serialize(object.region,
            specifiedType: const FullType(String)));
    }
    if (object.woeid != null) {
      result
        ..add('woeid')
        ..add(serializers.serialize(object.woeid,
            specifiedType: const FullType(int)));
    }
    if (object.country != null) {
      result
        ..add('country')
        ..add(serializers.serialize(object.country,
            specifiedType: const FullType(String)));
    }
    if (object.lat != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.lat,
            specifiedType: const FullType(double)));
    }
    if (object.long != null) {
      result
        ..add('long')
        ..add(serializers.serialize(object.long,
            specifiedType: const FullType(double)));
    }
    if (object.timezoneId != null) {
      result
        ..add('timezoneId')
        ..add(serializers.serialize(object.timezoneId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  LocationDetailModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LocationDetailModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'city':
          result.city = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'region':
          result.region = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'woeid':
          result.woeid = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'long':
          result.long = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'timezoneId':
          result.timezoneId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$LocationDetailModel extends LocationDetailModel {
  @override
  final String city;
  @override
  final String region;
  @override
  final int woeid;
  @override
  final String country;
  @override
  final double lat;
  @override
  final double long;
  @override
  final String timezoneId;

  factory _$LocationDetailModel(
          [void Function(LocationDetailModelBuilder) updates]) =>
      (new LocationDetailModelBuilder()..update(updates)).build();

  _$LocationDetailModel._(
      {this.city,
      this.region,
      this.woeid,
      this.country,
      this.lat,
      this.long,
      this.timezoneId})
      : super._();

  @override
  LocationDetailModel rebuild(
          void Function(LocationDetailModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LocationDetailModelBuilder toBuilder() =>
      new LocationDetailModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LocationDetailModel &&
        city == other.city &&
        region == other.region &&
        woeid == other.woeid &&
        country == other.country &&
        lat == other.lat &&
        long == other.long &&
        timezoneId == other.timezoneId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, city.hashCode), region.hashCode),
                        woeid.hashCode),
                    country.hashCode),
                lat.hashCode),
            long.hashCode),
        timezoneId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LocationDetailModel')
          ..add('city', city)
          ..add('region', region)
          ..add('woeid', woeid)
          ..add('country', country)
          ..add('lat', lat)
          ..add('long', long)
          ..add('timezoneId', timezoneId))
        .toString();
  }
}

class LocationDetailModelBuilder
    implements Builder<LocationDetailModel, LocationDetailModelBuilder> {
  _$LocationDetailModel _$v;

  String _city;
  String get city => _$this._city;
  set city(String city) => _$this._city = city;

  String _region;
  String get region => _$this._region;
  set region(String region) => _$this._region = region;

  int _woeid;
  int get woeid => _$this._woeid;
  set woeid(int woeid) => _$this._woeid = woeid;

  String _country;
  String get country => _$this._country;
  set country(String country) => _$this._country = country;

  double _lat;
  double get lat => _$this._lat;
  set lat(double lat) => _$this._lat = lat;

  double _long;
  double get long => _$this._long;
  set long(double long) => _$this._long = long;

  String _timezoneId;
  String get timezoneId => _$this._timezoneId;
  set timezoneId(String timezoneId) => _$this._timezoneId = timezoneId;

  LocationDetailModelBuilder();

  LocationDetailModelBuilder get _$this {
    if (_$v != null) {
      _city = _$v.city;
      _region = _$v.region;
      _woeid = _$v.woeid;
      _country = _$v.country;
      _lat = _$v.lat;
      _long = _$v.long;
      _timezoneId = _$v.timezoneId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LocationDetailModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LocationDetailModel;
  }

  @override
  void update(void Function(LocationDetailModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LocationDetailModel build() {
    final _$result = _$v ??
        new _$LocationDetailModel._(
            city: city,
            region: region,
            woeid: woeid,
            country: country,
            lat: lat,
            long: long,
            timezoneId: timezoneId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
