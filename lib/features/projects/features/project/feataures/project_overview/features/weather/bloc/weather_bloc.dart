import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/features/projects/features/add_project/models/location_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_response_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/repository/weather_repository.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc({@required this.weatherRepository, @required this.location})
      : super(WeatherInitial()) {
    add(WeatherRequested(location: location));
  }

  final WeatherRepository weatherRepository;
  final LocationModel location;

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is WeatherRequested) {
      yield* _mapWeatherRequestedToState(event.location);
    }
  }

  Stream<WeatherState> _mapWeatherRequestedToState(
      LocationModel location) async* {
    if (location == null) {
      yield WeatherLoadFailure();
    } else {
      try {
        yield WeatherLoadInProgress();
        WeatherResponseModel weatherResponseModel =
            await weatherRepository.getWeather(location);

        yield WeatherLoadSuccess(weather: weatherResponseModel.weather);
      } catch (e) {
        print(e);

        yield WeatherLoadFailure();
      }
    }
  }
}
