import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/forecast_model.dart';

class WeatherWidget extends StatelessWidget {
  final ForecastModel weatherForecastModel;

  WeatherWidget({@required this.weatherForecastModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          Text(
            weatherForecastModel.day,
            style: TextStyle(
                color: ColorConfig.primary,
                fontSize: 21,
                fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Text(
                '${weatherForecastModel.low}oC',
                style: TextStyle(
                    color: ColorConfig.primary,
                    fontSize: 15,
                    fontWeight: FontWeight.w400),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '${weatherForecastModel.high}oC',
                style: TextStyle(
                    color: ColorConfig.red4,
                    fontSize: 15,
                    fontWeight: FontWeight.w400),
              ),
            ],
          )
        ],
      ),
    );
  }
}
