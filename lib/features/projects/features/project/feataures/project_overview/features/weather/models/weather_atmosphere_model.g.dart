// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_atmosphere_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherAtmosphereModel> _$weatherAtmosphereModelSerializer =
    new _$WeatherAtmosphereModelSerializer();

class _$WeatherAtmosphereModelSerializer
    implements StructuredSerializer<WeatherAtmosphereModel> {
  @override
  final Iterable<Type> types = const [
    WeatherAtmosphereModel,
    _$WeatherAtmosphereModel
  ];
  @override
  final String wireName = 'WeatherAtmosphereModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, WeatherAtmosphereModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.humidity != null) {
      result
        ..add('humidity')
        ..add(serializers.serialize(object.humidity,
            specifiedType: const FullType(int)));
    }
    if (object.visibility != null) {
      result
        ..add('visibility')
        ..add(serializers.serialize(object.visibility,
            specifiedType: const FullType(double)));
    }
    if (object.pressure != null) {
      result
        ..add('pressure')
        ..add(serializers.serialize(object.pressure,
            specifiedType: const FullType(int)));
    }
    if (object.rising != null) {
      result
        ..add('rising')
        ..add(serializers.serialize(object.rising,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  WeatherAtmosphereModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherAtmosphereModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'humidity':
          result.humidity = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'visibility':
          result.visibility = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'pressure':
          result.pressure = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'rising':
          result.rising = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherAtmosphereModel extends WeatherAtmosphereModel {
  @override
  final int humidity;
  @override
  final double visibility;
  @override
  final int pressure;
  @override
  final int rising;

  factory _$WeatherAtmosphereModel(
          [void Function(WeatherAtmosphereModelBuilder) updates]) =>
      (new WeatherAtmosphereModelBuilder()..update(updates)).build();

  _$WeatherAtmosphereModel._(
      {this.humidity, this.visibility, this.pressure, this.rising})
      : super._();

  @override
  WeatherAtmosphereModel rebuild(
          void Function(WeatherAtmosphereModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherAtmosphereModelBuilder toBuilder() =>
      new WeatherAtmosphereModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherAtmosphereModel &&
        humidity == other.humidity &&
        visibility == other.visibility &&
        pressure == other.pressure &&
        rising == other.rising;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, humidity.hashCode), visibility.hashCode),
            pressure.hashCode),
        rising.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherAtmosphereModel')
          ..add('humidity', humidity)
          ..add('visibility', visibility)
          ..add('pressure', pressure)
          ..add('rising', rising))
        .toString();
  }
}

class WeatherAtmosphereModelBuilder
    implements Builder<WeatherAtmosphereModel, WeatherAtmosphereModelBuilder> {
  _$WeatherAtmosphereModel _$v;

  int _humidity;
  int get humidity => _$this._humidity;
  set humidity(int humidity) => _$this._humidity = humidity;

  double _visibility;
  double get visibility => _$this._visibility;
  set visibility(double visibility) => _$this._visibility = visibility;

  int _pressure;
  int get pressure => _$this._pressure;
  set pressure(int pressure) => _$this._pressure = pressure;

  int _rising;
  int get rising => _$this._rising;
  set rising(int rising) => _$this._rising = rising;

  WeatherAtmosphereModelBuilder();

  WeatherAtmosphereModelBuilder get _$this {
    if (_$v != null) {
      _humidity = _$v.humidity;
      _visibility = _$v.visibility;
      _pressure = _$v.pressure;
      _rising = _$v.rising;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherAtmosphereModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherAtmosphereModel;
  }

  @override
  void update(void Function(WeatherAtmosphereModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherAtmosphereModel build() {
    final _$result = _$v ??
        new _$WeatherAtmosphereModel._(
            humidity: humidity,
            visibility: visibility,
            pressure: pressure,
            rising: rising);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
