library weather_astronomy_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'weather_astronomy_model.g.dart';

abstract class WeatherAstronomyModel
    implements Built<WeatherAstronomyModel, WeatherAstronomyModelBuilder> {
  @nullable
  String get sunrise;

  @nullable
  String get sunset;

  WeatherAstronomyModel._();

  factory WeatherAstronomyModel([updates(WeatherAstronomyModelBuilder b)]) =
      _$WeatherAstronomyModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(WeatherAstronomyModel.serializer, this));
  }

  static WeatherAstronomyModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherAstronomyModel.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherAstronomyModel> get serializer =>
      _$weatherAstronomyModelSerializer;
}
