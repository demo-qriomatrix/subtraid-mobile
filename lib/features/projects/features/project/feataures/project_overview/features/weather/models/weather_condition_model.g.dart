// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_condition_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherConditionModel> _$weatherConditionModelSerializer =
    new _$WeatherConditionModelSerializer();

class _$WeatherConditionModelSerializer
    implements StructuredSerializer<WeatherConditionModel> {
  @override
  final Iterable<Type> types = const [
    WeatherConditionModel,
    _$WeatherConditionModel
  ];
  @override
  final String wireName = 'WeatherConditionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, WeatherConditionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.text != null) {
      result
        ..add('text')
        ..add(serializers.serialize(object.text,
            specifiedType: const FullType(String)));
    }
    if (object.code != null) {
      result
        ..add('code')
        ..add(serializers.serialize(object.code,
            specifiedType: const FullType(int)));
    }
    if (object.temperature != null) {
      result
        ..add('temperature')
        ..add(serializers.serialize(object.temperature,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  WeatherConditionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherConditionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'text':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'temperature':
          result.temperature = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherConditionModel extends WeatherConditionModel {
  @override
  final String text;
  @override
  final int code;
  @override
  final int temperature;

  factory _$WeatherConditionModel(
          [void Function(WeatherConditionModelBuilder) updates]) =>
      (new WeatherConditionModelBuilder()..update(updates)).build();

  _$WeatherConditionModel._({this.text, this.code, this.temperature})
      : super._();

  @override
  WeatherConditionModel rebuild(
          void Function(WeatherConditionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherConditionModelBuilder toBuilder() =>
      new WeatherConditionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherConditionModel &&
        text == other.text &&
        code == other.code &&
        temperature == other.temperature;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, text.hashCode), code.hashCode), temperature.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherConditionModel')
          ..add('text', text)
          ..add('code', code)
          ..add('temperature', temperature))
        .toString();
  }
}

class WeatherConditionModelBuilder
    implements Builder<WeatherConditionModel, WeatherConditionModelBuilder> {
  _$WeatherConditionModel _$v;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  int _code;
  int get code => _$this._code;
  set code(int code) => _$this._code = code;

  int _temperature;
  int get temperature => _$this._temperature;
  set temperature(int temperature) => _$this._temperature = temperature;

  WeatherConditionModelBuilder();

  WeatherConditionModelBuilder get _$this {
    if (_$v != null) {
      _text = _$v.text;
      _code = _$v.code;
      _temperature = _$v.temperature;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherConditionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherConditionModel;
  }

  @override
  void update(void Function(WeatherConditionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherConditionModel build() {
    final _$result = _$v ??
        new _$WeatherConditionModel._(
            text: text, code: code, temperature: temperature);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
