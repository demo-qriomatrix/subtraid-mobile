library forecast_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'forecast_model.g.dart';

abstract class ForecastModel
    implements Built<ForecastModel, ForecastModelBuilder> {
  @nullable
  String get day;

  @nullable
  int get date;

  @nullable
  int get low;

  @nullable
  int get high;

  @nullable
  String get text;

  @nullable
  int get code;

  ForecastModel._();

  factory ForecastModel([updates(ForecastModelBuilder b)]) = _$ForecastModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ForecastModel.serializer, this));
  }

  static ForecastModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ForecastModel.serializer, json.decode(jsonString));
  }

  static Serializer<ForecastModel> get serializer => _$forecastModelSerializer;
}
