// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherResponseModel> _$weatherResponseModelSerializer =
    new _$WeatherResponseModelSerializer();

class _$WeatherResponseModelSerializer
    implements StructuredSerializer<WeatherResponseModel> {
  @override
  final Iterable<Type> types = const [
    WeatherResponseModel,
    _$WeatherResponseModel
  ];
  @override
  final String wireName = 'WeatherResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, WeatherResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.weather != null) {
      result
        ..add('weather')
        ..add(serializers.serialize(object.weather,
            specifiedType: const FullType(WeatherModel)));
    }
    return result;
  }

  @override
  WeatherResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'weather':
          result.weather.replace(serializers.deserialize(value,
              specifiedType: const FullType(WeatherModel)) as WeatherModel);
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherResponseModel extends WeatherResponseModel {
  @override
  final WeatherModel weather;

  factory _$WeatherResponseModel(
          [void Function(WeatherResponseModelBuilder) updates]) =>
      (new WeatherResponseModelBuilder()..update(updates)).build();

  _$WeatherResponseModel._({this.weather}) : super._();

  @override
  WeatherResponseModel rebuild(
          void Function(WeatherResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherResponseModelBuilder toBuilder() =>
      new WeatherResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherResponseModel && weather == other.weather;
  }

  @override
  int get hashCode {
    return $jf($jc(0, weather.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherResponseModel')
          ..add('weather', weather))
        .toString();
  }
}

class WeatherResponseModelBuilder
    implements Builder<WeatherResponseModel, WeatherResponseModelBuilder> {
  _$WeatherResponseModel _$v;

  WeatherModelBuilder _weather;
  WeatherModelBuilder get weather =>
      _$this._weather ??= new WeatherModelBuilder();
  set weather(WeatherModelBuilder weather) => _$this._weather = weather;

  WeatherResponseModelBuilder();

  WeatherResponseModelBuilder get _$this {
    if (_$v != null) {
      _weather = _$v.weather?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherResponseModel;
  }

  @override
  void update(void Function(WeatherResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherResponseModel build() {
    _$WeatherResponseModel _$result;
    try {
      _$result =
          _$v ?? new _$WeatherResponseModel._(weather: _weather?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'weather';
        _weather?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
