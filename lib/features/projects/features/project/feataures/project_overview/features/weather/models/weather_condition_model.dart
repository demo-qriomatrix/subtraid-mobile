library weather_condition_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'weather_condition_model.g.dart';

abstract class WeatherConditionModel
    implements Built<WeatherConditionModel, WeatherConditionModelBuilder> {
  @nullable
  String get text;

  @nullable
  int get code;

  @nullable
  int get temperature;
  WeatherConditionModel._();

  factory WeatherConditionModel([updates(WeatherConditionModelBuilder b)]) =
      _$WeatherConditionModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(WeatherConditionModel.serializer, this));
  }

  static WeatherConditionModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherConditionModel.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherConditionModel> get serializer =>
      _$weatherConditionModelSerializer;
}
