// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_astronomy_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherAstronomyModel> _$weatherAstronomyModelSerializer =
    new _$WeatherAstronomyModelSerializer();

class _$WeatherAstronomyModelSerializer
    implements StructuredSerializer<WeatherAstronomyModel> {
  @override
  final Iterable<Type> types = const [
    WeatherAstronomyModel,
    _$WeatherAstronomyModel
  ];
  @override
  final String wireName = 'WeatherAstronomyModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, WeatherAstronomyModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.sunrise != null) {
      result
        ..add('sunrise')
        ..add(serializers.serialize(object.sunrise,
            specifiedType: const FullType(String)));
    }
    if (object.sunset != null) {
      result
        ..add('sunset')
        ..add(serializers.serialize(object.sunset,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  WeatherAstronomyModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherAstronomyModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'sunrise':
          result.sunrise = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'sunset':
          result.sunset = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherAstronomyModel extends WeatherAstronomyModel {
  @override
  final String sunrise;
  @override
  final String sunset;

  factory _$WeatherAstronomyModel(
          [void Function(WeatherAstronomyModelBuilder) updates]) =>
      (new WeatherAstronomyModelBuilder()..update(updates)).build();

  _$WeatherAstronomyModel._({this.sunrise, this.sunset}) : super._();

  @override
  WeatherAstronomyModel rebuild(
          void Function(WeatherAstronomyModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherAstronomyModelBuilder toBuilder() =>
      new WeatherAstronomyModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherAstronomyModel &&
        sunrise == other.sunrise &&
        sunset == other.sunset;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, sunrise.hashCode), sunset.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherAstronomyModel')
          ..add('sunrise', sunrise)
          ..add('sunset', sunset))
        .toString();
  }
}

class WeatherAstronomyModelBuilder
    implements Builder<WeatherAstronomyModel, WeatherAstronomyModelBuilder> {
  _$WeatherAstronomyModel _$v;

  String _sunrise;
  String get sunrise => _$this._sunrise;
  set sunrise(String sunrise) => _$this._sunrise = sunrise;

  String _sunset;
  String get sunset => _$this._sunset;
  set sunset(String sunset) => _$this._sunset = sunset;

  WeatherAstronomyModelBuilder();

  WeatherAstronomyModelBuilder get _$this {
    if (_$v != null) {
      _sunrise = _$v.sunrise;
      _sunset = _$v.sunset;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherAstronomyModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherAstronomyModel;
  }

  @override
  void update(void Function(WeatherAstronomyModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherAstronomyModel build() {
    final _$result =
        _$v ?? new _$WeatherAstronomyModel._(sunrise: sunrise, sunset: sunset);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
