library current_observation_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_astronomy_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_atmosphere_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_condition_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_wind_model.dart';

part 'current_observation_model.g.dart';

abstract class CurrentObservationModel
    implements Built<CurrentObservationModel, CurrentObservationModelBuilder> {
  @nullable
  WeatherWindModel get wind;

  @nullable
  WeatherAtmosphereModel get atmosphere;

  @nullable
  WeatherAstronomyModel get astronomy;

  @nullable
  WeatherConditionModel get condition;

  @nullable
  int get pubDate;

  CurrentObservationModel._();

  factory CurrentObservationModel([updates(CurrentObservationModelBuilder b)]) =
      _$CurrentObservationModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CurrentObservationModel.serializer, this));
  }

  static CurrentObservationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CurrentObservationModel.serializer, json.decode(jsonString));
  }

  static Serializer<CurrentObservationModel> get serializer =>
      _$currentObservationModelSerializer;
}
