// GENERATED CODE - DO NOT MODIFY BY HAND

part of current_observation_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CurrentObservationModel> _$currentObservationModelSerializer =
    new _$CurrentObservationModelSerializer();

class _$CurrentObservationModelSerializer
    implements StructuredSerializer<CurrentObservationModel> {
  @override
  final Iterable<Type> types = const [
    CurrentObservationModel,
    _$CurrentObservationModel
  ];
  @override
  final String wireName = 'CurrentObservationModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CurrentObservationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.wind != null) {
      result
        ..add('wind')
        ..add(serializers.serialize(object.wind,
            specifiedType: const FullType(WeatherWindModel)));
    }
    if (object.atmosphere != null) {
      result
        ..add('atmosphere')
        ..add(serializers.serialize(object.atmosphere,
            specifiedType: const FullType(WeatherAtmosphereModel)));
    }
    if (object.astronomy != null) {
      result
        ..add('astronomy')
        ..add(serializers.serialize(object.astronomy,
            specifiedType: const FullType(WeatherAstronomyModel)));
    }
    if (object.condition != null) {
      result
        ..add('condition')
        ..add(serializers.serialize(object.condition,
            specifiedType: const FullType(WeatherConditionModel)));
    }
    if (object.pubDate != null) {
      result
        ..add('pubDate')
        ..add(serializers.serialize(object.pubDate,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  CurrentObservationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CurrentObservationModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'wind':
          result.wind.replace(serializers.deserialize(value,
                  specifiedType: const FullType(WeatherWindModel))
              as WeatherWindModel);
          break;
        case 'atmosphere':
          result.atmosphere.replace(serializers.deserialize(value,
                  specifiedType: const FullType(WeatherAtmosphereModel))
              as WeatherAtmosphereModel);
          break;
        case 'astronomy':
          result.astronomy.replace(serializers.deserialize(value,
                  specifiedType: const FullType(WeatherAstronomyModel))
              as WeatherAstronomyModel);
          break;
        case 'condition':
          result.condition.replace(serializers.deserialize(value,
                  specifiedType: const FullType(WeatherConditionModel))
              as WeatherConditionModel);
          break;
        case 'pubDate':
          result.pubDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$CurrentObservationModel extends CurrentObservationModel {
  @override
  final WeatherWindModel wind;
  @override
  final WeatherAtmosphereModel atmosphere;
  @override
  final WeatherAstronomyModel astronomy;
  @override
  final WeatherConditionModel condition;
  @override
  final int pubDate;

  factory _$CurrentObservationModel(
          [void Function(CurrentObservationModelBuilder) updates]) =>
      (new CurrentObservationModelBuilder()..update(updates)).build();

  _$CurrentObservationModel._(
      {this.wind,
      this.atmosphere,
      this.astronomy,
      this.condition,
      this.pubDate})
      : super._();

  @override
  CurrentObservationModel rebuild(
          void Function(CurrentObservationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CurrentObservationModelBuilder toBuilder() =>
      new CurrentObservationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CurrentObservationModel &&
        wind == other.wind &&
        atmosphere == other.atmosphere &&
        astronomy == other.astronomy &&
        condition == other.condition &&
        pubDate == other.pubDate;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, wind.hashCode), atmosphere.hashCode),
                astronomy.hashCode),
            condition.hashCode),
        pubDate.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CurrentObservationModel')
          ..add('wind', wind)
          ..add('atmosphere', atmosphere)
          ..add('astronomy', astronomy)
          ..add('condition', condition)
          ..add('pubDate', pubDate))
        .toString();
  }
}

class CurrentObservationModelBuilder
    implements
        Builder<CurrentObservationModel, CurrentObservationModelBuilder> {
  _$CurrentObservationModel _$v;

  WeatherWindModelBuilder _wind;
  WeatherWindModelBuilder get wind =>
      _$this._wind ??= new WeatherWindModelBuilder();
  set wind(WeatherWindModelBuilder wind) => _$this._wind = wind;

  WeatherAtmosphereModelBuilder _atmosphere;
  WeatherAtmosphereModelBuilder get atmosphere =>
      _$this._atmosphere ??= new WeatherAtmosphereModelBuilder();
  set atmosphere(WeatherAtmosphereModelBuilder atmosphere) =>
      _$this._atmosphere = atmosphere;

  WeatherAstronomyModelBuilder _astronomy;
  WeatherAstronomyModelBuilder get astronomy =>
      _$this._astronomy ??= new WeatherAstronomyModelBuilder();
  set astronomy(WeatherAstronomyModelBuilder astronomy) =>
      _$this._astronomy = astronomy;

  WeatherConditionModelBuilder _condition;
  WeatherConditionModelBuilder get condition =>
      _$this._condition ??= new WeatherConditionModelBuilder();
  set condition(WeatherConditionModelBuilder condition) =>
      _$this._condition = condition;

  int _pubDate;
  int get pubDate => _$this._pubDate;
  set pubDate(int pubDate) => _$this._pubDate = pubDate;

  CurrentObservationModelBuilder();

  CurrentObservationModelBuilder get _$this {
    if (_$v != null) {
      _wind = _$v.wind?.toBuilder();
      _atmosphere = _$v.atmosphere?.toBuilder();
      _astronomy = _$v.astronomy?.toBuilder();
      _condition = _$v.condition?.toBuilder();
      _pubDate = _$v.pubDate;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CurrentObservationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CurrentObservationModel;
  }

  @override
  void update(void Function(CurrentObservationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CurrentObservationModel build() {
    _$CurrentObservationModel _$result;
    try {
      _$result = _$v ??
          new _$CurrentObservationModel._(
              wind: _wind?.build(),
              atmosphere: _atmosphere?.build(),
              astronomy: _astronomy?.build(),
              condition: _condition?.build(),
              pubDate: pubDate);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'wind';
        _wind?.build();
        _$failedField = 'atmosphere';
        _atmosphere?.build();
        _$failedField = 'astronomy';
        _astronomy?.build();
        _$failedField = 'condition';
        _condition?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CurrentObservationModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
