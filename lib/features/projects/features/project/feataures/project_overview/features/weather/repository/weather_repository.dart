import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/add_project/models/location_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/models/weather_response_model.dart';

class WeatherRepository {
  final HttpClient httpClient;

  WeatherRepository({@required this.httpClient});

  Future<WeatherResponseModel> getWeather(LocationModel location) async {
    Map<String, double> queryParameters = {
      'lat': location.lat,
      'lng': location.lng,
    };
    Response response = await httpClient.dio
        .get(EndpointConfig.weather, queryParameters: queryParameters);

    return WeatherResponseModel.fromJson(json.encode(response.data));
  }
}
