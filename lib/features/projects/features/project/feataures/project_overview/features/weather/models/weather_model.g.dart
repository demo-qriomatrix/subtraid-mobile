// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherModel> _$weatherModelSerializer =
    new _$WeatherModelSerializer();

class _$WeatherModelSerializer implements StructuredSerializer<WeatherModel> {
  @override
  final Iterable<Type> types = const [WeatherModel, _$WeatherModel];
  @override
  final String wireName = 'WeatherModel';

  @override
  Iterable<Object> serialize(Serializers serializers, WeatherModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(LocationDetailModel)));
    }
    if (object.currentObservation != null) {
      result
        ..add('current_observation')
        ..add(serializers.serialize(object.currentObservation,
            specifiedType: const FullType(CurrentObservationModel)));
    }
    if (object.forecasts != null) {
      result
        ..add('forecasts')
        ..add(serializers.serialize(object.forecasts,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ForecastModel)])));
    }
    return result;
  }

  @override
  WeatherModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'location':
          result.location.replace(serializers.deserialize(value,
                  specifiedType: const FullType(LocationDetailModel))
              as LocationDetailModel);
          break;
        case 'current_observation':
          result.currentObservation.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CurrentObservationModel))
              as CurrentObservationModel);
          break;
        case 'forecasts':
          result.forecasts.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ForecastModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherModel extends WeatherModel {
  @override
  final LocationDetailModel location;
  @override
  final CurrentObservationModel currentObservation;
  @override
  final BuiltList<ForecastModel> forecasts;

  factory _$WeatherModel([void Function(WeatherModelBuilder) updates]) =>
      (new WeatherModelBuilder()..update(updates)).build();

  _$WeatherModel._({this.location, this.currentObservation, this.forecasts})
      : super._();

  @override
  WeatherModel rebuild(void Function(WeatherModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherModelBuilder toBuilder() => new WeatherModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherModel &&
        location == other.location &&
        currentObservation == other.currentObservation &&
        forecasts == other.forecasts;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, location.hashCode), currentObservation.hashCode),
        forecasts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherModel')
          ..add('location', location)
          ..add('currentObservation', currentObservation)
          ..add('forecasts', forecasts))
        .toString();
  }
}

class WeatherModelBuilder
    implements Builder<WeatherModel, WeatherModelBuilder> {
  _$WeatherModel _$v;

  LocationDetailModelBuilder _location;
  LocationDetailModelBuilder get location =>
      _$this._location ??= new LocationDetailModelBuilder();
  set location(LocationDetailModelBuilder location) =>
      _$this._location = location;

  CurrentObservationModelBuilder _currentObservation;
  CurrentObservationModelBuilder get currentObservation =>
      _$this._currentObservation ??= new CurrentObservationModelBuilder();
  set currentObservation(CurrentObservationModelBuilder currentObservation) =>
      _$this._currentObservation = currentObservation;

  ListBuilder<ForecastModel> _forecasts;
  ListBuilder<ForecastModel> get forecasts =>
      _$this._forecasts ??= new ListBuilder<ForecastModel>();
  set forecasts(ListBuilder<ForecastModel> forecasts) =>
      _$this._forecasts = forecasts;

  WeatherModelBuilder();

  WeatherModelBuilder get _$this {
    if (_$v != null) {
      _location = _$v.location?.toBuilder();
      _currentObservation = _$v.currentObservation?.toBuilder();
      _forecasts = _$v.forecasts?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherModel;
  }

  @override
  void update(void Function(WeatherModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherModel build() {
    _$WeatherModel _$result;
    try {
      _$result = _$v ??
          new _$WeatherModel._(
              location: _location?.build(),
              currentObservation: _currentObservation?.build(),
              forecasts: _forecasts?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'location';
        _location?.build();
        _$failedField = 'currentObservation';
        _currentObservation?.build();
        _$failedField = 'forecasts';
        _forecasts?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
