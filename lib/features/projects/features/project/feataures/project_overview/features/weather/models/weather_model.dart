library weather_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'current_observation_model.dart';
import 'forecast_model.dart';
import 'location_detail_model.dart';

part 'weather_model.g.dart';

abstract class WeatherModel
    implements Built<WeatherModel, WeatherModelBuilder> {
  @nullable
  LocationDetailModel get location;

  @nullable
  @BuiltValueField(wireName: 'current_observation')
  CurrentObservationModel get currentObservation;

  @nullable
  BuiltList<ForecastModel> get forecasts;

  WeatherModel._();

  factory WeatherModel([updates(WeatherModelBuilder b)]) = _$WeatherModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(WeatherModel.serializer, this));
  }

  static WeatherModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherModel.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherModel> get serializer => _$weatherModelSerializer;
}
