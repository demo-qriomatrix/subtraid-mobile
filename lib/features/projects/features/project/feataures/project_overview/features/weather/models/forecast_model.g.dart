// GENERATED CODE - DO NOT MODIFY BY HAND

part of forecast_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ForecastModel> _$forecastModelSerializer =
    new _$ForecastModelSerializer();

class _$ForecastModelSerializer implements StructuredSerializer<ForecastModel> {
  @override
  final Iterable<Type> types = const [ForecastModel, _$ForecastModel];
  @override
  final String wireName = 'ForecastModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ForecastModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.day != null) {
      result
        ..add('day')
        ..add(serializers.serialize(object.day,
            specifiedType: const FullType(String)));
    }
    if (object.date != null) {
      result
        ..add('date')
        ..add(serializers.serialize(object.date,
            specifiedType: const FullType(int)));
    }
    if (object.low != null) {
      result
        ..add('low')
        ..add(serializers.serialize(object.low,
            specifiedType: const FullType(int)));
    }
    if (object.high != null) {
      result
        ..add('high')
        ..add(serializers.serialize(object.high,
            specifiedType: const FullType(int)));
    }
    if (object.text != null) {
      result
        ..add('text')
        ..add(serializers.serialize(object.text,
            specifiedType: const FullType(String)));
    }
    if (object.code != null) {
      result
        ..add('code')
        ..add(serializers.serialize(object.code,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ForecastModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ForecastModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'day':
          result.day = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'low':
          result.low = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'high':
          result.high = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'text':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ForecastModel extends ForecastModel {
  @override
  final String day;
  @override
  final int date;
  @override
  final int low;
  @override
  final int high;
  @override
  final String text;
  @override
  final int code;

  factory _$ForecastModel([void Function(ForecastModelBuilder) updates]) =>
      (new ForecastModelBuilder()..update(updates)).build();

  _$ForecastModel._(
      {this.day, this.date, this.low, this.high, this.text, this.code})
      : super._();

  @override
  ForecastModel rebuild(void Function(ForecastModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ForecastModelBuilder toBuilder() => new ForecastModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ForecastModel &&
        day == other.day &&
        date == other.date &&
        low == other.low &&
        high == other.high &&
        text == other.text &&
        code == other.code;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, day.hashCode), date.hashCode), low.hashCode),
                high.hashCode),
            text.hashCode),
        code.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ForecastModel')
          ..add('day', day)
          ..add('date', date)
          ..add('low', low)
          ..add('high', high)
          ..add('text', text)
          ..add('code', code))
        .toString();
  }
}

class ForecastModelBuilder
    implements Builder<ForecastModel, ForecastModelBuilder> {
  _$ForecastModel _$v;

  String _day;
  String get day => _$this._day;
  set day(String day) => _$this._day = day;

  int _date;
  int get date => _$this._date;
  set date(int date) => _$this._date = date;

  int _low;
  int get low => _$this._low;
  set low(int low) => _$this._low = low;

  int _high;
  int get high => _$this._high;
  set high(int high) => _$this._high = high;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  int _code;
  int get code => _$this._code;
  set code(int code) => _$this._code = code;

  ForecastModelBuilder();

  ForecastModelBuilder get _$this {
    if (_$v != null) {
      _day = _$v.day;
      _date = _$v.date;
      _low = _$v.low;
      _high = _$v.high;
      _text = _$v.text;
      _code = _$v.code;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ForecastModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ForecastModel;
  }

  @override
  void update(void Function(ForecastModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ForecastModel build() {
    final _$result = _$v ??
        new _$ForecastModel._(
            day: day, date: date, low: low, high: high, text: text, code: code);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
