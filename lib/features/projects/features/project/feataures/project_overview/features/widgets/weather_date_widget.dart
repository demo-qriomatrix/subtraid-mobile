import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:Subtraid/core/config/color_config.dart';

class WeatherDateTimeWidget extends StatefulWidget {
  const WeatherDateTimeWidget({
    Key key,
  }) : super(key: key);

  @override
  _WeatherDateTimeWidgetState createState() => _WeatherDateTimeWidgetState();
}

class _WeatherDateTimeWidgetState extends State<WeatherDateTimeWidget> {
  Timer timer;
  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  _getTime() {
    setState(() {});
  }

  @override
  void dispose() {
    if (timer != null) {
      timer.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(DateFormat('EEEE, hh:mm:ss').format(DateTime.now()),
            style: TextStyle(
              color: ColorConfig.primary,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            )),
        Text(DateFormat('MMMM dd  yyyy').format(DateTime.now()),
            style: TextStyle(
              color: ColorConfig.primary,
              fontSize: 20,
              fontWeight: FontWeight.w500,
            )),
      ],
    );
  }
}
