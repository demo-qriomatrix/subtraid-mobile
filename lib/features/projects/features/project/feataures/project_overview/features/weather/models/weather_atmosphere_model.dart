library weather_atmosphere_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'weather_atmosphere_model.g.dart';

abstract class WeatherAtmosphereModel
    implements Built<WeatherAtmosphereModel, WeatherAtmosphereModelBuilder> {
  @nullable
  int get humidity;

  @nullable
  double get visibility;

  @nullable
  int get pressure;

  @nullable
  int get rising;

  WeatherAtmosphereModel._();

  factory WeatherAtmosphereModel([updates(WeatherAtmosphereModelBuilder b)]) =
      _$WeatherAtmosphereModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(WeatherAtmosphereModel.serializer, this));
  }

  static WeatherAtmosphereModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherAtmosphereModel.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherAtmosphereModel> get serializer =>
      _$weatherAtmosphereModelSerializer;
}
