library weather_wind_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'weather_wind_model.g.dart';

abstract class WeatherWindModel
    implements Built<WeatherWindModel, WeatherWindModelBuilder> {
  @nullable
  int get chill;

  @nullable
  int get direction;

  @nullable
  int get speed;

  WeatherWindModel._();

  factory WeatherWindModel([updates(WeatherWindModelBuilder b)]) =
      _$WeatherWindModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(WeatherWindModel.serializer, this));
  }

  static WeatherWindModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherWindModel.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherWindModel> get serializer =>
      _$weatherWindModelSerializer;
}
