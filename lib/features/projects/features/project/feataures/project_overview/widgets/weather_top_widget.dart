import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/widgets/weather_date_widget.dart';
import 'package:flutter/material.dart';

class WeatherTopWidget extends StatelessWidget {
  const WeatherTopWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 12,
                color: ColorConfig.shadow2,
                spreadRadius: 1,
                offset: Offset(0, 3))
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ImageIcon(
              AssetImage(LocalImages.calenderAlt2),
              color: ColorConfig.primary,
              size: 32,
            ),
            WeatherDateTimeWidget()
          ],
        ),
      ),
    );
  }
}
