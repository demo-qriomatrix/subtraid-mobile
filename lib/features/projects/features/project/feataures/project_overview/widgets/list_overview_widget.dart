import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_item_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/scrum_board_cards/widgets/card_checklist_item_widget.dart';
import 'package:Subtraid/features/projects/features/project/models/project_card_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_list_model.dart';
import 'package:Subtraid/features/projects/features/project_list/view/project_list_screen.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';

class ListOverviewWidget extends StatelessWidget {
  final ProjectListModel list;
  final Map<String, ProjectCardModel> cards;
  ListOverviewWidget({@required this.list, @required this.cards});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Container(
            margin: EdgeInsets.only(bottom: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    blurRadius: 12,
                    color: ColorConfig.shadow2,
                    spreadRadius: 1,
                    offset: Offset(0, 3))
              ],
            ),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.end,
                    //   children: <Widget>[
                    //     Text(
                    //       'Hold 02',
                    //       style: TextStyle(
                    //           fontSize: 16,
                    //           fontWeight: FontWeight.w400,
                    //           color: ColorConfig.red4),
                    //     )
                    //   ],
                    // ),
                    Text(
                      list.name,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: ColorConfig.primary),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: [
                            Text(
                              list.idCards.length.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 24,
                                  color: ColorConfig.orange),
                            ),
                            Text(
                              'Cards',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18,
                                  color: ColorConfig.primary),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          children: [
                            Text(
                              list.idProjects.length.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                  color: ColorConfig.orange),
                            ),
                            Text(
                              'Sub projects',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14,
                                  color: ColorConfig.primary),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              calcCompleted(list.idCards
                                      .map((card) => cards[card])
                                      .toList())
                                  .toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                  color: ColorConfig.black2),
                            ),
                            Text(
                              'Completed',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14,
                                  color: ColorConfig.black2),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ChecklistItem(
                      cardChecklistModel: CardChecklistModel(
                        (cardChecklistModel) => cardChecklistModel
                          ..name = 'Task Summery'
                          ..checkItems = ListBuilder(List.generate(
                              calcCheckItems(list.idCards
                                  .map((card) => cards[card])
                                  .toList()),
                              (index) => CardChecklistItemModel()))
                          ..checkItemsChecked = calcCheckedItems(
                              list.idCards.map((card) => cards[card]).toList()),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ProgressIndicatorWidget(
                      progress: ((calcCheckedItems(list.idCards
                                      .map((card) => cards[card])
                                      .toList()) ==
                                  0) &&
                              (calcCheckItems(list.idCards
                                      .map((card) => cards[card])
                                      .toList()) ==
                                  0))
                          ? 0
                          : (((calcCheckedItems(list.idCards
                                      .map((card) => cards[card])
                                      .toList())) /
                                  (calcCheckItems(list.idCards
                                      .map((card) => cards[card])
                                      .toList()))) *
                              100),
                      total: ((calcCheckedItems(list.idCards
                                      .map((card) => cards[card])
                                      .toList()) ==
                                  0) &&
                              (calcCheckItems(list.idCards
                                      .map((card) => cards[card])
                                      .toList()) ==
                                  0))
                          ? 100
                          : (100 -
                              (((calcCheckedItems(list.idCards
                                          .map((card) => cards[card])
                                          .toList())) /
                                      (calcCheckItems(list.idCards
                                          .map((card) => cards[card])
                                          .toList()))) *
                                  100)),
                    )
                  ],
                ))));
  }
}

int calcCompleted(List<ProjectCardModel> cards) {
  return cards
      .where((element) => element.checkItems == element.checkItemsChecked)
      .length;
}

int calcCheckItems(List<ProjectCardModel> cards) {
  List<ProjectCardModel> filteredCards = cards
      .where((element) => element != null && element.checkItems != null)
      .toList();

  if (filteredCards.isEmpty) return 0;

  return filteredCards
      .map((e) => e.checkItems)
      .reduce((value, element) => value + element);
}

int calcCheckedItems(List<ProjectCardModel> cards) {
  List<ProjectCardModel> filteredCards = cards
      .where((element) => element != null && element.checkItemsChecked != null)
      .toList();

  if (filteredCards.isEmpty) return 0;

  return filteredCards
      .map((e) => e.checkItemsChecked)
      .reduce((value, element) => value + element);
}
