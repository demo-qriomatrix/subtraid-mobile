import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/bloc/weather_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/widgets/weather.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherDetailsWidget extends StatelessWidget {
  const WeatherDetailsWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80,
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 3,
                color: ColorConfig.shadow2,
                spreadRadius: 1,
                offset: Offset(0, 3))
          ],
        ),
        child: BlocBuilder<WeatherBloc, WeatherState>(
          builder: (context, state) {
            if (state is WeatherLoadInProgress) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[CircularProgressIndicator()],
              );
            }

            if (state is WeatherLoadFailure) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Location data not found!',
                    style: TextStyle(fontSize: 16),
                  )
                ],
              );
            }
            if (state is WeatherLoadSuccess) {
              return ListView(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.zero,
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.wb_incandescent,
                            color: ColorConfig.primary,
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            '${state.weather.currentObservation?.wind?.speed ?? ''}KPH',
                            style: TextStyle(
                                color: ColorConfig.primary,
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.wb_incandescent,
                            color: ColorConfig.primary,
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            '${state.weather.currentObservation?.wind?.direction ?? ''}',
                            style: TextStyle(
                                color: ColorConfig.primary,
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 1,
                        height: 40,
                        color: Colors.black,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  state.weather.forecasts != null &&
                          state.weather.forecasts.isNotEmpty
                      ? Row(
                          children: state.weather.forecasts
                              .map((forecast) => WeatherWidget(
                                    weatherForecastModel: forecast,
                                  ))
                              .toList(),
                        )
                      : Container()
                ],
              );
            }

            return Container();
          },
        ));
  }
}
