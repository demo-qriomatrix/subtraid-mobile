import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/models/project_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_response_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProjectDetailsWidget extends StatefulWidget {
  final ProjectLoadSuccess state;

  ProjectDetailsWidget({@required this.state});

  @override
  _ProjectDetailsWidgetState createState() => _ProjectDetailsWidgetState();
}

class _ProjectDetailsWidgetState extends State<ProjectDetailsWidget> {
  bool editMode = false;

  TextEditingController projectName = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController startDate = TextEditingController();

  @override
  void initState() {
    super.initState();

    projectName.text = widget.state.projectModel?.name ?? '';
    address.text = widget.state.projectModel?.formattedAddress ?? '';
    startDate.text = widget.state.projectModel?.startDate != null
        ? DateFormat('dd MMM yyyy')
            .format(DateTime.parse(widget.state.projectModel.startDate))
        : '';
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                blurRadius: 12,
                color: ColorConfig.shadow2,
                spreadRadius: 1,
                offset: Offset(0, 3))
          ],
        ),
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: editMode
                        ? TextFormField(controller: projectName)
                        : Text(
                            widget.state.projectModel.name == '' ||
                                    widget.state.projectModel.name == null
                                ? " - "
                                : widget.state.projectModel.name,
                            style: TextStyle(
                                color: ColorConfig.primary,
                                fontSize: 20,
                                fontWeight: FontWeight.w500),
                          ),
                  ),
                  editMode
                      ? Container()
                      : RawMaterialButton(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          padding: EdgeInsets.all(10),
                          constraints: BoxConstraints(),
                          shape: CircleBorder(),
                          child: ImageIcon(
                            AssetImage(LocalImages.edit),
                            color: ColorConfig.primary,
                            size: 16,
                          ),
                          onPressed: () {
                            setState(() {
                              editMode = true;
                            });
                          },
                        )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: <Widget>[
                  ImageIcon(
                    AssetImage(LocalImages.mapMakerAlt),
                    color: ColorConfig.primary,
                    size: 16,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Flexible(
                    child: editMode
                        ? TextFormField(controller: address)
                        : Text(
                            widget.state.projectModel.formattedAddress == "" ||
                                    widget.state.projectModel
                                            .formattedAddress ==
                                        null
                                ? " - "
                                : widget.state.projectModel.formattedAddress,
                            style: TextStyle(
                                color: ColorConfig.grey1, fontSize: 16),
                          ),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: <Widget>[
                  ImageIcon(
                    AssetImage(LocalImages.calenderAlt),
                    color: ColorConfig.primary,
                    size: 16,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Flexible(
                    child: editMode
                        ? TextFormField(
                            onTap: () {
                              showDatePicker(
                                context: context,
                                firstDate: DateTime.now()
                                    .subtract(Duration(days: 3650)),
                                initialDate: DateTime.now(),
                                lastDate:
                                    DateTime.now().add(Duration(days: 3650)),
                              ).then((value) {
                                if (value != null) {
                                  startDate.text =
                                      DateFormat('dd MMM yyyy').format(value);
                                }
                              });
                            },
                            readOnly: true,
                            controller: startDate)
                        : Text(
                            widget.state.projectModel.startDate == "" ||
                                    widget.state.projectModel.startDate == null
                                ? " - "
                                : DateFormat('dd MMM yyyy').format(
                                    DateTime.parse(
                                        widget.state.projectModel.startDate)),
                            style: TextStyle(
                                color: ColorConfig.grey1, fontSize: 16),
                          ),
                  )
                ],
              ),
              editMode
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          RawMaterialButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            padding: EdgeInsets.all(10),
                            constraints: BoxConstraints(),
                            shape: CircleBorder(),
                            child: Icon(
                              Icons.close,
                              color: ColorConfig.primary,
                            ),
                            onPressed: () {
                              setState(() {
                                editMode = false;
                              });
                            },
                          ),
                          RawMaterialButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            padding: EdgeInsets.all(10),
                            constraints: BoxConstraints(),
                            shape: CircleBorder(),
                            child: Icon(
                              Icons.done,
                              color: ColorConfig.primary,
                            ),
                            onPressed: () {
                              setState(() {
                                editMode = false;
                              });

                              ProjectModel project = widget.state.projectModel;

                              project = project.rebuild((data) => data
                                ..name = projectName.text
                                ..formattedAddress = address.text
                                ..startDate = DateFormat("dd MMM yyyy")
                                    .parse(startDate.text)
                                    .toLocal()
                                    .toIso8601String());

                              ProjectResponseModel projectResponseModel =
                                  ProjectResponseModel((data) =>
                                      data..board = project.toBuilder());

                              context.read<ProjectBloc>().add(
                                  ProjectUpdate(project: projectResponseModel));
                            },
                          ),
                        ],
                      ),
                    )
                  : Container()
            ])),
      ),
    );
  }
}
