import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/features/weather/bloc/weather_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/widgets/list_overview_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/widgets/project_details_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/widgets/weather_details_widget.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/widgets/weather_top_widget.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProjectOverviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      child: Column(
        children: [
          // StAppBar(
          //   title: 'Project Overview',
          // ),
          Expanded(
              child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: ConstrainedBox(
                    constraints:
                        BoxConstraints(minHeight: getMinHeight(context) + 30),
                    child: BlocBuilder<ProjectBloc, ProjectState>(
                      builder: (context, state) {
                        if (state is ProjectLoadSuccess) {
                          return BlocProvider<WeatherBloc>(
                            lazy: false,
                            create: (context) => WeatherBloc(
                                weatherRepository: sl(),
                                location: state.projectModel.getLocation),
                            child: Column(
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    ClipRRect(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(50),
                                            topRight: Radius.circular(50)),
                                        child: Image.asset(
                                          LocalImages.map,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.2,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          fit: BoxFit.cover,
                                        )),
                                    Column(
                                      children: <Widget>[
                                        SizedBox(
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.2 -
                                              60,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(50),
                                                  topRight:
                                                      Radius.circular(50))),
                                          child: Column(
                                            children: <Widget>[
                                              WeatherTopWidget(),
                                              WeatherDetailsWidget(),
                                              SizedBox(height: 20),
                                              ProjectDetailsWidget(
                                                state: state,
                                              ),
                                              SizedBox(height: 20),
                                              Column(
                                                children:
                                                    state.projectModel.lists
                                                        .map(
                                                          (list) =>
                                                              ListOverviewWidget(
                                                            cards: state.cards,
                                                            list: list,
                                                          ),
                                                        )
                                                        .toList(),
                                              ),
                                              SizedBox(height: 40),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          );
                        }

                        return Container();
                      },
                    )),
              )
            ],
          ))
        ],
      ),
    );
  }
}
