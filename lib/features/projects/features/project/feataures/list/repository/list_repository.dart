import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/add_list_post_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/list/model/remove_list_post_model.dart';

class ListRepository {
  final HttpClient httpClient;

  ListRepository({@required this.httpClient});

  Future<Response> addListToProject(AddListPostModel list) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectList + '/add', data: list.toJson());
  }

  Future<Response> removeListFromProject(RemoveListPostModel list) async {
    return await httpClient.dio
        .patch(EndpointConfig.projectList + '/remove', data: list.toJson());
  }
}
