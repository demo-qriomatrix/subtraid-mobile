// GENERATED CODE - DO NOT MODIFY BY HAND

part of remove_list_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RemoveListPostModel> _$removeListPostModelSerializer =
    new _$RemoveListPostModelSerializer();

class _$RemoveListPostModelSerializer
    implements StructuredSerializer<RemoveListPostModel> {
  @override
  final Iterable<Type> types = const [
    RemoveListPostModel,
    _$RemoveListPostModel
  ];
  @override
  final String wireName = 'RemoveListPostModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, RemoveListPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.listId != null) {
      result
        ..add('listId')
        ..add(serializers.serialize(object.listId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RemoveListPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RemoveListPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'listId':
          result.listId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RemoveListPostModel extends RemoveListPostModel {
  @override
  final String projectId;
  @override
  final String listId;

  factory _$RemoveListPostModel(
          [void Function(RemoveListPostModelBuilder) updates]) =>
      (new RemoveListPostModelBuilder()..update(updates)).build();

  _$RemoveListPostModel._({this.projectId, this.listId}) : super._();

  @override
  RemoveListPostModel rebuild(
          void Function(RemoveListPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RemoveListPostModelBuilder toBuilder() =>
      new RemoveListPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RemoveListPostModel &&
        projectId == other.projectId &&
        listId == other.listId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, projectId.hashCode), listId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RemoveListPostModel')
          ..add('projectId', projectId)
          ..add('listId', listId))
        .toString();
  }
}

class RemoveListPostModelBuilder
    implements Builder<RemoveListPostModel, RemoveListPostModelBuilder> {
  _$RemoveListPostModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _listId;
  String get listId => _$this._listId;
  set listId(String listId) => _$this._listId = listId;

  RemoveListPostModelBuilder();

  RemoveListPostModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _listId = _$v.listId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RemoveListPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RemoveListPostModel;
  }

  @override
  void update(void Function(RemoveListPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RemoveListPostModel build() {
    final _$result = _$v ??
        new _$RemoveListPostModel._(projectId: projectId, listId: listId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
