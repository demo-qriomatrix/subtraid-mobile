library add_list_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'add_list_post_model.g.dart';

abstract class AddListPostModel
    implements Built<AddListPostModel, AddListPostModelBuilder> {
  @nullable
  String get projectId;

  @nullable
  String get name;

  AddListPostModel._();

  factory AddListPostModel([updates(AddListPostModelBuilder b)]) =
      _$AddListPostModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(AddListPostModel.serializer, this));
  }

  static AddListPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AddListPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<AddListPostModel> get serializer =>
      _$addListPostModelSerializer;
}
