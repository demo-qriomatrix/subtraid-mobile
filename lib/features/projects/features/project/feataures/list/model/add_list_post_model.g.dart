// GENERATED CODE - DO NOT MODIFY BY HAND

part of add_list_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AddListPostModel> _$addListPostModelSerializer =
    new _$AddListPostModelSerializer();

class _$AddListPostModelSerializer
    implements StructuredSerializer<AddListPostModel> {
  @override
  final Iterable<Type> types = const [AddListPostModel, _$AddListPostModel];
  @override
  final String wireName = 'AddListPostModel';

  @override
  Iterable<Object> serialize(Serializers serializers, AddListPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectId != null) {
      result
        ..add('projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AddListPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddListPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AddListPostModel extends AddListPostModel {
  @override
  final String projectId;
  @override
  final String name;

  factory _$AddListPostModel(
          [void Function(AddListPostModelBuilder) updates]) =>
      (new AddListPostModelBuilder()..update(updates)).build();

  _$AddListPostModel._({this.projectId, this.name}) : super._();

  @override
  AddListPostModel rebuild(void Function(AddListPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddListPostModelBuilder toBuilder() =>
      new AddListPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddListPostModel &&
        projectId == other.projectId &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, projectId.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddListPostModel')
          ..add('projectId', projectId)
          ..add('name', name))
        .toString();
  }
}

class AddListPostModelBuilder
    implements Builder<AddListPostModel, AddListPostModelBuilder> {
  _$AddListPostModel _$v;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  AddListPostModelBuilder();

  AddListPostModelBuilder get _$this {
    if (_$v != null) {
      _projectId = _$v.projectId;
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddListPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AddListPostModel;
  }

  @override
  void update(void Function(AddListPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddListPostModel build() {
    final _$result =
        _$v ?? new _$AddListPostModel._(projectId: projectId, name: name);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
