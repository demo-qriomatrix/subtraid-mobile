library remove_list_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'remove_list_post_model.g.dart';

abstract class RemoveListPostModel
    implements Built<RemoveListPostModel, RemoveListPostModelBuilder> {
  @nullable
  String get projectId;

  @nullable
  String get listId;

  RemoveListPostModel._();

  factory RemoveListPostModel([updates(RemoveListPostModelBuilder b)]) =
      _$RemoveListPostModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(RemoveListPostModel.serializer, this));
  }

  static RemoveListPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RemoveListPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<RemoveListPostModel> get serializer =>
      _$removeListPostModelSerializer;
}
