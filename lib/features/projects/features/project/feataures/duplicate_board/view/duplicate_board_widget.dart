import 'package:Subtraid/core/config/color_config.dart';
import 'package:flutter/material.dart';

class DuplicateProjectScreen extends StatefulWidget {
  const DuplicateProjectScreen({
    Key key,
  }) : super(key: key);

  @override
  _DuplicateProjectScreenState createState() => _DuplicateProjectScreenState();
}

class _DuplicateProjectScreenState extends State<DuplicateProjectScreen> {
  int currentPage = 0;
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();

    pageController.addListener(() {
      if (pageController.page == 0) {
        setState(() {
          currentPage = 0;
        });
      }
      if (pageController.page == 1) {
        setState(() {
          currentPage = 1;
        });
      }
      if (pageController.page == 2) {
        setState(() {
          currentPage = 2;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 40,
        ),
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width - 40,
            decoration: BoxDecoration(
                color: ColorConfig.blue4,
                borderRadius: BorderRadius.circular(10)),
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Duplicate Board',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: ColorConfig.primary),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    CustomWidget2(1, 'List', currentPage == 0),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Container(
                          height: 1,
                          color: ColorConfig.primary,
                        ),
                      ),
                    ),
                    CustomWidget2(2, 'Card', currentPage == 1),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Container(
                          height: 1,
                          color: ColorConfig.primary,
                        ),
                      ),
                    ),
                    CustomWidget2(3, 'Labels', currentPage == 2),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: Container(
                    child: PageView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: pageController,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            CustomCheckBoxWidget('All'),
                            CustomCheckBoxWidget('Ready for inspections'),
                            CustomCheckBoxWidget('Backlog'),
                            CustomCheckBoxWidget('In Progress'),
                            Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  color: ColorConfig.primary,
                                  child: Text(
                                    'Next',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.white),
                                  ),
                                  onPressed: () {
                                    pageController.animateToPage(1,
                                        curve: Curves.easeInOut,
                                        duration: Duration(milliseconds: 200));
                                  },
                                )
                              ],
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            CustomCheckBoxWidget('All'),
                            CustomCheckBoxWidget('Install Roof Drains'),
                            CustomCheckBoxWidget('Product sample testing'),
                            CustomCheckBoxWidget(
                                'Install Roof Drains 2nd Stage'),
                            CustomCheckBoxWidget(
                                'Install Roof Drains 2rd Stage'),
                            Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text(
                                    'Back',
                                    style: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        fontSize: 14,
                                        color: ColorConfig.grey5),
                                  ),
                                  onPressed: () {
                                    pageController.animateToPage(0,
                                        curve: Curves.easeInOut,
                                        duration: Duration(milliseconds: 200));
                                  },
                                ),
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  color: ColorConfig.primary,
                                  child: Text(
                                    'Next',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.white),
                                  ),
                                  onPressed: () {
                                    pageController.animateToPage(2,
                                        curve: Curves.easeInOut,
                                        duration: Duration(milliseconds: 200));
                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            CustomCheckBoxWidget('All'),
                            CustomCheckBoxWidget('Underground'),
                            CustomCheckBoxWidget('Sample Testing'),
                            CustomCheckBoxWidget('Training'),
                            CustomCheckBoxWidget('Construction'),
                            Spacer(),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text(
                                    'Back',
                                    style: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        fontSize: 14,
                                        color: ColorConfig.grey5),
                                  ),
                                  onPressed: () {
                                    pageController.animateToPage(1,
                                        curve: Curves.easeInOut,
                                        duration: Duration(milliseconds: 200));
                                  },
                                ),
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)),
                                  color: ColorConfig.primary,
                                  child: Text(
                                    'Done',
                                    style: TextStyle(
                                        fontSize: 14, color: Colors.white),
                                  ),
                                  onPressed: () {},
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class CustomWidget2 extends StatelessWidget {
  final int number;
  final String title;
  final bool selected;

  CustomWidget2(this.number, this.title, this.selected);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        CircleAvatar(
          backgroundColor: selected ? ColorConfig.primary : ColorConfig.black2,
          radius: 12,
          child: Text(
            number.toString(),
            style: TextStyle(
                color: Colors.white, fontSize: 13, fontWeight: FontWeight.w500),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          title,
          style: TextStyle(
              color: selected ? ColorConfig.primary : ColorConfig.black2,
              fontSize: 14,
              fontWeight: FontWeight.w500),
        )
      ],
    );
  }
}

class CustomCheckBoxWidget extends StatelessWidget {
  final String title;

  CustomCheckBoxWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: false,
          onChanged: (val) {},
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          title,
          style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: ColorConfig.black2),
        )
      ],
    );
  }
}
