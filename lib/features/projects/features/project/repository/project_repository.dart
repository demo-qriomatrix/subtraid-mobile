import 'package:Subtraid/features/projects/features/project/feataures/team_members/models/remove_member_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_response_model.dart';
import 'package:dio/dio.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:meta/meta.dart';

class ProjectRepository {
  final HttpClient httpClient;

  ProjectRepository({@required this.httpClient});

  Future<Response> deleteProject(String projectId) async {
    Response response =
        await httpClient.dio.delete(EndpointConfig.projects + '/$projectId');

    return response;
  }

  Future<Response> removeMember(RemoveMemberModel memberModel) async {
    Response response = await httpClient.dio.patch(
        EndpointConfig.projects + '/member/remove',
        data: memberModel.toJson());

    return response;
  }

  Future<Response> updateProject(ProjectResponseModel project) async {
    Response response = await httpClient.dio.patch(
        EndpointConfig.projects + '/${project.board.id}',
        data: project.toJson());

    return response;
  }
}
