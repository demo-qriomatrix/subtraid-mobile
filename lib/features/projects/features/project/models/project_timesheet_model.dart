library project_timesheet_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'project_timesheet_model.g.dart';

abstract class ProjectTimesheetModel
    implements Built<ProjectTimesheetModel, ProjectTimesheetModelBuilder> {
  @nullable
  String get start;

  @nullable
  String get end;

  @nullable
  String get user;

  @nullable
  int get status;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  @BuiltValueField(wireName: '_projectId')
  String get projectId;

  ProjectTimesheetModel._();

  factory ProjectTimesheetModel([updates(ProjectTimesheetModelBuilder b)]) =
      _$ProjectTimesheetModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectTimesheetModel.serializer, this));
  }

  static ProjectTimesheetModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectTimesheetModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectTimesheetModel> get serializer =>
      _$projectTimesheetModelSerializer;
}
