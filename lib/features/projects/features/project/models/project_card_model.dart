library project_card_model;

import 'dart:convert';

import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_timesheet_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_attachment_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_checklist_model.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/models/card_comment_model.dart';

part 'project_card_model.g.dart';

abstract class ProjectCardModel
    implements Built<ProjectCardModel, ProjectCardModelBuilder> {
  @nullable
  String get status;

  @nullable
  BuiltList<CardTimesheetModel> get timesheets;

  @nullable
  BuiltList<UserModel> get idMembers;

  @nullable
  BuiltList<String> get idLabels;

  @nullable
  BuiltList<CardAttachmentModel> get attachments;

  // @nullable
  // bool get subscribed;

  @nullable
  int get checkItems;

  @nullable
  int get checkItemsChecked;

  // @nullable
  // BuiltList<Null> get activities;

  @nullable
  String get name;

  @nullable
  String get description;

  @nullable
  BuiltList<CardChecklistModel> get checklists;

  @nullable
  BuiltList<CardCommentModel> get comments;

  // @nullable
  // String get due;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get listId;

  @nullable
  String get start;

  @nullable
  String get end;

  // @nullable
  // String get dateCreated;

  @nullable
  CardTimesheetModel get activeTimesheet;

  ProjectCardModel._();

  factory ProjectCardModel([updates(ProjectCardModelBuilder b)]) =
      _$ProjectCardModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ProjectCardModel.serializer, this));
  }

  static ProjectCardModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectCardModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectCardModel> get serializer =>
      _$projectCardModelSerializer;
}
