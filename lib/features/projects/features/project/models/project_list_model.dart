library project_list_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'project_list_model.g.dart';

abstract class ProjectListModel
    implements Built<ProjectListModel, ProjectListModelBuilder> {
  @nullable
  BuiltList<String> get idCards;

  @nullable
  BuiltList<String> get idProjects;

  // @nullable
  // BuiltList<Null> get managers;

  @nullable
  String get name;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  int get position;

  ProjectListModel._();

  factory ProjectListModel([updates(ProjectListModelBuilder b)]) =
      _$ProjectListModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ProjectListModel.serializer, this));
  }

  static ProjectListModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectListModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectListModel> get serializer =>
      _$projectListModelSerializer;
}
