library project_collaborators_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'project_collaborators_model.g.dart';

abstract class ProjectCollaboratorsModel
    implements
        Built<ProjectCollaboratorsModel, ProjectCollaboratorsModelBuilder> {
  @nullable
  String get img;

  @nullable
  String get name;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  ProjectCollaboratorsModel._();

  factory ProjectCollaboratorsModel(
          [updates(ProjectCollaboratorsModelBuilder b)]) =
      _$ProjectCollaboratorsModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectCollaboratorsModel.serializer, this));
  }

  static ProjectCollaboratorsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectCollaboratorsModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectCollaboratorsModel> get serializer =>
      _$projectCollaboratorsModelSerializer;
}
