library project_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/add_project/models/location_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_collaborators_model.dart';
import 'package:Subtraid/features/projects/features/project/models/project_timesheet_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permissions_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_role_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

import 'project_card_model.dart';
import 'project_label_model.dart';
import 'project_list_model.dart';
import 'project_member_id_model.dart';
import 'project_sub_project_model.dart';

part 'project_model.g.dart';

abstract class ProjectModel
    implements Built<ProjectModel, ProjectModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  // @nullable
  // ProjectSettingModel get settings;

  @nullable
  @BuiltValueField(wireName: 'subprojects')
  BuiltList<ProjectSubProjectModel> get subProjects;

  @nullable
  BuiltList<ProjectCollaboratorsModel> get collaborators;

  @nullable
  BuiltList<ProjectTimesheetModel> get timesheets;

  // @nullable
  // String get status;

  @nullable
  BuiltList<ProjectMemberIdModel> get idMembers;

  @nullable
  BuiltList<String> get idCrews;

  // @nullable
  // BuiltList<ProjectManagerModel> get managers;

  @nullable
  String get name;

  // @nullable
  // String get author;

  // @nullable
  // String get owner;

  @nullable
  JsonObject get location;

  LocationModel get getLocation {
    if (location != null && location.isString) {
      print(location);
    }

    if (name != null && location != null && location.isMap) {
      return LocationModel.fromJson(json.encode(location.asMap));
    }

    return null;
  }

  @nullable
  BuiltList<ProjectLabelModel> get labels;

  @nullable
  BuiltList<ProjectListModel> get lists;

  @nullable
  BuiltList<ProjectCardModel> get cards;

  @nullable
  String get formattedAddress;

  // @nullable
  // String get dateCreated;

  @nullable
  String get startDate;

  @nullable
  BuiltList<ProjectPermissionRoleModel> get permissions;

  // @nullable
  // @BuiltValueField(wireName: '__v')
  // int get v;

  // @nullable
  // String get conversation;

  @nullable
  PermissionsModel get loginUserProjectPermission;

  ProjectModel._();

  factory ProjectModel([void Function(ProjectModelBuilder) updates]) =
      _$ProjectModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ProjectModel.serializer, this));
  }

  static ProjectModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectModel> get serializer => _$projectModelSerializer;
}
