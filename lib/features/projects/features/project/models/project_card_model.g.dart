// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_card_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectCardModel> _$projectCardModelSerializer =
    new _$ProjectCardModelSerializer();

class _$ProjectCardModelSerializer
    implements StructuredSerializer<ProjectCardModel> {
  @override
  final Iterable<Type> types = const [ProjectCardModel, _$ProjectCardModel];
  @override
  final String wireName = 'ProjectCardModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ProjectCardModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(String)));
    }
    if (object.timesheets != null) {
      result
        ..add('timesheets')
        ..add(serializers.serialize(object.timesheets,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CardTimesheetModel)])));
    }
    if (object.idMembers != null) {
      result
        ..add('idMembers')
        ..add(serializers.serialize(object.idMembers,
            specifiedType:
                const FullType(BuiltList, const [const FullType(UserModel)])));
    }
    if (object.idLabels != null) {
      result
        ..add('idLabels')
        ..add(serializers.serialize(object.idLabels,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.attachments != null) {
      result
        ..add('attachments')
        ..add(serializers.serialize(object.attachments,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CardAttachmentModel)])));
    }
    if (object.checkItems != null) {
      result
        ..add('checkItems')
        ..add(serializers.serialize(object.checkItems,
            specifiedType: const FullType(int)));
    }
    if (object.checkItemsChecked != null) {
      result
        ..add('checkItemsChecked')
        ..add(serializers.serialize(object.checkItemsChecked,
            specifiedType: const FullType(int)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.checklists != null) {
      result
        ..add('checklists')
        ..add(serializers.serialize(object.checklists,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CardChecklistModel)])));
    }
    if (object.comments != null) {
      result
        ..add('comments')
        ..add(serializers.serialize(object.comments,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CardCommentModel)])));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.listId != null) {
      result
        ..add('listId')
        ..add(serializers.serialize(object.listId,
            specifiedType: const FullType(String)));
    }
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.activeTimesheet != null) {
      result
        ..add('activeTimesheet')
        ..add(serializers.serialize(object.activeTimesheet,
            specifiedType: const FullType(CardTimesheetModel)));
    }
    return result;
  }

  @override
  ProjectCardModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectCardModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'timesheets':
          result.timesheets.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CardTimesheetModel)]))
              as BuiltList<Object>);
          break;
        case 'idMembers':
          result.idMembers.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(UserModel)]))
              as BuiltList<Object>);
          break;
        case 'idLabels':
          result.idLabels.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case 'attachments':
          result.attachments.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CardAttachmentModel)]))
              as BuiltList<Object>);
          break;
        case 'checkItems':
          result.checkItems = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'checkItemsChecked':
          result.checkItemsChecked = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'checklists':
          result.checklists.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CardChecklistModel)]))
              as BuiltList<Object>);
          break;
        case 'comments':
          result.comments.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CardCommentModel)]))
              as BuiltList<Object>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'listId':
          result.listId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'activeTimesheet':
          result.activeTimesheet.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardTimesheetModel))
              as CardTimesheetModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectCardModel extends ProjectCardModel {
  @override
  final String status;
  @override
  final BuiltList<CardTimesheetModel> timesheets;
  @override
  final BuiltList<UserModel> idMembers;
  @override
  final BuiltList<String> idLabels;
  @override
  final BuiltList<CardAttachmentModel> attachments;
  @override
  final int checkItems;
  @override
  final int checkItemsChecked;
  @override
  final String name;
  @override
  final String description;
  @override
  final BuiltList<CardChecklistModel> checklists;
  @override
  final BuiltList<CardCommentModel> comments;
  @override
  final String id;
  @override
  final String listId;
  @override
  final String start;
  @override
  final String end;
  @override
  final CardTimesheetModel activeTimesheet;

  factory _$ProjectCardModel(
          [void Function(ProjectCardModelBuilder) updates]) =>
      (new ProjectCardModelBuilder()..update(updates)).build();

  _$ProjectCardModel._(
      {this.status,
      this.timesheets,
      this.idMembers,
      this.idLabels,
      this.attachments,
      this.checkItems,
      this.checkItemsChecked,
      this.name,
      this.description,
      this.checklists,
      this.comments,
      this.id,
      this.listId,
      this.start,
      this.end,
      this.activeTimesheet})
      : super._();

  @override
  ProjectCardModel rebuild(void Function(ProjectCardModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectCardModelBuilder toBuilder() =>
      new ProjectCardModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectCardModel &&
        status == other.status &&
        timesheets == other.timesheets &&
        idMembers == other.idMembers &&
        idLabels == other.idLabels &&
        attachments == other.attachments &&
        checkItems == other.checkItems &&
        checkItemsChecked == other.checkItemsChecked &&
        name == other.name &&
        description == other.description &&
        checklists == other.checklists &&
        comments == other.comments &&
        id == other.id &&
        listId == other.listId &&
        start == other.start &&
        end == other.end &&
        activeTimesheet == other.activeTimesheet;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    0,
                                                                    status
                                                                        .hashCode),
                                                                timesheets
                                                                    .hashCode),
                                                            idMembers.hashCode),
                                                        idLabels.hashCode),
                                                    attachments.hashCode),
                                                checkItems.hashCode),
                                            checkItemsChecked.hashCode),
                                        name.hashCode),
                                    description.hashCode),
                                checklists.hashCode),
                            comments.hashCode),
                        id.hashCode),
                    listId.hashCode),
                start.hashCode),
            end.hashCode),
        activeTimesheet.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectCardModel')
          ..add('status', status)
          ..add('timesheets', timesheets)
          ..add('idMembers', idMembers)
          ..add('idLabels', idLabels)
          ..add('attachments', attachments)
          ..add('checkItems', checkItems)
          ..add('checkItemsChecked', checkItemsChecked)
          ..add('name', name)
          ..add('description', description)
          ..add('checklists', checklists)
          ..add('comments', comments)
          ..add('id', id)
          ..add('listId', listId)
          ..add('start', start)
          ..add('end', end)
          ..add('activeTimesheet', activeTimesheet))
        .toString();
  }
}

class ProjectCardModelBuilder
    implements Builder<ProjectCardModel, ProjectCardModelBuilder> {
  _$ProjectCardModel _$v;

  String _status;
  String get status => _$this._status;
  set status(String status) => _$this._status = status;

  ListBuilder<CardTimesheetModel> _timesheets;
  ListBuilder<CardTimesheetModel> get timesheets =>
      _$this._timesheets ??= new ListBuilder<CardTimesheetModel>();
  set timesheets(ListBuilder<CardTimesheetModel> timesheets) =>
      _$this._timesheets = timesheets;

  ListBuilder<UserModel> _idMembers;
  ListBuilder<UserModel> get idMembers =>
      _$this._idMembers ??= new ListBuilder<UserModel>();
  set idMembers(ListBuilder<UserModel> idMembers) =>
      _$this._idMembers = idMembers;

  ListBuilder<String> _idLabels;
  ListBuilder<String> get idLabels =>
      _$this._idLabels ??= new ListBuilder<String>();
  set idLabels(ListBuilder<String> idLabels) => _$this._idLabels = idLabels;

  ListBuilder<CardAttachmentModel> _attachments;
  ListBuilder<CardAttachmentModel> get attachments =>
      _$this._attachments ??= new ListBuilder<CardAttachmentModel>();
  set attachments(ListBuilder<CardAttachmentModel> attachments) =>
      _$this._attachments = attachments;

  int _checkItems;
  int get checkItems => _$this._checkItems;
  set checkItems(int checkItems) => _$this._checkItems = checkItems;

  int _checkItemsChecked;
  int get checkItemsChecked => _$this._checkItemsChecked;
  set checkItemsChecked(int checkItemsChecked) =>
      _$this._checkItemsChecked = checkItemsChecked;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  ListBuilder<CardChecklistModel> _checklists;
  ListBuilder<CardChecklistModel> get checklists =>
      _$this._checklists ??= new ListBuilder<CardChecklistModel>();
  set checklists(ListBuilder<CardChecklistModel> checklists) =>
      _$this._checklists = checklists;

  ListBuilder<CardCommentModel> _comments;
  ListBuilder<CardCommentModel> get comments =>
      _$this._comments ??= new ListBuilder<CardCommentModel>();
  set comments(ListBuilder<CardCommentModel> comments) =>
      _$this._comments = comments;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _listId;
  String get listId => _$this._listId;
  set listId(String listId) => _$this._listId = listId;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  CardTimesheetModelBuilder _activeTimesheet;
  CardTimesheetModelBuilder get activeTimesheet =>
      _$this._activeTimesheet ??= new CardTimesheetModelBuilder();
  set activeTimesheet(CardTimesheetModelBuilder activeTimesheet) =>
      _$this._activeTimesheet = activeTimesheet;

  ProjectCardModelBuilder();

  ProjectCardModelBuilder get _$this {
    if (_$v != null) {
      _status = _$v.status;
      _timesheets = _$v.timesheets?.toBuilder();
      _idMembers = _$v.idMembers?.toBuilder();
      _idLabels = _$v.idLabels?.toBuilder();
      _attachments = _$v.attachments?.toBuilder();
      _checkItems = _$v.checkItems;
      _checkItemsChecked = _$v.checkItemsChecked;
      _name = _$v.name;
      _description = _$v.description;
      _checklists = _$v.checklists?.toBuilder();
      _comments = _$v.comments?.toBuilder();
      _id = _$v.id;
      _listId = _$v.listId;
      _start = _$v.start;
      _end = _$v.end;
      _activeTimesheet = _$v.activeTimesheet?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectCardModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectCardModel;
  }

  @override
  void update(void Function(ProjectCardModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectCardModel build() {
    _$ProjectCardModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectCardModel._(
              status: status,
              timesheets: _timesheets?.build(),
              idMembers: _idMembers?.build(),
              idLabels: _idLabels?.build(),
              attachments: _attachments?.build(),
              checkItems: checkItems,
              checkItemsChecked: checkItemsChecked,
              name: name,
              description: description,
              checklists: _checklists?.build(),
              comments: _comments?.build(),
              id: id,
              listId: listId,
              start: start,
              end: end,
              activeTimesheet: _activeTimesheet?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'timesheets';
        _timesheets?.build();
        _$failedField = 'idMembers';
        _idMembers?.build();
        _$failedField = 'idLabels';
        _idLabels?.build();
        _$failedField = 'attachments';
        _attachments?.build();

        _$failedField = 'checklists';
        _checklists?.build();
        _$failedField = 'comments';
        _comments?.build();

        _$failedField = 'activeTimesheet';
        _activeTimesheet?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectCardModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
