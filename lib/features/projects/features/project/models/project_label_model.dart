library project_label_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'project_label_model.g.dart';

abstract class ProjectLabelModel
    implements Built<ProjectLabelModel, ProjectLabelModelBuilder> {
  @nullable
  String get name;

  @nullable
  String get color;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  ProjectLabelModel._();

  factory ProjectLabelModel([updates(ProjectLabelModelBuilder b)]) =
      _$ProjectLabelModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ProjectLabelModel.serializer, this));
  }

  static ProjectLabelModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectLabelModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectLabelModel> get serializer =>
      _$projectLabelModelSerializer;
}
