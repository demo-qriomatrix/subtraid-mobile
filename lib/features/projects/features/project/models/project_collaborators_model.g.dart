// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_collaborators_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectCollaboratorsModel> _$projectCollaboratorsModelSerializer =
    new _$ProjectCollaboratorsModelSerializer();

class _$ProjectCollaboratorsModelSerializer
    implements StructuredSerializer<ProjectCollaboratorsModel> {
  @override
  final Iterable<Type> types = const [
    ProjectCollaboratorsModel,
    _$ProjectCollaboratorsModel
  ];
  @override
  final String wireName = 'ProjectCollaboratorsModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectCollaboratorsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProjectCollaboratorsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectCollaboratorsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectCollaboratorsModel extends ProjectCollaboratorsModel {
  @override
  final String img;
  @override
  final String name;
  @override
  final String id;

  factory _$ProjectCollaboratorsModel(
          [void Function(ProjectCollaboratorsModelBuilder) updates]) =>
      (new ProjectCollaboratorsModelBuilder()..update(updates)).build();

  _$ProjectCollaboratorsModel._({this.img, this.name, this.id}) : super._();

  @override
  ProjectCollaboratorsModel rebuild(
          void Function(ProjectCollaboratorsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectCollaboratorsModelBuilder toBuilder() =>
      new ProjectCollaboratorsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectCollaboratorsModel &&
        img == other.img &&
        name == other.name &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, img.hashCode), name.hashCode), id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectCollaboratorsModel')
          ..add('img', img)
          ..add('name', name)
          ..add('id', id))
        .toString();
  }
}

class ProjectCollaboratorsModelBuilder
    implements
        Builder<ProjectCollaboratorsModel, ProjectCollaboratorsModelBuilder> {
  _$ProjectCollaboratorsModel _$v;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ProjectCollaboratorsModelBuilder();

  ProjectCollaboratorsModelBuilder get _$this {
    if (_$v != null) {
      _img = _$v.img;
      _name = _$v.name;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectCollaboratorsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectCollaboratorsModel;
  }

  @override
  void update(void Function(ProjectCollaboratorsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectCollaboratorsModel build() {
    final _$result =
        _$v ?? new _$ProjectCollaboratorsModel._(img: img, name: name, id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
