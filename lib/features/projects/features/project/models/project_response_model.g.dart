// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectResponseModel> _$projectResponseModelSerializer =
    new _$ProjectResponseModelSerializer();

class _$ProjectResponseModelSerializer
    implements StructuredSerializer<ProjectResponseModel> {
  @override
  final Iterable<Type> types = const [
    ProjectResponseModel,
    _$ProjectResponseModel
  ];
  @override
  final String wireName = 'ProjectResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.board != null) {
      result
        ..add('board')
        ..add(serializers.serialize(object.board,
            specifiedType: const FullType(ProjectModel)));
    }
    return result;
  }

  @override
  ProjectResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'board':
          result.board.replace(serializers.deserialize(value,
              specifiedType: const FullType(ProjectModel)) as ProjectModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectResponseModel extends ProjectResponseModel {
  @override
  final ProjectModel board;

  factory _$ProjectResponseModel(
          [void Function(ProjectResponseModelBuilder) updates]) =>
      (new ProjectResponseModelBuilder()..update(updates)).build();

  _$ProjectResponseModel._({this.board}) : super._();

  @override
  ProjectResponseModel rebuild(
          void Function(ProjectResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectResponseModelBuilder toBuilder() =>
      new ProjectResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectResponseModel && board == other.board;
  }

  @override
  int get hashCode {
    return $jf($jc(0, board.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectResponseModel')
          ..add('board', board))
        .toString();
  }
}

class ProjectResponseModelBuilder
    implements Builder<ProjectResponseModel, ProjectResponseModelBuilder> {
  _$ProjectResponseModel _$v;

  ProjectModelBuilder _board;
  ProjectModelBuilder get board => _$this._board ??= new ProjectModelBuilder();
  set board(ProjectModelBuilder board) => _$this._board = board;

  ProjectResponseModelBuilder();

  ProjectResponseModelBuilder get _$this {
    if (_$v != null) {
      _board = _$v.board?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectResponseModel;
  }

  @override
  void update(void Function(ProjectResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectResponseModel build() {
    _$ProjectResponseModel _$result;
    try {
      _$result = _$v ?? new _$ProjectResponseModel._(board: _board?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'board';
        _board?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
