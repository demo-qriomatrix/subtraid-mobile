// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_list_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectListModel> _$projectListModelSerializer =
    new _$ProjectListModelSerializer();

class _$ProjectListModelSerializer
    implements StructuredSerializer<ProjectListModel> {
  @override
  final Iterable<Type> types = const [ProjectListModel, _$ProjectListModel];
  @override
  final String wireName = 'ProjectListModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ProjectListModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idCards != null) {
      result
        ..add('idCards')
        ..add(serializers.serialize(object.idCards,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.idProjects != null) {
      result
        ..add('idProjects')
        ..add(serializers.serialize(object.idProjects,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.position != null) {
      result
        ..add('position')
        ..add(serializers.serialize(object.position,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ProjectListModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectListModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'idCards':
          result.idCards.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case 'idProjects':
          result.idProjects.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'position':
          result.position = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectListModel extends ProjectListModel {
  @override
  final BuiltList<String> idCards;
  @override
  final BuiltList<String> idProjects;
  @override
  final String name;
  @override
  final String id;
  @override
  final int position;

  factory _$ProjectListModel(
          [void Function(ProjectListModelBuilder) updates]) =>
      (new ProjectListModelBuilder()..update(updates)).build();

  _$ProjectListModel._(
      {this.idCards, this.idProjects, this.name, this.id, this.position})
      : super._();

  @override
  ProjectListModel rebuild(void Function(ProjectListModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectListModelBuilder toBuilder() =>
      new ProjectListModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectListModel &&
        idCards == other.idCards &&
        idProjects == other.idProjects &&
        name == other.name &&
        id == other.id &&
        position == other.position;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, idCards.hashCode), idProjects.hashCode),
                name.hashCode),
            id.hashCode),
        position.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectListModel')
          ..add('idCards', idCards)
          ..add('idProjects', idProjects)
          ..add('name', name)
          ..add('id', id)
          ..add('position', position))
        .toString();
  }
}

class ProjectListModelBuilder
    implements Builder<ProjectListModel, ProjectListModelBuilder> {
  _$ProjectListModel _$v;

  ListBuilder<String> _idCards;
  ListBuilder<String> get idCards =>
      _$this._idCards ??= new ListBuilder<String>();
  set idCards(ListBuilder<String> idCards) => _$this._idCards = idCards;

  ListBuilder<String> _idProjects;
  ListBuilder<String> get idProjects =>
      _$this._idProjects ??= new ListBuilder<String>();
  set idProjects(ListBuilder<String> idProjects) =>
      _$this._idProjects = idProjects;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  int _position;
  int get position => _$this._position;
  set position(int position) => _$this._position = position;

  ProjectListModelBuilder();

  ProjectListModelBuilder get _$this {
    if (_$v != null) {
      _idCards = _$v.idCards?.toBuilder();
      _idProjects = _$v.idProjects?.toBuilder();
      _name = _$v.name;
      _id = _$v.id;
      _position = _$v.position;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectListModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectListModel;
  }

  @override
  void update(void Function(ProjectListModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectListModel build() {
    _$ProjectListModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectListModel._(
              idCards: _idCards?.build(),
              idProjects: _idProjects?.build(),
              name: name,
              id: id,
              position: position);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'idCards';
        _idCards?.build();
        _$failedField = 'idProjects';
        _idProjects?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectListModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
