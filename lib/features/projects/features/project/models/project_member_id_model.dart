library project_member_id_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'project_member_id_model.g.dart';

abstract class ProjectMemberIdModel
    implements Built<ProjectMemberIdModel, ProjectMemberIdModelBuilder> {
  @BuiltValueField(wireName: '_id')
  @nullable
  String get id;

  @nullable
  bool get isSubtraid;

  @nullable
  bool get isAvailable;

  @nullable
  bool get isVerified;

  @nullable
  String get name;

  @nullable
  String get img;

  @nullable
  bool get selected;

  ProjectMemberIdModel._();

  factory ProjectMemberIdModel([updates(ProjectMemberIdModelBuilder b)]) =
      _$ProjectMemberIdModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectMemberIdModel.serializer, this));
  }

  static ProjectMemberIdModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectMemberIdModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectMemberIdModel> get serializer =>
      _$projectMemberIdModelSerializer;
}
