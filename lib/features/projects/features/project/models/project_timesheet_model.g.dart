// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_timesheet_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectTimesheetModel> _$projectTimesheetModelSerializer =
    new _$ProjectTimesheetModelSerializer();

class _$ProjectTimesheetModelSerializer
    implements StructuredSerializer<ProjectTimesheetModel> {
  @override
  final Iterable<Type> types = const [
    ProjectTimesheetModel,
    _$ProjectTimesheetModel
  ];
  @override
  final String wireName = 'ProjectTimesheetModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectTimesheetModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.start != null) {
      result
        ..add('start')
        ..add(serializers.serialize(object.start,
            specifiedType: const FullType(String)));
    }
    if (object.end != null) {
      result
        ..add('end')
        ..add(serializers.serialize(object.end,
            specifiedType: const FullType(String)));
    }
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.projectId != null) {
      result
        ..add('_projectId')
        ..add(serializers.serialize(object.projectId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProjectTimesheetModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectTimesheetModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'start':
          result.start = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'end':
          result.end = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user':
          result.user = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_projectId':
          result.projectId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectTimesheetModel extends ProjectTimesheetModel {
  @override
  final String start;
  @override
  final String end;
  @override
  final String user;
  @override
  final int status;
  @override
  final String id;
  @override
  final String projectId;

  factory _$ProjectTimesheetModel(
          [void Function(ProjectTimesheetModelBuilder) updates]) =>
      (new ProjectTimesheetModelBuilder()..update(updates)).build();

  _$ProjectTimesheetModel._(
      {this.start, this.end, this.user, this.status, this.id, this.projectId})
      : super._();

  @override
  ProjectTimesheetModel rebuild(
          void Function(ProjectTimesheetModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectTimesheetModelBuilder toBuilder() =>
      new ProjectTimesheetModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectTimesheetModel &&
        start == other.start &&
        end == other.end &&
        user == other.user &&
        status == other.status &&
        id == other.id &&
        projectId == other.projectId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, start.hashCode), end.hashCode), user.hashCode),
                status.hashCode),
            id.hashCode),
        projectId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectTimesheetModel')
          ..add('start', start)
          ..add('end', end)
          ..add('user', user)
          ..add('status', status)
          ..add('id', id)
          ..add('projectId', projectId))
        .toString();
  }
}

class ProjectTimesheetModelBuilder
    implements Builder<ProjectTimesheetModel, ProjectTimesheetModelBuilder> {
  _$ProjectTimesheetModel _$v;

  String _start;
  String get start => _$this._start;
  set start(String start) => _$this._start = start;

  String _end;
  String get end => _$this._end;
  set end(String end) => _$this._end = end;

  String _user;
  String get user => _$this._user;
  set user(String user) => _$this._user = user;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _projectId;
  String get projectId => _$this._projectId;
  set projectId(String projectId) => _$this._projectId = projectId;

  ProjectTimesheetModelBuilder();

  ProjectTimesheetModelBuilder get _$this {
    if (_$v != null) {
      _start = _$v.start;
      _end = _$v.end;
      _user = _$v.user;
      _status = _$v.status;
      _id = _$v.id;
      _projectId = _$v.projectId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectTimesheetModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectTimesheetModel;
  }

  @override
  void update(void Function(ProjectTimesheetModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectTimesheetModel build() {
    final _$result = _$v ??
        new _$ProjectTimesheetModel._(
            start: start,
            end: end,
            user: user,
            status: status,
            id: id,
            projectId: projectId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
