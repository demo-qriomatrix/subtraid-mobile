// library permission_list_model;

// import 'dart:convert';

// import 'package:built_collection/built_collection.dart';
// import 'package:built_value/built_value.dart';
// import 'package:built_value/serializer.dart';
// import 'package:Subtraid/core/serializers/serializers.dart';

// part 'permission_list_model.g.dart';

// abstract class PermissionListModel
//     implements Built<PermissionListModel, PermissionListModelBuilder> {
//   // fields go here

//   PermissionListModel._();

//   factory PermissionListModel([updates(PermissionListModelBuilder b)]) =
//       _$PermissionListModel;

//   String toJson() {
//     return json.encode(
//         serializers.serializeWith(PermissionListModel.serializer, this));
//   }

//   static PermissionListModel fromJson(String jsonString) {
//     return serializers.deserializeWith(
//         PermissionListModel.serializer, json.decode(jsonString));
//   }

//   static Serializer<PermissionListModel> get serializer =>
//       _$permissionListModelSerializer;
// }
