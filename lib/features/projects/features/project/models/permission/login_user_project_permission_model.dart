// library login_user_project_permission_model;

// import 'dart:convert';

// import 'package:built_value/built_value.dart';
// import 'package:built_value/serializer.dart';
// import 'package:Subtraid/core/serializers/serializers.dart';

// import 'permission_card_model.dart';
// import 'permission_list_model.dart';
// import 'permission_member_model.dart';
// import 'permission_project_model.dart';
// import 'permission_sub_project_model.dart';

// part 'login_user_project_permission_model.g.dart';

// abstract class LoginUserProjectPermission
//     implements
//         Built<LoginUserProjectPermission, LoginUserProjectPermissionBuilder> {
//   PermissionProjectModel get project;

//   PermissionCardModel get cards;

//   PermissionListModel get list;

//   @BuiltValueField(wireName: 'subproject')
//   PermissionSubProjectModel get subProject;

//   PermissionMemberModel get member;

//   LoginUserProjectPermission._();

//   factory LoginUserProjectPermission(
//           [void Function(LoginUserProjectPermissionBuilder) updates]) =
//       _$LoginUserProjectPermission;

//   String toJson() {
//     return json.encode(
//         serializers.serializeWith(LoginUserProjectPermission.serializer, this));
//   }

//   static LoginUserProjectPermission fromJson(String jsonString) {
//     return serializers.deserializeWith(
//         LoginUserProjectPermission.serializer, json.decode(jsonString));
//   }

//   static Serializer<LoginUserProjectPermission> get serializer =>
//       _$loginUserProjectPermissionSerializer;
// }
