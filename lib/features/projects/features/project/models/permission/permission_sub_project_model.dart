// library permission_sub_project_model;

// import 'dart:convert';

// import 'package:built_collection/built_collection.dart';
// import 'package:built_value/built_value.dart';
// import 'package:built_value/serializer.dart';
// import 'package:Subtraid/core/serializers/serializers.dart';

// part 'permission_sub_project_model.g.dart';

// abstract class PermissionSubProjectModel
//     implements
//         Built<PermissionSubProjectModel, PermissionSubProjectModelBuilder> {
//   // fields go here

//   PermissionSubProjectModel._();

//   factory PermissionSubProjectModel(
//           [updates(PermissionSubProjectModelBuilder b)]) =
//       _$PermissionSubProjectModel;

//   String toJson() {
//     return json.encode(
//         serializers.serializeWith(PermissionSubProjectModel.serializer, this));
//   }

//   static PermissionSubProjectModel fromJson(String jsonString) {
//     return serializers.deserializeWith(
//         PermissionSubProjectModel.serializer, json.decode(jsonString));
//   }

//   static Serializer<PermissionSubProjectModel> get serializer =>
//       _$permissionSubProjectModelSerializer;
// }
