library project_sub_project_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';

part 'project_sub_project_model.g.dart';

abstract class ProjectSubProjectModel
    implements Built<ProjectSubProjectModel, ProjectSubProjectModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  MemberModel get owner;

  ProjectSubProjectModel._();

  factory ProjectSubProjectModel(
          [void Function(ProjectSubProjectModelBuilder) updates]) =
      _$ProjectSubProjectModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectSubProjectModel.serializer, this));
  }

  static ProjectSubProjectModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectSubProjectModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectSubProjectModel> get serializer =>
      _$projectSubProjectModelSerializer;
}
