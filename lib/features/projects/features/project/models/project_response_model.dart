library project_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'project_model.dart';

part 'project_response_model.g.dart';

abstract class ProjectResponseModel
    implements Built<ProjectResponseModel, ProjectResponseModelBuilder> {
  // @nullable
  // String get message;

  // @nullable
  // int get status;

  @nullable
  ProjectModel get board;

  ProjectResponseModel._();

  factory ProjectResponseModel(
          [void Function(ProjectResponseModelBuilder) updates]) =
      _$ProjectResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectResponseModel.serializer, this));
  }

  static ProjectResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectResponseModel> get serializer =>
      _$projectResponseModelSerializer;
}
