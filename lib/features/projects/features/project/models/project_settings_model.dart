// library project_settings_model;

// import 'dart:convert';

// import 'package:built_value/built_value.dart';
// import 'package:built_value/serializer.dart';
// import 'package:Subtraid_mobile_v2/core/serializer/serializers.dart';
// import 'package:Subtraid/core/serializers/serializers.dart';

// part 'project_settings_model.g.dart';

// abstract class ProjectSettingModel
//     implements Built<ProjectSettingModel, ProjectSettingModelBuilder> {
//   @nullable
//   bool get subscribed;

//   @nullable
//   bool get cardCoverImages;

//   @nullable
//   String get color;

//   ProjectSettingModel._();

//   factory ProjectSettingModel(
//           [void Function(ProjectSettingModelBuilder) updates]) =
//       _$ProjectSettingModel;

//   String toJson() {
//     return json.encode(
//         serializers.serializeWith(ProjectSettingModel.serializer, this));
//   }

//   static ProjectSettingModel fromJson(String jsonString) {
//     return serializers.deserializeWith(
//         ProjectSettingModel.serializer, json.decode(jsonString));
//   }

//   static Serializer<ProjectSettingModel> get serializer =>
//       _$projectSettingModelSerializer;
// }
