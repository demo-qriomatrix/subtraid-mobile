// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectModel> _$projectModelSerializer =
    new _$ProjectModelSerializer();

class _$ProjectModelSerializer implements StructuredSerializer<ProjectModel> {
  @override
  final Iterable<Type> types = const [ProjectModel, _$ProjectModel];
  @override
  final String wireName = 'ProjectModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ProjectModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.subProjects != null) {
      result
        ..add('subprojects')
        ..add(serializers.serialize(object.subProjects,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectSubProjectModel)])));
    }
    if (object.collaborators != null) {
      result
        ..add('collaborators')
        ..add(serializers.serialize(object.collaborators,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectCollaboratorsModel)])));
    }
    if (object.timesheets != null) {
      result
        ..add('timesheets')
        ..add(serializers.serialize(object.timesheets,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectTimesheetModel)])));
    }
    if (object.idMembers != null) {
      result
        ..add('idMembers')
        ..add(serializers.serialize(object.idMembers,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectMemberIdModel)])));
    }
    if (object.idCrews != null) {
      result
        ..add('idCrews')
        ..add(serializers.serialize(object.idCrews,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(JsonObject)));
    }
    if (object.labels != null) {
      result
        ..add('labels')
        ..add(serializers.serialize(object.labels,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectLabelModel)])));
    }
    if (object.lists != null) {
      result
        ..add('lists')
        ..add(serializers.serialize(object.lists,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectListModel)])));
    }
    if (object.cards != null) {
      result
        ..add('cards')
        ..add(serializers.serialize(object.cards,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ProjectCardModel)])));
    }
    if (object.formattedAddress != null) {
      result
        ..add('formattedAddress')
        ..add(serializers.serialize(object.formattedAddress,
            specifiedType: const FullType(String)));
    }
    if (object.startDate != null) {
      result
        ..add('startDate')
        ..add(serializers.serialize(object.startDate,
            specifiedType: const FullType(String)));
    }
    if (object.permissions != null) {
      result
        ..add('permissions')
        ..add(serializers.serialize(object.permissions,
            specifiedType: const FullType(BuiltList,
                const [const FullType(ProjectPermissionRoleModel)])));
    }
    if (object.loginUserProjectPermission != null) {
      result
        ..add('loginUserProjectPermission')
        ..add(serializers.serialize(object.loginUserProjectPermission,
            specifiedType: const FullType(PermissionsModel)));
    }
    return result;
  }

  @override
  ProjectModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'subprojects':
          result.subProjects.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(ProjectSubProjectModel)
              ])) as BuiltList<Object>);
          break;
        case 'collaborators':
          result.collaborators.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(ProjectCollaboratorsModel)
              ])) as BuiltList<Object>);
          break;
        case 'timesheets':
          result.timesheets.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectTimesheetModel)]))
              as BuiltList<Object>);
          break;
        case 'idMembers':
          result.idMembers.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectMemberIdModel)]))
              as BuiltList<Object>);
          break;
        case 'idCrews':
          result.idCrews.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'location':
          result.location = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
        case 'labels':
          result.labels.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectLabelModel)]))
              as BuiltList<Object>);
          break;
        case 'lists':
          result.lists.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectListModel)]))
              as BuiltList<Object>);
          break;
        case 'cards':
          result.cards.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ProjectCardModel)]))
              as BuiltList<Object>);
          break;
        case 'formattedAddress':
          result.formattedAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'startDate':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'permissions':
          result.permissions.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(ProjectPermissionRoleModel)
              ])) as BuiltList<Object>);
          break;
        case 'loginUserProjectPermission':
          result.loginUserProjectPermission.replace(serializers.deserialize(
                  value,
                  specifiedType: const FullType(PermissionsModel))
              as PermissionsModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectModel extends ProjectModel {
  @override
  final String id;
  @override
  final BuiltList<ProjectSubProjectModel> subProjects;
  @override
  final BuiltList<ProjectCollaboratorsModel> collaborators;
  @override
  final BuiltList<ProjectTimesheetModel> timesheets;
  @override
  final BuiltList<ProjectMemberIdModel> idMembers;
  @override
  final BuiltList<String> idCrews;
  @override
  final String name;
  @override
  final JsonObject location;
  @override
  final BuiltList<ProjectLabelModel> labels;
  @override
  final BuiltList<ProjectListModel> lists;
  @override
  final BuiltList<ProjectCardModel> cards;
  @override
  final String formattedAddress;
  @override
  final String startDate;
  @override
  final BuiltList<ProjectPermissionRoleModel> permissions;
  @override
  final PermissionsModel loginUserProjectPermission;

  factory _$ProjectModel([void Function(ProjectModelBuilder) updates]) =>
      (new ProjectModelBuilder()..update(updates)).build();

  _$ProjectModel._(
      {this.id,
      this.subProjects,
      this.collaborators,
      this.timesheets,
      this.idMembers,
      this.idCrews,
      this.name,
      this.location,
      this.labels,
      this.lists,
      this.cards,
      this.formattedAddress,
      this.startDate,
      this.permissions,
      this.loginUserProjectPermission})
      : super._();

  @override
  ProjectModel rebuild(void Function(ProjectModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectModelBuilder toBuilder() => new ProjectModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectModel &&
        id == other.id &&
        subProjects == other.subProjects &&
        collaborators == other.collaborators &&
        timesheets == other.timesheets &&
        idMembers == other.idMembers &&
        idCrews == other.idCrews &&
        name == other.name &&
        location == other.location &&
        labels == other.labels &&
        lists == other.lists &&
        cards == other.cards &&
        formattedAddress == other.formattedAddress &&
        startDate == other.startDate &&
        permissions == other.permissions &&
        loginUserProjectPermission == other.loginUserProjectPermission;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(0, id.hashCode),
                                                            subProjects
                                                                .hashCode),
                                                        collaborators.hashCode),
                                                    timesheets.hashCode),
                                                idMembers.hashCode),
                                            idCrews.hashCode),
                                        name.hashCode),
                                    location.hashCode),
                                labels.hashCode),
                            lists.hashCode),
                        cards.hashCode),
                    formattedAddress.hashCode),
                startDate.hashCode),
            permissions.hashCode),
        loginUserProjectPermission.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectModel')
          ..add('id', id)
          ..add('subProjects', subProjects)
          ..add('collaborators', collaborators)
          ..add('timesheets', timesheets)
          ..add('idMembers', idMembers)
          ..add('idCrews', idCrews)
          ..add('name', name)
          ..add('location', location)
          ..add('labels', labels)
          ..add('lists', lists)
          ..add('cards', cards)
          ..add('formattedAddress', formattedAddress)
          ..add('startDate', startDate)
          ..add('permissions', permissions)
          ..add('loginUserProjectPermission', loginUserProjectPermission))
        .toString();
  }
}

class ProjectModelBuilder
    implements Builder<ProjectModel, ProjectModelBuilder> {
  _$ProjectModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ListBuilder<ProjectSubProjectModel> _subProjects;
  ListBuilder<ProjectSubProjectModel> get subProjects =>
      _$this._subProjects ??= new ListBuilder<ProjectSubProjectModel>();
  set subProjects(ListBuilder<ProjectSubProjectModel> subProjects) =>
      _$this._subProjects = subProjects;

  ListBuilder<ProjectCollaboratorsModel> _collaborators;
  ListBuilder<ProjectCollaboratorsModel> get collaborators =>
      _$this._collaborators ??= new ListBuilder<ProjectCollaboratorsModel>();
  set collaborators(ListBuilder<ProjectCollaboratorsModel> collaborators) =>
      _$this._collaborators = collaborators;

  ListBuilder<ProjectTimesheetModel> _timesheets;
  ListBuilder<ProjectTimesheetModel> get timesheets =>
      _$this._timesheets ??= new ListBuilder<ProjectTimesheetModel>();
  set timesheets(ListBuilder<ProjectTimesheetModel> timesheets) =>
      _$this._timesheets = timesheets;

  ListBuilder<ProjectMemberIdModel> _idMembers;
  ListBuilder<ProjectMemberIdModel> get idMembers =>
      _$this._idMembers ??= new ListBuilder<ProjectMemberIdModel>();
  set idMembers(ListBuilder<ProjectMemberIdModel> idMembers) =>
      _$this._idMembers = idMembers;

  ListBuilder<String> _idCrews;
  ListBuilder<String> get idCrews =>
      _$this._idCrews ??= new ListBuilder<String>();
  set idCrews(ListBuilder<String> idCrews) => _$this._idCrews = idCrews;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  JsonObject _location;
  JsonObject get location => _$this._location;
  set location(JsonObject location) => _$this._location = location;

  ListBuilder<ProjectLabelModel> _labels;
  ListBuilder<ProjectLabelModel> get labels =>
      _$this._labels ??= new ListBuilder<ProjectLabelModel>();
  set labels(ListBuilder<ProjectLabelModel> labels) => _$this._labels = labels;

  ListBuilder<ProjectListModel> _lists;
  ListBuilder<ProjectListModel> get lists =>
      _$this._lists ??= new ListBuilder<ProjectListModel>();
  set lists(ListBuilder<ProjectListModel> lists) => _$this._lists = lists;

  ListBuilder<ProjectCardModel> _cards;
  ListBuilder<ProjectCardModel> get cards =>
      _$this._cards ??= new ListBuilder<ProjectCardModel>();
  set cards(ListBuilder<ProjectCardModel> cards) => _$this._cards = cards;

  String _formattedAddress;
  String get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  String _startDate;
  String get startDate => _$this._startDate;
  set startDate(String startDate) => _$this._startDate = startDate;

  ListBuilder<ProjectPermissionRoleModel> _permissions;
  ListBuilder<ProjectPermissionRoleModel> get permissions =>
      _$this._permissions ??= new ListBuilder<ProjectPermissionRoleModel>();
  set permissions(ListBuilder<ProjectPermissionRoleModel> permissions) =>
      _$this._permissions = permissions;

  PermissionsModelBuilder _loginUserProjectPermission;
  PermissionsModelBuilder get loginUserProjectPermission =>
      _$this._loginUserProjectPermission ??= new PermissionsModelBuilder();
  set loginUserProjectPermission(
          PermissionsModelBuilder loginUserProjectPermission) =>
      _$this._loginUserProjectPermission = loginUserProjectPermission;

  ProjectModelBuilder();

  ProjectModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _subProjects = _$v.subProjects?.toBuilder();
      _collaborators = _$v.collaborators?.toBuilder();
      _timesheets = _$v.timesheets?.toBuilder();
      _idMembers = _$v.idMembers?.toBuilder();
      _idCrews = _$v.idCrews?.toBuilder();
      _name = _$v.name;
      _location = _$v.location;
      _labels = _$v.labels?.toBuilder();
      _lists = _$v.lists?.toBuilder();
      _cards = _$v.cards?.toBuilder();
      _formattedAddress = _$v.formattedAddress;
      _startDate = _$v.startDate;
      _permissions = _$v.permissions?.toBuilder();
      _loginUserProjectPermission = _$v.loginUserProjectPermission?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectModel;
  }

  @override
  void update(void Function(ProjectModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectModel build() {
    _$ProjectModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectModel._(
              id: id,
              subProjects: _subProjects?.build(),
              collaborators: _collaborators?.build(),
              timesheets: _timesheets?.build(),
              idMembers: _idMembers?.build(),
              idCrews: _idCrews?.build(),
              name: name,
              location: location,
              labels: _labels?.build(),
              lists: _lists?.build(),
              cards: _cards?.build(),
              formattedAddress: formattedAddress,
              startDate: startDate,
              permissions: _permissions?.build(),
              loginUserProjectPermission: _loginUserProjectPermission?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'subProjects';
        _subProjects?.build();
        _$failedField = 'collaborators';
        _collaborators?.build();
        _$failedField = 'timesheets';
        _timesheets?.build();
        _$failedField = 'idMembers';
        _idMembers?.build();
        _$failedField = 'idCrews';
        _idCrews?.build();

        _$failedField = 'labels';
        _labels?.build();
        _$failedField = 'lists';
        _lists?.build();
        _$failedField = 'cards';
        _cards?.build();

        _$failedField = 'permissions';
        _permissions?.build();
        _$failedField = 'loginUserProjectPermission';
        _loginUserProjectPermission?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
