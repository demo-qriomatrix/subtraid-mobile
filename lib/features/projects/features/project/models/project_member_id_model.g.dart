// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_member_id_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectMemberIdModel> _$projectMemberIdModelSerializer =
    new _$ProjectMemberIdModelSerializer();

class _$ProjectMemberIdModelSerializer
    implements StructuredSerializer<ProjectMemberIdModel> {
  @override
  final Iterable<Type> types = const [
    ProjectMemberIdModel,
    _$ProjectMemberIdModel
  ];
  @override
  final String wireName = 'ProjectMemberIdModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectMemberIdModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.isSubtraid != null) {
      result
        ..add('isSubtraid')
        ..add(serializers.serialize(object.isSubtraid,
            specifiedType: const FullType(bool)));
    }
    if (object.isAvailable != null) {
      result
        ..add('isAvailable')
        ..add(serializers.serialize(object.isAvailable,
            specifiedType: const FullType(bool)));
    }
    if (object.isVerified != null) {
      result
        ..add('isVerified')
        ..add(serializers.serialize(object.isVerified,
            specifiedType: const FullType(bool)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    if (object.selected != null) {
      result
        ..add('selected')
        ..add(serializers.serialize(object.selected,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ProjectMemberIdModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectMemberIdModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isSubtraid':
          result.isSubtraid = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isAvailable':
          result.isAvailable = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isVerified':
          result.isVerified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'selected':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectMemberIdModel extends ProjectMemberIdModel {
  @override
  final String id;
  @override
  final bool isSubtraid;
  @override
  final bool isAvailable;
  @override
  final bool isVerified;
  @override
  final String name;
  @override
  final String img;
  @override
  final bool selected;

  factory _$ProjectMemberIdModel(
          [void Function(ProjectMemberIdModelBuilder) updates]) =>
      (new ProjectMemberIdModelBuilder()..update(updates)).build();

  _$ProjectMemberIdModel._(
      {this.id,
      this.isSubtraid,
      this.isAvailable,
      this.isVerified,
      this.name,
      this.img,
      this.selected})
      : super._();

  @override
  ProjectMemberIdModel rebuild(
          void Function(ProjectMemberIdModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectMemberIdModelBuilder toBuilder() =>
      new ProjectMemberIdModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectMemberIdModel &&
        id == other.id &&
        isSubtraid == other.isSubtraid &&
        isAvailable == other.isAvailable &&
        isVerified == other.isVerified &&
        name == other.name &&
        img == other.img &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), isSubtraid.hashCode),
                        isAvailable.hashCode),
                    isVerified.hashCode),
                name.hashCode),
            img.hashCode),
        selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectMemberIdModel')
          ..add('id', id)
          ..add('isSubtraid', isSubtraid)
          ..add('isAvailable', isAvailable)
          ..add('isVerified', isVerified)
          ..add('name', name)
          ..add('img', img)
          ..add('selected', selected))
        .toString();
  }
}

class ProjectMemberIdModelBuilder
    implements Builder<ProjectMemberIdModel, ProjectMemberIdModelBuilder> {
  _$ProjectMemberIdModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _isSubtraid;
  bool get isSubtraid => _$this._isSubtraid;
  set isSubtraid(bool isSubtraid) => _$this._isSubtraid = isSubtraid;

  bool _isAvailable;
  bool get isAvailable => _$this._isAvailable;
  set isAvailable(bool isAvailable) => _$this._isAvailable = isAvailable;

  bool _isVerified;
  bool get isVerified => _$this._isVerified;
  set isVerified(bool isVerified) => _$this._isVerified = isVerified;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  bool _selected;
  bool get selected => _$this._selected;
  set selected(bool selected) => _$this._selected = selected;

  ProjectMemberIdModelBuilder();

  ProjectMemberIdModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _isSubtraid = _$v.isSubtraid;
      _isAvailable = _$v.isAvailable;
      _isVerified = _$v.isVerified;
      _name = _$v.name;
      _img = _$v.img;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectMemberIdModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectMemberIdModel;
  }

  @override
  void update(void Function(ProjectMemberIdModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectMemberIdModel build() {
    final _$result = _$v ??
        new _$ProjectMemberIdModel._(
            id: id,
            isSubtraid: isSubtraid,
            isAvailable: isAvailable,
            isVerified: isVerified,
            name: name,
            img: img,
            selected: selected);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
