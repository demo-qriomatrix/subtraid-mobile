// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_label_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectLabelModel> _$projectLabelModelSerializer =
    new _$ProjectLabelModelSerializer();

class _$ProjectLabelModelSerializer
    implements StructuredSerializer<ProjectLabelModel> {
  @override
  final Iterable<Type> types = const [ProjectLabelModel, _$ProjectLabelModel];
  @override
  final String wireName = 'ProjectLabelModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ProjectLabelModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.color != null) {
      result
        ..add('color')
        ..add(serializers.serialize(object.color,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ProjectLabelModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectLabelModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'color':
          result.color = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectLabelModel extends ProjectLabelModel {
  @override
  final String name;
  @override
  final String color;
  @override
  final String id;

  factory _$ProjectLabelModel(
          [void Function(ProjectLabelModelBuilder) updates]) =>
      (new ProjectLabelModelBuilder()..update(updates)).build();

  _$ProjectLabelModel._({this.name, this.color, this.id}) : super._();

  @override
  ProjectLabelModel rebuild(void Function(ProjectLabelModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectLabelModelBuilder toBuilder() =>
      new ProjectLabelModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectLabelModel &&
        name == other.name &&
        color == other.color &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, name.hashCode), color.hashCode), id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectLabelModel')
          ..add('name', name)
          ..add('color', color)
          ..add('id', id))
        .toString();
  }
}

class ProjectLabelModelBuilder
    implements Builder<ProjectLabelModel, ProjectLabelModelBuilder> {
  _$ProjectLabelModel _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _color;
  String get color => _$this._color;
  set color(String color) => _$this._color = color;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ProjectLabelModelBuilder();

  ProjectLabelModelBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _color = _$v.color;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectLabelModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectLabelModel;
  }

  @override
  void update(void Function(ProjectLabelModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectLabelModel build() {
    final _$result =
        _$v ?? new _$ProjectLabelModel._(name: name, color: color, id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
