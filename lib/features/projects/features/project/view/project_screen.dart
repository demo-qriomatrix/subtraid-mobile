import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/collaborators/view/collaborators_screen.dart';
import 'package:Subtraid/features/projects/features/project/feataures/project_overview/view/project_overview_screen.dart';
import 'package:Subtraid/features/projects/features/project/feataures/scrum_board/view/scrum_board_screen.dart';
import 'package:Subtraid/features/projects/features/project/feataures/team_members/view/team_members_screen.dart';
import 'package:Subtraid/features/projects/features/project/feataures/timesheet/view/timesheet_screen.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProjectScreen extends StatefulWidget {
  final String projectId;

  ProjectScreen({@required this.projectId});

  @override
  _ProjectScreenState createState() => _ProjectScreenState();
}

class _ProjectScreenState extends State<ProjectScreen>
    with TickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 5, vsync: this, initialIndex: 0);
    context.read<ProjectBloc>().add(ProjectRequested(
        withoutLoader: false, listItemBloc: null, projectId: widget.projectId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GlobalBloc, GlobalState>(listener: (context, state) {
      if (state is SuccessSnackBarState) {
        if (state.event == SucceessEvents.ProjectDelete) {
          Navigator.of(context).pop();
        }
      }
    }, child: BlocBuilder<ProjectBloc, ProjectState>(builder: (context, state) {
      return Scaffold(
        backgroundColor: ColorConfig.primary,
        // drawer: Drawer(
        //   child: Container(
        //     color: ColorConfig.primary,
        //     child: Column(
        //       children: <Widget>[
        //         SizedBox(
        //           height: MediaQuery.of(context).padding.top,
        //         ),
        //         Expanded(
        //           child: Column(
        //             children: <Widget>[
        //               Flexible(
        //                 fit: FlexFit.tight,
        //                 flex: 1,
        //                 child: Column(
        //                   children: <Widget>[
        //                     Row(
        //                       mainAxisAlignment: MainAxisAlignment.end,
        //                       children: <Widget>[
        //                         RawMaterialButton(
        //                           constraints: BoxConstraints(),
        //                           shape: CircleBorder(),
        //                           child: Icon(
        //                             Icons.close,
        //                             color: Colors.white,
        //                             size: 28,
        //                           ),
        //                           onPressed: () {
        //                             Navigator.of(context).pop();
        //                           },
        //                         )
        //                       ],
        //                     ),
        //                   ],
        //                 ),
        //               ),
        //               Flexible(
        //                   fit: FlexFit.tight,
        //                   flex: 6,
        //                   child: Column(
        //                     crossAxisAlignment: CrossAxisAlignment.start,
        //                     children: <Widget>[
        //                       Padding(
        //                         padding: const EdgeInsets.all(15),
        //                         child: Text(
        //                           'Project Applications',
        //                           style: TextStyle(
        //                               color: ColorConfig.orange, fontSize: 16),
        //                         ),
        //                       ),
        //                       DrawerItem(
        //                         selected: state.selectedIndex == 0,
        //                         callback: () {
        //                           context
        //                               .read<ProjectBloc>()
        //                               .add(ProjectChangeIndex(index: 0));
        //                         },
        //                         icon: LocalImages.chessQueen,
        //                         title: 'Scrum Board',
        //                       ),
        //                       DrawerItem(
        //                         selected: state.selectedIndex == 1,
        //                         callback: () {
        //                           context
        //                               .read<ProjectBloc>()
        //                               .add(ProjectChangeIndex(index: 1));
        //                         },
        //                         icon: LocalImages.projectDiagram,
        //                         title: 'Project Overview',
        //                       ),
        //                       DrawerItem(
        //                         selected: state.selectedIndex == 2,
        //                         callback: () {
        //                           context
        //                               .read<ProjectBloc>()
        //                               .add(ProjectChangeIndex(index: 2));
        //                         },
        //                         icon: LocalImages.briefcase,
        //                         title: 'Collaborators',
        //                       ),
        //                       DrawerItem(
        //                         selected: state.selectedIndex == 3,
        //                         callback: () {
        //                           context
        //                               .read<ProjectBloc>()
        //                               .add(ProjectChangeIndex(index: 3));
        //                         },
        //                         icon: LocalImages.usersCog,
        //                         title: 'Team Members',
        //                       ),
        //                       DrawerItem(
        //                         selected: state.selectedIndex == 4,
        //                         callback: () {
        //                           context
        //                               .read<ProjectBloc>()
        //                               .add(ProjectChangeIndex(index: 4));
        //                         },
        //                         icon: LocalImages.history,
        //                         title: 'Timesheet',
        //                       ),
        //                     ],
        //                   ))
        //             ],
        //           ),
        //         )
        //       ],
        //     ),
        //   ),
        // ),

        body: Column(
          children: [
            StAppBar(
              // leading: Row(
              //   children: [
              //     BackButton(),
              //     // IconButton(icon: Icon(Icons.menu), onPressed: () {})
              //   ],
              // ),
              title: (state is ProjectLoadSuccess)
                  ? (state.projectModel.name)
                  : '',
            ),
            TabBar(
                controller: tabController,
                indicator: UnderlineTabIndicator(
                    borderSide:
                        BorderSide(color: ColorConfig.orange, width: 3)),
                indicatorColor: ColorConfig.orange,
                indicatorSize: TabBarIndicatorSize.tab,
                isScrollable: true,
                tabs: [
                  'Scrum Board',
                  'Project Overview',
                  'Collaborators',
                  'Team Members',
                  'Timesheets'
                ]
                    .map((e) => Tab(
                          text: e,
                        ))
                    .toList()),
            SizedBox(
              height: 5,
            ),
            Expanded(
                child: TabBarView(
              controller: tabController,
              children: [
                ScrumBoardWidget(),
                ProjectOverviewScreen(),
                CollaboratorsScreen(),
                TeamMembersScreen(),
                TimesheetScreen(),
              ],
            )),
          ],
        ),

        // body: Column(
        //   children: [
        //     Expanded(
        //         child: [
        //       ScrumBoardWidget(),
        //       ProjectOverviewScreen(),
        //       CollaboratorsScreen(),
        //       TeamMembersScreen(),
        //       TimesheetScreen(),
        //     ][state.selectedIndex]),
        //   ],
        // ),
      );
    }));
  }
}
