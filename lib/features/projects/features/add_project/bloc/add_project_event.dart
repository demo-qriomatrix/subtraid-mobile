part of 'add_project_bloc.dart';

abstract class AddProjectEvent extends Equatable {
  const AddProjectEvent();

  @override
  List<Object> get props => [];
}

class AddProjectIndexChanged extends AddProjectEvent {
  final int index;

  AddProjectIndexChanged({@required this.index});
}

class AddNewProject extends AddProjectEvent {
  final CreateProjectModel createProjectModel;

  AddNewProject({
    @required this.createProjectModel,
  });
}
