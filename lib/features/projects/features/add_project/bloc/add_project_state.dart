part of 'add_project_bloc.dart';

abstract class AddProjectState extends Equatable {
  final int selectedIndex;

  AddProjectState({
    @required this.selectedIndex,
  });

  @override
  List<Object> get props => [
        selectedIndex,
      ];
}

class AddProjectInitial extends AddProjectState {
  AddProjectInitial({@required int selectedIndex})
      : super(selectedIndex: selectedIndex);
}
