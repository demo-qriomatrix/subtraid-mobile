import 'dart:async';
import 'dart:convert';

import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_model.dart';
import 'package:Subtraid/features/projects/features/project_list/repository/project_list_repository.dart';

part 'add_project_event.dart';
part 'add_project_state.dart';

class AddProjectBloc extends Bloc<AddProjectEvent, AddProjectState> {
  AddProjectBloc({@required this.projectsRepository, @required this.globalBloc})
      : super(AddProjectInitial(selectedIndex: 1));

  final ProjectsRepository projectsRepository;
  final GlobalBloc globalBloc;

  @override
  Stream<AddProjectState> mapEventToState(
    AddProjectEvent event,
  ) async* {
    if (event is AddProjectIndexChanged) {
      yield* _mapAddProjectIndexChangedToState(event.index);
    }

    if (event is AddNewProject) {
      yield* _mapAddNewProjectToState(event.createProjectModel);
    }
  }

  Stream<AddProjectState> _mapAddProjectIndexChangedToState(int index) async* {
    yield AddProjectInitial(selectedIndex: index);
  }

  Stream<AddProjectState> _mapAddNewProjectToState(
      CreateProjectModel data) async* {
    globalBloc.add(ShowLoadingWidget());
    try {
      await projectsRepository.createProject(data);

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Project added successfully',
          event: SucceessEvents.AddProject));
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }

    // yield AddProjectInitial(selectedIndex: index);
  }
}
