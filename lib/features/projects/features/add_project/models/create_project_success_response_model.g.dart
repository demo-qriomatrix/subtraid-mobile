// GENERATED CODE - DO NOT MODIFY BY HAND

part of create_project_success_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CreateProjectSuccessResponseModel>
    _$createProjectSuccessResponseModelSerializer =
    new _$CreateProjectSuccessResponseModelSerializer();

class _$CreateProjectSuccessResponseModelSerializer
    implements StructuredSerializer<CreateProjectSuccessResponseModel> {
  @override
  final Iterable<Type> types = const [
    CreateProjectSuccessResponseModel,
    _$CreateProjectSuccessResponseModel
  ];
  @override
  final String wireName = 'CreateProjectSuccessResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CreateProjectSuccessResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.board != null) {
      result
        ..add('board')
        ..add(serializers.serialize(object.board,
            specifiedType: const FullType(CreateProjectSuccuessModel)));
    }
    return result;
  }

  @override
  CreateProjectSuccessResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreateProjectSuccessResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'board':
          result.board.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CreateProjectSuccuessModel))
              as CreateProjectSuccuessModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CreateProjectSuccessResponseModel
    extends CreateProjectSuccessResponseModel {
  @override
  final CreateProjectSuccuessModel board;

  factory _$CreateProjectSuccessResponseModel(
          [void Function(CreateProjectSuccessResponseModelBuilder) updates]) =>
      (new CreateProjectSuccessResponseModelBuilder()..update(updates)).build();

  _$CreateProjectSuccessResponseModel._({this.board}) : super._();

  @override
  CreateProjectSuccessResponseModel rebuild(
          void Function(CreateProjectSuccessResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateProjectSuccessResponseModelBuilder toBuilder() =>
      new CreateProjectSuccessResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateProjectSuccessResponseModel && board == other.board;
  }

  @override
  int get hashCode {
    return $jf($jc(0, board.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateProjectSuccessResponseModel')
          ..add('board', board))
        .toString();
  }
}

class CreateProjectSuccessResponseModelBuilder
    implements
        Builder<CreateProjectSuccessResponseModel,
            CreateProjectSuccessResponseModelBuilder> {
  _$CreateProjectSuccessResponseModel _$v;

  CreateProjectSuccuessModelBuilder _board;
  CreateProjectSuccuessModelBuilder get board =>
      _$this._board ??= new CreateProjectSuccuessModelBuilder();
  set board(CreateProjectSuccuessModelBuilder board) => _$this._board = board;

  CreateProjectSuccessResponseModelBuilder();

  CreateProjectSuccessResponseModelBuilder get _$this {
    if (_$v != null) {
      _board = _$v.board?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateProjectSuccessResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CreateProjectSuccessResponseModel;
  }

  @override
  void update(void Function(CreateProjectSuccessResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateProjectSuccessResponseModel build() {
    _$CreateProjectSuccessResponseModel _$result;
    try {
      _$result = _$v ??
          new _$CreateProjectSuccessResponseModel._(board: _board?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'board';
        _board?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CreateProjectSuccessResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
