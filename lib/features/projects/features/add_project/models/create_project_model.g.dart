// GENERATED CODE - DO NOT MODIFY BY HAND

part of create_project_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CreateProjectModel> _$createProjectModelSerializer =
    new _$CreateProjectModelSerializer();

class _$CreateProjectModelSerializer
    implements StructuredSerializer<CreateProjectModel> {
  @override
  final Iterable<Type> types = const [CreateProjectModel, _$CreateProjectModel];
  @override
  final String wireName = 'CreateProjectModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CreateProjectModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.formattedAddress != null) {
      result
        ..add('formattedAddress')
        ..add(serializers.serialize(object.formattedAddress,
            specifiedType: const FullType(String)));
    }
    if (object.templateId != null) {
      result
        ..add('templateId')
        ..add(serializers.serialize(object.templateId,
            specifiedType: const FullType(String)));
    }
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(LocationModel)));
    }
    return result;
  }

  @override
  CreateProjectModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreateProjectModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'formattedAddress':
          result.formattedAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'templateId':
          result.templateId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'location':
          result.location.replace(serializers.deserialize(value,
              specifiedType: const FullType(LocationModel)) as LocationModel);
          break;
      }
    }

    return result.build();
  }
}

class _$CreateProjectModel extends CreateProjectModel {
  @override
  final String name;
  @override
  final String formattedAddress;
  @override
  final String templateId;
  @override
  final LocationModel location;

  factory _$CreateProjectModel(
          [void Function(CreateProjectModelBuilder) updates]) =>
      (new CreateProjectModelBuilder()..update(updates)).build();

  _$CreateProjectModel._(
      {this.name, this.formattedAddress, this.templateId, this.location})
      : super._();

  @override
  CreateProjectModel rebuild(
          void Function(CreateProjectModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateProjectModelBuilder toBuilder() =>
      new CreateProjectModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateProjectModel &&
        name == other.name &&
        formattedAddress == other.formattedAddress &&
        templateId == other.templateId &&
        location == other.location;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, name.hashCode), formattedAddress.hashCode),
            templateId.hashCode),
        location.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateProjectModel')
          ..add('name', name)
          ..add('formattedAddress', formattedAddress)
          ..add('templateId', templateId)
          ..add('location', location))
        .toString();
  }
}

class CreateProjectModelBuilder
    implements Builder<CreateProjectModel, CreateProjectModelBuilder> {
  _$CreateProjectModel _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _formattedAddress;
  String get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  String _templateId;
  String get templateId => _$this._templateId;
  set templateId(String templateId) => _$this._templateId = templateId;

  LocationModelBuilder _location;
  LocationModelBuilder get location =>
      _$this._location ??= new LocationModelBuilder();
  set location(LocationModelBuilder location) => _$this._location = location;

  CreateProjectModelBuilder();

  CreateProjectModelBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _formattedAddress = _$v.formattedAddress;
      _templateId = _$v.templateId;
      _location = _$v.location?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateProjectModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CreateProjectModel;
  }

  @override
  void update(void Function(CreateProjectModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateProjectModel build() {
    _$CreateProjectModel _$result;
    try {
      _$result = _$v ??
          new _$CreateProjectModel._(
              name: name,
              formattedAddress: formattedAddress,
              templateId: templateId,
              location: _location?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'location';
        _location?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CreateProjectModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
