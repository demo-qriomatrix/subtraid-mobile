library create_project_success_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'create_project_success_model.g.dart';

abstract class CreateProjectSuccuessModel
    implements
        Built<CreateProjectSuccuessModel, CreateProjectSuccuessModelBuilder> {
  // @nullable
  // SettingsModel get settings;

  // @nullable
  // List<Null> subprojects;

  // @nullable
  // List<Null> collaborators;

  // @nullable
  // List<Null> timesheets;

  // @nullable
  // String status;

  // @nullable
  // List<Null> idMembers;

  // @nullable
  // List<Null> idCrews;

  // @nullable
  // List<Null> managers;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  // @nullable
  // String author;

  // @nullable
  // String owner;

  // @nullable
  // List<Null> labels;

  // @nullable
  // List<Null> lists;

  // @nullable
  // List<Null> cards;

  // @nullable
  // String dateCreated;

  // @nullable
  // String startDate;

  // @nullable
  // List<Null> permissions;

  CreateProjectSuccuessModel._();

  factory CreateProjectSuccuessModel(
          [updates(CreateProjectSuccuessModelBuilder b)]) =
      _$CreateProjectSuccuessModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CreateProjectSuccuessModel.serializer, this));
  }

  static CreateProjectSuccuessModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CreateProjectSuccuessModel.serializer, json.decode(jsonString));
  }

  static Serializer<CreateProjectSuccuessModel> get serializer =>
      _$createProjectSuccuessModelSerializer;
}
