library location_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'location_model.g.dart';

abstract class LocationModel
    implements Built<LocationModel, LocationModelBuilder> {
  @nullable
  double get lat;

  @nullable
  double get lng;

  LocationModel._();

  factory LocationModel([updates(LocationModelBuilder b)]) = _$LocationModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(LocationModel.serializer, this));
  }

  static LocationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        LocationModel.serializer, json.decode(jsonString));
  }

  static Serializer<LocationModel> get serializer => _$locationModelSerializer;
}
