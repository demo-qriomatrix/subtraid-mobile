library settings_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'settings_model.g.dart';

abstract class SettingsModel
    implements Built<SettingsModel, SettingsModelBuilder> {
  // @nullable
  // bool get subscribed;

  // @nullable
  // bool get cardCoverImages;

  SettingsModel._();

  factory SettingsModel([updates(SettingsModelBuilder b)]) = _$SettingsModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(SettingsModel.serializer, this));
  }

  static SettingsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        SettingsModel.serializer, json.decode(jsonString));
  }

  static Serializer<SettingsModel> get serializer => _$settingsModelSerializer;
}
