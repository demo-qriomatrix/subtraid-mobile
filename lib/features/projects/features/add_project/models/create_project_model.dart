library create_project_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'location_model.dart';

part 'create_project_model.g.dart';

abstract class CreateProjectModel
    implements Built<CreateProjectModel, CreateProjectModelBuilder> {
  @nullable
  String get name;

  @nullable
  String get formattedAddress;

  @nullable
  String get templateId;

  @nullable
  LocationModel get location;

  CreateProjectModel._();

  factory CreateProjectModel([updates(CreateProjectModelBuilder b)]) =
      _$CreateProjectModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CreateProjectModel.serializer, this));
  }

  static CreateProjectModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CreateProjectModel.serializer, json.decode(jsonString));
  }

  static Serializer<CreateProjectModel> get serializer =>
      _$createProjectModelSerializer;
}
