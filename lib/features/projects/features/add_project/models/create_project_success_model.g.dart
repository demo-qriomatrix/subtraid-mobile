// GENERATED CODE - DO NOT MODIFY BY HAND

part of create_project_success_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CreateProjectSuccuessModel> _$createProjectSuccuessModelSerializer =
    new _$CreateProjectSuccuessModelSerializer();

class _$CreateProjectSuccuessModelSerializer
    implements StructuredSerializer<CreateProjectSuccuessModel> {
  @override
  final Iterable<Type> types = const [
    CreateProjectSuccuessModel,
    _$CreateProjectSuccuessModel
  ];
  @override
  final String wireName = 'CreateProjectSuccuessModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CreateProjectSuccuessModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CreateProjectSuccuessModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreateProjectSuccuessModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CreateProjectSuccuessModel extends CreateProjectSuccuessModel {
  @override
  final String id;
  @override
  final String name;

  factory _$CreateProjectSuccuessModel(
          [void Function(CreateProjectSuccuessModelBuilder) updates]) =>
      (new CreateProjectSuccuessModelBuilder()..update(updates)).build();

  _$CreateProjectSuccuessModel._({this.id, this.name}) : super._();

  @override
  CreateProjectSuccuessModel rebuild(
          void Function(CreateProjectSuccuessModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateProjectSuccuessModelBuilder toBuilder() =>
      new CreateProjectSuccuessModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateProjectSuccuessModel &&
        id == other.id &&
        name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateProjectSuccuessModel')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class CreateProjectSuccuessModelBuilder
    implements
        Builder<CreateProjectSuccuessModel, CreateProjectSuccuessModelBuilder> {
  _$CreateProjectSuccuessModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  CreateProjectSuccuessModelBuilder();

  CreateProjectSuccuessModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateProjectSuccuessModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CreateProjectSuccuessModel;
  }

  @override
  void update(void Function(CreateProjectSuccuessModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateProjectSuccuessModel build() {
    final _$result =
        _$v ?? new _$CreateProjectSuccuessModel._(id: id, name: name);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
