library create_project_success_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_success_model.dart';

part 'create_project_success_response_model.g.dart';

abstract class CreateProjectSuccessResponseModel
    implements
        Built<CreateProjectSuccessResponseModel,
            CreateProjectSuccessResponseModelBuilder> {
  @nullable
  CreateProjectSuccuessModel get board;

  CreateProjectSuccessResponseModel._();

  factory CreateProjectSuccessResponseModel(
          [updates(CreateProjectSuccessResponseModelBuilder b)]) =
      _$CreateProjectSuccessResponseModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        CreateProjectSuccessResponseModel.serializer, this));
  }

  static CreateProjectSuccessResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CreateProjectSuccessResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<CreateProjectSuccessResponseModel> get serializer =>
      _$createProjectSuccessResponseModelSerializer;
}
