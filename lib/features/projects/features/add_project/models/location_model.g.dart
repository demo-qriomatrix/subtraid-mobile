// GENERATED CODE - DO NOT MODIFY BY HAND

part of location_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LocationModel> _$locationModelSerializer =
    new _$LocationModelSerializer();

class _$LocationModelSerializer implements StructuredSerializer<LocationModel> {
  @override
  final Iterable<Type> types = const [LocationModel, _$LocationModel];
  @override
  final String wireName = 'LocationModel';

  @override
  Iterable<Object> serialize(Serializers serializers, LocationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.lat != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.lat,
            specifiedType: const FullType(double)));
    }
    if (object.lng != null) {
      result
        ..add('lng')
        ..add(serializers.serialize(object.lng,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  LocationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LocationModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'lng':
          result.lng = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$LocationModel extends LocationModel {
  @override
  final double lat;
  @override
  final double lng;

  factory _$LocationModel([void Function(LocationModelBuilder) updates]) =>
      (new LocationModelBuilder()..update(updates)).build();

  _$LocationModel._({this.lat, this.lng}) : super._();

  @override
  LocationModel rebuild(void Function(LocationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LocationModelBuilder toBuilder() => new LocationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LocationModel && lat == other.lat && lng == other.lng;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, lat.hashCode), lng.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LocationModel')
          ..add('lat', lat)
          ..add('lng', lng))
        .toString();
  }
}

class LocationModelBuilder
    implements Builder<LocationModel, LocationModelBuilder> {
  _$LocationModel _$v;

  double _lat;
  double get lat => _$this._lat;
  set lat(double lat) => _$this._lat = lat;

  double _lng;
  double get lng => _$this._lng;
  set lng(double lng) => _$this._lng = lng;

  LocationModelBuilder();

  LocationModelBuilder get _$this {
    if (_$v != null) {
      _lat = _$v.lat;
      _lng = _$v.lng;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LocationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LocationModel;
  }

  @override
  void update(void Function(LocationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LocationModel build() {
    final _$result = _$v ?? new _$LocationModel._(lat: lat, lng: lng);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
