// GENERATED CODE - DO NOT MODIFY BY HAND

part of settings_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SettingsModel> _$settingsModelSerializer =
    new _$SettingsModelSerializer();

class _$SettingsModelSerializer implements StructuredSerializer<SettingsModel> {
  @override
  final Iterable<Type> types = const [SettingsModel, _$SettingsModel];
  @override
  final String wireName = 'SettingsModel';

  @override
  Iterable<Object> serialize(Serializers serializers, SettingsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    return <Object>[];
  }

  @override
  SettingsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    return new SettingsModelBuilder().build();
  }
}

class _$SettingsModel extends SettingsModel {
  factory _$SettingsModel([void Function(SettingsModelBuilder) updates]) =>
      (new SettingsModelBuilder()..update(updates)).build();

  _$SettingsModel._() : super._();

  @override
  SettingsModel rebuild(void Function(SettingsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SettingsModelBuilder toBuilder() => new SettingsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SettingsModel;
  }

  @override
  int get hashCode {
    return 464819441;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('SettingsModel').toString();
  }
}

class SettingsModelBuilder
    implements Builder<SettingsModel, SettingsModelBuilder> {
  _$SettingsModel _$v;

  SettingsModelBuilder();

  @override
  void replace(SettingsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SettingsModel;
  }

  @override
  void update(void Function(SettingsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SettingsModel build() {
    final _$result = _$v ?? new _$SettingsModel._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
