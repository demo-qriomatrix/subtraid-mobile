import 'dart:async';

import 'package:Subtraid/features/projects/features/templates/bloc/templates_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:rxdart/rxdart.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/config.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/add_project/bloc/add_project_bloc.dart';
import 'package:Subtraid/features/projects/features/add_project/models/create_project_model.dart';
import 'package:Subtraid/features/projects/features/add_project/models/location_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';

class AddProjectScreen extends StatefulWidget {
  @override
  _AddProjectScreenState createState() => _AddProjectScreenState();
}

class _AddProjectScreenState extends State<AddProjectScreen> {
  String template;

  TextEditingController boardNameController = TextEditingController();

  bool autoValidate = false;
  bool submitted = false;

  TextEditingController locationController = TextEditingController();

  GoogleMapsPlaces _places;

  Future<void> _initPlaces() async {
    _places = GoogleMapsPlaces(
      apiKey: kGoogleApiKey,
      apiHeaders: await GoogleApiHeaders().getHeaders(),
    );

    locationController.addListener(_onQueryChange);
  }

  PlacesAutocompleteResponse _response;
  final _queryBehavior = BehaviorSubject<String>.seeded('');
  // bool _searching;
  Timer _debounce;

  @override
  void initState() {
    super.initState();
    _initPlaces();
  }

  void _onQueryChange() {
    // if (_debounce?.isActive ?? false) _debounce.cancel();
    // _debounce = Timer(Duration(milliseconds: 500), () {
    //   if (!_queryBehavior.isClosed) {
    //     _queryBehavior.add(locationController.text);
    //   }
    // });

    // locationController.addListener(doSearch);
  }

  LocationModel location;

  getLocation(Prediction p) async {
    try {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      location = LocationModel((location) => location
        ..lat = lat
        ..lng = lng);
    } catch (e) {
      location = null;
    }
  }

  Future<Null> doSearch(String value) async {
    if (mounted && value.isNotEmpty && _places != null) {
      if (selectedPrediction != null &&
          selectedPrediction.description == value) {
        setState(() {
          _response = null;
        });
      } else {
        setState(() {
          // _searching = true;
        });

        final res = await _places.autocomplete(
          value,
          language: "en",
          // sessionToken: Uuid().generateV4(),
          // components: [Component(Component.country, "uk")],
        );

        if (res.errorMessage?.isNotEmpty == true ||
            res.status == "REQUEST_DENIED") {
          onResponseError(res);
        } else {
          onResponse(res);
        }
      }
    } else {
      onResponse(null);
    }
  }

  void onResponseError(PlacesAutocompleteResponse res) {
    if (!mounted) return;

    setState(() {
      _response = null;
      // _searching = false;
    });
  }

  void onResponse(PlacesAutocompleteResponse res) {
    if (!mounted) return;

    setState(() {
      _response = res;
      // _searching = false;
    });
  }

  @override
  void dispose() {
    super.dispose();

    _places.dispose();
    if (_debounce != null) _debounce.cancel();
    _queryBehavior.close();
    locationController.removeListener(_onQueryChange);
  }

  Prediction selectedPrediction;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConfig.primary,
      child: Column(
        children: [
          StAppBar(
            title: 'Add Project',
          ),
          BlocProvider<AddProjectBloc>(
            create: (context) => AddProjectBloc(
                projectsRepository: sl(),
                globalBloc: context.read<GlobalBloc>()),
            lazy: false,
            child: Expanded(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  BlocBuilder<AddProjectBloc, AddProjectState>(
                      builder: (context, state) {
                    return Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(50),
                                topRight: Radius.circular(50))),
                        child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minHeight: getMinHeight(context),
                            ),
                            child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: Container(
                                    padding: EdgeInsets.symmetric(vertical: 20),
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 20, horizontal: 15),
                                      decoration: BoxDecoration(
                                          color: ColorConfig.blue4,
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      child: Form(
                                        key: _formKey,
                                        autovalidateMode: autoValidate
                                            ? AutovalidateMode.always
                                            : AutovalidateMode.disabled,
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    'Create Project',
                                                    style: TextStyle(
                                                        color:
                                                            ColorConfig.primary,
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w700),
                                                  )
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Radio(
                                                    materialTapTargetSize:
                                                        MaterialTapTargetSize
                                                            .shrinkWrap,
                                                    activeColor:
                                                        ColorConfig.primary,
                                                    groupValue:
                                                        state.selectedIndex,
                                                    onChanged: (val) {
                                                      context
                                                          .read<
                                                              AddProjectBloc>()
                                                          .add(
                                                              AddProjectIndexChanged(
                                                                  index: val));
                                                    },
                                                    value: 0,
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      context
                                                          .read<
                                                              AddProjectBloc>()
                                                          .add(
                                                              AddProjectIndexChanged(
                                                                  index: 0));
                                                    },
                                                    child: Text(
                                                      'Create board from Saved template',
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Radio(
                                                    materialTapTargetSize:
                                                        MaterialTapTargetSize
                                                            .shrinkWrap,
                                                    activeColor:
                                                        ColorConfig.primary,
                                                    groupValue:
                                                        state.selectedIndex,
                                                    onChanged: (val) {
                                                      context
                                                          .read<
                                                              AddProjectBloc>()
                                                          .add(
                                                              AddProjectIndexChanged(
                                                                  index: val));
                                                    },
                                                    value: 1,
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      context
                                                          .read<
                                                              AddProjectBloc>()
                                                          .add(
                                                              AddProjectIndexChanged(
                                                                  index: 1));
                                                    },
                                                    child: Text(
                                                      'Create custom board',
                                                      style: TextStyle(
                                                          fontSize: 13,
                                                          color: Colors.black),
                                                    ),
                                                  ),
                                                ],
                                              ),

                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text('Project Name',
                                                      style: textStyle1),
                                                  SizedBox(
                                                    height: height2,
                                                  ),
                                                  TextFormField(
                                                    validator: (val) {
                                                      if (val.isEmpty) {
                                                        return 'Required';
                                                      }

                                                      return null;
                                                    },
                                                    controller:
                                                        boardNameController,
                                                    decoration: profileInputDecoration
                                                        .copyWith(
                                                            hintText:
                                                                'Type Project Name',
                                                            hintStyle: TextStyle(
                                                                color: ColorConfig
                                                                    .black1)),
                                                    maxLines: null,
                                                    style: textStyle2,
                                                  ),
                                                ],
                                              ),
                                              state.selectedIndex == 0
                                                  ? Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        SizedBox(
                                                          height: height2,
                                                        ),
                                                        Text(
                                                            'Select Saved Template',
                                                            style: textStyle1),
                                                        SizedBox(
                                                          height: height2,
                                                        ),
                                                        BlocProvider<
                                                            TemplatesBloc>(
                                                          create: (context) =>
                                                              TemplatesBloc(
                                                                  templatesRepository:
                                                                      sl()),
                                                          child: BlocConsumer<
                                                              TemplatesBloc,
                                                              TemplatesState>(
                                                            listener: (context,
                                                                state) {
                                                              if (state
                                                                  is TemplatesLoadSuccess) {}
                                                            },
                                                            builder: (context,
                                                                state) {
                                                              return DropdownButtonFormField(
                                                                validator:
                                                                    (val) {
                                                                  if (val ==
                                                                          null ||
                                                                      val.isEmpty) {
                                                                    return 'Required';
                                                                  }

                                                                  return null;
                                                                },
                                                                dropdownColor:
                                                                    ColorConfig
                                                                        .primary,
                                                                decoration: profileInputDecoration.copyWith(
                                                                    hintText:
                                                                        'Select template',
                                                                    hintStyle: TextStyle(
                                                                        color: ColorConfig
                                                                            .black1,
                                                                        fontSize:
                                                                            14,
                                                                        fontWeight:
                                                                            FontWeight.w500)),
                                                                value: template,
                                                                items: (state
                                                                        is TemplatesLoadSuccess)
                                                                    ? state
                                                                        .templates
                                                                        .map((e) =>
                                                                            DropdownMenuItem(
                                                                              value: e.id,
                                                                              child: Text(
                                                                                e.name,
                                                                                style: TextStyle(fontSize: 14, color: Colors.white),
                                                                              ),
                                                                            ))
                                                                        .toList()
                                                                    : [],
                                                                selectedItemBuilder: (context) => (state
                                                                        is TemplatesLoadSuccess)
                                                                    ? state
                                                                        .templates
                                                                        .map((e) =>
                                                                            DropdownMenuItem(
                                                                              value: e.id,
                                                                              child: Text(
                                                                                e.name,
                                                                                style: TextStyle(fontSize: 14, color: Colors.black),
                                                                              ),
                                                                            ))
                                                                        .toList()
                                                                    : [],
                                                                onChanged:
                                                                    (val) {
                                                                  template =
                                                                      val;
                                                                },
                                                              );
                                                            },
                                                          ),
                                                        )
                                                      ],
                                                    )
                                                  : Container(),

                                              SizedBox(
                                                height: height2,
                                              ),
                                              Text('Address',
                                                  style: textStyle1),
                                              SizedBox(
                                                height: height2,
                                              ),
                                              TextFormField(
                                                validator: (val) {
                                                  if (val.isEmpty) {
                                                    return 'Required';
                                                  }

                                                  return null;
                                                },
                                                controller: locationController,
                                                decoration: profileInputDecoration
                                                    .copyWith(
                                                        hintText:
                                                            'Search Location',
                                                        hintStyle: TextStyle(
                                                            color: ColorConfig
                                                                .black1)),
                                                maxLines: null,
                                                style: textStyle2,
                                                onChanged: (value) {
                                                  doSearch(value);
                                                },
                                              ),
                                              _response?.predictions
                                                          ?.isNotEmpty ??
                                                      false
                                                  ? SingleChildScrollView(
                                                      child: Material(
                                                        color:
                                                            ColorConfig.blue1,
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  20),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  20),
                                                        ),
                                                        // color: theme.dialogBackgroundColor,
                                                        child: ListBody(
                                                          children:
                                                              _response
                                                                  .predictions
                                                                  .map(
                                                                    (p) =>
                                                                        PredictionTile(
                                                                      prediction:
                                                                          p,
                                                                      onTap:
                                                                          (value) {
                                                                        selectedPrediction =
                                                                            value;

                                                                        getLocation(
                                                                            p);

                                                                        locationController.text =
                                                                            value.description;

                                                                        setState(
                                                                            () {
                                                                          _response =
                                                                              null;
                                                                          print(
                                                                              'setting null');
                                                                        });

                                                                        FocusScope.of(context)
                                                                            .requestFocus(new FocusNode());

                                                                        // locationController
                                                                        //         .selection =
                                                                        //     new TextSelection(
                                                                        //   baseOffset: 0,
                                                                        //   extentOffset:
                                                                        //       locationController
                                                                        //               .text
                                                                        //               ?.length ??
                                                                        //           0,
                                                                        // );
                                                                      },
                                                                    ),
                                                                  )
                                                                  .toList(),
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                              // SizedBox(
                                              //   height: 10,
                                              // ),
                                              // Row(
                                              //   children: <Widget>[
                                              //     Checkbox(
                                              //       value: false,
                                              //       onChanged: (val) {},
                                              //     ),
                                              //     Text(
                                              //       'Save as template',
                                              //       style: TextStyle(
                                              //           fontSize: 13,
                                              //           color: ColorConfig.black2,
                                              //           fontWeight:
                                              //               FontWeight.w400),
                                              //     )
                                              //   ],
                                              // ),
                                              SizedBox(
                                                height: height2,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: <Widget>[
                                                  CancelButton(
                                                    onPress: () {},
                                                  ),
                                                  SaveButton('Create', () {
                                                    setState(() {
                                                      autoValidate = true;
                                                    });
                                                    if (_formKey.currentState
                                                        .validate()) {
                                                      if (!submitted) {
                                                        submitted = true;

                                                        CreateProjectModel data = CreateProjectModel(
                                                            (model) => model
                                                              ..templateId =
                                                                  state.selectedIndex ==
                                                                          0
                                                                      ? template
                                                                      : null
                                                              ..name =
                                                                  boardNameController
                                                                      .text
                                                              ..location =
                                                                  location
                                                                      .toBuilder()
                                                              ..formattedAddress =
                                                                  selectedPrediction
                                                                      .description);

                                                        context
                                                            .read<
                                                                AddProjectBloc>()
                                                            .add(AddNewProject(
                                                                createProjectModel:
                                                                    data));
                                                      }
                                                    }
                                                  }),
                                                ],
                                              ),
                                            ]),
                                      ),
                                    )))));
                  })
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class PredictionTile extends StatelessWidget {
  final Prediction prediction;
  final ValueChanged<Prediction> onTap;

  PredictionTile({@required this.prediction, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(prediction.description),
      onTap: () {
        if (onTap != null) {
          onTap(prediction);
        }
      },
    );
  }
}
