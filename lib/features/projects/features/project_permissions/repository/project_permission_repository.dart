import 'dart:convert';

import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_update_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/company_info_response_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/ppg_post_model.dart';

class ProjectPermissionRepository {
  final HttpClient httpClient;

  ProjectPermissionRepository({@required this.httpClient});

  Future<CompanyInfoResponseModel> getProjectPermissions() async {
    Response response =
        await httpClient.dio.get(EndpointConfig.userEmployerPpg);

    return CompanyInfoResponseModel.fromJson(json.encode(response.data));
  }

  Future<Response> createProjectGroupPermission(
      PpgPostModel permissonGroupModel) async {
    Response response = await httpClient.dio.patch(
        EndpointConfig.companiesProjectGroupPermission + '/create',
        data: permissonGroupModel.toJson());

    return response;
  }

  Future<Response> updateProjectGroupPermission(
      ProjectPermissionUpdateModel permissonGroupModel) async {
    Response response = await httpClient.dio.patch(
        EndpointConfig.companiesProjectGroupPermission + '/update',
        data: permissonGroupModel.toJson());

    return response;
  }
}
