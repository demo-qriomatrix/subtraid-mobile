import 'package:Subtraid/features/projects/features/project_permissions/models/permissions_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_group_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_update_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/bloc/project_permission_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/card_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/list_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/member_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/project_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/sub_project_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/ppg_post_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';

class UserRolesWidget extends StatefulWidget {
  final bool editMode;
  final VoidCallback callback;
  final ProjectPermissionGroupModel permissonGroupModel;

  UserRolesWidget({this.editMode, this.callback, this.permissonGroupModel});

  @override
  _UserRolesWidgetState createState() => _UserRolesWidgetState();
}

class _UserRolesWidgetState extends State<UserRolesWidget> {
  TextEditingController userRoleController = TextEditingController();
  TextEditingController roleDescriptionController = TextEditingController();

  TextEditingController aboutController = TextEditingController(
      text:
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but');
  TextEditingController currentStatusController =
      TextEditingController(text: 'False');

  bool project = false;

  bool listCreate = false;
  bool listDelete = false;
  bool listUpdate = false;
  bool listMove = false;

  bool cardCreate = false;
  bool cardDelete = false;
  bool cardMove = false;
  bool cardGeneralUpdate = false;
  bool cardMarkComplete = false;
  bool cardComment = false;
  bool cardChecklist = false;
  bool cardAttachment = false;

  bool subProjectCreate = false;

  bool memberAdd = false;
  bool memberRemove = false;
  bool memberAssignRole = false;
  bool memberAssigntoCard = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.editMode == true) {
      userRoleController.text = widget.permissonGroupModel.name;
      roleDescriptionController.text = widget.permissonGroupModel.description;

      project = widget.permissonGroupModel.permissions.project.general;

      listCreate = widget.permissonGroupModel.permissions.list.create;
      listDelete = widget.permissonGroupModel.permissions.list.delete;
      listUpdate = widget.permissonGroupModel.permissions.list.updateList;
      listMove = widget.permissonGroupModel.permissions.list.delete;

      cardCreate = widget.permissonGroupModel.permissions.cards.create;
      cardDelete = widget.permissonGroupModel.permissions.cards.delete;
      cardMove = widget.permissonGroupModel.permissions.cards.move;
      cardGeneralUpdate =
          widget.permissonGroupModel.permissions.cards.generalUpdate;
      cardMarkComplete =
          widget.permissonGroupModel.permissions.cards.markComplete;
      cardComment = widget.permissonGroupModel.permissions.cards.comments;
      cardChecklist = widget.permissonGroupModel.permissions.cards.checklist;
      cardAttachment = widget.permissonGroupModel.permissions.cards.attachment;

      subProjectCreate =
          widget.permissonGroupModel.permissions.subproject.create;

      memberAdd = widget.permissonGroupModel.permissions.member.add;
      memberRemove = widget.permissonGroupModel.permissions.member.remove;
      memberAssignRole =
          widget.permissonGroupModel.permissions.member.assignRoles;
      memberAssigntoCard =
          widget.permissonGroupModel.permissions.member.assignToCards;
    }
  }

  void reset() {
    userRoleController.clear();
    roleDescriptionController.clear();

    aboutController.clear();
    currentStatusController.clear();

    setState(() {
      project = false;

      listCreate = false;
      listDelete = false;
      listUpdate = false;
      listMove = false;

      cardCreate = false;
      cardDelete = false;
      cardMove = false;
      cardGeneralUpdate = false;
      cardMarkComplete = false;
      cardComment = false;
      cardChecklist = false;
      cardAttachment = false;

      subProjectCreate = false;

      memberAdd = false;
      memberRemove = false;
      memberAssignRole = false;
      memberAssigntoCard = false;
    });

    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GlobalBloc, GlobalState>(
      listener: (context, state) {
        if (state is SuccessSnackBarState) {
          if (state.event == SucceessEvents.UserRole) {
            widget.callback();
            reset();
          }
        }
      },
      child: ListView(
        physics: widget.editMode
            ? NeverScrollableScrollPhysics()
            : AlwaysScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.zero,
        children: [
          Container(
            margin: widget.editMode
                ? EdgeInsets.zero
                : EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
                color: widget.editMode ? Colors.white : ColorConfig.blue4,
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      widget.editMode ? 'Edit User Role' : 'Create User Role',
                      style: TextStyle(
                          color: ColorConfig.primary,
                          fontSize: 16,
                          fontWeight: FontWeight.w700),
                    ),
                    IconButton(
                        icon: Icon(
                          Icons.close,
                          color: ColorConfig.primary,
                        ),
                        onPressed: () {
                          widget.callback();
                        })
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text('User Role', style: textStyle1),
                SizedBox(
                  height: height2,
                ),
                TextField(
                  controller: userRoleController,
                  decoration: profileInputDecoration.copyWith(
                      fillColor: widget.editMode
                          ? ColorConfig.blue4
                          : ColorConfig.blue2,
                      hintText: 'User Role',
                      hintStyle: TextStyle(color: ColorConfig.black1)),
                  maxLines: null,
                  style: textStyle2,
                ),
                SizedBox(
                  height: height2,
                ),
                Text('Role Description', style: textStyle1),
                SizedBox(
                  height: height2,
                ),
                TextField(
                  controller: roleDescriptionController,
                  decoration: profileInputDecoration.copyWith(
                      fillColor: widget.editMode
                          ? ColorConfig.blue4
                          : ColorConfig.blue2,
                      hintText: 'Text here',
                      hintStyle: TextStyle(color: ColorConfig.black1)),
                  maxLines: null,
                  style: textStyle2,
                ),
                SizedBox(
                  height: 20,
                ),
                CustomWidget1('Projects'),
                Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    // CustomSwitch('General (Name & Description)'),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.project = val;
                            });
                          },
                          value: this.project,
                        ),
                        Text(
                          'General (Name & Description)',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    )
                  ],
                ),
                CustomWidget1('List'),
                Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.listCreate = val;
                            });
                          },
                          value: this.listCreate,
                        ),
                        Text(
                          'Create',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.listDelete = val;
                            });
                          },
                          value: this.listDelete,
                        ),
                        Text(
                          'Delete',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.listUpdate = val;
                            });
                          },
                          value: this.listUpdate,
                        ),
                        Text(
                          'Update',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.listMove = val;
                            });
                          },
                          value: this.listMove,
                        ),
                        Text(
                          'Move',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                  ],
                ),
                CustomWidget1('Card'),
                Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardCreate = val;
                            });
                          },
                          value: this.cardCreate,
                        ),
                        Text(
                          'Create',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardDelete = val;
                            });
                          },
                          value: this.cardDelete,
                        ),
                        Text(
                          'Delete',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardMove = val;
                            });
                          },
                          value: this.cardMove,
                        ),
                        Text(
                          'Move',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardGeneralUpdate = val;
                            });
                          },
                          value: this.cardGeneralUpdate,
                        ),
                        Text(
                          'General Update',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardMarkComplete = val;
                            });
                          },
                          value: this.cardMarkComplete,
                        ),
                        Text(
                          'Mark Complete',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardComment = val;
                            });
                          },
                          value: this.cardComment,
                        ),
                        Text(
                          'Comments',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardChecklist = val;
                            });
                          },
                          value: this.cardChecklist,
                        ),
                        Text(
                          'Checklist',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.cardAttachment = val;
                            });
                          },
                          value: this.cardAttachment,
                        ),
                        Text(
                          'Attachment',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                  ],
                ),
                CustomWidget1('Sub Project'),
                Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.subProjectCreate = val;
                            });
                          },
                          value: this.subProjectCreate,
                        ),
                        Text(
                          'Create',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    )
                  ],
                ),
                CustomWidget1('Member'),
                Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.memberAdd = val;
                            });
                          },
                          value: this.memberAdd,
                        ),
                        Text(
                          'Add',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.memberRemove = val;
                            });
                          },
                          value: this.memberRemove,
                        ),
                        Text(
                          'Remove',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.memberAssignRole = val;
                            });
                          },
                          value: this.memberAssignRole,
                        ),
                        Text(
                          'Assign Role',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Switch(
                          inactiveTrackColor: Color(0xffCDDEF4),
                          onChanged: (val) {
                            setState(() {
                              this.memberAssigntoCard = val;
                            });
                          },
                          value: this.memberAssigntoCard,
                        ),
                        Text(
                          'Assign to Card',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        ),
                        SizedBox(
                          width: 5,
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(
                    // height: height2,
                    ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CancelButton(
                      onPress: () {
                        if (widget.editMode == true) {
                          widget.callback();
                        }
                      },
                    ),
                    SaveButton('Save', () async {
                      if (widget.editMode == true) {
                        CardPermissionModel cardsPermissonModel =
                            CardPermissionModel(
                                (cardsPermissonModel) => cardsPermissonModel
                                  ..attachment = cardAttachment
                                  ..checklist = cardChecklist
                                  ..comments = cardComment
                                  ..create = cardCreate
                                  ..delete = cardDelete
                                  ..move = cardMove
                                  ..generalUpdate = cardGeneralUpdate
                                  ..markComplete = cardMarkComplete);

                        ListPermissionModel listPermissonModel =
                            ListPermissionModel(
                                (listPermissonModel) => listPermissonModel
                                  ..create = listCreate
                                  ..delete = listDelete
                                  ..move = listMove
                                  ..updateList = listUpdate);

                        MemberPermissionModel memberPermissonModel =
                            MemberPermissionModel(
                                (memberPermissonModel) => memberPermissonModel
                                  ..add = memberAdd
                                  ..assignRoles = memberAssignRole
                                  ..assignToCards = memberAssigntoCard
                                  ..remove = memberRemove);

                        ProjectPermissionModel projectPermissonModel =
                            ProjectPermissionModel((projectPermissonModel) =>
                                projectPermissonModel..general = project);

                        SubProjectPermissionModel subprojectPermissonModel =
                            SubProjectPermissionModel(
                                (subprojectPermissonModel) =>
                                    subprojectPermissonModel
                                      ..create = subProjectCreate);

                        PermissionsModel permissions = PermissionsModel(
                            (data) => data
                              ..cards = cardsPermissonModel.toBuilder()
                              ..list = listPermissonModel.toBuilder()
                              ..member = memberPermissonModel.toBuilder()
                              ..project = projectPermissonModel.toBuilder()
                              ..subproject =
                                  subprojectPermissonModel.toBuilder());

                        ProjectPermissionGroupModel ppg =
                            widget.permissonGroupModel.rebuild((data) => data
                              ..description = roleDescriptionController.text
                              ..name = userRoleController.text
                              ..permissions = permissions.toBuilder());

                        ProjectPermissionUpdateModel update =
                            ProjectPermissionUpdateModel(
                                (data) => data..ppgObj = ppg.toBuilder());

                        context.read<ProjectPermissionBloc>().add(
                            UpdateProjectPermissionGroup(
                                permissonGroupModel: update));
                      } else {
                        CardPermissionModel cardsPermissonModel =
                            CardPermissionModel(
                                (cardsPermissonModel) => cardsPermissonModel
                                  ..attachment = cardAttachment
                                  ..checklist = cardChecklist
                                  ..comments = cardComment
                                  ..create = cardCreate
                                  ..delete = cardDelete
                                  ..move = cardMove
                                  ..generalUpdate = cardGeneralUpdate
                                  ..markComplete = cardMarkComplete);

                        ListPermissionModel listPermissonModel =
                            ListPermissionModel(
                                (listPermissonModel) => listPermissonModel
                                  ..create = listCreate
                                  ..delete = listDelete
                                  ..move = listMove
                                  ..updateList = listUpdate);

                        MemberPermissionModel memberPermissonModel =
                            MemberPermissionModel(
                                (memberPermissonModel) => memberPermissonModel
                                  ..add = memberAdd
                                  ..assignRoles = memberAssignRole
                                  ..assignToCards = memberAssigntoCard
                                  ..remove = memberRemove);

                        ProjectPermissionModel projectPermissonModel =
                            ProjectPermissionModel((projectPermissonModel) =>
                                projectPermissonModel..general = project);

                        SubProjectPermissionModel subprojectPermissonModel =
                            SubProjectPermissionModel(
                                (subprojectPermissonModel) =>
                                    subprojectPermissonModel
                                      ..create = subProjectCreate);

                        // PermissionsModel permissonModel =
                        //     PermissionsModel((permissonModel) => permissonModel
                        //       ..cards = cardsPermissonModel.toBuilder()
                        //       ..list = listPermissonModel.toBuilder()
                        //       ..member = memberPermissonModel.toBuilder()
                        //       ..project = projectPermissonModel.toBuilder()
                        //       ..subproject = subprojectPermissonModel.toBuilder());

                        PpgPostModel permissonGroupModel = PpgPostModel(
                            (permissonGroupModel) => permissonGroupModel
                              ..cards = cardsPermissonModel.toBuilder()
                              ..list = listPermissonModel.toBuilder()
                              ..member = memberPermissonModel.toBuilder()
                              ..project = projectPermissonModel.toBuilder()
                              ..subproject =
                                  subprojectPermissonModel.toBuilder()
                              ..description = roleDescriptionController.text
                              ..name = userRoleController.text);

                        context.read<ProjectPermissionBloc>().add(
                            CreateProjectPermissionGroup(
                                permissonGroupModel: permissonGroupModel));
                      }
                    }),
                  ],
                ),
                SizedBox(
                    // height: height2,
                    ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CustomWidget1 extends StatelessWidget {
  final String label;

  const CustomWidget1(this.label);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: ColorConfig.primary,
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          children: <Widget>[
            Text(label,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                )),
          ],
        ));
  }
}
