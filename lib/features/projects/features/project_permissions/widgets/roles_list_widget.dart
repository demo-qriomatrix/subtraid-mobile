import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project_permissions/widgets/user_roles_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/projects/features/project_permissions/bloc/project_permission_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_group_model.dart';

class RolesListWidget extends StatefulWidget {
  final TextEditingController roleSearch;

  RolesListWidget({@required this.roleSearch});

  @override
  _RolesListWidgetState createState() => _RolesListWidgetState();
}

class _RolesListWidgetState extends State<RolesListWidget> {
  String search = '';

  @override
  void initState() {
    super.initState();
    widget.roleSearch.addListener(onChange);
  }

  onChange() {
    setState(() {
      search = widget.roleSearch.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProjectPermissionBloc, ProjectPermissionState>(
        builder: (context, state) {
      if (state is ProjectPermissionLoadSuccess) {
        return ListView(padding: EdgeInsets.zero, children: [
          Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              decoration: BoxDecoration(
                  color: ColorConfig.blue4,
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.end,
                    //   children: <Widget>[
                    //     FlatButton(
                    //       shape: RoundedRectangleBorder(
                    //           borderRadius: BorderRadius.circular(20)),
                    //       color: ColorConfig.red1,
                    //       onPressed: () {},
                    //       padding: EdgeInsets.symmetric(horizontal: 30),
                    //       child: Row(
                    //         children: <Widget>[
                    //           ImageIcon(
                    //             AssetImage(LocalImages.trashAlt),
                    //             color: Colors.white,
                    //             size: 14,
                    //           ),
                    //           SizedBox(
                    //             width: 5,
                    //           ),
                    //           Text(
                    //             'Delete',
                    //             style: TextStyle(
                    //                 fontSize: 14,
                    //                 color: Colors.white,
                    //                 fontWeight: FontWeight.w600),
                    //           )
                    //         ],
                    //       ),
                    //     )
                    //   ],
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // TextField(
                    //   onChanged: (val) {
                    //     setState(() {
                    //       search = val;
                    //     });
                    //   },
                    //   decoration: InputDecoration(
                    //     hintText: 'User Role',
                    //     hintStyle: TextStyle(
                    //         color: ColorConfig.primary,
                    //         fontSize: 14,
                    //         fontWeight: FontWeight.w600),
                    //     fillColor: ColorConfig.blue5,
                    //     filled: true,
                    //     enabledBorder: UnderlineInputBorder(
                    //         borderSide: BorderSide(
                    //             width: 2, color: ColorConfig.primary)),
                    //     focusedBorder: UnderlineInputBorder(
                    //         borderSide: BorderSide(
                    //             width: 2, color: ColorConfig.primary)),
                    //     border: UnderlineInputBorder(
                    //         borderSide: BorderSide(
                    //             width: 2, color: ColorConfig.primary)),
                    //   ),
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    Column(
                      children: state
                          .companyInfoModel.company.projectPermissionGroups
                          .where((element) => search != ''
                              ? element.name
                                  .toLowerCase()
                                  .contains(search.toLowerCase())
                              : true)
                          .map((ppg) => UserRoleWidget(ppg))
                          .toList(),
                    )
                  ]))
        ]);
      }

      return Container();
    });
  }
}

class UserRoleWidget extends StatefulWidget {
  final ProjectPermissionGroupModel permissonGroupModel;

  UserRoleWidget(this.permissonGroupModel);

  @override
  _UserRoleWidgetState createState() => _UserRoleWidgetState();
}

class _UserRoleWidgetState extends State<UserRoleWidget> {
  bool editMode = false;

  @override
  Widget build(BuildContext context) {
    return editMode
        ? Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: UserRolesWidget(
              editMode: true,
              permissonGroupModel: widget.permissonGroupModel,
              callback: () {
                setState(() {
                  editMode = false;
                });
              },
            ),
          )
        : Container(
            margin: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      blurRadius: 10,
                      offset: Offset(2, 7),
                      color: ColorConfig.shadow1)
                ]),
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            child: Row(
              children: <Widget>[
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: ColorConfig.orange,
                  ),
                  child: Checkbox(
                    onChanged: (val) {
                      context.read<ProjectPermissionBloc>().add(
                          ProjectPermissionGroupSetSelected(
                              id: widget.permissonGroupModel.id,
                              selected: val));
                    },
                    value:
                        (widget.permissonGroupModel?.selected ?? false) == true,
                  ),
                ),
                Text(
                  widget.permissonGroupModel.name,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
                Spacer(),
                RawMaterialButton(
                  constraints: BoxConstraints(),
                  padding: EdgeInsets.all(12),
                  shape: CircleBorder(),
                  onPressed: () {
                    setState(() {
                      editMode = true;
                    });
                  },
                  child: ImageIcon(
                    AssetImage(LocalImages.edit),
                    color: ColorConfig.primary,
                    size: 16,
                  ),
                ),
                // RawMaterialButton(
                //   constraints: BoxConstraints(),
                //   padding: EdgeInsets.all(12),
                //   shape: CircleBorder(),
                //   onPressed: () {},
                //   child: ImageIcon(
                //     AssetImage(LocalImages.trashAlt),
                //     color: ColorConfig.red1,
                //     size: 16,
                //   ),
                // ),
              ],
            ),
          );
  }
}
