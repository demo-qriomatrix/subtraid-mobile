import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/projects/features/project_permissions/bloc/project_permission_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/widgets/roles_list_widget.dart';
import 'package:Subtraid/features/projects/features/project_permissions/widgets/user_roles_widget.dart';
import 'package:Subtraid/features/shared/widgets/custom_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProjectPermissionsScreen extends StatelessWidget {
  final TextEditingController roleSearch;

  ProjectPermissionsScreen({@required this.roleSearch});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProjectPermissionBloc, ProjectPermissionState>(
      builder: (context, state) {
        return Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 20,
                ),
                CustomIcon(
                  icon: LocalImages.addressBook,
                  title: 'Roles List',
                  selected: state.selectedIndex == 0,
                  ontap: () {
                    context
                        .read<ProjectPermissionBloc>()
                        .add(ProjectPermissionIndexChanged(index: 0));
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                CustomIcon(
                  icon: LocalImages.plus,
                  title: 'User Roles',
                  selected: state.selectedIndex == 1,
                  ontap: () {
                    context
                        .read<ProjectPermissionBloc>()
                        .add(ProjectPermissionIndexChanged(index: 1));
                  },
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Expanded(
                child: [
              RolesListWidget(
                roleSearch: roleSearch,
              ),
              UserRolesWidget(
                editMode: false,
              ),
            ][state.selectedIndex])
          ],
        );
      },
    );
  }
}
