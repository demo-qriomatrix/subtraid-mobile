// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_permission_role_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectPermissionRoleModel> _$projectPermissionRoleModelSerializer =
    new _$ProjectPermissionRoleModelSerializer();

class _$ProjectPermissionRoleModelSerializer
    implements StructuredSerializer<ProjectPermissionRoleModel> {
  @override
  final Iterable<Type> types = const [
    ProjectPermissionRoleModel,
    _$ProjectPermissionRoleModel
  ];
  @override
  final String wireName = 'ProjectPermissionRoleModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectPermissionRoleModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.idMember != null) {
      result
        ..add('idMember')
        ..add(serializers.serialize(object.idMember,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.permission != null) {
      result
        ..add('permission')
        ..add(serializers.serialize(object.permission,
            specifiedType: const FullType(ProjectPermissionGroupModel)));
    }
    return result;
  }

  @override
  ProjectPermissionRoleModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectPermissionRoleModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'idMember':
          result.idMember = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'permission':
          result.permission.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectPermissionGroupModel))
              as ProjectPermissionGroupModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectPermissionRoleModel extends ProjectPermissionRoleModel {
  @override
  final String idMember;
  @override
  final String id;
  @override
  final ProjectPermissionGroupModel permission;

  factory _$ProjectPermissionRoleModel(
          [void Function(ProjectPermissionRoleModelBuilder) updates]) =>
      (new ProjectPermissionRoleModelBuilder()..update(updates)).build();

  _$ProjectPermissionRoleModel._({this.idMember, this.id, this.permission})
      : super._();

  @override
  ProjectPermissionRoleModel rebuild(
          void Function(ProjectPermissionRoleModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectPermissionRoleModelBuilder toBuilder() =>
      new ProjectPermissionRoleModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectPermissionRoleModel &&
        idMember == other.idMember &&
        id == other.id &&
        permission == other.permission;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, idMember.hashCode), id.hashCode), permission.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectPermissionRoleModel')
          ..add('idMember', idMember)
          ..add('id', id)
          ..add('permission', permission))
        .toString();
  }
}

class ProjectPermissionRoleModelBuilder
    implements
        Builder<ProjectPermissionRoleModel, ProjectPermissionRoleModelBuilder> {
  _$ProjectPermissionRoleModel _$v;

  String _idMember;
  String get idMember => _$this._idMember;
  set idMember(String idMember) => _$this._idMember = idMember;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ProjectPermissionGroupModelBuilder _permission;
  ProjectPermissionGroupModelBuilder get permission =>
      _$this._permission ??= new ProjectPermissionGroupModelBuilder();
  set permission(ProjectPermissionGroupModelBuilder permission) =>
      _$this._permission = permission;

  ProjectPermissionRoleModelBuilder();

  ProjectPermissionRoleModelBuilder get _$this {
    if (_$v != null) {
      _idMember = _$v.idMember;
      _id = _$v.id;
      _permission = _$v.permission?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectPermissionRoleModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectPermissionRoleModel;
  }

  @override
  void update(void Function(ProjectPermissionRoleModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectPermissionRoleModel build() {
    _$ProjectPermissionRoleModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectPermissionRoleModel._(
              idMember: idMember, id: id, permission: _permission?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'permission';
        _permission?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectPermissionRoleModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
