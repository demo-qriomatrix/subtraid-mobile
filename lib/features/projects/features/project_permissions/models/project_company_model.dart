library project_company_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_group_model.dart';

part 'project_company_model.g.dart';

abstract class ProjectCompanyModel
    implements Built<ProjectCompanyModel, ProjectCompanyModelBuilder> {
  @nullable
  BuiltList<ProjectPermissionGroupModel> get projectPermissionGroups;

  ProjectCompanyModel._();

  factory ProjectCompanyModel([updates(ProjectCompanyModelBuilder b)]) =
      _$ProjectCompanyModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectCompanyModel.serializer, this));
  }

  static ProjectCompanyModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectCompanyModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectCompanyModel> get serializer =>
      _$projectCompanyModelSerializer;
}
