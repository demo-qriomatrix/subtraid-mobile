// GENERATED CODE - DO NOT MODIFY BY HAND

part of company_info_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CompanyInfoResponseModel> _$companyInfoResponseModelSerializer =
    new _$CompanyInfoResponseModelSerializer();

class _$CompanyInfoResponseModelSerializer
    implements StructuredSerializer<CompanyInfoResponseModel> {
  @override
  final Iterable<Type> types = const [
    CompanyInfoResponseModel,
    _$CompanyInfoResponseModel
  ];
  @override
  final String wireName = 'CompanyInfoResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CompanyInfoResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.companyInfo != null) {
      result
        ..add('companyInfo')
        ..add(serializers.serialize(object.companyInfo,
            specifiedType: const FullType(ProjectCompanyInfoModel)));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CompanyInfoResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CompanyInfoResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'companyInfo':
          result.companyInfo.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectCompanyInfoModel))
              as ProjectCompanyInfoModel);
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CompanyInfoResponseModel extends CompanyInfoResponseModel {
  @override
  final ProjectCompanyInfoModel companyInfo;
  @override
  final String message;

  factory _$CompanyInfoResponseModel(
          [void Function(CompanyInfoResponseModelBuilder) updates]) =>
      (new CompanyInfoResponseModelBuilder()..update(updates)).build();

  _$CompanyInfoResponseModel._({this.companyInfo, this.message}) : super._();

  @override
  CompanyInfoResponseModel rebuild(
          void Function(CompanyInfoResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyInfoResponseModelBuilder toBuilder() =>
      new CompanyInfoResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyInfoResponseModel &&
        companyInfo == other.companyInfo &&
        message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, companyInfo.hashCode), message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyInfoResponseModel')
          ..add('companyInfo', companyInfo)
          ..add('message', message))
        .toString();
  }
}

class CompanyInfoResponseModelBuilder
    implements
        Builder<CompanyInfoResponseModel, CompanyInfoResponseModelBuilder> {
  _$CompanyInfoResponseModel _$v;

  ProjectCompanyInfoModelBuilder _companyInfo;
  ProjectCompanyInfoModelBuilder get companyInfo =>
      _$this._companyInfo ??= new ProjectCompanyInfoModelBuilder();
  set companyInfo(ProjectCompanyInfoModelBuilder companyInfo) =>
      _$this._companyInfo = companyInfo;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  CompanyInfoResponseModelBuilder();

  CompanyInfoResponseModelBuilder get _$this {
    if (_$v != null) {
      _companyInfo = _$v.companyInfo?.toBuilder();
      _message = _$v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyInfoResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CompanyInfoResponseModel;
  }

  @override
  void update(void Function(CompanyInfoResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyInfoResponseModel build() {
    _$CompanyInfoResponseModel _$result;
    try {
      _$result = _$v ??
          new _$CompanyInfoResponseModel._(
              companyInfo: _companyInfo?.build(), message: message);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'companyInfo';
        _companyInfo?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CompanyInfoResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
