// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_company_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectCompanyModel> _$projectCompanyModelSerializer =
    new _$ProjectCompanyModelSerializer();

class _$ProjectCompanyModelSerializer
    implements StructuredSerializer<ProjectCompanyModel> {
  @override
  final Iterable<Type> types = const [
    ProjectCompanyModel,
    _$ProjectCompanyModel
  ];
  @override
  final String wireName = 'ProjectCompanyModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectCompanyModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.projectPermissionGroups != null) {
      result
        ..add('projectPermissionGroups')
        ..add(serializers.serialize(object.projectPermissionGroups,
            specifiedType: const FullType(BuiltList,
                const [const FullType(ProjectPermissionGroupModel)])));
    }
    return result;
  }

  @override
  ProjectCompanyModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectCompanyModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'projectPermissionGroups':
          result.projectPermissionGroups.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(ProjectPermissionGroupModel)
              ])) as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectCompanyModel extends ProjectCompanyModel {
  @override
  final BuiltList<ProjectPermissionGroupModel> projectPermissionGroups;

  factory _$ProjectCompanyModel(
          [void Function(ProjectCompanyModelBuilder) updates]) =>
      (new ProjectCompanyModelBuilder()..update(updates)).build();

  _$ProjectCompanyModel._({this.projectPermissionGroups}) : super._();

  @override
  ProjectCompanyModel rebuild(
          void Function(ProjectCompanyModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectCompanyModelBuilder toBuilder() =>
      new ProjectCompanyModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectCompanyModel &&
        projectPermissionGroups == other.projectPermissionGroups;
  }

  @override
  int get hashCode {
    return $jf($jc(0, projectPermissionGroups.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectCompanyModel')
          ..add('projectPermissionGroups', projectPermissionGroups))
        .toString();
  }
}

class ProjectCompanyModelBuilder
    implements Builder<ProjectCompanyModel, ProjectCompanyModelBuilder> {
  _$ProjectCompanyModel _$v;

  ListBuilder<ProjectPermissionGroupModel> _projectPermissionGroups;
  ListBuilder<ProjectPermissionGroupModel> get projectPermissionGroups =>
      _$this._projectPermissionGroups ??=
          new ListBuilder<ProjectPermissionGroupModel>();
  set projectPermissionGroups(
          ListBuilder<ProjectPermissionGroupModel> projectPermissionGroups) =>
      _$this._projectPermissionGroups = projectPermissionGroups;

  ProjectCompanyModelBuilder();

  ProjectCompanyModelBuilder get _$this {
    if (_$v != null) {
      _projectPermissionGroups = _$v.projectPermissionGroups?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectCompanyModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectCompanyModel;
  }

  @override
  void update(void Function(ProjectCompanyModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectCompanyModel build() {
    _$ProjectCompanyModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectCompanyModel._(
              projectPermissionGroups: _projectPermissionGroups?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'projectPermissionGroups';
        _projectPermissionGroups?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectCompanyModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
