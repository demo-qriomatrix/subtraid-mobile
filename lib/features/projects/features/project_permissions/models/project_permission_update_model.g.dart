// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_permission_update_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectPermissionUpdateModel>
    _$projectPermissionUpdateModelSerializer =
    new _$ProjectPermissionUpdateModelSerializer();

class _$ProjectPermissionUpdateModelSerializer
    implements StructuredSerializer<ProjectPermissionUpdateModel> {
  @override
  final Iterable<Type> types = const [
    ProjectPermissionUpdateModel,
    _$ProjectPermissionUpdateModel
  ];
  @override
  final String wireName = 'ProjectPermissionUpdateModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectPermissionUpdateModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.ppgObj != null) {
      result
        ..add('ppgObj')
        ..add(serializers.serialize(object.ppgObj,
            specifiedType: const FullType(ProjectPermissionGroupModel)));
    }
    return result;
  }

  @override
  ProjectPermissionUpdateModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectPermissionUpdateModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ppgObj':
          result.ppgObj.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectPermissionGroupModel))
              as ProjectPermissionGroupModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectPermissionUpdateModel extends ProjectPermissionUpdateModel {
  @override
  final ProjectPermissionGroupModel ppgObj;

  factory _$ProjectPermissionUpdateModel(
          [void Function(ProjectPermissionUpdateModelBuilder) updates]) =>
      (new ProjectPermissionUpdateModelBuilder()..update(updates)).build();

  _$ProjectPermissionUpdateModel._({this.ppgObj}) : super._();

  @override
  ProjectPermissionUpdateModel rebuild(
          void Function(ProjectPermissionUpdateModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectPermissionUpdateModelBuilder toBuilder() =>
      new ProjectPermissionUpdateModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectPermissionUpdateModel && ppgObj == other.ppgObj;
  }

  @override
  int get hashCode {
    return $jf($jc(0, ppgObj.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectPermissionUpdateModel')
          ..add('ppgObj', ppgObj))
        .toString();
  }
}

class ProjectPermissionUpdateModelBuilder
    implements
        Builder<ProjectPermissionUpdateModel,
            ProjectPermissionUpdateModelBuilder> {
  _$ProjectPermissionUpdateModel _$v;

  ProjectPermissionGroupModelBuilder _ppgObj;
  ProjectPermissionGroupModelBuilder get ppgObj =>
      _$this._ppgObj ??= new ProjectPermissionGroupModelBuilder();
  set ppgObj(ProjectPermissionGroupModelBuilder ppgObj) =>
      _$this._ppgObj = ppgObj;

  ProjectPermissionUpdateModelBuilder();

  ProjectPermissionUpdateModelBuilder get _$this {
    if (_$v != null) {
      _ppgObj = _$v.ppgObj?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectPermissionUpdateModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectPermissionUpdateModel;
  }

  @override
  void update(void Function(ProjectPermissionUpdateModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectPermissionUpdateModel build() {
    _$ProjectPermissionUpdateModel _$result;
    try {
      _$result =
          _$v ?? new _$ProjectPermissionUpdateModel._(ppgObj: _ppgObj?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'ppgObj';
        _ppgObj?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectPermissionUpdateModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
