library project_permission_group_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permissions_model.dart';

part 'project_permission_group_model.g.dart';

abstract class ProjectPermissionGroupModel
    implements
        Built<ProjectPermissionGroupModel, ProjectPermissionGroupModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  PermissionsModel get permissions;

  @nullable
  String get name;

  @nullable
  String get description;

  @nullable
  bool get selected;

  ProjectPermissionGroupModel._();

  factory ProjectPermissionGroupModel(
          [updates(ProjectPermissionGroupModelBuilder b)]) =
      _$ProjectPermissionGroupModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        ProjectPermissionGroupModel.serializer, this));
  }

  static ProjectPermissionGroupModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectPermissionGroupModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectPermissionGroupModel> get serializer =>
      _$projectPermissionGroupModelSerializer;
}
