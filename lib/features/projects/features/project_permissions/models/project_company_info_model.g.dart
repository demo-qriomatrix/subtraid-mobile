// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_company_info_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectCompanyInfoModel> _$projectCompanyInfoModelSerializer =
    new _$ProjectCompanyInfoModelSerializer();

class _$ProjectCompanyInfoModelSerializer
    implements StructuredSerializer<ProjectCompanyInfoModel> {
  @override
  final Iterable<Type> types = const [
    ProjectCompanyInfoModel,
    _$ProjectCompanyInfoModel
  ];
  @override
  final String wireName = 'ProjectCompanyInfoModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectCompanyInfoModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.company != null) {
      result
        ..add('company')
        ..add(serializers.serialize(object.company,
            specifiedType: const FullType(ProjectCompanyModel)));
    }
    return result;
  }

  @override
  ProjectCompanyInfoModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectCompanyInfoModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'company':
          result.company.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectCompanyModel))
              as ProjectCompanyModel);
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectCompanyInfoModel extends ProjectCompanyInfoModel {
  @override
  final String name;
  @override
  final ProjectCompanyModel company;

  factory _$ProjectCompanyInfoModel(
          [void Function(ProjectCompanyInfoModelBuilder) updates]) =>
      (new ProjectCompanyInfoModelBuilder()..update(updates)).build();

  _$ProjectCompanyInfoModel._({this.name, this.company}) : super._();

  @override
  ProjectCompanyInfoModel rebuild(
          void Function(ProjectCompanyInfoModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectCompanyInfoModelBuilder toBuilder() =>
      new ProjectCompanyInfoModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectCompanyInfoModel &&
        name == other.name &&
        company == other.company;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), company.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectCompanyInfoModel')
          ..add('name', name)
          ..add('company', company))
        .toString();
  }
}

class ProjectCompanyInfoModelBuilder
    implements
        Builder<ProjectCompanyInfoModel, ProjectCompanyInfoModelBuilder> {
  _$ProjectCompanyInfoModel _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  ProjectCompanyModelBuilder _company;
  ProjectCompanyModelBuilder get company =>
      _$this._company ??= new ProjectCompanyModelBuilder();
  set company(ProjectCompanyModelBuilder company) => _$this._company = company;

  ProjectCompanyInfoModelBuilder();

  ProjectCompanyInfoModelBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _company = _$v.company?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectCompanyInfoModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectCompanyInfoModel;
  }

  @override
  void update(void Function(ProjectCompanyInfoModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectCompanyInfoModel build() {
    _$ProjectCompanyInfoModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectCompanyInfoModel._(
              name: name, company: _company?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'company';
        _company?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectCompanyInfoModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
