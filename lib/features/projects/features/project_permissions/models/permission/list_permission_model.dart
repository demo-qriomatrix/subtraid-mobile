library list_permission_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'list_permission_model.g.dart';

abstract class ListPermissionModel
    implements Built<ListPermissionModel, ListPermissionModelBuilder> {
  @nullable
  @BuiltValueField(wireName: 'update')
  bool get updateList;

  @nullable
  bool get create;

  @nullable
  bool get delete;

  @nullable
  bool get move;

  ListPermissionModel._();

  factory ListPermissionModel([updates(ListPermissionModelBuilder b)]) =
      _$ListPermissionModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ListPermissionModel.serializer, this));
  }

  static ListPermissionModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ListPermissionModel.serializer, json.decode(jsonString));
  }

  static Serializer<ListPermissionModel> get serializer =>
      _$listPermissionModelSerializer;
}
