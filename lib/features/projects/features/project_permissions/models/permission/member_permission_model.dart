library member_permission_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'member_permission_model.g.dart';

abstract class MemberPermissionModel
    implements Built<MemberPermissionModel, MemberPermissionModelBuilder> {
  @nullable
  bool get add;

  @nullable
  bool get remove;

  @nullable
  bool get assignRoles;

  @nullable
  bool get assignToCards;

  MemberPermissionModel._();

  factory MemberPermissionModel([updates(MemberPermissionModelBuilder b)]) =
      _$MemberPermissionModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(MemberPermissionModel.serializer, this));
  }

  static MemberPermissionModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        MemberPermissionModel.serializer, json.decode(jsonString));
  }

  static Serializer<MemberPermissionModel> get serializer =>
      _$memberPermissionModelSerializer;
}
