// GENERATED CODE - DO NOT MODIFY BY HAND

part of list_permission_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ListPermissionModel> _$listPermissionModelSerializer =
    new _$ListPermissionModelSerializer();

class _$ListPermissionModelSerializer
    implements StructuredSerializer<ListPermissionModel> {
  @override
  final Iterable<Type> types = const [
    ListPermissionModel,
    _$ListPermissionModel
  ];
  @override
  final String wireName = 'ListPermissionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ListPermissionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.updateList != null) {
      result
        ..add('update')
        ..add(serializers.serialize(object.updateList,
            specifiedType: const FullType(bool)));
    }
    if (object.create != null) {
      result
        ..add('create')
        ..add(serializers.serialize(object.create,
            specifiedType: const FullType(bool)));
    }
    if (object.delete != null) {
      result
        ..add('delete')
        ..add(serializers.serialize(object.delete,
            specifiedType: const FullType(bool)));
    }
    if (object.move != null) {
      result
        ..add('move')
        ..add(serializers.serialize(object.move,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ListPermissionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ListPermissionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'update':
          result.updateList = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'create':
          result.create = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'delete':
          result.delete = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'move':
          result.move = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ListPermissionModel extends ListPermissionModel {
  @override
  final bool updateList;
  @override
  final bool create;
  @override
  final bool delete;
  @override
  final bool move;

  factory _$ListPermissionModel(
          [void Function(ListPermissionModelBuilder) updates]) =>
      (new ListPermissionModelBuilder()..update(updates)).build();

  _$ListPermissionModel._(
      {this.updateList, this.create, this.delete, this.move})
      : super._();

  @override
  ListPermissionModel rebuild(
          void Function(ListPermissionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ListPermissionModelBuilder toBuilder() =>
      new ListPermissionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ListPermissionModel &&
        updateList == other.updateList &&
        create == other.create &&
        delete == other.delete &&
        move == other.move;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, updateList.hashCode), create.hashCode), delete.hashCode),
        move.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ListPermissionModel')
          ..add('updateList', updateList)
          ..add('create', create)
          ..add('delete', delete)
          ..add('move', move))
        .toString();
  }
}

class ListPermissionModelBuilder
    implements Builder<ListPermissionModel, ListPermissionModelBuilder> {
  _$ListPermissionModel _$v;

  bool _updateList;
  bool get updateList => _$this._updateList;
  set updateList(bool updateList) => _$this._updateList = updateList;

  bool _create;
  bool get create => _$this._create;
  set create(bool create) => _$this._create = create;

  bool _delete;
  bool get delete => _$this._delete;
  set delete(bool delete) => _$this._delete = delete;

  bool _move;
  bool get move => _$this._move;
  set move(bool move) => _$this._move = move;

  ListPermissionModelBuilder();

  ListPermissionModelBuilder get _$this {
    if (_$v != null) {
      _updateList = _$v.updateList;
      _create = _$v.create;
      _delete = _$v.delete;
      _move = _$v.move;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ListPermissionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ListPermissionModel;
  }

  @override
  void update(void Function(ListPermissionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ListPermissionModel build() {
    final _$result = _$v ??
        new _$ListPermissionModel._(
            updateList: updateList, create: create, delete: delete, move: move);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
