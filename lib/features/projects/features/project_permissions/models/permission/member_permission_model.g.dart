// GENERATED CODE - DO NOT MODIFY BY HAND

part of member_permission_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MemberPermissionModel> _$memberPermissionModelSerializer =
    new _$MemberPermissionModelSerializer();

class _$MemberPermissionModelSerializer
    implements StructuredSerializer<MemberPermissionModel> {
  @override
  final Iterable<Type> types = const [
    MemberPermissionModel,
    _$MemberPermissionModel
  ];
  @override
  final String wireName = 'MemberPermissionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, MemberPermissionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.add != null) {
      result
        ..add('add')
        ..add(serializers.serialize(object.add,
            specifiedType: const FullType(bool)));
    }
    if (object.remove != null) {
      result
        ..add('remove')
        ..add(serializers.serialize(object.remove,
            specifiedType: const FullType(bool)));
    }
    if (object.assignRoles != null) {
      result
        ..add('assignRoles')
        ..add(serializers.serialize(object.assignRoles,
            specifiedType: const FullType(bool)));
    }
    if (object.assignToCards != null) {
      result
        ..add('assignToCards')
        ..add(serializers.serialize(object.assignToCards,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  MemberPermissionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MemberPermissionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'add':
          result.add = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'remove':
          result.remove = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'assignRoles':
          result.assignRoles = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'assignToCards':
          result.assignToCards = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$MemberPermissionModel extends MemberPermissionModel {
  @override
  final bool add;
  @override
  final bool remove;
  @override
  final bool assignRoles;
  @override
  final bool assignToCards;

  factory _$MemberPermissionModel(
          [void Function(MemberPermissionModelBuilder) updates]) =>
      (new MemberPermissionModelBuilder()..update(updates)).build();

  _$MemberPermissionModel._(
      {this.add, this.remove, this.assignRoles, this.assignToCards})
      : super._();

  @override
  MemberPermissionModel rebuild(
          void Function(MemberPermissionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MemberPermissionModelBuilder toBuilder() =>
      new MemberPermissionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MemberPermissionModel &&
        add == other.add &&
        remove == other.remove &&
        assignRoles == other.assignRoles &&
        assignToCards == other.assignToCards;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, add.hashCode), remove.hashCode), assignRoles.hashCode),
        assignToCards.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MemberPermissionModel')
          ..add('add', add)
          ..add('remove', remove)
          ..add('assignRoles', assignRoles)
          ..add('assignToCards', assignToCards))
        .toString();
  }
}

class MemberPermissionModelBuilder
    implements Builder<MemberPermissionModel, MemberPermissionModelBuilder> {
  _$MemberPermissionModel _$v;

  bool _add;
  bool get add => _$this._add;
  set add(bool add) => _$this._add = add;

  bool _remove;
  bool get remove => _$this._remove;
  set remove(bool remove) => _$this._remove = remove;

  bool _assignRoles;
  bool get assignRoles => _$this._assignRoles;
  set assignRoles(bool assignRoles) => _$this._assignRoles = assignRoles;

  bool _assignToCards;
  bool get assignToCards => _$this._assignToCards;
  set assignToCards(bool assignToCards) =>
      _$this._assignToCards = assignToCards;

  MemberPermissionModelBuilder();

  MemberPermissionModelBuilder get _$this {
    if (_$v != null) {
      _add = _$v.add;
      _remove = _$v.remove;
      _assignRoles = _$v.assignRoles;
      _assignToCards = _$v.assignToCards;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MemberPermissionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MemberPermissionModel;
  }

  @override
  void update(void Function(MemberPermissionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MemberPermissionModel build() {
    final _$result = _$v ??
        new _$MemberPermissionModel._(
            add: add,
            remove: remove,
            assignRoles: assignRoles,
            assignToCards: assignToCards);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
