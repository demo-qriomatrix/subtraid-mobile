// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_permission_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectPermissionModel> _$projectPermissionModelSerializer =
    new _$ProjectPermissionModelSerializer();

class _$ProjectPermissionModelSerializer
    implements StructuredSerializer<ProjectPermissionModel> {
  @override
  final Iterable<Type> types = const [
    ProjectPermissionModel,
    _$ProjectPermissionModel
  ];
  @override
  final String wireName = 'ProjectPermissionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectPermissionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.general != null) {
      result
        ..add('general')
        ..add(serializers.serialize(object.general,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ProjectPermissionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectPermissionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'general':
          result.general = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectPermissionModel extends ProjectPermissionModel {
  @override
  final bool general;

  factory _$ProjectPermissionModel(
          [void Function(ProjectPermissionModelBuilder) updates]) =>
      (new ProjectPermissionModelBuilder()..update(updates)).build();

  _$ProjectPermissionModel._({this.general}) : super._();

  @override
  ProjectPermissionModel rebuild(
          void Function(ProjectPermissionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectPermissionModelBuilder toBuilder() =>
      new ProjectPermissionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectPermissionModel && general == other.general;
  }

  @override
  int get hashCode {
    return $jf($jc(0, general.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectPermissionModel')
          ..add('general', general))
        .toString();
  }
}

class ProjectPermissionModelBuilder
    implements Builder<ProjectPermissionModel, ProjectPermissionModelBuilder> {
  _$ProjectPermissionModel _$v;

  bool _general;
  bool get general => _$this._general;
  set general(bool general) => _$this._general = general;

  ProjectPermissionModelBuilder();

  ProjectPermissionModelBuilder get _$this {
    if (_$v != null) {
      _general = _$v.general;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectPermissionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectPermissionModel;
  }

  @override
  void update(void Function(ProjectPermissionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectPermissionModel build() {
    final _$result = _$v ?? new _$ProjectPermissionModel._(general: general);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
