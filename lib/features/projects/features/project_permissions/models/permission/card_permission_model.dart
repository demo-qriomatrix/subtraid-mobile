library card_permission_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'card_permission_model.g.dart';

abstract class CardPermissionModel
    implements Built<CardPermissionModel, CardPermissionModelBuilder> {
  @nullable
  bool get create;

  @nullable
  bool get delete;

  @nullable
  bool get markComplete;

  @nullable
  bool get generalUpdate;

  @nullable
  bool get move;

  @nullable
  bool get comments;

  @nullable
  bool get checklist;

  @nullable
  bool get attachment;

  CardPermissionModel._();

  factory CardPermissionModel([updates(CardPermissionModelBuilder b)]) =
      _$CardPermissionModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CardPermissionModel.serializer, this));
  }

  static CardPermissionModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CardPermissionModel.serializer, json.decode(jsonString));
  }

  static Serializer<CardPermissionModel> get serializer =>
      _$cardPermissionModelSerializer;
}
