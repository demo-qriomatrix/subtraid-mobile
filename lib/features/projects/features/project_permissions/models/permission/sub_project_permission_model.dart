library sub_project_permission_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'sub_project_permission_model.g.dart';

abstract class SubProjectPermissionModel
    implements
        Built<SubProjectPermissionModel, SubProjectPermissionModelBuilder> {
  @nullable
  bool get create;

  SubProjectPermissionModel._();

  factory SubProjectPermissionModel(
          [updates(SubProjectPermissionModelBuilder b)]) =
      _$SubProjectPermissionModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(SubProjectPermissionModel.serializer, this));
  }

  static SubProjectPermissionModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        SubProjectPermissionModel.serializer, json.decode(jsonString));
  }

  static Serializer<SubProjectPermissionModel> get serializer =>
      _$subProjectPermissionModelSerializer;
}
