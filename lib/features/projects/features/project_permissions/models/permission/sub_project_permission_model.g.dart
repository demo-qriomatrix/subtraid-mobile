// GENERATED CODE - DO NOT MODIFY BY HAND

part of sub_project_permission_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SubProjectPermissionModel> _$subProjectPermissionModelSerializer =
    new _$SubProjectPermissionModelSerializer();

class _$SubProjectPermissionModelSerializer
    implements StructuredSerializer<SubProjectPermissionModel> {
  @override
  final Iterable<Type> types = const [
    SubProjectPermissionModel,
    _$SubProjectPermissionModel
  ];
  @override
  final String wireName = 'SubProjectPermissionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SubProjectPermissionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.create != null) {
      result
        ..add('create')
        ..add(serializers.serialize(object.create,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  SubProjectPermissionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SubProjectPermissionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'create':
          result.create = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$SubProjectPermissionModel extends SubProjectPermissionModel {
  @override
  final bool create;

  factory _$SubProjectPermissionModel(
          [void Function(SubProjectPermissionModelBuilder) updates]) =>
      (new SubProjectPermissionModelBuilder()..update(updates)).build();

  _$SubProjectPermissionModel._({this.create}) : super._();

  @override
  SubProjectPermissionModel rebuild(
          void Function(SubProjectPermissionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SubProjectPermissionModelBuilder toBuilder() =>
      new SubProjectPermissionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SubProjectPermissionModel && create == other.create;
  }

  @override
  int get hashCode {
    return $jf($jc(0, create.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SubProjectPermissionModel')
          ..add('create', create))
        .toString();
  }
}

class SubProjectPermissionModelBuilder
    implements
        Builder<SubProjectPermissionModel, SubProjectPermissionModelBuilder> {
  _$SubProjectPermissionModel _$v;

  bool _create;
  bool get create => _$this._create;
  set create(bool create) => _$this._create = create;

  SubProjectPermissionModelBuilder();

  SubProjectPermissionModelBuilder get _$this {
    if (_$v != null) {
      _create = _$v.create;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SubProjectPermissionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SubProjectPermissionModel;
  }

  @override
  void update(void Function(SubProjectPermissionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SubProjectPermissionModel build() {
    final _$result = _$v ?? new _$SubProjectPermissionModel._(create: create);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
