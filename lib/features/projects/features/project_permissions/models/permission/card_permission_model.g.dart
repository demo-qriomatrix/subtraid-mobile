// GENERATED CODE - DO NOT MODIFY BY HAND

part of card_permission_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CardPermissionModel> _$cardPermissionModelSerializer =
    new _$CardPermissionModelSerializer();

class _$CardPermissionModelSerializer
    implements StructuredSerializer<CardPermissionModel> {
  @override
  final Iterable<Type> types = const [
    CardPermissionModel,
    _$CardPermissionModel
  ];
  @override
  final String wireName = 'CardPermissionModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, CardPermissionModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.create != null) {
      result
        ..add('create')
        ..add(serializers.serialize(object.create,
            specifiedType: const FullType(bool)));
    }
    if (object.delete != null) {
      result
        ..add('delete')
        ..add(serializers.serialize(object.delete,
            specifiedType: const FullType(bool)));
    }
    if (object.markComplete != null) {
      result
        ..add('markComplete')
        ..add(serializers.serialize(object.markComplete,
            specifiedType: const FullType(bool)));
    }
    if (object.generalUpdate != null) {
      result
        ..add('generalUpdate')
        ..add(serializers.serialize(object.generalUpdate,
            specifiedType: const FullType(bool)));
    }
    if (object.move != null) {
      result
        ..add('move')
        ..add(serializers.serialize(object.move,
            specifiedType: const FullType(bool)));
    }
    if (object.comments != null) {
      result
        ..add('comments')
        ..add(serializers.serialize(object.comments,
            specifiedType: const FullType(bool)));
    }
    if (object.checklist != null) {
      result
        ..add('checklist')
        ..add(serializers.serialize(object.checklist,
            specifiedType: const FullType(bool)));
    }
    if (object.attachment != null) {
      result
        ..add('attachment')
        ..add(serializers.serialize(object.attachment,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  CardPermissionModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CardPermissionModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'create':
          result.create = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'delete':
          result.delete = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'markComplete':
          result.markComplete = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'generalUpdate':
          result.generalUpdate = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'move':
          result.move = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'comments':
          result.comments = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'checklist':
          result.checklist = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'attachment':
          result.attachment = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$CardPermissionModel extends CardPermissionModel {
  @override
  final bool create;
  @override
  final bool delete;
  @override
  final bool markComplete;
  @override
  final bool generalUpdate;
  @override
  final bool move;
  @override
  final bool comments;
  @override
  final bool checklist;
  @override
  final bool attachment;

  factory _$CardPermissionModel(
          [void Function(CardPermissionModelBuilder) updates]) =>
      (new CardPermissionModelBuilder()..update(updates)).build();

  _$CardPermissionModel._(
      {this.create,
      this.delete,
      this.markComplete,
      this.generalUpdate,
      this.move,
      this.comments,
      this.checklist,
      this.attachment})
      : super._();

  @override
  CardPermissionModel rebuild(
          void Function(CardPermissionModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardPermissionModelBuilder toBuilder() =>
      new CardPermissionModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardPermissionModel &&
        create == other.create &&
        delete == other.delete &&
        markComplete == other.markComplete &&
        generalUpdate == other.generalUpdate &&
        move == other.move &&
        comments == other.comments &&
        checklist == other.checklist &&
        attachment == other.attachment;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, create.hashCode), delete.hashCode),
                            markComplete.hashCode),
                        generalUpdate.hashCode),
                    move.hashCode),
                comments.hashCode),
            checklist.hashCode),
        attachment.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardPermissionModel')
          ..add('create', create)
          ..add('delete', delete)
          ..add('markComplete', markComplete)
          ..add('generalUpdate', generalUpdate)
          ..add('move', move)
          ..add('comments', comments)
          ..add('checklist', checklist)
          ..add('attachment', attachment))
        .toString();
  }
}

class CardPermissionModelBuilder
    implements Builder<CardPermissionModel, CardPermissionModelBuilder> {
  _$CardPermissionModel _$v;

  bool _create;
  bool get create => _$this._create;
  set create(bool create) => _$this._create = create;

  bool _delete;
  bool get delete => _$this._delete;
  set delete(bool delete) => _$this._delete = delete;

  bool _markComplete;
  bool get markComplete => _$this._markComplete;
  set markComplete(bool markComplete) => _$this._markComplete = markComplete;

  bool _generalUpdate;
  bool get generalUpdate => _$this._generalUpdate;
  set generalUpdate(bool generalUpdate) =>
      _$this._generalUpdate = generalUpdate;

  bool _move;
  bool get move => _$this._move;
  set move(bool move) => _$this._move = move;

  bool _comments;
  bool get comments => _$this._comments;
  set comments(bool comments) => _$this._comments = comments;

  bool _checklist;
  bool get checklist => _$this._checklist;
  set checklist(bool checklist) => _$this._checklist = checklist;

  bool _attachment;
  bool get attachment => _$this._attachment;
  set attachment(bool attachment) => _$this._attachment = attachment;

  CardPermissionModelBuilder();

  CardPermissionModelBuilder get _$this {
    if (_$v != null) {
      _create = _$v.create;
      _delete = _$v.delete;
      _markComplete = _$v.markComplete;
      _generalUpdate = _$v.generalUpdate;
      _move = _$v.move;
      _comments = _$v.comments;
      _checklist = _$v.checklist;
      _attachment = _$v.attachment;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardPermissionModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CardPermissionModel;
  }

  @override
  void update(void Function(CardPermissionModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardPermissionModel build() {
    final _$result = _$v ??
        new _$CardPermissionModel._(
            create: create,
            delete: delete,
            markComplete: markComplete,
            generalUpdate: generalUpdate,
            move: move,
            comments: comments,
            checklist: checklist,
            attachment: attachment);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
