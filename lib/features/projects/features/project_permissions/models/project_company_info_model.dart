library project_company_info_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'project_company_model.dart';

part 'project_company_info_model.g.dart';

abstract class ProjectCompanyInfoModel
    implements Built<ProjectCompanyInfoModel, ProjectCompanyInfoModelBuilder> {
  @nullable
  String get name;

  @nullable
  ProjectCompanyModel get company;

  ProjectCompanyInfoModel._();

  factory ProjectCompanyInfoModel([updates(ProjectCompanyInfoModelBuilder b)]) =
      _$ProjectCompanyInfoModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectCompanyInfoModel.serializer, this));
  }

  static ProjectCompanyInfoModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectCompanyInfoModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectCompanyInfoModel> get serializer =>
      _$projectCompanyInfoModelSerializer;
}
