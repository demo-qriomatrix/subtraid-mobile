// GENERATED CODE - DO NOT MODIFY BY HAND

part of permissions_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PermissionsModel> _$permissionsModelSerializer =
    new _$PermissionsModelSerializer();

class _$PermissionsModelSerializer
    implements StructuredSerializer<PermissionsModel> {
  @override
  final Iterable<Type> types = const [PermissionsModel, _$PermissionsModel];
  @override
  final String wireName = 'PermissionsModel';

  @override
  Iterable<Object> serialize(Serializers serializers, PermissionsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.project != null) {
      result
        ..add('project')
        ..add(serializers.serialize(object.project,
            specifiedType: const FullType(ProjectPermissionModel)));
    }
    if (object.cards != null) {
      result
        ..add('cards')
        ..add(serializers.serialize(object.cards,
            specifiedType: const FullType(CardPermissionModel)));
    }
    if (object.list != null) {
      result
        ..add('list')
        ..add(serializers.serialize(object.list,
            specifiedType: const FullType(ListPermissionModel)));
    }
    if (object.subproject != null) {
      result
        ..add('subproject')
        ..add(serializers.serialize(object.subproject,
            specifiedType: const FullType(SubProjectPermissionModel)));
    }
    if (object.member != null) {
      result
        ..add('member')
        ..add(serializers.serialize(object.member,
            specifiedType: const FullType(MemberPermissionModel)));
    }
    return result;
  }

  @override
  PermissionsModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PermissionsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'project':
          result.project.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectPermissionModel))
              as ProjectPermissionModel);
          break;
        case 'cards':
          result.cards.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardPermissionModel))
              as CardPermissionModel);
          break;
        case 'list':
          result.list.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ListPermissionModel))
              as ListPermissionModel);
          break;
        case 'subproject':
          result.subproject.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SubProjectPermissionModel))
              as SubProjectPermissionModel);
          break;
        case 'member':
          result.member.replace(serializers.deserialize(value,
                  specifiedType: const FullType(MemberPermissionModel))
              as MemberPermissionModel);
          break;
      }
    }

    return result.build();
  }
}

class _$PermissionsModel extends PermissionsModel {
  @override
  final ProjectPermissionModel project;
  @override
  final CardPermissionModel cards;
  @override
  final ListPermissionModel list;
  @override
  final SubProjectPermissionModel subproject;
  @override
  final MemberPermissionModel member;

  factory _$PermissionsModel(
          [void Function(PermissionsModelBuilder) updates]) =>
      (new PermissionsModelBuilder()..update(updates)).build();

  _$PermissionsModel._(
      {this.project, this.cards, this.list, this.subproject, this.member})
      : super._();

  @override
  PermissionsModel rebuild(void Function(PermissionsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PermissionsModelBuilder toBuilder() =>
      new PermissionsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PermissionsModel &&
        project == other.project &&
        cards == other.cards &&
        list == other.list &&
        subproject == other.subproject &&
        member == other.member;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, project.hashCode), cards.hashCode), list.hashCode),
            subproject.hashCode),
        member.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PermissionsModel')
          ..add('project', project)
          ..add('cards', cards)
          ..add('list', list)
          ..add('subproject', subproject)
          ..add('member', member))
        .toString();
  }
}

class PermissionsModelBuilder
    implements Builder<PermissionsModel, PermissionsModelBuilder> {
  _$PermissionsModel _$v;

  ProjectPermissionModelBuilder _project;
  ProjectPermissionModelBuilder get project =>
      _$this._project ??= new ProjectPermissionModelBuilder();
  set project(ProjectPermissionModelBuilder project) =>
      _$this._project = project;

  CardPermissionModelBuilder _cards;
  CardPermissionModelBuilder get cards =>
      _$this._cards ??= new CardPermissionModelBuilder();
  set cards(CardPermissionModelBuilder cards) => _$this._cards = cards;

  ListPermissionModelBuilder _list;
  ListPermissionModelBuilder get list =>
      _$this._list ??= new ListPermissionModelBuilder();
  set list(ListPermissionModelBuilder list) => _$this._list = list;

  SubProjectPermissionModelBuilder _subproject;
  SubProjectPermissionModelBuilder get subproject =>
      _$this._subproject ??= new SubProjectPermissionModelBuilder();
  set subproject(SubProjectPermissionModelBuilder subproject) =>
      _$this._subproject = subproject;

  MemberPermissionModelBuilder _member;
  MemberPermissionModelBuilder get member =>
      _$this._member ??= new MemberPermissionModelBuilder();
  set member(MemberPermissionModelBuilder member) => _$this._member = member;

  PermissionsModelBuilder();

  PermissionsModelBuilder get _$this {
    if (_$v != null) {
      _project = _$v.project?.toBuilder();
      _cards = _$v.cards?.toBuilder();
      _list = _$v.list?.toBuilder();
      _subproject = _$v.subproject?.toBuilder();
      _member = _$v.member?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PermissionsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PermissionsModel;
  }

  @override
  void update(void Function(PermissionsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PermissionsModel build() {
    _$PermissionsModel _$result;
    try {
      _$result = _$v ??
          new _$PermissionsModel._(
              project: _project?.build(),
              cards: _cards?.build(),
              list: _list?.build(),
              subproject: _subproject?.build(),
              member: _member?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'project';
        _project?.build();
        _$failedField = 'cards';
        _cards?.build();
        _$failedField = 'list';
        _list?.build();
        _$failedField = 'subproject';
        _subproject?.build();
        _$failedField = 'member';
        _member?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PermissionsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
