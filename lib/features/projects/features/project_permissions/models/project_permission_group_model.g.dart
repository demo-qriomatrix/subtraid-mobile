// GENERATED CODE - DO NOT MODIFY BY HAND

part of project_permission_group_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ProjectPermissionGroupModel>
    _$projectPermissionGroupModelSerializer =
    new _$ProjectPermissionGroupModelSerializer();

class _$ProjectPermissionGroupModelSerializer
    implements StructuredSerializer<ProjectPermissionGroupModel> {
  @override
  final Iterable<Type> types = const [
    ProjectPermissionGroupModel,
    _$ProjectPermissionGroupModel
  ];
  @override
  final String wireName = 'ProjectPermissionGroupModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ProjectPermissionGroupModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.permissions != null) {
      result
        ..add('permissions')
        ..add(serializers.serialize(object.permissions,
            specifiedType: const FullType(PermissionsModel)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.selected != null) {
      result
        ..add('selected')
        ..add(serializers.serialize(object.selected,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ProjectPermissionGroupModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ProjectPermissionGroupModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'permissions':
          result.permissions.replace(serializers.deserialize(value,
                  specifiedType: const FullType(PermissionsModel))
              as PermissionsModel);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'selected':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ProjectPermissionGroupModel extends ProjectPermissionGroupModel {
  @override
  final String id;
  @override
  final PermissionsModel permissions;
  @override
  final String name;
  @override
  final String description;
  @override
  final bool selected;

  factory _$ProjectPermissionGroupModel(
          [void Function(ProjectPermissionGroupModelBuilder) updates]) =>
      (new ProjectPermissionGroupModelBuilder()..update(updates)).build();

  _$ProjectPermissionGroupModel._(
      {this.id, this.permissions, this.name, this.description, this.selected})
      : super._();

  @override
  ProjectPermissionGroupModel rebuild(
          void Function(ProjectPermissionGroupModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProjectPermissionGroupModelBuilder toBuilder() =>
      new ProjectPermissionGroupModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProjectPermissionGroupModel &&
        id == other.id &&
        permissions == other.permissions &&
        name == other.name &&
        description == other.description &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), permissions.hashCode), name.hashCode),
            description.hashCode),
        selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProjectPermissionGroupModel')
          ..add('id', id)
          ..add('permissions', permissions)
          ..add('name', name)
          ..add('description', description)
          ..add('selected', selected))
        .toString();
  }
}

class ProjectPermissionGroupModelBuilder
    implements
        Builder<ProjectPermissionGroupModel,
            ProjectPermissionGroupModelBuilder> {
  _$ProjectPermissionGroupModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  PermissionsModelBuilder _permissions;
  PermissionsModelBuilder get permissions =>
      _$this._permissions ??= new PermissionsModelBuilder();
  set permissions(PermissionsModelBuilder permissions) =>
      _$this._permissions = permissions;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  bool _selected;
  bool get selected => _$this._selected;
  set selected(bool selected) => _$this._selected = selected;

  ProjectPermissionGroupModelBuilder();

  ProjectPermissionGroupModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _permissions = _$v.permissions?.toBuilder();
      _name = _$v.name;
      _description = _$v.description;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProjectPermissionGroupModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProjectPermissionGroupModel;
  }

  @override
  void update(void Function(ProjectPermissionGroupModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProjectPermissionGroupModel build() {
    _$ProjectPermissionGroupModel _$result;
    try {
      _$result = _$v ??
          new _$ProjectPermissionGroupModel._(
              id: id,
              permissions: _permissions?.build(),
              name: name,
              description: description,
              selected: selected);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'permissions';
        _permissions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProjectPermissionGroupModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
