library ppg_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/card_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/list_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/member_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/project_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/sub_project_permission_model.dart';

part 'ppg_post_model.g.dart';

abstract class PpgPostModel
    implements Built<PpgPostModel, PpgPostModelBuilder> {
  @nullable
  ProjectPermissionModel get project;

  @nullable
  CardPermissionModel get cards;

  @nullable
  ListPermissionModel get list;

  @nullable
  SubProjectPermissionModel get subproject;

  @nullable
  MemberPermissionModel get member;

  @nullable
  String get name;

  @nullable
  String get description;

  PpgPostModel._();

  factory PpgPostModel([updates(PpgPostModelBuilder b)]) = _$PpgPostModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(PpgPostModel.serializer, this));
  }

  static PpgPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        PpgPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<PpgPostModel> get serializer => _$ppgPostModelSerializer;
}
