library permissions_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/card_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/list_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/member_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/project_permission_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/permission/sub_project_permission_model.dart';

part 'permissions_model.g.dart';

abstract class PermissionsModel
    implements Built<PermissionsModel, PermissionsModelBuilder> {
  @nullable
  ProjectPermissionModel get project;

  @nullable
  CardPermissionModel get cards;

  @nullable
  ListPermissionModel get list;

  @nullable
  SubProjectPermissionModel get subproject;

  @nullable
  MemberPermissionModel get member;

  PermissionsModel._();

  factory PermissionsModel([updates(PermissionsModelBuilder b)]) =
      _$PermissionsModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(PermissionsModel.serializer, this));
  }

  static PermissionsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        PermissionsModel.serializer, json.decode(jsonString));
  }

  static Serializer<PermissionsModel> get serializer =>
      _$permissionsModelSerializer;
}
