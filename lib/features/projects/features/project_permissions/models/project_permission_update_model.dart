library project_permission_update_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'project_permission_group_model.dart';

part 'project_permission_update_model.g.dart';

abstract class ProjectPermissionUpdateModel
    implements
        Built<ProjectPermissionUpdateModel,
            ProjectPermissionUpdateModelBuilder> {
  @nullable
  ProjectPermissionGroupModel get ppgObj;

  ProjectPermissionUpdateModel._();

  factory ProjectPermissionUpdateModel(
          [updates(ProjectPermissionUpdateModelBuilder b)]) =
      _$ProjectPermissionUpdateModel;

  String toJson() {
    return json.encode(serializers.serializeWith(
        ProjectPermissionUpdateModel.serializer, this));
  }

  static ProjectPermissionUpdateModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectPermissionUpdateModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectPermissionUpdateModel> get serializer =>
      _$projectPermissionUpdateModelSerializer;
}
