library project_permission_role_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_group_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'project_permission_role_model.g.dart';

abstract class ProjectPermissionRoleModel
    implements
        Built<ProjectPermissionRoleModel, ProjectPermissionRoleModelBuilder> {
  @nullable
  String get idMember;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  ProjectPermissionGroupModel get permission;

  ProjectPermissionRoleModel._();

  factory ProjectPermissionRoleModel(
          [updates(ProjectPermissionRoleModelBuilder b)]) =
      _$ProjectPermissionRoleModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(ProjectPermissionRoleModel.serializer, this));
  }

  static ProjectPermissionRoleModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ProjectPermissionRoleModel.serializer, json.decode(jsonString));
  }

  static Serializer<ProjectPermissionRoleModel> get serializer =>
      _$projectPermissionRoleModelSerializer;
}
