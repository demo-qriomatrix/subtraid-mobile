// GENERATED CODE - DO NOT MODIFY BY HAND

part of ppg_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PpgPostModel> _$ppgPostModelSerializer =
    new _$PpgPostModelSerializer();

class _$PpgPostModelSerializer implements StructuredSerializer<PpgPostModel> {
  @override
  final Iterable<Type> types = const [PpgPostModel, _$PpgPostModel];
  @override
  final String wireName = 'PpgPostModel';

  @override
  Iterable<Object> serialize(Serializers serializers, PpgPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.project != null) {
      result
        ..add('project')
        ..add(serializers.serialize(object.project,
            specifiedType: const FullType(ProjectPermissionModel)));
    }
    if (object.cards != null) {
      result
        ..add('cards')
        ..add(serializers.serialize(object.cards,
            specifiedType: const FullType(CardPermissionModel)));
    }
    if (object.list != null) {
      result
        ..add('list')
        ..add(serializers.serialize(object.list,
            specifiedType: const FullType(ListPermissionModel)));
    }
    if (object.subproject != null) {
      result
        ..add('subproject')
        ..add(serializers.serialize(object.subproject,
            specifiedType: const FullType(SubProjectPermissionModel)));
    }
    if (object.member != null) {
      result
        ..add('member')
        ..add(serializers.serialize(object.member,
            specifiedType: const FullType(MemberPermissionModel)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  PpgPostModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PpgPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'project':
          result.project.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ProjectPermissionModel))
              as ProjectPermissionModel);
          break;
        case 'cards':
          result.cards.replace(serializers.deserialize(value,
                  specifiedType: const FullType(CardPermissionModel))
              as CardPermissionModel);
          break;
        case 'list':
          result.list.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ListPermissionModel))
              as ListPermissionModel);
          break;
        case 'subproject':
          result.subproject.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SubProjectPermissionModel))
              as SubProjectPermissionModel);
          break;
        case 'member':
          result.member.replace(serializers.deserialize(value,
                  specifiedType: const FullType(MemberPermissionModel))
              as MemberPermissionModel);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PpgPostModel extends PpgPostModel {
  @override
  final ProjectPermissionModel project;
  @override
  final CardPermissionModel cards;
  @override
  final ListPermissionModel list;
  @override
  final SubProjectPermissionModel subproject;
  @override
  final MemberPermissionModel member;
  @override
  final String name;
  @override
  final String description;

  factory _$PpgPostModel([void Function(PpgPostModelBuilder) updates]) =>
      (new PpgPostModelBuilder()..update(updates)).build();

  _$PpgPostModel._(
      {this.project,
      this.cards,
      this.list,
      this.subproject,
      this.member,
      this.name,
      this.description})
      : super._();

  @override
  PpgPostModel rebuild(void Function(PpgPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PpgPostModelBuilder toBuilder() => new PpgPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PpgPostModel &&
        project == other.project &&
        cards == other.cards &&
        list == other.list &&
        subproject == other.subproject &&
        member == other.member &&
        name == other.name &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, project.hashCode), cards.hashCode),
                        list.hashCode),
                    subproject.hashCode),
                member.hashCode),
            name.hashCode),
        description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PpgPostModel')
          ..add('project', project)
          ..add('cards', cards)
          ..add('list', list)
          ..add('subproject', subproject)
          ..add('member', member)
          ..add('name', name)
          ..add('description', description))
        .toString();
  }
}

class PpgPostModelBuilder
    implements Builder<PpgPostModel, PpgPostModelBuilder> {
  _$PpgPostModel _$v;

  ProjectPermissionModelBuilder _project;
  ProjectPermissionModelBuilder get project =>
      _$this._project ??= new ProjectPermissionModelBuilder();
  set project(ProjectPermissionModelBuilder project) =>
      _$this._project = project;

  CardPermissionModelBuilder _cards;
  CardPermissionModelBuilder get cards =>
      _$this._cards ??= new CardPermissionModelBuilder();
  set cards(CardPermissionModelBuilder cards) => _$this._cards = cards;

  ListPermissionModelBuilder _list;
  ListPermissionModelBuilder get list =>
      _$this._list ??= new ListPermissionModelBuilder();
  set list(ListPermissionModelBuilder list) => _$this._list = list;

  SubProjectPermissionModelBuilder _subproject;
  SubProjectPermissionModelBuilder get subproject =>
      _$this._subproject ??= new SubProjectPermissionModelBuilder();
  set subproject(SubProjectPermissionModelBuilder subproject) =>
      _$this._subproject = subproject;

  MemberPermissionModelBuilder _member;
  MemberPermissionModelBuilder get member =>
      _$this._member ??= new MemberPermissionModelBuilder();
  set member(MemberPermissionModelBuilder member) => _$this._member = member;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  PpgPostModelBuilder();

  PpgPostModelBuilder get _$this {
    if (_$v != null) {
      _project = _$v.project?.toBuilder();
      _cards = _$v.cards?.toBuilder();
      _list = _$v.list?.toBuilder();
      _subproject = _$v.subproject?.toBuilder();
      _member = _$v.member?.toBuilder();
      _name = _$v.name;
      _description = _$v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PpgPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PpgPostModel;
  }

  @override
  void update(void Function(PpgPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PpgPostModel build() {
    _$PpgPostModel _$result;
    try {
      _$result = _$v ??
          new _$PpgPostModel._(
              project: _project?.build(),
              cards: _cards?.build(),
              list: _list?.build(),
              subproject: _subproject?.build(),
              member: _member?.build(),
              name: name,
              description: description);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'project';
        _project?.build();
        _$failedField = 'cards';
        _cards?.build();
        _$failedField = 'list';
        _list?.build();
        _$failedField = 'subproject';
        _subproject?.build();
        _$failedField = 'member';
        _member?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PpgPostModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
