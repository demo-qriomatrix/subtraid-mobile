library company_info_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'project_company_info_model.dart';

part 'company_info_response_model.g.dart';

abstract class CompanyInfoResponseModel
    implements
        Built<CompanyInfoResponseModel, CompanyInfoResponseModelBuilder> {
  @nullable
  ProjectCompanyInfoModel get companyInfo;

  @nullable
  String get message;

  CompanyInfoResponseModel._();

  factory CompanyInfoResponseModel(
          [updates(CompanyInfoResponseModelBuilder b)]) =
      _$CompanyInfoResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(CompanyInfoResponseModel.serializer, this));
  }

  static CompanyInfoResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CompanyInfoResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<CompanyInfoResponseModel> get serializer =>
      _$companyInfoResponseModelSerializer;
}
