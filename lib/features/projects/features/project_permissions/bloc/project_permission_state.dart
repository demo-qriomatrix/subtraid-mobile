part of 'project_permission_bloc.dart';

abstract class ProjectPermissionState extends Equatable {
  final int selectedIndex;

  ProjectPermissionState({@required this.selectedIndex});

  @override
  List<Object> get props => [selectedIndex];
}

class ProjectPermissionInitial extends ProjectPermissionState {
  ProjectPermissionInitial({@required int selectedIndex})
      : super(selectedIndex: selectedIndex);

  @override
  List<Object> get props => [selectedIndex];
}

class ProjectPermissionLoadInProgress extends ProjectPermissionState {
  ProjectPermissionLoadInProgress({@required int selectedIndex})
      : super(selectedIndex: selectedIndex);

  @override
  List<Object> get props => [selectedIndex];
}

class ProjectPermissionLoadSuccess extends ProjectPermissionState {
  final ProjectCompanyInfoModel companyInfoModel;

  ProjectPermissionLoadSuccess(
      {@required int selectedIndex, @required this.companyInfoModel})
      : super(selectedIndex: selectedIndex);

  @override
  List<Object> get props => [selectedIndex, companyInfoModel];
}

class ProjectPermissionLoadFailure extends ProjectPermissionState {
  ProjectPermissionLoadFailure({@required int selectedIndex})
      : super(selectedIndex: selectedIndex);

  @override
  List<Object> get props => [selectedIndex];
}
