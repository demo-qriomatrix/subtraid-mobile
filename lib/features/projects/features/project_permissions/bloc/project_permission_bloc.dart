import 'dart:async';
import 'dart:convert';

import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/company_info_response_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/ppg_post_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_company_info_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_group_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/models/project_permission_update_model.dart';
import 'package:Subtraid/features/projects/features/project_permissions/repository/project_permission_repository.dart';
import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:built_collection/built_collection.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'project_permission_event.dart';
part 'project_permission_state.dart';

class ProjectPermissionBloc
    extends Bloc<ProjectPermissionEvent, ProjectPermissionState> {
  ProjectPermissionBloc({
    @required this.projectPermissionRepository,
    @required this.globalBloc,
  }) : super(ProjectPermissionInitial(selectedIndex: 0)) {
    add(ProjectPermissionRequested());
  }

  final GlobalBloc globalBloc;
  final ProjectPermissionRepository projectPermissionRepository;

  @override
  Stream<ProjectPermissionState> mapEventToState(
    ProjectPermissionEvent event,
  ) async* {
    if (event is ProjectPermissionRequested) {
      yield* _mapProjectPermissionRequestedToState();
    }

    if (event is ProjectPermissionIndexChanged) {
      yield* _mapProjectPermissionIndexChangedToState(event);
    }

    if (event is CreateProjectPermissionGroup) {
      yield* _mapCreateProjectPermissionGroupToState(event.permissonGroupModel);
    }

    if (event is UpdateProjectPermissionGroup) {
      yield* _mapUpdateProjectPermissionGroupToState(event.permissonGroupModel);
    }

    if (event is ProjectPermissionGroupSetSelected) {
      yield* _mapProjectPermissionGroupSetSelectedToState(event);
    }
  }

  Stream<ProjectPermissionState> _mapProjectPermissionGroupSetSelectedToState(
      ProjectPermissionGroupSetSelected event) async* {
    if (state is ProjectPermissionLoadSuccess) {
      List<ProjectPermissionGroupModel> previousProjectPermissionGroupModel =
          (state as ProjectPermissionLoadSuccess)
              .companyInfoModel
              .company
              .projectPermissionGroups
              .toList();

      int index = previousProjectPermissionGroupModel
          .indexWhere((element) => element.id == event.id);

      ProjectPermissionGroupModel projectPermissionGroupModel =
          previousProjectPermissionGroupModel[index];

      projectPermissionGroupModel = projectPermissionGroupModel
          .rebuild((t) => t..selected = event.selected);

      previousProjectPermissionGroupModel[index] = projectPermissionGroupModel;

      List<ProjectPermissionGroupModel> updatedTimesheets =
          List.from(previousProjectPermissionGroupModel.toList());

      ProjectCompanyInfoModel projectCompanyInfoModel = (state
              as ProjectPermissionLoadSuccess)
          .companyInfoModel
          .rebuild((e) => e.company = (state as ProjectPermissionLoadSuccess)
              .companyInfoModel
              .company
              .rebuild((f) =>
                  f..projectPermissionGroups = ListBuilder(updatedTimesheets))
              .toBuilder());

      yield ProjectPermissionLoadSuccess(
          companyInfoModel: projectCompanyInfoModel,
          selectedIndex: state.selectedIndex);
    }
  }

  Stream<ProjectPermissionState> _mapUpdateProjectPermissionGroupToState(
      ProjectPermissionUpdateModel event) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      await projectPermissionRepository.updateProjectGroupPermission(event);

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'User Role updated successfully',
          event: SucceessEvents.UserRole));

      add(ProjectPermissionRequested());
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);

      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<ProjectPermissionState> _mapProjectPermissionIndexChangedToState(
      ProjectPermissionIndexChanged event) async* {
    if (state is ProjectPermissionLoadSuccess)
      yield ProjectPermissionLoadSuccess(
          selectedIndex: event.index,
          companyInfoModel:
              (state as ProjectPermissionLoadSuccess).companyInfoModel);
    if (state is ProjectPermissionInitial)
      yield ProjectPermissionInitial(selectedIndex: event.index);
    if (state is ProjectPermissionLoadFailure)
      yield ProjectPermissionLoadFailure(selectedIndex: event.index);
  }

  Stream<ProjectPermissionState>
      _mapProjectPermissionRequestedToState() async* {
    try {
      // globalBloc.add(ShowLoadingWidget());

      CompanyInfoResponseModel companyInfoResponseModel =
          await projectPermissionRepository.getProjectPermissions();

      yield ProjectPermissionLoadSuccess(
          selectedIndex: 0,
          companyInfoModel: companyInfoResponseModel.companyInfo);

      // globalBloc.add(HideLoadingWidget());
    } catch (e) {
      // globalBloc.add(HideLoadingWidget());
      yield ProjectPermissionLoadFailure(selectedIndex: state.selectedIndex);
    }
  }

  Stream<ProjectPermissionState> _mapCreateProjectPermissionGroupToState(
      PpgPostModel permissonGroupModel) async* {
    try {
      globalBloc.add(ShowLoadingWidget());

      await projectPermissionRepository
          .createProjectGroupPermission(permissonGroupModel);

      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'User Role created successfully',
          event: SucceessEvents.UserRole));

      add(ProjectPermissionRequested());
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);

      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }
}
