part of 'project_permission_bloc.dart';

abstract class ProjectPermissionEvent extends Equatable {
  const ProjectPermissionEvent();

  @override
  List<Object> get props => [];
}

class ProjectPermissionIndexChanged extends ProjectPermissionEvent {
  final int index;

  ProjectPermissionIndexChanged({@required this.index});
}

class ProjectPermissionRequested extends ProjectPermissionEvent {}

class CreateProjectPermissionGroup extends ProjectPermissionEvent {
  final PpgPostModel permissonGroupModel;

  CreateProjectPermissionGroup({@required this.permissonGroupModel});
}

class UpdateProjectPermissionGroup extends ProjectPermissionEvent {
  final ProjectPermissionUpdateModel permissonGroupModel;

  UpdateProjectPermissionGroup({@required this.permissonGroupModel});
}

class ProjectPermissionGroupSetSelected extends ProjectPermissionEvent {
  final bool selected;
  final String id;

  ProjectPermissionGroupSetSelected(
      {@required this.id, @required this.selected});
}
