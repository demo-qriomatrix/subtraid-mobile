part of 'projects_bloc.dart';

class ProjectsState extends Equatable {
  final int selectedIndex;

  ProjectsState({@required this.selectedIndex});

  @override
  List<Object> get props => [selectedIndex];
}
