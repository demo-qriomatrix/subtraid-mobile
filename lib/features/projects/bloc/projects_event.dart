part of 'projects_bloc.dart';

abstract class ProjectsEvent extends Equatable {
  const ProjectsEvent();

  @override
  List<Object> get props => [];
}

class ProjectsIndexChanged extends ProjectsEvent {
  final int index;

  ProjectsIndexChanged({@required this.index});
}
