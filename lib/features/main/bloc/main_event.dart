part of 'main_bloc.dart';

abstract class MainEvent extends Equatable {
  const MainEvent();

  @override
  List<Object> get props => [];
}

class MainIndexChanged extends MainEvent {
  final int index;

  MainIndexChanged({@required this.index});
}
