part of 'main_bloc.dart';

class MainState extends Equatable {
  final int selectedIndex;

  MainState({
    @required this.selectedIndex,
  });

  @override
  List<Object> get props => [selectedIndex];
}
