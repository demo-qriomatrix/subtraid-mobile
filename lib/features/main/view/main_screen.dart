import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/core/global/bloc/event_enum.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/core/helpers/get_company_user.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/chat/bloc/chat_bloc.dart';
import 'package:Subtraid/features/main/bloc/main_bloc.dart';
import 'package:Subtraid/features/main/view/screens.dart';
import 'package:Subtraid/features/more/bloc/more_bloc.dart';
import 'package:Subtraid/features/more/company/features/employee/bloc/employee_bloc.dart';
import 'package:Subtraid/features/notifications/bloc/notifications_bloc.dart';
import 'package:Subtraid/features/projects/features/project/bloc/project_bloc.dart';
import 'package:Subtraid/features/projects/features/project/feataures/card/bloc/card_bloc.dart';
import 'package:Subtraid/features/projects/features/project_permissions/bloc/project_permission_bloc.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/drawer_item.dart';
import 'package:Subtraid/features/shared/widgets/nav_bar.dart';
import 'package:Subtraid/features/splash/view/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';

GlobalKey<NavigatorState> secondNavigatorKey = GlobalKey<NavigatorState>();

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  String appVersion = '';

  @override
  void initState() {
    super.initState();

    getVersion();
  }

  void getVersion() async {
    PackageInfo info = await PackageInfo.fromPlatform();

    setState(() {
      appVersion = info.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is UnauthentcatedState) {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => SplashScreen(),
              ));
            }
          },
        ),
        BlocListener<GlobalBloc, GlobalState>(
          listener: (context, state) {
            if (state is ShowLoadingState) {
              showDialog(
                  useRootNavigator: false,
                  barrierDismissible: false,
                  context: context,
                  builder: (context) => Center(
                        child: SizedBox(
                            width: 150,
                            child: Center(
                              child: CircularProgressIndicator(),
                            )),
                      ));
            }
            var snackBar = (Color color, String msg) {
              _scaffoldState.currentState.removeCurrentSnackBar();
              _scaffoldState.currentState.showSnackBar(SnackBar(
                backgroundColor: color,
                content: Text(
                  msg,
                  style: TextStyle(color: Colors.white),
                ),
              ));
            };
            if (state is HideLoadingState) {
              if (Navigator.of(context).canPop()) Navigator.of(context).pop();
            } else if (state is ErrorSnackBarState) {
              snackBar(Colors.red, state.error);
            } else if (state is SuccessSnackBarState) {
              snackBar(Colors.green, state.message);

              if (state.event == SucceessEvents.AddProject) {
                navKeys[0].currentState.pop();
              }
            }

            if (state is LogoutEvent) {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => SplashScreen(),
              ));
            }
          },
        ),
      ],
      child: MultiBlocProvider(
          providers: [
            BlocProvider<NotificationsBloc>(
              create: (context) =>
                  NotificationsBloc(notificationRepository: sl()),
              lazy: false,
            ),
            BlocProvider<MainBloc>(
              create: (context) => MainBloc(),
              lazy: false,
            ),
            BlocProvider<MoreBloc>(
              create: (context) => MoreBloc(),
              lazy: false,
            ),
            BlocProvider<ChatBloc>(
                lazy: false,
                create: (context) => ChatBloc(
                    chatRepository: sl(),
                    authenticationBloc: context.read<AuthenticationBloc>())),
            BlocProvider<ProjectBloc>(
              lazy: false,
              create: (context) => ProjectBloc(
                  projectRepository: sl(),
                  subProjectRepository: sl(),
                  calenderRepository: sl(),
                  projectsRepository: sl(),
                  labelRepository: sl(),
                  authenticationBloc: context.read<AuthenticationBloc>(),
                  listRepository: sl(),
                  projectsTimesheetsRepository: sl(),
                  cardRepository: sl(),
                  globalBloc: context.read<GlobalBloc>()),
            ),
            BlocProvider<CardBloc>(
              create: (context) => CardBloc(
                projectBloc: context.read<ProjectBloc>(),
                authenticationBloc: context.read<AuthenticationBloc>(),
              ),
            ),
            BlocProvider<EmployeeBloc>(
              lazy: false,
              create: (context) => EmployeeBloc(employeeRepository: sl()),
            ),
            BlocProvider<ProjectPermissionBloc>(
                lazy: false,
                create: (context) => ProjectPermissionBloc(
                    projectPermissionRepository: sl(),
                    globalBloc: context.read<GlobalBloc>()))
          ],
          child: Navigator(
            key: secondNavigatorKey,
            onGenerateRoute: (settings) {
              return MaterialPageRoute(
                builder: (context) =>
                    BlocBuilder<MainBloc, MainState>(builder: (context, state) {
                  return Scaffold(
                    key: _scaffoldState,
                    backgroundColor: ColorConfig.primary,
                    endDrawer: Drawer(
                      child: Container(
                        color: ColorConfig.primary,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: MediaQuery.of(context).padding.top,
                            ),
                            Expanded(child: BlocBuilder<MoreBloc, MoreState>(
                              builder: (context, state) {
                                return Column(
                                  children: <Widget>[
                                    Flexible(
                                      fit: FlexFit.tight,
                                      flex: 1,
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: <Widget>[
                                              RawMaterialButton(
                                                constraints: BoxConstraints(),
                                                shape: CircleBorder(),
                                                child: Icon(
                                                  Icons.close,
                                                  color: Colors.white,
                                                  size: 28,
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                        fit: FlexFit.tight,
                                        flex: 3,
                                        child: BlocBuilder<AuthenticationBloc,
                                            AuthenticationState>(
                                          builder: (context, state) {
                                            if (state is AuthentcatedState)
                                              return GestureDetector(
                                                onTap: () {
                                                  context.read<MainBloc>().add(
                                                      MainIndexChanged(
                                                          index: 4));

                                                  context.read<MoreBloc>().add(
                                                      MoreIndexChanged(
                                                          index: 3));

                                                  if (Navigator.of(context)
                                                      .canPop())
                                                    Navigator.of(context).pop();
                                                },
                                                child: Column(
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      backgroundImage:
                                                          AssetImage(LocalImages
                                                              .defaultAvatar),
                                                      radius: 40,
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Text(
                                                      state.user.name,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 16),
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      state.user.email,
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    )
                                                  ],
                                                ),
                                              );

                                            return Container();
                                          },
                                        )),
                                    Flexible(
                                        fit: FlexFit.tight,
                                        flex: 8,
                                        child: Column(
                                          children: <Widget>[
                                            // DrawerItem(
                                            //   selected:
                                            //       state.selectedIndex == 0,
                                            //   callback: () {
                                            //     if (moreNavKeys[0]
                                            //                 .currentState !=
                                            //             null &&
                                            //         moreNavKeys[0]
                                            //             .currentState
                                            //             .canPop()) {
                                            //       moreNavKeys[0]
                                            //           .currentState
                                            //           .pop();
                                            //     }

                                            //     context.read<MoreBloc>().add(
                                            //         MoreIndexChanged(index: 0));

                                            //     if (Navigator.of(context)
                                            //         .canPop())
                                            //       Navigator.of(context).pop();
                                            //   },
                                            //   icon: LocalImages.folder,
                                            //   title: 'Projects',
                                            // ),

                                            getCompanyUser(context
                                                    .read<AuthenticationBloc>())
                                                ? DrawerItem(
                                                    selected:
                                                        state.selectedIndex ==
                                                            0,
                                                    callback: () {
                                                      context
                                                          .read<MainBloc>()
                                                          .add(MainIndexChanged(
                                                              index: 4));

                                                      context
                                                          .read<MoreBloc>()
                                                          .add(MoreIndexChanged(
                                                              index: 0));

                                                      if (Navigator.of(context)
                                                          .canPop())
                                                        Navigator.of(context)
                                                            .pop();
                                                    },
                                                    icon: LocalImages.building,
                                                    title: 'Company',
                                                  )
                                                : Container(),
                                            getCompanyUser(context
                                                    .read<AuthenticationBloc>())
                                                ? DrawerItem(
                                                    selected:
                                                        state.selectedIndex ==
                                                            1,
                                                    callback: () {
                                                      context
                                                          .read<MainBloc>()
                                                          .add(MainIndexChanged(
                                                              index: 4));

                                                      context
                                                          .read<MoreBloc>()
                                                          .add(MoreIndexChanged(
                                                              index: 1));

                                                      if (Navigator.of(context)
                                                          .canPop())
                                                        Navigator.of(context)
                                                            .pop();
                                                    },
                                                    icon: LocalImages.history,
                                                    title: 'Timesheets',
                                                  )
                                                : Container(),
                                            // DrawerItem(
                                            //   selected:
                                            //       state.selectedIndex == 3,
                                            //   callback: () {
                                            //     context.read<MoreBloc>().add(
                                            //         MoreIndexChanged(index: 3));

                                            //     if (Navigator.of(context)
                                            //         .canPop())
                                            //       Navigator.of(context).pop();
                                            //   },
                                            //   icon: LocalImages.calenderAlt,
                                            //   title: 'Calendar',
                                            // ),
                                            DrawerItem(
                                              selected:
                                                  state.selectedIndex == 2,
                                              callback: () {
                                                context.read<MainBloc>().add(
                                                    MainIndexChanged(index: 4));

                                                context.read<MoreBloc>().add(
                                                    MoreIndexChanged(index: 2));

                                                if (Navigator.of(context)
                                                    .canPop())
                                                  Navigator.of(context).pop();
                                              },
                                              icon: LocalImages.questionCircle,
                                              title: 'Help',
                                            ),
                                            Spacer(),
                                            Divider(),
                                            GestureDetector(
                                              onTap: () {
                                                context
                                                    .read<AuthenticationBloc>()
                                                    .add(LogoutEvent());
                                              },
                                              child: Text(
                                                'Logout',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 48,
                                            ),
                                            Text(
                                              appVersion,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 24,
                                            ),
                                          ],
                                        ))
                                  ],
                                );
                              },
                            ))
                          ],
                        ),
                      ),
                    ),
                    body: Column(children: <Widget>[
                      Expanded(
                        child: Container(
                          // child: currentPage,
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Offstage(
                                offstage: state.selectedIndex != 0,
                                child: pageList[0],
                              ),
                              Offstage(
                                offstage: state.selectedIndex != 1,
                                child: pageList[1],
                              ),
                              Offstage(
                                offstage: state.selectedIndex != 2,
                                child: pageList[2],
                              ),
                              Offstage(
                                offstage: state.selectedIndex != 3,
                                child: pageList[3],
                              ),
                              Offstage(
                                offstage: state.selectedIndex != 4,
                                child: pageList[4],
                              ),
                              // Offstage(
                              //   offstage: state.selectedIndex != 5,
                              //   child: pageList[5],
                              // ),
                            ],
                          ),
                          // margin: EdgeInsets.only(bottom: 70),
                        ),
                      ),
                      Container(
                        height: bottomNavbarHeight +
                            MediaQuery.of(context).padding.bottom * 0.3,
                        width: MediaQuery.of(context).size.width,
                        child: CustomBottomNavBar(
                          onPressed: (index) {
                            // _selectTab(index);
                            //

                            if (index == 4) {
                              _scaffoldState.currentState.openEndDrawer();
                            } else {
                              context
                                  .read<MainBloc>()
                                  .add(MainIndexChanged(index: index));

                              context
                                  .read<MoreBloc>()
                                  .add(MoreIndexChanged(index: null));
                            }
                          },
                        ),
                      ),
                    ]),
                  );
                }),
              );
            },
          )),
    );
  }
}
