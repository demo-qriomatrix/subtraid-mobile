import 'package:Subtraid/features/chat/view/chats_screen.dart';
import 'package:Subtraid/features/contacts/view/contacts_screen.dart';
import 'package:Subtraid/features/calender/view/calender_screen.dart';
import 'package:Subtraid/features/projects/view/projects_screen.dart';
import 'package:Subtraid/features/more/view/more_screen.dart';
import 'package:flutter/material.dart';

List<GlobalKey<NavigatorState>> navKeys = [];

List<Widget> pageList = [
  ProjectsScreen(),
  CalenderScreen(),
  ContactsScreen(
    index: 0,
  ),
  ChatsScreen(),
  MoreScreen(),
].map((e) {
  GlobalKey<NavigatorState> key = GlobalKey<NavigatorState>();
  print(e);
  navKeys.add(key);

  return Navigator(
      key: key,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(builder: (context) {
          return e;
        });
      });
}).toList();
