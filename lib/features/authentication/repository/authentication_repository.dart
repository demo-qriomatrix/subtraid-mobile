import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/authentication/models/access_token_model.dart';
import 'package:Subtraid/features/authentication/models/access_token_response_model.dart';
import 'package:Subtraid/features/authentication/models/credential_model.dart';
import 'package:Subtraid/features/authentication/models/register_post_model.dart';
import 'package:Subtraid/features/authentication/models/update_password_post_model.dart';
import 'package:Subtraid/features/authentication/models/user_response_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String KEY_TOKEN = "KEY_TOKEN";

class AuthenticationRepository {
  final HttpClient httpClient;
  SharedPreferences sharedPreferences;

  AuthenticationRepository({@required this.httpClient})
      : assert(httpClient != null);

  Future<SharedPreferences> getSharedPreferencesObject() async {
    if (sharedPreferences != null)
      return sharedPreferences;
    else
      return await SharedPreferences.getInstance();
  }

  Future<AccessTokenModel> getAccessToken(
      CredentialModel credentialModel) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.userLogin, data: credentialModel.toJson());

    AccessTokenResponseModel accessTokenResponseModel =
        AccessTokenResponseModel.fromJson(json.encode(response.data));
    AccessTokenModel accessTokenModel = accessTokenResponseModel.returnToken;

    httpClient.addTokenInterceptor(accessTokenModel.token);
    saveTokenToLocalStorage(accessTokenModel.token);

    return accessTokenModel;
  } // getAccessToken

  Future<String> checkLocalTokenAndAddInterceptor() async {
    String token = await readTokenFromLocalStorage();
    if (token != null) {
      httpClient.addTokenInterceptor(token);
      return token;
    }
    return null;
  } // checkLocalTokenAndAddInterceptor

  Future<void> removeLocalTokenAndInterceptor() async {
    await removeTokenFromLocalStorage();
    httpClient.removeTokenInterceptor();
    return null;
  } // removeLocalTokenAndInterceptor

  void saveTokenToLocalStorage(String token) async {
    sharedPreferences = await getSharedPreferencesObject();
    sharedPreferences.setString(KEY_TOKEN, token);
  } // saveTokenToLocalStorage()

  Future<String> readTokenFromLocalStorage() async {
    sharedPreferences = await getSharedPreferencesObject();
    return sharedPreferences.getString(KEY_TOKEN);
  } // readTokenFromLocalStorage

  Future<bool> removeTokenFromLocalStorage() async {
    sharedPreferences = await getSharedPreferencesObject();
    return sharedPreferences.remove(KEY_TOKEN);
  } // removeTokenFromLocalStorage

  Future<UserResponseModel> getUser(String userId) async {
    Response response =
        await httpClient.dio.get(EndpointConfig.userUrl + '/$userId');

    return UserResponseModel.fromJson(json.encode(response.data));
  }

  Future<Response> registerUser(RegisterPostModel data) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.userRegister, data: data.toJson());

    return response;
  }

  Future<Response> updatePassword(UpdatePasswordPostModel data) async {
    Response response = await httpClient.dio
        .post(EndpointConfig.userUpdatePass, data: data.toJson());

    return response;
  }
}
