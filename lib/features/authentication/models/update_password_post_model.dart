library update_password_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'update_password_post_model.g.dart';

abstract class UpdatePasswordPostModel
    implements Built<UpdatePasswordPostModel, UpdatePasswordPostModelBuilder> {
  @nullable
  String get newpwd;

  @nullable
  String get pwd;

  UpdatePasswordPostModel._();

  factory UpdatePasswordPostModel([updates(UpdatePasswordPostModelBuilder b)]) =
      _$UpdatePasswordPostModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(UpdatePasswordPostModel.serializer, this));
  }

  static UpdatePasswordPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UpdatePasswordPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<UpdatePasswordPostModel> get serializer =>
      _$updatePasswordPostModelSerializer;
}
