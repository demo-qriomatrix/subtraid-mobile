library access_token_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'access_token_model.dart';

part 'access_token_response_model.g.dart';

abstract class AccessTokenResponseModel
    implements
        Built<AccessTokenResponseModel, AccessTokenResponseModelBuilder> {
  @nullable
  @BuiltValueField(wireName: 'return')
  AccessTokenModel get returnToken;

  @nullable
  String get message;

  @nullable
  int get status;

  AccessTokenResponseModel._();

  factory AccessTokenResponseModel(
          [updates(AccessTokenResponseModelBuilder b)]) =
      _$AccessTokenResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(AccessTokenResponseModel.serializer, this));
  }

  static AccessTokenResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AccessTokenResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<AccessTokenResponseModel> get serializer =>
      _$accessTokenResponseModelSerializer;
}
