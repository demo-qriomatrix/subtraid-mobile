library credential_model.dart;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'credential_model.g.dart';

abstract class CredentialModel
    implements Built<CredentialModel, CredentialModelBuilder> {
  @nullable
  String get email;

  @nullable
  String get pwd;

  CredentialModel._();

  factory CredentialModel([updates(CredentialModelBuilder b)]) =
      _$CredentialModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CredentialModel.serializer, this));
  }

  static CredentialModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        CredentialModel.serializer, json.decode(jsonString));
  }

  static Serializer<CredentialModel> get serializer =>
      _$credentialModelSerializer;
}
