// GENERATED CODE - DO NOT MODIFY BY HAND

part of user_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserModel> _$userModelSerializer = new _$UserModelSerializer();

class _$UserModelSerializer implements StructuredSerializer<UserModel> {
  @override
  final Iterable<Type> types = const [UserModel, _$UserModel];
  @override
  final String wireName = 'UserModel';

  @override
  Iterable<Object> serialize(Serializers serializers, UserModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.accountType != null) {
      result
        ..add('accountType')
        ..add(serializers.serialize(object.accountType,
            specifiedType: const FullType(String)));
    }
    if (object.employer != null) {
      result
        ..add('employer')
        ..add(serializers.serialize(object.employer,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(JsonObject)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.skills != null) {
      result
        ..add('skills')
        ..add(serializers.serialize(object.skills,
            specifiedType:
                const FullType(BuiltList, const [const FullType(SkillModel)])));
    }
    if (object.licences != null) {
      result
        ..add('licences')
        ..add(serializers.serialize(object.licences,
            specifiedType: const FullType(
                BuiltList, const [const FullType(LicenseModel)])));
    }
    if (object.experiences != null) {
      result
        ..add('experiences')
        ..add(serializers.serialize(object.experiences,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ExperienceModel)])));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    if (object.phone != null) {
      result
        ..add('phone')
        ..add(serializers.serialize(object.phone,
            specifiedType: const FullType(String)));
    }
    if (object.company != null) {
      result
        ..add('company')
        ..add(serializers.serialize(object.company,
            specifiedType: const FullType(UserCompanyModel)));
    }
    return result;
  }

  @override
  UserModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'accountType':
          result.accountType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'employer':
          result.employer = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'location':
          result.location = serializers.deserialize(value,
              specifiedType: const FullType(JsonObject)) as JsonObject;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'skills':
          result.skills.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SkillModel)]))
              as BuiltList<Object>);
          break;
        case 'licences':
          result.licences.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(LicenseModel)]))
              as BuiltList<Object>);
          break;
        case 'experiences':
          result.experiences.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ExperienceModel)]))
              as BuiltList<Object>);
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phone':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'company':
          result.company.replace(serializers.deserialize(value,
                  specifiedType: const FullType(UserCompanyModel))
              as UserCompanyModel);
          break;
      }
    }

    return result.build();
  }
}

class _$UserModel extends UserModel {
  @override
  final String name;
  @override
  final String accountType;
  @override
  final String employer;
  @override
  final String id;
  @override
  final JsonObject location;
  @override
  final String email;
  @override
  final String description;
  @override
  final BuiltList<SkillModel> skills;
  @override
  final BuiltList<LicenseModel> licences;
  @override
  final BuiltList<ExperienceModel> experiences;
  @override
  final String img;
  @override
  final String phone;
  @override
  final UserCompanyModel company;

  factory _$UserModel([void Function(UserModelBuilder) updates]) =>
      (new UserModelBuilder()..update(updates)).build();

  _$UserModel._(
      {this.name,
      this.accountType,
      this.employer,
      this.id,
      this.location,
      this.email,
      this.description,
      this.skills,
      this.licences,
      this.experiences,
      this.img,
      this.phone,
      this.company})
      : super._();

  @override
  UserModel rebuild(void Function(UserModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserModelBuilder toBuilder() => new UserModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserModel &&
        name == other.name &&
        accountType == other.accountType &&
        employer == other.employer &&
        id == other.id &&
        location == other.location &&
        email == other.email &&
        description == other.description &&
        skills == other.skills &&
        licences == other.licences &&
        experiences == other.experiences &&
        img == other.img &&
        phone == other.phone &&
        company == other.company;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc($jc(0, name.hashCode),
                                                    accountType.hashCode),
                                                employer.hashCode),
                                            id.hashCode),
                                        location.hashCode),
                                    email.hashCode),
                                description.hashCode),
                            skills.hashCode),
                        licences.hashCode),
                    experiences.hashCode),
                img.hashCode),
            phone.hashCode),
        company.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserModel')
          ..add('name', name)
          ..add('accountType', accountType)
          ..add('employer', employer)
          ..add('id', id)
          ..add('location', location)
          ..add('email', email)
          ..add('description', description)
          ..add('skills', skills)
          ..add('licences', licences)
          ..add('experiences', experiences)
          ..add('img', img)
          ..add('phone', phone)
          ..add('company', company))
        .toString();
  }
}

class UserModelBuilder implements Builder<UserModel, UserModelBuilder> {
  _$UserModel _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _accountType;
  String get accountType => _$this._accountType;
  set accountType(String accountType) => _$this._accountType = accountType;

  String _employer;
  String get employer => _$this._employer;
  set employer(String employer) => _$this._employer = employer;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  JsonObject _location;
  JsonObject get location => _$this._location;
  set location(JsonObject location) => _$this._location = location;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  ListBuilder<SkillModel> _skills;
  ListBuilder<SkillModel> get skills =>
      _$this._skills ??= new ListBuilder<SkillModel>();
  set skills(ListBuilder<SkillModel> skills) => _$this._skills = skills;

  ListBuilder<LicenseModel> _licences;
  ListBuilder<LicenseModel> get licences =>
      _$this._licences ??= new ListBuilder<LicenseModel>();
  set licences(ListBuilder<LicenseModel> licences) =>
      _$this._licences = licences;

  ListBuilder<ExperienceModel> _experiences;
  ListBuilder<ExperienceModel> get experiences =>
      _$this._experiences ??= new ListBuilder<ExperienceModel>();
  set experiences(ListBuilder<ExperienceModel> experiences) =>
      _$this._experiences = experiences;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  UserCompanyModelBuilder _company;
  UserCompanyModelBuilder get company =>
      _$this._company ??= new UserCompanyModelBuilder();
  set company(UserCompanyModelBuilder company) => _$this._company = company;

  UserModelBuilder();

  UserModelBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _accountType = _$v.accountType;
      _employer = _$v.employer;
      _id = _$v.id;
      _location = _$v.location;
      _email = _$v.email;
      _description = _$v.description;
      _skills = _$v.skills?.toBuilder();
      _licences = _$v.licences?.toBuilder();
      _experiences = _$v.experiences?.toBuilder();
      _img = _$v.img;
      _phone = _$v.phone;
      _company = _$v.company?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserModel;
  }

  @override
  void update(void Function(UserModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserModel build() {
    _$UserModel _$result;
    try {
      _$result = _$v ??
          new _$UserModel._(
              name: name,
              accountType: accountType,
              employer: employer,
              id: id,
              location: location,
              email: email,
              description: description,
              skills: _skills?.build(),
              licences: _licences?.build(),
              experiences: _experiences?.build(),
              img: img,
              phone: phone,
              company: _company?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'skills';
        _skills?.build();
        _$failedField = 'licences';
        _licences?.build();
        _$failedField = 'experiences';
        _experiences?.build();

        _$failedField = 'company';
        _company?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
