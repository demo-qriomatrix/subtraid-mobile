// GENERATED CODE - DO NOT MODIFY BY HAND

part of credential_model.dart;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CredentialModel> _$credentialModelSerializer =
    new _$CredentialModelSerializer();

class _$CredentialModelSerializer
    implements StructuredSerializer<CredentialModel> {
  @override
  final Iterable<Type> types = const [CredentialModel, _$CredentialModel];
  @override
  final String wireName = 'CredentialModel';

  @override
  Iterable<Object> serialize(Serializers serializers, CredentialModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.pwd != null) {
      result
        ..add('pwd')
        ..add(serializers.serialize(object.pwd,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CredentialModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CredentialModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'pwd':
          result.pwd = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$CredentialModel extends CredentialModel {
  @override
  final String email;
  @override
  final String pwd;

  factory _$CredentialModel([void Function(CredentialModelBuilder) updates]) =>
      (new CredentialModelBuilder()..update(updates)).build();

  _$CredentialModel._({this.email, this.pwd}) : super._();

  @override
  CredentialModel rebuild(void Function(CredentialModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CredentialModelBuilder toBuilder() =>
      new CredentialModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CredentialModel && email == other.email && pwd == other.pwd;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, email.hashCode), pwd.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CredentialModel')
          ..add('email', email)
          ..add('pwd', pwd))
        .toString();
  }
}

class CredentialModelBuilder
    implements Builder<CredentialModel, CredentialModelBuilder> {
  _$CredentialModel _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _pwd;
  String get pwd => _$this._pwd;
  set pwd(String pwd) => _$this._pwd = pwd;

  CredentialModelBuilder();

  CredentialModelBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _pwd = _$v.pwd;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CredentialModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CredentialModel;
  }

  @override
  void update(void Function(CredentialModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CredentialModel build() {
    final _$result = _$v ?? new _$CredentialModel._(email: email, pwd: pwd);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
