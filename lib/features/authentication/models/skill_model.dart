library skill_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'skill_model.g.dart';

abstract class SkillModel implements Built<SkillModel, SkillModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  int get from;

  @nullable
  int get to;

  SkillModel._();

  factory SkillModel([updates(SkillModelBuilder b)]) = _$SkillModel;

  String toJson() {
    return json.encode(serializers.serializeWith(SkillModel.serializer, this));
  }

  static SkillModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        SkillModel.serializer, json.decode(jsonString));
  }

  static Serializer<SkillModel> get serializer => _$skillModelSerializer;
}
