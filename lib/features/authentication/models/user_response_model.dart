library user_response_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';

part 'user_response_model.g.dart';

abstract class UserResponseModel
    implements Built<UserResponseModel, UserResponseModelBuilder> {
  @nullable
  UserModel get user;

  UserResponseModel._();

  factory UserResponseModel([updates(UserResponseModelBuilder b)]) =
      _$UserResponseModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(UserResponseModel.serializer, this));
  }

  static UserResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<UserResponseModel> get serializer =>
      _$userResponseModelSerializer;
}
