// GENERATED CODE - DO NOT MODIFY BY HAND

part of license_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LicenseModel> _$licenseModelSerializer =
    new _$LicenseModelSerializer();

class _$LicenseModelSerializer implements StructuredSerializer<LicenseModel> {
  @override
  final Iterable<Type> types = const [LicenseModel, _$LicenseModel];
  @override
  final String wireName = 'LicenseModel';

  @override
  Iterable<Object> serialize(Serializers serializers, LicenseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.yearExpire != null) {
      result
        ..add('yearExpire')
        ..add(serializers.serialize(object.yearExpire,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  LicenseModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LicenseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'yearExpire':
          result.yearExpire = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$LicenseModel extends LicenseModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final int yearExpire;

  factory _$LicenseModel([void Function(LicenseModelBuilder) updates]) =>
      (new LicenseModelBuilder()..update(updates)).build();

  _$LicenseModel._({this.id, this.name, this.yearExpire}) : super._();

  @override
  LicenseModel rebuild(void Function(LicenseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LicenseModelBuilder toBuilder() => new LicenseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LicenseModel &&
        id == other.id &&
        name == other.name &&
        yearExpire == other.yearExpire;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, id.hashCode), name.hashCode), yearExpire.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LicenseModel')
          ..add('id', id)
          ..add('name', name)
          ..add('yearExpire', yearExpire))
        .toString();
  }
}

class LicenseModelBuilder
    implements Builder<LicenseModel, LicenseModelBuilder> {
  _$LicenseModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _yearExpire;
  int get yearExpire => _$this._yearExpire;
  set yearExpire(int yearExpire) => _$this._yearExpire = yearExpire;

  LicenseModelBuilder();

  LicenseModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _yearExpire = _$v.yearExpire;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LicenseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LicenseModel;
  }

  @override
  void update(void Function(LicenseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LicenseModel build() {
    final _$result =
        _$v ?? new _$LicenseModel._(id: id, name: name, yearExpire: yearExpire);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
