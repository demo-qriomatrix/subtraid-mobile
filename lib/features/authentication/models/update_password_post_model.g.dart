// GENERATED CODE - DO NOT MODIFY BY HAND

part of update_password_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UpdatePasswordPostModel> _$updatePasswordPostModelSerializer =
    new _$UpdatePasswordPostModelSerializer();

class _$UpdatePasswordPostModelSerializer
    implements StructuredSerializer<UpdatePasswordPostModel> {
  @override
  final Iterable<Type> types = const [
    UpdatePasswordPostModel,
    _$UpdatePasswordPostModel
  ];
  @override
  final String wireName = 'UpdatePasswordPostModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, UpdatePasswordPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.newpwd != null) {
      result
        ..add('newpwd')
        ..add(serializers.serialize(object.newpwd,
            specifiedType: const FullType(String)));
    }
    if (object.pwd != null) {
      result
        ..add('pwd')
        ..add(serializers.serialize(object.pwd,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UpdatePasswordPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UpdatePasswordPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'newpwd':
          result.newpwd = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'pwd':
          result.pwd = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UpdatePasswordPostModel extends UpdatePasswordPostModel {
  @override
  final String newpwd;
  @override
  final String pwd;

  factory _$UpdatePasswordPostModel(
          [void Function(UpdatePasswordPostModelBuilder) updates]) =>
      (new UpdatePasswordPostModelBuilder()..update(updates)).build();

  _$UpdatePasswordPostModel._({this.newpwd, this.pwd}) : super._();

  @override
  UpdatePasswordPostModel rebuild(
          void Function(UpdatePasswordPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdatePasswordPostModelBuilder toBuilder() =>
      new UpdatePasswordPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdatePasswordPostModel &&
        newpwd == other.newpwd &&
        pwd == other.pwd;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, newpwd.hashCode), pwd.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdatePasswordPostModel')
          ..add('newpwd', newpwd)
          ..add('pwd', pwd))
        .toString();
  }
}

class UpdatePasswordPostModelBuilder
    implements
        Builder<UpdatePasswordPostModel, UpdatePasswordPostModelBuilder> {
  _$UpdatePasswordPostModel _$v;

  String _newpwd;
  String get newpwd => _$this._newpwd;
  set newpwd(String newpwd) => _$this._newpwd = newpwd;

  String _pwd;
  String get pwd => _$this._pwd;
  set pwd(String pwd) => _$this._pwd = pwd;

  UpdatePasswordPostModelBuilder();

  UpdatePasswordPostModelBuilder get _$this {
    if (_$v != null) {
      _newpwd = _$v.newpwd;
      _pwd = _$v.pwd;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdatePasswordPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UpdatePasswordPostModel;
  }

  @override
  void update(void Function(UpdatePasswordPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdatePasswordPostModel build() {
    final _$result =
        _$v ?? new _$UpdatePasswordPostModel._(newpwd: newpwd, pwd: pwd);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
