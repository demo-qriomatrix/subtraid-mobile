// GENERATED CODE - DO NOT MODIFY BY HAND

part of user_company_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserCompanyModel> _$userCompanyModelSerializer =
    new _$UserCompanyModelSerializer();

class _$UserCompanyModelSerializer
    implements StructuredSerializer<UserCompanyModel> {
  @override
  final Iterable<Type> types = const [UserCompanyModel, _$UserCompanyModel];
  @override
  final String wireName = 'UserCompanyModel';

  @override
  Iterable<Object> serialize(Serializers serializers, UserCompanyModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.companyType != null) {
      result
        ..add('companyType')
        ..add(serializers.serialize(object.companyType,
            specifiedType: const FullType(String)));
    }
    if (object.projectPermissionGroups != null) {
      result
        ..add('projectPermissionGroups')
        ..add(serializers.serialize(object.projectPermissionGroups,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.owner != null) {
      result
        ..add('owner')
        ..add(serializers.serialize(object.owner,
            specifiedType: const FullType(String)));
    }
    if (object.defultProjectPermissionGroups != null) {
      result
        ..add('defultProjectPermissionGroups')
        ..add(serializers.serialize(object.defultProjectPermissionGroups,
            specifiedType: const FullType(String)));
    }
    if (object.admintProjectPermissionGroups != null) {
      result
        ..add('admintProjectPermissionGroups')
        ..add(serializers.serialize(object.admintProjectPermissionGroups,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UserCompanyModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserCompanyModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'companyType':
          result.companyType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'projectPermissionGroups':
          result.projectPermissionGroups.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'owner':
          result.owner = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'defultProjectPermissionGroups':
          result.defultProjectPermissionGroups = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'admintProjectPermissionGroups':
          result.admintProjectPermissionGroups = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$UserCompanyModel extends UserCompanyModel {
  @override
  final String companyType;
  @override
  final BuiltList<String> projectPermissionGroups;
  @override
  final String id;
  @override
  final String owner;
  @override
  final String defultProjectPermissionGroups;
  @override
  final String admintProjectPermissionGroups;

  factory _$UserCompanyModel(
          [void Function(UserCompanyModelBuilder) updates]) =>
      (new UserCompanyModelBuilder()..update(updates)).build();

  _$UserCompanyModel._(
      {this.companyType,
      this.projectPermissionGroups,
      this.id,
      this.owner,
      this.defultProjectPermissionGroups,
      this.admintProjectPermissionGroups})
      : super._();

  @override
  UserCompanyModel rebuild(void Function(UserCompanyModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserCompanyModelBuilder toBuilder() =>
      new UserCompanyModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserCompanyModel &&
        companyType == other.companyType &&
        projectPermissionGroups == other.projectPermissionGroups &&
        id == other.id &&
        owner == other.owner &&
        defultProjectPermissionGroups == other.defultProjectPermissionGroups &&
        admintProjectPermissionGroups == other.admintProjectPermissionGroups;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc(0, companyType.hashCode),
                        projectPermissionGroups.hashCode),
                    id.hashCode),
                owner.hashCode),
            defultProjectPermissionGroups.hashCode),
        admintProjectPermissionGroups.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserCompanyModel')
          ..add('companyType', companyType)
          ..add('projectPermissionGroups', projectPermissionGroups)
          ..add('id', id)
          ..add('owner', owner)
          ..add('defultProjectPermissionGroups', defultProjectPermissionGroups)
          ..add('admintProjectPermissionGroups', admintProjectPermissionGroups))
        .toString();
  }
}

class UserCompanyModelBuilder
    implements Builder<UserCompanyModel, UserCompanyModelBuilder> {
  _$UserCompanyModel _$v;

  String _companyType;
  String get companyType => _$this._companyType;
  set companyType(String companyType) => _$this._companyType = companyType;

  ListBuilder<String> _projectPermissionGroups;
  ListBuilder<String> get projectPermissionGroups =>
      _$this._projectPermissionGroups ??= new ListBuilder<String>();
  set projectPermissionGroups(ListBuilder<String> projectPermissionGroups) =>
      _$this._projectPermissionGroups = projectPermissionGroups;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _owner;
  String get owner => _$this._owner;
  set owner(String owner) => _$this._owner = owner;

  String _defultProjectPermissionGroups;
  String get defultProjectPermissionGroups =>
      _$this._defultProjectPermissionGroups;
  set defultProjectPermissionGroups(String defultProjectPermissionGroups) =>
      _$this._defultProjectPermissionGroups = defultProjectPermissionGroups;

  String _admintProjectPermissionGroups;
  String get admintProjectPermissionGroups =>
      _$this._admintProjectPermissionGroups;
  set admintProjectPermissionGroups(String admintProjectPermissionGroups) =>
      _$this._admintProjectPermissionGroups = admintProjectPermissionGroups;

  UserCompanyModelBuilder();

  UserCompanyModelBuilder get _$this {
    if (_$v != null) {
      _companyType = _$v.companyType;
      _projectPermissionGroups = _$v.projectPermissionGroups?.toBuilder();
      _id = _$v.id;
      _owner = _$v.owner;
      _defultProjectPermissionGroups = _$v.defultProjectPermissionGroups;
      _admintProjectPermissionGroups = _$v.admintProjectPermissionGroups;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserCompanyModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserCompanyModel;
  }

  @override
  void update(void Function(UserCompanyModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserCompanyModel build() {
    _$UserCompanyModel _$result;
    try {
      _$result = _$v ??
          new _$UserCompanyModel._(
              companyType: companyType,
              projectPermissionGroups: _projectPermissionGroups?.build(),
              id: id,
              owner: owner,
              defultProjectPermissionGroups: defultProjectPermissionGroups,
              admintProjectPermissionGroups: admintProjectPermissionGroups);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'projectPermissionGroups';
        _projectPermissionGroups?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'UserCompanyModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
