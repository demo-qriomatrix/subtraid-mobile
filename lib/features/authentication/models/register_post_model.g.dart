// GENERATED CODE - DO NOT MODIFY BY HAND

part of register_post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RegisterPostModel> _$registerPostModelSerializer =
    new _$RegisterPostModelSerializer();

class _$RegisterPostModelSerializer
    implements StructuredSerializer<RegisterPostModel> {
  @override
  final Iterable<Type> types = const [RegisterPostModel, _$RegisterPostModel];
  @override
  final String wireName = 'RegisterPostModel';

  @override
  Iterable<Object> serialize(Serializers serializers, RegisterPostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.accountType != null) {
      result
        ..add('accountType')
        ..add(serializers.serialize(object.accountType,
            specifiedType: const FullType(String)));
    }
    if (object.companyType != null) {
      result
        ..add('companyType')
        ..add(serializers.serialize(object.companyType,
            specifiedType: const FullType(String)));
    }
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.pwd != null) {
      result
        ..add('pwd')
        ..add(serializers.serialize(object.pwd,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RegisterPostModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RegisterPostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'accountType':
          result.accountType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'companyType':
          result.companyType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'pwd':
          result.pwd = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$RegisterPostModel extends RegisterPostModel {
  @override
  final String accountType;
  @override
  final String companyType;
  @override
  final String email;
  @override
  final String name;
  @override
  final String pwd;

  factory _$RegisterPostModel(
          [void Function(RegisterPostModelBuilder) updates]) =>
      (new RegisterPostModelBuilder()..update(updates)).build();

  _$RegisterPostModel._(
      {this.accountType, this.companyType, this.email, this.name, this.pwd})
      : super._();

  @override
  RegisterPostModel rebuild(void Function(RegisterPostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RegisterPostModelBuilder toBuilder() =>
      new RegisterPostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RegisterPostModel &&
        accountType == other.accountType &&
        companyType == other.companyType &&
        email == other.email &&
        name == other.name &&
        pwd == other.pwd;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, accountType.hashCode), companyType.hashCode),
                email.hashCode),
            name.hashCode),
        pwd.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RegisterPostModel')
          ..add('accountType', accountType)
          ..add('companyType', companyType)
          ..add('email', email)
          ..add('name', name)
          ..add('pwd', pwd))
        .toString();
  }
}

class RegisterPostModelBuilder
    implements Builder<RegisterPostModel, RegisterPostModelBuilder> {
  _$RegisterPostModel _$v;

  String _accountType;
  String get accountType => _$this._accountType;
  set accountType(String accountType) => _$this._accountType = accountType;

  String _companyType;
  String get companyType => _$this._companyType;
  set companyType(String companyType) => _$this._companyType = companyType;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _pwd;
  String get pwd => _$this._pwd;
  set pwd(String pwd) => _$this._pwd = pwd;

  RegisterPostModelBuilder();

  RegisterPostModelBuilder get _$this {
    if (_$v != null) {
      _accountType = _$v.accountType;
      _companyType = _$v.companyType;
      _email = _$v.email;
      _name = _$v.name;
      _pwd = _$v.pwd;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RegisterPostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RegisterPostModel;
  }

  @override
  void update(void Function(RegisterPostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RegisterPostModel build() {
    final _$result = _$v ??
        new _$RegisterPostModel._(
            accountType: accountType,
            companyType: companyType,
            email: email,
            name: name,
            pwd: pwd);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
