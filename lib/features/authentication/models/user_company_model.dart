library user_company_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_company_model.g.dart';

abstract class UserCompanyModel
    implements Built<UserCompanyModel, UserCompanyModelBuilder> {
  @nullable
  String get companyType;

  @nullable
  BuiltList<String> get projectPermissionGroups;

  // @nullable
  // BuiltList<Null> get stripeSessionIds;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get owner;

  // @nullable
  // BuiltList<Null> get employees;

  @nullable
  String get defultProjectPermissionGroups;

  @nullable
  String get admintProjectPermissionGroups;

  // @nullable
  // BuiltList<Null> get tags;

  // @nullable
  // BuiltList<Null> get crews;

  UserCompanyModel._();

  factory UserCompanyModel([updates(UserCompanyModelBuilder b)]) =
      _$UserCompanyModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(UserCompanyModel.serializer, this));
  }

  static UserCompanyModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserCompanyModel.serializer, json.decode(jsonString));
  }

  static Serializer<UserCompanyModel> get serializer =>
      _$userCompanyModelSerializer;
}
