// GENERATED CODE - DO NOT MODIFY BY HAND

part of user_location_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserLocationModel> _$userLocationModelSerializer =
    new _$UserLocationModelSerializer();

class _$UserLocationModelSerializer
    implements StructuredSerializer<UserLocationModel> {
  @override
  final Iterable<Type> types = const [UserLocationModel, _$UserLocationModel];
  @override
  final String wireName = 'UserLocationModel';

  @override
  Iterable<Object> serialize(Serializers serializers, UserLocationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.formattedAddress != null) {
      result
        ..add('formattedAddress')
        ..add(serializers.serialize(object.formattedAddress,
            specifiedType: const FullType(String)));
    }
    if (object.lat != null) {
      result
        ..add('lat')
        ..add(serializers.serialize(object.lat,
            specifiedType: const FullType(double)));
    }
    if (object.lng != null) {
      result
        ..add('lng')
        ..add(serializers.serialize(object.lng,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  UserLocationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserLocationModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'formattedAddress':
          result.formattedAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lat':
          result.lat = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'lng':
          result.lng = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$UserLocationModel extends UserLocationModel {
  @override
  final String id;
  @override
  final String formattedAddress;
  @override
  final double lat;
  @override
  final double lng;

  factory _$UserLocationModel(
          [void Function(UserLocationModelBuilder) updates]) =>
      (new UserLocationModelBuilder()..update(updates)).build();

  _$UserLocationModel._({this.id, this.formattedAddress, this.lat, this.lng})
      : super._();

  @override
  UserLocationModel rebuild(void Function(UserLocationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserLocationModelBuilder toBuilder() =>
      new UserLocationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserLocationModel &&
        id == other.id &&
        formattedAddress == other.formattedAddress &&
        lat == other.lat &&
        lng == other.lng;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), formattedAddress.hashCode), lat.hashCode),
        lng.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserLocationModel')
          ..add('id', id)
          ..add('formattedAddress', formattedAddress)
          ..add('lat', lat)
          ..add('lng', lng))
        .toString();
  }
}

class UserLocationModelBuilder
    implements Builder<UserLocationModel, UserLocationModelBuilder> {
  _$UserLocationModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _formattedAddress;
  String get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  double _lat;
  double get lat => _$this._lat;
  set lat(double lat) => _$this._lat = lat;

  double _lng;
  double get lng => _$this._lng;
  set lng(double lng) => _$this._lng = lng;

  UserLocationModelBuilder();

  UserLocationModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _formattedAddress = _$v.formattedAddress;
      _lat = _$v.lat;
      _lng = _$v.lng;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserLocationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserLocationModel;
  }

  @override
  void update(void Function(UserLocationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserLocationModel build() {
    final _$result = _$v ??
        new _$UserLocationModel._(
            id: id, formattedAddress: formattedAddress, lat: lat, lng: lng);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
