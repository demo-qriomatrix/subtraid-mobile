// GENERATED CODE - DO NOT MODIFY BY HAND

part of access_token_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AccessTokenResponseModel> _$accessTokenResponseModelSerializer =
    new _$AccessTokenResponseModelSerializer();

class _$AccessTokenResponseModelSerializer
    implements StructuredSerializer<AccessTokenResponseModel> {
  @override
  final Iterable<Type> types = const [
    AccessTokenResponseModel,
    _$AccessTokenResponseModel
  ];
  @override
  final String wireName = 'AccessTokenResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, AccessTokenResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.returnToken != null) {
      result
        ..add('return')
        ..add(serializers.serialize(object.returnToken,
            specifiedType: const FullType(AccessTokenModel)));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add('status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  AccessTokenResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AccessTokenResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'return':
          result.returnToken.replace(serializers.deserialize(value,
                  specifiedType: const FullType(AccessTokenModel))
              as AccessTokenModel);
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$AccessTokenResponseModel extends AccessTokenResponseModel {
  @override
  final AccessTokenModel returnToken;
  @override
  final String message;
  @override
  final int status;

  factory _$AccessTokenResponseModel(
          [void Function(AccessTokenResponseModelBuilder) updates]) =>
      (new AccessTokenResponseModelBuilder()..update(updates)).build();

  _$AccessTokenResponseModel._({this.returnToken, this.message, this.status})
      : super._();

  @override
  AccessTokenResponseModel rebuild(
          void Function(AccessTokenResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AccessTokenResponseModelBuilder toBuilder() =>
      new AccessTokenResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AccessTokenResponseModel &&
        returnToken == other.returnToken &&
        message == other.message &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, returnToken.hashCode), message.hashCode), status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AccessTokenResponseModel')
          ..add('returnToken', returnToken)
          ..add('message', message)
          ..add('status', status))
        .toString();
  }
}

class AccessTokenResponseModelBuilder
    implements
        Builder<AccessTokenResponseModel, AccessTokenResponseModelBuilder> {
  _$AccessTokenResponseModel _$v;

  AccessTokenModelBuilder _returnToken;
  AccessTokenModelBuilder get returnToken =>
      _$this._returnToken ??= new AccessTokenModelBuilder();
  set returnToken(AccessTokenModelBuilder returnToken) =>
      _$this._returnToken = returnToken;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  int _status;
  int get status => _$this._status;
  set status(int status) => _$this._status = status;

  AccessTokenResponseModelBuilder();

  AccessTokenResponseModelBuilder get _$this {
    if (_$v != null) {
      _returnToken = _$v.returnToken?.toBuilder();
      _message = _$v.message;
      _status = _$v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AccessTokenResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AccessTokenResponseModel;
  }

  @override
  void update(void Function(AccessTokenResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AccessTokenResponseModel build() {
    _$AccessTokenResponseModel _$result;
    try {
      _$result = _$v ??
          new _$AccessTokenResponseModel._(
              returnToken: _returnToken?.build(),
              message: message,
              status: status);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'returnToken';
        _returnToken?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AccessTokenResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
