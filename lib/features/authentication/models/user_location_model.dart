library user_location_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'user_location_model.g.dart';

abstract class UserLocationModel
    implements Built<UserLocationModel, UserLocationModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get formattedAddress;

  @nullable
  double get lat;

  @nullable
  double get lng;

  UserLocationModel._();

  factory UserLocationModel([updates(UserLocationModelBuilder b)]) =
      _$UserLocationModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(UserLocationModel.serializer, this));
  }

  static UserLocationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserLocationModel.serializer, json.decode(jsonString));
  }

  static Serializer<UserLocationModel> get serializer =>
      _$userLocationModelSerializer;
}
