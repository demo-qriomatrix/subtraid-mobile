library license_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'license_model.g.dart';

abstract class LicenseModel
    implements Built<LicenseModel, LicenseModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get name;

  @nullable
  int get yearExpire;

  LicenseModel._();

  factory LicenseModel([updates(LicenseModelBuilder b)]) = _$LicenseModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(LicenseModel.serializer, this));
  }

  static LicenseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        LicenseModel.serializer, json.decode(jsonString));
  }

  static Serializer<LicenseModel> get serializer => _$licenseModelSerializer;
}
