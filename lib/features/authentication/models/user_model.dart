library user_model;

import 'dart:convert';

import 'package:Subtraid/features/authentication/models/license_model.dart';
import 'package:Subtraid/features/authentication/models/skill_model.dart';
import 'package:Subtraid/features/authentication/models/user_location_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';

import 'user_company_model.dart';
import 'experience_model.dart';

part 'user_model.g.dart';

abstract class UserModel implements Built<UserModel, UserModelBuilder> {
  @nullable
  String get name;

  @nullable
  String get accountType;

  @nullable
  String get employer;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  JsonObject get location;

  @nullable
  UserLocationModel get getLocation {
    if (location != null && location.isList && location.asList.isNotEmpty) {
      try {
        return UserLocationModel.fromJson(json.encode(location.asList.first));
      } catch (e) {
        return null;
      }
    }

    return null;
  }

  @nullable
  String get email;

  @nullable
  String get description;

  @nullable
  BuiltList<SkillModel> get skills;

  @nullable
  BuiltList<LicenseModel> get licences;

  @nullable
  BuiltList<ExperienceModel> get experiences;

  @nullable
  String get img;

  @nullable
  String get phone;

  @nullable
  UserCompanyModel get company;

  UserModel._();

  factory UserModel([updates(UserModelBuilder b)]) = _$UserModel;

  String toJson() {
    return json.encode(serializers.serializeWith(UserModel.serializer, this));
  }

  static UserModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserModel.serializer, json.decode(jsonString));
  }

  static Serializer<UserModel> get serializer => _$userModelSerializer;
}
