// GENERATED CODE - DO NOT MODIFY BY HAND

part of experience_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ExperienceModel> _$experienceModelSerializer =
    new _$ExperienceModelSerializer();

class _$ExperienceModelSerializer
    implements StructuredSerializer<ExperienceModel> {
  @override
  final Iterable<Type> types = const [ExperienceModel, _$ExperienceModel];
  @override
  final String wireName = 'ExperienceModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ExperienceModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.isCurrent != null) {
      result
        ..add('isCurrent')
        ..add(serializers.serialize(object.isCurrent,
            specifiedType: const FullType(bool)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.companyName != null) {
      result
        ..add('companyName')
        ..add(serializers.serialize(object.companyName,
            specifiedType: const FullType(String)));
    }
    if (object.jobTitle != null) {
      result
        ..add('jobTitle')
        ..add(serializers.serialize(object.jobTitle,
            specifiedType: const FullType(String)));
    }
    if (object.from != null) {
      result
        ..add('from')
        ..add(serializers.serialize(object.from,
            specifiedType: const FullType(String)));
    }
    if (object.to != null) {
      result
        ..add('to')
        ..add(serializers.serialize(object.to,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ExperienceModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ExperienceModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isCurrent':
          result.isCurrent = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'companyName':
          result.companyName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'jobTitle':
          result.jobTitle = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'from':
          result.from = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'to':
          result.to = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ExperienceModel extends ExperienceModel {
  @override
  final String id;
  @override
  final bool isCurrent;
  @override
  final String dateCreated;
  @override
  final String companyName;
  @override
  final String jobTitle;
  @override
  final String from;
  @override
  final String to;

  factory _$ExperienceModel([void Function(ExperienceModelBuilder) updates]) =>
      (new ExperienceModelBuilder()..update(updates)).build();

  _$ExperienceModel._(
      {this.id,
      this.isCurrent,
      this.dateCreated,
      this.companyName,
      this.jobTitle,
      this.from,
      this.to})
      : super._();

  @override
  ExperienceModel rebuild(void Function(ExperienceModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ExperienceModelBuilder toBuilder() =>
      new ExperienceModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ExperienceModel &&
        id == other.id &&
        isCurrent == other.isCurrent &&
        dateCreated == other.dateCreated &&
        companyName == other.companyName &&
        jobTitle == other.jobTitle &&
        from == other.from &&
        to == other.to;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), isCurrent.hashCode),
                        dateCreated.hashCode),
                    companyName.hashCode),
                jobTitle.hashCode),
            from.hashCode),
        to.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ExperienceModel')
          ..add('id', id)
          ..add('isCurrent', isCurrent)
          ..add('dateCreated', dateCreated)
          ..add('companyName', companyName)
          ..add('jobTitle', jobTitle)
          ..add('from', from)
          ..add('to', to))
        .toString();
  }
}

class ExperienceModelBuilder
    implements Builder<ExperienceModel, ExperienceModelBuilder> {
  _$ExperienceModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  bool _isCurrent;
  bool get isCurrent => _$this._isCurrent;
  set isCurrent(bool isCurrent) => _$this._isCurrent = isCurrent;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  String _companyName;
  String get companyName => _$this._companyName;
  set companyName(String companyName) => _$this._companyName = companyName;

  String _jobTitle;
  String get jobTitle => _$this._jobTitle;
  set jobTitle(String jobTitle) => _$this._jobTitle = jobTitle;

  String _from;
  String get from => _$this._from;
  set from(String from) => _$this._from = from;

  String _to;
  String get to => _$this._to;
  set to(String to) => _$this._to = to;

  ExperienceModelBuilder();

  ExperienceModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _isCurrent = _$v.isCurrent;
      _dateCreated = _$v.dateCreated;
      _companyName = _$v.companyName;
      _jobTitle = _$v.jobTitle;
      _from = _$v.from;
      _to = _$v.to;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ExperienceModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ExperienceModel;
  }

  @override
  void update(void Function(ExperienceModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ExperienceModel build() {
    final _$result = _$v ??
        new _$ExperienceModel._(
            id: id,
            isCurrent: isCurrent,
            dateCreated: dateCreated,
            companyName: companyName,
            jobTitle: jobTitle,
            from: from,
            to: to);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
