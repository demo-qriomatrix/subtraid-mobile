library access_token_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'access_token_model.g.dart';

abstract class AccessTokenModel
    implements Built<AccessTokenModel, AccessTokenModelBuilder> {
  @nullable
  String get token;

  AccessTokenModel._();

  factory AccessTokenModel([updates(AccessTokenModelBuilder b)]) =
      _$AccessTokenModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(AccessTokenModel.serializer, this));
  }

  static AccessTokenModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        AccessTokenModel.serializer, json.decode(jsonString));
  }

  static Serializer<AccessTokenModel> get serializer =>
      _$accessTokenModelSerializer;
}
