library register_post_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'register_post_model.g.dart';

abstract class RegisterPostModel
    implements Built<RegisterPostModel, RegisterPostModelBuilder> {
  @nullable
  String get accountType;

  @nullable
  String get companyType;

  @nullable
  String get email;

  @nullable
  String get name;

  @nullable
  String get pwd;

  RegisterPostModel._();

  factory RegisterPostModel([updates(RegisterPostModelBuilder b)]) =
      _$RegisterPostModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(RegisterPostModel.serializer, this));
  }

  static RegisterPostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        RegisterPostModel.serializer, json.decode(jsonString));
  }

  static Serializer<RegisterPostModel> get serializer =>
      _$registerPostModelSerializer;
}
