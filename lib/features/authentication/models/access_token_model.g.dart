// GENERATED CODE - DO NOT MODIFY BY HAND

part of access_token_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AccessTokenModel> _$accessTokenModelSerializer =
    new _$AccessTokenModelSerializer();

class _$AccessTokenModelSerializer
    implements StructuredSerializer<AccessTokenModel> {
  @override
  final Iterable<Type> types = const [AccessTokenModel, _$AccessTokenModel];
  @override
  final String wireName = 'AccessTokenModel';

  @override
  Iterable<Object> serialize(Serializers serializers, AccessTokenModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.token != null) {
      result
        ..add('token')
        ..add(serializers.serialize(object.token,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AccessTokenModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AccessTokenModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'token':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$AccessTokenModel extends AccessTokenModel {
  @override
  final String token;

  factory _$AccessTokenModel(
          [void Function(AccessTokenModelBuilder) updates]) =>
      (new AccessTokenModelBuilder()..update(updates)).build();

  _$AccessTokenModel._({this.token}) : super._();

  @override
  AccessTokenModel rebuild(void Function(AccessTokenModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AccessTokenModelBuilder toBuilder() =>
      new AccessTokenModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AccessTokenModel && token == other.token;
  }

  @override
  int get hashCode {
    return $jf($jc(0, token.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AccessTokenModel')
          ..add('token', token))
        .toString();
  }
}

class AccessTokenModelBuilder
    implements Builder<AccessTokenModel, AccessTokenModelBuilder> {
  _$AccessTokenModel _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  AccessTokenModelBuilder();

  AccessTokenModelBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AccessTokenModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AccessTokenModel;
  }

  @override
  void update(void Function(AccessTokenModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AccessTokenModel build() {
    final _$result = _$v ?? new _$AccessTokenModel._(token: token);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
