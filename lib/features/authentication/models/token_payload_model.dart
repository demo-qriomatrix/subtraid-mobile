library token_payload_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'token_payload_model.g.dart';

abstract class TokenPayloadModel
    implements Built<TokenPayloadModel, TokenPayloadModelBuilder> {
  @nullable
  String get email;

  @nullable
  String get name;

  // @nullable
  // bool get isSubtraid;

  // @nullable
  // bool get isVerified;

  // @nullable
  // String get accountType;

  // @nullable
  // String get company;

  @nullable
  String get img;

  // @nullable
  // Null get employer;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  // @nullable
  // bool get isAdmin;

  // @nullable
  // bool get isPaidAccount;

  // @nullable
  // bool get isRootUser;

  // @nullable
  // int get iat;

  // @nullable
  // int get exp;

  TokenPayloadModel._();

  factory TokenPayloadModel([updates(TokenPayloadModelBuilder b)]) =
      _$TokenPayloadModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(TokenPayloadModel.serializer, this));
  }

  static TokenPayloadModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        TokenPayloadModel.serializer, json.decode(jsonString));
  }

  static Serializer<TokenPayloadModel> get serializer =>
      _$tokenPayloadModelSerializer;
}
