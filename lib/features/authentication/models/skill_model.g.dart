// GENERATED CODE - DO NOT MODIFY BY HAND

part of skill_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SkillModel> _$skillModelSerializer = new _$SkillModelSerializer();

class _$SkillModelSerializer implements StructuredSerializer<SkillModel> {
  @override
  final Iterable<Type> types = const [SkillModel, _$SkillModel];
  @override
  final String wireName = 'SkillModel';

  @override
  Iterable<Object> serialize(Serializers serializers, SkillModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.from != null) {
      result
        ..add('from')
        ..add(serializers.serialize(object.from,
            specifiedType: const FullType(int)));
    }
    if (object.to != null) {
      result
        ..add('to')
        ..add(serializers.serialize(object.to,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  SkillModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SkillModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'from':
          result.from = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'to':
          result.to = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SkillModel extends SkillModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final int from;
  @override
  final int to;

  factory _$SkillModel([void Function(SkillModelBuilder) updates]) =>
      (new SkillModelBuilder()..update(updates)).build();

  _$SkillModel._({this.id, this.name, this.from, this.to}) : super._();

  @override
  SkillModel rebuild(void Function(SkillModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SkillModelBuilder toBuilder() => new SkillModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SkillModel &&
        id == other.id &&
        name == other.name &&
        from == other.from &&
        to == other.to;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc($jc(0, id.hashCode), name.hashCode), from.hashCode),
        to.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SkillModel')
          ..add('id', id)
          ..add('name', name)
          ..add('from', from)
          ..add('to', to))
        .toString();
  }
}

class SkillModelBuilder implements Builder<SkillModel, SkillModelBuilder> {
  _$SkillModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _from;
  int get from => _$this._from;
  set from(int from) => _$this._from = from;

  int _to;
  int get to => _$this._to;
  set to(int to) => _$this._to = to;

  SkillModelBuilder();

  SkillModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _from = _$v.from;
      _to = _$v.to;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SkillModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SkillModel;
  }

  @override
  void update(void Function(SkillModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SkillModel build() {
    final _$result =
        _$v ?? new _$SkillModel._(id: id, name: name, from: from, to: to);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
