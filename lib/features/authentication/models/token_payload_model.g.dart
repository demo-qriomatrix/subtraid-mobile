// GENERATED CODE - DO NOT MODIFY BY HAND

part of token_payload_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<TokenPayloadModel> _$tokenPayloadModelSerializer =
    new _$TokenPayloadModelSerializer();

class _$TokenPayloadModelSerializer
    implements StructuredSerializer<TokenPayloadModel> {
  @override
  final Iterable<Type> types = const [TokenPayloadModel, _$TokenPayloadModel];
  @override
  final String wireName = 'TokenPayloadModel';

  @override
  Iterable<Object> serialize(Serializers serializers, TokenPayloadModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.email != null) {
      result
        ..add('email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.img != null) {
      result
        ..add('img')
        ..add(serializers.serialize(object.img,
            specifiedType: const FullType(String)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  TokenPayloadModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TokenPayloadModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'img':
          result.img = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TokenPayloadModel extends TokenPayloadModel {
  @override
  final String email;
  @override
  final String name;
  @override
  final String img;
  @override
  final String id;

  factory _$TokenPayloadModel(
          [void Function(TokenPayloadModelBuilder) updates]) =>
      (new TokenPayloadModelBuilder()..update(updates)).build();

  _$TokenPayloadModel._({this.email, this.name, this.img, this.id}) : super._();

  @override
  TokenPayloadModel rebuild(void Function(TokenPayloadModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TokenPayloadModelBuilder toBuilder() =>
      new TokenPayloadModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TokenPayloadModel &&
        email == other.email &&
        name == other.name &&
        img == other.img &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, email.hashCode), name.hashCode), img.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TokenPayloadModel')
          ..add('email', email)
          ..add('name', name)
          ..add('img', img)
          ..add('id', id))
        .toString();
  }
}

class TokenPayloadModelBuilder
    implements Builder<TokenPayloadModel, TokenPayloadModelBuilder> {
  _$TokenPayloadModel _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _img;
  String get img => _$this._img;
  set img(String img) => _$this._img = img;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  TokenPayloadModelBuilder();

  TokenPayloadModelBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _name = _$v.name;
      _img = _$v.img;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TokenPayloadModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TokenPayloadModel;
  }

  @override
  void update(void Function(TokenPayloadModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TokenPayloadModel build() {
    final _$result = _$v ??
        new _$TokenPayloadModel._(email: email, name: name, img: img, id: id);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
