library experience_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'experience_model.g.dart';

abstract class ExperienceModel
    implements Built<ExperienceModel, ExperienceModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  bool get isCurrent;

  @nullable
  String get dateCreated;

  @nullable
  String get companyName;

  @nullable
  String get jobTitle;

  @nullable
  String get from;

  @nullable
  String get to;

  ExperienceModel._();

  factory ExperienceModel([updates(ExperienceModelBuilder b)]) =
      _$ExperienceModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ExperienceModel.serializer, this));
  }

  static ExperienceModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ExperienceModel.serializer, json.decode(jsonString));
  }

  static Serializer<ExperienceModel> get serializer =>
      _$experienceModelSerializer;
}
