import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/main/view/main_screen.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  double currentPage = 0;

  PageController pageController = PageController();

  @override
  void initState() {
    pageController.addListener(() {
      setState(() {
        currentPage = pageController.page;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConfig.primary,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => MainScreen()));
                      },
                      child: Text(
                        'Skip',
                        style: TextStyle(color: ColorConfig.orange),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 10,
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          DotsIndicator(
                            axis: Axis.horizontal,
                            dotsCount: 3,
                            position: currentPage,
                            decorator: DotsDecorator(
                              size: const Size(20.0, 7.0),
                              activeSize: const Size(20.0, 7.0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              activeShape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              color: Colors.white30,
                              activeColor: ColorConfig.orange,
                            ),
                          ),
                        ],
                      ),
                    ),
                    PageView(
                      controller: pageController,
                      children: <Widget>[
                        OnboardPage(
                          header:
                              'Stay on top of things with our project management solution',
                          footer:
                              'Monitor project performance with real time updates and tracking ensuring everyone is on the same page',
                          image: LocalImages.welcome1,
                        ),
                        OnboardPage(
                          header:
                              'Keep everyone connected with our collaboration and communication solution',
                          footer:
                              'Communication application enabling participants to send messages, attachments, images, audio or video to a person or group of people',
                          image: LocalImages.welcome2,
                        ),
                        OnboardPage(
                          header: 'Effortless timesheet management solution',
                          footer:
                              'Tasks and project timesheets with a push of a button',
                          image: LocalImages.welcome3,
                          button: true,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class OnboardPage extends StatelessWidget {
  final String header;
  final String footer;
  final String image;
  final bool button;

  const OnboardPage(
      {Key key, this.header, this.footer, this.image, this.button = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 50),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text(
            header,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          Expanded(
            child: Center(
              child: Image.asset(
                image,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Text(
            footer,
            style: TextStyle(color: Colors.white, fontSize: 16, height: 1.5),
          ),
          SizedBox(
            height: 20,
          ),
          button
              ? FlatButton(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                    child: Text(
                      'Get Started !',
                      style:
                          TextStyle(color: ColorConfig.primary, fontSize: 16),
                    ),
                  ),
                  color: ColorConfig.orange,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => MainScreen()));
                  },
                )
              : Container()
        ],
      ),
    );
  }
}
