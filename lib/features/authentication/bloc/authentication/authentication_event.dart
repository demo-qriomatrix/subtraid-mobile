part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class CheckLocalToken extends AuthenticationEvent {}

class LogoutEvent extends AuthenticationEvent {}

class AuthenticationUnauthenticatedEvent extends AuthenticationEvent {}

class UpdatePassword extends AuthenticationEvent {
  final UpdatePasswordPostModel model;

  UpdatePassword({@required this.model});
}
