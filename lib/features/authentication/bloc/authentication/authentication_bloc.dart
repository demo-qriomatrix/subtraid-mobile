import 'dart:async';
import 'dart:convert';

import 'package:Subtraid/features/shared/models/api_reponse_model.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:Subtraid/core/global/bloc/global_bloc.dart';
import 'package:Subtraid/features/authentication/models/token_payload_model.dart';
import 'package:Subtraid/features/authentication/models/update_password_post_model.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/authentication/models/user_response_model.dart';
import 'package:Subtraid/features/authentication/repository/authentication_repository.dart';

import 'package:meta/meta.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc(
      {@required this.authenticationRepository, @required this.globalBloc})
      : super(AuthenticationInitial()) {
    authenticationRepository.httpClient.setAuthenticationBloc(this);
  }

  final AuthenticationRepository authenticationRepository;
  final GlobalBloc globalBloc;

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is CheckLocalToken) {
      yield* _mapCheckLocalTokenToState();
    }

    if (event is LogoutEvent) {
      yield* _mapLogoutEventToState();
    }

    if (event is UpdatePassword) {
      yield* _mapUpdatePasswordToState(event.model);
    }
  }

  Stream<AuthenticationState> _mapUpdatePasswordToState(
      UpdatePasswordPostModel data) async* {
    try {
      globalBloc.add(ShowLoadingWidget());
      await authenticationRepository.updatePassword(data);
      globalBloc.add(HideLoadingWidget());

      globalBloc.add(ShowSuccessSnackBar(
          message: 'Password updated successfully, Please login again',
          event: null));

      Future.delayed(Duration(seconds: 3), () {
        add(LogoutEvent());
      });
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(
            error:
                ApiResponseModel.fromJson(json.encode(dioError.response.data))
                    .message));
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      globalBloc.add(HideLoadingWidget());
      globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
    }
  }

  Stream<AuthenticationState> _mapLogoutEventToState() async* {
    await authenticationRepository.removeTokenFromLocalStorage();
    authenticationRepository.httpClient.removeTokenInterceptor();
    yield UnauthentcatedState();
  }

  Stream<AuthenticationState> _mapCheckLocalTokenToState() async* {
    try {
      String token =
          await authenticationRepository.checkLocalTokenAndAddInterceptor();

      if (token != null) {
        Map<String, dynamic> payload = Jwt.parseJwt(token);

        TokenPayloadModel tokenPayload =
            TokenPayloadModel.fromJson(json.encode(payload));

        UserResponseModel userResponseModel =
            await authenticationRepository.getUser(tokenPayload.id);

        UserModel user = userResponseModel.user;

        authenticationRepository.httpClient.add401Interceptor();

        yield AuthentcatedState(user: user);
      } else {
        yield UnauthentcatedState();
      }
    } on DioError catch (dioError) {
      if (dioError.type == DioErrorType.RESPONSE) {
        globalBloc.add(HideLoadingWidget());

        if (dioError.response.statusCode == 503) {
          print('503');
          globalBloc.add(
              ShowErrorSnackBar(error: '503 Service Temporarily Unavailable '));
        } else {
          globalBloc.add(ShowErrorSnackBar(
              error:
                  ApiResponseModel.fromJson(json.encode(dioError.response.data))
                      .message));
        }
      } else {
        globalBloc.add(HideLoadingWidget());
        globalBloc.add(ShowErrorSnackBar(error: 'Something went wrong'));
      }
    } catch (e) {
      print(e);
      await authenticationRepository.removeTokenFromLocalStorage();
      yield UnauthentcatedState();
    }
  }
}
