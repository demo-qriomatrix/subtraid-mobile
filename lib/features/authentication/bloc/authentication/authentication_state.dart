part of 'authentication_bloc.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class UnauthentcatedState extends AuthenticationState {}

class AuthentcatedState extends AuthenticationState {
  final UserModel user;

  AuthentcatedState({@required this.user});
}
