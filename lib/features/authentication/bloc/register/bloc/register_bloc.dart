import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/features/authentication/models/register_post_model.dart';
import 'package:Subtraid/features/authentication/repository/authentication_repository.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc({@required this.authenticationRepository})
      : super(RegisterInitial());

  final AuthenticationRepository authenticationRepository;

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterRequest) {
      yield* _mapRegisterRequestToState(event);
    }
  }

  Stream<RegisterState> _mapRegisterRequestToState(
      RegisterRequest event) async* {
    yield RegisterInProgressState();

    try {
      // Response response =
      await authenticationRepository.registerUser(event.registerPostModel);

      yield RegisterSuccessState();
    } on DioError catch (dioError) {
      if (dioError.response.statusCode == 401) {
        yield RegisterFailureState(error: 'Invalid Credentials');
      } else if (dioError.response.statusCode == 409) {
        yield RegisterFailureState(
            error:
                'This email has been taken, please try again with another email.');
      } else {
        yield RegisterFailureState(
            error: 'Something went wrong ' +
                dioError.response.statusCode.toString());
      }
    } catch (e) {
      yield RegisterFailureState(error: e.toString());
    }
  }
}
