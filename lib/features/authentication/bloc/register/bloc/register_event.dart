part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];
}

class RegisterRequest extends RegisterEvent {
  final RegisterPostModel registerPostModel;

  RegisterRequest({@required this.registerPostModel});
}
