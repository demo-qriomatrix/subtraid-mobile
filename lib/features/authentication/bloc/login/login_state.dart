part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginFailureState extends LoginState {
  final String error;
  LoginFailureState({@required this.error});
}

class LoginInProgressState extends LoginState {}

class LoginSuccessState extends LoginState {
  final String token;

  LoginSuccessState({@required this.token});
}
