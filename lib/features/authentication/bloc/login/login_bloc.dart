import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:Subtraid/features/authentication/models/access_token_model.dart';
import 'package:Subtraid/features/authentication/models/credential_model.dart';
import 'package:Subtraid/features/authentication/repository/authentication_repository.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({@required this.authenticationRepository}) : super(LoginInitial());

  final AuthenticationRepository authenticationRepository;

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginRequest) {
      yield* _mapLoginRequestToState(event);
    }
  }

  Stream<LoginState> _mapLoginRequestToState(LoginRequest event) async* {
    yield LoginInProgressState();

    try {
      AccessTokenModel accessTokenModel =
          await authenticationRepository.getAccessToken(event.credentialModel);

      yield LoginSuccessState(token: accessTokenModel.token);
    } on DioError catch (dioError) {
      if (dioError.response.statusCode == 401) {
        yield LoginFailureState(error: 'Invalid Credentials');
      } else {
        yield LoginFailureState(
            error: 'Something went wrong ' +
                dioError.response.statusCode.toString());
      }
    } catch (e) {
      yield LoginFailureState(error: e.toString());
    }
  }
}
