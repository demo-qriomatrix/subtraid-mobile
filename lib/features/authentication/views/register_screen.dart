import 'package:Subtraid/core/helpers/get_company_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/authentication/bloc/register/bloc/register_bloc.dart';
import 'package:Subtraid/features/authentication/models/register_post_model.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/loader.dart';
import 'package:Subtraid/features/shared/widgets/snackbars.dart';

import 'login_screen.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen>
    with TickerProviderStateMixin {
  TabController tabController;

  GlobalKey<ScaffoldState> _scafoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool remember = false;

  bool dialog = false;

  bool submitted = false;

  bool autoValidate = false;

  bool company = false;

  String accountType;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RegisterBloc>(
      create: (context) => RegisterBloc(authenticationRepository: sl()),
      lazy: false,
      child: Scaffold(
        key: _scafoldKey,
        appBar: StAppBarGuest(
          title: 'Sign Up',
        ),
        backgroundColor: ColorConfig.primary,
        body: BlocConsumer<RegisterBloc, RegisterState>(
          listener: (context, state) {
            if (state is RegisterFailureState) {
              submitted = false;
              _scafoldKey.currentState
                  .showSnackBar(buildErrorSnackBar(state.error));
              if (dialog && Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            }
            if (state is RegisterSuccessState) {
              submitted = false;

              if (dialog && Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
              _scafoldKey.currentState.showSnackBar(
                  buildSuccessSnackBar('Register Success. Please sign in'));

              Future.delayed(Duration(seconds: 2)).then((value) {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => LoginScreen(
                    fromRegister: true,
                  ),
                ));
              });
            }
            if (state is RegisterInProgressState) {
              dialog = true;
              showDialog(context: context, child: buildLoader());
            }
          },
          builder: (context, state) {
            return ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height -
                            kToolbarHeight -
                            MediaQuery.of(context).padding.top),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Form(
                        key: _formKey,
                        autovalidateMode: autoValidate
                            ? AutovalidateMode.always
                            : AutovalidateMode.disabled,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                TabBar(
                                    onTap: (index) {
                                      if (index == 0) {
                                        setState(() {
                                          company = false;
                                        });
                                      }
                                      if (index == 1) {
                                        setState(() {
                                          company = true;
                                        });
                                      }
                                    },
                                    indicatorWeight: 3,
                                    isScrollable: true,
                                    labelColor: ColorConfig.primary,
                                    labelStyle:
                                        TextStyle(fontWeight: FontWeight.w600),
                                    controller: tabController,
                                    tabs: ['User', 'Company']
                                        .map((e) => Tab(
                                              text: e,
                                            ))
                                        .toList()),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Welcome to Subtraid',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            (context.watch<RegisterBloc>().state
                                    is RegisterFailureState)
                                ? Center(
                                    child: Text(
                                      (state as RegisterFailureState).error,
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.red),
                                    ),
                                  )
                                : Text(
                                    'Create your own account',
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                            SizedBox(
                              height: 30,
                            ),
                            TextFormField(
                              controller: nameController,
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'Name is required';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: ImageIcon(
                                      AssetImage(LocalImages.user),
                                      color: ColorConfig.primary,
                                      size: 16,
                                    ),
                                  ),
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  hintStyle:
                                      TextStyle(color: ColorConfig.primary),
                                  border: signInOutlineInputBorder,
                                  hintText: 'Name'),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              controller: emailController,
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'Email is required';
                                }

                                if (!RegExp(
                                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(val)) {
                                  return 'Please enter a valid email address';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: ImageIcon(
                                      AssetImage(LocalImages.email),
                                      color: ColorConfig.primary,
                                      size: 16,
                                    ),
                                  ),
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  hintStyle:
                                      TextStyle(color: ColorConfig.primary),
                                  border: signInOutlineInputBorder,
                                  hintText: 'Email'),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              obscureText: true,
                              controller: passwordController,
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'Password is required';
                                }
                                if (val.length < 6) {
                                  return 'Password has to be at least 6 characters';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: ImageIcon(
                                      AssetImage(LocalImages.password),
                                      color: ColorConfig.primary,
                                      size: 16,
                                    ),
                                  ),
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  hintStyle:
                                      TextStyle(color: ColorConfig.primary),
                                  border: signInOutlineInputBorder,
                                  hintText: 'Password'),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              obscureText: true,
                              controller: confirmPasswordController,
                              validator: (val) {
                                if (val != passwordController.text) {
                                  return 'Confirm password does not match';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: ImageIcon(
                                      AssetImage(LocalImages.password),
                                      color: ColorConfig.primary,
                                      size: 16,
                                    ),
                                  ),
                                  fillColor: ColorConfig.blue1,
                                  filled: true,
                                  hintStyle:
                                      TextStyle(color: ColorConfig.primary),
                                  border: signInOutlineInputBorder,
                                  hintText: 'Re-type Password'),
                            ),
                            company
                                ? Column(
                                    children: [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      DropdownButtonFormField<String>(
                                        validator: (String value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Required';
                                          }

                                          return null;
                                        },
                                        dropdownColor: ColorConfig.primary,
                                        icon: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 40),
                                            child: Icon(
                                              Icons.keyboard_arrow_down,
                                            )),
                                        value: accountType,
                                        decoration: InputDecoration(
                                            prefixIcon: Padding(
                                              padding:
                                                  const EdgeInsets.all(16.0),
                                              child: ImageIcon(
                                                AssetImage(
                                                    LocalImages.building),
                                                color: ColorConfig.primary,
                                                size: 16,
                                              ),
                                            ),
                                            fillColor: ColorConfig.blue1,
                                            filled: true,
                                            hintStyle: TextStyle(
                                                color: ColorConfig.primary),
                                            border: signInOutlineInputBorder,
                                            hintText:
                                                'Select your account type'),
                                        iconEnabledColor: Colors.black,
                                        iconDisabledColor: Colors.black,
                                        items: [
                                          'Contractor',
                                          'General Contractor'
                                        ]
                                            .map((e) => DropdownMenuItem(
                                                  value: e,
                                                  child: Text(
                                                    e,
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white),
                                                  ),
                                                ))
                                            .toList(),
                                        selectedItemBuilder: (context) => [
                                          'Contractor',
                                          'General Contractor'
                                        ]
                                            .map((e) => DropdownMenuItem(
                                                  value: e,
                                                  child: Text(
                                                    e,
                                                    style: TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ))
                                            .toList(),
                                        onChanged: (val) {
                                          setState(() {
                                            accountType = val;
                                          });
                                        },
                                      ),
                                    ],
                                  )
                                : Container(),
                            SizedBox(
                              height: 20,
                            ),
                            FlatButton(
                              color: ColorConfig.primary,
                              onPressed: () {
                                setState(() {
                                  autoValidate = true;
                                });
                                if (_formKey.currentState.validate()) {
                                  if (!submitted) {
                                    submitted = true;

                                    RegisterPostModel registerPostModel =
                                        RegisterPostModel((registerPostModel) =>
                                            registerPostModel
                                              ..name = nameController.text
                                              ..email = emailController.text
                                              ..pwd = passwordController.text
                                              ..companyType = company
                                                  ? getCompanyType(accountType)
                                                  : null
                                              ..accountType =
                                                  company ? 'company' : 'user');

                                    context.read<RegisterBloc>().add(
                                        RegisterRequest(
                                            registerPostModel:
                                                registerPostModel));
                                  }
                                }
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 15),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Sign Up',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    height: 1,
                                    color: ColorConfig.orange,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Text(
                                    'or',
                                    style: TextStyle(
                                        color: ColorConfig.orange,
                                        fontSize: 16),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    height: 1,
                                    color: ColorConfig.orange,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                                child: Text(
                              'Do you have an account?',
                              style: TextStyle(
                                  color: ColorConfig.primary, fontSize: 16),
                            )),
                            SizedBox(
                              height: 20,
                            ),
                            OutlineButton(
                              borderSide:
                                  BorderSide(color: ColorConfig.primary),
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 15),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Sign In',
                                      style: TextStyle(
                                          color: ColorConfig.primary,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

OutlineInputBorder signInOutlineInputBorder = OutlineInputBorder(
    borderSide: BorderSide.none, borderRadius: BorderRadius.circular(20));
