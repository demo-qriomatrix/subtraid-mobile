import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/di/injection_container.dart';
import 'package:Subtraid/features/authentication/bloc/login/login_bloc.dart';
import 'package:Subtraid/features/authentication/models/credential_model.dart';
import 'package:Subtraid/features/authentication/views/forgot_password_screen.dart';
import 'package:Subtraid/features/authentication/views/register_screen.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:Subtraid/features/shared/widgets/loader.dart';
import 'package:Subtraid/features/shared/widgets/snackbars.dart';
import 'package:Subtraid/features/splash/view/splash_screen.dart';

class LoginScreen extends StatefulWidget {
  final bool fromRegister;

  LoginScreen({this.fromRegister});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  GlobalKey<ScaffoldState> _scafoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailController = TextEditingController();
  // text: 'qriomatrix+testcomapny2@gmail.com'
  // text: 'qriomatrix+qriouser2@gmail.com'
  // text: 'jayanishashi1995+8@gmail.com'
  TextEditingController passwordController = TextEditingController();
  // text: 'Abc@1234'

  bool remember = false;

  bool dialog = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (context) => LoginBloc(authenticationRepository: sl()),
      lazy: false,
      child: Scaffold(
        key: _scafoldKey,
        appBar: StAppBarGuest(
          title: 'Sign In',
        ),
        backgroundColor: ColorConfig.primary,
        body: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is LoginFailureState) {
              _scafoldKey.currentState
                  .showSnackBar(buildErrorSnackBar(state.error));
              if (dialog && Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
            }
            if (state is LoginSuccessState) {
              _scafoldKey.currentState
                  .showSnackBar(buildSuccessSnackBar('Login Success'));
              if (dialog && Navigator.of(context).canPop()) {
                Navigator.of(context).pop();
              }
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => SplashScreen(
                  fromRegister: widget.fromRegister,
                ),
              ));
            }
            if (state is LoginInProgressState) {
              dialog = true;
              showDialog(context: context, child: buildLoader());
            }
          },
          builder: (context, state) {
            return ListView(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50))),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height -
                            kToolbarHeight -
                            MediaQuery.of(context).padding.top),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            'Welcome Back !',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          (context.watch<LoginBloc>().state
                                  is LoginFailureState)
                              ? Center(
                                  child: Text(
                                    (context.watch<LoginBloc>().state
                                            as LoginFailureState)
                                        .error,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.red),
                                  ),
                                )
                              : Text(
                                  'Enter your credential to login',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            controller: emailController,
                            decoration: InputDecoration(
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.email),
                                    color: ColorConfig.primary,
                                    size: 16,
                                  ),
                                ),
                                fillColor: ColorConfig.blue1,
                                filled: true,
                                hintStyle:
                                    TextStyle(color: ColorConfig.primary),
                                border: signInOutlineInputBorder,
                                hintText: 'Email'),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          TextFormField(
                            controller: passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: ImageIcon(
                                    AssetImage(LocalImages.password),
                                    color: ColorConfig.primary,
                                    size: 16,
                                  ),
                                ),
                                fillColor: ColorConfig.blue1,
                                filled: true,
                                hintStyle:
                                    TextStyle(color: ColorConfig.primary),
                                border: signInOutlineInputBorder,
                                hintText: 'Password'),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Theme(
                                data: ThemeData(
                                    unselectedWidgetColor: ColorConfig.orange),
                                child: Checkbox(
                                  checkColor: ColorConfig.orange,
                                  activeColor: ColorConfig.lightOrange,
                                  value: remember,
                                  onChanged: (val) {
                                    setState(() {
                                      remember = val;
                                    });
                                  },
                                ),
                              ),
                              Text(
                                'Remember Me',
                                style: TextStyle(color: ColorConfig.orange),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          FlatButton(
                            color: ColorConfig.primary,
                            onPressed: () {
                              CredentialModel credentialsModel =
                                  CredentialModel(
                                      (credentialsModel) => credentialsModel
                                        ..email = emailController.text
                                        ..pwd = passwordController.text);

                              context.read<LoginBloc>().add(LoginRequest(
                                  credentialModel: credentialsModel));
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Sign In',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Center(
                              child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      ForgotPasswordScreen()));
                            },
                            child: Text(
                              'Forgot your Password ?',
                              style: TextStyle(
                                  color: ColorConfig.orange, fontSize: 16),
                            ),
                          )),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 1,
                                  color: ColorConfig.orange,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  'or',
                                  style: TextStyle(
                                      color: ColorConfig.orange, fontSize: 16),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 1,
                                  color: ColorConfig.orange,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Center(
                              child: Text(
                            'Dont\'t have an account?',
                            style: TextStyle(
                                color: ColorConfig.primary, fontSize: 16),
                          )),
                          SizedBox(
                            height: 20,
                          ),
                          OutlineButton(
                            borderSide: BorderSide(color: ColorConfig.primary),
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => RegisterScreen()));
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Create an Account',
                                    style: TextStyle(
                                        color: ColorConfig.primary,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

OutlineInputBorder signInOutlineInputBorder = OutlineInputBorder(
    borderSide: BorderSide.none, borderRadius: BorderRadius.circular(20));
