import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/notifications/bloc/notifications_bloc.dart';
import 'package:Subtraid/features/notifications/models/notification_model.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:timeago/timeago.dart' as timeago;

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  @override
  void initState() {
    context.read<NotificationsBloc>().add(NotificationsRequest());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConfig.primary,
      appBar: AppBar(
        backgroundColor: ColorConfig.primary,
        elevation: 0,
        title: Text('Notifications'),
        centerTitle: true,
        actions: [
          IconButton(
              icon: Icon(
                Icons.remove_red_eye,
                color: ColorConfig.orange,
              ),
              onPressed: () {
                context
                    .read<NotificationsBloc>()
                    .add(NotificationsMarkAsSeen());
              }),
          IconButton(
              icon: Icon(
                Icons.delete,
                color: ColorConfig.red1,
              ),
              onPressed: () {
                context.read<NotificationsBloc>().add(NotificationsDeleteAll());
              }),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(50), topRight: Radius.circular(50)),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50), topRight: Radius.circular(50))),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: getMinHeight(context),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: BlocBuilder<NotificationsBloc, NotificationsState>(
                builder: (context, state) {
                  if (state is NotificationsLoadInProgress) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CupertinoActivityIndicator(),
                      ],
                    );
                  }

                  if (state is NotificationsLoadSuceess) {
                    return ListView(
                        children: state.notifications
                            .map((e) => NotificationTile(e))
                            .toList());
                  }

                  return Container();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class NotificationTile extends StatelessWidget {
  final NotificationModel notificationModel;

  NotificationTile(this.notificationModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5),
                  child: CircleAvatar(
                    backgroundImage: AssetImage(LocalImages.defaultAvatar),
                    radius: 25,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          Flexible(
                            child: Text(
                              notificationModel.message,
                              style: TextStyle(color: ColorConfig.primary),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'From ' + notificationModel.from.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: ColorConfig.grey1),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  timeago.format(DateTime.parse(notificationModel.dateCreated)),
                  style: TextStyle(color: ColorConfig.orange),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 1,
              color: Color(0xffDCDCDC),
            )
          ],
        ),
      ),
    );
  }
} // class
