part of 'notifications_bloc.dart';

abstract class NotificationsEvent extends Equatable {
  const NotificationsEvent();

  @override
  List<Object> get props => [];
}

class NotificationCountRequest extends NotificationsEvent {}

class NotificationsRequest extends NotificationsEvent {}

class NotificationsMarkAsSeen extends NotificationsEvent {}

class NotificationsDeleteAll extends NotificationsEvent {}
