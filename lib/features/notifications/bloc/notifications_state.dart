part of 'notifications_bloc.dart';

abstract class NotificationsState extends Equatable {
  const NotificationsState({@required this.count});

  final int count;

  @override
  List<Object> get props => [];
}

class NotificationsInitial extends NotificationsState {
  NotificationsInitial({@required int count}) : super(count: count);
}

class NotificationsInitialed extends NotificationsState {
  NotificationsInitialed({@required int count}) : super(count: count);
}

class NotificationsLoadInProgress extends NotificationsState {
  NotificationsLoadInProgress({@required int count}) : super(count: count);
}

class NotificationsLoadSuceess extends NotificationsState {
  final List<NotificationModel> notifications;

  NotificationsLoadSuceess({@required int count, @required this.notifications})
      : super(count: count);
}

class NotificationsLoadFailure extends NotificationsState {
  NotificationsLoadFailure({@required int count}) : super(count: count);
}
