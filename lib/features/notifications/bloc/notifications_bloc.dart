import 'dart:async';

import 'package:Subtraid/features/notifications/models/notification_count_model.dart';
import 'package:Subtraid/features/notifications/models/notification_ids_model.dart';
import 'package:Subtraid/features/notifications/models/notification_model.dart';
import 'package:Subtraid/features/notifications/models/notifications_response_model.dart';
import 'package:Subtraid/features/notifications/repository/notification_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:built_collection/built_collection.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'notifications_event.dart';
part 'notifications_state.dart';

class NotificationsBloc extends Bloc<NotificationsEvent, NotificationsState> {
  NotificationsBloc({@required this.notificationRepository})
      : super(NotificationsInitial(count: 0)) {
    add(NotificationCountRequest());
  }

  final NotificationRepository notificationRepository;

  @override
  Stream<NotificationsState> mapEventToState(
    NotificationsEvent event,
  ) async* {
    if (event is NotificationCountRequest) {
      yield* _mapNotificationCountRequestToState();
    }
    if (event is NotificationsRequest) {
      yield* _mapNotificationsRequestToState();
    }
    if (event is NotificationsMarkAsSeen) {
      yield* _mapNotificationsMarkAsSeenToState();
    }
    if (event is NotificationsDeleteAll) {
      yield* _mapNotificationsDeleteAllToState();
    }
  }

  Stream<NotificationsState> _mapNotificationsDeleteAllToState() async* {
    try {
      await notificationRepository.deleteNotifications();

      add(NotificationsRequest());
    } catch (e) {}
  }

  Stream<NotificationsState> _mapNotificationsMarkAsSeenToState() async* {
    try {
      if (state is NotificationsLoadSuceess) {
        List<NotificationModel> notifications =
            (state as NotificationsLoadSuceess).notifications;

        List<String> ids = notifications.map((e) => e.id).toList();

        NotificationIds notificationIds =
            NotificationIds((data) => data..notificationIds = ListBuilder(ids));

        await notificationRepository.markAsSeen(notificationIds);

        add(NotificationsRequest());
      }
    } catch (e) {}
  }

  Stream<NotificationsState> _mapNotificationsRequestToState() async* {
    yield NotificationsLoadInProgress(count: state.count);
    try {
      NotificationCountModel notificationCountModel =
          await notificationRepository.getNotificationCount();

      NotificationsResponseModel notificationsResponseModel =
          await notificationRepository.getNotifications();

      yield NotificationsLoadSuceess(
          count: notificationCountModel.counts,
          notifications: notificationsResponseModel.notifications.toList());
    } catch (e) {
      print(e);

      yield NotificationsLoadFailure(count: state.count);
    }
  }

  Stream<NotificationsState> _mapNotificationCountRequestToState() async* {
    try {
      NotificationCountModel notificationCountModel =
          await notificationRepository.getNotificationCount();
      yield NotificationsInitialed(count: notificationCountModel.counts);
    } catch (e) {
      print(e);

      yield NotificationsInitial(count: state.count);
    }
  }
}
