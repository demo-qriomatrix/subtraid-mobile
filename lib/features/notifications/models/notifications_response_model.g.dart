// GENERATED CODE - DO NOT MODIFY BY HAND

part of notifications_response_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<NotificationsResponseModel> _$notificationsResponseModelSerializer =
    new _$NotificationsResponseModelSerializer();

class _$NotificationsResponseModelSerializer
    implements StructuredSerializer<NotificationsResponseModel> {
  @override
  final Iterable<Type> types = const [
    NotificationsResponseModel,
    _$NotificationsResponseModel
  ];
  @override
  final String wireName = 'NotificationsResponseModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, NotificationsResponseModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.notifications != null) {
      result
        ..add('notifications')
        ..add(serializers.serialize(object.notifications,
            specifiedType: const FullType(
                BuiltList, const [const FullType(NotificationModel)])));
    }
    return result;
  }

  @override
  NotificationsResponseModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NotificationsResponseModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'notifications':
          result.notifications.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(NotificationModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$NotificationsResponseModel extends NotificationsResponseModel {
  @override
  final BuiltList<NotificationModel> notifications;

  factory _$NotificationsResponseModel(
          [void Function(NotificationsResponseModelBuilder) updates]) =>
      (new NotificationsResponseModelBuilder()..update(updates)).build();

  _$NotificationsResponseModel._({this.notifications}) : super._();

  @override
  NotificationsResponseModel rebuild(
          void Function(NotificationsResponseModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NotificationsResponseModelBuilder toBuilder() =>
      new NotificationsResponseModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NotificationsResponseModel &&
        notifications == other.notifications;
  }

  @override
  int get hashCode {
    return $jf($jc(0, notifications.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NotificationsResponseModel')
          ..add('notifications', notifications))
        .toString();
  }
}

class NotificationsResponseModelBuilder
    implements
        Builder<NotificationsResponseModel, NotificationsResponseModelBuilder> {
  _$NotificationsResponseModel _$v;

  ListBuilder<NotificationModel> _notifications;
  ListBuilder<NotificationModel> get notifications =>
      _$this._notifications ??= new ListBuilder<NotificationModel>();
  set notifications(ListBuilder<NotificationModel> notifications) =>
      _$this._notifications = notifications;

  NotificationsResponseModelBuilder();

  NotificationsResponseModelBuilder get _$this {
    if (_$v != null) {
      _notifications = _$v.notifications?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NotificationsResponseModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$NotificationsResponseModel;
  }

  @override
  void update(void Function(NotificationsResponseModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$NotificationsResponseModel build() {
    _$NotificationsResponseModel _$result;
    try {
      _$result = _$v ??
          new _$NotificationsResponseModel._(
              notifications: _notifications?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'notifications';
        _notifications?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'NotificationsResponseModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
