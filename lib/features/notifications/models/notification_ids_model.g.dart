// GENERATED CODE - DO NOT MODIFY BY HAND

part of notification_ids_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<NotificationIds> _$notificationIdsSerializer =
    new _$NotificationIdsSerializer();

class _$NotificationIdsSerializer
    implements StructuredSerializer<NotificationIds> {
  @override
  final Iterable<Type> types = const [NotificationIds, _$NotificationIds];
  @override
  final String wireName = 'NotificationIds';

  @override
  Iterable<Object> serialize(Serializers serializers, NotificationIds object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.notificationIds != null) {
      result
        ..add('notificationIds')
        ..add(serializers.serialize(object.notificationIds,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    return result;
  }

  @override
  NotificationIds deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NotificationIdsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'notificationIds':
          result.notificationIds.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$NotificationIds extends NotificationIds {
  @override
  final BuiltList<String> notificationIds;

  factory _$NotificationIds([void Function(NotificationIdsBuilder) updates]) =>
      (new NotificationIdsBuilder()..update(updates)).build();

  _$NotificationIds._({this.notificationIds}) : super._();

  @override
  NotificationIds rebuild(void Function(NotificationIdsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NotificationIdsBuilder toBuilder() =>
      new NotificationIdsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NotificationIds && notificationIds == other.notificationIds;
  }

  @override
  int get hashCode {
    return $jf($jc(0, notificationIds.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NotificationIds')
          ..add('notificationIds', notificationIds))
        .toString();
  }
}

class NotificationIdsBuilder
    implements Builder<NotificationIds, NotificationIdsBuilder> {
  _$NotificationIds _$v;

  ListBuilder<String> _notificationIds;
  ListBuilder<String> get notificationIds =>
      _$this._notificationIds ??= new ListBuilder<String>();
  set notificationIds(ListBuilder<String> notificationIds) =>
      _$this._notificationIds = notificationIds;

  NotificationIdsBuilder();

  NotificationIdsBuilder get _$this {
    if (_$v != null) {
      _notificationIds = _$v.notificationIds?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NotificationIds other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$NotificationIds;
  }

  @override
  void update(void Function(NotificationIdsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$NotificationIds build() {
    _$NotificationIds _$result;
    try {
      _$result = _$v ??
          new _$NotificationIds._(notificationIds: _notificationIds?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'notificationIds';
        _notificationIds?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'NotificationIds', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
