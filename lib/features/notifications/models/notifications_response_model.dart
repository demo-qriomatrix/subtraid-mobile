library notifications_response_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/notifications/models/notification_model.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notifications_response_model.g.dart';

abstract class NotificationsResponseModel
    implements
        Built<NotificationsResponseModel, NotificationsResponseModelBuilder> {
  @nullable
  BuiltList<NotificationModel> get notifications;

  NotificationsResponseModel._();

  factory NotificationsResponseModel(
          [updates(NotificationsResponseModelBuilder b)]) =
      _$NotificationsResponseModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(NotificationsResponseModel.serializer, this));
  }

  static NotificationsResponseModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        NotificationsResponseModel.serializer, json.decode(jsonString));
  }

  static Serializer<NotificationsResponseModel> get serializer =>
      _$notificationsResponseModelSerializer;
}
