library notification_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notification_model.g.dart';

abstract class NotificationModel
    implements Built<NotificationModel, NotificationModelBuilder> {
  @nullable
  bool get seen;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get notificationType;

  @nullable
  UserModel get from;

  @nullable
  String get owner;

  @nullable
  String get message;

  @nullable
  String get dateCreated;

  NotificationModel._();

  factory NotificationModel([updates(NotificationModelBuilder b)]) =
      _$NotificationModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(NotificationModel.serializer, this));
  }

  static NotificationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        NotificationModel.serializer, json.decode(jsonString));
  }

  static Serializer<NotificationModel> get serializer =>
      _$notificationModelSerializer;
}
