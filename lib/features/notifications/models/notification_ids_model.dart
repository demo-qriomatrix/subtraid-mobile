library notification_ids_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notification_ids_model.g.dart';

abstract class NotificationIds
    implements Built<NotificationIds, NotificationIdsBuilder> {
  @nullable
  BuiltList<String> get notificationIds;

  NotificationIds._();

  factory NotificationIds([updates(NotificationIdsBuilder b)]) =
      _$NotificationIds;

  String toJson() {
    return json
        .encode(serializers.serializeWith(NotificationIds.serializer, this));
  }

  static NotificationIds fromJson(String jsonString) {
    return serializers.deserializeWith(
        NotificationIds.serializer, json.decode(jsonString));
  }

  static Serializer<NotificationIds> get serializer =>
      _$notificationIdsSerializer;
}
