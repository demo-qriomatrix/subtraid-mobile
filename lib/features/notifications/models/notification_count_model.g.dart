// GENERATED CODE - DO NOT MODIFY BY HAND

part of notification_count_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<NotificationCountModel> _$notificationCountModelSerializer =
    new _$NotificationCountModelSerializer();

class _$NotificationCountModelSerializer
    implements StructuredSerializer<NotificationCountModel> {
  @override
  final Iterable<Type> types = const [
    NotificationCountModel,
    _$NotificationCountModel
  ];
  @override
  final String wireName = 'NotificationCountModel';

  @override
  Iterable<Object> serialize(
      Serializers serializers, NotificationCountModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.counts != null) {
      result
        ..add('counts')
        ..add(serializers.serialize(object.counts,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  NotificationCountModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NotificationCountModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'counts':
          result.counts = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$NotificationCountModel extends NotificationCountModel {
  @override
  final int counts;

  factory _$NotificationCountModel(
          [void Function(NotificationCountModelBuilder) updates]) =>
      (new NotificationCountModelBuilder()..update(updates)).build();

  _$NotificationCountModel._({this.counts}) : super._();

  @override
  NotificationCountModel rebuild(
          void Function(NotificationCountModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NotificationCountModelBuilder toBuilder() =>
      new NotificationCountModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NotificationCountModel && counts == other.counts;
  }

  @override
  int get hashCode {
    return $jf($jc(0, counts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NotificationCountModel')
          ..add('counts', counts))
        .toString();
  }
}

class NotificationCountModelBuilder
    implements Builder<NotificationCountModel, NotificationCountModelBuilder> {
  _$NotificationCountModel _$v;

  int _counts;
  int get counts => _$this._counts;
  set counts(int counts) => _$this._counts = counts;

  NotificationCountModelBuilder();

  NotificationCountModelBuilder get _$this {
    if (_$v != null) {
      _counts = _$v.counts;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NotificationCountModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$NotificationCountModel;
  }

  @override
  void update(void Function(NotificationCountModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$NotificationCountModel build() {
    final _$result = _$v ?? new _$NotificationCountModel._(counts: counts);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
