library notification_count_model;

import 'dart:convert';

import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'notification_count_model.g.dart';

abstract class NotificationCountModel
    implements Built<NotificationCountModel, NotificationCountModelBuilder> {
  @nullable
  int get counts;

  NotificationCountModel._();

  factory NotificationCountModel([updates(NotificationCountModelBuilder b)]) =
      _$NotificationCountModel;

  String toJson() {
    return json.encode(
        serializers.serializeWith(NotificationCountModel.serializer, this));
  }

  static NotificationCountModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        NotificationCountModel.serializer, json.decode(jsonString));
  }

  static Serializer<NotificationCountModel> get serializer =>
      _$notificationCountModelSerializer;
}
