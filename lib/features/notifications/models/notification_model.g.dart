// GENERATED CODE - DO NOT MODIFY BY HAND

part of notification_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<NotificationModel> _$notificationModelSerializer =
    new _$NotificationModelSerializer();

class _$NotificationModelSerializer
    implements StructuredSerializer<NotificationModel> {
  @override
  final Iterable<Type> types = const [NotificationModel, _$NotificationModel];
  @override
  final String wireName = 'NotificationModel';

  @override
  Iterable<Object> serialize(Serializers serializers, NotificationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.seen != null) {
      result
        ..add('seen')
        ..add(serializers.serialize(object.seen,
            specifiedType: const FullType(bool)));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.notificationType != null) {
      result
        ..add('notificationType')
        ..add(serializers.serialize(object.notificationType,
            specifiedType: const FullType(String)));
    }
    if (object.from != null) {
      result
        ..add('from')
        ..add(serializers.serialize(object.from,
            specifiedType: const FullType(UserModel)));
    }
    if (object.owner != null) {
      result
        ..add('owner')
        ..add(serializers.serialize(object.owner,
            specifiedType: const FullType(String)));
    }
    if (object.message != null) {
      result
        ..add('message')
        ..add(serializers.serialize(object.message,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  NotificationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NotificationModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'seen':
          result.seen = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'notificationType':
          result.notificationType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'from':
          result.from.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserModel)) as UserModel);
          break;
        case 'owner':
          result.owner = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$NotificationModel extends NotificationModel {
  @override
  final bool seen;
  @override
  final String id;
  @override
  final String notificationType;
  @override
  final UserModel from;
  @override
  final String owner;
  @override
  final String message;
  @override
  final String dateCreated;

  factory _$NotificationModel(
          [void Function(NotificationModelBuilder) updates]) =>
      (new NotificationModelBuilder()..update(updates)).build();

  _$NotificationModel._(
      {this.seen,
      this.id,
      this.notificationType,
      this.from,
      this.owner,
      this.message,
      this.dateCreated})
      : super._();

  @override
  NotificationModel rebuild(void Function(NotificationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NotificationModelBuilder toBuilder() =>
      new NotificationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NotificationModel &&
        seen == other.seen &&
        id == other.id &&
        notificationType == other.notificationType &&
        from == other.from &&
        owner == other.owner &&
        message == other.message &&
        dateCreated == other.dateCreated;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, seen.hashCode), id.hashCode),
                        notificationType.hashCode),
                    from.hashCode),
                owner.hashCode),
            message.hashCode),
        dateCreated.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NotificationModel')
          ..add('seen', seen)
          ..add('id', id)
          ..add('notificationType', notificationType)
          ..add('from', from)
          ..add('owner', owner)
          ..add('message', message)
          ..add('dateCreated', dateCreated))
        .toString();
  }
}

class NotificationModelBuilder
    implements Builder<NotificationModel, NotificationModelBuilder> {
  _$NotificationModel _$v;

  bool _seen;
  bool get seen => _$this._seen;
  set seen(bool seen) => _$this._seen = seen;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _notificationType;
  String get notificationType => _$this._notificationType;
  set notificationType(String notificationType) =>
      _$this._notificationType = notificationType;

  UserModelBuilder _from;
  UserModelBuilder get from => _$this._from ??= new UserModelBuilder();
  set from(UserModelBuilder from) => _$this._from = from;

  String _owner;
  String get owner => _$this._owner;
  set owner(String owner) => _$this._owner = owner;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  NotificationModelBuilder();

  NotificationModelBuilder get _$this {
    if (_$v != null) {
      _seen = _$v.seen;
      _id = _$v.id;
      _notificationType = _$v.notificationType;
      _from = _$v.from?.toBuilder();
      _owner = _$v.owner;
      _message = _$v.message;
      _dateCreated = _$v.dateCreated;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NotificationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$NotificationModel;
  }

  @override
  void update(void Function(NotificationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$NotificationModel build() {
    _$NotificationModel _$result;
    try {
      _$result = _$v ??
          new _$NotificationModel._(
              seen: seen,
              id: id,
              notificationType: notificationType,
              from: _from?.build(),
              owner: owner,
              message: message,
              dateCreated: dateCreated);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'from';
        _from?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'NotificationModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
