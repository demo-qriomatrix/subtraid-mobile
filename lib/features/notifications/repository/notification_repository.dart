import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/notifications/models/notification_count_model.dart';
import 'package:Subtraid/features/notifications/models/notification_ids_model.dart';
import 'package:Subtraid/features/notifications/models/notifications_response_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class NotificationRepository {
  final HttpClient httpClient;

  NotificationRepository({@required this.httpClient});

  Future<NotificationCountModel> getNotificationCount() async {
    final Response response =
        await httpClient.dio.get(EndpointConfig.notificationsCount);

    return NotificationCountModel.fromJson(json.encode(response.data));
  }

  Future<NotificationsResponseModel> getNotifications() async {
    Map<String, dynamic> params = Map();

    params.putIfAbsent('limit', () => 20);
    params.putIfAbsent('skip', () => 0);

    final Response response = await httpClient.dio
        .get(EndpointConfig.userNotifications, queryParameters: params);

    return NotificationsResponseModel.fromJson(json.encode(response.data));
  }

  Future<NotificationsResponseModel> deleteNotifications() async {
    final Response response = await httpClient.dio.delete(
      EndpointConfig.notifications,
    );

    return NotificationsResponseModel.fromJson(json.encode(response.data));
  }

  Future<NotificationsResponseModel> markAsSeen(
      NotificationIds notificationIds) async {
    final Response response = await httpClient.dio.patch(
        EndpointConfig.notifications + '/seen',
        data: notificationIds.toJson());

    return NotificationsResponseModel.fromJson(json.encode(response.data));
  }
}
