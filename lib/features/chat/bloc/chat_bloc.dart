import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/chat/models/chat_model.dart';
import 'package:Subtraid/features/chat/models/chats_model.dart';
import 'package:Subtraid/features/chat/models/message_model.dart';
import 'package:Subtraid/features/chat/models/seen_model.dart';
import 'package:Subtraid/features/chat/repository/chat_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:built_collection/built_collection.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:web_socket_channel/io.dart';

part 'chat_event.dart';
part 'chat_state.dart';

Comparator<ChatModel> sortByDate = (b, a) => DateTime.now()
    .difference(DateTime.parse(
        b.messages.isNotEmpty ? b.messages.first.dateCreated : b.dateCreated))
    .inSeconds
    .compareTo(DateTime.now()
        .difference(DateTime.parse(a.messages.isNotEmpty
            ? a.messages.first.dateCreated
            : a.dateCreated))
        .inSeconds);

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc({@required this.chatRepository, @required this.authenticationBloc})
      : super(ChatInitial(selectedIndex: 0, createGroup: false)) {
    add(ChatsRequested(loading: true));

    startWebSocket();
  }

  final ChatRepository chatRepository;
  final AuthenticationBloc authenticationBloc;

  IOWebSocketChannel channel;
  Stream<dynamic> stream;

  @override
  Stream<ChatState> mapEventToState(
    ChatEvent event,
  ) async* {
    if (event is ChatUpdateSeen) {
      yield* _mapChatUpdateSeenToState(event.seenModel);
    }

    if (event is ChatCreateGroup) {
      yield* _mapChatCreateGroupToState(event);
    }

    if (event is ChatsRequested) {
      yield* _mapChatsRequestedToState(event);
    }

    if (event is ChatsIndexChanged) {
      yield* _mapChatsIndexChangedToState(event);
    }

    if (event is ChatAddMessage) {
      _mapChatAddMessage(event.message);
    }
  }

  _mapChatAddMessage(MessageModel message) async {
    try {
      if (state is ChatLoadSuccess) {
        Map<String, ChatModel> allChatsMap =
            (state as ChatLoadSuccess).allChatsMap;

        if (allChatsMap.containsKey(message.chatId)) {
          ChatModel chatModel = allChatsMap[message.chatId];
          if (chatModel != null) {
            chatModel = chatModel.rebuild((e) =>
                e.messages = ListBuilder([message, ...chatModel.messages]));

            add(ChatsRequested(update: chatModel));
          }
        }
      }

      await chatRepository.addMessage(message);
      add(ChatsRequested());
    } catch (e) {
      if (state is ChatLoadSuccess) {
        Map<String, ChatModel> allChatsMap =
            (state as ChatLoadSuccess).allChatsMap;

        if (allChatsMap.containsKey(message.chatId)) {
          ChatModel chatModel = allChatsMap[message.chatId];

          List<MessageModel> messages = chatModel.messages.toList();

          if (messages.first.content == message.content) {
            messages[0] = messages.first.rebuild((e) => e..failed = true);

            if (chatModel != null) {
              chatModel =
                  chatModel.rebuild((e) => e.messages = ListBuilder(messages));

              add(ChatsRequested(update: chatModel));
            }
          }
        }
      }
    }
  }

  Stream<ChatState> _mapChatUpdateSeenToState(SeenModel seenModel) async* {
    try {
      await chatRepository.updateSeen(seenModel);

      if (state is ChatLoadSuccess) {
        Map<String, ChatModel> allChatsMap =
            (state as ChatLoadSuccess).allChatsMap;

        if (allChatsMap.containsKey(seenModel.chatId)) {
          ChatModel chatModel = allChatsMap[seenModel.chatId];

          if (authenticationBloc.state is AuthentcatedState) {
            String userId =
                (authenticationBloc.state as AuthentcatedState).user.id;

            List<SeenModel> seen = chatModel.seen.toList();

            int index = seen.indexWhere((element) => element.userId == userId);

            if (index != -1) {
              seen[index] = seenModel;

              chatModel = chatModel.rebuild((e) => e.seen = ListBuilder(seen));
            } else {
              chatModel = chatModel.rebuild(
                  (e) => e.seen = ListBuilder([seenModel, ...chatModel.seen]));
            }

            add(ChatsRequested(update: chatModel));
          }
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Stream<ChatState> _mapChatCreateGroupToState(ChatCreateGroup event) async* {
    if (state is ChatLoadSuccess)
      yield ChatLoadSuccess(
          allChatsMap: (state as ChatLoadSuccess).allChatsMap,
          chats: (state as ChatLoadSuccess).chats,
          all: (state as ChatLoadSuccess).all,
          groups: (state as ChatLoadSuccess).groups,
          project: (state as ChatLoadSuccess).project,
          createGroup: event.createGroup,
          selectedIndex: state.selectedIndex);

    if (state is ChatLoadInProgress)
      yield ChatLoadInProgress(
          selectedIndex: state.selectedIndex, createGroup: event.createGroup);
    if (state is ChatLoadFailure)
      yield ChatLoadFailure(
          selectedIndex: state.selectedIndex, createGroup: event.createGroup);
  }

  Stream<ChatState> _mapChatsRequestedToState(ChatsRequested event) async* {
    try {
      Map<String, ChatModel> allChatsMap;

      if (event.update != null) {
        if (state is ChatLoadSuccess) {
          allChatsMap = (state as ChatLoadSuccess).allChatsMap;

          if (allChatsMap.containsKey(event.update.id)) {
            allChatsMap[event.update.id] = event.update;
          } else {
            allChatsMap.putIfAbsent(event.update.id, () => event.update);
          }
        }
      } else {
        if (event.loading == true)
          yield ChatLoadInProgress(
              selectedIndex: state.selectedIndex,
              createGroup: state.createGroup);

        ChatsModel chatsModel = await chatRepository.getChats();

        List<ChatModel> allChats =
            chatsModel.chats.where((chat) => chat.version >= 2).toList();

        Map<String, ChatModel> allChatsMapLocal = Map();

        allChats.forEach((element) {
          allChatsMapLocal.putIfAbsent(element.id, () => element);
        });

        allChatsMap = allChatsMapLocal;
      }

      List<ChatModel> allChats = allChatsMap.values.toList();

      allChats.sort(sortByDate);

      List<String> all = allChats.map((e) => e.id).toList();

      List<String> chats = allChats
          .where(
              (element) => element.type != null && element.type == 'personal')
          .map((e) => e.id)
          .toList();

      List<String> groups = allChats
          .where((element) => element.type != null && (element.type == 'group'))
          .map((e) => e.id)
          .toList();
      List<String> project = allChats
          .where(
              (element) => element.type != null && (element.type == 'project'))
          .map((e) => e.id)
          .toList();

      yield ChatLoadSuccess(
          chats: chats,
          all: all,
          groups: groups,
          project: project,
          allChatsMap: Map.from(allChatsMap),
          selectedIndex: state.selectedIndex,
          createGroup: false);
    } catch (e) {
      print(e);
      yield ChatLoadFailure(
          selectedIndex: state.selectedIndex, createGroup: false);
    }
  }

  Stream<ChatState> _mapChatsIndexChangedToState(
      ChatsIndexChanged event) async* {
    if (state is ChatLoadSuccess)
      yield ChatLoadSuccess(
          allChatsMap: (state as ChatLoadSuccess).allChatsMap,
          chats: (state as ChatLoadSuccess).chats,
          all: (state as ChatLoadSuccess).all,
          groups: (state as ChatLoadSuccess).groups,
          project: (state as ChatLoadSuccess).project,
          createGroup: false,
          selectedIndex: event.index);

    if (state is ChatLoadInProgress)
      yield ChatLoadInProgress(selectedIndex: event.index, createGroup: false);
    if (state is ChatLoadFailure)
      yield ChatLoadFailure(selectedIndex: event.index, createGroup: false);
  }

  void startWebSocket() {
    String userId;
    try {
      if (authenticationBloc.state is AuthentcatedState) {
        userId = (authenticationBloc.state as AuthentcatedState).user.id;
      }

      // channel = IOWebSocketChannel.connect(
      //     'wss://dev.api.subtraid.com/socket.io/?EIO=3&transport=websocket');
      // stream = channel.stream.asBroadcastStream();

      // stream.listen((event) {
      //   print(event);

      //   channel.sink.add('42["joinChat",{"userId":"601eb9a35bc1a92722401071"}]');
      // });

      Socket socket = io(
          'https://dev.api.subtraid.com',
          OptionBuilder()
              //
              .setPath('/socket.io')
              //
              .setTransports(['websocket']).build());

      socket.on('event', (data) => print(data));

      socket.onConnect((data) {
        socket.emit('joinChat', {'userId': '$userId'});
      });

      socket.on('newMessage', (data) {
        log('newMessage: ' + data.toString());
        handleNewMessage(data);
      });

      socket.connect();
    } catch (e) {
      print('--------');
      print('error');
      log(e.toString());
      print('--------');
    }
  }

  handleNewMessage(data) {
    print('handleNewMessage');
    try {
      MessageModel newMessage = MessageModel.fromJson(json.encode(data));

      if (state is ChatLoadSuccess) {
        Map<String, ChatModel> allChatsMap =
            (state as ChatLoadSuccess).allChatsMap;

        if (allChatsMap.containsKey(newMessage.chatId)) {
          ChatModel chatModel = allChatsMap[newMessage.chatId];

          chatModel = chatModel.rebuild((e) =>
              e.messages = ListBuilder([newMessage, ...chatModel.messages]));

          print('yield');
          add(ChatsRequested(update: chatModel));
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
