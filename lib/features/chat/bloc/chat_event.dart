part of 'chat_bloc.dart';

abstract class ChatEvent extends Equatable {
  const ChatEvent();

  @override
  List<Object> get props => [];
}

class ChatsRequested extends ChatEvent {
  final ChatModel update;
  final bool loading;

  ChatsRequested({this.update, this.loading});
}

class ChatsIndexChanged extends ChatEvent {
  final int index;

  ChatsIndexChanged({@required this.index});
}

class ChatCreateGroup extends ChatEvent {
  final bool createGroup;

  ChatCreateGroup({@required this.createGroup});
}

class ChatUpdateSeen extends ChatEvent {
  final SeenModel seenModel;

  ChatUpdateSeen({@required this.seenModel});
}

class ChatAddMessage extends ChatEvent {
  final MessageModel message;

  ChatAddMessage({@required this.message});
}
