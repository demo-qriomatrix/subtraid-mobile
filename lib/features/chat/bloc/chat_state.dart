part of 'chat_bloc.dart';

abstract class ChatState extends Equatable {
  final int selectedIndex;
  final bool createGroup;

  ChatState({
    @required this.selectedIndex,
    @required this.createGroup,
  });

  @override
  List<Object> get props => [selectedIndex, createGroup];
}

class ChatInitial extends ChatState {
  ChatInitial({
    @required int selectedIndex,
    @required bool createGroup,
  }) : super(selectedIndex: selectedIndex, createGroup: createGroup);
}

class ChatLoadInProgress extends ChatState {
  ChatLoadInProgress({
    @required int selectedIndex,
    @required bool createGroup,
  }) : super(selectedIndex: selectedIndex, createGroup: createGroup);
}

class ChatLoadSuccess extends ChatState {
  final List<String> all;
  final List<String> chats;
  final List<String> groups;
  final List<String> project;
  final Map<String, ChatModel> allChatsMap;

  ChatLoadSuccess({
    @required this.all,
    @required this.chats,
    @required this.groups,
    @required this.project,
    @required this.allChatsMap,
    @required bool createGroup,
    @required int selectedIndex,
  }) : super(selectedIndex: selectedIndex, createGroup: createGroup);

  @override
  List<Object> get props => [
        all,
        chats,
        groups,
        project,
        createGroup,
        selectedIndex,
        allChatsMap,
        allChatsMap.hashCode
      ];
}

class ChatLoadFailure extends ChatState {
  ChatLoadFailure({
    @required int selectedIndex,
    @required bool createGroup,
  }) : super(selectedIndex: selectedIndex, createGroup: createGroup);
}
