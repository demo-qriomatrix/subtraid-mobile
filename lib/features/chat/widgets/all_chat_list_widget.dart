// import 'package:flutter/material.dart';
// import 'package:Subtraid/core/config/color_config.dart';
// import 'package:Subtraid/core/enums/online_status_enum.dart';

// import 'new_message_widget.dart';
// import 'old_message_widget.dart';

// class AllChatList extends StatelessWidget {
//   const AllChatList({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: <Widget>[
//         SizedBox(
//           height: 20,
//         ),
//         Padding(
//           padding: const EdgeInsets.only(left: 30),
//           child: Text(
//             'New Messages',
//             style: TextStyle(
//                 color: ColorConfig.primary,
//                 fontSize: 14,
//                 fontWeight: FontWeight.w500),
//           ),
//         ),
//         SizedBox(
//           height: 20,
//         ),
//         Row(
//           children: <Widget>[
//             SizedBox(
//               width: 30,
//             ),
//             NewMessage(),
//             NewMessage(),
//             NewMessage()
//           ],
//         ),
//         SizedBox(
//           height: 10,
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.end,
//           children: <Widget>[
//             Text(
//               'Clear',
//               style: TextStyle(color: ColorConfig.orange),
//             ),
//             SizedBox(
//               width: 30,
//             ),
//           ],
//         ),
//         OldMessage(
//           image:
//               'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
//           chatFrom: 'Maheeshi Peris',
//           message: 'You : Yes 10:12 AM',
//           onlineStatus: OnlineStatus.online,
//         ),
//         OldMessage(
//           image: 'https://via.placeholder.com/150',
//           chatFrom: 'Subtraid Plumbing',
//           message: 'You : You have assigned to SAT',
//           // onlineStatus: OnlineStatus.online,
//         ),
//         OldMessage(
//           image:
//               'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
//           chatFrom: 'Maheeshi Peris',
//           message: 'You : I have sent to boss MAY 6',
//           onlineStatus: OnlineStatus.online,
//         ),
//         OldMessage(
//           image:
//               'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
//           chatFrom: 'Maheeshi Peris',
//           message: 'You : Thank You JAN 15',
//           onlineStatus: OnlineStatus.offline,
//         ),
//         SizedBox(
//           height: 20,
//         ),
//         Row(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Text(
//               'See all messages',
//               style: TextStyle(color: ColorConfig.orange),
//             )
//           ],
//         ),
//       ],
//     );
//   }
// }
