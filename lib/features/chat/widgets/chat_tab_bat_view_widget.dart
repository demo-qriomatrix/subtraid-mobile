import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/features/chat/bloc/chat_bloc.dart';

import 'chat_list_widget.dart';

class ChatsTabBarViewWidget extends StatelessWidget {
  const ChatsTabBarViewWidget({
    Key key,
    @required this.tabController,
  }) : super(key: key);

  final TabController tabController;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(builder: (context, state) {
      if (state is ChatLoadSuccess) {
        return TabBarView(
          children: <Widget>[
            ChatListWidget(
              chatList: state.all,
            ),
            ChatListWidget(
              chatList: state.chats,
            ),
            ChatListWidget(
              chatList: state.groups,
            ),
            ChatListWidget(
              chatList: state.project,
            ),
          ],
          controller: tabController,
        );
      }

      return Container();
    });
  }
}
