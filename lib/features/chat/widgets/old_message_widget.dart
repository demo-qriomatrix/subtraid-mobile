// import 'package:flutter/material.dart';
// import 'package:Subtraid/core/config/color_config.dart';
// import 'package:Subtraid/core/enums/online_status_enum.dart';
// import 'package:Subtraid/core/helpers/get_online_color_from_status.dart';
// import 'package:Subtraid/features/chat/view/chat_screen.dart';

// class OldMessage extends StatelessWidget {
//   final String chatFrom;
//   final String message;
//   final String image;
//   final OnlineStatus onlineStatus;

//   OldMessage({this.chatFrom, this.message, this.image, this.onlineStatus});

//   @override
//   Widget build(BuildContext context) {
//     return Material(
//       color: Colors.white,
//       child: InkWell(
//         onTap: () {
//           Navigator.of(context)
//               .push(MaterialPageRoute(builder: (context) => ChatScreen()));
//         },
//         child: Padding(
//           padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
//           child: Row(
//             children: <Widget>[
//               Stack(
//                 children: <Widget>[
//                   CircleAvatar(
//                     radius: 25,
//                     backgroundImage: NetworkImage(this.image),
//                   ),
//                   Positioned(
//                     bottom: 1,
//                     right: 1,
//                     child: Container(
//                       height: 15,
//                       width: 15,
//                       decoration: BoxDecoration(
//                         color: getOnlineStatusColor(onlineStatus),
//                         shape: BoxShape.circle,
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//               SizedBox(
//                 width: 20,
//               ),
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     this.chatFrom,
//                     style: TextStyle(fontWeight: FontWeight.w500),
//                   ),
//                   SizedBox(
//                     height: 5,
//                   ),
//                   Text(
//                     this.message,
//                     style: TextStyle(color: Colors.grey),
//                   ),
//                 ],
//               ),
//               Spacer(),
//               Theme(
//                 data: ThemeData(unselectedWidgetColor: ColorConfig.primary),
//                 child: Checkbox(
//                   value: false,
//                   onChanged: (val) {},
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
