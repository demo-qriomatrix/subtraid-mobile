import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/authentication/models/user_model.dart';
import 'package:Subtraid/features/chat/bloc/chat_bloc.dart';
import 'package:Subtraid/features/chat/models/seen_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/features/chat/models/chat_model.dart';
import 'package:Subtraid/features/chat/view/chat_screen.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatListWidget extends StatelessWidget {
  final List<String> chatList;

  ChatListWidget({@required this.chatList});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        if (state is ChatLoadSuccess)
          return ListView(
            children: chatList
                .map((chat) => ChatListItemWidget(
                      chat: state.allChatsMap[chat],
                    ))
                .toList(),
          );

        return Container();
      },
    );
  }
}

class ChatListItemWidget extends StatelessWidget {
  final ChatModel chat;

  ChatListItemWidget({@required this.chat});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => ChatScreen(chat.id)));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          child: Row(
            children: [
              getAvatar(chat),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    getReceipientNameAndTime(chat, context),
                    SizedBox(
                      height: 5,
                    ),
                    getMessageContent(chat),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getAvatar(ChatModel chat) {
    if (chat.type == 'personal') {
      return CircleAvatar(
        backgroundImage: AssetImage(LocalImages.defaultAvatar),
        radius: 20,
      );
    }
    if (chat.type == 'group' || chat.type == 'project') {
      if (chat.members.length == 1) {
        return CircleAvatar(
          backgroundImage: AssetImage(LocalImages.defaultAvatar),
          radius: 20,
        );
      }
      if (chat.members.length == 2) {
        return Row(
          children: [
            CircleAvatar(
              backgroundImage: AssetImage(LocalImages.defaultAvatar),
              radius: 10,
            ),
            CircleAvatar(
              backgroundImage: AssetImage(LocalImages.defaultAvatar),
              radius: 10,
            ),
          ],
        );
      }
      if (chat.members.length == 3) {
        return Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
              ],
            ),
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
              ],
            ),
          ],
        );
      }
      if (chat.members.length >= 4) {
        return Column(
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
              ],
            ),
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
                CircleAvatar(
                  backgroundImage: AssetImage(LocalImages.defaultAvatar),
                  radius: 10,
                ),
              ],
            ),
          ],
        );
      }
    }

    return Container(
      width: 40,
    );
    // return Stack(
    //   children: <Widget>[
    //     CircleAvatar(
    //       backgroundImage: AssetImage(LocalImages.defaultAvatar),
    //       radius: 20,
    //     ),
    //     // Positioned(
    //     //   bottom: 1,
    //     //   right: 1,
    //     //   child: Container(
    //     //     height: 15,
    //     //     width: 15,
    //     //     decoration: BoxDecoration(
    //     //       color: getOnlineStatusColor(OnlineStatus.online),
    //     //       shape: BoxShape.circle,
    //     //     ),
    //     //   ),
    //     // )
    //   ],
    // );
  }
}

String getReceipientNameString(ChatModel chat, BuildContext context) {
  UserModel user;

  String name = '';

  if (chat.type == 'personal' && chat.messages.isNotEmpty) {
    if (context.read<AuthenticationBloc>().state is AuthentcatedState) {
      user =
          (context.read<AuthenticationBloc>().state as AuthentcatedState).user;
    }
    if (user != null) {
      MemberModel author = chat.members
          .firstWhere((member) => member.id != user.id, orElse: () => null);
      if (author != null) name = author.name;
    }
  }

  if (chat.type == 'project' || chat.type == 'group') {
    name = chat.name;
  }

  if (name == null || name == '') name = (chat.type + ' ' + chat.id);

  return name;
}

Row getReceipientNameAndTime(ChatModel chat, BuildContext context) {
  UserModel user;

  String name = '';

  if (context.read<AuthenticationBloc>().state is AuthentcatedState) {
    user = (context.read<AuthenticationBloc>().state as AuthentcatedState).user;
  }

  if (chat.type == 'personal' && chat.messages.isNotEmpty) {
    if (user != null) {
      MemberModel author = chat.members
          .firstWhere((member) => member.id != user.id, orElse: () => null);
      if (author != null) name = author.name;
    }
  }

  if (chat.type == 'project' || chat.type == 'group') {
    name = chat.name;
  }

  if (name == null || name == '') name = (chat.type + ' ' + chat.id);

  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Expanded(
        child: Text(
          name,
          style: TextStyle(fontWeight: FontWeight.w500),
        ),
      ),
      getUnreadCount(chat, user) > 0
          ? Container(
              decoration: BoxDecoration(
                  color: ColorConfig.red1, shape: BoxShape.circle),
              padding: EdgeInsets.all(5),
              child: Text(
                getUnreadCount(chat, user).toString(),
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            )
          : Container(),
    ],
  );
}

int getUnreadCount(ChatModel chat, UserModel user) {
  if (user != null) {
    SeenModel lastSeen =
        chat.seen.firstWhere((e) => e.userId == user.id, orElse: () => null);

    if (lastSeen != null) {
      int count = chat.messages
          .where((e) => e.id != null)
          .takeWhile((e) => lastSeen.messageId != e.id)
          .length;

      return count;
    }
  }

  return 0;
}

Row getMessageContent(ChatModel chat) {
  String message = '';
  String date = '';

  if (chat.messages.isNotEmpty && chat.messages.first.type == 'text') {
    message = chat.messages.first.content;
  }

  if (chat.messages.isNotEmpty) {
    date = chat.messages.first.dateCreated;
    date = DateFormat('HH:mm aa dd-MMM').format(DateTime.parse(date).toLocal());
  }

  return Row(
    children: [
      Expanded(
        child: Text(
          message ?? '',
          style: TextStyle(
              color: Colors.grey.shade500,
              fontWeight: FontWeight.w500,
              fontSize: 14),
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      SizedBox(
        width: 20,
      ),
      Text(
        date,
        style: TextStyle(color: Colors.grey, fontSize: 12),
      ),
    ],
  );

  //  Text(
  //       date,
  //       style: TextStyle(fontWeight: FontWeight.w400, fontSize: 12),
  //     ),
}
