import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';

class Participant extends StatelessWidget {
  const Participant({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 20,
            backgroundImage: NetworkImage(
              'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Text(
            'John Doe',
            style: TextStyle(
                fontSize: 13,
                color: ColorConfig.primary,
                fontWeight: FontWeight.w400),
          )
        ],
      ),
    );
  }
}
