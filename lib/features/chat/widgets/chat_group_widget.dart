import 'package:flutter/material.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/chat/widgets/participant_widget.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/buttons.dart';

class CreateGroupWidget extends StatelessWidget {
  const CreateGroupWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Text(
            'Create Group',
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: ColorConfig.primary),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Group name',
            style: TextStyle(
                fontSize: 12, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: profileInputDecoration.copyWith(
                fillColor: ColorConfig.blue17,
                hintText: 'Type Group Name',
                hintStyle: TextStyle(color: ColorConfig.black1)),
            maxLines: null,
            style: textStyle2,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Add People',
            style: TextStyle(
                fontSize: 12, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  decoration: profileInputDecoration.copyWith(
                      fillColor: ColorConfig.blue17,
                      hintText: 'Search People',
                      hintStyle: TextStyle(color: ColorConfig.black1)),
                  maxLines: null,
                  style: textStyle2,
                ),
              ),
              SizedBox(
                width: 20,
              ),
              RawMaterialButton(
                fillColor: ColorConfig.primary,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                constraints: BoxConstraints(),
                onPressed: () {},
                shape: CircleBorder(),
                padding: const EdgeInsets.all(4.0),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 24,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            '2 participants',
            style: TextStyle(
                fontSize: 13, fontWeight: FontWeight.w500, color: Colors.black),
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            children: <Widget>[
              Participant(),
              Participant(),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SaveButton('Create Group', () {
                // setState(() {
                //   createGroup = false;
                // });
              })
            ],
          )
        ],
      ),
    );
  }
}
