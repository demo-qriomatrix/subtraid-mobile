// import 'package:flutter/material.dart';
// import 'package:Subtraid/core/config/color_config.dart';
// import 'package:Subtraid/features/chat/view/chat_screen.dart';

// class NewMessage extends StatelessWidget {
//   const NewMessage({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.only(right: 20),
//       child: Column(
//         children: <Widget>[
//           Stack(
//             children: <Widget>[
//               InkWell(
//                 onTap: () {
//                   Navigator.of(context).push(
//                       MaterialPageRoute(builder: (context) => ChatScreen()));
//                 },
//                 child: CircleAvatar(
//                   radius: 30,
//                   backgroundImage: NetworkImage(
//                       'https://images.pexels.com/photos/1222271/pexels-photo-1222271.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
//                 ),
//               ),
//               Positioned(
//                 bottom: 0,
//                 right: 0,
//                 child: Container(
//                   decoration: BoxDecoration(
//                     color: Colors.red,
//                     shape: BoxShape.circle,
//                   ),
//                   child: Padding(
//                     padding: EdgeInsets.all(5.0),
//                     child: Text(
//                       '1',
//                       style: TextStyle(color: Colors.white, fontSize: 12),
//                     ),
//                   ),
//                 ),
//               )
//             ],
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           InkWell(
//             onTap: () {
//               Navigator.of(context)
//                   .push(MaterialPageRoute(builder: (context) => ChatScreen()));
//             },
//             child: Text(
//               'Jone',
//               style: TextStyle(color: ColorConfig.primary, fontSize: 12),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
