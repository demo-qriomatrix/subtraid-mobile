// GENERATED CODE - DO NOT MODIFY BY HAND

part of chats_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ChatsModel> _$chatsModelSerializer = new _$ChatsModelSerializer();

class _$ChatsModelSerializer implements StructuredSerializer<ChatsModel> {
  @override
  final Iterable<Type> types = const [ChatsModel, _$ChatsModel];
  @override
  final String wireName = 'ChatsModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ChatsModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.chats != null) {
      result
        ..add('chats')
        ..add(serializers.serialize(object.chats,
            specifiedType:
                const FullType(BuiltList, const [const FullType(ChatModel)])));
    }
    return result;
  }

  @override
  ChatsModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ChatsModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'chats':
          result.chats.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ChatModel)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$ChatsModel extends ChatsModel {
  @override
  final BuiltList<ChatModel> chats;

  factory _$ChatsModel([void Function(ChatsModelBuilder) updates]) =>
      (new ChatsModelBuilder()..update(updates)).build();

  _$ChatsModel._({this.chats}) : super._();

  @override
  ChatsModel rebuild(void Function(ChatsModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChatsModelBuilder toBuilder() => new ChatsModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChatsModel && chats == other.chats;
  }

  @override
  int get hashCode {
    return $jf($jc(0, chats.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChatsModel')..add('chats', chats))
        .toString();
  }
}

class ChatsModelBuilder implements Builder<ChatsModel, ChatsModelBuilder> {
  _$ChatsModel _$v;

  ListBuilder<ChatModel> _chats;
  ListBuilder<ChatModel> get chats =>
      _$this._chats ??= new ListBuilder<ChatModel>();
  set chats(ListBuilder<ChatModel> chats) => _$this._chats = chats;

  ChatsModelBuilder();

  ChatsModelBuilder get _$this {
    if (_$v != null) {
      _chats = _$v.chats?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChatsModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChatsModel;
  }

  @override
  void update(void Function(ChatsModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChatsModel build() {
    _$ChatsModel _$result;
    try {
      _$result = _$v ?? new _$ChatsModel._(chats: _chats?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'chats';
        _chats?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ChatsModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
