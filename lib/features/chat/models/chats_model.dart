library chats_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

import 'chat_model.dart';

part 'chats_model.g.dart';

abstract class ChatsModel implements Built<ChatsModel, ChatsModelBuilder> {
  @nullable
  BuiltList<ChatModel> get chats;

  ChatsModel._();

  factory ChatsModel([updates(ChatsModelBuilder b)]) = _$ChatsModel;

  String toJson() {
    return json.encode(serializers.serializeWith(ChatsModel.serializer, this));
  }

  static ChatsModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ChatsModel.serializer, json.decode(jsonString));
  }

  static Serializer<ChatsModel> get serializer => _$chatsModelSerializer;
}
