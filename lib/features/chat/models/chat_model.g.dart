// GENERATED CODE - DO NOT MODIFY BY HAND

part of chat_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ChatModel> _$chatModelSerializer = new _$ChatModelSerializer();

class _$ChatModelSerializer implements StructuredSerializer<ChatModel> {
  @override
  final Iterable<Type> types = const [ChatModel, _$ChatModel];
  @override
  final String wireName = 'ChatModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ChatModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.members != null) {
      result
        ..add('members')
        ..add(serializers.serialize(object.members,
            specifiedType: const FullType(
                BuiltList, const [const FullType(MemberModel)])));
    }
    if (object.messages != null) {
      result
        ..add('messages')
        ..add(serializers.serialize(object.messages,
            specifiedType: const FullType(
                BuiltList, const [const FullType(MessageModel)])));
    }
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.chatId != null) {
      result
        ..add('chatId')
        ..add(serializers.serialize(object.chatId,
            specifiedType: const FullType(String)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.seen != null) {
      result
        ..add('seen')
        ..add(serializers.serialize(object.seen,
            specifiedType:
                const FullType(BuiltList, const [const FullType(SeenModel)])));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.version != null) {
      result
        ..add('__v')
        ..add(serializers.serialize(object.version,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ChatModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ChatModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'members':
          result.members.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(MemberModel)]))
              as BuiltList<Object>);
          break;
        case 'messages':
          result.messages.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(MessageModel)]))
              as BuiltList<Object>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'chatId':
          result.chatId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'seen':
          result.seen.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SeenModel)]))
              as BuiltList<Object>);
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '__v':
          result.version = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ChatModel extends ChatModel {
  @override
  final BuiltList<MemberModel> members;
  @override
  final BuiltList<MessageModel> messages;
  @override
  final String id;
  @override
  final String chatId;
  @override
  final String type;
  @override
  final BuiltList<SeenModel> seen;
  @override
  final String name;
  @override
  final String dateCreated;
  @override
  final int version;

  factory _$ChatModel([void Function(ChatModelBuilder) updates]) =>
      (new ChatModelBuilder()..update(updates)).build();

  _$ChatModel._(
      {this.members,
      this.messages,
      this.id,
      this.chatId,
      this.type,
      this.seen,
      this.name,
      this.dateCreated,
      this.version})
      : super._();

  @override
  ChatModel rebuild(void Function(ChatModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChatModelBuilder toBuilder() => new ChatModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChatModel &&
        members == other.members &&
        messages == other.messages &&
        id == other.id &&
        chatId == other.chatId &&
        type == other.type &&
        seen == other.seen &&
        name == other.name &&
        dateCreated == other.dateCreated &&
        version == other.version;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, members.hashCode),
                                    messages.hashCode),
                                id.hashCode),
                            chatId.hashCode),
                        type.hashCode),
                    seen.hashCode),
                name.hashCode),
            dateCreated.hashCode),
        version.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChatModel')
          ..add('members', members)
          ..add('messages', messages)
          ..add('id', id)
          ..add('chatId', chatId)
          ..add('type', type)
          ..add('seen', seen)
          ..add('name', name)
          ..add('dateCreated', dateCreated)
          ..add('version', version))
        .toString();
  }
}

class ChatModelBuilder implements Builder<ChatModel, ChatModelBuilder> {
  _$ChatModel _$v;

  ListBuilder<MemberModel> _members;
  ListBuilder<MemberModel> get members =>
      _$this._members ??= new ListBuilder<MemberModel>();
  set members(ListBuilder<MemberModel> members) => _$this._members = members;

  ListBuilder<MessageModel> _messages;
  ListBuilder<MessageModel> get messages =>
      _$this._messages ??= new ListBuilder<MessageModel>();
  set messages(ListBuilder<MessageModel> messages) =>
      _$this._messages = messages;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _chatId;
  String get chatId => _$this._chatId;
  set chatId(String chatId) => _$this._chatId = chatId;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  ListBuilder<SeenModel> _seen;
  ListBuilder<SeenModel> get seen =>
      _$this._seen ??= new ListBuilder<SeenModel>();
  set seen(ListBuilder<SeenModel> seen) => _$this._seen = seen;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  int _version;
  int get version => _$this._version;
  set version(int version) => _$this._version = version;

  ChatModelBuilder();

  ChatModelBuilder get _$this {
    if (_$v != null) {
      _members = _$v.members?.toBuilder();
      _messages = _$v.messages?.toBuilder();
      _id = _$v.id;
      _chatId = _$v.chatId;
      _type = _$v.type;
      _seen = _$v.seen?.toBuilder();
      _name = _$v.name;
      _dateCreated = _$v.dateCreated;
      _version = _$v.version;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChatModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChatModel;
  }

  @override
  void update(void Function(ChatModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChatModel build() {
    _$ChatModel _$result;
    try {
      _$result = _$v ??
          new _$ChatModel._(
              members: _members?.build(),
              messages: _messages?.build(),
              id: id,
              chatId: chatId,
              type: type,
              seen: _seen?.build(),
              name: name,
              dateCreated: dateCreated,
              version: version);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'members';
        _members?.build();
        _$failedField = 'messages';
        _messages?.build();

        _$failedField = 'seen';
        _seen?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ChatModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
