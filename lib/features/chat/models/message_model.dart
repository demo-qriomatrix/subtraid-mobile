library message_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';

part 'message_model.g.dart';

abstract class MessageModel
    implements Built<MessageModel, MessageModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  MemberModel get author;

  @nullable
  String get type;

  @nullable
  String get content;

  @nullable
  String get chatId;

  @nullable
  String get dateCreated;

  @nullable
  bool get failed;

  MessageModel._();

  factory MessageModel([updates(MessageModelBuilder b)]) = _$MessageModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(MessageModel.serializer, this));
  }

  static MessageModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        MessageModel.serializer, json.decode(jsonString));
  }

  static Serializer<MessageModel> get serializer => _$messageModelSerializer;
}
