// GENERATED CODE - DO NOT MODIFY BY HAND

part of seen_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SeenModel> _$seenModelSerializer = new _$SeenModelSerializer();

class _$SeenModelSerializer implements StructuredSerializer<SeenModel> {
  @override
  final Iterable<Type> types = const [SeenModel, _$SeenModel];
  @override
  final String wireName = 'SeenModel';

  @override
  Iterable<Object> serialize(Serializers serializers, SeenModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.userId != null) {
      result
        ..add('userId')
        ..add(serializers.serialize(object.userId,
            specifiedType: const FullType(String)));
    }
    if (object.messageId != null) {
      result
        ..add('messageId')
        ..add(serializers.serialize(object.messageId,
            specifiedType: const FullType(String)));
    }
    if (object.chatId != null) {
      result
        ..add('chatId')
        ..add(serializers.serialize(object.chatId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  SeenModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SeenModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userId':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'messageId':
          result.messageId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'chatId':
          result.chatId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SeenModel extends SeenModel {
  @override
  final String id;
  @override
  final String userId;
  @override
  final String messageId;
  @override
  final String chatId;

  factory _$SeenModel([void Function(SeenModelBuilder) updates]) =>
      (new SeenModelBuilder()..update(updates)).build();

  _$SeenModel._({this.id, this.userId, this.messageId, this.chatId})
      : super._();

  @override
  SeenModel rebuild(void Function(SeenModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SeenModelBuilder toBuilder() => new SeenModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SeenModel &&
        id == other.id &&
        userId == other.userId &&
        messageId == other.messageId &&
        chatId == other.chatId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), userId.hashCode), messageId.hashCode),
        chatId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SeenModel')
          ..add('id', id)
          ..add('userId', userId)
          ..add('messageId', messageId)
          ..add('chatId', chatId))
        .toString();
  }
}

class SeenModelBuilder implements Builder<SeenModel, SeenModelBuilder> {
  _$SeenModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _userId;
  String get userId => _$this._userId;
  set userId(String userId) => _$this._userId = userId;

  String _messageId;
  String get messageId => _$this._messageId;
  set messageId(String messageId) => _$this._messageId = messageId;

  String _chatId;
  String get chatId => _$this._chatId;
  set chatId(String chatId) => _$this._chatId = chatId;

  SeenModelBuilder();

  SeenModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _userId = _$v.userId;
      _messageId = _$v.messageId;
      _chatId = _$v.chatId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SeenModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SeenModel;
  }

  @override
  void update(void Function(SeenModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SeenModel build() {
    final _$result = _$v ??
        new _$SeenModel._(
            id: id, userId: userId, messageId: messageId, chatId: chatId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
