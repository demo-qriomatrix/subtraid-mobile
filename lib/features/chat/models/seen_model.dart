library seen_model;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';

part 'seen_model.g.dart';

abstract class SeenModel implements Built<SeenModel, SeenModelBuilder> {
  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get userId;

  @nullable
  String get messageId;

  @nullable
  String get chatId;

  SeenModel._();

  factory SeenModel([updates(SeenModelBuilder b)]) = _$SeenModel;

  String toJson() {
    return json.encode(serializers.serializeWith(SeenModel.serializer, this));
  }

  static SeenModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        SeenModel.serializer, json.decode(jsonString));
  }

  static Serializer<SeenModel> get serializer => _$seenModelSerializer;
}
