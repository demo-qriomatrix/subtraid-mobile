library chat_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:Subtraid/core/serializers/serializers.dart';
import 'package:Subtraid/features/chat/models/message_model.dart';
import 'package:Subtraid/features/chat/models/seen_model.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';

part 'chat_model.g.dart';

abstract class ChatModel implements Built<ChatModel, ChatModelBuilder> {
  @nullable
  BuiltList<MemberModel> get members;

  @nullable
  BuiltList<MessageModel> get messages;

  @nullable
  @BuiltValueField(wireName: '_id')
  String get id;

  @nullable
  String get chatId;

  @nullable
  String get type;

  @nullable
  BuiltList<SeenModel> get seen;

  @nullable
  String get name;

  @nullable
  String get dateCreated;

  @nullable
  @BuiltValueField(wireName: '__v')
  int get version;

  ChatModel._();

  factory ChatModel([updates(ChatModelBuilder b)]) = _$ChatModel;

  String toJson() {
    return json.encode(serializers.serializeWith(ChatModel.serializer, this));
  }

  static ChatModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ChatModel.serializer, json.decode(jsonString));
  }

  static Serializer<ChatModel> get serializer => _$chatModelSerializer;
}
