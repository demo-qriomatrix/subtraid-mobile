// GENERATED CODE - DO NOT MODIFY BY HAND

part of message_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MessageModel> _$messageModelSerializer =
    new _$MessageModelSerializer();

class _$MessageModelSerializer implements StructuredSerializer<MessageModel> {
  @override
  final Iterable<Type> types = const [MessageModel, _$MessageModel];
  @override
  final String wireName = 'MessageModel';

  @override
  Iterable<Object> serialize(Serializers serializers, MessageModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('_id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.author != null) {
      result
        ..add('author')
        ..add(serializers.serialize(object.author,
            specifiedType: const FullType(MemberModel)));
    }
    if (object.type != null) {
      result
        ..add('type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(String)));
    }
    if (object.content != null) {
      result
        ..add('content')
        ..add(serializers.serialize(object.content,
            specifiedType: const FullType(String)));
    }
    if (object.chatId != null) {
      result
        ..add('chatId')
        ..add(serializers.serialize(object.chatId,
            specifiedType: const FullType(String)));
    }
    if (object.dateCreated != null) {
      result
        ..add('dateCreated')
        ..add(serializers.serialize(object.dateCreated,
            specifiedType: const FullType(String)));
    }
    if (object.failed != null) {
      result
        ..add('failed')
        ..add(serializers.serialize(object.failed,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  MessageModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MessageModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'author':
          result.author.replace(serializers.deserialize(value,
              specifiedType: const FullType(MemberModel)) as MemberModel);
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'content':
          result.content = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'chatId':
          result.chatId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'dateCreated':
          result.dateCreated = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'failed':
          result.failed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$MessageModel extends MessageModel {
  @override
  final String id;
  @override
  final MemberModel author;
  @override
  final String type;
  @override
  final String content;
  @override
  final String chatId;
  @override
  final String dateCreated;
  @override
  final bool failed;

  factory _$MessageModel([void Function(MessageModelBuilder) updates]) =>
      (new MessageModelBuilder()..update(updates)).build();

  _$MessageModel._(
      {this.id,
      this.author,
      this.type,
      this.content,
      this.chatId,
      this.dateCreated,
      this.failed})
      : super._();

  @override
  MessageModel rebuild(void Function(MessageModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MessageModelBuilder toBuilder() => new MessageModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MessageModel &&
        id == other.id &&
        author == other.author &&
        type == other.type &&
        content == other.content &&
        chatId == other.chatId &&
        dateCreated == other.dateCreated &&
        failed == other.failed;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), author.hashCode),
                        type.hashCode),
                    content.hashCode),
                chatId.hashCode),
            dateCreated.hashCode),
        failed.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MessageModel')
          ..add('id', id)
          ..add('author', author)
          ..add('type', type)
          ..add('content', content)
          ..add('chatId', chatId)
          ..add('dateCreated', dateCreated)
          ..add('failed', failed))
        .toString();
  }
}

class MessageModelBuilder
    implements Builder<MessageModel, MessageModelBuilder> {
  _$MessageModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  MemberModelBuilder _author;
  MemberModelBuilder get author => _$this._author ??= new MemberModelBuilder();
  set author(MemberModelBuilder author) => _$this._author = author;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _content;
  String get content => _$this._content;
  set content(String content) => _$this._content = content;

  String _chatId;
  String get chatId => _$this._chatId;
  set chatId(String chatId) => _$this._chatId = chatId;

  String _dateCreated;
  String get dateCreated => _$this._dateCreated;
  set dateCreated(String dateCreated) => _$this._dateCreated = dateCreated;

  bool _failed;
  bool get failed => _$this._failed;
  set failed(bool failed) => _$this._failed = failed;

  MessageModelBuilder();

  MessageModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _author = _$v.author?.toBuilder();
      _type = _$v.type;
      _content = _$v.content;
      _chatId = _$v.chatId;
      _dateCreated = _$v.dateCreated;
      _failed = _$v.failed;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MessageModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MessageModel;
  }

  @override
  void update(void Function(MessageModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MessageModel build() {
    _$MessageModel _$result;
    try {
      _$result = _$v ??
          new _$MessageModel._(
              id: id,
              author: _author?.build(),
              type: type,
              content: content,
              chatId: chatId,
              dateCreated: dateCreated,
              failed: failed);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'author';
        _author?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MessageModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
