import 'package:Subtraid/core/enums/chat_from_to_enum.dart';

class DummyChat {
  String message;
  ChatFromTo chatFromTo;

  DummyChat({this.message, this.chatFromTo});
}
