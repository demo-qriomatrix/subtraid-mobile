import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/features/chat/bloc/chat_bloc.dart';
import 'package:Subtraid/features/chat/widgets/chat_group_widget.dart';
import 'package:Subtraid/features/chat/widgets/chat_tab_bat_view_widget.dart';
import 'package:Subtraid/features/shared/styles.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatsScreen extends StatefulWidget {
  @override
  _ChatsScreenState createState() => _ChatsScreenState();
}

class _ChatsScreenState extends State<ChatsScreen>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 4, vsync: this, initialIndex: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return Scaffold(
            backgroundColor: ColorConfig.primary,
            appBar: StAppBar(
              title: 'Messages',
            ),
            body: ListView(children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  keyboardType: TextInputType.text,
                  decoration: searchInputDecoration.copyWith(
                      hintText: 'People, group and messages'),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: getMinHeight(context) - 50,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Stack(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Flexible(
                                flex: 9,
                                fit: FlexFit.tight,
                                child: Container(
                                  height: 50,
                                  child: TabBar(
                                    indicatorColor: Colors.blue.shade900,
                                    indicatorSize: TabBarIndicatorSize.tab,
                                    isScrollable: true,
                                    controller: tabController,
                                    indicatorWeight: 3,
                                    labelPadding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 0),
                                    indicatorPadding: EdgeInsets.symmetric(
                                        horizontal: 0, vertical: 0),
                                    labelColor: ColorConfig.primary,
                                    unselectedLabelColor: ColorConfig.grey16,
                                    onTap: (val) {
                                      context.read<ChatBloc>().add(
                                          ChatCreateGroup(createGroup: false));
                                      // setState(() {
                                      //   createGroup = false;
                                      // });
                                    },
                                    tabs: <Widget>[
                                      Tab(
                                        icon: Container(),
                                        text: '    All    ',
                                      ),
                                      Tab(
                                        icon: Container(),
                                        text: '  Chats  ',
                                      ),
                                      Tab(
                                        icon: Container(),
                                        text: 'Groups',
                                      ),
                                      Tab(
                                        icon: Container(),
                                        text: 'Project',
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              // Flexible(
                              //   flex: 2,
                              //   fit: FlexFit.tight,
                              //   child: Row(
                              //     children: <Widget>[
                              //       RawMaterialButton(
                              //         fillColor: ColorConfig.primary,
                              //         materialTapTargetSize:
                              //             MaterialTapTargetSize.shrinkWrap,
                              //         constraints: BoxConstraints(),
                              //         onPressed: () {
                              //           // setState(() {
                              //           //   createGroup = true;
                              //           // });

                              //           context.read<ChatBloc>().add(
                              //               ChatCreateGroup(createGroup: true));
                              //         },
                              //         shape: CircleBorder(),
                              //         padding: const EdgeInsets.all(4.0),
                              //         child: Icon(
                              //           Icons.add,
                              //           color: Colors.white,
                              //           size: 24,
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // )
                            ],
                          ),
                          Positioned.fill(
                            bottom: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: Container(
                                    color:
                                        ColorConfig.primary.withOpacity(0.43),
                                    height: 1,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                        child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Offstage(
                          offstage: state.createGroup,
                          child: ChatsTabBarViewWidget(
                              tabController: tabController),
                        ),
                        Offstage(
                          offstage: !state.createGroup,
                          child: CreateGroupWidget(),
                        )
                      ],
                    ))
                  ],
                ),
              )
            ]));
      },
    );
  }
}
