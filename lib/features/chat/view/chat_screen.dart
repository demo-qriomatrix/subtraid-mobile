import 'package:Subtraid/features/chat/bloc/chat_bloc.dart';
import 'package:Subtraid/features/chat/models/message_model.dart';
import 'package:Subtraid/features/chat/models/seen_model.dart';
import 'package:Subtraid/features/shared/models/member_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Subtraid/core/config/color_config.dart';
import 'package:Subtraid/core/config/local_images.dart';
import 'package:Subtraid/core/enums/chat_from_to_enum.dart';
import 'package:Subtraid/features/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:Subtraid/features/chat/models/chat_model.dart';
import 'package:Subtraid/features/chat/widgets/chat_list_widget.dart';
import 'package:Subtraid/features/shared/widgets/app_bar.dart';
import 'package:intl/intl.dart';

class ChatScreen extends StatefulWidget {
  final String chatId;

  ChatScreen(this.chatId);

  @override
  _ChatScreenState createState() => _ChatScreenState(chatId);
}

class _ChatScreenState extends State<ChatScreen> {
  final String chatId;

  _ChatScreenState(this.chatId);

  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();

    if (context.read<ChatBloc>().state is ChatLoadSuccess) {
      ChatModel chat = (context.read<ChatBloc>().state as ChatLoadSuccess)
          .allChatsMap[chatId];

      if (chat != null && chat.messages.isNotEmpty) {
        MessageModel lastMessage = chat.messages.first;

        if (lastMessage.id != null) {
          SeenModel seenModel = SeenModel((data) => data
            ..userId =
                context.read<AuthenticationBloc>().state is AuthentcatedState
                    ? (context.read<AuthenticationBloc>().state
                            as AuthentcatedState)
                        .user
                        .id
                    : null
            ..chatId = chat.id
            ..messageId = lastMessage.id);

          context.read<ChatBloc>().add(ChatUpdateSeen(seenModel: seenModel));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    String userId = context.select<AuthenticationBloc, String>((value) =>
        (value.state is AuthentcatedState)
            ? (value.state as AuthentcatedState).user.id
            : null);

    return BlocConsumer<ChatBloc, ChatState>(
      listener: (context, state) {
        if (state is ChatLoadSuccess) {
          ChatModel chat = state.allChatsMap[chatId];

          if (chat != null && chat.messages.isNotEmpty) {
            MessageModel lastMessage = chat.messages.first;

            if (chat.seen.isNotEmpty) {
              SeenModel usersLastSeen = chat.seen
                  .firstWhere((e) => e.userId == userId, orElse: () => null);

              if (usersLastSeen != null &&
                  lastMessage.id != null &&
                  usersLastSeen.messageId != lastMessage.id) {
                SeenModel seenModel = SeenModel((data) => data
                  ..userId = context.read<AuthenticationBloc>().state
                          is AuthentcatedState
                      ? (context.read<AuthenticationBloc>().state
                              as AuthentcatedState)
                          .user
                          .id
                      : null
                  ..chatId = chat.id
                  ..messageId = lastMessage.id);
                context
                    .read<ChatBloc>()
                    .add(ChatUpdateSeen(seenModel: seenModel));
              }
            }
          }
        }
      },
      builder: (context, state) {
        if (state is ChatLoadSuccess) {
          ChatModel chat = state.allChatsMap[chatId];

          return Scaffold(
              backgroundColor: ColorConfig.primary,
              appBar: StAppBar(
                title: getReceipientNameString(chat, context),
              ),
              body: Column(children: <Widget>[
                // Text(
                //   'Senior Supervisor',
                //   style: TextStyle(color: Colors.white70),
                // ),
                // SizedBox(
                //   height: 20,
                // ),
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50)),
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Stack(
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              child: Column(
                                children: <Widget>[
                                  Expanded(
                                    child: ListView.builder(
                                        reverse: true,
                                        scrollDirection: Axis.vertical,
                                        itemCount: chat.messages.length,
                                        padding: EdgeInsets.only(
                                            bottom: 150, top: 50),
                                        itemBuilder: (context, index) {
                                          if (index > 0
                                              // index <
                                              //   chat.messages.length - 1

                                              ) {
                                            return ChatBubble(
                                                chat.messages[index],
                                                chat.messages[index].author
                                                            .id ==
                                                        userId
                                                    ? ChatFromTo.from
                                                    : ChatFromTo.to,
                                                // chat.messages[index + 1].author
                                                chat.messages[index - 1].author
                                                            .id ==
                                                        userId
                                                    ? ChatFromTo.from
                                                    : ChatFromTo.to);
                                          } else {
                                            return ChatBubble(
                                                chat.messages[index],
                                                chat.messages[index].author
                                                            .id ==
                                                        userId
                                                    ? ChatFromTo.from
                                                    : ChatFromTo.to,
                                                null);
                                          }
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(50),
                                    topRight: Radius.circular(50)),
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
                                  decoration: BoxDecoration(
                                    color: ColorConfig.primary,
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      IconButton(
                                        icon: ImageIcon(
                                          AssetImage(LocalImages.image),
                                          color: ColorConfig.orange,
                                        ),
                                        onPressed: () {},
                                      ),
                                      Expanded(
                                        child: TextFormField(
                                          controller: textEditingController,
                                          decoration: InputDecoration(
                                              fillColor: Colors.white,
                                              filled: true,
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      horizontal: 10,
                                                      vertical: 20),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide.none,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          30)),
                                              suffixIcon: Padding(
                                                padding:
                                                    EdgeInsets.only(right: 10),
                                                child: RawMaterialButton(
                                                  onPressed: () {
                                                    if (textEditingController
                                                            .text !=
                                                        '') {
                                                      MessageModel message = MessageModel(
                                                          (data) => data
                                                            ..chatId =
                                                                widget.chatId
                                                            ..type = 'text'
                                                            ..author = MemberModel(
                                                                    (author) => author
                                                                      ..id =
                                                                          userId)
                                                                .toBuilder()
                                                            ..dateCreated =
                                                                DateTime.now()
                                                                    .toIso8601String()
                                                            ..content =
                                                                textEditingController
                                                                    .text);

                                                      context
                                                          .read<ChatBloc>()
                                                          .add(ChatAddMessage(
                                                              message:
                                                                  message));

                                                      textEditingController
                                                          .clear();

                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              new FocusNode());
                                                    }
                                                    // print(textEditingController
                                                    //     .text.isNotEmpty);
                                                    // if (textEditingController
                                                    //     .text.isNotEmpty) {
                                                    //   setState(() {
                                                    //     chat.messages.insert(
                                                    //       0,
                                                    //       DummyChat(
                                                    //           message:
                                                    //               textEditingController
                                                    //                   .text,
                                                    //           chatFromTo:
                                                    //               ChatFromTo.from),
                                                    //     );
                                                    //   });

                                                    //   textEditingController.clear();
                                                    // }
                                                  },
                                                  constraints: BoxConstraints(),
                                                  elevation: 0,
                                                  fillColor: Colors.white,
                                                  child: ImageIcon(
                                                    AssetImage(
                                                        LocalImages.send),
                                                    color: ColorConfig.orange,
                                                  ),
                                                  shape: CircleBorder(),
                                                ),
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        )),
                  ),
                )
              ]));
        }

        return Container();
      },
    );
  }
}

class ChatBubble extends StatelessWidget {
  final ChatFromTo chatFromTo;
  final MessageModel messageModel;
  final ChatFromTo previouseMessage;

  ChatBubble(this.messageModel, this.chatFromTo, this.previouseMessage);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: getAlignment(chatFromTo),
            children: <Widget>[
              getAvatarFrom(chatFromTo)
                  ? Row(
                      children: <Widget>[
                        previouseMessage != null &&
                                getAvatarFrom(previouseMessage)
                            ? Container(
                                width: 40,
                              )
                            : CircleAvatar(
                                backgroundImage:
                                    AssetImage(LocalImages.defaultAvatar),
                              ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    )
                  : Container(),
              Container(
                width: width * 0.6,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: getAlignment(chatFromTo),
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: getColor(chatFromTo),
                                borderRadius: BorderRadius.circular(15)
                                // BorderRadius.only(
                                //   bottomLeft: getAvatarFrom(chatFromTo)
                                //       ? Radius.circular(0)
                                //       : Radius.circular(20),
                                //   bottomRight: getAvatarFrom(chatFromTo)
                                //       ? Radius.circular(20)
                                //       : Radius.circular(0),
                                //   topLeft: getAvatarFrom(chatFromTo)
                                //       ? Radius.circular(20)
                                //       : Radius.circular(20),
                                //   topRight: getAvatarFrom(chatFromTo)
                                //       ? Radius.circular(20)
                                //       : Radius.circular(20),
                                // ),
                                ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                ConstrainedBox(
                                  constraints:
                                      BoxConstraints(maxWidth: width * 0.6),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        12.0, 12.0, 12.0, 8.0),
                                    child: Text(
                                      messageModel.content,
                                      style: TextStyle(
                                        color: getTextColor(chatFromTo),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      right: 8, bottom: 8, left: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Text(
                                        DateFormat('HH:mm aa').format(
                                            DateTime.parse(
                                                    messageModel.dateCreated)
                                                .toLocal()),
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: getTimeTextColor(chatFromTo),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      messageModel.failed == true
                          ? Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    'Couldn\'t deliver',
                                    style: TextStyle(color: ColorConfig.red1),
                                  )
                                ],
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ),
              getAvatarTo(chatFromTo)
                  ? Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        previouseMessage != null &&
                                getAvatarTo(previouseMessage)
                            ? Container(
                                width: 40,
                              )
                            : CircleAvatar(
                                backgroundImage:
                                    AssetImage(LocalImages.defaultAvatar),
                              )
                      ],
                    )
                  : Container(),
            ],
          ),
          getAvatarTo(chatFromTo) &&
                      !(previouseMessage != null &&
                          getAvatarTo(previouseMessage)) ||
                  getAvatarFrom(chatFromTo) &&
                      !(previouseMessage != null &&
                          getAvatarFrom(previouseMessage))
              ? Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Row(
                    mainAxisAlignment: getAvatarFrom(chatFromTo)
                        ? MainAxisAlignment.start
                        : MainAxisAlignment.end,
                    children: [
                      Container(
                        width: 50,
                      ),
                      Text(
                        DateFormat('MM/dd/yyyy HH:mm aa').format(
                            DateTime.parse(messageModel.dateCreated).toLocal()),
                        style: TextStyle(
                          fontSize: 12,
                          color: ColorConfig.grey4,
                        ),
                      ),
                      Container(
                        width: 50,
                      ),
                    ],
                  ),
                )
              : Container(),
          // getAvatarFrom(chatFromTo) &&
          //         !(previouseMessage != null && getAvatarFrom(previouseMessage))
          //     ? Text(
          //         DateFormat('HH:mm aa dd-MMM').format(
          //             DateTime.parse(messageModel.dateCreated).toLocal()),
          //         style: TextStyle(
          //           fontSize: 10,
          //           color: getTimeTextColor(chatFromTo),
          //         ),
          //       )
          //     : Container(),
        ],
      ),
    );
  }

  bool getAvatarFrom(ChatFromTo chatFromTo) {
    switch (chatFromTo) {
      case ChatFromTo.from:
        return false;
      case ChatFromTo.to:
        return true;
    }

    return null;
  }

  bool getAvatarTo(ChatFromTo chatFromTo) {
    switch (chatFromTo) {
      case ChatFromTo.to:
        return false;
      case ChatFromTo.from:
        return true;
    }

    return null;
  }

  MainAxisAlignment getAlignment(ChatFromTo chatFromTo) {
    switch (chatFromTo) {
      case ChatFromTo.from:
        return MainAxisAlignment.end;
      case ChatFromTo.to:
        return MainAxisAlignment.start;
    }

    return null;
  }

  Color getColor(ChatFromTo chatFromTo) {
    switch (chatFromTo) {
      case ChatFromTo.from:
        return ColorConfig.blue3;
      case ChatFromTo.to:
        return ColorConfig.primary;
    }

    return null;
  }

  Color getTextColor(ChatFromTo chatFromTo) {
    switch (chatFromTo) {
      case ChatFromTo.from:
        return Colors.black;
      case ChatFromTo.to:
        return Colors.white;
    }

    return null;
  }

  Color getTimeTextColor(ChatFromTo chatFromTo) {
    switch (chatFromTo) {
      case ChatFromTo.from:
        return ColorConfig.orange;
      case ChatFromTo.to:
        return Colors.white;
    }

    return null;
  }
}
