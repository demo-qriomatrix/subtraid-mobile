import 'dart:convert';

import 'package:Subtraid/core/config/endpoint_config.dart';
import 'package:Subtraid/core/http/http_client.dart';
import 'package:Subtraid/features/chat/models/chats_model.dart';
import 'package:Subtraid/features/chat/models/message_model.dart';
import 'package:Subtraid/features/chat/models/seen_model.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class ChatRepository {
  final HttpClient httpClient;

  ChatRepository({@required this.httpClient});

  Future<ChatsModel> getChats() async {
    final Response response = await httpClient.dio.get(
      EndpointConfig.userChats,
    );

    return ChatsModel.fromJson(json.encode(response.data));
  }

  Future<Response> updateSeen(SeenModel seenModel) async {
    final Response response = await httpClient.dio
        .patch(EndpointConfig.chatsUpdateSeen, data: seenModel.toJson());

    return response;
  }

  Future<Response> addMessage(MessageModel message) async {
    final Response response = await httpClient.dio
        .post(EndpointConfig.chatsAddMessage, data: message.toJson());

    return response;
  }
}
